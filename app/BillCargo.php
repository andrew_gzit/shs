<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillCargo extends Model
{
    public function containers(){
    	return $this->hasMany('App\CargoDetail', 'cargo_id', 'id');
    }
}
