<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\BillLading;
use \App\CargoDetail;

class BillCharge extends Model
{
	protected $table = "bill_charges";

	public function getCharge(){
		return $this->hasOne('\App\Charge', 'id', 'charge_code')->withTrashed();
	}

	public function getUnit(){
		$str = explode("-", $this->charge_unit);
		$bl = BillLading::find($this->bl_no);
		$unit = 0;

		if($bl->vessel_type == "LCL"){
			foreach($bl->getCargos AS $cargo){
				$unit += $cargo->weight;
			}
		} else {
			foreach($bl->getCargos AS $cargo){
				$exist = CargoDetail::where("cargo_id", $cargo->id)->where("size", $str[0])->where("type", $str[1])->count();

				if($exist > 0){
					$unit += $exist;
				}
			}
		}

		if($unit == 0){
			return 1;
		} else {
			return $unit;
		}
	}

	public function getChargeCargo(){
		return $this->hasMany('App\ChargeCargo', 'blcharge_id', 'id');
	}

	public function displayCharges(){

		
		
		$totalQty = $this->getTotalCargoQty($this->bl_no, $this->charge_unit);
		

        // $total_weight = this::getTotalCargoQty();

        // switch($str[1]){
        //     case "M3":
        //     $total_weight = $bl->getCargos->sum('volume');
        //     break;
        //     case "MT":
        //     $total_weight = $bl->getCargos->sum('weight');
        //     break;
        //     case "KG":
        //     $total_weight = $bl->getCargos->sum('weight');
        //     break;
        //     case "SET":
        //     $total_weight = 1;
        //     break;
        //     default:
        //     foreach($bl->getCargos AS $cargo){
        //         $total_weight += CargoDetail::where("cargo_id", $cargo->id)->where("size", $str[0])->where("type", $str[1])->count();
        //     }
        //     break;
        // }

        // if($str[1] == "M3"){
        //     $total_weight = $bl->getCargos->sum('volume');
        // } else {
        //     $total_weight = $bl->getCargos->sum('weight');
        // }

		return number_format($totalQty, 3) . " X " . $this->charge_rate . " / " . $this->getChargeUnit();
	}

	public function getChargeUnit(){
		$str = explode("-", $this->charge_unit);

		if($str[0] != 1){
			return $str[0] . '\'' . $str[1];
		} else {
			return $str[1];
		}
	}

	public function checkQty(){
		$bl_no = $this->bl_no;
		$unit_str = $this->charge_unit;

		$str = explode("-", $unit_str);
		$bl = BillLading::find($bl_no);

		$totalQty = 0;

		// Check if charges has cargo tied to it
		$chargeCargos = $this->getChargeCargo;

		if(count($chargeCargos) > 0){
			foreach($chargeCargos AS $cc){
				$cargo = $cc->getCargo;
				$totalQty += $cargo->cargo_qty;
			}
		} else {
			switch($str[1]){
				case "M3":
				$totalQty = $bl->getCargos->sum('volume');

				// Minimum quantity is 1 for M3 and MT
				$totalQty = ($totalQty < 1) ? 1 : $totalQty;
				break;
				case "MT":
				$totalQty = $bl->getCargos->sum('weight');

				$totalQty = ($totalQty < 1) ? 1 : $totalQty;
				break;
				case "KG":
				$totalQty = $bl->getCargos->sum('weight');
				break;
				case "SET":
				$totalQty = 1;
				break;
				case "UNIT":
				$totalQty = $bl->getCargos->reduce(function($carry, $item){
					if($item->cargo_packing == "UNIT"){
						return $carry + $item->cargo_qty;
					} else {
						return $carry;
					}
				});

				if($totalQty == null){
					$totalQty = $bl->getCargos->reduce(function($carry, $item){
						if($item->cargo_packing != "PACKAGE"){
							return $carry + $item->cargo_qty;
						} else {
							return $carry;
						}
					});
				}
				break;
				case "PACKAGE":
				$totalQty = $bl->getCargos->reduce(function($carry, $item){
					if($item->cargo_packing == "PACKAGE"){
						return $carry + $item->cargo_qty;
					} else {
						return;
					}
				});
				break;
				default:
				foreach($bl->getCargos AS $cargo){
					$totalQty += CargoDetail::where("cargo_id", $cargo->id)->where("size", $str[0])->where("type", $str[1])->count();
				}
				break;
			}

			if($totalQty == 0){
				$totalQty = 1;
			}

			// Minus quantity already has cargo tied to it
			$minusQty = 0;

			if($str[1] != "MT" && $str[1] != "M3"){
				$otrCharges = BillCharge::where("charge_code", $this->charge_code)->where("bl_no", $this->bl_no)->where("id", "!=", $this->id)->get();
				if(!empty($otrCharges)){
					foreach($otrCharges AS $charge){
						$cc = $charge->getChargeCargo;
						foreach($cc AS $c){
							$cargo = $c->getCargo;
							$minusQty += $cargo->cargo_qty;
						}
					}
				}

				$totalQty -= $minusQty;
			}
		}

		return $totalQty;
	}

	public function getTotalCargoQty($bl_no, $unit_str){
		$str = explode("-", $unit_str);
		$bl = BillLading::find($bl_no);

		$totalQty = 0;

		// Check if charges has cargo tied to it
		$chargeCargos = $this->getChargeCargo;

		if(count($chargeCargos) > 0){
			foreach($chargeCargos AS $cc){
				$cargo = $cc->getCargo;
				if($str[1] != "MT" && $str[1] != "M3" && $str[1] != "KG"){
					$totalQty += $cargo->cargo_qty;
				} else {
					if($str[1] == "MT" || $str[1] == "KG"){
						$totalQty += $cargo->weight;
					} else {
						$totalQty += $cargo->volume;
					}
				}
			}
		} else {
			switch($str[1]){
				case "M3":
				$totalQty = $bl->getCargos->sum('volume');

				// Minimum quantity is 1 for M3 and MT
				$totalQty = ($totalQty < 1) ? 1 : $totalQty;
				break;

				case "MT":
				$totalQty = $bl->getCargos->sum('weight');

				$totalQty = ($totalQty < 1) ? 1 : $totalQty;

				break;

				case "KG":
				$totalQty = $bl->getCargos->sum('weight');

				$totalQty = ($totalQty < 1) ? 1 : $totalQty;

				break;
				case "SET":
				$totalQty = 1;
				break;
				case "UNIT":
				$totalQty = $bl->getCargos->reduce(function($carry, $item){
					if($item->cargo_packing == "UNIT"){
						return $carry + $item->cargo_qty;
					} else {
						return $carry;
					}
				});


				if($totalQty == null){
					$totalQty = $bl->getCargos->reduce(function($carry, $item){
						if($item->cargo_packing != "PACKAGE"){
							return $carry + $item->cargo_qty;
						} else {
							return $carry;
						}
					});
				}
				break;
				case "PACKAGE":
				$totalQty = $bl->getCargos->reduce(function($carry, $item){
					if($item->cargo_packing == "PACKAGE"){
						return $carry + $item->cargo_qty;
					} else {
						return;
					}
				});
				break;
				default:
				foreach($bl->getCargos AS $cargo){
					$totalQty += CargoDetail::where("cargo_id", $cargo->id)->where("size", $str[0])->where("type", $str[1])->count();
				}
				break;
			}

			if($totalQty == 0){
				$totalQty = 1;
			}

			// Minus quantity already has cargo tied to it
			$minusQty = 0;

			if($str[1] != "MT" && $str[1] != "M3" && $str[1] != "KG"){
				$otrCharges = BillCharge::where("charge_code", $this->charge_code)->where("bl_no", $this->bl_no)->where("id", "!=", $this->id)->get();
				if(!empty($otrCharges)){
					foreach($otrCharges AS $charge){
						$cc = $charge->getChargeCargo;
						foreach($cc AS $c){
							$cargo = $c->getCargo;
							$minusQty += $cargo->cargo_qty;
						}
					}
				}

				$totalQty -= $minusQty;
			} else {

				//TODO : CHECK
				$otrCharges = BillCharge::where("charge_code", $this->charge_code)->where("bl_no", $this->bl_no)->where("id", "!=", $this->id)->get();
				if(!empty($otrCharges)){
					foreach($otrCharges AS $charge){
						$cc = $charge->getChargeCargo;
						foreach($cc AS $c){
							$cargo = $c->getCargo;
							if($str[1] == "MT" || $str[1] == "M3"){
								$minusQty += $cargo->weight;
							} else if ($str[1] == "KG") {
								$minusQty += $cargo->volume;
							}
						}
						$totalQty -= $minusQty;
					}
				}

			}
		}

		return $totalQty;
	}  

	public function calculateCharges(){
		$totalQty = $this->getTotalCargoQty($this->bl_no, $this->charge_unit);
		return round($totalQty * $this->charge_rate, 2);
	}
}
