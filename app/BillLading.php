<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \App\VoyageDestinations;
use Vinkla\Hashids\Facades\Hashids;

class BillLading extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at', 'bl_date'];

    public function pol() {
    	return $this->hasOne('App\Port', 'id', 'pol_id');
    }

    public function pol_time() {
        $dest = VoyageDestinations::where("voyage_id", $this->voyage_id)->where("port", $this->pol_id)->first();
        return $dest->etd->format("d/m/Y");
    }

    public function pod() {
    	return $this->hasOne('App\Port', 'id', 'pod_id');
    }

    public function fpd() {
        return $this->hasOne('App\Port', 'id', 'fpd_id');
    }

    public function shipper() {
    	return $this->hasOne('App\Company', 'id', 'shipper_id')->withTrashed();
    }

    public function consignee() {
    	return $this->hasOne('App\Company', 'id', 'consignee_id')->withTrashed();
    }

    public function notify() {
        return $this->hasOne('App\Company', 'id', 'notify_id')->withTrashed();
    }

    public function shipperAddresses(){
        return $this->hasMany('App\CompanyAddress', 'company_id', 'shipper_id');
    }

    public function consigneeAddresses(){
        return $this->hasMany('App\CompanyAddress', 'company_id', 'consignee_id');
    }

    public function notifyAddresses(){
        return $this->hasMany('App\CompanyAddress', 'company_id', 'notify_id');
    }

    public function getVessel(){
        return $this->hasOne('App\Vessel', 'id', 'vessel_id');
    }

    public function getVoyage(){
        return $this->hasOne('App\Voyage', 'id', 'voyage_id');
    }

    public function getCargos(){
        return $this->hasMany('App\BillCargo', 'bl_no', 'bl_no');
    }

    public function getBL_POL(){
        $voyage_dest = \App\VoyageDestinations::where("voyage_id", $this->voyage_id)->where("port", $this->pol_id)->first();
        return $voyage_dest;
    }

    public function getBooking(){
        return $this->hasOne('App\Booking', 'id', 'bc_no');
    }

    public function getTotalQty(){
        $cargos = BillCargo::where("bl_no", $this->bl_no)->get();
        if(!empty($cargos)){
            if(!empty($cargos[0]->cargo_qty)){
                return $cargos->sum("cargo_qty");
            } else {
                $sum = 0;
                foreach($cargos AS $cargo){
                    $str = $cargo->cargo_name;
                    $strArr = explode("&", $str);
                    foreach($strArr AS $ea){
                        $numArr = explode("x", $ea);
                        $sum += (int)$numArr[0];
                    }
                }
                return $sum;
            }
        }
    }

    public function getNoPackages(){
        $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);

        $strArr = [];
        $objArr = [];

        foreach($this->getCargos AS $key => $cargo){
            if($cargo->raw_qty != null){
                $semi_split = explode(";", $cargo->raw_qty);

                foreach($semi_split AS $key1 => $semi){
                    if(!empty($semi)){
                        $comma_split = explode(",", $semi);

                        //Create object to store size and type of cargo
                        $strObj = new \stdClass();
                        $strObj->size = $comma_split[0];
                        $strObj->type = $comma_split[1];
                        array_push($objArr, $strObj);
                    }
                }
            } else {
                //Create object to store size and type of cargo
                $strObj = new \stdClass();
                $strObj->size = $cargo->cargo_qty;
                $strObj->type = $cargo->cargo_packing;
                array_push($objArr, $strObj);
            }
        }

        $collection = collect($objArr);

        //Group by type of cargo first
        $groupedCollection = $collection->groupBy('type');

        foreach ($groupedCollection AS $key => $ea) {
            //Convert number of cargo to words
            $newStr = $f->format($ea->sum('size')) . " ";

            $cargo = explode("'", $key);
            if(count($cargo) > 1){
                //Convert cargo size to words
                $newStr .= $f->format($cargo[0]) . " " . $this->getCargoWord($cargo[1]);
            } else {
                $newStr .= $cargo[0];
            }
            array_push($strArr, $newStr);
        }

        $complete_str = "";
        foreach($strArr AS $key => $string){
            if($key == count($strArr) - 2){
                $complete_str .= $string . " AND ";
            } else if ($key == count($strArr) - 1){
                $complete_str .= $string;
            } else {
                $complete_str .= $string . ", ";
            }
        }

        if(!empty($complete_str)){
            return strtoupper($complete_str . " ONLY");
        } else {
            return '';
        }
    }

    public function getCargoWord($type){
        // switch($type){
        //     case "GP":
        //     return "General Purpose";
        //     break;
        //     case "HC":
        //     return "High Cube";
        //     break;
        // }
        return "FOOTER CONTAINER";
    }

    public function getNoBL(){
        $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);
        return $this->no_of_bl . " (" . strtoupper($f->format($this->no_of_bl)) . ")";
    }

    public function getCharges(){
        return $this->hasMany('App\BillCharge', 'bl_no', 'id');
    }

    public function getSumCharges($type){
        $sum = 0;

        foreach($this->getCharges->where("charge_payment", $type) AS $chg){
            $sum += $chg->calculateCharges();
        }

        // foreach($this->getCharges->where("charge_payment", $type) AS $chg){
        //     $sum += $chg->getUnit() * $chg->charge_rate;
        // }
        return $sum;
    }

    public function getFreightTerm(){
        if($this->freight_term == 0){
            return "PREPAID";
        } else if($this->freight_term == 1) {
            return "COLLECT";
        } else {
            return "PAYABLE";
        }
    }

    public function renderTelex()
    {
        return ($this->telex_release) ? '<span class="text-green">Yes</span>' : '<span class="text-danger">No</span>';
    }

    // Render HTML for bill lading datatable
    public function renderActions()
    {
									// 		<a class="dropdown-item" href="{{ action('BillLadingController@edit', ) }}">BL Details</a>
									// 		<a class="dropdown-item btn-cargo" href="{{ action('BillLadingController@cargo', Hashids::encode($outward->id)) }}" data-id="{{ $outward->id }}">Cargo</a>
									// 		<a class="dropdown-item btn-charges" href="{{ action('BillLadingController@charges', Hashids::encode($outward->id)) }}" data-id="{{ $outward->id }}">Charges</a>
									// 		<div class="dropdown-divider"></div>
									// 		<a class="dropdown-item btn-part" href="javascript:;" data-id="{{ $outward->id }}"><i class="fas fa-clone m-r-10"></i>Part BL</a>
									// 		<a class="dropdown-item btn-copy" href="{{ action('BillLadingController@create') }}?bl={{ $outward->bl_no }}"><i class="fas fa-copy m-r-10"></i>Copy BL</a>
									// 		{{-- <a class="dropdown-item btn-delete" href="" data-id="{{ $outward->id }}">Delete BL</a> --}}
									// 		<div class="dropdown-divider"></div>
									// 		<a class="dropdown-item btn-print-so" href="{{ action('BillLadingController@print_so', Hashids::encode($outward->id)) }}?bl=true"><i class="fas fa-print m-r-10"></i>Print Shipping Order</a>
									// 		<a class="dropdown-item btn-print" data-bl-id="{{ $outward->id }}" data-bl-no="{{ $outward->bl_no }}" href="javascript:;"><i class="fas fa-print m-r-10"></i>Print BL</a>
									// 	</div>
                                    // </div>

        return '<div class="dropdown">' .
        '<button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' .
        '<i class="fa fa-cog"></i>' .
        '</button>' .
        '<div class="dropdown-menu dropdown-menu-right f-s-14" aria-labelledby="dropdownMenuButton" data-id="' . $this->id . '">' .
        '<a class="dropdown-item" href="' . action('BillLadingController@edit', Hashids::encode($this->id)) . '">BL Details</a>' .
        '<a class="dropdown-item btn-cargo" href="' . action('BillLadingController@cargo', Hashids::encode($this->id)) . '">Cargo</a>' .
        '<a class="dropdown-item btn-charges" href="' . action('BillLadingController@charges', Hashids::encode($this->id)) . '">Charges</a>' .
        '<div class="dropdown-divider"></div>' .
        '<a class="dropdown-item btn-part" href="javascript:;" data-id="' . $this->id . '"><i class="fas fa-clone m-r-10"></i>Part BL</a>' .
        '<a class="dropdown-item btn-copy" href="' . action('BillLadingController@create', ['bl' => $this->bl_no ]) . '"><i class="fas fa-copy m-r-10"></i>Copy BL</a>' .
        '<div class="dropdown-divider"></div>' .
        '<a class="dropdown-item btn-print-so" href="' . action('BillLadingController@print_so', Hashids::encode($this->id) ) . '?bl=true"><i class="fas fa-print m-r-10"></i>Print Shipping Order</a>' .
        '<a class="dropdown-item btn-print" data-bl-id="' . $this->id . '" data-bl-no="' . $this->bl_no . '" href="javascript:;"><i class="fas fa-print m-r-10"></i>Print BL</a>' .
        '</div>' .
        '</div>';

    }

}
