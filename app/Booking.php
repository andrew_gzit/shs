<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
	use SoftDeletes;
	protected $dates = ['date', 'eta_pol', 'eta_pod', 'eta_fpd', 'haulge_request_date', 'cy_cutoff', 'deleted_at'];

	const status_open = 0;
	const status_closed = 1;
	const status_transferred = 2;
	const status_cancelled = 3;

	public function NBCT(){
		$port = \App\Port::where("location", "like", "%Penang%")->first();
		return $port->id;
	}

	const updateArr = [
		"date",
		"booking_no",
		"bookingparty_id",
		"shipper_id",
		"scn",
		"operator_code",
		"voyage_id",
		"shipment_type",
		"freight_term",
		"pol_id",
		"pol_sc",
		"eta_pol",
		"pod_id",
		"pod_sc",
		"eta_pod",
		"fpd_id",
		"fpd_sc",
		"eta_fpd",
		"cont_ownership"
	];

	const updateArrText = [
		"Date",
		"Booking No",
		"Booking Party",
		"Shipper",
		"SCN",
		"Operator Code",
		"Voyage",
		"Shipment Type",
		"Freight Term",
		"POL",
		"POL Station Code",
		"ETA POL",
		"POD",
		"POD Station Code",
		"ETA POD",
		"FPD",
		"FPD Station Code",
		"ETA FPD",
		"Container Ownership"
	];

	const type = [
		"Open",
		"Closed",
		"Transferred",
		"Cancelled"
	];

	public function getVoyage(){
		return $this->hasOne('App\Voyage', 'id', 'voyage_id')->withTrashed();
	}

	public function getBookingParty() {
		return $this->hasOne('App\Shipper', 'company_id', 'bookingparty_id')->withTrashed();
	}

	public function getShipper() {
		return $this->hasOne('App\Shipper', 'company_id', 'shipper_id')->withTrashed();
	}

	public function getAmendment() {
		return $this->hasMany('App\BookingAmendment', 'booking_id', 'id')->groupBy('revision');
	}

	public function getPol(){
		return $this->hasOne('App\Port', 'id', 'pol_id');
	}

	public function getPod(){
		return $this->hasOne('App\Port', 'id', 'pod_id');
	}

	public function getFpd(){
		return $this->hasOne('App\Port', 'id', 'fpd_id');
	}

	public function checkVoyageExist(){
		$voy = \App\Voyage::find($this->voyage_id);
		if(!empty($voy)){
			return true;
		} else {
			return false;
		}
	}

}
