<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Charge extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function getCharges(){
    	return $this->hasMany('App\ChargeDetail');
    }
}
