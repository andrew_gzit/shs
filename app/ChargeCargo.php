<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChargeCargo extends Model
{
    public function getCargo(){
    	return $this->hasOne('App\BillCargo', 'id', 'cargo_id');
    }
}
