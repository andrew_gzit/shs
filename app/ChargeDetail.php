<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChargeDetail extends Model
{
    public function chargeDetailText(){
    	if($this->unit_size == 1){
    		return "PER " . $this->unit;
    	} else {
    		return "PER " . $this->unit_size . " " . $this->unit;
    	}
    }
}
