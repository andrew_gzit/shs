<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChargeVessel extends Model
{
    public function getVessel(){
    	return $this->hasOne('App\Vessel', 'id', 'vessel_id');
    }
}
