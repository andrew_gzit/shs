<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['type', 'name', 'roc_no', 'port_code', 'ba_code', 'telephone', 'fax'];
    
    const type_shipper = 0;
    const type_consignee = 1;
    const type_shipperconsignee = 2;
    const type_agent = 3;
    const type_basecompany = 4;

    const type = [
        "Shipper",
        "Consignee",
        "Shipper & Consignee",
        "Agent"
    ];

    public function getAddresses(){
    	return $this->hasMany('App\CompanyAddress');
    }

    public function getEmails(){
    	return $this->hasMany('App\CompanyEmail');
    }

    public function getStatus(){
        return Company::type[$this->type];
    }
    
    public function getShipper(){
        return $this->hasOne('App\Shipper', 'company_id', 'id');
    }

    public function isShipper(){
        if($this->type == Company::type_shipper || $this->type == Company::type_shipperconsignee){
            return true;
        } else {
            return false;
        }
    }

    public function getDefaultCharges(){
        $shipper = \App\Shipper::where("company_id", $this->id)->first();
        return $this->hasMany('App\DefaultCharge');
    }

    public function getCompanyOfType(){
        switch($this->type){
            case $this::type_shipper:
            $company_model = \App\Shipper::where("company_id", $this->id)->first();
            break;
            case $this::type_consignee:
            $company_model = \App\Consignee::where("company_id", $this->id)->first();
            break;
            case $this::type_shipperconsignee:
            $company_model = \App\Shipper::where("company_id", $this->id)->first();
            break;
            case $this::type_agent:
            $company_model = \App\Agent::where("company_id", $this->id)->first();
            break;
        }

        return $company_model;
    }

}
