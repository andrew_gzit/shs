<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Consignee extends Model
{
	use SoftDeletes;
	
    protected $fillable = ['company_id', 'code', 'telephone', 'fax', 'pic'];

    public function getAddresses(){
    	return $this->hasMany('App\CompanyAddress', 'company_id', 'company_id');
    }
}
