<?php

namespace App;

use App\DataTableFilter;
use Carbon\Carbon;

class DataTable {

	public function filterData(DataTable $dt){
		$data = $dt->query;

		$totalCount = $data->get()->count();

		if(isset($dt->search)){
			$search = $dt->search;

			if(!empty($search)){
				// For input field
				$data = $data->where(function($query) use($search, $dt, $data){
					foreach($dt->searchables AS $index => $column){
						if($index == 0){
							$query->where($column, 'LIKE', "%$search%");
						} else {
							$query->orWhere($column, 'LIKE', "%$search%");
						}
					}
				});
			}
		}

		// For custom filtering
		if(isset($dt->filters)){
			foreach($dt->filters AS $filter){
				switch($filter->type){
					case "date":

					if($filter->date_type === DataTableFilter::DATE_SINGLE){

						$data = $data->where($filter->key, $filter->value);

					} else if($filter->date_type === DataTableFilter::DATE_RANGE){

						$data = $data->where($filter->key, '>=', $filter->start_date->format('Y-m-d H:i:s'))
						->where($filter->key, '<=', $filter->end_date->format('Y-m-d H:i:s'));

					} else {
						throw new Exception("Invalid date type entered. Expecting: single / range", 1);
					}

					break;
					default:
					$data = $data->where($filter->key, $filter->value);
				}
			}
		}

        $dataFiltered = clone $data;

		$dataFiltered = $dataFiltered->offset($dt->start)->limit($dt->limit)->orderBy($dt->order, $dt->dir)->get();

        $data = $data->get();

		$dt->data = $dataFiltered;
		$dt->recordsTotal = $totalCount;
		$dt->recordsFiltered = count($data);

		return $dt;

	}

}