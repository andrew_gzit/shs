<?php

namespace App;

class DataTableFilter
{
	const DATE = "date";
	const STR = "str";
	const DATE_SINGLE = "single";
	const DATE_RANGE = "range";
}