<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DefaultCharge extends Model
{
	public function getPort($port_id)
	{
		if(!empty($port_id)){
			$port = \App\Port::find($port_id);
			return $port->code;
		} else {
			return "-";
		}
	}

    public function getPOL()
    {
        return $this->hasOne('App\Port', 'id', 'pol');
    }

	public function chargeDetailText(){
    	if($this->unit_size == 1){
    		return "PER " . $this->unit;
    	} else {
    		return "PER " . $this->unit_size . " " . $this->unit;
    	}
    }

    public function getCharge(){
    	return $this->hasOne('App\Charge', 'id', 'charge_id');
    }

    public function getChargeVessel(){
    	return $this->hasMany('App\ChargeVessel', 'charge_id', 'id');
    }

    public function getVesselList(){
        $vessel_name = collect();

        foreach($this->getChargeVessel AS $cv){
            $vessel_name->push($cv->getVessel->name);
        }

        return $vessel_name->implode(', ');
    }
}
