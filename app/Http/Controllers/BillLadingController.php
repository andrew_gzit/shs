<?php

namespace App\Http\Controllers;

use App\BillCargo;
use App\BillCharge;
use App\BillLading;
use App\BillNumber;
use App\BLFormat;
use App\Booking;
use App\BookingAmendment;

// Eloquent Models
use App\CargoDetail;
use App\Charge;
use App\ChargeCargo;
use App\Company;
use App\CompanyAddress;
use App\Consignee;
use App\DataTable;
use App\DataTableFilter;
use App\InwardBL;
use App\Port;
use App\Shipper;
use App\ShortcutKey;
use App\Vessel;
use App\Voyage;
use App\VoyageDestinations;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Storage;
use Vinkla\Hashids\Facades\Hashids;

class BillLadingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $shippers = Company::where("type", Company::type_shipper)->orWhere("type", Company::type_shipperconsignee)->get();
        $inward = InwardBL::latest()->get();
        $ports = Port::all();
        $vessels = Vessel::all();
        if (count($vessels) > 0) {
            $voyages = Voyage::where('vessel_id', $vessels[0]->id)->get();
        } else {
            $voyages = [];
        }

        //Get shipper that isnt deleted, prevent recovering BL with deleted company
        $shipper = Shipper::select("company_id")->get()->pluck('company_id');
        $consignee = Consignee::select("company_id")->get()->pluck('company_id');

        //Retrieve deleted BL where attached voyage is not deleted
        $outward_deleted = BillLading::onlyTrashed()
            ->join("voyages", "bill_ladings.voyage_id", "voyages.id")
            ->whereIn("shipper_id", $shipper)
            ->whereIn("consignee_id", $consignee)
            ->whereIn("notify_id", $consignee)
            ->where("voyages.deleted_at", null)
            ->select("bill_ladings.*")->get();

        $formats = BLFormat::get();

        if ($request->query("tab") == "inward") {
            Session::flash("inward", true);
        }

        return view('bl.index-bl', compact('vessels', 'voyages', 'ports', 'shippers', 'inward', 'outward_deleted'));

        $ladings = BillLading::all();

        $outwards = BillLading::orderBy('created_at', 'desc')->get();

        //Get shipper that isnt deleted, prevent recovering BL with deleted company
        $shipper = Shipper::select("company_id")->get()->pluck('company_id');
        $consignee = Consignee::select("company_id")->get()->pluck('company_id');

        return view('bl.index-bl', compact('voyages', 'vessels', 'ladings', 'outwards', 'outward_deleted', 'formats', 'ports', 'shippers', 'inward'));
    }

    public function billLadingDT(Request $request)
    {
        $columns = [
            '',
            'bl_no',
            'vessel',
            'voyage',
            'pol',
            'pod',
            'shipper',
            'telex_status',
            'created_at',
        ];

        // Setup DataTable configurations
        $dt = new DataTable();
        $dt->limit = $request->length;
        $dt->start = $request->start;
        $dt->order = $columns[$request->input('order.0.column')];
        $dt->dir = $request->input('order.0.dir');

        if (!empty($request->input('search.value'))) {
            $dt->search = $request->input('search.value');
            $dt->searchables = ['bl_no', 'vessels.name', 'voyages.voyage_id', 'shipper_name', 'pol_name', 'pod_name'];
        }

        $dt->filters = collect();
        $filter = $request->filter;

        if (!empty($filter['vessel_id']) && $filter['vessel_id'] != 'ALL') {
            $dtFilter = new DataTableFilter();
            $dtFilter->type = DataTableFilter::STR;
            $dtFilter->key = 'vessels.id';
            $dtFilter->value = $filter['vessel_id'];

            $dt->filters->push($dtFilter);

            if (!empty($filter['voyage_id']) && $filter['voyage_id'] != 'NULL') {
                $dtFilter = new DataTableFilter();
                $dtFilter->type = DataTableFilter::STR;
                $dtFilter->key = 'voyages.id';
                $dtFilter->value = $filter['voyage_id'];

                $dt->filters->push($dtFilter);
            }
        }

        if (!empty($filter['pol_id']) && $filter['pol_id'] != 'ALL') {
            $dtFilter = new DataTableFilter();
            $dtFilter->type = DataTableFilter::STR;
            $dtFilter->key = 'bill_ladings.pol_id';
            $dtFilter->value = $filter['pol_id'];

            $dt->filters->push($dtFilter);
        }

        if (!empty($filter['pod_id']) && $filter['pod_id'] != 'ALL') {
            $dtFilter = new DataTableFilter();
            $dtFilter->type = DataTableFilter::STR;
            $dtFilter->key = 'bill_ladings.pod_id';
            $dtFilter->value = $filter['pod_id'];

            $dt->filters->push($dtFilter);
        }

        if (!empty($filter['shipper_id']) && $filter['shipper_id'] != 'ALL') {
            $dtFilter = new DataTableFilter();
            $dtFilter->type = DataTableFilter::STR;
            $dtFilter->key = 'bill_ladings.shipper_id';
            $dtFilter->value = $filter['shipper_id'];

            $dt->filters->push($dtFilter);
        }

        $dt->query = BillLading::join('vessels', 'bill_ladings.vessel_id', 'vessels.id')
            ->join('voyages', 'bill_ladings.voyage_id', 'voyages.id')
            ->select(['vessels.name AS vessel_name', 'voyages.voyage_id AS voyage_name', 'bill_ladings.*']);

        $returnObj = $dt->filterData($dt);
        $dataCollection = collect();

        $billLadings = $returnObj->data;

        foreach ($billLadings as $bl) {
            $data['checkbox'] = "<input type='checkbox' class='chk_bl' value='" . $bl->id . "'>";
            $data['bl_no'] = $bl->bl_no;
            $data['vessel'] = $bl->vessel_name;
            $data['voyage'] = $bl->voyage_name;
            $data['pol'] = $bl->pol->code;
            $data['pod'] = $bl->pod->code;
            $data['shipper'] = $bl->shipper_name;
            $data['telex_release'] = $bl->renderTelex();
            $data['created_at'] = $bl->created_at->format('d/m/Y h:i A');
            $data['actions'] = $bl->renderActions();
            $dataCollection->push($data);
        }

        $jsonData = [
            "draw" => (int) $request->input('draw'),
            "recordsTotal" => (int) $returnObj->recordsFiltered,
            "recordsFiltered" => (int) $returnObj->recordsFiltered,
            "data" => $dataCollection,
        ];

        return json_encode($jsonData);
    }

    //Batch delete BL
    public function batch_delete(Request $request)
    {
        $bl_id = json_decode($request->txt_bl_id);

        foreach ($bl_id as $id) {
            $bl = BillLading::find($id);
            if (!empty($bl)) {

                $bl->delete();
            }
        }

        return redirect::back()->with("success", "Successfully deleted selected BLs");
    }

    //Recover deleted BL
    public function recover_bl($hashed, Request $request)
    {
        try {
            $decoded = Hashids::decode($hashed);
            if (!empty($decoded)) {
                $bl = BillLading::withTrashed()->find($decoded[0]);
                $bl->deleted_at = null;
                $bl->save();

                Session::flash("msg", "Successfully recovered selected BL - " . $bl->bl_no);
                Session::flash("class", "alert-success");
            } else {
                Session::flash("msg", "Recover BL unsuccessful. BL not found.");
                Session::flash("class", "alert-danger");
            }
            return redirect::action("BillLadingController@index");
        } catch (\Exception $e) {
            Session::flash("msg", "Recover BL unsuccessful. " . $e->getMessage());
            Session::flash("class", "alert-danger");
            return redirect::action("BillLadingController@index");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $vessels = Vessel::all();
            $ports = Port::all();
            $companies = Company::where('type', '!=', 0)->where('type', '!=', 4)->get();

            $notify_parties = collect();

            foreach ($companies as $comp) {
                $comp_type = $comp->getCompanyOfType();
                $comp_type->comp_id = $comp->id;
                $comp_type->name = $comp->name;
                $notify_parties->push($comp_type);
            }

            $notify_parties = $notify_parties->sortBy('code');

            $consignees = Consignee::orderBy("code", "ASC")->get();
            $shippers = Shipper::orderBy("code", "ASC")->get();

            $voyage_detail = [];
            $vessel_detail = [];
            $lading = [];

            //If create bill lading from booking
            $bk = null;
            if ($request->booking) {
                $bk = Booking::findOrFail($request->booking);
                $voyages = Voyage::where('booking_status', 0)->get();

                //Assign voyage and vessel from selected booking
                $request->voyage = $bk->voyage_id;
                $bk_vessel = Voyage::find($bk->voyage_id);
                $request->vessel = $bk_vessel->vessel->id;
            } else {
                if (!$request->bl) {
                    $voyages = Voyage::where('vessel_id', $request->vessel)->get();
                } else {
                    $voyages = Voyage::where('booking_status', 0)->get();
                }
            }

            if ($request->voyage && $request->vessel && $request->bl) {
                $voyage_detail = Voyage::find($request->voyage);
                $vessel_detail = Vessel::find($request->vessel);
                $lading = BillLading::where('bl_no', $request->bl)->first();

            } else if ($request->voyage && $request->vessel && !$request->bl) {
                $voyage_detail = Voyage::find($request->voyage);
                $vessel_detail = Vessel::find($request->vessel);
                $voyages = Voyage::where('vessel_id', $request->vessel)->where("booking_status", 0)->get();

            } else if ($request->voyage && !$request->vessel && $request->bl) {
                $voyage_detail = Voyage::find($request->voyage);
                $lading = BillLading::where('bl_no', $request->bl)->first();

            } else if (!$request->voyage && $request->vessel && $request->bl) {
                $vessel_detail = Vessel::find($request->vessel);
                $lading = BillLading::where('bl_no', $request->bl)->first();

            } else if ($request->voyage && !$request->vessel) {
                $voyage_detail = Voyage::find($request->voyage);

            } else if (!$request->voyage && $request->vessel) {
                $vessel_detail = Vessel::find($request->vessel);
                $voyages = Voyage::where('vessel_id', $request->vessel)->where('booking_status', 0)->get();

            } else if ($request->bl) {
                $lading = BillLading::where('bl_no', $request->bl)->first();
                $voyage_detail = Voyage::find($lading->voyage_id);
                $request->voyage = $lading->voyage_id;
            }

            if (!empty($voyage_detail)) {
                $dest = VoyageDestinations::where('voyage_id', $request->voyage)->orderBy('id', 'desc')->first();

                $voyage_detail->fpd_id = $dest->port;
                $voyage_detail->fpd_name = $dest->getPort->name;

                $pol = VoyageDestinations::where('voyage_id', $request->voyage)->orderBy('id', 'asc')->first();
                $voyage_detail->pol_name = $pol->getPort->name;
                $voyage_detail->pol_etd = $pol->etd;
            }

            if ($consignees->isNotEmpty()) {
                foreach ($consignees as $consignee) {
                    $company = Company::find($consignee->company_id);

                    if ($company) {
                        $consignee->name = $company->name;
                    }
                }
            }

            if ($shippers->isNotEmpty()) {
                foreach ($shippers as $shipper) {
                    $company = Company::find($shipper->company_id);

                    if ($company) {
                        $shipper->name = $company->name;
                    }
                }
            }

            if (count($voyages) == 0 && !$request->bl) {
                $request->session()->flash('no-voyage', 'Selected vessel does not seem to have any voyages.<br>Create a voyage now for selected vessel?<br><br><i>(cancelling will redirect you to the index page)</i>');
            }

            $booking = Booking::where("status", 0)->get();

            return view('bl.create-bl', compact('voyages', 'vessels', 'ports', 'voyage_detail', 'vessel_detail', 'companies', 'consignees', 'shippers', 'notify_parties', 'lading', 'booking', 'bk'));
        } catch (\Exception $e) {
            Session::flash("msg", "Unable to create BL. " . $e->getMessage());
            Session::flash("class", "alert-danger");
            return redirect::action("BillLadingController@index");
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $bill = new BillLading;
            $vessel = Vessel::find($request->vessel_id);

            if ($request->booking_number) {
                $booking = Booking::find($request->booking_number);
                $bill->bl_no = $booking->booking_no;
            } else {
                $bill->bl_no = $this->getNewBLNumber($request->pol_id, $request->fpd_id, $request->vessel_id, $request->vessel_type);
            }

            $bill->vessel_id = $request->vessel_id;
            $bill->vessel_type = $request->vessel_type;
            $bill->voyage_id = $request->voyage_id;
            $bill->pol_id = $request->pol_id;
            $bill->pol_name = $request->pol_name;
            $bill->pod_id = $request->pod_id;
            $bill->pod_name = $request->pod_name;
            $bill->fpd_id = $request->fpd_id;
            $bill->fpd_name = $request->fpd_name;

            $bill->shipper_id = $request->shipper_id;
            $bill->shipper_name = $request->shipper_name;
            $bill->shipper_add_id = $request->shipper_add_id;
            $bill->shipper_add = $request->shipper_add;

            $bill->consignee_id = $request->consignee_id;
            $bill->consignee_name = $request->consignee_name;
            $bill->consignee_add_id = $request->consignee_add_id;
            $bill->consignee_add = $request->consignee_add;

            $bill->notify_id = $request->notify_id;
            $bill->notify_name = $request->notify_name;
            $bill->notify_add_id = $request->notify_add_id;
            $bill->notify_add = $request->notify_add;

            $bill->no_of_bl = $request->no_of_bl;

            $bill->bl_date = Carbon::createFromFormat('d/m/Y', $request->bl_date)->format('Y-m-d H:i:s');
            $bill->freight_term = $request->freight_term;
            $bill->cont_ownership = $request->cont_owner;
            $bill->telex_status = $request->telex_release;

            if ($request->telex_attachment) {
                $filename = Storage::disk('public_uploads')->put("/", $request->telex_attachment);
                $bill->telex_path = $filename;
            }

            $bill->save();

            if ($request->booking_number) {
                $bill->bc_no = $request->booking_number;
                $bill->save();

                $booking = Booking::find($bill->bc_no);

                $booking->status = $booking::status_transferred;

                $booking->voyage_id = $request->voyage_id;

                $booking->pol_id = $request->pol_id;
                $booking->fpd_id = $request->fpd_id;

                //Saving amendments into booking
                $uid = str_random(5);
                foreach ($booking::updateArr as $key => $ea) {
                    if ($booking->isDirty($ea)) {
                        $amend = new BookingAmendment();
                        $amend->booking_id = $booking->id;
                        $amend->revision = $uid;
                        $amend->type = $booking::updateArrText[$key];
                        if ($ea == "pol_id" || $ea == "pod_id" || $ea == "fpd_id") {
                            $old_port = Port::find($booking->getOriginal($ea));
                            $port = Port::find($booking->$ea);
                            $amend->old_value = (!empty($booking->getOriginal($ea))) ? $old_port->code : "";
                            $amend->value = $port->code;
                        } else if ($ea == "voyage_id") {
                            $old_voy = Voyage::find($booking->getOriginal($ea));
                            $new_voy = Voyage::find($booking->ea);
                            $amend->old_value = $old_voy->voyage_id;
                            $amend->value = $new_voy->voyage_id;
                        } else {
                            $amend->old_value = (!empty($booking->getOriginal($ea))) ? $booking->getOriginal($ea) : "";
                            $amend->value = $booking->$ea;
                        }
                        $amend->save();
                    }
                }

                $booking->save();

                // Remove old cargo if exist
                $old_cargo = BillCargo::where("bl_no", $bill->bl_no)->delete();

                if (!empty($booking->weight)) {
                    $bl_cargo = new BillCargo();
                    $bl_cargo->bl_id = $bill->id;
                    $bl_cargo->bl_no = $bill->bl_no;

                    if ($bill->vessel_type == $booking->shipment_type) {
                        if ($bill->vessel_type == "FCL") {
                            $bl_cargo->raw_qty = $booking->cargo_qty;
                            $bl_cargo->cargo_name = $booking->cargo_packing;
                        } else {
                            $bl_cargo->cargo_qty = $booking->cargo_qty;
                            $bl_cargo->cargo_packing = $booking->cargo_packing;
                            $bl_cargo->cargo_name = $bl_cargo->cargo_qty . " " . $bl_cargo->cargo_packing;
                        }
                    }

                    $bl_cargo->uncode = $booking->uncode;
                    $bl_cargo->remarks = $booking->remarks;
                    $bl_cargo->cargo_nature = !empty($bl_cargo->uncode) ? "Dangerous" : "General";
                    $bl_cargo->weight = $booking->weight;
                    $bl_cargo->weight_unit = "MT";
                    $bl_cargo->volume = 0;
                    $bl_cargo->temperature = $booking->temperature;
                    $bl_cargo->cargo_desc = !empty($booking->cargo_desc) ? $booking->cargo_desc : "";
                    $bl_cargo->save();
                }

                // Remove other BL with same booking number

                $del_bl = BillLading::where("bc_no", $request->booking_number)->where("id", "!=", $bill->id)->delete();

            }

            if ($request->bl_no) {
                $bl = BillLading::where('bl_no', $request->bl_no)->first();
                $cargos = BillCargo::where('bl_id', $bl->id)->get();

                foreach ($cargos as $cargo) {
                    $newCargo = $cargo->replicate();

                    $newCargo->bl_id = $bill->id;
                    $newCargo->bl_no = $bill->bl_no;
                    $newCargo->save();

                    foreach ($cargo->containers as $container) {
                        $newContainer = $container->replicate();

                        $newContainer->cargo_id = $newCargo->id;
                        $newContainer->save();
                    }
                }
            }

            DB::commit();

            if ($request->next) {
                return redirect()->route('index-cargo', ['hash' => Hashids::encode($bill->id)])->with('success', 'Successfully added new BL.');
            }

            return redirect()->route('index-bl')->with('success', 'Successfully added new BL.');
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash("msg", "Unable to create BL. " . $e->getMessage());
            Session::flash("class", "alert-danger");
            return redirect::back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($hash)
    {
        $bl_no = Hashids::decode($hash);

        $bl = BillLading::find($bl_no[0]);
        $vessel = Vessel::find($bl->vessel_id);
        $voyages = Voyage::where('vessel_id', $vessel->id)->get();
        $voyage = Voyage::find($bl->voyage_id);

        $vessels = Vessel::all();
        $companies = Company::where('type', '!=', 0)->where('type', '!=', 4)->get();

        $notify_parties = collect();

        foreach ($companies as $comp) {
            $comp_type = $comp->getCompanyOfType();
            $comp_type->comp_id = $comp->id;
            $comp_type->name = $comp->name;
            $notify_parties->push($comp_type);
        }

        $notify_parties = $notify_parties->sortBy('code');

        $consignees = Consignee::orderBy("code", "ASC")->get();
        $shippers = Shipper::orderBy("code", "ASC")->get();

        if ($consignees->isNotEmpty()) {
            foreach ($consignees as $consignee) {
                $company = Company::find($consignee->company_id);

                if ($company) {
                    $consignee->name = $company->name;
                }
            }
        }

        if ($shippers->isNotEmpty()) {
            foreach ($shippers as $shipper) {
                $company = Company::find($shipper->company_id);

                if ($company) {
                    $shipper->name = $company->name;
                }
            }
        }

        return view('bl.edit-bl', compact('bl', 'vessel', 'voyages', 'voyage', 'vessels', 'companies', 'consignees', 'shippers', 'notify_parties'));
    }

    public function getNewBLNumber($pol_id, $pod_id, $vessel_id, $shipment_type)
    {
        $pol = Port::find($pol_id);
        $pod = Port::find($pod_id);
        $vessel = Vessel::find($vessel_id);

        $short_year = substr(Carbon::now()->format('y'), -1, 1);

        $search_str = $pol->bl_prefix . $pod->bl_prefix;

        if ($vessel->type == 'Container Vessel') {
            //<POL><POD><4-digit number running for the same POL & POD>

            // Query issue when
            // $bl = BillLading::where("bl_no", "LIKE", $search_str . "%")->where("bl_no", "NOT REGEXP", '.[0-9]{5}' .  $short_year . '$')->withTrashed()->latest()->first();
            $bl = BillLading::where("bl_no", "LIKE", $search_str . "____")->withTrashed()->latest()->first();

            if (!empty($bl)) {
                $bl_no = substr($bl->bl_no, strlen($search_str));
                $new_bl_no = (int) $bl_no + 1;
            } else {
                $new_bl_no = "0001";
            }

            $bl_no = $search_str . str_pad($new_bl_no, 4, 0, STR_PAD_LEFT);
        } else {
            //General Cargo Vessel
            //<POL><POD><1 for LCL / 2 for FCL><4-digit number running for all BL><single digit year>

            if ($shipment_type == "LCL") {
                $shipment_num = "1";
            } else {
                $shipment_num = "2";
            }

            $billnumber = BillNumber::where("vessel_default", "General Cargo Vessel")->first();
            $billnumber->current_number = $billnumber->current_number + 1;

            if ($billnumber->current_number > 9999) {
                $billnumber->current_number = 1;
            }

            $billnumber->save();

            $new_bl_no = str_pad($billnumber->current_number, 4, 0, STR_PAD_LEFT);

            // $bl = BillLading::where("bl_no", "REGEXP", '.[0-9]{5}' .  $short_year . '$')->withTrashed()->latest()->first();

            // $booking = Booking::where("booking_no", "LIKE", "%____" . $short_year)->where("booking_no", "NOT LIKE", "SYMT%")->withTrashed()->latest()->first();

            // if(!empty($booking)){
            //     $bk_no = substr($booking->booking_no, -5, 4);

            //     if(!empty($bl)){
            //         $bl_no = substr($bl->bl_no, -5, 4);

            //         if($bk_no > $bl_no){
            //             $new_bl_no = (int)$bk_no+1;
            //         } else {
            //             $new_bl_no = (int)$bl_no+1;
            //         }
            //     }
            // }

            // if(empty($new_bl_no)){
            //     $new_bl_no = "0001";
            // }

            // if(!empty($bl)){
            //     $bl_no = substr($bl->bl_no, -5, 4);
            //     $new_bl_no = (int)$bl_no + 1;
            // } else {
            //     $new_bl_no = "0001";
            // }

            $bl_no = $search_str . $shipment_num . str_pad($new_bl_no, 4, 0, STR_PAD_LEFT) . $short_year;
        }

        return $bl_no;

        // if($vessel->type == 'General Cargo Vessel' && $shipment_type == 'FCL') {
        //     $latestNum = BillNumber::where('vessel_default', $shipment_type)->latest()->first();
        //     $currNum = $latestNum->current_number + 1;

        //     if($currNum == 2000) {
        //         $bl_no .= 1001;
        //         $latestNum->current_number = 1001;
        //     } else {
        //         $bl_no .= $currNum;
        //         $latestNum->current_number = $currNum;
        //     }

        //     $latestNum->save();
        // } else if($vessel->type == 'General Cargo Vessel' && $shipment_type == 'LCL') {
        //     $latestNum = BillNumber::where('vessel_default', $shipment_type)->latest()->first();
        //     $currNum = $latestNum->current_number + 1;

        //     if($currNum == 1000) {
        //         $bl_no .= sprintf('%04d', 1);
        //         $latestNum->current_number = 1;
        //     } else {
        //         $bl_no .= sprintf('%04d', $currNum);
        //         $latestNum->current_number = $currNum;
        //     }

        //     $latestNum->save();
        // } else {
        //     /**
        //         Search string = (POL BL prefix)(Number for month / ABC for 2 digit month) + [001](running number)
        //         Start with num 1 if search string doesn't exist
        //     **/
        //         $search = $pol->bl_prefix . Carbon::now()->format("y");
        //         $month = Carbon::now()->format("n");
        //         if($month >= 10){
        //             $ascii = 65 + ($month - 10);
        //             $search .= chr($ascii);
        //         } else {
        //             $search .= $month;
        //         }

        //         $latestNum = BillLading::where("bl_no", "LIKE", $search . "%")->withTrashed()->latest()->first();
        //         if(!empty($latestNum)){
        //             $currNum = substr($latestNum->bl_no, strlen($search));
        //             $newNum = (int)$currNum+1;
        //             $bl_no = ($newNum > 1000) ? $search . "001" : $search . str_pad($newNum, 3, 0, STR_PAD_LEFT);
        //         } else {
        //             $bl_no = $search . "001";
        //         }
        //     }
        //     return $bl_no;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $bl = BillLading::find($id);

            $bl->vessel_type = $request->vessel_type;

            if ($bl->isDirty("vessel_type")) {
                $new_bl_no = $this->getNewBLNumber($request->pol_id, $request->fpd_id, $request->vessel_id, $bl->vessel_type);

                $bl->delete();
                $bl = new BillLading();
                $bl->bl_no = $new_bl_no;
                $bl->vessel_type = $request->vessel_type;
            }

            $bl->vessel_id = $request->vessel_id;
            $bl->voyage_id = $request->voyage_id;
            $bl->pol_id = $request->pol_id;
            $bl->pol_name = $request->pol_name;
            $bl->pod_id = $request->pod_id;
            $bl->pod_name = $request->pod_name;
            $bl->fpd_id = $request->fpd_id;
            $bl->fpd_name = $request->fpd_name;

            $bl->shipper_id = $request->shipper_id;
            $bl->shipper_name = $request->shipper_name;
            $bl->shipper_add_id = $request->shipper_add_id;
            $bl->shipper_add = $request->shipper_add;

            $bl->consignee_id = $request->consignee_id;
            $bl->consignee_name = $request->consignee_name;
            $bl->consignee_add_id = $request->consignee_add_id;
            $bl->consignee_add = $request->consignee_add;

            $bl->notify_id = $request->notify_id;
            $bl->notify_name = $request->notify_name;
            $bl->notify_add_id = $request->notify_add_id;
            $bl->notify_add = $request->notify_add;

            $bl->no_of_bl = $request->no_of_bl;

            $bl->bl_date = Carbon::createFromFormat('d/m/Y', $request->bl_date)->format('Y-m-d H:i:s');
            $bl->freight_term = $request->freight_term;
            $bl->cont_ownership = $request->cont_owner;
            $bl->telex_status = $request->telex_release;

            if (!$bl->telex_status) {
                $bl->telex_path = null;
            } else if ($bl->telex_status && $request->telex_attachment != null) {
                $filename = Storage::disk('public_uploads')->put("/", $request->telex_attachment);
                $bl->telex_path = $filename;
            }

            $bl->save();

            switch ($request->step) {
                case "save":
                    Session::flash("msg", 'Successfully updated BL ');
                    Session::flash("class", "alert-success");
                    return redirect::back();
                    // return redirect()->route('index-bl')->with('success', 'Successfully updated BL ' . $bl->bl_no);
                    break;
                case "cargo":
                    return redirect()->route('index-cargo', ['hash' => Hashids::encode($bl->id)]);
                    break;
                case "charges":
                    return redirect()->action('BillLadingController@charges', Hashids::encode($bl->id));
                // return redirect()->route('index-cargo', ['hash' => Hashids::encode($bill->id)])->with('success', 'Successfully added new BL.');
                case "so":
                    $hash = Hashids::encode($bl->id);
                    return redirect("/bl/" . $hash . "/print-so?id=" . $bl->id);
                    // return redirect()->action('BillLadingController@print_so', Hashids::encode($bl->id));
                    break;
                case "bl":
                    $request->cname = explode(",", $request->cname);
                    return redirect()->action('BillLadingController@redirect_format', [$bl->id, $request->bl_format, $request->cname[0], $request->cname[1]]);
                    break;
            }
            // return redirect()->route('index-bl')->with('success', 'Successfully edit BL - '.$bl->bl_no);
        } catch (\Exception $e) {
            Session::flash("msg", $e->getMessage());
            Session::flash("class", "alert-danger");
            return redirect::back();
        }
    }

    public function validate_voyage($bl_id, $voy_id)
    {
        try {
            $bl = BillLading::findOrFail($bl_id);
            $voy = Voyage::find($voy_id);

            $pol_bool = false;
            $pod_bool = false;

            if ($voy->vessel->default_shipment != $bl->vessel_type) {
                return response()->json(['success' => false, 'msg' => 'The selected voyage does not have the same shipment type.']);
            }

            foreach ($voy->destinations as $dest) {
                if ($dest->port == $bl->pol_id) {
                    if (!$pol_bool) {
                        $pol_bool = true;
                    }
                }

                if ($dest->port == $bl->pod_id) {
                    if ($pol_bool) {
                        $pod_bool = true;
                    } else {
                        //Incorrect sequence
                        return response()->json(['success' => false, 'msg' => 'The selected voyage does not satisfy the port\'s rotation.']);
                    }
                }
            }

            if ($pol_bool == true && $pod_bool == true) {
                return response()->json(['success' => true, 'voy_id' => $voy->id, 'ves_id' => $voy->vessel->id]);
            } else {
                $msg = "The selected voyage does not have the following port(s): <br>";
                $msgArr = [];
                if (!$pol_bool) {
                    array_push($msgArr, $bl->pol->code);
                }

                if (!$pod_bool) {
                    array_push($msgArr, $bl->pod->code);
                }

                $msg .= implode(", ", $msgArr);
                return response()->json(['success' => false, 'msg' => $msg]);
            }
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd($id);
    }

    public function set_format_bl(Request $request)
    {
        $bl_format = $request->txt_blformat;

        //plain or form format
        $format_type = $request->format_type;

        $test_decode = json_decode($request->bl_id);

        if (json_last_error() === JSON_ERROR_NONE) {
            $request->bl_id = $test_decode;
        }

        $blArr = BillLading::where('id', $request->bl_id)->get();

        if (empty($request->chk_bl)) {
            $request->chk_bl = [$request->bl_id];
        }

        $bl_id = json_encode($request->chk_bl);

        $company_name1 = $request->txt_companyname1;
        $company_name2 = $request->txt_companyname2;

        $format = "";

        $from = "BL";

        if ($request->query("id")) {
            $from = Hashids::encode($request->query("id"));
        }

        switch ($bl_format) {
            case "apollo":
                $format = "print.apollo-pdf";
                $page_num_height = 530;
                break;

            case "malsuria":
                $page_num_height = 560;
                if (!empty($company_name1) || !empty($company_name2)) {
                    $format = "print.shinyang-pdf";
                } else {
                    // $format = "print.malsuria-pdf";
                    if (env("SITE") == "TEGAS") {
                        $format = "print.tegasshinyang-pdf";
                    } else {
                        $format = "print.shinyang-pdf";
                    }
                }
                break;
        }

        $files = [];

        foreach ($blArr as $bl) {
            $filename = "/bl_export/BL_" . $bl->bl_no . '.pdf';

            // Testing purpose
            // $html = view($format, compact("bl", "company_name1", "company_name2", "from", "format_type"))->render();
            //return @\PDF::loadHTML($html, 'utf-8')->stream();

            $pdf = \PDF::loadView($format, compact("bl", "company_name1", "company_name2", "from", "format_type"));
            $pdf->output();

            //Add page number
            $dompdf = $pdf->getDomPDF();
            $canvas = $dompdf->get_canvas();

            // $canvas->page_text(500, $page_num_height, "Page {PAGE_NUM} of {PAGE_COUNT}", '', 9, array(0, 0, 0));

            $pdf = $pdf->save("../public" . $filename);
            $bl->filename = $filename;

            array_push($files, $filename);
        }

        //Merge all pdf into one if more than one BL
        $merged_filename = null;

        //Merge all pdf into one if more than one BL
        if (count($files) > 1) {
            $pdf_merge = new \LynX39\LaraPdfMerger\PdfManage();
            foreach ($files as $file) {
                $pdf_merge->addPDF("../public" . $file, 'all');
            }

            $merged_filename = str_random(10);

            $pdf_merge->merge('file', env("BL_PATH") . $merged_filename . ".pdf");
        }

        return view('bl.print-bl', compact("bl_format", "company_name1", "company_name2", "bl_id", "blArr", "from", "merged_filename"));

        // return view($format, compact("blArr", "company_name1", "company_name2", "from"));
    }

    public function print_bl($hash)
    {
        $id = Hashids::decode($hash);
        if (!empty($id)) {
            $bl = BillLading::find($id[0]);
            return view("bl.print-bl", compact("bl"));
        }
    }

    public function print_so($hash, Request $request)
    {
        $id = Hashids::decode($hash);
        if (!empty($id)) {
            $from = "";
            if ($request->query("bl")) {
                $from = "BL";
            } elseif ($request->query("id")) {
                $from = Hashids::encode($request->query("id"));
            }
            $blArr = BillLading::where('id', $id[0])->get();

            $format_type = $request->format_type;

            foreach ($blArr as $bl) {
                $pdf = \PDF::loadView("print.so-pdf", compact("bl", "format_type"));

                $filename = "/so_export/SO_" . $bl->bl_no . '.pdf';

                $pdf->save("../public" . $filename);
                $bl->filename = $filename;
            }

            $bl_id = json_encode([$id[0]]);

            $merged_filename = null;

            return view("bl.print-so", compact("blArr", "from", "bl_id", "merged_filename"));
        }
    }

    public function shortcut_list()
    {
        $shortcuts = ShortcutKey::get();
        return response()->json($shortcuts->keyBy('shortcut_code'));
    }

    public function get_address($id)
    {
        $address = CompanyAddress::find($id);
        if (!empty($address)) {
            return response()->json(['success' => true, 'address' => $address]);
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function get_voyages(Request $request)
    {
        $voyages = Voyage::where('vessel_id', $request->id)->get();

        if ($voyages->isNotEmpty()) {
            return response()->json(["success" => true, 'result' => $voyages]);
        } else {
            return response()->json(["success" => false]);
        }
    }

    public function get_bls(Request $request)
    {

        $ladings = BillLading::query();

        if ($request->vessel_id != "ALL") {
            if ($request->voyage_id == 'NULL') {
                $ladings = BillLading::where('vessel_id', $request->vessel_id);
            } else {
                $ladings = BillLading::where('vessel_id', $request->vessel_id)->where('voyage_id', $request->voyage_id);
            }
        }

        if ($request->pol_id != "ALL") {
            $ladings = $ladings->where("pol_id", $request->pol_id);
        }

        if ($request->pod_id != "ALL") {
            $ladings = $ladings->where("pod_id", $request->pod_id);
        }

        if ($request->shipper_id != "ALL") {
            $ladings = $ladings->where("shipper_id", $request->shipper_id);
        }

        $ladings = $ladings->orderBy("created_at", "ASC")->get();

        if ($ladings->isNotEmpty()) {
            foreach ($ladings as $lading) {

                $lading->vessel_name = $lading->getVessel->name;
                $lading->voyage_name = $lading->getVoyage->voyage_id;

                $lading->pol = $lading->pol;
                $lading->pod = $lading->pod;

                if ($lading->cust_s_name) {
                    $lading->shipper_name = $lading->cust_s_name;
                } else {
                    $lading->shipper_name = $lading->shipper->name;
                }

                if ($lading->cust_c_name) {
                    $lading->consignee_name = $lading->cust_c_name;
                } else {
                    $lading->consignee_name = $lading->consignee->name;
                }

                if ($lading->telex_status == 0) {
                    $lading->status = '<span class="text-danger">No</span>';
                } else {
                    $lading->status = '<span class="text-green">Yes</span>';
                }

                $lading->hashID = Hashids::encode($lading->id);
            }

            return response()->json(["success" => true, 'result' => $ladings]);
        } else {
            return response()->json(["success" => false]);
        }
    }

    public function company_address(Request $request)
    {
        $company = Company::find($request->id);

        if ($company) {
            return response()->json(["success" => true, 'result' => $company->getAddresses]);
        } else {
            return response()->json(["success" => false]);
        }
    }

    public function part_bl(Request $request)
    {
        $bl = BillLading::find($request->id);

        try {
            $newBl = $bl->replicate();
            $bl_no = $this->blno_suffix($bl->bl_no);

            $newBl->bc_no = null;
            $newBl->bl_no = $bl_no;
            $newBl->save();

            $cargos = BillCargo::where('bl_id', $bl->id)->get();

            foreach ($cargos as $cargo) {
                $newCargo = $cargo->replicate();

                $newCargo->bl_id = $newBl->id;
                $newCargo->bl_no = $newBl->bl_no;
                $newCargo->save();

                foreach ($cargo->containers as $container) {
                    $newContainer = $container->replicate();

                    $newContainer->cargo_id = $newCargo->id;
                    $newContainer->save();
                }
            }

            return response()->json(["success" => true]);
        } catch (Exception $e) {
            return response()->json(["success" => false]);
        }
    }

    public function vessel_details(Request $request)
    {
        $exist = Vessel::find($request->id);

        if ($exist) {
            $vessel = array();
            $vessel['name'] = $exist->name;
            $vessel['type'] = $exist->type;
            $vessel['dwt'] = $exist->dwt;

            if ($exist->semicontainer_service == 0) {
                $vessel['semicontainer'] = '<span class="text-danger">No</span>';
            } else {
                $vessel['semicontainer'] = '<span class="text-green">Yes</span>';
            }

            $vessel['manu_year'] = $exist->manufactured_year;
            $vessel['flag'] = $exist->flag;
            $vessel['default_shipment'] = $exist->default_shipment;

            return response()->json(['result' => true, 'vessel' => $vessel]);
        } else {
            return response()->json(['result' => false]);
        }
    }

    public function cargo($hash)
    {
        try {
            $id = Hashids::decode($hash);
            $bl = BillLading::find($id[0]);
            $bl->hash = $hash;
            $cargos = BillCargo::where('bl_no', $bl->bl_no)->get();
            $f_cargo = BillCargo::where('bl_no', $bl->bl_no)->first();

            return view('bl.index-cargo', compact('hash', 'cargos', 'f_cargo', 'bl'));
        } catch (\Exception $ex) {
            return redirect::action("BillLadingController@index");
        }
    }

    public function create_cargo($hash)
    {
        $id = Hashids::decode($hash);
        $bl = BillLading::find($id[0]);
        $vessel = Vessel::select('default_shipment')->where('id', $bl->vessel_id)->first();
        $vessel = $vessel->default_shipment;

        return view('bl.create-cargo', compact('hash', 'bl', 'vessel'));
    }

    public function store_cargo(Request $request, $hash)
    {
        DB::beginTransaction();
        try {
            $cargo = new BillCargo;

            $cargo->bl_id = $request->bl_id;
            $cargo->bl_no = $request->bl_no;
            $cargo->cargo_qty = $request->cargo_qty;
            $cargo->cargo_packing = strtoupper($request->cargo_packing);

            $cargo_name = $cargo->cargo_qty . " " . $cargo->cargo_packing;

            if (trim($cargo_name) != "") {
                $cargo->cargo_name = strtoupper($cargo_name);
            } else {
                $cargo->cargo_name = $request->cargo_name;
            }

            if (empty($cargo->qty)) {
                $sum = 0;
                $str = $cargo->cargo_name;
                $strArr = explode("&", $str);
                foreach ($strArr as $ea) {
                    $numArr = explode("x", $ea);
                    $sum += (int) $numArr[0];
                }
                $cargo->cargo_qty = $sum;
            }

            $cargo->markings = $request->markings;
            $cargo->temperature = $request->temperature;
            $cargo->cargo_nature = !empty($request->uncode) ? "Dangerous" : "General";
            $cargo->uncode = $request->uncode;
            $cargo->cargo_desc = $request->cargo_desc;
            $cargo->cargo_type = $request->cargo_type;
            $cargo->weight = $request->weight;
            $cargo->weight_unit = $request->weight_unit;
            $cargo->volume = $request->volume;
            $cargo->detailed_desc = nl2br($request->detailed_desc);
            $cargo->remarks = $request->remarks;
            $cargo->raw_qty = $request->raw_qty;
            $cargo->save();

            if (!empty($request->container_no)) {
                for ($i = 0; $i < count($request->container_no); $i++) {
                    $cargo_detail = new CargoDetail;

                    $cargo_detail->cargo_id = $cargo->id;
                    $cargo_detail->cgo = 1;
                    $cargo_detail->container_no = $request->container_no[$i];
                    $cargo_detail->seal_no = $request->seal_no[$i];
                    $cargo_detail->size = $request->container_size[$i];
                    $cargo_detail->type = $request->container_type[$i];
                    $cargo_detail->save();
                }
            }

            DB::commit();

            return redirect()->route('index-cargo', ['hash' => $hash])->with('success', 'Successfully added cargo.');
        } catch (\Exception $e) {
            DB::rollBack();
            Session::flash("msg", "Unable to save cargo. " . $e->getMessage());
            Session::flash("class", "alert-danger");
            return redirect::back();
        }
    }

    public function edit_cargo($hashBL, $hashCG)
    {
        $cargo_id = Hashids::decode($hashCG);
        $bl_id = Hashids::decode($hashBL);

        $cargo = BillCargo::find($cargo_id[0]);

        $containers = $cargo->containers;

        $grouped = $containers->groupBy(function ($item, $key) {
            return $item['size'] . $item['type'];
        });

        $bl = BillLading::find($bl_id[0]);

        //Should get vessel type saved in bill lading
        $vessel = $bl->vessel_type;

        return view('bl.edit-cargo', compact('cargo', 'hashBL', 'hashCG', 'vessel', 'grouped', 'bl'));
    }

    public function update_cargo(Request $request, $hashBL, $hashCG)
    {
        DB::beginTransaction();
        try {
            $id = Hashids::decode($hashCG);
            $cargo = BillCargo::find($id[0]);

            $cargo->cargo_qty = $request->cargo_qty;
            $cargo->cargo_packing = strtoupper($request->cargo_packing);

            if ($cargo->cargo_qty && $cargo->cargo_packing) {
                $cargo->cargo_name = $cargo->cargo_qty . ' ' . $cargo->cargo_packing;
            } else {
                $cargo->cargo_name = $request->cargo_name;
            }

            if (empty($cargo->qty)) {
                $sum = 0;
                $str = $cargo->cargo_name;
                $strArr = explode("&", $str);
                foreach ($strArr as $ea) {
                    $numArr = explode("x", $ea);
                    $sum += (int) $numArr[0];
                }
                $cargo->cargo_qty = $sum;
            }

            $cargo->markings = $request->markings;
            $cargo->temperature = $request->temperature;
            $cargo->uncode = $request->uncode;

            $cargo->cargo_nature = !empty($request->uncode) ? "Dangerous" : "General";

            $cargo->cargo_desc = $request->cargo_desc;
            $cargo->cargo_type = $request->cargo_type;
            $cargo->weight = $request->weight;
            $cargo->weight_unit = $request->weight_unit;
            $cargo->volume = $request->volume;
            $cargo->detailed_desc = nl2br($request->detailed_desc);

            $cargo->remarks = $request->remarks;
            $cargo->raw_qty = $request->raw_qty;
            $cargo->save();

            $count = CargoDetail::where('cargo_id', $cargo->id)->count();

            if (!empty($request->container_no)) {
                //Store all adding & updating cargo detail ID, to be used to remove other cargos that is not in this array
                $valid_cargo_detail_arr = [];

                for ($i = 0; $i < count($request->container_no); $i++) {

                    if ($request->cargo_detail_id[$i] == 0) {
                        $cargo_detail = new CargoDetail;
                        $cargo_detail->cargo_id = $cargo->id;
                        $cargo_detail->cgo = 1;
                        $cargo_detail->container_no = $request->container_no[$i];
                        $cargo_detail->seal_no = $request->seal_no[$i];
                        $cargo_detail->size = $request->container_size[$i];
                        $cargo_detail->type = $request->container_type[$i];
                        $cargo_detail->save();

                        array_push($valid_cargo_detail_arr, $cargo_detail->id);
                    } else {
                        DB::table('cargo_details')
                            ->where('id', $request->cargo_detail_id[$i])
                            ->update(['container_no' => $request->container_no[$i], 'seal_no' => $request->seal_no[$i]]);

                        array_push($valid_cargo_detail_arr, $request->cargo_detail_id[$i]);
                    }

                    // $cargo_det = CargoDetail::updateOrCreate(
                    //     [
                    //         'cargo_id' => $cargo->id,
                    //         'size' => $request->container_size[$i],
                    //         'type' => $request->container_type[$i],
                    //         'container_no' =>$request->container_no[$i],
                    //     ],
                    //     ['container_no' => 99]
                    // );

                    // $cargo_detail = new CargoDetail;

                    // $cargo_detail->cargo_id = $cargo->id;
                    // $cargo_detail->cgo = 1;
                    // $cargo_detail->container_no = $request->container_no[$i];
                    // $cargo_detail->seal_no = $request->seal_no[$i];
                    // $cargo_detail->size = $request->container_size[$i];
                    // $cargo_detail->type = $request->container_type[$i];
                    // $cargo_detail->save();
                }

                //Remove cargo details which is not in the array of IDs
                CargoDetail::where("cargo_id", $cargo->id)->whereNotIn("id", $valid_cargo_detail_arr)->delete();
            } else {
                //No cargos, assume all is removed
                DB::table('cargo_details')->where('cargo_id', $cargo->id)->delete();
            }

            DB::commit();

            return redirect()->route('index-cargo', ['hash' => $hashBL])->with('success', 'Successfully edited cargo.');
        } catch (\Exception $e) {
            DB::rollBack();
            Session::flash("msg", "Unable to update cargo. " . $e->getMessage());
            Session::flash("class", "alert-danger");
            // return redirect::action("BillLadingController@edit_cargo", [$hashBL, $hashCG]);
        }
    }

    public function delete_cargo($id)
    {
        DB::transaction(function () use ($id) {
            $cargo = BillCargo::find($id);
            if (!empty($cargo)) {

                $cargo_charges = ChargeCargo::where("cargo_id", $id)->delete();

                $cargo->delete();
                Session::flash("msg", "Successfully deleted cargo - " . $cargo->cargo_name . " " . $cargo->cargo_desc);
                Session::flash("class", "alert-success");
            } else {
                Session::flash("msg", "Delete cargo failed. Please try again.");
                Session::flash("class", "alert-danger");
            }
        });
        return redirect::back();
    }

    public function charges(Request $request, $hash)
    {
        $id = Hashids::decode($hash);
        $bl = BillLading::find($id[0]);
        $cgs = Charge::orderBy('code', 'ASC')->get();

        $bill_charges = BillCharge::where('bl_no', $id[0])->get();

        return view('bl.index-charges', compact('hash', 'bill_charges', 'cgs', 'bl'));
    }

    public function store_charges(Request $request, $hash)
    {
        $bl_id = Hashids::decode($hash);

        DB::beginTransaction();
        try {
            $chargeArr = json_decode($request->txt_charge);

            $count = 0;

            foreach ($chargeArr as $key => $chg) {

                // $exist = BillCharge::where("bl_no", $bl_id[0])
                // ->where("charge_code", $request->ddl_chargecode)
                // ->where("charge_unit", $chg->type)
                // ->first();

                // if(empty($exist)){
                $charge = new BillCharge;
                $charge->bl_no = $bl_id[0];
                $charge->charge_code = $request->ddl_chargecode;
                $charge->charge_unit = $chg->type;
                $charge->charge_rate = $chg->price;
                $charge->charge_payment = $request->ddl_payment;
                $charge->save();

                // Check if any cargo selected for this bill lading charge
                $multiselect = "ddl_cargo";
                $multiselect .= ($key > 0) ?? "1";
                $cargos = $request->$multiselect;

                if (!empty($cargos) > 0) {
                    foreach ($cargos as $cg) {
                        $cc = new ChargeCargo;
                        $cc->blcharge_id = $charge->id;
                        $cc->cargo_id = $cg;
                        $cc->save();
                    }
                }

                // } else {
                // $count++;
                // }
            }

            DB::commit();

            if ($count == 0) {
                Session::flash("msg", "Successfully saved charges.");
                Session::flash("class", "alert-success");
            } else {
                Session::flash("msg", "Duplicate charge(s) cannot added.");
                Session::flash("class", "alert-danger");
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Session::flash("msg", "Failed to save charges." . $e->getMessage());
            Session::flash("class", "alert-danger");
        }

        return redirect::back();
        // DB::beginTransaction();
        // try {
        //     $ids = explode(',', $request->charge_id);
        //     if($request->count > 1) {
        //         $codes = explode(',', $request->charge_code);
        //         $units = explode(',', $request->charge_unit);
        //         $rates = explode(',', $request->charge_rate);
        //         $payments = explode(',', $request->charge_payment);

        //         for($i = 0; $i < count($ids); $i++) {
        //             if($ids[$i] == -1) {
        //                 $charge = new BillCharge;

        //                 $charge->bl_no = $bl_id[0];
        //                 $charge->charge_code = $codes[$i];
        //                 $charge->charge_unit = $units[$i];
        //                 $charge->charge_rate = $rates[$i];
        //                 $charge->charge_payment = $payments[$i];
        //                 $charge->save();
        //             }
        //         }
        //     } else {
        //         if($request->charge_id == -1) {
        //             $charge = new BillCharge;

        //             $charge->bl_no = $bl_id[0];
        //             $charge->charge_code = $request->charge_code;
        //             $charge->charge_unit = $request->charge_unit;
        //             $charge->charge_rate = $request->charge_rate;
        //             $charge->charge_payment = $request->charge_payment;
        //             $charge->save();
        //         }
        //     }

        //     DB::commit();

        //     Session::flash("msg", "Successfully saved charge.");
        //     Session::flash("class", "alert-success");
        // } catch(\Exception $e) {
        //     DB::rollBack();
        //     Session::flash("msg", "Failed to save charges." . $e->getMessage());
        //     Session::flash("class", "alert-danger");
        // }

        // return redirect::back();
    }

    public function edit_charges(Request $request)
    {
        try {
            $charge = BillCharge::find($request->edit_charge_id);
            $charge->charge_rate = $request->txt_edit_amount;
            $charge->save();

            Session::flash("msg", "Successfully edited charge.");
            Session::flash("class", "alert-success");
        } catch (\Exception $e) {
            Session::flash("msg", "Failed to edit charge. " . $e->getMessage());
            Session::flash("class", "alert-danger");
        }

        return redirect::back();
    }

    public function delete_charges($id)
    {
        try {

            // Check for charge cargo
            ChargeCargo::where("blcharge_id", $id)->delete();

            $charge = BillCharge::find($id);
            $charge->delete();

            // Session::flash("msg", "Successfully deleted charge.");
            // Session::flash("class", "alert-success");
            return response()->json(['success' => true]);
        } catch (Exception $e) {
            // Session::flash("msg", "Failed to delete charge. Please try again.");
            // Session::flash("class", "alert-danger");
            return response()->json(['success' => false]);
        }

        // return redirect::back();
    }

    public function redirect_format($p_bl_id, $p_format, $p_cname1 = null, $p_cname2 = null)
    {
        $bl_id = $p_bl_id;
        $format = $p_format;
        $cname1 = $p_cname1;
        $cname2 = $p_cname2;
        return view("print.bl-redirect", compact("bl_id", "format", "cname1", "cname2"));
    }

    public function blno_suffix($bl_no)
    {
        $lastChar = substr($bl_no, -1, 1);

        // ASCII 48 = 0, ASCII 57 = 9
        if (ord($lastChar) >= 48 && ord($lastChar) <= 57) {
            // ASCII 65 = 'A'
            $alphabet = chr(65);
        } else {
            // ASCII 90 = 'Z'
            if ((ord($lastChar) + 1) > 90) {
                $alphabet = chr(65);
            } else {
                $alphabet = chr(ord($lastChar) + 1);
                $bl_no = substr($bl_no, 0, -1);
            }
        }

        $bl_no .= $alphabet;

        $exist = BillLading::where('bl_no', $bl_no)->first();

        if (!empty($exist)) {
            return $this->blno_suffix($bl_no);
        } else {
            return $bl_no;
        }
    }
}
