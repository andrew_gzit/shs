<?php

namespace App\Http\Controllers;

use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Illuminate\Support\Facades\Mail;

use \App\Company;
use \App\Port;
use \App\Voyage;
use \App\Vessel;
use \App\Booking;
use \App\BillLading;
use \App\BillNumber;
use \App\VoyageDestinations;
use \App\BookingAmendment;
use \App\Reservation;
use \App\Shipper;
use \App\Restriction;
use \App\Mail\BookingConfirmation_LCL;
use \App\Mail\BookingConfirmation_FCL;
use \App\ViewModel\VesselListBox;

class BookingController extends Controller
{
	public function index(Request $request){

		$query_vessel = $request->query("vessel", null);
		$query_voyage = $request->query("voyage", null);

		// $bookings = Booking::where("eta_fpd", ">=", Carbon::now());

		$bookings = DB::select("SELECT * FROM bookings WHERE deleted_at IS NULL AND voyage_id IN (SELECT voyage_id AS id FROM voyage_destinations WHERE atd >= CURDATE() GROUP BY voyage_id)");

		//Shows bookings where current date is before voyage's last port ETA / ATD.
		$bookings1 = Booking::hydrate(DB::select("SELECT * FROM bookings WHERE deleted_at IS NULL AND voyage_id IN (SELECT voyage_id AS id FROM voyage_destinations WHERE etd >= CURDATE() OR atd >= CURDATE() GROUP BY voyage_id) ORDER BY id DESC"));

		// dd($bookings1);

		// $query_bool = false;

		if(!empty($query_vessel)){
            // Query vessel name
            $vesselName = Vessel::where('id', $query_vessel)->get(['name'])->pluck('name');

			//Show all bookings if there is filter
			$bookings1 = Booking::hydrate(DB::select("SELECT * FROM bookings WHERE deleted_at IS NULL ORDER BY id DESC"));

            $voyage_in_vessel = Voyage::where("vessel_id", $query_vessel)->get(['id'])->pluck('id');
            $bookingsMerge1 = $bookings1->whereIn("voyage_id", $voyage_in_vessel);
            $bookingsMerge2 = $bookings1->whereIn('mvessel', $vesselName);
            $bookingsMerge3 = $bookings1->where('mvessel', $query_vessel);

            $bookings1 = $bookingsMerge1->merge($bookingsMerge2)->merge($bookingsMerge3);

			$query_bool = true;
		}

		if(!empty($query_voyage)){
			$bookings1 = $bookings1->where("voyage_id", $query_voyage);
			$query_bool = true;
		}

		// //Bookings that has been transferred to BL will not show in filtered list
		// if($query_bool){
		// 	$bookings1 = $bookings1->orderBy("status", "ASC")->latest()->get();
		// } else {
		// 	$bookings1 = $bookings1->latest()->get();
		// }

		// $bookings = Booking::latest()->get();
		$deleted = Booking::onlyTrashed()->where("status", 3)->orderBy("deleted_at", "DESC")->where("eta_pol", ">=", Carbon::now())->get();

		//For filter section
		$voyages = Voyage::get();
        $vessels = Vessel::orderBy('name')->get();
        $mvessels = Booking::whereNotNull('mvessel')->distinct('mvessel')->orderBy('mvessel')->pluck('mvessel');

        $vessels = $vessels->map(function($vessel) {
            $v = new VesselListBox();
            $v->id = $vessel->id;
            $v->name = $vessel->name;

            return $v;
        });

        $mvessels = $mvessels->map(function($mvessel) {
            $mv = new VesselListBox();
            $mv->id = $mvessel;
            $mv->name = $mvessel;

            return $mv;
        });

        $vessels = $vessels->merge($mvessels)->unique('name');
        $sorted = $vessels->sortBy('name');
        $vessels = $sorted->values()->all();

		return view("booking.index-booking", compact("bookings", "deleted", "voyages", "vessels", "mvessels", "query_vessel", "query_voyage", "bookings1"));
	}

	public function email_booking($bk_id, Request $request){
		try {
			$bk = Booking::find($bk_id);

			$filename = 'bc_' . $bk->booking_no . '.pdf';
			$path = '/booking_confirmation/' . $filename;

			$bk_party = Shipper::where("company_id", $bk->bookingparty_id)->first();

			$bk->pic = $bk_party->pic;

			$base_company = Company::where("type", 4)->first();

            if (env('SITE') === "TEGAS") {
                if($bk->shipment_type == "LCL"){
                    // Mail::to("andrews.kkg@gmail.com")->send(new BookingConfirmation_LCL($bk));
                    $pdf = \PDF::loadView('emails.bc_lcl', compact("bk", "base_company"));
                } else {
                    // Mail::to("andrews.kkg@gmail.com")->send(new BookingConfirmation_FCL($bk));
                    $pdf = \PDF::loadView('emails.tegas-bc_fcl', compact("bk", "base_company"));
                }
            } else {
                if($bk->shipment_type == "LCL"){
                    // Mail::to("andrews.kkg@gmail.com")->send(new BookingConfirmation_LCL($bk));
                    $pdf = \PDF::loadView('emails.bc_lcl', compact("bk", "base_company"));
                } else {
                    // Mail::to("andrews.kkg@gmail.com")->send(new BookingConfirmation_FCL($bk));
                    $pdf = \PDF::loadView('emails.bc_fcl', compact("bk", "base_company"));
                }
            }


			$pdf->save('../public' . $path);

			return response()->json(['success' => true, 'path' => $path]);
		} catch (\Exception $e) {
			return response()->json(['success' => false, 'msg' => $e->getMessage()]);
		}
	}

	public function check_transfer_bl($bk_id){
		$bk = Booking::find($bk_id);
		if($bk->weight > 0 && $bk->cargo_desc){
			return response()->json(['success' => true]);
		} else {
			return response()->json(['success' => false, 'msg' => 'Cargo has not been filled yet!']);
		}
	}

	public function create(Request $request){
		try {
			$query = $request->query("voyage", null);

			$voy = null;
			$fpd = null;

			if(!empty($query)){
				$voy = Voyage::find($query);
				if(empty($voy)){
					return redirect::action("BookingController@create");
				}
			}

			$res = $request->query("reservation", null);
			$resv = "";
			if(!empty($res)){
				$resv = Reservation::find($res);
				if(!empty($resv)){
					$resv->shipper = $resv->shipper_id;
					$resv->date_text = $resv->date->format('d/m/Y');
				}
			}

			$companies = Company::where("type", Company::type_shipper)->orWhere("type", Company::type_shipperconsignee)->get();

			$shippers = collect();

			foreach($companies AS $comp){
				$comp_type = $comp->getCompanyOfType();
				$comp_type->comp_id = $comp->id;
				$comp_type->name = $comp->name;
				$shippers->push($comp_type);
			}

			$shippers = $shippers->sortBy('code');

			$ports = Port::get();
			$voyages = Voyage::where("booking_status", 0)->where("eta_pol", ">=", Carbon::now()->format('Y-m-d 00:00:00'))->get();
			$company_info = Company::where("type", 4)->first();
			$vessels = Vessel::get();

			return view("booking.create-booking", compact("shippers", "ports", "voyages", "vessels", "company_info", "query", "res", "resv", "voy", "fpd"));
		} catch (\Exception $e) {
			dd($e->getMessage());
			return redirect::action("BookingController@index");
		}
	}

	public function check_restriction($company_id, $voyage_id){
		try {
			$comp = Company::findOrFail($company_id);
			$shipper_id = $comp->getCompanyOfType()->id;
			$restrictions = Restriction::where("shipper_id", $shipper_id)->get()->groupBy('type');

			//Retrieve selected voyage
			$voyage = Voyage::findOrFail($voyage_id);
			$vessel = $voyage->vessel;
			$vessel->age = date('Y') - $vessel->manufactured_year;

			//Array to store violated restrictions
			$errArr = [];

			//Check vessel with company's restrictions
			if(isset($restrictions['name'])){
				if($restrictions['name']->contains('value', $vessel->name)){
					array_push($errArr, "Vessel: " . $vessel->name);
				}
			}

			if(isset($restrictions['type'])){
				if($restrictions['type']->contains('value', $vessel->type)){
					array_push($errArr, "Type: " . strtoupper($vessel->type));
				}
			}

			if(isset($restrictions['age'])){
				if($vessel->age > (int)$restrictions['age'][0]->value){
					array_push($errArr, "Age: " . $vessel->age);
				}
			}

			if(isset($restrictions['flag'])){
				if($restrictions['flag']->contains('value', $vessel->flag)){
					array_push($errArr, "Flag: " . strtoupper($vessel->flag));
				}
			}

			return response()->json(['success' => true, 'restrictions' => $restrictions, 'err' => $errArr]);
		} catch (\Exception $e) {
			return response()->json(['success' => false, 'msg' => $e->getMessage()]);
		}
	}

	public function store(Request $request){
		DB::beginTransaction();
		try {
			$booking = new Booking;
			$booking->date = Carbon::createFromFormat('d/m/Y', $request->txt_date)->format('Y-m-d 00:00:00');

			$voy = Voyage::find($request->voyage_id);


			$booking->booking_no = $this->getNewBookingNumber($request->pol, $request->fpd, $voy->vessel->id, $request->shipment_type);
			$booking->bookingparty_id = $request->bookingparty_id;
			$booking->shipper_id = $request->shipper_id;

			$booking->status = 0;

			$booking->pol_id = $request->pol;
			$booking->eta_pol = $voy->eta_pol;

			$booking->pod_id = $request->pod;
			$booking->eta_pod = $voy->getPort($request->pod)->eta;


			//If FPD is not same as POD, means there is transhipment (mother vessel and ETA FPD required)
			$booking->fpd_id = $request->fpd;
			if($booking->fpd_id != $booking->pod_id){
				$booking->eta_fpd = Carbon::createFromFormat('d/m/Y', $request->eta_fpd)->format('Y-m-d 00:00:00');
				$booking->mvessel = $request->mvessel;
			} else {
				$booking->eta_fpd = $booking->eta_pod;
			}

			$booking->voyage_id = $request->voyage_id;
			$booking->shipment_type = $request->shipment_type;

			$booking->cargo_nature = "General";

			//Set CY cutoff if MYPEN (NBCT)
			if($booking->pol_id == $booking->NBCT()){
				$dest = VoyageDestinations::where("voyage_id", $voy->id)->where("port", $booking->pol_id)->first();
				$booking->cy_cutoff = $dest->eta;
			}

			if($request->txt_resv != null){
				$resv = Reservation::find($request->txt_resv);

				if(!empty($resv)){
					$booking->weight = $resv->getRemainWeight();
					$booking->res_id = $resv->id;
					$booking->save();
				}

			} else {
				$booking->save();
			}

			DB::commit();

			switch($request->txt_button){
				case "save":
				Session::flash("msg", "Successfully created new booking: " . $booking->booking_no);
				Session::flash("class", "alert-success");
				return redirect::action("BookingController@index");
				break;

				case "cargo":
				return redirect::action("BookingController@create_cargo", Hashids::encode($booking->id));
				break;

				case "confirm":
				$booking->confirm = 1;
				$booking->save();

				//Send BC to user
				// Mail::to("andrews.kkg@gmail.com")->send(new BookingConfirmation_FCL($booking));

				return redirect::action("BookingController@create_cargo", Hashids::encode($booking->id));
				break;
			}
		} catch (\Exception $e) {
			DB::rollBack();
			Session::flash("msg", "Unable to save booking. " . $e->getMessage());
			Session::flash("class", "alert-danger");
			return redirect::back();
		}
	}

	public function edit($bk_no){
		try {
			$id = Hashids::decode($bk_no);
			$bk = Booking::find($id[0]);
			$vessels = Vessel::get();
			if(!empty($bk)){
				$companies = Company::where("type", Company::type_shipper)->orWhere("type", Company::type_shipperconsignee)->get();

				$shippers = collect();

				foreach($companies AS $comp){
					$comp_type = $comp->getCompanyOfType();
					$comp_type->comp_id = $comp->id;
					$comp_type->name = $comp->name;
					$shippers->push($comp_type);
				}

				$shippers = $shippers->sortBy('code');

				$ports = Port::get();
				$voyages = Voyage::where("booking_status", 0)->where("eta_pol", ">=", Carbon::now()->format('Y-m-d 00:00:00'))->get();
				$company_info = Company::where("type", 4)->first();
				return view("booking.edit-booking", compact("bk", "shippers", "ports", "voyages", "company_info", "vessels"));
			} else {
				return redirect::back();
			}
		} catch (\Exception $e) {
			Session::flash("msg", "Unable to edit selected booking. " . $e->getMessage());
			Session::flash("class", "alert-danger");
			return redirect::action("BookingController@index");
		}
	}

	public function update($id, Request $request){
		try {
			$bk = Booking::find($id);
			if(!empty($bk)){
				$bk->date = Carbon::createFromFormat('d/m/Y', $request->txt_date)->format('Y-m-d 00:00:00');
				$bk->shipper_id = $request->shipper_id;
				$bk->bookingparty_id = $request->bookingparty_id;

				$voy = Voyage::find($request->voyage_id);

				$bk->pol_id = $request->pol;
				$bk->eta_pol = $voy->eta_pol;

				$bk->pod_id = $request->pod;
				$bk->eta_pod = $voy->getPort($request->pod)->eta;


				//If FPD is not same as POD, means there is transhipment (mother vessel and ETA FPD required)
				$bk->fpd_id = $request->fpd;
				if($bk->fpd_id != $bk->pod_id){
					$bk->eta_fpd = Carbon::createFromFormat('d/m/Y', $request->eta_fpd)->format('Y-m-d 00:00:00');
					$bk->mvessel = $request->mvessel;
				} else {
					$bk->eta_fpd = $bk->eta_pod;
				}

				$bk->voyage_id = $request->voyage_id;
				$bk->shipment_type = $request->shipment_type;

				//Bool to check if booking has been clone;
				$clone_bk = false;
				$new_bk = null;

				if($bk->isDirty('pol_id') || $bk->isDirty('fpd_id') || $bk->isDirty('pod_id') || $bk->isDirty('shipment_type')){
					//Clone old booking into new one, as booking number used cannot be changed, therefore cloning the booking row and rename the booking number
					$new_bk = $bk->replicate();

					//Retrieve new voyage to create new booking number
					$voy = Voyage::find($bk->voyage_id);
					$new_bk->booking_no = $this->getNewBookingNumber($request->pol, $request->fpd, $voy->vessel->id, $request->shipment_type);
					$new_bk->cargo_packing = null;
					$new_bk->status = 0;
					$new_bk->save();

					$clone_bk = true;
				}

				if($bk->confirm == 1){
					$uid = str_random(5);
					foreach($bk::updateArr AS $key => $ea){
						if($bk->isDirty($ea)){
							$amend = new BookingAmendment();
							$amend->booking_id = $bk->id;
							$amend->revision = $uid;
							$amend->type = $bk::updateArrText[$key];
							if(starts_with($ea, 'eta')){
								$amend->old_value = (!empty($bk->getOriginal($ea))) ? Carbon::parse($bk->getOriginal($ea))->format("d/m/Y") : "";
								$amend->value = $bk->$ea->format("d/m/Y");
							} else if($ea == "pol_id" || $ea == "pod_id" || $ea == "fpd_id") {
								$old_port = Port::find($bk->getOriginal($ea));
								$port = Port::find($bk->$ea);
								$amend->old_value = (!empty($bk->getOriginal($ea))) ? $old_port->code : "";
								$amend->value = $port->code;
							} else if($ea == "shipper_id" || $ea == "bookingparty_id") {
								$old_company = Company::find($bk->getOriginal($ea));
								$new_company = Company::find($bk->$ea);
								$amend->old_value = $old_company->getCompanyOfType()->code;
								$amend->value = $new_company->getCompanyOfType()->code;
							} else {
								$amend->old_value = (!empty($bk->getOriginal($ea))) ? $bk->getOriginal($ea) : "";
								$amend->value = $bk->$ea;
							}
							$amend->save();
						}
					}
				}

				//If booking has been cloned, replace $bk to $new_bk, and remove old booking
				if($clone_bk){
					//Update booking amendments from old booking to new booking
					DB::table('booking_amendments')
					->where('booking_id', $bk->id)
					->update(['booking_id' => $new_bk->id]);

					$bk->delete();
					$bk = $new_bk;
				} else {
					$bk->save();
				}

				switch($request->txt_button){
					case "save":
					Session::flash("msg", "Successfully updated booking: " . $bk->booking_no);
					Session::flash("class", "alert-success");
					return redirect::action("BookingController@index");
					break;

					case "cargo":
					return redirect::action("BookingController@create_cargo", Hashids::encode($bk->id));
					break;
				}
			}
		} catch (\Exception $e) {
			Session::flash("msg", "Update booking unsuccessful. " . $e->getMessage());
			Session::flash("class", "alert-danger");
			return redirect::back();
		}

	}

	public function delete($id){
		$bk = Booking::find($id);
		if(!empty($bk)){
			$bk->status = $bk::status_cancelled;
			$bk->save();

			VoyageController::checkCargo($bk->getVoyage->id);

			$bk->delete();

			Session::flash("msg", "Successfully cancelled booking - " . $bk->booking_no);
			Session::flash("class", "alert-success");
		} else {
			Session::flash("msg", "Unable to cancel selected booking. Please try again.");
			Session::flash("class", "alert-danger");
		}
		return redirect::back();
	}

	public function recover_bk($bk_no){
		try {
			$bk_num = Hashids::decode($bk_no);
			$bk = Booking::onlyTrashed()->find($bk_num[0]);
			$bk->deleted_at = null;
			$bk->status = 0;
			$bk->save();

			Session::flash("msg", "Successfully recovered booking - " . $bk->booking_no);
			Session::flash("class", "alert-success");
		} catch (\Exception $e) {
			Session::flash("msg", "Recover booking unsuccessful. " . $e->getMessage());
			Session::flash("class", "alert-danger");
		}
		return redirect::back();
	}

	public function get_booking($bk_id){
		$bk = Booking::find($bk_id);
		if(!empty($bk)){
			$bk->bk_date = $bk->date->format("d/m/Y");
			$bk->bk_bookingparty = $bk->getBookingParty->code;
			$bk->bk_shipper = $bk->getShipper->code;
			$bk->bk_voyage = $bk->getVoyage->voyage_id;
			$bk->bk_vessel = $bk->getVoyage->vessel->name;

			$bk->pol = $bk->getPol->code;
			$bk->fpd = $bk->getFpd->code;

			$bk->etapol = $bk->eta_pol->format("d/m/Y");
			$bk->etafpd = $bk->eta_fpd->format("d/m/Y");

			if($bk->cont_ownership != ""){
				if($bk->cont_ownership == 0){
					$bk->contownership = "SOC";
				} else {
					$bk->contownership = "COC";
				}
			}

			if($bk->fpd_id != $bk->pod_id){
				$bk->pod = $bk->getPod->code;

				$voy_dest = VoyageDestinations::where("voyage_id", $bk->voyage_id)
				->where("port", $bk->pod_id)
				->first();

				$bk->etapod = $voy_dest->eta->format("d/m/Y");
			}


			$bk->freight = ($bk->freight_term == "P") ? "Prepaid" : "Collect";

			$amendments = BookingAmendment::where("booking_id", $bk->id)->get();
			$amendments = $amendments->groupBy("revision")->toArray();
			return response()->json(['success' => true, 'booking' => $bk, 'amendments' => $amendments]);
		} else {
			return response()->json(['success' => false]);
		}
	}

	public function get_vessel_shipment($voy_id, $ves_id = "null"){
		if($voy_id != "null"){
			$voy = Voyage::find($voy_id);
			$vessel = Vessel::find($voy->vessel_id);
		} else {
			$vessel = Vessel::find($ves_id);
		}

		$set = null;
		if($vessel->type == "General Cargo Vessel"){
			if($vessel->semicontainer_service == 1){
				$shipment_type = "BOTH";
				$set = $vessel->default_shipment;
			} else {
				$shipment_type = "LCL";
			}
		} else {
			$shipment_type = "FCL";
		}

		return response()->json([$shipment_type, $set]);
	}

	public function get_destinations($voy_id){
		$dest = VoyageDestinations::where("voyage_id", $voy_id)->join("ports", "ports.id", "voyage_destinations.port")->select("ports.code", "ports.name", "ports.id", "ports.location", "voyage_destinations.etd")->orderBy('eta', 'ASC')->get();
		$voy = Voyage::find($voy_id);
		$pol = $voy->pol_id;
		if(!empty($dest)){
			return response()->json(['success' => true, 'dest' => $dest, 'pol' => $pol]);
		} else {
			return response()->json(['success' => false]);
		}
	}

	public function getNewBookingNumber($pol, $pod, $vessel, $shipment_type){
		$port_load = Port::find($pol);
		$port_discharge = Port::find($pod);

		$vessel = Vessel::find($vessel);

		$short_year = substr(Carbon::now()->format('y'), -1, 1);

		if($vessel->type == "General Cargo Vessel"){
			//<POL><POD><1 for LCL / 2 for FCL><4-digit number running for all BL><single digit year>
			$search = $port_load->bl_prefix . $port_discharge->bl_prefix;

			if($shipment_type == "LCL"){
				$shipment_num = "1";
			} else {
				$shipment_num = "2";
			}

			$billnumber = BillNumber::where("vessel_default", "General Cargo Vessel")->first();
			$billnumber->current_number = $billnumber->current_number+1;

			if($billnumber->current_number > 9999){
				$billnumber->current_number = 1;
			}

			$billnumber->save();

			$new_booking_no = str_pad($billnumber->current_number, 4, 0, STR_PAD_LEFT);

			// //NOT LIKE to prevent searching for Container Vessel's booking
			// $booking = Booking::where("booking_no", "LIKE", "%____" . $short_year)->where("booking_no", "NOT LIKE", "SYMT%")->withTrashed()->latest()->first();

			// //Check for BL number too, choose whichever is higher
			// $bl = BillLading::where("bl_no", "REGEXP", '.[0-9]{5}' .  $short_year . '$')->withTrashed()->orderBy("bl_no", "DESC")->first();

			// if(!empty($bl)){
			// 	$check_bl = substr($bl->bl_no, -5, 4);

			// 	if(!empty($booking)){
			// 		$check_bk = substr($booking->booking_no, -5, 4);

			// 		/**
			// 		 * If BL number is higher, use BL to get next number, else use booking number
			// 		 */
			// 		if($check_bl > $check_bk){
			// 			$new_booking_no = (int)$check_bl+1;
			// 		} else {
			// 			$new_booking_no = (int)$check_bk+1;
			// 		}
			// 	}
			// }

			// dd($new_booking_no);

			// if(empty($new_booking_no)){
			// 	$new_booking_no = "0001";
			// }

			// if(!empty($booking)){
			// 	$booking_no = substr($booking->booking_no, -5, 4);
			// 	$new_booking_no = (int)$booking_no+1;
			// } else {
			// 	$new_booking_no = "0001";
			// }

			// if($new_booking_no < 9999){
			$new_booking_no = $search . $shipment_num . str_pad($new_booking_no, 4, 0, STR_PAD_LEFT) . $short_year;
			// } else {
				// $new_booking_no = $search . $shipment_num . str_pad(1, 4, 0, STR_PAD_LEFT) . $short_year;

				// $billnumber->current_number = 1;
				// $billnumber->save();
			// }
		} else {
			//Container Vessel - SYTM<5-digit running number reset after used up>
			$search = "SYTM";

			$booking = Booking::where("booking_no", "LIKE", $search . "%")->withTrashed()->latest()->first();

			if(!empty($booking)){
				$booking_no = substr($booking->booking_no, strlen($search));
				$new_booking_no = (int)$booking_no+1;
			} else {
				$new_booking_no = "00001";
			}

			if($new_booking_no < 99999){
				$new_booking_no = $search . str_pad($new_booking_no, 5, 0, STR_PAD_LEFT);
			} else {
				$new_booking_no = $search . str_pad(1, 5, 0, STR_PAD_LEFT);
			}
		}

		return $new_booking_no;
	}

	public function create_cargo($hash){
		$id = Hashids::decode($hash);
		$booking = Booking::find($id[0]);
		if(!empty($booking)){
			return view("booking.create-cargo", compact("booking", "hash"));
		} else {
			Session::flash("msg", "Booking not found!");
			Session::flash("class", "alert-danger");
			return redirect::back();
		}
	}

	public function store_cargo($bk_no, Request $request){
		$booking = Booking::where("booking_no", $bk_no)->first();

		try {
			$voy = Voyage::find($booking->voyage_id);

			$old_weight = $booking->weight;

			$booking->weight = $request->weight;
			$booking->save();

			if($voy->totalUsedWeight() > $voy->vessel->dwt){
				Session::flash("msg", "Saving cargo unsuccessful. Entered weight has exceeded allowable DWT.");
				Session::flash("class", "alert-danger");

				$booking->weight = $old_weight;
				$booking->save();

				return redirect::action("BookingController@create_cargo", Hashids::encode($booking->id));
			}

			if($booking->shipment_type == "FCL"){
				if($request->haulge_request_date){
					$booking->haulge_request_date = Carbon::createFromFormat('d/m/Y', $request->haulge_request_date)->format('Y-m-d H:i:s');
				}

                $booking->yard = $request->yard;
				$booking->empty_delivery_loc = $request->empty_delivery_location;

				if($request->cy_cutoff){
					$booking->cy_cutoff = Carbon::createFromFormat('d/m/Y', $request->cy_cutoff)->format('Y-m-d H:i:s');
				}
				$booking->cargo_qty = $request->raw_qty;
				$booking->cargo_packing = $request->cargo_name;
			} else {
				$booking->cargo_qty = $request->cargo_qty;
				$booking->cargo_packing = $request->cargo_packing;
			}

			$booking->cargo_desc = $request->cargo_desc;
			$booking->temperature = $request->temperature;
			$booking->remarks = $request->remarks;
			$booking->uncode = $request->uncode;
			$booking->cargo_nature = (!empty($request->uncode)) ? 'Dangerous' : 'General';
			$booking->save();

			VoyageController::checkCargo($voy->id);

			if($request->txt_button == "confirm"){
				$booking->confirm = 1;
				$booking->save();
			} else if($request->txt_button == "transfer") {
				Session::flash("msg", "Successfully saved cargo details for " . $booking->booking_no);
				Session::flash("class", "alert-success");
				return redirect::action('BillLadingController@create', ['booking' => $booking->id]);
			}

			Session::flash("msg", "Successfully saved cargo details for " . $booking->booking_no);
			Session::flash("class", "alert-success");
			return redirect::action("BookingController@index");
		} catch (\Exception  $e) {
			Session::flash("msg", $e->getMessage());
			Session::flash("class", "alert-danger");
			return redirect::back();
		}
	}
}
