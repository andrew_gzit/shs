<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Spatie\PdfToText\Pdf;

use App\Voyage;
use App\Vessel;
use App\InwardBL;
use App\Shipper;
use App\Company;
use App\Mail\InwardBillLading;
use App\Port;

use Session;
use Redirect;
use DB;

class InwardBLController extends Controller
{
	public function create(){
		$vessels = Vessel::get();
		$shippers = Shipper::get();
		$consignees = Company::where("type", Company::type_consignee)->orWhere("type", Company::type_shipperconsignee)->get();

		return view("bl.create-inwardbl", compact("vessels", "shippers", "consignees"));
	}

	public function voyage_list($vessel_name){
		try {
			$vessel = Vessel::where("name", $vessel_name)->first();

			if(!empty($vessel)){
				$voyages = Voyage::where("vessel_id", $vessel->id)
				->where(function($query){
					$query->where("atd_pol", ">=", Carbon::now()->format("Y-m-d 00:00:00"))
					->orWhereNull("atd_pol");
				})
				->get();

				return response()->json(['success' => true, 'voyages' => $voyages]);
			} else {
				return response()->json(['success' => false]);
			}
		} catch (\Exception $e) {
			return response()->json(['success' => false]);
		}
	}

	public function get_ports(){
		$ports = Port::get();
		$html = "<option disabled='' selected='' value=''>None</option>";

		foreach ($ports AS $port) {
			$html .= "<option value='" . $port->id . "' data-name='" . $port->name . "' data-location='" . strtoupper($port->location) . "'>" . $port->code . "</option>";
		}

		return response()->json($html);
	}

	public function store(Request $request){
		try {
			$bl = new InwardBL();
			$bl->vessel = $request->txt_vessel;
			$bl->shipment_type = '';
			$bl->voyage = $request->txt_voyage;
			$bl->bl_no = $request->txt_blno;

			$bl->pol_id = $request->pol_id;
			$bl->pol_name = $request->txt_polname;
			$bl->pod_id = $request->pod_id;
			$bl->pod_name = $request->txt_podname;
			$bl->fpd_id = $request->fpd_id;
			$bl->fpd_name = $request->txt_fpdname;

			$bl->shipper_name = $request->txt_shippername;
			$bl->shipper_add = $request->txt_shipperadd;

			$bl->consignee_name = $request->txt_consigneename;
			$bl->consignee_add = $request->txt_consigneeadd;

			$bl->notify_name = $request->txt_notifyname;
			$bl->notify_add = $request->txt_notifyadd;

			$bl->no_bl = $request->txt_no_of_bl;

			$bl->no_packages = $request->txt_no_packages;
			$bl->containers = $request->txt_containers;
			$bl->cargo_desc = $request->txt_cargodesc;
			$bl->detailed_desc = $request->txt_detailedesc;
			$bl->weight = $request->txt_weight;
			$bl->weight_unit = $request->txt_weightunit;
			$bl->volume = $request->txt_volume;

			$bl->save();

			Session::flash("inward", true);
			Session::flash("msg", "Successfully created inward BL - " . $bl->bl_no);
			Session::flash("class", "alert-success");
			return redirect::action('BillLadingController@index');
		} catch (\Exception $e){

			return redirect::back();
		}
	}

	public function edit($hashed){
		$id = Hashids::decode($hashed);
		try {
			$bl = InwardBL::findOrFail($id[0]);

			$vessels = Vessel::get();
			$shippers = Shipper::get();
			$consignees = Company::where("type", Company::type_consignee)->orWhere("type", Company::type_shipperconsignee)->get();

			return view("bl.edit-inwardbl", compact("bl", "vessels", "shippers", "consignees"));
		} catch (\Exception $e) {
			Session::flash("inward", true);
			Session::flash("msg", "Unable to edit inward BL. " . $e->getMessage());
			Session::flash("class", "alert-danger");
			return redirect::action('BillLadingController@index');
		}
	}

	public function update($hashed, Request $request){
		$id = Hashids::decode($hashed);
		try {

			$bl = InwardBL::findOrFail($id[0]);
			$bl->vessel = $request->txt_vessel;
			$bl->voyage = $request->txt_voyage;
			$bl->bl_no = $request->txt_blno;

			$bl->pol_id = $request->pol_id;
			$bl->pol_name = $request->txt_polname;
			$bl->pod_id = $request->pod_id;
			$bl->pod_name = $request->txt_podname;
			$bl->fpd_id = $request->fpd_id;
			$bl->fpd_name = $request->txt_fpdname;

			$bl->shipper_name = $request->txt_shippername;
			$bl->shipper_add = $request->txt_shipperadd;

			$bl->consignee_name = $request->txt_consigneename;
			$bl->consignee_add = $request->txt_consigneeadd;

			$bl->notify_name = $request->txt_notifyname;
			$bl->notify_add = $request->txt_notifyadd;

			$bl->no_bl = $request->txt_no_of_bl;

			$bl->no_packages = $request->txt_no_packages;
			$bl->containers = $request->txt_containers;
			$bl->cargo_desc = $request->txt_cargodesc;
			$bl->detailed_desc = $request->txt_detailedesc;
			$bl->weight = $request->txt_weight;
			$bl->weight_unit = $request->txt_weightunit;
			$bl->volume = $request->txt_volume;

			$bl->save();

			Session::flash("inward", true);
			Session::flash("msg", "Successfully edited Inward BL - " . $bl->bl_no);
			Session::flash("class", "alert-success");
			return redirect::action("BillLadingController@index");

		} catch (\Exception $e) {

			Session::flash("msg", "Unable to save changes. " . $e->getMessage());
			Session::flash("class", "alert-danger");
			return redirect::back();
		}
	}

	public function delete(Request $request){
		$inw = $request->chk_inw;
		try {
			DB::beginTransaction();
			$bl = InwardBL::whereIn("id", $inw)->delete();
			DB::commit();

			Session::flash("inward", true);
			Session::flash("msg", "Successfully deleted selected inward BL(s)");
			Session::flash("class", "alert-success");
			return redirect::action('BillLadingController@index');
		} catch (\Exception $e) {
			DB::rollBack();
			Session::flash("inward", true);
			Session::flash("msg", "Unable to delete inward BL. " . $e->getMessage());
			Session::flash("class", "alert-danger");
			return redirect::back();
		}
	}

	public function inw_bl_check($bl_no){
		$inw = InwardBL::where("bl_no", $bl_no)->first();
		if(!empty($inw)){
			return response()->json(['success' => false]);
		} else {
			return response()->json(['success' => true]);
		}
	}

	public function email_consignee($id, Request $request){
		try {
			$bl = InwardBL::findOrFail($id);
			$bl->date_now = strtoupper(Carbon::now()->format("d-M-Y"));
			$base_company = Company::where("type", Company::type_basecompany)->first();
			$bl->bc_name = $base_company->name;
			$bl->bc_add = $base_company->getAddresses->first()->address;
			$bl->bc_telephone = $base_company->telephone;
			$bl->bc_fax = $base_company->fax;
			$bl->bc_email = $base_company->getEmails->first()->email;
			$bl->bc_portcode = $base_company->port_code;
			$bl->bc_bacode = $base_company->ba_code;
			$bl->ref = $request->ref;
			$bl->email = $request->email;

			if($bl->consignee_name == "TO ORDER"){
				$bl->to = $bl->notify_name;
			} else {
				$bl->to = $bl->consignee_name;
			}

			Mail::to($bl->email)->send(new InwardBillLading($bl));
			return response()->json(['success' => true]);
		} catch (\Exception $e) {
			return response()->json(['success' => false, 'msg' => $e->getMessage()]);
		}
	}

	public function noa_pdf($id, Request $request)
	{
		try {

			$bl = InwardBL::findOrFail($id);

			$bl->date_now = strtoupper(Carbon::now()->format("d.m.y"));
			$base_company = Company::where("type", Company::type_basecompany)->first();
			$bl->bc_name = $base_company->name;
			$bl->bc_roc_no = $base_company->roc_no;
			$bl->bc_add = $base_company->getAddresses->first()->address;
			$bl->bc_telephone = $base_company->telephone;
			$bl->bc_fax = $base_company->fax;
			$bl->bc_email = $base_company->getEmails->first()->email;
			$bl->bc_portcode = $base_company->port_code;
			$bl->bc_bacode = $base_company->ba_code;
			$bl->ref = $request->ref;
			$bl->email = $request->email;

			if($bl->consignee_name == "TO ORDER"){
				$bl->to = $bl->notify_name;
			} else {
				$bl->to = $bl->consignee_name;
			}

			$filename = 'NOA_' . $bl->bl_no . '.pdf';
			$path = '/noa/' . $filename;

			$pdf = \PDF::loadView('emails.noa', compact("bl"));

			$pdf->save('../public' . $path);

			$file = '../public' . $path;

			$headers = [
				'Content-Type' => 'application/pdf',
			];

			return response()->download($file, $filename, $headers);
		} catch (Exception $e) {
			dd($e->getMessage());
		}
	}

	public function upload_edi(Request $request){
		try {
			$file = $request->file('file_edi');
			$text = Pdf::getText($file->getPathName(), 'pdftotext');

			$inw_bl = explode("\f", $text);

			$parsed_bl = [];

			foreach($inw_bl AS $key => $ea){
				$str_arr = explode("\r\n", $ea);

				$inward = new InwardBL();

				$company_count = 0;
				$company_arr = [];
				$container_extra = false;
				$cur_page = 1;

				foreach($str_arr AS $str){
					if($str != ""){
						if($company_count < 3){
							$company_count++;
							array_push($company_arr, $str);
							continue;
						}

						if(empty($inward->page)){
							preg_match_all('!\d+!', $str, $matches);
							$inward->page = $matches[0][0];
							$cur_page = $inward->page;
							$inward->total_page = $matches[0][1];
							continue;
						}

						// if($inward->total_page > 1 && $inward->page != 1){
						// 	$inward = $parsed_bl[$key-1];
						// 	$inward->page = $cur_page;
						// }

						//Get vessel and port of loading
						if(empty($inward->vessel)){
							//Split on first space
							$temp_pol = trim(substr($str, strpos($str, " ")));
							if($this->checkPort($temp_pol)){
								$inward->pol_name = $temp_pol;
								$inward->vessel = trim(substr($str, 0, strpos($str, " ")));
								continue;
							} else {
								$inward->vessel = $str;
								continue;
							}
						}

						if(empty($inward->voyage)){
							$temp_pod = trim(substr($str, strpos($str, " ")));
							if($this->checkPort($temp_pod)){
								$inward->pod_name = $temp_pod;
								$inward->voyage = trim(substr($str, 0, strpos($str, " ")));
								continue;
							} else {
								//Make string into array, check each part for valid port
								$temp_pod_arr = explode(" ", $str);
								$valid = false;
								foreach($temp_pod_arr AS $pod_str){
									if($this->checkPort($pod_str) && !$valid){
										$inward->pod_name = trim(substr($str, strpos($str, $pod_str)));
										$inward->voyage = trim(substr($str, 0, strpos($str, $pod_str)));
										$valid = true;
									}
								}

								if(empty($inward->voyage)){
									$inward->voyage = $str;
								}

								continue;
							}
						}

						if(empty($inward->pol_name)){
							$inward->pol_name = $str;
							continue;
						}

						//If has zero, probably is BL number
						if(str_is('*0*', $str) && empty($inward->bl_no) && strlen($str) <= 11){
							$inward->bl_no = $str;
							continue;
						}

						if(empty($inward->freight_payable)){
							$inward->freight_payable = $str;
							continue;
						}

						if(empty($inward->pod_name)){
							$inward->pod_name = $str;
							continue;
						}

						if(str_is('*(3)*', $str) && empty($inward->no_bl)){
							$inward->no_bl = $str;
							continue;
						}


						if($str == "MT" || $str == "KG"){
							$inward->weight_unit = $str;
							continue;
						}

						if($str == "M3"){
							$inward->volume_unit = $str;
							continue;
						}

						if(empty($inward->containers)){
						//Second page might not have container, should skip to detailed description
							if($inward->page == $inward->total_page && $inward->page != 1){
								$inward->containers = ".";
								$inward->detailed_desc .= $str;
							} else {
								$inward->containers = $str;
							}
							continue;
						}

						if(!str_contains($str, "(S)")){
							if(str_contains($str, "STC")){
								$inward->detailed_desc .= $str;
								continue;
							} else if(!$container_extra && !is_numeric($str) && $inward->page == 1 && $str != $inward->pol_name && !str_is('*/*/*', $str)) {
								//Make sure string is not date & place of issue
								$inward->containers .= "<br>" . $str;
								continue;
							}
						}

						if(empty($inward->no_packages)){
						//No of packages and detailed_desc might join together, have to check
							if(strpos($str, "STC") == false){
								$container_extra = true;
								$inward->no_packages = $str;
							} else {
								$container_extra = true;
								$inward->no_packages = trim(substr($str, 0, strpos($str, "STC")));
								$inward->detailed_desc .= trim(substr($str, strpos($str, "STC")));
							}
							continue;
						}

						if((empty($inward->weight) || empty($iwnard->volume)) && is_numeric($str)){
							$split = explode(".", $str);

							if(isset($split[1])){
								if(strlen($split[1]) == 3){
									$inward->weight = $str;
									continue;
								} else {
									$inward->volume = $str;
									continue;
								}
							}
						}

						if(empty($inward->remarks) && !floatval($str) && !str_contains($str, "TOTAL NO. OF PACKAGES") && $str != $inward->pol_name && str_is('*FREIGHT PAYABLE*', $str)){
							$inward->remarks = $str;
							continue;
						}

						if(empty($inward->detailed_desc)){
							$inward->detailed_desc .= $str;
							continue;
						}

						if(!str_contains($str, "(S)") && empty($inward->total_packages)){
							$inward->detailed_desc .= "\n" . $str;
							continue;
						} else if(empty($inward->total_packages)) {
							$inward->total_packages = $str;
							continue;
						}

						if(str_is('*0*', $str) && empty($inward->bl_no) && strlen($str) <= 11){
							$inward->bl_no = $str;
							continue;
						}

						if(empty($inward->no_bl)){
							$inward->no_bl = $str;
							continue;
						}

						if(empty($inward->weight_unit)){
							$inward->weight_unit = $str;
							continue;
						}

						if(empty($inward->weight)){
							$inward->weight = $str;
							continue;
						}

						if(empty($inward->volume)){
							$inward->volume = $str;
							continue;
						}

						if(str_contains($str, "TOTAL NO. OF PACKAGES") && empty($inward->total_in_text)){
							$inward->total_in_text = $str;
							continue;
						}

						if(empty($inward->place_of_issue)){
							$inward->place_of_issue = $str;
							continue;
						}

						if(empty($inward->date_of_issue)){
							$inward->date_of_issue = $str;
							continue;
						}
					}
				}

				$shipper = $this->splitByText($company_arr[0]);
				$inward->shipper_name = $shipper[0];
				$inward->shipper_add = $shipper[1];

				$consignee = $this->splitByText($company_arr[1]);
				$inward->consignee_name = $consignee[0];
				$inward->consignee_add = $consignee[1];

				$notify = $this->splitByText($company_arr[2]);
				$inward->notify_name = $notify[0];
				$inward->notify_add = $notify[1];

				// array_push($parsed_bl, $inward);

				if($inward->total_page > 1 && $inward->page != 1){
					$parsed_bl[$inward->bl_no . "_" . $inward->page] = $inward;
				} else {
					$parsed_bl[$inward->bl_no] = $inward;
				}
			}

			//Process BL with more than 1 page
			foreach($parsed_bl AS $key => $ea){
				if($ea->total_page > 1 && $ea->page != 1){
					//Retrieve ori 1st page bl and remove this current one from array
					$ori_bl = $parsed_bl[$ea->bl_no];
					$ori_bl->detailed_desc .= $ea->detailed_desc;
					$ori_bl->remarks .= $ea->remarks;
					unset($parsed_bl[$key]);
				}
			}

			if($request->save == "true"){


				//Remove bl that is not selected
				if(count($request->chk_edi) != count($parsed_bl)){
					$chk_col = collect($request->chk_edi);

					foreach($parsed_bl AS $key => $p){
						if(!$chk_col->contains($key)){
							unset($parsed_bl[$key]);
						}
					}
				}

				DB::beginTransaction();
				foreach($parsed_bl AS $parsed){
					$inw = new InwardBL();
					$inw->vessel = $parsed->vessel;
					$inw->voyage = $parsed->voyage;
					$inw->bl_no = $parsed->bl_no;
					$inw->pol_name = $parsed->pol_name;
					$inw->pod_name = $parsed->pod_name;
					$inw->fpd_name = $parsed->pod_name;
					$inw->shipper_name = $parsed->shipper_name;
					$inw->shipper_add = $parsed->shipper_add;
					$inw->consignee_name = $parsed->consignee_name;
					$inw->consignee_add = $parsed->shipper_add;
					$inw->notify_name = $parsed->notify_name;
					$inw->notify_add = $parsed->notify_add;
					$inw->no_bl = preg_replace("/[^0-9]/", "", $parsed->no_bl);
					$inw->no_packages = $parsed->no_packages;
					$inw->containers = $parsed->containers;
					$inw->detailed_desc = $parsed->detailed_desc;
					$inw->weight = $parsed->weight;
					$inw->weight_unit = $parsed->weight_unit;
					$inw->volume = $parsed->volume;
					$inw->remarks = $parsed->remarks;
					$inw->date_of_issue = $parsed->date_of_issue;
					$inw->save();
				}
				DB::commit();
				Session::flash("msg", "Successfully added " . count($parsed_bl) . " inward BL.");
				Session::flash("class", "alert-success");
				Session::flash("inward", true);
				return redirect::action("BillLadingController@index");
			}

			return response()->json(["success" => true, "data" => $parsed_bl]);
		} catch (\Exception $e) {
			DB::rollBack();
			return response()->json(["success" => false, "data" => $e->getMessage()]);
		}
	}

	public function splitByText($string){
		$text_arr = ["*-*)", "BHD.", "BHD", "BERHAD", "ORDER", "CONSIGNEE"];

		$company_name = ".";
		$company_add = ".";

		foreach($text_arr AS $split_text){
			if($split_text == "*-*)"){
				if(preg_match("/\(\S+\-\D\)/", $string)){
					//Search for (xxxx-x) SSM number
					preg_match("/\(\S+\-\D\)/", $string, $matches, PREG_OFFSET_CAPTURE);

					$position = $matches[0][1];

					$company_name = substr($string, 0, $position + strlen($matches[0][0]));
					$company_add = trim(substr($string, $position + strlen($matches[0][0])));
					break;
				}
			}

			if(strpos($string, $split_text) != false){
				$company_name = substr($string, 0, strpos($string, $split_text) + strlen($split_text));
				$company_add = trim(substr($string, strpos($string, $split_text) + strlen($split_text)));
				break;
			}
		}

		return [$company_name, $company_add];
	}

	public function checkPort($string) {
		$port = Port::where("location", "LIKE", "%" . $string . "%")->first();
		if(!empty($port)){
			return true;
		} else {
			return false;
		}
	}


}
