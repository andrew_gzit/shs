<?php

namespace App\Http\Controllers;

use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Http\Request;
use Session;
use Redirect;
use DB;
use Carbon\Carbon;

// Form Request Validations
use App\Http\Requests\StorePort;
use App\Http\Requests\StoreVessel;
use App\Http\Requests\StoreCharge;
use App\Http\Requests\StoreCompany;
use App\Http\Requests\StoreBaseCompany;

use App\Http\Requests\EditPort;
use App\Http\Requests\EditVessel;
use App\Http\Requests\EditCharge;
use App\Http\Requests\EditCompany;

// Eloquent Models
use App\Voyage;
use App\Vessel;
use App\Port;
use App\Company;
use App\Shipper;
use App\Consignee;
use App\Agent;
use App\CompanyAddress;
use App\CompanyEmail;
use App\Charge;
use App\ChargeDetail;
use App\DefaultCharge;
use App\Restriction;
use App\VoyageDestinations;
use App\ShortcutKey;
use App\BillLading;
use App\Reservation;
use App\ChargeVessel;

class MaintenanceController extends Controller
{
    /**
     * COMPANY CONTROLLERS
     */
    public function index_basecompany()
    {
    	$company = Company::where("type", 4)->first();
    	return view("maintenance.company.create-basecompany", compact("company"));
    }

    public function store_basecompany(StoreBaseCompany $request)
    {
    	$company = Company::updateOrCreate(
    		['type' => 4],
    		[
    			'name' => $request->txt_name, 'roc_no' => $request->txt_roc,
    			'port_code' => $request->txt_portcode, 'ba_code' => $request->txt_bacode,
    			'telephone' => $request->txt_tel, 'fax' => $request->txt_fax
    		]
    	);

    	$addresses = json_decode(html_entity_decode($request->txt_addressArr));

    	$id = $company->id;
    	$del_query = CompanyAddress::where("company_id", $id)->where(function($query) use(&$addresses, $id){
    		foreach($addresses AS $key => $ea){
                //Remove address that is not in the $addresses array under the selected company_id
    			$adr = CompanyAddress::where("company_id", $id)
    			->where("address", $ea)
    			->first();

    			if(!empty($detail)){
    				$query->where("id", "!=", $adr->id);
                            //Remove element from array so it will not be added into DB
    				unset($addresses[$key]);
    			}
    		}
    	})->delete();

    	foreach($addresses AS $ea){
    		$add = new CompanyAddress();
    		$add->company_id = $company->id;
    		$add->address = $ea;
    		$add->save();
    	}

    	$emails = json_decode($request->txt_emailArr);
    	$del_email = CompanyEmail::where("company_id", $id)->where(function($query) use(&$emails, $id){
    		foreach($emails AS $key => $ea){
    			$email = CompanyEmail::where("company_id", $id)
    			->where("email", $ea)
    			->first();

    			if(!empty($detail)){
    				$query->where("id", "!=", $email->id);
    				unset($emails[$key]);
    			}
    		}
    	})->delete();
    	foreach($emails AS $ea){
    		$email = new CompanyEmail();
    		$email->company_id = $company->id;
    		$email->email = $ea;
    		$email->save();
    	}

    	return redirect::back()
    	->with("class", "alert-green")
    	->with("msg", "Successfully saved base company details");
    }

    public function index_company(Request $request)
    {
    	$filter_type = $request->query('company_type', null);
    	if($filter_type != null){
    		if($filter_type == 2){
    			$companies = Company::where("type", "!=", Company::type_basecompany)->where("type", "!=", Company::type_agent)->orderBy("name", "ASC")->get();
    		} else {
    			$companies = Company::where("type", $filter_type)->orderBy("name", "ASC")->get();
    		}

    	} else {
    		$companies = Company::where("type", "!=", 4)->orderBy("name", "ASC")->get();
    	}
    	return view("maintenance.company.index-company", compact("companies", "filter_type"));
    }

    public function create_company(Request $request)
    {
    	$cid = $request->query('cid');
    	$company = null;

    	if(!empty($cid)){
    		$company_id = Hashids::decode($cid);
    		if($company_id){
    			$company = Company::find($company_id[0]);
    		}
    	}

    	$companies = Company::where("type", 0)->orWhere("type", 2)->orderBy("name", "ASC")->get();
    	$ports = Port::get();
    	$vessels = Vessel::get();
    	return view("maintenance.company.create-company", compact("ports", "companies", "company", "vessels"));
    }

    public function store_restriction($id, $vesselname, $vesseltype, $age, $flag, $store_type){
    	if($store_type == "update"){
    		$del_query = Restriction::where("shipper_id", $id)->where("type", "name")->where(function($query) use(&$vesselname, $id){
    			foreach($vesselname AS $key => $ea){
    				$name = Restriction::where("shipper_id", $id)
    				->where("type", "name")
    				->where("value", $ea)
    				->first();

    				if(!empty($name)){
    					$query->where("id", "!=", $name->id);
    					unset($vesselname[$key]);
    				}
    			}
    		})->delete();

    		$del_query1 = Restriction::where("shipper_id", $id)->where("type", "flag")->where(function($query) use(&$flag, $id){
    			foreach($flag AS $key => $ea){
    				$name = Restriction::where("shipper_id", $id)
    				->where("type", "flag")
    				->where("value", $ea)
    				->first();

    				if(!empty($name)){
    					$query->where("id", "!=", $name->id);
    					unset($flag[$key]);
    				}
    			}
    		})->delete();
    	}

    	foreach($vesselname AS $name){
    		if($name != "0"){
    			$res = new Restriction();
    			$res->shipper_id = $id;
    			$res->type = "name";
    			$res->value = $name;
    			$res->save();
    		}
    	}

    	foreach($flag AS $ea){
    		if($ea != "0"){
    			$res = new Restriction();
    			$res->shipper_id = $id;
    			$res->type = "flag";
    			$res->value = $ea;
    			$res->save();
    		}
    	}

    	Restriction::updateOrCreate(
    		['shipper_id' => $id, 'type' => 'type'],
    		[
    			'value' => $vesseltype
    		]
    	);

    	$age = (int)$age;

    	if($age < 0){
    		$age = 0;
    	}

    	if($age == 0) {
    		Restriction::where("shipper_id", $id)->where("type", "age")->delete();
    	} else {
    		Restriction::updateOrCreate(
    			['shipper_id' => $id, 'type' => 'age'],
    			[
    				'value' => $age
    			]
    		);
    	}
    }

    public function getUpdateType($radio_update){
    	$voy_update = 0;
    	$del_update = 0;

    	if(!empty($radio_update)){
    		if(count($radio_update) == 2){
    			$voy_update = 1;
    			$del_update = 1;
    		} else {
    			if($radio_update[0] == 1){
    				$voy_update = 1;
    			} else {
    				$del_update = 1;
    			}
    		}
    	}

    	return [$voy_update, $del_update];
    }

    public function store_company(StoreCompany $request)
    {
    	$company = DB::transaction(function () use ($request) {

    		$company = new Company();
    		$company->name = $request->txt_name;
    		$company->roc_no = $request->txt_roc;

        //If company_type is array, it should be shipper & consignee (type_shipperconsignee)
    		$company->type = (count($request->radio_company_type) == 1) ? $request->radio_company_type[0] : 2;
    		$company->save();

    		$addresses = json_decode(html_entity_decode($request->txt_addressArr));

    		foreach($addresses AS $ea){

    			$needle = "</strong><br>";

    			$needle_index = strpos($ea, $needle);

    			if($needle_index > 0){
    				$desc = strip_tags(substr($ea, 0, $needle_index + strlen($needle)));
    				$ea = substr($ea, strpos($ea, $needle) + strlen($needle));
    			}

    			$add = new CompanyAddress();
    			$add->company_id = $company->id;
    			$add->address = $ea;
    			$add->description = !empty($desc) ? $desc : null;
    			$add->save();
    		}

    		$emails = json_decode($request->txt_emailArr);
    		foreach($emails AS $ea){
    			$email = new CompanyEmail();
    			$email->company_id = $company->id;
    			$email->email = $ea;
    			$email->save();
    		}

    		switch ($company->type) {
    			case Company::type_shipper:
    			$shipper = new Shipper();
    			$shipper->company_id = $company->id;
    			$shipper->code = $request->txt_code;
    			$shipper->telephone = $request->txt_tel;
    			$shipper->fax = $request->txt_fax;
    			$shipper->pic = $request->txt_pic;

    			$updateArr = $this->getUpdateType($request->radio_update);

    			$shipper->voyage_updates = $updateArr[0];
    			$shipper->delivery_updates = $updateArr[1];

    			$shipper->handling_method = $request->txt_handlingmethod;
    			$shipper->save();
    			break;

    			case Company::type_consignee:
    			$consignee = new Consignee();
    			$consignee->company_id = $company->id;
    			$consignee->code = $request->txt_code;
    			$consignee->telephone = $request->txt_tel;
    			$consignee->fax = $request->txt_fax;
    			$consignee->pic = $request->txt_pic;
    			$consignee->save();
    			break;

    			case Company::type_shipperconsignee:
    			$shipper = new Shipper();
    			$shipper->company_id = $company->id;
    			$shipper->code = $request->txt_code;
    			$shipper->telephone = $request->txt_tel;
    			$shipper->fax = $request->txt_fax;
    			$shipper->pic = $request->txt_pic;

    			$updateArr = $this->getUpdateType($request->radio_update);

    			$shipper->voyage_updates = $updateArr[0];
    			$shipper->delivery_updates = $updateArr[1];

    			$shipper->handling_method = $request->txt_handlingmethod;
    			$shipper->save();

    			$consignee = new Consignee();
    			$consignee->company_id = $company->id;
    			$consignee->code = $request->txt_code;
    			$consignee->telephone = $request->txt_tel;
    			$consignee->fax = $request->txt_fax;
    			$consignee->pic = $request->txt_pic;
    			$consignee->save();
    			break;

    			case Company::type_agent:
    			$agent = new Agent();
    			$agent->company_id = $company->id;
    			$agent->code = $request->txt_code;
    			$agent->telephone = $request->txt_tel;
    			$agent->fax = $request->txt_fax;
    			$agent->pic = $request->txt_pic;
    			$agent->port_id = $request->txt_portid;

    			$company->port_code = $request->txt_portcode;
    			$company->ba_code = $request->txt_bacode;
    			$company->save();

    			$agent->save();
    			break;
    		}

    		if($company->isShipper()){
    			$this->store_restriction(
    				$shipper->id, $request->ddl_vesselname,
    				$request->txt_vesseltype, $request->txt_vesselage,
    				$request->ddl_flag, "store"
    			);

    			$cid = $request->query('cid');
    			if(!empty($cid)){
    				$company_id = Hashids::decode($cid);
    				if($company_id){
    					$dup_company = Company::find($company_id[0]);
    					if($dup_company->isShipper()){
    				    //Get duplicate company's default charges if it is a shipper
    						$charges = DefaultCharge::where("shipper_id", $dup_company->getShipper->id)->get();

    						foreach($charges AS $ea){
    							$charge = new DefaultCharge();
    							$charge->shipper_id = $shipper->id;
    							$charge->charge_id = $ea->charge_id;
    							$charge->unit = $ea->unit;
    							$charge->unit_size = $ea->unit_size;
    							$charge->price = $ea->price;
    							$charge->pol = $ea->pol;
    							$charge->final_dest = $ea->final_dest;
    							$charge->payment = $ea->payment;
    							$charge->save();
    						}
    					}
    				}
    			}
    		}

    		return $company;
    	});

if($request->txt_submittype == "proceed"){
	return redirect::action('MaintenanceController@edit_companycharges', Hashids::encode($company->id));
} else {
	return redirect::action('MaintenanceController@index_company')
	->with("class", "alert-green")
	->with("msg", "Successfully created " . $company->name . " profile");
}
}

public function edit_company($hash)
{
	$company_id = Hashids::decode($hash);

	if($company_id){
		$company = Company::find($company_id[0]);

		if($company){
			$ports = Port::get();
			$consignee = null;

			if($company->type == Company::type_shipperconsignee){
				$consignee = Consignee::where("company_id", $company_id)->first();
			}
			$vessels = Vessel::get();

			$restrictions = null;
			if($company->isShipper()){
				$restrictions = Restriction::where("shipper_id", $company->getShipper->id)->get()->groupBy('type');
			}
			return view("maintenance.company.edit-company", compact("company", "ports", "consignee", "vessels", "restrictions"));
		}
	}
	return redirect::action("MaintenanceController@index_company")->with("error", "Company not found.");
}

public function delete_company_by_type($type, $newtype, $id, $bool){
	DB::beginTransaction();
	try{
    		//Delete company's category
		if($type == Company::type_shipper || $type == Company::type_shipperconsignee && $newtype != Company::type_shipperconsignee){
			$shipper = Shipper::where("company_id", $id)->first();
			Restriction::where("shipper_id", $shipper->id)->delete();
			DefaultCharge::where("shipper_id", $shipper->id)->delete();
			$shipper->delete();

			if($newtype != Company::type_shipperconsignee){
				Consignee::where("company_id", $id)->delete();
			}

		} else if ($type == Company::type_consignee || $type == Company::type_shipperconsignee && $newtype != Company::type_shipperconsignee){
			Consignee::where("company_id", $id)->delete();
		} else {
			Agent::where("company_id", $id)->delete();
		}

    		//Delete company details
		if($bool){
			Company::find($id)->delete();
		}

		DB::commit();
	} catch (Exception $e) {
		DB::rollBack();
		dd($e->getMessage());
	}
}

public function update_company($id, EditCompany $request){
	$company = Company::find($id);
	if($company){
		DB::beginTransaction();

		try {
			$oldCompanyType = $company->type;

			$company->name = $request->txt_name;
			$company->roc_no = $request->txt_roc;
			$company->type = (count($request->radio_company_type) == 1) ? $request->radio_company_type[0] : 2;

			if($company->isDirty('type')){
				$this->delete_company_by_type($oldCompanyType, $company->type, $company->id, false);
			}

			$company->save();

			switch($company->type){
				case Company::type_shipper:

				$updateArr = $this->getUpdateType($request->radio_update);

				Shipper::updateOrCreate(
					['company_id' => $id],
					[
						'code' => $request->txt_code, 'telephone' => $request->txt_tel, 'fax' => $request->txt_fax,
						'pic' => $request->txt_pic,
						'voyage_updates' => $updateArr[0],
						'delivery_updates' => $updateArr[1],
						'handling_method' => $request->txt_handlingmethod
					]
				);
				break;

				case Company::type_consignee:
				Consignee::updateOrCreate(
					['company_id' => $id],
					[
						'code' => $request->txt_code, 'telephone' => $request->txt_tel, 'fax' => $request->txt_fax,
						'pic' => $request->txt_pic
					]
				);
				break;

				case Company::type_shipperconsignee:

				$updateArr = $this->getUpdateType($request->radio_update);

				Shipper::updateOrCreate(
					['company_id' => $id],
					[
						'code' => $request->txt_code, 'telephone' => $request->txt_tel, 'fax' => $request->txt_fax,
						'pic' => $request->txt_pic,
						'voyage_updates' => $updateArr[0],
						'delivery_updates' => $updateArr[1], 'handling_method' => $request->txt_handlingmethod
					]
				);
				Consignee::updateOrCreate(
					['company_id' => $id],
					[
						'code' => $request->txt_code, 'telephone' => $request->txt_tel, 'fax' => $request->txt_fax,
						'pic' => $request->txt_pic
					]
				);
				break;

				case Company::type_agent:
				Agent::updateOrCreate(
					['company_id' => $id],
					[
						'code' => $request->txt_code, 'telephone' => $request->txt_tel, 'fax' => $request->txt_fax,
						'pic' => $request->txt_pic, 'port_id' => $request->txt_portid
					]
				);

				$company->port_code = $request->txt_portcode;
				$company->ba_code = $request->txt_bacode;
				$company->save();
				break;
			}

			$addresses = json_decode(html_entity_decode($request->txt_addressArr));
			$del_query = CompanyAddress::where("company_id", $id)->where(function($query) use(&$addresses, $id){
				foreach($addresses AS $key => $ea){
                    	//Remove address that is not in the $addresses array under the selected company_id
					$adr = CompanyAddress::where("company_id", $id)
					->where("address", $ea)
					->first();

					if(!empty($detail)){
						$query->where("id", "!=", $adr->id);
                        	//Remove element from array so it will not be added into DB
						unset($addresses[$key]);
					}
				}
			})->delete();

			foreach($addresses AS $ea){
				$needle = "</strong><br>";

				$needle_index = strpos($ea, $needle);

				if($needle_index > 0){
					$desc = strip_tags(substr($ea, 0, $needle_index + strlen($needle)));
					$ea = substr($ea, strpos($ea, $needle) + strlen($needle));
				}

				$add = new CompanyAddress();
				$add->company_id = $id;
				$add->address = $ea;
				$add->description = !empty($desc) ? $desc : null;
				$add->save();
			}

			$emails = json_decode($request->txt_emailArr);
			$del_email = CompanyEmail::where("company_id", $id)->where(function($query) use(&$emails, $id){
				foreach($emails AS $key => $ea){
					$email = CompanyEmail::where("company_id", $id)
					->where("email", $ea)
					->first();

					if(!empty($detail)){
						$query->where("id", "!=", $email->id);
						unset($emails[$key]);
					}
				}
			})->delete();

			foreach($emails AS $ea){
				$email = new CompanyEmail();
				$email->company_id = $id;
				$email->email = $ea;
				$email->save();
			}

			if($company->isShipper()){
				$this->store_restriction($company->getShipper->id, $request->ddl_vesselname, $request->txt_vesseltype, $request->txt_vesselage, $request->ddl_flag, "update");
			}

			DB::commit();
			if($request->txt_submittype == "proceed"){
				return redirect::action('MaintenanceController@edit_companycharges', Hashids::encode($company->id));
			} else {
				return redirect::action('MaintenanceController@index_company')
				->with("class", "alert-green")
				->with("msg", "Successfully updated " . $company->name . " profile");
			}

            // return redirect::action("MaintenanceController@edit_company", Hashids::encode($company->id))
            // ->with("class", "alert-green")
            // ->with("msg", "Successfully updated company details.");
		} catch (Exception $e) {
			DB::rollBack();
			return redirect::action("MaintenanceController@edit_company", Hashids::encode($company->id))
			->with("class", "alert-red")
			->with("msg", $e->getMessage());
		}
	}
}

public function check_company_delete($id){

	$company = Company::find($id);

	$voyages = collect(DB::select("SELECT voyage_id AS id FROM voyage_destinations WHERE etd >= CURDATE() OR atd >= CURDATE() GROUP BY voyage_id"))->pluck('id');

	if(!empty($company)){
        //Check if BL or Reservation uses selected company before deleting
		$bl = BillLading::where("shipper_id", $id)->orWhere("consignee_id", $id)->orWhere("notify_id", $id)->where("deleted_at", null)->get();

		if($company->isShipper()){

            //Get voyages that POL don't have ATD / yet to sail (Can't delete company with reservation under these voyage)
			$resv_voyages = Voyage::where("atd_pol", ">=", Carbon::now()->format('Y-m-d 00:00:00'))->orWhere("atd_pol", null)->get(['voyage_id']);

			$resv = Reservation::where("shipper_id", $company->id)->whereIn("voyage_id", $resv_voyages)->where("deleted_at", null)->get();
		} else {
			$resv = [];
		}

		if(count($bl) > 0){
        //Join BL No.
			$text = "The selected company is used in the following BL: <br>" . $bl->implode('bl_no', ", ");
			return response()->json(['success' => false, 'msg' => $text]);
		}

		if(count($resv) > 0){
			$textArr = [];
			foreach($resv AS $res){
				array_push($textArr, "R" . str_pad($res->id, 4, "0", STR_PAD_LEFT));
			}
			$text = "The selected company is used in the following reservation: <br>" . implode(", ", $textArr);
			return response()->json(['success' => false, 'msg' => $text]);
		}

		return response()->json(['success' => true]);
	} else {
		return response()->json(['success' => false, 'msg' => "Company not found!"]);
	}
}

public function delete_company($id){
	$company = Company::find($id);

	if(!empty($company)){
		$this->delete_company_by_type($company->type, $company->type, $id, true);
		return redirect::action("MaintenanceController@index_company")
		->with("class", "alert-green")
		->with("msg", "Successfully deleted " . $company->name . "'s profile." );
	} else {
		return redirect::action("MaintenanceController@index_company")
		->with("class", "alert-red")
		->with("msg", "Delete unsuccessful. Company not found.");
	}
}

public function index_companycharges($hash){
	$company_id = Hashids::decode($hash);

	if($company_id){
		$company = Company::find($company_id[0]);
		$charges = Charge::get();

		return view("maintenance.company.index-companycharges", compact("company", "charges"));
	} else {
		return redirect::action("MaintenanceController@index_company")
		->with("class", "alert-red")
		->with("msg", "Company not found.");
	}
}

public function store_companycharges($id, Request $request){
	$company = Company::find($id);

	$chargeType = json_decode($request->txt_charge);

	foreach ($chargeType AS $ea) {
		$charge = new DefaultCharge;
		$charge->shipper_id = $company->getShipper->id;
		$charge->chargedetail_id = $ea->type;
		$charge->price = $ea->price;
            // $default_charges = Charge::
		$charge->save();
	}

	return redirect::action('MaintenanceController@index_company')->with('success', 'Successfully added default charges.');
}

public function edit_companycharges($hash){
	$company_id = Hashids::decode($hash);

	if($company_id){
		$company = Company::find($company_id[0]);
		$defaultCharges = DefaultCharge::where("shipper_id", $company->getShipper->id)->select('charge_id')->groupBy('charge_id')->get();

		$charges = Charge::orderBy('code', 'ASC')->get();
		$ports = Port::orderBy("code", "ASC")->get();

		$vessels = Vessel::orderBy("name", "ASC")->get();

		$shipper_id = $company->getShipper->id;

		return view("maintenance.company.edit-companycharges", compact("shipper_id", "company", "defaultCharges", "charges", "ports", "vessels"));
	} else {
		return redirect::action("MaintenanceController@index_company")
		->with("class", "alert-red")
		->with("msg", "Company not found.");
	}
}

public function update_companycharges($id, Request $request){
	try {

		$company = Company::find($id);

		DB::transaction(function() use($company, $request){
			$shipper = Shipper::where("company_id", $company->id)->first();

			$charge_code = $request->ddl_chargecode;
			if($request->chk_port = 'on'){
				$pol = $request->ddl_pol;
				$final_dest = $request->ddl_final;
			}
			$payment = $request->ddl_payment;

			$amount = $request->txt_amount;

			foreach($amount AS $key => $amt){

				$vesselinputname = "ddl_vessel";

				if($key != 0){
					$vesselinputname = "ddl_vessel" . $key;
				}

				$vessels = $request->$vesselinputname;

				$charge_unit = $request->ddl_unit[$key];

				$unit = substr($charge_unit, strpos($charge_unit, "-")+1);
				$unit_size = substr($charge_unit, 0, strpos($charge_unit, "-"));

				$pol = ($pol != "-") ? $pol : null;
				$final_dest = ($final_dest != "-") ? $final_dest : null;


            // Check if exist already
				$exist = DefaultCharge::where("shipper_id", $shipper->id)
				->where("charge_id", $charge_code)
				->where("unit", $unit)
				->where("unit_size", $unit_size)
				->where("pol", $pol)
				->where("final_dest", $final_dest)
				->where("payment", $payment)
				->get();

				$removeVessel = false;

				if(!empty($exist)){
					if(!empty($vessels)){
						foreach($exist AS $e){
							foreach($vessels AS $key => $v_id){
								$check_cv = ChargeVessel::where("charge_id", $e->id)->where("vessel_id", $v_id)->first();
								if(!empty($check_cv)){
									unset($vessels[$key]);
									$removeVessel = true;
								}
							}
						}
					}
				}


            // Skip this charge because charge without vessel already exist
				if(empty($vessels) || $vessels == null){
                // If no vessels left, check if charge without vessel is added
					$no_vessel_charge = DefaultCharge::where("shipper_id", $shipper->id)
					->where("charge_id", $charge_code)
					->where("unit", $unit)
					->where("unit_size", $unit_size)
					->where("pol", $pol)
					->where("final_dest", $final_dest)
					->where("payment", $payment)
					->doesntHave('getChargeVessel')
					->first();

					if(!empty($no_vessel_charge)){
						continue;
					}
				}

				$charge = new DefaultCharge();
				$charge->shipper_id = $shipper->id;
				$charge->charge_id = $charge_code;
				$charge->unit = $unit;
				$charge->unit_size = $unit_size;
				$charge->price = $amt;
				$charge->pol = $pol;
				$charge->final_dest = $final_dest;
				$charge->payment = $payment;
				$charge->save();

            // Attach vessels to default charge
				if(!empty($vessels)){
					foreach($vessels AS $v_id){
						$cv = new ChargeVessel();
						$cv->charge_id = $charge->id;
						$cv->vessel_id = $v_id;
						$cv->save();
					}
				}


			}
		});

		return redirect::action('MaintenanceController@edit_companycharges', Hashids::encode($company->id))->with("success", "Successfully updated company's default charges");

	} catch (\Exception $e) {
		dd($e);
		return redirect::action('MaintenanceController@edit_companycharges', Hashids::encode($company->id))->with("error", "Unable to update company's default charges. " . $e->getMessage());
	}
}

public function get_chargedetail($id, $bl_id = NULL, $payment = NULL){
	$details = ChargeDetail::where("charge_id", $id)->get();
	if($bl_id != null){
		$bl = BillLading::find($bl_id);
	}

	foreach($details AS $ea){
		if(!empty($bl)){
			$shipper = Shipper::where("company_id", $bl->shipper_id)->first();
			$charges = collect(DB::select("SELECT * FROM default_charges WHERE shipper_id = " . $shipper->id . " AND charge_id = " . $id . " AND payment = '" . $payment . "' AND unit = '" . $ea->unit . "' AND unit_size = '" . $ea->unit_size . "' AND ((pol IS NULL AND final_dest IS NULL) OR (pol = '" . $bl->pol_id . "' AND final_dest = " . $bl->fpd_id ."))"));

			$amt = 0;

			foreach($charges AS $c){
				$vsl = ChargeVessel::where("charge_id", $c->id)->where("vessel_id", $bl->vessel_id)->first();
				if(!empty($vsl)){
					$amt = $c->price;
					break;
				}
			}

			$ea->default = $amt;

            // if(count($charges) > 1){
            //     $chg = $charges->where("pol", "!=", null)->first();
            // } else {
            //     if(count($charges)){
            //         $ea->default = $charges[0]->price;
            //     } else {
            //         $ea->default = 0;
            //     }
            // }
		}

		$ea->text = $ea->chargeDetailText();
	}

	return response()->json($details->groupBy("unit")->toArray());
}

private function checkCharge($charge, $vessels){
	if(!empty($vessels)){
		$no_vessel_charge = DefaultCharge::query()->where("shipper_id", $charge->shipper_id)
		->where("charge_id", $charge->charge_id)
		->where("unit", $charge->unit)
		->where("unit_size", $charge->unit_size)
		->where("payment", $charge->payment)
		->where("id", "!=", $charge->id);

		if($charge->pol == null){
			$no_vessel_charge = $no_vessel_charge->whereNull('pol')->whereNull('final_dest');
		} else {
			$no_vessel_charge = $no_vessel_charge->where('pol', $charge->pol)->where('final_dest', $charge->final_dest);
		}

		$no_vessel_charge = $no_vessel_charge->whereHas('getChargeVessel', function($query) use($vessels) {
			$query->whereIn("vessel_id", $vessels);
		})->get();

		if($no_vessel_charge){
			foreach($no_vessel_charge AS $nvc){
				$chargeVessel = ChargeVessel::where("charge_id", $nvc->id)->whereIn("vessel_id", $vessels)->get();
				$vessels = collect($vessels)->diff($chargeVessel->pluck('vessel_id'));
			}

			return ["false", $vessels];
		}
	} else {
		$charge = DefaultCharge::where("shipper_id", $charge->shipper_id)
		->where("charge_id", $charge->charge_id)
		->where("unit", $charge->unit)
		->where("unit_size", $charge->unit_size)
		->where("pol", $charge->pol)
		->where("final_dest", $charge->final_dest)
		->where("payment", $charge->payment)
		->doesntHave('getChargeVessel')
		->get();

		if(!empty($charge)){
			return ["false", $vessels];
		}
	}
	return ["true", $vessels, $charge];
}

public function update_defaultcharge($id, Request $request){
	$charge = DefaultCharge::find($id);
	if(!empty($charge)){

		$charge->pol = ($request->pol != "-") ? $request->pol : null;
		$charge->final_dest = ($request->final_dest != "-") ? $request->final_dest : null;
		$charge->price = $request->amount;
		$charge->payment = $request->payment;

		$vessels = $request->vessel;

        // Check if charge has this details saved
		$valid = $this::checkCharge($charge, $vessels);

		$vessels = $valid[1];

		$charge->save();

		if(empty($vessels)){
            // Delete all
			ChargeVessel::where("charge_id", $charge->id)->delete();
		} else {
			foreach($vessels AS $v){
				$chargeVessel = ChargeVessel::where("charge_id", $charge->id)->where("vessel_id", $v)->first();
				if(empty($chargeVessel)){
					$cv = new ChargeVessel();
					$cv->charge_id = $charge->id;
					$cv->vessel_id = $v;
					$cv->save();
				}
			}

            // Delete charge vessel that does not have vessel id in the list
			ChargeVessel::where("charge_id", $charge->id)->whereNotIn("vessel_id", $vessels)->delete();
		}

		return response()->json(["success" => true, "charge_id" => $charge->charge_id, "valid" => $valid]);
	} else {
		return response()->json(["success" => false]);
	}
}

public function delete_defaultcharge($id){
	$charge = DefaultCharge::find($id);
	if(!empty($charge)){

        // Delete charge vessels
		$vc = ChargeVessel::where("charge_id", $id)->delete();

		$charge->delete();
		return response()->json(["success" => true, "charge_id" => $charge->charge_id]);
	} else {
		return response()->json(["success" => false]);
	}
}

public function viewDefaultCharge($shipper_id, $charge_id)
{
	$charge = DefaultCharge::where("shipper_id", $shipper_id)->where("charge_id", $charge_id)->get();
	if(!empty($charge)){

		$pills = $charge->keyBy("final_dest")->sortKeys();

		$pillArr = [];

		$pillCollection = [];

		foreach($pills AS $id => $p){
			$portCode = "General";

			if($id != ""){
				$port = Port::find($id);
				$portCode = $port->code;
			} else {
				$id = null;
			}

			array_push($pillArr, $portCode);

			$charges = $charge->where("final_dest", $id);

			foreach($charges AS $c){
				if($c->pol != null){
					$c->pol_code = $c->getPOL->code;
				} else {
					$c->pol_code = "-";
				}

				$c->unit_text = $c->chargeDetailText();
				$c->price = number_format($c->price, 2);

				$vessels = $c->getChargeVessel->map(function($item, $key){
					return $item->getVessel->name;
				});

				$c->vessel_text = $vessels->implode(", ");

				if(!array_key_exists($portCode, $pillCollection)){
					$pillCollection[$portCode] = [];
				}
				array_push($pillCollection[$portCode], $c);
			}
		}

		$m_charge = Charge::find($charge_id);

		$chargeStr = $m_charge->code . ' - ' . $m_charge->name;

		return response()->json(['success' => true, 'charge' => $chargeStr, 'data' => $pillCollection, 'pills' => $pillArr]);
	} else {
		return response()->json(['success' => false]);
	}
}

public function getDefaultCharge($charge_id)
{
	$charge = DefaultCharge::find($charge_id);
	if(!empty($charge)){
		$charge->charge_code = $charge->getCharge->code;
		$charge->unit_text = $charge->chargeDetailText();
		$charge->price = number_format($charge->price, 2);

		$cv = $charge->getChargeVessel;
		$vessel_arr = collect();

		if(!empty($cv)){
			foreach($cv AS $vessel){
				$vessel_arr->push($vessel->getVessel);
			}
		}

		$charge->vessels = $vessel_arr;
		return response()->json(['success' => true, 'data' => $charge]);
	} else {
		return response()->json(['success' => false]);
	}
}

    /**
     * PORT CONTROLLERS
     */
    public function index_port()
    {
    	$ports = Port::all();
    	return view('maintenance.port.index-port', compact('ports'));
    }

    public function create_port()
    {
    	return view('maintenance.port.create-port');
    }

    public function store_port(StorePort $request)
    {
    	$validated = $request->all();

    	$port = new Port;
    	$port->name = trim($request->port_name);
    	$port->code = strtoupper(trim($request->port_code));
    	$port->location = ucwords(strtolower(trim($request->port_location)));

    	$split_location = explode(' ', $port->location);
    	$location_code = '';
    	$count = count($split_location);
    	if($count == 1) {
    		$location_code .= substr($split_location[0], 0, 1);
    		$location_code .= strtoupper(substr($split_location[0], -1));
    	} else {
    		for($i = 0; $i < $count; $i++) {
    			$location_code .= substr($split_location[$i], 0, 1);
    		}
    	}

    	$port->location_code = $location_code;

    	$port->bl_prefix = trim($request->bl_prefix);
    	$port->station_code = strtoupper(trim($request->station_code));
    	$port->save();

    	return redirect()->route('index-port')->with('success', 'Successfully added port.');
    }

    public function edit_port($hash)
    {
    	$port_id = Hashids::decode($hash);

    	if($port_id) {
    		$port = Port::find($port_id[0]);
    		return view('maintenance.port.edit-port', compact('port'));
    	} else {
    		return redirect()->route('index-port')->with('error', 'Port not found.');
    	}
    }

    public function put_port(EditPort $request, $id)
    {
    	$validated = $request->all();

    	$port = Port::find($id);
    	$port->name = trim($request->port_name);
    	$port->code = strtoupper(trim($request->port_code));
    	$port->location = ucwords(strtolower(trim($request->port_location)));

    	$split_location = explode(' ', $port->location);
    	$location_code = '';
    	$count = count($split_location);
    	if($count == 1) {
    		$location_code .= substr($split_location[0], 0, 1);
    		$location_code .= strtoupper(substr($split_location[0], -1));
    	} else {
    		for($i = 0; $i < $count; $i++) {
    			$location_code .= substr($split_location[$i], 0, 1);
    		}
    	}

    	$port->location_code = $location_code;

    	$port->bl_prefix = trim($request->bl_prefix);
    	$port->station_code = strtoupper(trim($request->station_code));
    	$port->save();

    	return redirect()->route('index-port')->with('success', 'Successfully edited port '.$request->port_name.'.');
    }

    public function delete_port(Request $request)
    {
    	$dest_bind = VoyageDestinations::where('port', $request->port_id)->get();
    	$voyage_bind = Voyage::where('pol_id', $request->port_id)->get();
    	$port = Port::find($request->port_id);
    	$name = $port->name;

    	if($voyage_bind->isNotEmpty()) {
    		$msg = 'Failed to delete port as it a POL in voyage(s).<br>Remove port from any voyage(s) to delete this port.<br><br><b>Found in voyage(s):</b> ';

    		foreach($voyage_bind AS $key => $voyage) {
    			if($key > 0) {
    				$msg .= ', ';
    			}
    			$msg .= $voyage->voyage_id;
    		}

    		return redirect()->route('index-port')->with('error', $msg);
    	} else if($dest_bind->isNotEmpty()) {
    		$msg = 'Failed to delete port as it an active destination.<br>Remove port from any voyage(s) to delete this port.<br><br><b>Found in voyage(s):</b> ';

    		foreach($dest_bind AS $key => $dest) {
    			$voyage = Voyage::find($dest->voyage_id);
    			if($key > 0) {
    				$msg .= ', ';
    			}
    			$msg .= $voyage->voyage_id;
    		}

    		return redirect()->route('index-port')->with('error', $msg);
    	} else {
    		$port->delete();
    		return redirect()->route('index-port')->with('success', 'Successfully deleted port '.$name.'.');
    	}
    }

    /**
     * CHARGE CONTROLLERS
     */
    public function index_charge()
    {
    	$charges = Charge::get();
    	return view('maintenance.charge.index-charge', compact("charges"));
    }

    public function create_charge()
    {
    	return view('maintenance.charge.create-charge');
    }

    public function store_charge(StoreCharge $request){

    	// $charges = json_decode($request->txt_charges);
    	$charges = $request->ddl_unit;

    	$charge = DB::transaction(function () use ($request, $charges) {
    		$charge = new Charge;
    		$charge->code = $request->txt_code;
    		$charge->name = $request->txt_name;
    		$charge->save();

    		foreach($charges AS $ea){
    			$charge_detail = new ChargeDetail;
    			$charge_detail->charge_id = $charge->id;

    			$unit = substr($ea, strpos($ea, "-")+1);
    			$unit_size = substr($ea, 0, strpos($ea, "-"));

    			$charge_detail->unit = $unit;
    			$charge_detail->unit_size = $unit_size;
                // $charge_detail->unit_price = $ea->unitprice;
    			$charge_detail->save();
    		}

    		return $charge;
    	});

    	Session::flash('msg', 'Successfully added ' . $charge->code . ' charges.');
    	Session::flash('class', 'alert-success');

    	return redirect::action('MaintenanceController@index_charge');
    }

    public function edit_charge($hash){
    	$charge_id = Hashids::decode($hash);

    	if($charge_id){
    		$charge = Charge::find($charge_id[0]);
    		if(empty($charge)){
    			return redirect::action('MaintenanceController@index_charge')->with('error', 'Charge not found.');
    		}
    	} else {

    	}

    	return view('maintenance.charge.edit-charge', compact("charge"));
    }

    public function put_charge($id, EditCharge $request){
    	DB::beginTransaction();
    	try{
    		$charge = Charge::find($id);

    		$charge->code = $request->txt_code;
    		$charge->name = ucwords($request->txt_name);
    		$charge->save();

    		$charges = $request->ddl_unit;

    		// $charges = json_decode($request->txt_charges);

    		$del_query = ChargeDetail::where("charge_id", $id)->where(function($query) use(&$charges, $id){
    			foreach($charges AS $key => $ea){

    				$unit = substr($ea, strpos($ea, "-")+1);
    				$unit_size = substr($ea, 0, strpos($ea, "-"));

                    //Remove charge detail that is not in the $charges array under the selected charge_id
    				$detail = ChargeDetail::where("charge_id", $id)
    				->where("unit", $unit)
    				->where("unit_size", $unit_size)
    				->first();

    				if(!empty($detail)){
    					$query->where("id", "!=", $detail->id);
                        //Remove element from array so it will not be added into DB
    					unset($charges[$key]);
    				}
    			}
    		})->delete();

    		foreach($charges AS $ea){

    			$unit = substr($ea, strpos($ea, "-")+1);
    			$unit_size = substr($ea, 0, strpos($ea, "-"));

    			$charge_detail = new ChargeDetail;
    			$charge_detail->charge_id = $charge->id;
    			$charge_detail->unit = $unit;
    			$charge_detail->unit_size = $unit_size;
    			$charge_detail->save();
    		}

    		DB::commit();

    		Session::flash('msg', 'Successfully updated ' . $charge->code . ' charges.');
    		Session::flash('class', 'alert-success');
    		return redirect::action('MaintenanceController@index_charge');
    	} catch (Exception $e) {
    		DB::rollBack();
    		return redirect::back()->with('error', $e->getMessage());
    	}
    }

    public function delete_charge($id){
    	$charge = Charge::find($id);
    	if(!empty($charge)){
    		$code = $charge->code;
    		$charge->delete();
    		return redirect()->route('index-charge')->with('success', 'Successfully deleted '.$code.' charge.');
    	} else {
    		return redirect()->route('index-charge')->with('error', 'Delete unsuccessful.');
    	}
    }

    /**
     * VESSEL CONTROLLERS
     */
    public function index_vessel()
    {
    	$vessels = Vessel::all();
    	return view('maintenance.vessel.index-vessel', compact('vessels'));
    }

    public function create_vessel()
    {
    	return view('maintenance.vessel.create-vessel');
    }

    public function store_vessel(StoreVessel $request)
    {
    	$validated = $request->all();

    	$vessel = new Vessel;
    	$vessel->name = strtoupper(trim($request->vessel_name));
    	$vessel->type = $request->vessel_type;
    	$vessel->dwt = $request->vessel_dwt;
    	if(!empty($request->radio_semi_type)) {
    		$vessel->semicontainer_service = $request->radio_semi_type;
    	} else {
    		$vessel->semicontainer_service = 0;
    	}
    	$vessel->manufactured_year = $request->vessel_year;
    	$vessel->flag = ucfirst(trim($request->vessel_flag));
    	$vessel->default_shipment = $request->vessel_default;
    	$vessel->save();

    	return redirect()->route('index-vessel')->with('success', 'Successfully added vessel.');
    }

    public function edit_vessel($hash)
    {
    	$vessel_id = Hashids::decode($hash);

    	if($vessel_id) {
    		$vessel = Vessel::find($vessel_id[0]);

            //Check if vessel is used in any voyage
            //If not used, enable edit on shipment type
    		$voy_arr = Voyage::where("vessel_id", $vessel->id)->get()->pluck("id")->toArray();

    		$sql_query = "SELECT * FROM (SELECT voyage_id
    		FROM reservations WHERE voyage_id IS NOT NULL AND deleted_at IS NULL
    		UNION
    		SELECT voyage_id
    		FROM bookings WHERE voyage_id IS NOT NULL
    		AND deleted_at IS NULL
    		UNION
    		SELECT voyage_id
    		FROM bill_ladings WHERE voyage_id IS NOT NULL
    		AND deleted_at IS NULL
    	) a";

    	if(count($voy_arr) > 0){
    		$sql_query .= " WHERE voyage_id IN (" . implode(", ", $voy_arr) . ")";
    	}

    	$db_check = DB::select($sql_query);

    	if(count($db_check) > 0 && count($voy_arr) > 0){
    		$vessel->editable = false;
    	} else {
    		$vessel->editable = true;
    	}

    	return view('maintenance.vessel.edit-vessel', compact('vessel'));
    } else {
    	return redirect()->route('index-vessel')->with('error', 'Vessel not found.');
    }
}

public function put_vessel(EditVessel $request, $id)
{
	$validated = $request->all();

	$vessel = Vessel::find($id);
	$vessel->name = strtoupper(trim($request->vessel_name));
	$vessel->type = $request->vessel_type;
	$vessel->dwt = $request->vessel_dwt;
	if(!empty($request->radio_semi_type)) {
		$vessel->semicontainer_service = $request->radio_semi_type;
	} else {
		$vessel->semicontainer_service = 0;
	}
	$vessel->manufactured_year = $request->vessel_year;
	$vessel->flag = ucfirst(trim($request->vessel_flag));
	$vessel->default_shipment = $request->vessel_default;
	$vessel->save();

	return redirect()->route('index-vessel')->with('success', 'Successfully edited vessel '.$request->vessel_name.'.');
}

public function delete_vessel(Request $request)
{
	$voyage_bind = Voyage::where('vessel_id', $request->vessel_id)->get();
	$vessel = Vessel::find($request->vessel_id);
	$name = $vessel->name;

	if($voyage_bind->isNotEmpty()) {
		$msg = 'Failed to delete vessel as it in bound to a voyage.<br>Remove vessel from any existing voyage(s) to delete this vessel.<br><br><b>Found in voyage(s):</b> ';

		foreach($voyage_bind AS $key => $voyage) {
			if($key > 0) {
				$msg .= ', ';
			}
			$msg .= $voyage->voyage_id;
		}

		return redirect()->route('index-vessel')->with('error', $msg);
	} else {
		$vessel->delete();
		return redirect()->route('index-vessel')->with('success', 'Successfully deleted vessel '.$name.'.');
	}
}

    /**
     * PACKING CONTROLLERS
     */
    public function index_packing_type()
    {
    	return view('maintenance.packing.index-packing');
    }

    public function create_packing_type()
    {
    	return view('maintenance.packing.create-packing');
    }

    /**
     * SHORTCUT KEYS CONTROLLERS
     */
    public function index_shortcut()
    {
    	$shortcuts = ShortcutKey::get();
    	return view('maintenance.shortcut.index-shortcut', compact("shortcuts"));
    }

    public function edit_shortcut($hash)
    {
    	$id = Hashids::decode($hash);
    	if(!empty($id)){
    		$shortcut = ShortcutKey::find($id[0]);
    		return view('maintenance.shortcut.edit-shortcut', compact("shortcut"));
    	} else {
    		return redirect::back();
    	}
    }

    public function update_shortcut($id, Request $request)
    {
    	$sk = ShortcutKey::find($id);
    	$sk->description = $request->txt_description;
    	$sk->save();

    	Session::flash('msg', 'Successfully updated ' . $sk->shortcut_code . "'s description text.");
    	Session::flash('class', 'alert-success');

    	return redirect::action('MaintenanceController@index_shortcut');
    }
}
