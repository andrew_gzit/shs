<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\PdfToText\Pdf;
use Carbon\Carbon;
use Storage;
use Artisan;
use Auth;
use Session;
use Redirect;

use App\Port;
use App\InwardBL;
use App\User;

class NormalController extends Controller
{
	public function index() {
		//$exitCode = Artisan::call('cache:clear');
		//$exitCode = Artisan::call('view:clear');
		return view('index');
	}

	public function login() {
		if(Auth::check()){
			return redirect::action("NormalController@index");
		}
		return view("login");
	}

	public function authenticate(Request $request) {
		if(\Auth::attempt(['username' => $request->username, 'password' => $request->password])){
			return redirect::action("NormalController@index");
		} else {
			Session::flash("msg", "Incorrect login username or password.");
			Session::flash("class", "alert-danger");
			return redirect::back();
		}
	}

	public function logout() {
		Auth::logout();
		Session::flash("msg", "Successfully logged out.");
		Session::flash("class", "alert-success");
		return redirect()->route('login');
	}

	public function splitByText($string){
		$text_arr = ["*-*)", "BHD.", "BHD", "BERHAD", "ORDER", "CONSIGNEE"];

		$company_name = ".";
		$company_add = ".";

		foreach($text_arr AS $split_text){
			if($split_text == "*-*)"){
				if(str_is('*-*)*', $string)){
					//Search for (xxxx-x) SSM number
					preg_match("/\(\S+\-\D\)/", $string, $matches, PREG_OFFSET_CAPTURE);
					$position = $matches[0][1];

					$company_name = substr($string, 0, $position + strlen($matches[0][0]));
					$company_add = trim(substr($string, $position + strlen($matches[0][0])));
					break;
				}
			}

			if(strpos($string, $split_text) != false){
				$company_name = substr($string, 0, strpos($string, $split_text) + strlen($split_text));
				$company_add = trim(substr($string, strpos($string, $split_text) + strlen($split_text)));
				break;
			}
		}

		return [$company_name, $company_add];
	}

	public function checkPort($string) {
		$port = Port::where("location", "LIKE", "%" . $string . "%")->first();
		if(!empty($port)){
			return true;
		} else {
			return false;
		}
	}

	public function minify_nav(Request $request){
		session(['minified' => $request->minified]);
		return response()->json(true);
	}
}