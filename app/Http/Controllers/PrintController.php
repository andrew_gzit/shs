<?php

namespace App\Http\Controllers;

use LynX39\LaraPdfMerger\Facades\PdfMerger;
use LynX39\LaraPdfMerger\PdfManage;
use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;
use DB;
use Redirect;

// Eloquent Models
use App\Voyage;
use App\Vessel;
use App\Port;
use App\Company;
use App\BillLading;
use App\CargoDetail;
use App\Reservation;
use App\Booking;
use App\InwardBL;

class PrintController extends Controller
{
    public function index_manifest(Request $request){
    	$vessels = Vessel::orderBy('name', 'ASC')->get();
    	$shippers = Company::where('type', Company::type_shipper)->orWhere('type', Company::type_shipperconsignee)->orderBy('name', 'ASC')->get();
    	$ports = Port::orderBy('name', 'ASC')->get();
    	$bls = BillLading::get();

        //Filtering
        $filter_vessel = $request->query('vessel', null);
        $filter_shipper = $request->query('shipper', null);
        $filter_pod = $request->query('pod', null);
        $filter_bl = $request->query('bl', null);

        $filter_bool = false;
        $msg = "Filter by:";

        if($filter_vessel || $filter_shipper || $filter_pod || $filter_bl){
            $filter_bool = true;

            $query = BillLading::query();

            if($filter_bl){
                $query->where("id", $filter_bl);
                $bl = BillLading::find($filter_bl);
                $msg .= "<br><strong>BL No.</strong>: " . $bl->bl_no;
            } else {
                if($filter_vessel){
                    $query->where('vessel_id', $filter_vessel);
                    $vessel = Vessel::find($filter_vessel);
                    $msg .= "<br><strong>Vessel</strong>: " . $vessel->name;
                }

                if($filter_shipper){
                    $query->where('shipper_id', $filter_shipper);
                    $company = Company::find($filter_shipper);
                    $msg .= "<br><strong>Shipper</strong>: " . $company->getCompanyOfType()->code . " - " . $company->name;
                }

                if($filter_pod){
                    $query->where('pod_id', $filter_pod);
                    $port = Port::find($filter_pod);
                    $msg .= "<br><strong>Port of Discharge</strong>:" . $port->code;
                }
            }

            $filter = $query->get();
            $filter_bool = true;
        } else {
            $filter = [];
        }

        if($filter_bool){
            if(count($filter) > 0){
                $msg = count($filter) . " records found.<br>" . $msg;
                Session::flash("class", "alert-success");
                Session::flash("msg", $msg);
                // $request->session()->flash("class", "alert-success");
                // $request->session()->flash("msg", $msg);
            } else {
                $msg = "No records found.<br>" . $msg;
                // $request->session()->flash("class", "alert-danger");
                // $request->session()->flash("msg", $msg);
                Session::flash("class", "alert-danger");
                Session::flash("msg", $msg);
            }
        }

        return view("print.index-manifest", compact("vessels", "shippers", "ports", "bls", "filter"));
    }

    public function print_manifest2(){
        return view("print.print-manifest2");
    }

    public function index_bl(Request $request){
        $vessels = Vessel::get();

        $filter_vessel = $request->query('vessel', null);

        $filter_start_date = $request->query('start_date', null);
        $filter_end_date = $request->query('end_date', null);

        $filter = false;

        $filter_str = "";

        if($filter_vessel != null){
            $filter_str .= " AND vessel_id = " . $filter_vessel;
        }

        if($filter_start_date != null){
            $start_date = Carbon::createFromFormat("d/m/Y", $filter_start_date, 'Asia/Kuala_Lumpur')->format('Y-m-d');
            if($filter_end_date != null){
                $end_date = Carbon::createFromFormat("d/m/Y", $filter_end_date, 'Asia/Kuala_Lumpur')->format('Y-m-d');

                $filter_str .= " AND voyages.eta_pol >= '" . $start_date . "' AND voyages.eta_pol <= '" . $end_date . "'";
            }
        }

        //Updated to show voyage that is before ATD or with unset ATD 20/2/2019
        $voyages = Voyage::hydrate(DB::select('SELECT * FROM (SELECT max(id), voyage_id, eta FROM voyage_destinations WHERE atd <= CURDATE() OR atd >= CURDATE() OR atd IS NULL GROUP BY voyage_id) v_d LEFT JOIN voyages ON v_d.voyage_id = voyages.id WHERE deleted_at IS NULL ' . $filter_str . ' ORDER BY voyages.eta_pol DESC'));

        //Retrieve voyages from inward BL
        $inw_voyages = InwardBL::groupBy("voyage")->get();

        return view("print.index-bl", compact("vessels", "voyages", "inw_voyages"));
    }

    public function get_bl($voyage_id){
        $bls = BillLading::where("voyage_id", $voyage_id)->orderBy('pod_id', 'ASC')->get();
        if(!empty($bls)){
            foreach($bls AS $bl){
                $bl->destination = $bl->pod->code;
            }

            return response()->json(['success' => true, "bls" => $bls, "hashed" => Hashids::encode($voyage_id)]);
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function get_bl_inw($voyage_name){
        $bls = InwardBL::where("voyage", $voyage_name)->get();
        if(!empty($bls)){
            return response()->json(['success' => true, 'bls' => $bls]);
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function view_print_bl(Request $request){
        $bl_format = $request->txt_blformat;

        if(empty($request->chk_bl)){
            $request->chk_bl = json_decode($request->bl_id);
        }

        //plain or form format
        $format_type = $request->format_type;

        $company_name1 = $request->txt_companyname1;
        $company_name2 = $request->txt_companyname2;

        $blArr = BillLading::whereIn('id', $request->chk_bl)->get();

        $bl_id = json_encode($request->chk_bl);

        $format = "";

        switch($bl_format){
            case "apollo":
            $format = "print.apollo-pdf";
            $page_num_height = 580;
            break;

            case "malsuria":
            $page_num_height = 560;
            if(!empty($company_name1) || !empty($company_name2)){
                // $format = "print.malsuria-pdf";
                $format = "print.shinyang-pdf";
            } else {
                if(env("SITE") == "TEGAS"){
                    $format = "print.tegasshinyang-pdf";
                } else {
                    $format = "print.shinyang-pdf";
                }
            }
            break;
        }

        $files = [];

        $from = null;

        $blArr = $this->sortBL($blArr);

        // Merge the same POD id
        $podPdfMergerArr = [];
        foreach($blArr AS $bl){
            $podPdfMerger = null;
            $podCode = $bl->pod->code;
            if (array_key_exists($podCode, $podPdfMergerArr)) {
                // Use the same merger
                $podPdfMerger = $podPdfMergerArr[$podCode];
            } else {
                // Create new merger
                $podPdfMerger = new PdfManage();
                $podPdfMerger->init();

                $podPdfMergerArr[$podCode] = $podPdfMerger;
            }

            $filename = "/bl_export/BL_" . $bl->bl_no .  '.pdf';
            $pdf = \PDF::loadView($format, compact("bl", "company_name1", "company_name2", "from", "format_type"));
            $pdf->output();

            //Add page number
            $dompdf = $pdf->getDomPDF();
            $canvas = $dompdf->get_canvas();
            // $canvas->page_text(100, $page_num_height, "Page {PAGE_NUM} of {PAGE_COUNT}", '', 9, array(0, 0, 0));

            $pdf = $pdf->save($_SERVER['DOCUMENT_ROOT'] . $filename);
            $bl->filename = $filename;

            array_push($files, $filename);

            // Merge POD pdf
            $podPdfMerger->addPDF($_SERVER['DOCUMENT_ROOT'] . $filename, 'all', 'L');
        }

        $merged_filename = null;

        //Merge all pdf into one if more than one BL
        if(count($files) > 1){
            $pdf_merge = PDFMerger::init();
            foreach($files AS $file){
                $pdf_merge->addPDF($_SERVER['DOCUMENT_ROOT'] . $file, 'all', 'L');
            }

            $merged_filename = str_random(10);

            $pdf_merge->merge();
            $pdf_merge->save($_SERVER['DOCUMENT_ROOT'] . '/bl_export/' . $merged_filename . ".pdf");
        }

        // Merge based on POD
        $podPdfs = [];
        foreach ($podPdfMergerArr as $key => $podPdfMerger) {
            $podFileName = '/bl_export/' . implode('_', array($merged_filename, $key)) . '.pdf';

            $podPdfMerger->merge();
            $podPdfMerger->save($_SERVER['DOCUMENT_ROOT'] . $podFileName);

            $podPdfs[$key] = $podFileName;
        }

        return view('bl.print-bl', compact("bl_format", "company_name1", "company_name2", "bl_id", 'blArr', 'merged_filename', 'podPdfs'));
    }

    public function view_print_so(Request $request){
        if(empty($request->chk_bl)){
            $request->chk_bl = json_decode($request->bl_id);
        }

        $from = $request->from;

        $blArr = BillLading::whereIn('id', $request->chk_bl)->get();

        $bl_id = json_encode($request->chk_bl);

        $format_type = $request->format_type;

        foreach($blArr AS $bl){
            $pdf = \PDF::loadView("print.so-pdf", compact("bl", "format_type"));

            $filename = "/so_export/SO_" . $bl->bl_no .  '.pdf';

            $pdf->save($_SERVER['DOCUMENT_ROOT'] . $filename);
            $bl->filename = $filename;

            // $pdf->output();

            //Add page number
            // $dompdf = $pdf->getDomPDF();
            // $canvas = $dompdf->get_canvas();
            // $canvas->page_text(540, $page_num_height, "Page {PAGE_NUM} of {PAGE_COUNT}", '', 9, array(0, 0, 0));
        }

        $merged_filename = null;

        //Merge all pdf into one if more than one BL
        if(count($blArr) > 1){
            $pdf_merge = PDFMerger::init();
            foreach($blArr AS $bl){
                $pdf_merge->addPDF($_SERVER['DOCUMENT_ROOT'] . $bl->filename, 'all');
            }

            $merged_filename = str_random(10);

            $pdf_merge->merge();
            $pdf_merge->save($_SERVER['DOCUMENT_ROOT'] . '/so_export/' . $merged_filename . ".pdf");
        }

        return view("bl.print-so", compact("blArr", "bl_id", "from", "merged_filename"));
    }

    public function get_cargo($id){
        $bl = BillLading::find($id);
        $cargos = $bl->getCargos;

        return response()->json([
            'success' =>  true,
            'cargos' => $cargos,
            'weight' => number_format($cargos->sum('weight'), 3),
            'volume' => number_format($cargos->sum('volume'), 3)
        ]);
    }

    public function get_container($cargo_id){
        $containers = CargoDetail::where('cargo_id', $cargo_id)->get();
        $containers->groupBy('cargo_id');
        return response()->json(['success' => true, 'containers' => $containers]);
    }

    public function index_voyage(Request $request){

        $filter_start_date = $request->query('start_date', null);
        $filter_end_date = $request->query('end_date', null);
        $filter_status = $request->query('status', null);

        $filter_bool = false;

        $query = Voyage::query();

        // if($filter_status != null){

        //     switch ($filter_status) {
        //         case '2':
        //             $query = $query->where("")
        //             break;

        //         default:
        //             $query = $query->where("booking_status", $filter_status);
        //             break;
        //     }

        //     $filter_bool = true;
        // }

        if($filter_start_date != null){
            $start_date = Carbon::createFromFormat("d/m/Y", $filter_start_date, 'Asia/Kuala_Lumpur')->format('Y-m-d');
            if($filter_end_date != null){
                $end_date = Carbon::createFromFormat("d/m/Y", $filter_end_date, 'Asia/Kuala_Lumpur')->format('Y-m-d');

                $filter_bool = true;

                $query = $query->whereDate('eta_pol', '>=', $start_date)->whereDate('eta_pol', '<=', $end_date);
            }
        }

        if($filter_bool){
            $voyages = $query->orderBy("eta_pol", "ASC")->get();
        } else {
            // $voyages = Voyage::where("eta_pol", ">=", Carbon::now()->format('Y-m-d 00:00:00'))->get();
            $voyages = Voyage::orderBy("eta_pol", "ASC")->get();
            foreach($voyages AS $key => $voyage){
                if($voyage->lastDestination[0]->eta <= Carbon::now()->format('Y-m-d 00:00:00')){
                    unset($voyages[$key]);
                }
            }
        }

        $now = Carbon::now()->format("Y-m-d 00:00:00");

        $company = Company::where("type", 4)->first();

        return view("print.index-voyage", compact("voyages", "now", "company"));
    }

    public function view_print_voyage(Request $request){
        $filter_start_date = $request->query('start_date', null);
        $filter_end_date = $request->query('end_date', null);

        $query = Voyage::query();

        if($filter_start_date != null){
            $start_date = Carbon::createFromFormat("d/m/Y", $filter_start_date, 'Asia/Kuala_Lumpur')->format('Y-m-d');
            if($filter_end_date != null){
                $end_date = Carbon::createFromFormat("d/m/Y", $filter_end_date, 'Asia/Kuala_Lumpur')->format('Y-m-d');

                $query = $query->whereDate('eta_pol', '>=', $start_date)->whereDate('eta_pol', '<=', $end_date);
            }
        }

        $voyages = $query->get();

        return view("print.print-voyage", compact("voyages", "now"));
    }

    public function index_malsuria(){
        $bl = BillLading::find(2);
        $company_name1 = "test";
        $company_name2 = "test2";
        return view("print.malsuria", compact("bl", "company_name1", "company_name2"));
    }

    public function index_shinyang(){
        $bl = BillLading::find(2);
        $company_name1 = "test";
        $company_name2 = "test2";
        return view("print.shinyang", compact("bl", "company_name1", "company_name2"));
    }

    public function index_cargostatus(){
        $vessels = Vessel::get();
        return view("print.index-cargostatusreport", compact("vessels"));
    }

    public function get_cargostatus($voyage_id){
        try {
            $voy = Voyage::findOrFail($voyage_id);

            $ports = $voy->destinations->keyBy("port");

            foreach($ports AS $port_id => $dest){
                //Get reservation made under this voyage no. and port of departure
                $dest->res_weight = Reservation::where("voyage_id", $voyage_id)->where("pod_id", $port_id)->sum("weight");

                //Get BL created with booking number under this voyage no. and port of departure
                $dest->bc_bl_weight = DB::select("SELECT SUM(weight) AS weight FROM ( SELECT id FROM bill_ladings WHERE deleted_at IS NULL AND voyage_id = " . $voyage_id . " AND fpd_id = " . $port_id . " AND bc_no IS NOT NULL) a INNER JOIN bill_cargos ON a.id = bill_cargos.bl_id")[0]->weight;

                //Get BL without booking number under this voyage no. and port of departure
                $dest->bl_weight = DB::select("SELECT SUM(weight) AS weight FROM ( SELECT id FROM bill_ladings WHERE deleted_at IS NULL AND voyage_id = " . $voyage_id . " AND fpd_id = " . $port_id . " AND bc_no IS NULL) a INNER JOIN bill_cargos ON a.id = bill_cargos.bl_id")[0]->weight;
            }

            return response()->json(['success' => true, 'cargo_status' => $this->build_cargoreport($voy, $ports)]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'msg', $e->getMessage()]);
        }
    }

    public function build_cargoreport($voyage, $ports){
        try {
            $html = "<table class='table table-bordered text-black table-cargoreport'>" .
            "<tr>" .
            "<th></th>" .
            "<th>Reserved</th>" .
            "<th>BL with BC</th>" .
            "<th>BL</th>" .
            "<th>Total</th>" .
            "</tr>";

            foreach($ports AS $port){
                $port->sum_weight = (float)$port->res_weight + (float)$port->bc_bl_weight + (float)$port->bl_weight;

                $html .= "<tr>" .
                "<td>" . $port->getPort->location . "</td>" .
                "<td>" . number_format($port->res_weight, 3) . " MT</td>" .
                "<td>" . number_format($port->bc_bl_weight, 3) . " MT</td>" .
                "<td>" . number_format($port->bl_weight, 3) . " MT</td>" .
                "<td>" . number_format($port->sum_weight, 3) . " MT</td>" .
                "</tr>";

            }

            //Add cargo status percentage in the front
            $html = "<div><label class='f-w-700 text-underline text-uppercase mb-3'>" . $voyage->vessel->name . " / " . $voyage->voyage_id . "</label>" .
            "<button data-voyage='" . $voyage->id . "' type='button' class='btn btn-danger btn-lg btn-delete pull-right hidden-print'><i class='fa fa-trash'></i></button>" .
            "<p class='mb-0'>Vessel DWT : <strong>" . number_format($voyage->vessel->dwt) . " MT</strong></p>" .
            "<p>Cargo Status : <strong>" . number_format(($ports->sum("sum_weight") / $voyage->vessel->dwt) * 100, 1) . "%</strong></p>" .
            $html .
            "<tr>" .
            "<td colspan='4'></td>" .
            "<td>" . number_format($ports->sum("sum_weight"), 3) . " MT</td>" .
            "</tr>" .
            "</table></div>";

            return $html;
        } catch (\Exception $e) {
            return "";
        }
    }

    public function print_freightlist(Request $request){
        try {
            $query = $request->query('bl');

            if($query){
                $bl_arr = explode(",", $query);

                $bls = BillLading::whereIn("id", $bl_arr)->get();
                $voyage = Voyage::find($bls[0]->voyage_id);

                 //Group port of discharge (FPD) together
                $grouped = $bls->groupBy('fpd_id');

                $ports = [];

                $base_company = Company::where("type", 4)->first();

                foreach($grouped AS $key => $bill_ladings){

                    $total_cargo = 0;

                    $total_weight_mt = $bill_ladings->reduce(function ($carry, $item) {
                        return $carry + $item->getCargos->where('weight_unit', 'MT')->sum('weight');
                    });

                    $total_weight_kg = $bill_ladings->reduce(function ($carry, $item) {
                        return $carry + $item->getCargos->where('weight_unit', 'KG')->sum('weight');
                    });

                    $total_volume = $bill_ladings->reduce(function ($carry, $item) {
                        return $carry + $item->getCargos->sum('volume');
                    });

                    /**
                     * If FCL then calculate total cargo by 20" 40",
                     * If LCL then calculate total cargo by total cargo_qty
                     */
                    if($bill_ladings[0]->vessel_type == "FCL"){

                        $total_cargo_20 = 0;
                        $total_cargo_40 = 0;

                        foreach($bill_ladings AS $bl){
                            foreach($bl->getCargos AS $cargo){
                                $all_cargo = CargoDetail::where("cargo_id", $cargo->id)->get();
                                $total_cargo_20 += $all_cargo->where('size', 20)->count();
                                $total_cargo_40 += $all_cargo->where('size', 40)->count();
                            }
                        }

                        $total_cargo = [$total_cargo_20, $total_cargo_40];
                    } else {
                        $total_cargo = $bill_ladings->reduce(function ($carry, $item) {
                            return $carry + $item->getCargos->sum('cargo_qty');
                        });
                    }

                    $total_price_p = $bill_ladings->reduce(function ($carry, $item) {
                        return $carry + round($item->getSumCharges("P"), 2);
                    });

                    $total_price_c = $bill_ladings->reduce(function ($carry, $item) {
                        return $carry + round($item->getSumCharges("C"), 2);
                    });

                    $total_amount = $total_price_p + $total_price_c;

                    $bill_ladings = $this->sortBL($bill_ladings);

                    $filename = "/freightlist_export/freightlist_" . Carbon::now()->format("dmYhis") . $key . '.pdf';

                    $pdf = \PDF::loadView("print.freight-list-pdf", compact("bill_ladings", "voyage", "base_company", "total_weight_mt", "total_weight_kg", "total_volume", "total_cargo", "total_amount", 'total_price_p', 'total_price_c'));
                    $pdf->output();

                    //Add page number
                    $dompdf = $pdf->getDomPDF();
                    $canvas = $dompdf->get_canvas();
                    $canvas->page_text(750, 20, "Page {PAGE_NUM} of {PAGE_COUNT}", '', 9, array(0, 0, 0));

                    $pdf = $pdf->save($_SERVER['DOCUMENT_ROOT'] . $filename);

                    $port = Port::find($bill_ladings[0]->fpd_id);

                    $ports[$port->code] = $filename;
                }


                $merged_filename = null;

                //Merge all pdf into one if more than one Freight List
                if(count($ports) > 1){
                    $pdf_merge = PDFMerger::init();
                    foreach($ports AS $file){
                        $pdf_merge->addPDF($_SERVER['DOCUMENT_ROOT'] . $file, 'all');
                    }

                    $merged_filename = str_random(10);

                    $pdf_merge->merge();
                    $pdf_merge->save($_SERVER['DOCUMENT_ROOT'] . '/freightlist_export/' . $merged_filename . ".pdf");
                }

                // return $pdf->stream();

                return view("print.index-freightlist", compact("ports", "merged_filename"));
            }
        } catch (\Exception $e) {
            Session::flash("msg", "An error has occured. " . $e->getMessage());
            Session::flash("class", "alert-danger");
            dd($e->getMessage());
            return redirect::back();
        }
    }

    public function print_manifest(Request $request){
        try {
            $query = $request->query('bl');

            if($query){
                $bl_arr = explode(",", $query);

                $bls = BillLading::whereIn("id", $bl_arr)->get();
                $voyage = Voyage::find($bls[0]->voyage_id);

                foreach($bls AS $bl){
                    if(count($bl->getCargos) > 0){
                        $bl->totallen = 2 + substr_count($bl->getCargos[0]->detailed_desc, "\n" );
                    }
                }

                //Group port of discharge (FPD) together
                $grouped = $bls->groupBy('fpd_id');

                $ports = [];

                $base_company = Company::where("type", 4)->first();

                foreach($grouped AS $key => $bill_ladings){

                    $total_weight_mt = $bill_ladings->reduce(function ($carry, $item) {
                        return $carry + $item->getCargos->where('weight_unit', 'MT')->sum('weight');
                    });

                    $total_weight_kg = $bill_ladings->reduce(function ($carry, $item) {
                        return $carry + $item->getCargos->where('weight_unit', 'KG')->sum('weight');
                    });

                    $total_volume = $bill_ladings->reduce(function ($carry, $item) {
                        return $carry + $item->getCargos->sum('volume');
                    });

                    if($bill_ladings[0]->vessel_type == "FCL"){

                        $total_cargo_20 = 0;
                        $total_cargo_40 = 0;

                        foreach($bill_ladings AS $bl){
                            foreach($bl->getCargos AS $cargo){
                                $all_cargo = CargoDetail::where("cargo_id", $cargo->id)->get();
                                $total_cargo_20 += $all_cargo->where('size', 20)->count();
                                $total_cargo_40 += $all_cargo->where('size', 40)->count();
                            }
                        }

                        $total_cargo = [$total_cargo_20, $total_cargo_40];
                    } else {
                        $total_cargo = $bill_ladings->reduce(function ($carry, $item) {
                            return $carry + $item->getCargos->sum('cargo_qty');
                        });
                    }

                    $bill_ladings = $this->sortBL($bill_ladings);

                    $filename = "/manifest_export/manifest_" . Carbon::now()->format("dmYhis") . '_' . $key . '.pdf';

                    $pdf = \PDF::loadView("print.manifest-pdf", compact("bill_ladings", "base_company", "total_weight_mt", "total_weight_kg", "total_volume", "total_cargo"));

                    $pdf->output();

                    //Add page number
                    $dompdf = $pdf->getDomPDF();
                    $canvas = $dompdf->get_canvas();
                    $canvas->page_text(750, 20, "Page {PAGE_NUM} of {PAGE_COUNT}", '', 9, array(0, 0, 0));

                    $pdf = $pdf->save($_SERVER['DOCUMENT_ROOT'] . $filename);

                    $port = Port::find($bill_ladings[0]->fpd_id);

                    $ports[$port->code] = $filename;
                }

                $merged_filename = null;

                //Merge all pdf into one if more than one Freight List
                if(count($ports) > 1){
                    $pdf_merge = PDFMerger::init();
                    foreach($ports AS $file){
                        $pdf_merge->addPDF($_SERVER['DOCUMENT_ROOT'] . $file, 'all', 'L');
                    }

                    $merged_filename = str_random(10);

                    $pdf_merge->merge();
                    $pdf_merge->save($_SERVER['DOCUMENT_ROOT'] . '/manifest_export/' . $merged_filename . ".pdf");
                }

                // return $pdf->stream();
                return view("print.index-manifest", compact("ports", "merged_filename"));
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
            Session::flash("msg", "An error has occured. " . $e->getMessage());
            Session::flash("class", "alert-danger");
            return redirect::back();
        }
    }

    private function sortBL($bill_ladings){
        // Sort bl first before generating pdf
        foreach($bill_ladings AS $bl){
            // Remove prefix and year from bl_no as sort_id for sorting ascending
            if($bl->vessel_type == "LCL"){
                $str = substr($bl->bl_no, strpos($bl->bl_no, '1')+1);

                // Remove year digit at the end
                $bl->sort_id = (int)substr($str, 0, -1);
            } else {
                $str = substr($bl->bl_no, strcspn($bl->bl_no, '0123456789'));
                $bl->sort_id = $str;
            }
        }
        return $bill_ladings->sortBy('sort_id', SORT_NATURAL);
    }

    public function print_manifest_inw(Request $request){
        try {
            $query = $request->query('bl');

            if($query){
                $bl_arr = explode(",", $query);

                $bls = InwardBL::whereIn("id", $bl_arr)->get();

                foreach($bls AS $bl){
                    $bl->totallen = substr_count($bl->detailed_desc, "\n" );
                }

                //Group port of discharge (FPD name) together
                $grouped = $bls->groupBy('fpd_name');

                $ports = [];

                foreach($grouped AS $key => $bill_ladings){
                    $filename = "/inw_manifest_export/inw_manifest_" . Carbon::now()->format("dmYhis") . '_' . $key . '.pdf';

                    $pdf = \PDF::loadView("print.manifest-pdf", compact("bill_ladings"));

                    $pdf->output();

                    //Add page number
                    $dompdf = $pdf->getDomPDF();
                    $canvas = $dompdf->get_canvas();
                    $canvas->page_text(750, 20, "Page {PAGE_NUM} of {PAGE_COUNT}", '', 9, array(0, 0, 0));

                    $pdf = $pdf->save($_SERVER['DOCUMENT_ROOT'] . $filename);
                    $ports[$bill_ladings[0]->fpd_name] = $filename;
                }

                return view("print.index-manifest-inw", compact("bls", "ports"));
            } else {
                Session::flash("msg", "Please select a BL to view in manifest format!");
                Session::flash("class", "alert-danger");
                return redirect::back();
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
            Session::flash("msg", "An error has occured. " . $e->getMessage());
            Session::flash("class", "alert-danger");
            return redirect::back();
        }
    }
}
