<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Redirect;

use \App\Company;
use \App\Voyage;
use \App\VoyageDestinations;
use \App\Port;
use \App\Reservation;
use \App\Vessel;
use \App\Shipper;

class ReservationController extends Controller
{
	public function index(Request $request){
		$query = $request->query("voyage", null);

		if(!empty($query)){
			$voy = Voyage::find($query);

			if(!empty($voy)){
				$voy_text = $voy->voyage_id . "/" . $voy->vessel->name;
				$allow = $voy->allow = $voy->vessel->dwt - $voy->totalUsedWeight();
			} else {
				$voy_text = "";
				$query = null;
			}
		} else {
			$voy_text = "";
			$allow = null;
		}

		//Retrieve all vessel ID with GCV
		$vessel_id_arr = Vessel::where("default_shipment", "LCL")
		->get()->pluck("id");

		$shippers = Company::where("type", Company::type_shipper)->orWhere("type", Company::type_shipperconsignee)->orderBy("name", "ASC")->get();

		$voyages = Voyage::where("booking_status", 0)
		->whereIn("vessel_id", $vessel_id_arr)
		->where("eta_pol", ">=", Carbon::now()->format('Y-m-d 00:00:00'))
		->orderBy("eta_pol", "ASC")->get();

		$ports = Port::get();

		//Showing voyage that POL don't have ATD / yet to sail
		$reservations = Reservation::leftJoin("voyages", "reservations.voyage_id", "voyages.id")->where("atd_pol", ">=", Carbon::now()->format('Y-m-d 00:00:00'))->orWhere("atd_pol", null)->orderBy("date", "DESC")->select("reservations.*")->get();

		//Retrieve cancelled reservation that the assigned voyage is not deleted
		$cancelled = Reservation::onlyTrashed()->join("voyages", "reservations.voyage_id", "voyages.id")
		->where("voyages.deleted_at", null)
		->orderBy("reservations.deleted_at", "DESC")
		->select("reservations.*")
		->get();

		return view("reservation.index-reservation", compact("shippers", "voyages", "ports", "reservations", "cancelled", "voy_text", "query", "allow"));
	}

	public function getReservationDetail($id){
		$res = Reservation::find($id);
		$booking_no = $res->getBooking()->pluck('booking_no');

		return response()->json([
			'success' => true, 
			'booking_no' => implode("<br>", $booking_no->toArray()), 
			'remarks' => (!empty($res->remarks) ? $res->remarks : '')]
		);
	}

	public function get_voyage($pol, $pod, $date, Request $request){
		$search_date = Carbon::createFromFormat('d-m-Y', $date)->format('Y-m-d 00:00:00');

		// $voyages = Voyage::where("booking_status", 0)
		// ->join("voyage_destinations", "voyages.id", "voyage_destinations.voyage_id")
		// ->where(function ($q) use ($pol, $pod) {
		// 	return $q->where('voyage_destinations.port', $pol)->orWhere('voyage_destinations.port', $pod);
		// })
		// ->where("voyage_destinations.eta", ">=", $search_date)
		// ->groupBy("voyage_destinations.port")
		// ->join("ports", "ports.id", "voyage_destinations.port")
		// ->join("vessels", "vessels.id", "voyages.vessel_id")
		// ->select("voyages.id", "voyages.voyage_id", "vessels.name", "voyages.eta_pol", "vessels.dwt")
		// ->orderBy("eta_pol", "ASC")
		// ->get();

		$voyages = Voyage::where("booking_status", 0);

		//If has reservation, check if voyage is really fully booked or transferring from reserved weight to booking weight
		$res_query = $request->query("reservation", null);

		if(!empty($res_query)){
			$res = Reservation::find($res_query);
			if(!empty($res->voyage_id)){
				$voyages = $voyages->orWhere("id", $res->voyage_id);
			}
		}

		$voyages = $voyages->get();

		//Retrieve voyage that has both POL and POD and POL ETA is before $search_date
		$voyages = $voyages->filter(function ($value, $key) use ($pol, $pod, $search_date){
			if($value->getValidVoyage($pol, $pod, $search_date)){
				return $value;
			}
		});

		if(count($voyages) > 0){
			$voyArr = [];
			foreach($voyages AS $voy){

				$voy->dwt = $voy->vessel->dwt;
				$voy->name = $voy->vessel->name;
				$voy->allow = $voy->vessel->dwt - $voy->totalUsedWeight();
				$voy->used = $voy->totalUsedWeight();

				$dest_arr = [];
				$destinations = $voy->destinations;
				foreach($destinations AS $key => $dest){
					array_push($dest_arr, $dest->getPort->code);
				}

				//Select temp POL based on $pol parameter (search pol will be temp pol)
				$temp_pol = $destinations->where("port", $pol)->first();

				//Append temp POL code into ETA
				$voy->eta_text = $temp_pol->eta->format("d/m/Y");

				$voy->eta = $voy->eta_pol->format("d/m/Y");
				$voy->fpd_eta = $voy->lastDestination()->first()->eta->format('d/m/Y');

				//For booking ETA POD use
				$voy->pod_eta = $voy->getPort($pod)->eta->format('d/m/Y');

				$voy->port_destinations = implode(" > ", $dest_arr);

				//Convert collection of object to array
				array_push($voyArr, $voy);
			}

			return response()->json(['success' => true, 'voyages' => $voyArr]);
		} else {
			return response()->json(['success' => false]);
		}
	}

	public function get_voyage1($voy_id, $pol, $pod, $date, Request $request){
		$search_date = Carbon::createFromFormat('d-m-Y', $date)->format('Y-m-d 00:00:00');
		$voyages = Voyage::where("booking_status", 0)->orWhere("id", $voy_id);

		//If has reservation, check if voyage is really fully booked or transferring from reserved weight to booking weight
		$res_query = $request->query("reservation", null);

		if(!empty($res_query)){
			$res = Reservation::find($res_query);
			if(!empty($res->voyage_id)){
				$voyages = $voyages->orWhere("id", $res->voyage_id);
			}
		}

		$voyages = $voyages->get();

		//Retrieve voyage that has both POL and POD and POL ETA is before $search_date
		$voyages = $voyages->filter(function ($value, $key) use ($pol, $pod, $search_date){
			if($value->getValidVoyage($pol, $pod, $search_date)){
				return $value;
			}
		});

		if(count($voyages) > 0){
			$voyArr = [];
			foreach($voyages AS $voy){

				$voy->dwt = $voy->vessel->dwt;
				$voy->name = $voy->vessel->name;
				$voy->allow = $voy->vessel->dwt - $voy->totalUsedWeight();
				$voy->used = $voy->totalUsedWeight();

				$dest_arr = [];
				$destinations = $voy->destinations;
				foreach($destinations AS $key => $dest){
					array_push($dest_arr, $dest->getPort->code);
				}

				//Select temp POL based on $pol parameter (search pol will be temp pol)
				$temp_pol = $destinations->where("port", $pol)->first();

				//Append temp POL code into ETA
				$voy->eta_text = $temp_pol->eta->format("d/m/Y");

				$voy->eta = $voy->eta_pol->format("d/m/Y");
				$voy->fpd_eta = $voy->lastDestination()->first()->eta->format('d/m/Y');

				//For booking ETA POD use
				$voy->pod_eta = $voy->getPort($pod)->eta->format('d/m/Y');

				$voy->port_destinations = implode(" > ", $dest_arr);

				//Convert collection of object to array
				array_push($voyArr, $voy);
			}

			return response()->json(['success' => true, 'voyages' => $voyArr]);
		} else {
			return response()->json(['success' => false]);
		}
	}

	public function store(Request $request){
		try {
			$res = new Reservation();
			$res->date = Carbon::now();

			$shipper = Shipper::find($request->txt_shipper);
			// $res->shipper_id = $request->txt_shipper;
			$res->shipper_id = $shipper->company_id;

			$res->voyage_id = $request->txt_voyage;
			$res->weight = $request->txt_weight;
			$res->remarks = $request->txt_remarks;
			$res->pod_id = $request->txt_pod != 'null' ? $request->txt_pod : 0;
			$res->save();

			VoyageController::checkCargo($res->voyage_id);

			Session::flash("class", "alert-success");
			Session::flash("msg", "Successfully created reservation");

			return redirect::action("ReservationController@index");
		} catch (\Exception $e) {
			Session::flash("class", "alert-danger");
			Session::flash("msg", "Unable to create reservation. " . $e->getMessage());
			return redirect::action("ReservationController@index");
		}
	}

	public function get_reservation($id){
		$res = Reservation::find($id);
		if(!empty($res)){
			$res->date_text = $res->date->format("d/m/Y");
			if(!empty($res->voyage_id)){
				$res->intended = $res->getVoyage->vessel->name . " / " . $res->getVoyage->voyage_id;
			} else {
				$res->intended = "";
			}
			if($res->voyage_id){
				$res->allow = (int)$res->getVoyage->vessel->dwt - (int)$res->getVoyage->totalUsedWeight();
			} else {
				$res->allow = 0;
			}
			return response()->json(['success' => true, 'reservation' => $res]);
		} else {
			return response()->json(['success' => false]);
		}
	}

	public function update($id, Request $request){
		$res = Reservation::find($id);
		if(!empty($res)){
			// $res->date = Carbon::createFromFormat('d/m/Y', $request->txt_date)->format('Y-m-d 00:00:00');
			$res->date = Carbon::now();

			$shipper = Shipper::find($request->txt_shipper);
			$res->shipper_id = $shipper->company_id;


			$res->weight = $request->txt_weight;
			$res->remarks = $request->txt_remarks;
			$res->pod_id = $request->txt_edit_pod != 'null' ? $request->txt_edit_pod : 0;

			$old_voyage = $res->voyage_id;

			$res->voyage_id = ($request->txt_voyage) ? $request->txt_voyage : null;
			$res->save();

			if($res->voyage_id == null){
				$voy_id = $old_voyage;
			} else {
				$voy_id = $res->voyage_id;
			}

			VoyageController::checkCargo($voy_id);

			Session::flash("class", "alert-success");
			Session::flash("msg", "Successfully updated reservation.");
		} else {
			Session::flash("class", "alert-danger");
			Session::flash("msg", "Unable to update reservation.");
		}

		return redirect::back();
	}

	public function delete($id){
		$res = Reservation::find($id);
		$res->delete();

		VoyageController::checkCargo($res->voyage_id);

		Session::flash("class", "alert-success");
		Session::flash("msg", "Successfully cancelled reservation");

		return redirect::back();
	}

}
