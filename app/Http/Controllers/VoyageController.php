<?php

namespace App\Http\Controllers;

use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Redirect;

// Eloquent Models
use App\Port;
use App\Vessel;
use App\Voyage;
use App\BillLading;
use App\VoyageDestinations;
use App\Booking;
use App\Reservation;

class VoyageController extends Controller
{
    //Check if cargo status is full
    public static function checkCargo($voy_id){
        $voyage = Voyage::find($voy_id);
        if(!empty($voyage)){
            if((int)$voyage->totalUsedWeight() >= (int)$voyage->vessel->dwt){
                //Cargo is full, update booking status to closed
                $voyage->booking_status = 1;
                $voyage->save();
            } else {
                //If removed weight from cargo, change status to OPEN again.
                if($voyage->booking_status == 1){
                    $voyage->booking_status = 0;
                    $voyage->save();
                }
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // abort(404);
        $ports = Port::all();
        $vessels = Vessel::all();
        // $voyages = Voyage::orderBy("eta_pol", "ASC")->get();

        $voyages = collect(DB::select('SELECT * FROM (SELECT max(id), voyage_id, eta FROM voyage_destinations WHERE atd IS NULL OR atd >= CURDATE() GROUP BY voyage_id) v_d LEFT JOIN voyages ON v_d.voyage_id = voyages.id WHERE deleted_at IS NULL ORDER BY eta ASC'));

        // \Log::channel('slack')->critical("'hello<br>hello'");

        $voyages = Voyage::whereIn("id", $voyages->pluck('id'))->get();

        foreach($voyages AS $voyage) {
            $vessel = Vessel::find($voyage->vessel_id);
            $pol = Port::find($voyage->pol_id);

            if($vessel) {
                $voyage->vessel_name = $vessel->name;
            } else {
                $voyage->vessel_name = '-';
            }

            if($pol) {
                $voyage->pol_code = $pol->code;
            } else {
                $voyage->pol_code = '-';
            }

            $voyage->dwt = $vessel->dwt;
        }

        Session::flash("filtered", "");

        return view('voyage.index-voyage', compact('voyages', 'ports', 'vessels'));
    }

    public function filter(Request $request)
    {
        try {
            $ports   = Port::all();
            $vessels = Vessel::all();

            $sql_query = 'SELECT * FROM voyages WHERE deleted_at IS NULL';

            $msg = '<b>Filtered results based on</b><br>';

            if($request->filled('filter_date_start') || $request->filled('filter_vessel') || $request->filled('filter_destination') || $request->filled('filter_pol')) {
                $msg .= '<table><tbody>';
            } else {
                $msg .= 'No filters';
            }

            if(!empty($request->filter_pol) ^ !empty($request->filter_destination)) {
                $filter_port = $request->filter_pol ?: $request->filter_destination;

                $port = Port::find($filter_port);
                $msg .= '<tr><td>Port of Loading</td><td style="padding: 0px 10px;">:</td><td>'.$port->name. ' ' . $port->code.'</td></tr>';

                $sql_query = 'SELECT id, voyages.voyage_id , vessel_id, eta_pol, ata_pol, etd_pol, atd_pol, pol_id, booking_status, closing_timestamp FROM (SELECT voyage_id, port FROM voyage_destinations WHERE eta > CURDATE() AND port = ' . $filter_port . ') vd INNER JOIN voyages ON voyages.id = vd.voyage_id ORDER BY eta_pol DESC';

                // if($request->filter_pol){
                //     $where_clause .= "AND (port = " . $request->filter_pol;
                //     $where_bool = true;
                // }

                // if(){
                    // if($where_bool){
                    //     $where_clause .= " OR ";
                    // } else {
                    //     $where_clause .= "(";
                    // }
                    // $where_clause .= "port = " . $request->filter_destination . ") AND eta > (SELECT eta FROM voyage_destinations WHERE port = " . $request->filter_destination . ") ";

                // }

                // $sql_query .= self::checkQuery($sql_query);
                // $sql_query .= 'pol_id = '.$request->filter_pol;

            } else if ($request->filter_pol && $request->filter_destination) {
                $sql_query = 'SELECT id, voyages.voyage_id, vessel_id, eta_pol, ata_pol, etd_pol, atd_pol, pol_id, booking_status, closing_timestamp FROM (SELECT a.voyage_id FROM (
                (SELECT * FROM voyage_destinations WHERE port = ' . $request->filter_pol . ') a
                JOIN
                (SELECT  * FROM voyage_destinations WHERE port = ' . $request->filter_destination . ') b ON a.voyage_id = b.voyage_id AND a.eta < b.eta)) c
                LEFT JOIN voyages ON c.voyage_id = voyages.id';
            }

            if($request->filter_date_start && $request->filter_date_end) {
                $sql_query .= self::checkQuery($sql_query);

                $date_start = Carbon::createFromFormat('d/m/Y', $request->filter_date_start)->format('Y-m-d 00:00:00');
                $date_end   = Carbon::createFromFormat('d/m/Y', $request->filter_date_end)->format('Y-m-d 23:59:59');
                $sql_query .= '(eta_pol >= "'.$date_start.'" AND eta_pol <= "'.$date_end.'")';

                $msg .= '<tr><td>Date</td><td style="padding: 0px 10px;">:</td><td>'.$request->filter_date_start.' to '.$request->filter_date_end.'</td></tr>';
            }

            if($request->filter_vessel) {
                $sql_query .= self::checkQuery($sql_query);
                $vessel = Vessel::find($request->filter_vessel);

                $sql_query .= 'vessel_id = '.$request->filter_vessel;

                if($vessel){
                    $msg .= '<tr><td>Vessel</td><td style="padding: 0px 10px;">:</td><td>'.$vessel->name.'</td></tr>';
                }
            }

            // if($request->filter_destination) {
            //     $sql_query .= self::checkQuery($sql_query);
            //     $destinations = VoyageDestinations::where('port', $request->filter_destination)->get();

            //     if($destinations->isNotEmpty()){
            //         $port = Port::find($destinations[0]->port);
            //         $msg .= '<tr><td>Destination</td><td style="padding: 0px 10px;">:</td><td>'.$port->name. ' ' . $port->code.'</td></tr>';

            //         $sql_query .= 'id IN (';

            //         foreach($destinations AS $key => $destination) {

            //             if($key > 0) {
            //                 $sql_query .= ', ';
            //             }

            //             $sql_query .= '"'.$destination->voyage_id.'"';
            //         }
            //         $sql_query .= ')';
            //     } else {
            //         $sql_query .= 'id = 0';
            //     }
            // }

            $voyages = DB::select($sql_query);

            foreach($voyages AS $key => $voyage) {

                $voy = Voyage::find($voyage->id);

                if($request->filter_pol) {
                    if($voy->lastDestination[0]->port == $request->filter_pol){
                        unset($voyages[$key]);
                        continue;
                    }
                }

                $vessel = Vessel::find($voyage->vessel_id);
                $pol = Port::find($voyage->pol_id);
            // $pod = Port::find($voyage->pod_id);

                if($vessel) {
                    $voyage->vessel_name = $vessel->name;
                } else {
                    $voyage->vessel_name = '-';
                }

                if($pol) {
                    $voyage->pol_code = $pol->code;
                } else {
                    $voyage->pol_code = '-';
                }

                // $booked_resv_weight = Booking::where("voyage_id", $voyage->id)->where("res_id", "!=", null)->sum("weight");

                $voyage->weight = $voy->totalUsedWeight();
                $voyage->bk_weight = $voy->getBookedWeight();
                $voyage->resv_weight = $voy->getReservedWeight();

                $voyage->dwt = $vessel->dwt;
                $voyage->default_shipment = $vessel->default_shipment;

            // if($pod) {
            //     $voyage->pod_code = $pod->code;
            // } else {
            //     $voyage->pod_code = '-';
            // }
            }

            $msg .= '</tbody></table>';
            $request->session()->flash('filtered', $msg);

            return view('voyage.index-voyage', compact('voyages', 'ports', 'vessels'));
        } catch (\Exception $e) {
            Session::flash("error", $e->getMessage());
            return redirect::action("VoyageController@index");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ports = Port::all();
        $vessels = Vessel::all();

        return view('voyage.create-voyage', compact('ports', 'vessels'));
    }

    public function validate_create($voyage_id = NULL, Request $request)
    {
        try {
            $pol_eta_time = Carbon::createFromFormat('d/m/Y', $request->pol_eta);

            $query = Voyage::where("voyage_id", $request->voyage_id)->where("vessel_id", $request->vessel_id);

            if($voyage_id){
                $query = $query->where("id", "!=", $voyage_id);
            }

            $voy_id = $query->first();

            if(!empty($voy_id)){
                return response()->json([
                    'success' => false,
                    'msg' => 'Voyage No. (<b>' . $request->voyage_id . '</b>) with the selected vessel already exists!'
                ]);
            } else {
               return response()->json(['success' => true]);
           }
       } catch (\Exception $e) {
        return response()->json(['succcess' => false, 'msg' => $e->getMessage()]);
    }
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $voyage = new Voyage;

            $voyage->voyage_id = $request->voyage_id;
            $voyage->vessel_id = $request->vessel_id;
            $voyage->scn = $request->scn;
            $voyage->booking_status = $request->booking_status;
            if($request->closing_timestamp) {
                $voyage->closing_timestamp = Carbon::createFromFormat('d/m/Y H:i:s', $request->closing_timestamp)->format('Y-m-d H:i:s');
            }
            $voyage->cancel_remark = $request->cancel_remark;
            $voyage->save();

            $ports = collect(json_decode($request->txt_portcalling));

            foreach($ports AS $port){
                $port->eta = Carbon::createFromFormat('d/m/Y', $port->eta)->format("Y-m-d 00:00:00");
            }

            $ports = $ports->sortBy("eta");

            $count = 0;

            foreach($ports AS  $port){
                if($count == 0){
                    $voyage->pol_id = $port->id;
                    $voyage->eta_pol = $port->eta;
                    $voyage->ata_pol = ($port->ata) ? Carbon::createFromFormat('d/m/Y', $port->ata)->format("Y-m-d 00:00:00") : null;
                    $voyage->etd_pol = Carbon::createFromFormat('d/m/Y', $port->etd)->format("Y-m-d 00:00:00");
                    $voyage->atd_pol = ($port->atd) ? Carbon::createFromFormat('d/m/Y', $port->atd)->format("Y-m-d 00:00:00") : null;
                    $voyage->save();
                }

                $destination = new VoyageDestinations;
                $destination->voyage_id = $voyage->id;
                $destination->port = $port->id;
                $destination->eta = $port->eta;
                $destination->ata = ($port->ata) ? Carbon::createFromFormat('d/m/Y', $port->ata)->format("Y-m-d 00:00:00") : null;
                $destination->etd = Carbon::createFromFormat('d/m/Y', $port->etd)->format("Y-m-d 00:00:00");
                $destination->atd = ($port->atd) ? Carbon::createFromFormat('d/m/Y', $port->atd)->format("Y-m-d 00:00:00") : null;
                $destination->save();

                $count++;
            }

            // for($i = 0; $i < count($request->port); $i++) {
            //     $details = explode(';', $request->port[$i]);

            //     if($i == 0) {
            //         $voyage->pol_id = $details[1];
            //         $voyage->eta_pol = Carbon::createFromFormat('d/m/Y', $details[2])->format('Y-m-d H:i:s');

            //         if($details[3] != '-') {
            //             $voyage->ata_pol = Carbon::createFromFormat('d/m/Y', $details[3])->format('Y-m-d H:i:s');
            //         }

            //         $voyage->etd_pol = Carbon::createFromFormat('d/m/Y', $details[4])->format('Y-m-d H:i:s');

            //         if($details[5] != '-') {
            //             $voyage->atd_pol = Carbon::createFromFormat('d/m/Y', $details[5])->format('Y-m-d H:i:s');
            //         }

            //         $voyage->save();
            //     }

            //     $destination = new VoyageDestinations;

            //     $destination->voyage_id = $voyage->id;
            //     $destination->port = $details[1];
            //     $destination->eta = Carbon::createFromFormat('d/m/Y', $details[2])->format('Y-m-d H:i:s');

            //     if($details[3] != '-') {
            //         $destination->ata = Carbon::createFromFormat('d/m/Y', $details[3])->format('Y-m-d H:i:s');
            //     }

            //     $destination->etd = Carbon::createFromFormat('d/m/Y', $details[4])->format('Y-m-d H:i:s');

            //     if($details[5] != '-') {
            //         $destination->atd = Carbon::createFromFormat('d/m/Y', $details[5])->format('Y-m-d H:i:s');
            //     }

            //     $destination->save();
            // }

            return redirect()->route('index-voyage')->with('success', 'Successfully added voyage.');
        } catch (\Exception $e) {
            Session::flash("msg", "Unable to create voyage. " . $e->getMessage());
            Session::flash("class", "alert-danger");
            return redirect::action("VoyageController@create");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($hash)
    {
        $ports = Port::all();
        $vessels = Vessel::all();

        // Decode hash to obtain voyage ID
        // Hashids::decode returns an array even though only one value being passed in
        $voyage_id = Hashids::decode($hash);

        if($voyage_id) {
            $voyage = Voyage::find($voyage_id[0]);
            return view('voyage.edit-voyage', compact('voyage', 'ports', 'vessels'));
        } else {
            return redirect()->route('index-voyage')->with('error', 'Voyage not found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $voyage = Voyage::find($id);

            $voyage->voyage_id = $request->voyage_id;
            $voyage->vessel_id = $request->vessel_id;
            $voyage->scn = $request->scn;
            $voyage->booking_status = $request->booking_status;

            if($request->closing_timestamp) {
                $voyage->closing_timestamp = Carbon::createFromFormat('d/m/Y H:i:s', $request->closing_timestamp)->format('Y-m-d H:i:s');
            } else {
                $voyage->closing_timestamp = null;
            }

            $voyage->cancel_remark = $request->cancel_remark;
            $voyage->save();

            $ports = collect(json_decode($request->txt_portcalling));

            foreach($ports AS $port){
                $port->eta = Carbon::createFromFormat('d/m/Y', $port->eta)->format("Y-m-d 00:00:00");
            }

            $ports = $ports->sortBy("eta");

            //Not to delete destinations ID
            $dest_id = [];

            $count = 0;

            foreach($ports AS $port){
                if($count == 0){
                    // dd($port);
                    $voyage->pol_id = $port->id;
                    $voyage->eta_pol = $port->eta;
                    $voyage->ata_pol = ($port->ata) ? Carbon::createFromFormat('d/m/Y', $port->ata)->format("Y-m-d 00:00:00") : null;
                    $voyage->etd_pol = Carbon::createFromFormat('d/m/Y', $port->etd)->format("Y-m-d 00:00:00");
                    $voyage->atd_pol = ($port->atd) ? Carbon::createFromFormat('d/m/Y', $port->atd)->format("Y-m-d 00:00:00") : null;

                    if($voyage->isDirty('etd_pol')){
                        // Change BL date to this POL ETD if BL is using this voyage
                        $bls = BillLading::where("voyage_id", $id)->where("pol_id", $port->id)->get();

                        foreach($bls AS $bl){
                            $bl->bl_date = $voyage->etd_pol;
                            $bl->save();
                        }
                    }

                    $voyage->save();
                }

                $destination = VoyageDestinations::where("voyage_id", $voyage->id)->where("port", $port->id)->first();
                if(empty($destination)){
                    $destination = new VoyageDestinations();
                    $destination->voyage_id = $voyage->id;
                    $destination->port = $port->id;
                }

                $destination->eta = $port->eta;

                $destination->ata = ($port->ata) ? Carbon::createFromFormat('d/m/Y', $port->ata)->format("Y-m-d 00:00:00") : null;
                $destination->etd = Carbon::createFromFormat('d/m/Y', $port->etd)->format("Y-m-d 00:00:00");
                $destination->atd = ($port->atd) ? Carbon::createFromFormat('d/m/Y', $port->atd)->format("Y-m-d 00:00:00") : null;
                $destination->save();

                array_push($dest_id, $destination->id);

                $count++;
            }

            //Check bookings made under this voyage, if have, update eta_pol & eta_fpd
            $booking = Booking::where("voyage_id", $id)->get();

            if(!empty($booking)){

                foreach($booking AS $bk){
                    $new_pol = VoyageDestinations::where("voyage_id", $id)->where("port", $bk->pol_id)->first();
                    $new_fpd = VoyageDestinations::where("voyage_id", $id)->where("port", $bk->pod_id)->first();

                    $bk->eta_pol = $new_pol->eta;
                    $bk->eta_fpd = $new_fpd->eta;
                    $bk->save();
                }
            }


            // for($i = 0; $i < count($request->port); $i++) {
            //     $details = explode(';', $request->port[$i]);

            //     if($i == 0) {
            //         $voyage->pol_id = $details[1];
            //         $voyage->eta_pol = Carbon::createFromFormat('d/m/Y', $details[2])->format('Y-m-d H:i:s');

            //         if($details[3] != '-') {
            //             $voyage->ata_pol = Carbon::createFromFormat('d/m/Y', $details[3])->format('Y-m-d H:i:s');
            //         } else {
            //             $voyage->ata_pol = null;
            //         }

            //         $voyage->etd_pol = Carbon::createFromFormat('d/m/Y', $details[4])->format('Y-m-d H:i:s');

            //         if($details[5] != '-') {
            //             $voyage->atd_pol = Carbon::createFromFormat('d/m/Y', $details[5])->format('Y-m-d H:i:s');
            //         } else {
            //             $voyage->atd_pol = null;
            //         }

            //         $voyage->save();
            //     }

            //     if($details[0] == -1) {
            //         $dest = VoyageDestinations::where('voyage_id', $voyage->id)->where('port', $details[1])->first();
            //         $dest->delete();
            //     } else {
            //         if($details[0] > 0) {
            //             $destination = VoyageDestinations::find($details[0]);
            //         } else {
            //             $destination = new VoyageDestinations;
            //         }

            //         $destination->voyage_id = $voyage->id;
            //         $destination->port = $details[1];
            //         $destination->eta = Carbon::createFromFormat('d/m/Y', $details[2])->format('Y-m-d H:i:s');

            //         if($details[3] != '-') {
            //             $destination->ata = Carbon::createFromFormat('d/m/Y', $details[3])->format('Y-m-d H:i:s');
            //         } else {
            //             $destination->ata = null;
            //         }

            //         $destination->etd = Carbon::createFromFormat('d/m/Y', $details[4])->format('Y-m-d H:i:s');

            //         if($details[5] != '-') {
            //             $destination->atd = Carbon::createFromFormat('d/m/Y', $details[5])->format('Y-m-d H:i:s');
            //         } else {
            //             $destination->atd = null;
            //         }

            //         $destination->save();
            //         array_push($dest_id, $destination->id);
            //     }
            // }

            //Delete other destinations
            VoyageDestinations::where("voyage_id", $id)->whereNotIn("id", $dest_id)->delete();

            DB::commit();

            return redirect()->route('index-voyage')->with('success', 'Successfully updated voyage '.$voyage->voyage_id.'.');
        } catch (\Exception $e) {
            DB::rollBack();
            Session::flash("msg", "Update voyage unsuccessful. " . $e->getMessage());
            Session::flash("class", "alert-danger");
            return redirect::back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $bl = BillLading::where('voyage_id', $request->voyage_id)->get();
        $resv = Reservation::where('voyage_id', $request->voyage_id)->get();
        $bk = Booking::where('voyage_id', $request->voyage_id)->get();

        if(count($bl) > 0) {
            return redirect::action("VoyageController@index")
            ->with("error", "Failed to delete voyage. The selected voyage is used in the following BL(s): <br> " . $bl->implode('bl_no', ", "));
        } else if(count($bk) > 0) {
            return redirect::action("VoyageController@index")
            ->with("error", "Failed to delete voyage. The selected voyage is used in the following booking(s): <br> " . $bk->implode('booking_no', ", "));
        } else if(count($resv) > 0) {
            $resv_id = $resv->map(function ($item, $key) {
                return "R" . str_pad($item->id, 4, "0", STR_PAD_LEFT);
            });

            return redirect::action("VoyageController@index")
            ->with("error", "Failed to delete voyage. The selected voyage is used in the following reservation(s): <br> " . $resv_id->implode(", "));
        } else {
            $voyage = Voyage::find($request->voyage_id);
            $destinations = VoyageDestinations::where('voyage_id', $request->voyage_id)->get();

            foreach($destinations AS $dest) {
                $dest->delete();
            }

            $voyage->delete();

            return redirect::action("VoyageController@index")
            ->with("success", "Successfully deleted voyage.");
        }
    }

    public function checkPortAssigned($voy_dest_id)
    {
        if($voy_dest_id != 0){
            $voy_dest = VoyageDestinations::find($voy_dest_id);
            $assigned = $voy_dest->isAssigned();
            if($assigned == ""){
                return response()->json(['success' => false]);
            } else {
                return response()->json(['success' => true, 'msg' => $assigned]);
            }
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function print_manifest($code){
        $id = Hashids::decode($code);
        if(!empty($id)){
            $voyage = Voyage::find($id[0]);
            return view("voyage.print-manifest", compact("voyage"));
        }
    }

    public function print_fl($hash)
    {
        $id = Hashids::decode($hash);
        $voy = Voyage::find($id[0]);
        return view("voyage.print-fl", compact("voy"));
    }

    public function voyage_details(Request $request)
    {
        $exist = Voyage::find($request->id);

        if($exist) {
            $voyage = array();
            $voyage['voyage_id'] = $exist->voyage_id;
            $voyage['vessel_name'] = $exist->vessel->name;
            $voyage['vessel_scn'] = $exist->scn;

            $voyage['bk_status'] = $exist->booking_statusl;

            if($exist->booking_status == 2) {
                $voyage['booking_status'] = '<span class="text-indigo">Cancelled</span>';
            } else if($exist->booking_status == 1) {
                $voyage['booking_status'] = '<span class="text-red">Closed</span>';
            } else {
                $voyage['booking_status'] = '<span class="text-green">Open</span>';
            }

            if($exist->closing_timestamp) {
                $voyage['closing_date'] = Carbon::createFromFormat('Y-m-d H:i:s', $exist->closing_timestamp)->format('d/m/Y H:i:s');
            } else {
                $voyage['closing_date'] = '-';
            }

            $voyage['pol_code'] = $exist->pol->code;
            $voyage['pol_eta'] = Carbon::createFromFormat('Y-m-d H:i:s', $exist->eta_pol)->format('d/m/Y');

            if($exist->ata_pol) {
                $voyage['pol_ata'] = Carbon::createFromFormat('Y-m-d H:i:s', $exist->ata_pol)->format('d/m/Y');
            } else {
                $voyage['pol_ata'] = '-';
            }

            $index = 0;
            $ports = array();

            foreach($exist->destinations()->orderBy('eta', 'asc')->get() as $destination) {
                $port = array();

                $port['port_code'] = $destination->getPort->code;
                $port['port_eta'] = Carbon::createFromFormat('Y-m-d H:i:s', $destination->eta)->format('d/m/Y');

                if($destination->ata) {
                    $port['port_ata'] = Carbon::createFromFormat('Y-m-d H:i:s', $destination->ata)->format('d/m/Y');
                } else {
                    $port['port_ata'] = '-';
                }

                $ports[$index] = $port;
                $index++;
            }

            $voyage['ports'] = $ports;

            return response()->json(['result' => true, 'voyage' => $voyage]);
        } else {
            return response()->json(['result' => false]);
        }
    }

    public function checkQuery($query)
    {
        if(strpos($query, 'WHERE') !== false) {
            return ' AND ';
        } else {
            return ' WHERE ';
        }
    }
}
