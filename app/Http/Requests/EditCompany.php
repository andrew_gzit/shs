<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditCompany extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // if(count($this->radio_company_type) == 2){
        //     $validation_code = 'required';
        //     $validation_tel = 'required|min:13|max:14';
        //     $validation_fax = 'nullable|min:13|max:14';
        //     $validation_pic = 'required';
        // } else {
        //     $validation_code = 'nullable';
        //     $validation_tel = 'nullable';
        //     $validation_fax = 'nullable';
        //     $validation_pic = 'nullable';
        // }

        return [
            'txt_name' => 'required|unique:companies,name,'.$this->id.',id,deleted_at,NULL',
            'txt_roc' => 'nullable|unique:companies,roc_no,'.$this->id.',id,deleted_at,NULL|regex:/^[\dA-Za-z. -]+$/',
            'txt_portcode' => 'nullable|alpha',
            'txt_bacode' => 'nullable|unique:companies,ba_code,'.$this->id.',id,deleted_at,NULL|alpha_num',
            'txt_code' => 'required|alpha_num',
            'txt_tel' => 'min:9|max:13|regex:/^[\d\+\-\(\)]{9,13}$/',
            'txt_fax' => 'nullable|min:9|max:13|regex:/^[\d\+\-\(\)]{9,13}$/'
        ];
    }

    public function messages() {
        return [
            'txt_name.required' => 'Company Name is required.',
            'txt_name.unique' => 'Company Name already exists.',
            // 'txt_name.regex' => 'Company Name may only contain alphabetical characters, brackets, hypen and spacing.',

            'txt_roc.unique' => 'ROC No. already exists.',
            'txt_roc.alpha' => 'ROC No. may only contain letters and numbers.',

            'txt_portcode.alpha' => 'Port Code may only contain letters.',

            'txt_bacode.unique' => 'Agent Code already exists.',
            'txt_bacode.alpha' => 'Agent Code may only contain letters and numbers.',

            'txt_code.required' => 'Code is required',
            'txt_code.alpha_num' => 'Code may only ontain letters and numbers',

            'txt_tel.min' => 'Telephone number is invalid.',
            'txt_tel.max' => 'Telephone number is invalid.',
            'txt_tel.regex' => 'Telephone number is invalid.',

            'txt_fax.min' => 'Fax number is invalid.',
            'txt_fax.max' => 'Fax number is invalid.',
            'txt_fax.regex' => 'Fax number is invalid.'

        ];
    }
}
