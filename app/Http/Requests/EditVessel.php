<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditVessel extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vessel_name' => 'required|unique:vessels,name,'.$this->id.',id',
            'vessel_dwt' => 'required|integer',
            'vessel_year' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'vessel_name.required' => 'Vessel Name is required.',
            'vessel_name.unique' => 'Vessel Name already exists.',
            'vessel_dwt.required' => 'Vessel DWT is required.',
            'vessel_dwt.integer' => 'Vessel DWT has to be a number.',
            'vessel_year.required' => 'Vessel Manufactured Year is required.',
        ];
    }
}
