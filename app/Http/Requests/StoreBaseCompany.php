<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBaseCompany extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $company = \App\Company::where("type", 4)->first();
        if(empty($company)){
            $company = NULL;
        } else {
            $company = $company->id;
        }

        return [
            'txt_name' => 'required|unique:companies,name,'.$company.',id,deleted_at,NULL|regex:/^[a-zA-Z\-(). \s]+$/',
            'txt_roc' => 'required|unique:companies,roc_no,'.$company.',id,deleted_at,NULL|regex:/^[\dA-Za-z. -]+$/',
            // 'txt_portcode' => 'nullable|alpha',
            'txt_bacode' => 'nullable|unique:companies,ba_code,'.$company.',id,deleted_at,NULL|alpha_num',
            'txt_tel' => 'required|min:9|max:13|regex:/^[\d\+\-\(\)]{9,13}$/',
            'txt_fax' => 'nullable|min:9|max:13|regex:/^[\d\+\-\(\)]{9,13}$/',
        ];
    }

    public function messages() {
        return [
            'txt_name.required' => 'Company Name is required.',
            'txt_name.unique' => 'Company Name already exists.',
            'txt_name.regex' => 'Company Name may only contain alphabetical characters, brackets, hypen and spacing.',

            'txt_roc.required' => 'ROC No. is required.',
            'txt_roc.unique' => 'ROC No. already exists.',
            'txt_roc.regex' => 'ROC No. may only contain alphanumeric characters and dash.',

            'txt_tel.required' => 'Telephone number is required.',
            'txt_tel.min' => 'Telephone number is invalid.',
            'txt_tel.max' => 'Telephone number is invalid.',
            'txt_tel.regex' => 'Telephone number is invalid.',

            'txt_fax.required' => 'Fax number is required.',
            'txt_fax.min' => 'Fax number is invalid.',
            'txt_fax.max' => 'Fax number is invalid.',
            'txt_fax.regex' => 'Fax number is invalid.',

        ];
    }
}
