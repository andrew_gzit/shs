<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCharge extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txt_code' => 'required|unique:charges,code,NULL,id,deleted_at,NULL|alpha',
            'txt_name' => 'required|unique:charges,name|regex:/^[a-zA-Z\s]+$/',
            // 'txt_unitprice' => 'required'
        ];
    }

    public function messages() {
        return [
            'txt_code.required' => 'Charge Code is required.',
            'txt_code.unique' => 'Charge Code already exists.',
            'txt_name.required' => 'Charge Name is required.',
            'txt_name.unique' => 'Charge Name already exists.',
            'txt_name.regex' => 'Charge Name may only contain letters and whitespaces.',
        ];
    }
}
