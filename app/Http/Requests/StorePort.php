<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePort extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'port_name' => 'required|unique:ports,name,NULL,id,deleted_at,NULL',
            'port_code' => 'nullable|unique:ports,code,NULL,id,deleted_at,NULL|alpha',
            'port_location' => 'required',
            'station_code' => 'required|unique:ports,station_code,NULL,id,deleted_at,NULL|alpha_num',
            'bl_prefix' => 'nullable',
        ];
    }

    public function messages() {
        return [
            'port_name.required' => 'Port Name is required.',
            'port_name.unique' => 'Port Name already exists.',
            'port_code.alpha' => 'Port Code may only contain letters.',
            'port_code.unique' => 'Port Code already exists.',
            'port_location.required' => 'Port Location is required.',
            'station_code.required' => 'Station Code is required.',
            'station_code.unique' => 'Station Code already exists.',
            'station_code.alpha_num' => 'Station Code may only contain letters and numbers.',
        ];
    }
}
