<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InwardBL extends Model
{
	use SoftDeletes;
	
    protected $table = "inward_bl";

    public function getPol(){
    	return $this->hasOne('App\Port', 'id', 'pol_id');
    }
}
