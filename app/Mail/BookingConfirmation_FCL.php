<?php

namespace App\Mail;

use App\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingConfirmation_FCL extends Mailable
{
    use Queueable, SerializesModels;

    public $booking;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $bk = $this->booking;
        $pdf = \PDF::loadView('emails.bc_fcl', compact("bk"));
        return $this->download('emails.bc_fcl', compact("bk"))
        ->attachData($pdf->stream(), $this->booking->booking_no . '.pdf', [
            'mime' => 'application/pdf',
        ]);;
    }
}
