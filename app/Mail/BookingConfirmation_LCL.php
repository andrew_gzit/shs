<?php

namespace App\Mail;

use App\Booking;
use App\Shipper;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingConfirmation_LCL extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $bk = $this->booking;

        //Find booking party info
        $shipper = Shipper::where('company_id', $bk->bookingparty_id)->first();
        $bk->pic = $shipper->pic;


        $pdf = \PDF::loadView('emails.bc_lcl', compact("bk"));
        return $this->view('emails.bc_lcl', compact("bk"))
        ->attachData($pdf->stream(), $this->booking->booking_no . '.pdf', [
            'mime' => 'application/pdf',
        ]);;
    }
}
