<?php

namespace App\Mail;

use App\InwardBL;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class InwardBillLading extends Mailable
{
    use Queueable, SerializesModels;

    public $bl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(InwardBL $bl)
    {
        $this->bl = $bl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $bl = $this->bl;
        $pdf = \PDF::loadView('emails.inward_bl', compact("bl"));
        return $this->view('emails.blank')
        ->subject('Notice of Arrival')
        ->attachData($pdf->stream(), $this->bl->bl_no . '.pdf', [
            'mime' => 'application/pdf',
        ]);
    }
}
