<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use \App\ShortcutKey;

class ShortcutServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('bl.*', function ($view) {
            $shortcut = ShortcutKey::select('shortcut_code', 'description')->get();
            $view->with('shortcut', $shortcut->keyBy('shortcut_code'));
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
