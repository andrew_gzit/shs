<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model
{
    use SoftDeletes;
    protected $dates = ['date', 'deleted_at'];

    public function getCompany(){
        return $this->hasOne('App\Company', 'id', 'shipper_id')->withTrashed();
    }

    public function getVoyage(){
        return $this->hasOne('App\Voyage', 'id', 'voyage_id');
    }

    public function getPod(){
        return $this->hasOne('App\Port', 'id', 'pod_id')->withTrashed();
    }

    public function getBooking(){
        return \App\Booking::where("res_id", $this->id)->get();
    }

    public function getRemainWeight(){
        $weight = $this->weight;
        $booked_weight = \App\Booking::where("res_id", $this->id)->where("voyage_id", $this->voyage_id)->sum("weight");
        return $weight - $booked_weight;
    }
}
