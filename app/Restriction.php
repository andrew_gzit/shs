<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restriction extends Model
{
	protected $fillable = ['shipper_id', 'type', 'value', 'created_at', 'updated_at'];
}
