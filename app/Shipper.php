<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shipper extends Model
{
	use SoftDeletes;

	protected $fillable = ['company_id', 'code', 'telephone', 'fax', 'pic', 'voyage_updates', 'delivery_updates', 'handling_method'];

    public function getName(){
        $company = \App\Company::withTrashed()->find($this->company_id);
        return $company->name;
    }

    public function getRestrictions(){
        return $this->hasMany('App\Restriction');
    }

    public function getDefaultCharges(){
    	return $this->hasMany('App\DefaultCharge');
    }

    public function getAddresses(){
    	return $this->hasMany('App\CompanyAddress', 'company_id', 'company_id');
    }
}
