<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
	const per_unit = [
		"1-MT",
		"1-M3",
		"1-SET",
		"1-UNIT",
		"1-PACKAGE"
	];

	const per_unit_text = [
		"PER MT",
		"PER M3",
		"PER SET",
		"PER UNIT",
		"PER PACKAGE",
	];

	const per20_unit = [
		"20-GP",
		"20-HC",
		"20-RF",
		"20-OT",
		"20-HRF",
		"20-FR",
		"20-TK"
	];

	const per20_unit_text = [
		"PER 20'GP",
		"PER 20'HC",
		"PER 20'RF",
		"PER 20'OT",
		"PER 20'HRF",
		"PER 20'FR",
		"PER 20'TK"
	];

	const per40_unit = [
		"40-GP",
		"40-HC",
		"40-RF",
		"40-OT",
		"40-HRF",
		"40-FR",
		"40-TK"
	];

	const per40_unit_text = [
		"PER 40'GP",
		"PER 40'HC",
		"PER 40'RF",
		"PER 40'OT",
		"PER 40'HRF",
		"PER 40'FR",
		"PER 40'TK"
    ];

    const per45_unit = [
		"45-GP",
		"45-HC",
		"45-RF",
		"45-OT",
		"45-HRF",
		"45-FR",
		"45-TK"
	];

	const per45_unit_text = [
		"PER 45'GP",
		"PER 45'HC",
		"PER 45'RF",
		"PER 45'OT",
		"PER 45'HRF",
		"PER 45'FR",
		"PER 45'TK"
	];
}
