<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Voyage extends Model
{
    use SoftDeletes;

    protected $dates = ['eta_pol', 'etd_pol', 'closing_timestamp'];

    public function vessel() {
    	return $this->hasOne('App\Vessel', 'id', 'vessel_id');
    }

    public function pol() {
        return $this->hasOne('App\Port', 'id', 'pol_id');
    }

    public function pod() {
        return $this->hasOne('App\Port', 'id', 'pod_id');
    }

    public function destinations() {
    	return $this->hasMany('App\VoyageDestinations')->orderBy('eta', 'ASC');
    }

    public function lastDestination() {
        return $this->hasMany('App\VoyageDestinations')->orderBy('eta', 'DESC');
    }

    public function getBL(){
        return $this->hasMany('App\BillLading', 'voyage_id', 'id');
    }

    public function getBookedWeight(){
        return \App\Booking::where("voyage_id", $this->id)->where("deleted_at", null)->sum("weight");
    }

    public function getReservedWeight(){

        // $remaining_weight = DB::select("SELECT SUM(reservations.weight - bookings.weight) AS weight FROM bookings INNER JOIN reservations ON bookings.res_id = reservations.id WHERE reservations.voyage_id = ? AND bookings.deleted_at IS NULL GROUP BY bookings.voyage_id", [$this->id]);

        // $remaining_weight = DB::select("SELECT (SELECT weight FROM reservations WHERE voyage_id = " . $this->id . " AND deleted_at IS NULL) - SUM(weight) AS weight FROM bookings WHERE voyage_id = " . $this->id . " AND res_id IS NOT NULL AND deleted_at IS NULL");

        $reservation_weight = \App\Reservation::where("voyage_id", $this->id)->sum('weight');
        $booking_weight = \App\Booking::where("voyage_id", $this->id)->where("res_id", "!=", null)->sum("weight");

        $remaining_weight =  $reservation_weight - $booking_weight;

        if($remaining_weight < 0){
            $resv = 0;
            return $resv;
        } else {
            $resv = $remaining_weight;
            return $resv;
        }

        // $resv = \App\Reservation::where("voyage_id", $this->id)->sum("weight");

        //Get bookings that is made from reservation to subtract weight from reservation
        $booking_resv = \App\Booking::where("voyage_id", $this->id)->where("res_id", "!=", null)->sum("weight");

        if((int)$resv == 0){
            return 0;
        } else {
            $total_resv_weight = (int)$resv - $booking_resv;
        }

        if($total_resv_weight < $resv){
            $bk_resv = DB::raw("SELECT reservations.id FROM ( SELECT bookings.id, SUM(weight) AS b_weight, res_id FROM bookings WHERE deleted_at IS NULL GROUP BY res_id ) b JOIN reservations ON b.res_id = reservations.id WHERE b_weight >= weight");
            $resv_1 = \App\Reservation::where("voyage_id", $this->id)->whereNotIn("id", array_pluck($bk_resv, 'id'))->sum("weight");

            $booking_resv = \App\Booking::where("voyage_id", $this->id)->whereNotIn("res_id", array_pluck($bk_resv, 'id'))->sum("weight");

            return $resv_1 - $booking_resv;
        } else {
            /**
              *If booked weight exceed reserved weight, only take the reserved weight to prevent negative value
              *As booking w/ reservation can be made with weight exceeding reserved weight
            **/
            if($booking_resv > $resv){
                return $resv;
            } else {
                return $resv - $booking_resv;
            }
        }
    }

    public function totalUsedWeight(){
        // $resv = \App\Reservation::where("voyage_id", $this->id)->where("deleted_at", null)->sum("weight");
        //Get bookings that is made from reservation to deduct weight from reservation
        // $booking_resv = \App\Booking::where("voyage_id", $this->id)->where("res_id", "!=", null)->sum("weight");
        // $booking = \App\Booking::where("voyage_id", $this->id)->where("deleted_at", null)->sum("weight");
        // return $resv + $booking - $booking_resv;
        return $this->getBookedWeight() + $this->getReservedWeight();
    }

    //Retrieve voyage that has both POL and POD and POL ETA is before $search_date
    public function getValidVoyage($pol, $pod, $date){
        $dest = $this->destinations;
        if($dest->contains("port", $pol) && $dest->contains("port", $pod)){
            $dest_pol_eta = $dest->where("port", $pol)->first()->eta;
            $dest_pod_eta = $dest->where("port", $pod)->first()->eta;
            if($dest_pol_eta >= $date && $dest_pod_eta->gte($dest_pol_eta)){
                return true;
            }
        }
        return false;
    }

    //Retrive ETA by port for this voyage
    public function getPort($port){
        $voy_dest = \App\VoyageDestinations::where("voyage_id", $this->id)->where("port", $port)->first();
        return $voy_dest;
    }

}
