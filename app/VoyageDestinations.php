<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoyageDestinations extends Model
{
    protected $table = 'voyage_destinations';
    protected $dates = ['eta', 'etd'];

    public function getPort() {
    	return $this->belongsTo('App\Port', 'port');
    }

    //Check if this port is assigned in any of the bookings / bill ladings
    public function isAssigned() {
    	$bk = \App\Booking::where("voyage_id", $this->voyage_id)->where(function($query){
            $query->where('pol_id', $this->port)
            ->orWhere('fpd_id', $this->port);
        })->get();

    	$bl = \App\BillLading::where("voyage_id", $this->voyage_id)->where(function($query){
            $query->where('pol_id', $this->port)
            ->orWhere("pod_id", $this->port)
            ->orWhere('fpd_id', $this->port);
        })->get();

    	$assgined = "";

    	if(count($bk) > 0){
    		$assgined .= "Selected port is used in the following booking(s) : <br>" . $bk->implode("booking_no", ", ") . "<br>";
    	}

    	if (count($bl) > 0){
    		$assgined .= "Selected port is used in the following bill lading(s) : <br>" . $bl->implode("bl_no", ", ") . "<br>";
    	}

    	return $assgined;
    }
}
