@foreach($outwards as $outward)
							<tr>
								<td><input type='checkbox' class='chk_bl' name='chk_bl[]' value='{{ $outward->id }}'></td></td>
								<td>{{ $outward->bl_no }}</td>
								<td>{{ optional($outward->getVessel)->name }}</td>
								<td>{{ optional($outward->getVoyage)->voyage_id }}</td>
								<td>{{ !empty($outward->pol) ? $outward->pol->code : '' }}</td>
								<td>{{ !empty($outward->pol) ? $outward->pod->code : '' }}</td>
								<td>{{ ($outward->cust_s_name) ? $outward->cust_s_name : optional($outward->shipper)->name }}</td>
								<td>{!! ($outward->telex_status == 0) ? '<span class="text-danger">No</span>' : '<span class="text-green">Yes</span>' !!}</td>
								<td>
									<div class="dropdown">
										<button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<i class="fa fa-cog"></i>
										</button>
										<div class="dropdown-menu dropdown-menu-right f-s-14" aria-labelledby="dropdownMenuButton">
											<a class="dropdown-item" href="{{ action('BillLadingController@edit', Hashids::encode($outward->id)) }}">BL Details</a>
											<a class="dropdown-item btn-cargo" href="{{ action('BillLadingController@cargo', Hashids::encode($outward->id)) }}" data-id="{{ $outward->id }}">Cargo</a>
											<a class="dropdown-item btn-charges" href="{{ action('BillLadingController@charges', Hashids::encode($outward->id)) }}" data-id="{{ $outward->id }}">Charges</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item btn-part" href="javascript:;" data-id="{{ $outward->id }}"><i class="fas fa-clone m-r-10"></i>Part BL</a>
											<a class="dropdown-item btn-copy" href="{{ action('BillLadingController@create') }}?bl={{ $outward->bl_no }}"><i class="fas fa-copy m-r-10"></i>Copy BL</a>
											{{-- <a class="dropdown-item btn-delete" href="" data-id="{{ $outward->id }}">Delete BL</a> --}}
											<div class="dropdown-divider"></div>
											<a class="dropdown-item btn-print-so" href="{{ action('BillLadingController@print_so', Hashids::encode($outward->id)) }}?bl=true"><i class="fas fa-print m-r-10"></i>Print Shipping Order</a>
											<a class="dropdown-item btn-print" data-bl-id="{{ $outward->id }}" data-bl-no="{{ $outward->bl_no }}" href="javascript:;"><i class="fas fa-print m-r-10"></i>Print BL</a>
										</div>
									</div>
								</td>
							</tr>
							@endforeach