<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillLadingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_ladings', function (Blueprint $table) {
            $table->increments('id');

            $table->string('bc_no')->nullable();
            $table->string('bl_no');
            $table->integer('vessel_id');
            $table->string('vessel_type');
            $table->integer('voyage_id');
            $table->integer('pol_id');
            $table->integer('pod_id');
            $table->integer('fpd_id');

            $table->integer('shipper_id');
            $table->string('shipper_name');
            $table->integer('shipper_add_id');
            $table->string('shipper_add');

            $table->integer('consignee_id');
            $table->string('consignee_name');
            $table->integer('consignee_add_id');
            $table->string('consignee_add');

            $table->integer('notify_id');
            $table->string('notify_name');
            $table->integer('notify_add_id');
            $table->string('notify_add');

            $table->datetime('bl_date');
            $table->integer('freight_term');
            $table->integer('cont_ownership')->nullable();
            $table->integer('telex_status');
            $table->integer('no_of_bl');

            $table->integer('format_id')->unsigned()->nullable();
            $table->foreign('format_id')->references('id')->on('bl_formats');
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_ladings');
    }
}
