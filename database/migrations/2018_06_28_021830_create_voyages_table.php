<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoyagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voyages', function (Blueprint $table) {
            $table->increments('id');

            $table->string('voyage_id');

            $table->integer('pol_id')->unsigned()->nullable();
            $table->foreign('pol_id')->references('id')->on('ports');

            $table->dateTime('eta_pol')->nullable();
            $table->dateTime('ata_pol')->nullable();
            $table->dateTime('etd_pol');
            $table->dateTime('atd_pol')->nullable();
            
            // $table->integer('pod_id')->unsigned();
            // $table->foreign('pod_id')->references('id')->on('ports');

            $table->integer('vessel_id')->unsigned();
            $table->foreign('vessel_id')->references('id')->on('vessels');

            $table->string('scn')->nullable();
            $table->integer('booking_status');
            $table->dateTime('closing_timestamp')->nullable();
            $table->string('cancel_remark')->nullable();

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voyages');
    }
}
