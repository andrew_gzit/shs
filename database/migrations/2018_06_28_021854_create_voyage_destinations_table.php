<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoyageDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voyage_destinations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('voyage_id')->unsigned();
            $table->foreign('voyage_id')->references('id')->on('voyages');

            $table->integer('port')->unsigned();
            $table->foreign('port')->references('id')->on('ports');

            $table->dateTime('eta');
            $table->dateTime('ata')->nullable();
            $table->dateTime('etd');
            $table->dateTime('atd')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voyage_destinations');
    }
}
