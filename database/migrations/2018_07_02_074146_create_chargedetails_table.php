<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChargedetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charge_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('charge_id');
            $table->foreign('charge_id')->references('id')->on('charges');
            $table->string('unit');
            $table->integer('unit_size');
            $table->double('unit_price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charge_details');
    }
}
