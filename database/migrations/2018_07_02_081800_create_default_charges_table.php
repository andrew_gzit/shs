<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_charges', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('shipper_id')->unsigned();
            $table->foreign('shipper_id')->references('id')->on('shippers');

            $table->integer('charge_id')->unsigned();
            $table->foreign('charge_id')->references('id')->on('charges');

            $table->string('unit');
            $table->integer('unit_size');

            $table->double('price');

            $table->integer('pol')->unsigned()->nullable()->default(null);
            $table->foreign('pol')->references('id')->on('ports');

            $table->integer('final_dest')->unsigned()->nullable()->default(null);
            $table->foreign('final_dest')->references('id')->on('ports');

            $table->string('payment');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_charges');
    }
}
