<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlCargosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_cargos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('bl_id');
            $table->string('bl_no');
            $table->integer('cargo_qty')->nullable();
            $table->string('cargo_packing')->nullable();
            $table->string('cargo_name')->nullable();
            $table->longText('markings')->nullable();
            $table->string('cargo_nature');
            $table->longText('uncode')->nullable();
            $table->longText('cargo_desc');
            $table->string('cargo_type');
            $table->double('weight');
            $table->string('weight_unit');
            $table->double('volume');
            $table->integer('temperature')->nullable();
            $table->longText('detailed_desc')->nullable();
            $table->longText('remarks')->nullable();
            $table->string('raw_qty')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bl_cargos');
    }
}
