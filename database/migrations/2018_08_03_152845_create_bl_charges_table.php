<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_charges', function (Blueprint $table) {
            $table->increments('id');

            $table->string('bl_no');
            $table->string('charge_code');
            $table->string('charge_unit');
            $table->double('charge_rate');
            $table->string('charge_payment');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bl_charges');
    }
}
