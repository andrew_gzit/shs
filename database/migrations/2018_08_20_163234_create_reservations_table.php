<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('date');
            $table->unsignedInteger('shipper_id');
            $table->unsignedInteger('voyage_id')->nullable();
            $table->integer('weight')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('shipper_id')->references('id')->on('companies');
            $table->foreign('voyage_id')->references('id')->on('voyages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
