<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('date');
            $table->string('booking_no');
            $table->unsignedInteger('bookingparty_id');
            $table->unsignedInteger('shipper_id');
            // $table->string('scn');
            // $table->string('operator_code');
            $table->unsignedInteger('voyage_id');
            $table->string('shipment_type');
            // $table->string('freight_term');
            $table->integer('status');

            $table->unsignedInteger('pol_id');
            // $table->string('pol_sc')->nullable();
            $table->datetime('eta_pol');

            // $table->unsignedInteger('pod_id');
            // $table->string('pod_sc')->nullable();
            // $table->datetime('eta_pod');

            $table->unsignedInteger('fpd_id');
            // $table->string('fpd_sc')->nullable();
            $table->datetime('eta_fpd');

            // $table->integer('cont_ownership')->nullable();

            $table->string('cargo_qty')->nullable();
            $table->string('cargo_packing')->nullable();

            $table->string('cargo_desc')->nullable();
            $table->integer('temperature')->nullable();
            $table->integer('weight')->nullable();
            $table->string('uncode')->nullable();
            $table->string('cargo_nature')->nullable();

            $table->string('remarks')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('bookingparty_id')->references('id')->on('companies');
            $table->foreign('shipper_id')->references('id')->on('companies');
            $table->foreign('voyage_id')->references('id')->on('voyages');

            $table->foreign('pol_id')->references('id')->on('ports');
            // $table->foreign('pod_id')->references('id')->on('ports');
            $table->foreign('fpd_id')->references('id')->on('ports');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
