<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bill_ladings', function (Blueprint $table) {
            $table->string('pol_name')->after('pol_id');
            $table->string('pod_name')->after('pod_id');
            $table->string('fpd_name')->after('fpd_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bill_ladings', function (Blueprint $table) {
            //
        });
    }
}
