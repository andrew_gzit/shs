<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            //Add reservation ID to track the weight/TEUS
            $table->unsignedInteger('res_id')->after('remarks')->nullable();
            $table->foreign('res_id')->references('id')->on('reservations');

        });

        Schema::table('reservations', function (Blueprint $table) {
            //Drop booking ID from reservation
            $table->dropForeign(['booking_id']);
            $table->dropColumn('booking_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            //
        });
    }
}
