<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMshipCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->unsignedInteger('mvessel_id')->nullable()->after('shipment_type');
            $table->unsignedInteger('ts_port_id')->nullable()->after('mvessel_id');
        });

        Schema::table('reservations', function (Blueprint $table) {
            $table->unsignedInteger('pod_id')->after('voyage_id');
            $table->string('remarks')->after('pod_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
