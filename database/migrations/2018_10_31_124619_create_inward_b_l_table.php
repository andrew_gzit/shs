<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInwardBLTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inward_bl', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vessel');
            $table->string('shipment_type');
            $table->string('voyage');
            $table->string('bl_no');
            $table->string('pol_name');
            $table->string('pod_name');
            $table->string('fpd_name');
            $table->string('shipper_name');
            $table->string('shipper_add');
            $table->string('consignee_name')->nullable();
            $table->string('consignee_add')->nullable();
            $table->string('notify_name');
            $table->string('notify_add')->nullable();
            $table->string('no_bl');
            $table->string('containers');
            $table->string('cargo_desc');
            $table->string('detailed_desc');
            $table->string('weight');
            $table->string('weight_unit');
            $table->string('volume');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inwardBL');
    }
}
