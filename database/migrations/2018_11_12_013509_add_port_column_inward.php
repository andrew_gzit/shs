<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPortColumnInward extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inward_bl', function (Blueprint $table) {
            $table->unsignedInteger('pol_id')->after('bl_no');
            $table->foreign('pol_id')->references('id')->on('ports');
            $table->unsignedInteger('pod_id')->after('pol_name');
            $table->foreign('pod_id')->references('id')->on('ports');
            $table->unsignedInteger('fpd_id')->after('pod_name');
            $table->foreign('fpd_id')->references('id')->on('ports');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inward_bl', function (Blueprint $table) {
            //
        });
    }
}
