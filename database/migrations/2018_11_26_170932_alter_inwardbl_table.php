<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInwardblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inward_bl', function (Blueprint $table) {
            $table->string('remarks')->nullable()->after('volume');
            $table->string('date_of_issue')->nullable();

            //Change to nullable
            $table->string('cargo_desc')->nullable()->change();
            $table->unsignedInteger('pol_id')->nullable()->change();
            $table->unsignedInteger('pod_id')->nullable()->change();
            $table->unsignedInteger('fpd_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inward_bl', function (Blueprint $table) {
            //
        });
    }
}
