<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChargeVesselTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charge_vessels', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('charge_id');
            $table->foreign('charge_id')->references('id')->on('default_charges');
            $table->unsignedInteger('vessel_id');
            $table->foreign('vessel_id')->references('id')->on('vessels');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charge_vessels');
    }
}
