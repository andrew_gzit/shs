<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChargeCargos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charge_cargos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('blcharge_id');
            $table->foreign('blcharge_id')->references('id')->on('bill_charges');
            $table->unsignedInteger('cargo_id');
            $table->foreign('cargo_id')->references('id')->on('bill_cargos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charge_cargos');
    }
}
