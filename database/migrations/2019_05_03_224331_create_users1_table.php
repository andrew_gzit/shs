<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('remember_token');
            $table->timestamps();
        });

        DB::table('users')->insert(
            array(
                'username' => 'shs',
                'password' => '$2y$10$pUogOSrQB8lArdnPkckr8.v3GPwU0eOa3k4annF46FipjEAsnRJFO'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
