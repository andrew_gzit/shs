<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BLNumbersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bill_numbers')->insert([
            [
                'current_number' => '0',
                'vessel_default' => 'LCL',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'current_number' => '1000',
                'vessel_default' => 'FCL',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);

        DB::table('bl_formats')->insert([
            [
                'name' => 'Apollo Agencies (1980)',
                'content' => '',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
