<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ChargesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('charges')->insert([
    		[
    			'code' => 'OFT',
    			'name' => 'Ocean Freight',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'code' => 'BAF',
    			'name' => 'Bunker Adjustment Factor',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'code' => 'BL',
    			'name' => 'Bill of Lading',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'code' => 'EDI',
    			'name' => 'Electronic Data Interchange',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    	]);

    	DB::table('charge_details')->insert([
    		[
    			'charge_id' => 1,
    			'unit_size' => 1,
    			'unit' => 'MT',
    			'unit_price' => 130,
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'charge_id' => 1,
    			'unit_size' => 20,
    			'unit' => 'GP',
    			'unit_price' => 850,
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'charge_id' => 1,
    			'unit_size' => 40,
    			'unit' => 'HC',
    			'unit_price' => 1700,
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'charge_id' => 2,
    			'unit_size' => 20,
    			'unit' => 'GP',
    			'unit_price' => 580,
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'charge_id' => 3,
    			'unit_size' => 1,
    			'unit' => 'SET',
    			'unit_price' => 175,
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'charge_id' => 4,
    			'unit_size' => 1,
    			'unit' => 'SET',
    			'unit_price' => 30,
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    	]);
    }
}
