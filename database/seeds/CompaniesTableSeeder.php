<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            [
                'name' => 'ARDATEK MALAYSIA SDN BHD',
                'roc_no' => '123',
                'type' => 0,
                'port_code' => 'MYPPK',
                'ba_code' => 'MYPPK',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'PERUSAHAAN TEN TEN',
                'roc_no' => 'TENTEN1',
                'type' => 1,
                'port_code' => 'MYPKG',
                'ba_code' => 'MYPKG',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'CSH HARDWARE BUTTERWORTH SDN BHD',
                'roc_no' => 'CSH',
                'type' => 1,
                'port_code' => 'MYPKB',
                'ba_code' => 'MYPKB',
                'created_at' => Carbon::now()
            ]
        ]);

        DB::table('shippers')->insert([
            [
                'company_id' => 1,
                'code' => 'ARDATEK',
                'telephone' => '(01) 6123-1231',
                'fax' => '(01) 6123-1231',
                'pic' => 'LIM',
                'voyage_updates' => 1,
                'delivery_updates' => 0,
                'handling_method' => 'Container',
                'created_at' => Carbon::now()
            ]
        ]);

        DB::table('consignees')->insert([
            [
                'company_id' => 2,
                'code' => 'TENTEN',
                'telephone' => '(01) 6123-1231',
                'fax' => '(01) 6123-1231',
                'pic' => 'LIM',
                'created_at' => Carbon::now()
            ],
            [
                'company_id' => 3,
                'code' => 'CSH',
                'telephone' => '(01) 6123-1231',
                'fax' => '(01) 6123-1231',
                'pic' => 'LIM',
                'created_at' => Carbon::now()
            ]
        ]);

        DB::table('company_addresses')->insert([
            [
                'company_id' => 1,
                'address' => 'NO.4, LORONG SUNGAI TUKANG 1/1<br>KAWANSAN PERUSAHAAN SUNGAI PETANI<br>08000 KEDAH MALAYSIA',
                'created_at' => Carbon::now()
            ],
            [
                'company_id' => 2,
                'address' => 'BLOCK P LOT 7 GROUND FLOOR<br>BANDAR RAMAI-RAMAI<br>90724 SANDAKAN SABAH',
                'created_at' => Carbon::now()
            ],
            [
                'company_id' => 3,
                'address' => '933, JALAN PERINDUSTRIAN BUKIT MINYAK 7<br>KAWASAN PERINDUSTRIAN, BUKIT MINYAK<br>14100 PENANG MALAYSIA',
                'created_at' => Carbon::now()
            ]
        ]);

        DB::table('company_emails')->insert([
            [
                'company_id' => 1,
                'email' => 'ardatek@gmail.com',
                'created_at' => Carbon::now()
            ],
            [
                'company_id' => 2,
                'email' => 'tenten@gmail.com',
                'created_at' => Carbon::now()
            ],
            [
                'company_id' => 3,
                'email' => 'csh@gmail.com',
                'created_at' => Carbon::now()
            ]
        ]);

        // DB::table('default_charges')->insert([
        //     [
        //         'shipper_id' => 1,
        //         'chargedetail_id' => 1,
        //         'price' => 10,
        //         'created_at' => Carbon::now()
        //     ],
        //     [
        //         'shipper_id' => 1,
        //         'chargedetail_id' => 2,
        //         'price' => 20,
        //         'created_at' => Carbon::now()
        //     ],
        //     [
        //         'shipper_id' => 1,
        //         'chargedetail_id' => 3,
        //         'price' => 30,
        //         'created_at' => Carbon::now()
        //     ],
        //     [
        //         'shipper_id' => 1,
        //         'chargedetail_id' => 4,
        //         'price' => 40,
        //         'created_at' => Carbon::now()
        //     ],
        //     [
        //         'shipper_id' => 1,
        //         'chargedetail_id' => 5,
        //         'price' => 50,
        //         'created_at' => Carbon::now()
        //     ],
        //     [
        //         'shipper_id' => 1,
        //         'chargedetail_id' => 6,
        //         'price' => 60,
        //         'created_at' => Carbon::now()
        //     ]
        // ]);
    }
}
