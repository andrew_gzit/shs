<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call([
        	PortsTableSeeder::class,
        	VesselsTableSeeder::class,
            // VoyagesTableSeeder::class,
            // VoyageDestTableSeeder::class,
            BLNumbersTableSeeder::class,
            ChargesTableSeeder::class,
            CompaniesTableSeeder::class,
            ShortcutKeyTableSeeder::class
        ]);
    }
}
