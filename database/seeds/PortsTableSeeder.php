<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PortsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ports')->insert([
            [
                'name' => 'Butterworth Wharves Cargo Terminal',
                'code' => 'MYBTW',
                'location' => 'Penang',
                'location_code' => 'PG',
                'bl_prefix' => 'PG',
                'station_code' => 'P14',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'North Butterworth Container Terminal',
                'code' => 'MYPEN',
                'location' => 'Penang',
                'location_code' => 'PG',
                'bl_prefix' => 'PG',
                'station_code' => 'P15',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Kota Kinabalu Port',
                'code' => 'MYBKI',
                'location' => 'Kota Kinabalu',
                'location_code' => 'KK',
                'bl_prefix' => 'KK',
                'station_code' => 'S28',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Sepangar Bay',
                'code' => 'MYSEP',
                'location' => 'Kota Kinabalu',
                'location_code' => 'KK',
                'bl_prefix' => 'KK',
                'station_code' => 'S87',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Sandakan',
                'code' => 'MYSDK',
                'location' => 'Sandakan',
                'location_code' => 'S',
                'bl_prefix' => 'S',
                'station_code' => 'S14',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
