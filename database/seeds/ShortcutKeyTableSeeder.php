<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ShortcutKeyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('shortcut_keys')->insert([
    		[
    			'shortcut_code' => 'F1',
    			'description' => 'DESTINATION PORT THC PREPAID AT PENANG.'."\n".'DOC FEE AND CARRIER EDI TO BE COLLECTED AT DESTINATION.',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'shortcut_code' => 'F2',
    			'description' => 'DESTINATION PORT THC, CHC AND GOPC PREPAID AT PENANG.'."\n".'LCHC, DOC FEE AND CARRIER EDI TO BE COLLECTED AT DESTINATION.',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'shortcut_code' => 'F3',
    			'description' => 'DESTINATION PORT THC, DO FEE & CARRIER EDI PREPAID AT PENANG',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'shortcut_code' => 'F4',
    			'description' => 'T/S CARGO FOR VIA SINGAPORE',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'shortcut_code' => 'F5',
    			'description' => 'DESTINATION PORT THC, DO FEE, CHC, LCHC & CARRIER EDI'."\n".'PREPAID AT PENANG',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'shortcut_code' => 'F6',
    			'description' => 'DESTINATION PORT THC, DO FEE, CHC, LCHC & CARRIER'."\n".
                'EDI TO BE COLLECTED AT DESTINATION PORT',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'shortcut_code' => 'F7',
    			'description' => 
                'FCL/FCL'."\n".
                'SHIPPER\'S LOAD,COUNT AND SEALED'."\n".
                'T/S AT PORT KLANG BY MV"',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'shortcut_code' => 'F8',
    			'description' =>
                'DESTINATION PORT THC, DO FEE, CHC, LCHC, GOPC & CARRIE\'S'."\n".
                'EDI TO BE COLLECTED AT DESTINATION',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'shortcut_code' => 'F9',
    			'description' => 
                'DESTINATION PORT THC PREPAID AT PENANG.'."\n".
                'LABUAN HANDLING CHARGES & DO FEES TO BE COLLECTED AT DESTINATION.',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'shortcut_code' => 'F10',
    			'description' =>
                'DESTINATION PORT THC, DO FEE, CHC, LCHC, GOPC & CARRIER'."\n".
                'EDI PREPAID AT PENANG.',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'shortcut_code' => 'F11',
    			'description' => 'LOADING PORT THC, BL FEE & EDI TO COLLECT AT DESTINATION.',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    		[
    			'shortcut_code' => 'F12',
    			'description' => '(TEMP: -21 DEG.C)',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		],
    	]);
    }
}
