<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class VesselsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vessels')->insert(
            [
                'name' => 'LENA',
                'type' => 'Container Vessel',
                'dwt' => '6000',
                'semicontainer_service' => 0,
                'manufactured_year' => '1988',
                'flag' => 'Malaysia',
                'default_shipment' => 'FCL',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'LEWANG',
                'type' => 'General Cargo Vessel',
                'dwt' => '3762',
                'semicontainer_service' => 0,
                'manufactured_year' => '1988',
                'flag' => 'Malaysia',
                'default_shipment' => 'FCL',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        );
    }
}
