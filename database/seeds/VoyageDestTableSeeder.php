<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class VoyageDestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('voyage_destinations')->insert([
            [
                'voyage_id' => 1,
                'port' => 4,
                'eta' => Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'voyage_id' => 1,
                'port' => 5,
                'eta' => Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'voyage_id' => 1,
                'port' => 2,
                'eta' => Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'voyage_id' => 2,
                'port' => 1,
                'eta' => Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'voyage_id' => 2,
                'port' => 5,
                'eta' => Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
