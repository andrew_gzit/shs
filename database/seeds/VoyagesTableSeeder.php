<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class VoyagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('voyages')->insert([
            [
                'voyage_id' => 'V0001',
                'pol_id' => 1,
                'eta_pol' => Carbon::now(),
                'pod_id' => 1,
                'eta_pod' => Carbon::now(),
                'vessel_id' => 1,
                'scn' => 10001,
                'booking_status' => 1,
                'closing_timestamp' => Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'voyage_id' => 'V0002',
                'pol_id' => 3,
                'eta_pol' => Carbon::now(),
                'pod_id' => 3,
                'eta_pod' => Carbon::now(),
                'vessel_id' => 1,
                'scn' => 10002,
                'booking_status' => 1,
                'closing_timestamp' => Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
