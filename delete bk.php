@if(count($outwards) > 0)
				<form id="form_delete" method="POST" class="clearfix" action="{{ action('BillLadingController@batch_delete') }}">
					@csrf
					@method('DELETE')
					<input type="text" hidden="" id="txt_bl_id" name="txt_bl_id">
					<button id="btn_batchdelete" disabled="" type="button" class="btn btn-danger m-b-15"><i class="fa fa-trash m-r-10"></i>Delete</button>
				</form>
                @endif