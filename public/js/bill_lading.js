$(function(){
	//Function to update full port name & location
	$('#pol_id, #pod_id, #fpd_id').change(function() {
		var name = $(this).find('option:selected').attr('data-name');
		var loc = $(this).find('option:selected').attr('data-location');

		if(name == undefined) name = "";

		if(loc == undefined) loc = "";

		//Insert value of location
		$("input[data-id='" + this.id + "']").val(name);

		$(this).closest(".d-flex").siblings("span").html('<i>' + name + '</i>');
	});
});