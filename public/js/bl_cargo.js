var shortcuts = "";
var return_url = "/";
var shipment_type = "";
var is_semicontainer = 0;

var container_type_obj = {
    "20GP": 0,
    "20HC": 0,
    "20RF": 0,
    "20HRF": 0,
    "20FR": 0,
    "20TK": 0,
    "20OT": 0,
    "40GP": 0,
    "40HC": 0,
    "40RF": 0,
    "40HRF": 0,
    "40FR": 0,
    "40TK": 0,
    "40OT": 0,
    "45GP": 0,
    "45HC": 0,
    "45RF": 0,
    "45HRF": 0,
    "45FR": 0,
    "45TK": 0,
    "45OT": 0
};

$.get('/shortcut-list', function(response) {
    shortcuts = response;
});

$('#btn_cancel').click(function() {
    swal({
        title: 'Are your sure?',
        text: 'All your current progress will be lost.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Proceed',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            window.location.replace(return_url);
        }
    });
});

// $("textarea").keypress(function(e){
// 	var maxrow = $(this).attr("maxrow");
// 	if(maxrow != ""){
// 		var txt_array = $(this).val().split("\n");
// 		var elem = $(this);

// 		if(txt_array.length >= maxrow){
// 			if(e.which == 13){
// 				return false;
// 			}
// 		}

// 		// setTimeout(function() {
// 		// 	var new_txt_array = elem.val().split("\n");
// 		// 	if(new_txt_array.length > maxrow){
// 		// 			// new_txt_array.splice(maxrow);
// 		// 			// elem.val(new_txt_array.join("\n"));
// 		// 		}
// 		// 	}, 100);
// 	}
// }).bind('paste', function(e) {

// 	if (this.clientHeight < this.scrollHeight) {
// 		e.preventDefault();
// 		return false;
// 	}

// 	var elem = $(this);
// 	var maxrow = elem.attr("maxrow");

// 	setTimeout(function() {
// 		var txt_array = elem.val().split("\n");
// 		if(txt_array.length > maxrow){
// 			txt_array.splice(maxrow);
// 			elem.val(txt_array.join("\n"));
// 		}
// 	}, 100);
// });

$('.cargo-qty').change(function() {

    var entered_val = $(this).val();

    if (entered_val == "" || isNaN(entered_val) || entered_val < 0) {
        $(this).val("0");
    } else {
        $(this).val(parseInt(entered_val));
    }

    var old_value = container_type_obj[this.id];

    container_type_obj[this.id] = entered_val;

    var quantityArr = [];
    var rawQuantityArr = [];

    for (var property in container_type_obj) {
        if (container_type_obj[property] > 0) {
            // Get data-attr for the ID
            var containerType = $(`#${property}`).data('type');

            var split_str = containerType.split('\'');
            quantityArr.push(container_type_obj[property] + " x " + split_str[0] + "'" + split_str[1]);
            rawQuantityArr.push(container_type_obj[property] + "," + split_str[0] + "'" + split_str[1]);
        }
    }

    var quantityText = quantityArr.join(" & ");
    var raw_qty = rawQuantityArr.join(";");

    if (shipment_type == "FCL") {
        calculateTues();
    }

    if (old_value > entered_val && old_value != 0) {
        var minus = old_value - entered_val;
        $("#div_" + this.id).find(".container-row:nth-last-child(-n+" + minus + ")").remove();
    } else {

        entered_val -= old_value;
        var desc = '';
        var html = '';
        var count = 0;
        var empty = true;

        var container_type = $(this).attr("data-type");
        var split = $(this).attr('data-type').split("'");

        for (var i = 0; i < entered_val; i++) {

            if (count > 0) {
                desc += ' & ';
            }

            html += '<div class="row container-row">' +
                '<div class="col-lg-3">' +
                '<div class="form-group m-b-10">' +
                '<label>Container No.</label>' +
                '<input type="hidden" name="cargo_detail_id[]" class="cargo_id" value="0">' +
                '<input type="text" name="container_no[]" data-title="Container No." class="required form-control form-control-lg" placeholder="Container No." />' +
                '</div>' +
                '</div>' +
                '<div class="col-lg-3">' +
                '<div class="form-group m-b-10">' +
                '<label>Seal No.</label>' +
                '<input type="text" name="seal_no[]" data-title="Seal No." class="required form-control form-control-lg" placeholder="Seal No." />' +
                '</div>' +
                '</div>' +
                '<div class="col-lg-3">' +
                '<div class="form-group m-b-10">' +
                '<label>Size</label>' +
                '<input type="text" name="container_size[]" class="form-control form-control-lg" placeholder="Size" value="' + split[0] + '" readonly />' +
                '</div>' +
                '</div>' +
                '<div class="col-lg-3">' +
                '<div class="form-group m-b-10">' +
                '<label>Type</label>' +
                '<input type="text" name="container_type[]" class="form-control form-control-lg" placeholder="Type" value="' + split[1] + '" readonly />' +
                '</div>' +
                '</div></div>';
        }

        if ($("#div_" + this.id).length == 0) {
            html = "<div id='div_" + this.id + "'>" + html + "</div>";
            $(".cargo-container").append(html);
        } else {
            $("#div_" + this.id).append(html);
        }

        count++;

        console.log(desc);
        console.log(raw_qty);
    }

    console.log(container_type_obj);

    $('#cargo_name').val(quantityText);
    $('#raw_qty').val(raw_qty);
});

$('#cargo_qty, #cargo_packing').change(function() {
    $('#cargo_name').val($('#cargo_qty').val() + ' ' + $('#cargo_packing').val().toUpperCase());
});

$(document).on("keydown", "input[data-type='number']", function(e) {
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
        (e.keyCode < 96 || e.keyCode > 105) &&
        e.keyCode != 8 && e.keyCode != 9 && e.keyCode != 110 && e.keyCode != 190) {
        e.preventDefault();
    }
});

$("#cargo_qty, .cargo-qty").keydown(function(e) {
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
        (e.keyCode < 96 || e.keyCode > 105) &&
        e.keyCode != 8 && e.keyCode != 9) {
        e.preventDefault();
    }
});

$("#cargo_qty").blur(function() {
    var entered_val = parseInt($(this).val());
    if (isNaN(entered_val) || entered_val < 1) {
        $(this).val(1);
    } else {
        $(this).val(entered_val);
    }
});

$("input[name='weight'], input[name='volume']").blur(function() {
    var entered_val = parseFloat($(this).val()).toFixed(3);

    if (isNaN(entered_val) || entered_val < 0) {
        $(this).val(0);
    } else if ($(this).val() > 999999.000) {
        $(this).val("999999.000");
    } else {
        $(this).val(entered_val);
    }
});

$('#btn_reset').click(function() {
    swal({
        title: 'Are your sure?',
        text: 'Resetting quantity will erase all your progress. Proceed?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Proceed',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $('.cargo-qty, input[name="weight"]').val(0);
            $('input[name="weight"]').focus().blur();
            $('#cargo_name, #raw_qty').val('');
            $('.cargo-container').html('');
            for (var property in container_type_obj) {
                container_type_obj[property] = 0;
            }
        }
    });
});

$('.btn-save').click(function() {
    var valid = true;


    var notFilled = $("input[name='container_no[]']").filter(function() {
        return this.value != '';
    });

    console.log(notFilled);


    $(".required").each(function() {
        if ($(this).val().trim() == "") {
            swal("Oops!", $(this).attr("data-title") + " is required.", "warning");
            valid = false;
            return false;
        }
    });


    //If cargo qty doesn't exist, means FCL shipment, require to enter container
    if ($("#cargo_qty").length == 0) {
        if ($("#cargo_name").val() == "") {
            valid = false;
            swal("Oops!", "There should be at least 1 container filled.", "warning");
        }
    }

    if (valid) {

        $(".editablecontent").each(function() {
            var content = $(this).html();
            var attr = $(this).attr("data-name");
            $("#" + attr).val(content);
        });

        $('#cargo_form').submit();
    }
});


$(".cargo-qty").blur(function() {
    var entered_value = $(this).val();
    if (entered_value == "") {
        $(this).val(0);
    } else {
        $(this).val(parseInt(entered_value));
    }
}).focus(function() {
    if ($(this).val() == 0) {
        $(this).val("");
    } else {
        $(this).select();
    }
});

//If semicontainer, 40footer is 25mt, 20footer is 20mt
function calculateTues() {
    var tues_count = 0;

    $('.cargo-qty').each(function(index) {
        var cur_split = $(this).attr('data-type').split("'");
        var cur_value = $(this).val();

        if (cur_split[0] == 40) {
            if (is_semicontainer == 1) {
                tues_count += parseInt(cur_value * 25);
            } else {
                tues_count += parseInt(cur_value * 2);
            }
        } else {
            if (is_semicontainer == 1) {
                tues_count += parseInt(cur_value * 20);
            } else {
                tues_count += parseInt(cur_value);
            }
        }
    });

    $("input[name='weight']").val(tues_count).focus().blur();
}
