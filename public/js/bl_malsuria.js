
function printPaging(bl_id){
	var $tbody = $("[class*='bl_content_" + bl_id + "']");
	var total_page = $tbody.length;

	$tbody.each(function(index, elem){
		$(this).closest("table").closest("td").find(".div-paging").text(index+1 + " OF " + total_page);
	});
}

function printTotal(bl_id, weight, volume){
	var $tbody = $("[class*='bl_content_" + bl_id + "']:last");

	var html = "<tr>" + 
	"<td></td>" +
	"<td></td>" +
	"<td><div style='width: 200px' class='sum_box text-left'>" + weight + " : " + volume + "</div></td>";
	$tbody.append(html);
}