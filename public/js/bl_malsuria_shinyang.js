$(function(){

	$.ajaxSetup({async:false});

	//Retrieve all BL's first page
	var content = $("[class*='bl_content_']");

	var cargo_counter = 1;
	var container_counter = 1;

	content.each(function(index, elem){
		var bl_id = $(elem).attr("data-id");
		getCargo(bl_id);
	});

	//Check if current BL content height exceed, return false if more than set height
	function checkHeight(bl_id){
		var height = $("[class^='bl_content_" + bl_id + "']:last").height();
		if(height >= 350){
			return "false";
		} else {
			return "true";
		}
	}

	//Get individual cargo of BL
	function getCargo(bl_id){
		var $tbody = $("[class^='bl_content_" + bl_id + "']:last");
		var $table = $tbody.closest(".table-bl");

		$.get("/get-cargo/" + bl_id, function(data){
			if(data.success){
				var cargos = data.cargos;

				//Loop each cargo, one cargo can contain many containers
				cargos.forEach(function(elem, index){

					var $tr = $("<tr id='cargo_id" + cargo_counter + "'></tr>");
					var $container_td = $("<td style='max-width: 25%; width: 25%;' id='container_id" + container_counter + "'></td>");
					$container_td.appendTo($tr);

						$tbody = $("[class^='bl_content_" + bl_id + "']:last"); //Search for newly craeted tbody

						$tbody.append($tr);

					//Get containers in cargo by cargo id (elem.id)
					$.get('/get-container/' + elem.id, function(data){
						var containers = data.containers;

						//Loop each container, insert into first cell
						containers.forEach(function(elem, index){
							var span = "<span class='d-block'>" + elem.container_no + "/" + elem.seal_no + "</span>";

							//Check if height exceeded, if so, clone current BL template and continue from there
							if(checkHeight(bl_id) == "false"){
								$tr = cloneBL(bl_id);
							}

							$("#container_id" + container_counter).append(span);
						});
					});

					//Display markings
					if(elem.markings != null){
						$("#container_id" + container_counter).append("<div class='prewrap'>" + elem.markings + "</div>");
						if(checkHeight(bl_id) == "false"){
							var div = $("#container_id" + container_counter).find("div:last").remove();
							$tr = cloneBL(bl_id);

							var $new_container_td = $("<td style='max-width: 25%; width: 25%;' id='container_id" + container_counter + "'></td>");
							$new_container_td.appendTo($tr);

							$tr.append($tr);
							$("#container_id" + container_counter).append("<div class='prewrap'>" + elem.markings + "</div>");
						}
					}

					if(elem.uncode != null){
						$tr.find("td:nth-child(1)").append("<div class='prewrap'>" + elem.uncode + "</div>");
					}

					//Display cargo name, cargo short desc
					var $cargo_td = $("<td>" + elem.cargo_name + "<br>" + elem.cargo_desc + "</td>");
					$tr.append($cargo_td);

					if(checkHeight(bl_id) == "false"){
						$tr = cloneBL(bl_id);
					}

					//Display cargo detailed desc
					if(elem.detailed_desc != null){
						// $tr.append("<td></td>"); // Fill up first cell
						$tr.find("td:nth-child(2)").append("<div class='prewrap'>" + elem.detailed_desc + "</div>");
					}

					container_counter++;
					cargo_counter++;

					//Display weight and volume
					var cargo_str = "";

					if (elem.weight == 0 || elem.volume == 0) {
						cargo_str = "";
					} else {
						cargo_str = parseFloat(elem.weight).toFixed(3) + " " + elem.weight_unit + " : " + parseFloat(elem.volume).toFixed(3);
					}

					$tr.append("<td style='max-width: 25%; width: 25%;'>" + cargo_str + "</td>");
				});

			}

			printTotal(bl_id, data.weight, data.volume);
		});
		//end display all cargo

		printPaging(bl_id);
	}

	//Clone BL template and remove all content from bl_content
	function cloneBL(bl_id){
		var $tbody = $("[class^='bl_content_" + bl_id + "']:last");
		$table = $tbody.closest(".table-bl");
		$clone = $table.clone();
		$clone.find("[class^='bl_content_']").html("<tr></tr>"); //Remove all content
		container_counter++;
		$table.after($clone);
		return $("[class^='bl_content_" + bl_id + "']:last").find("tr:last");
	}

	//Print paging
	function printPaging(bl_id){
		var $tbody = $("[class*='bl_content_" + bl_id + "']");
		var total_page = $tbody.length;

		$tbody.each(function(index, elem){
			$(this).closest("table").closest("td").find(".div-paging").text(index+1 + " OF " + total_page);
		});
	}

	//Print total weight and volume
	function printTotal(bl_id, weight, volume){
		var $tbody = $("[class^='bl_content_" + bl_id + "']:last");

		var html = "<tr>" + 
		"<td></td>" +
		"<td></td>" +
		"<td><div style='width: 200px' class='sum_box text-left'>" + weight + " : " + volume + "</div></td>";
		$tbody.append(html);
	}

});