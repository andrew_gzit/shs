var reservation_id = 0;
var edit = false;

function updateVoyage(id){
	if($("#filter_pol").length == 1 && $("#filter_pod").length == 1){
		var pol = $("#filter_pol").val();
		var pod = $("#filter_pod").val();
		var date = $("#txt_date").val();

		var url = "/get-voyage/";

		if(edit){
			url = "/get-voyage1/" + voy_id + "/";
		}

		if(pol != pod){
			url += pol + "/" + pod + "/" + date.replace(/\//g, "-");

			if(typeof voy_id !== 'undefined'){
				url += "?voyage=" + voy_id;
			}

			if(reservation_id != 0){
				url += "?reservation=" + reservation_id;
			}

			var selected_pod = $("#filter_pod").val();

			$.get(url, function(data){
				var html = "";
				if(data.success){
					var results = data.voyages;
					results.forEach(function(elem, index){

						var data_transhipment = [];

						if(elem.destinations.length > 2){
							var dests = elem.destinations;
							var start = false;
							dests.forEach(function(elem1, index1){
								if(index1 != 0){

									//Dont show port that is POD
									if(elem1.get_port.id == selected_pod){
										start = true;
									}

									if(start){
										var temp_obj = {};
										temp_obj.name = elem1.get_port.name;
										temp_obj.code = elem1.get_port.code;
										temp_obj.id = elem1.get_port.id;
										data_transhipment.push(temp_obj);
									}
								}
							});

							if(data_transhipment.length > 0){
								data_transhipment = JSON.stringify(data_transhipment);
							} else {
								data_transhipment = "";
							}
							
						} else {
							data_transhipment = "";
						}

						html += "<tr>" +
						"<td width='1%'><input type='radio' value='" + elem.id + "' name='voyage_id' data-transhipment='" + data_transhipment + "' style='margin-top: 6px;'></td>" +
						"<td>" + elem.name + "</td>" +
						"<td>" + elem.voyage_id + "</td>" +
						"<td>" + elem.eta_text + "</td>" +
						"<td>" + elem.pod_eta + "</td>" +
						"<td>" + elem.used + " / " + elem.dwt +" </td>" +
						"<td>" + elem.port_destinations + "</td>"
						+"</tr>";
					});

					$("#tbody_voy").empty().append(html);
					$("input[name='voyage_id']:first").trigger("click");

					//Select FPD as POD by default
					$("#ddl_fpd").val(selected_pod).trigger("change");
				} else {
					html = "<tr><td colspan='7' class='text-center'>No voyage found.</td></tr>";
					$("#tbody_voy").empty().append(html);
				}
			});
		} else {
			if(id == "filter_pol"){
				$("#filter_pol").val(old_value_pol).trigger("change");
			} else if (id == "filter_pod") {
				$("#filter_pod").val(old_value_fpd).trigger("change");
			}
			swal("Same port selected", "Select a different port to filter the voyage list", "warning");
		}
	}
};

var old_value_pol = $("#filter_pol").val();
var old_value_fpd = $("#filter_pod").val();

//Moved to cargo page only
$("#btn-confirm").click(function(e){
	var $btn = $(this);

	var valid = true;

	if($("input[name='voyage_id']:checked").length == 0){
		swal("Oops", "Voyage has not been selected yet.", "warning");
		valid = false;
		return false;
	}

	$(".required").each(function(index, elem){
		if($(this).val().trim() == ""){
			$(this).focus();
			swal("Oops", $(this).attr("data-title") + " is required.", "warning");
			valid = false;
			return false;
		}
	});

	if($("select[name='pod']").val() == $("select[name='pol']").val()){
		swal("Oops!", "Port of Loading and Port of Discharge cannot be the same!", "warning");
		valid = false;
		return false;
	}

	if($("select[name='fpd']").val() == $("select[name='pol']").val()){
		swal("Oops!", "Final Port of Discharge and Port of Loading cannot be the same!", "warning");
		valid = false;
		return false;
	}

	if(valid){
		swal({
			title: "Comfirm booking",
			text: "Future edits made to this booking will be stored as amendments",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: 'Continue',
			cancelButtonText: 'Cancel',
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				$("#txt_button").val("confirm");
				$btn.closest("form").submit();
			}
		});
	}
});

$("#ddl_fpd").change(function(){
	if($(this).val() == "none" || $(this).val() == $("#filter_pod").val()){
		$("#div_eta_fpd").hide();
		$("#mother_vessel").prop("disabled", true);
		$("#mother_vessel").val("-").trigger("change");
	} else {
		//Show ETA FPD
		$("#div_eta_fpd").show();
		$("#mother_vessel").prop("disabled", false).find("option:first-child").prop("selected", true).select2().trigger('change');
	}
});

//check restriction
function checkRestriction(company_id, voyage_id, input_id){
	$.get("/check-restriction/" + company_id + "/" + voyage_id, function(data2){
		if(data2.success){
			if(data2.err.length > 0){
				var msg = data2.err.join("<br>");
				var str = "shipper's";

				if(input_id == "txt_bookingparty"){
					str = "booking party's"
				} else if(input_id == "both") {
					str = "both shipper's and booking party's"
				}

				swal({
					title: 'Oops!',
					html: "Selected voyage has violated the " + str + " following restriction(s): <br><br>" + msg + "<br><br> Press <strong>Proceed</strong> to continue",
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: 'Proceed',
					reverseButtons: true,
					allowOutsideClick: false
				}).then((result) => {
					if(result.value) {

					} else {
						//Remove selected voyage
						$("input[name='voyage_id']").prop("checked", false);
					}
				});
			}
		} else {
			alert("Unable to retrieve shipper's restrictions");
		}
	});
}