$("input").attr("autocomplete", "off")

$(document).on("keydown", "input[data-type='number']", function(e){
	if(!((e.keyCode > 95 && e.keyCode < 106) || (e.keyCode > 47 && e.keyCode < 58) || e.keyCode == 8 || e.keyCode == 9)) {
		e.preventDefault();
		return false;
	}
});

$("#btn_charges").click(function(){
	var $row = $(".unit_row:last");
	var $clone = $row.clone();
	$clone.find("input").val("");
	$clone.find(".div_price label").remove();
	$clone.find(".div_unit label").remove();
	$row.after($clone);

	$(".lbl-remove").show();
});

$(document).on("click", ".lbl-remove", function(){
	$(this).closest(".unit_row").remove();
	if($(".unit_row").length == 1){
		$(".lbl-remove").hide();
	}
});

// $("#btn_save").click(function(){
// 	var charges = [];

// 	var valid = true;

// 	$(".unit_row").each(function(){
// 		var obj = {};
// 		obj.unitprice = $(this).find("input[name='txt_unitprice[]']").val();
// 		if(parseInt(obj.unitprice) < 1 || isNaN(parseInt(obj.unitprice))){
// 			swal("Oops", "Unit price cannot be empty!", "warning");
// 			valid = false;
// 			return false;
// 		}

// 				// obj.unitsize = $(this).find("select[name='ddl_unitsize[]']").val();
// 				obj.unit = $(this).find("select[name='ddl_unit[]']").val();
// 				// obj.unittext = $(this).find("select[name='ddl_unitsize[]']").find("option:selected").text();

// 				var exist = containsObject(obj, charges);

// 				if(!exist){
// 					charges.push(obj);
// 				} else {
// 					valid = false;
// 					return false;
// 				}
// 			});

// 	if(valid){
// 		$("#txt_charges").val(JSON.stringify(charges));
// 		$("#form_charge").submit();
// 	}
// });

$("#btn_cancel").click(function(){
	swal({
		title: 'Are your sure?',
		text: 'All your current progress will be lost.',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Proceed',
		reverseButtons: true
	}).then((result) => {
		if(result.value) {
			window.location.href = '/maintenance/charges/';
		}
	});
});

$("#btn_save").click(function(){
	var chargesArr = [];
	var valid = true;
	
	$(".ddl_unit").each(function(){
		var selected_val = $(this).val();
		if($.inArray(selected_val, chargesArr) > -1){
			swal("Oops", "Duplicate unit found.", "warning");
			valid = false;
			return false;
		} else {
			chargesArr.push(selected_val);
		}
	});

	if(valid){
		$("#form_charge").submit();
	}
});

function containsObject(obj, list) {
	var i;
	for (i = 0; i < list.length; i++) {
		if (list[i].unitsize == obj.unitsize && list[i].unit == obj.unit) {
			swal("Oops", "Duplicate unit found.", "warning");
			return true;
		}
	}
	return false;
}