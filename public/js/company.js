var edit_elem = "";
var edit_email_elem = "";
var adrs_count = 0;

function checkShipperConsignee(){
	var shipper = $("input[name='radio_company_type[]'][value='0']:checked").length;
	var consignee = $("input[name='radio_company_type[]'][value='1']:checked").length;
	if(shipper == 1 && consignee == 1){
		$("#div_shipperdetails").show();
		$("#div_shipperconsignee").show();

		$("#btn_proceed").show();
	} else if(shipper = 1 && consignee == 0){
		$("#div_shipperdetails").hide();
		$("#div_shipperconsignee").hide();
		// $("#div_restrictionrow").hide();
		$("#btn_proceed").show();
	} else {
		$("#div_restrictionrow").hide();
		$("#btn_proceed").hide();
	}
}

$("input[data-type='alphadot']").blur(function(){
	var form_group = $(this).closest(".form-group");
	var input = $(this);
	if(input.val() != ""){
		if(!IsAlphaDot(input.val())){
			form_group.addClass("has-error");
			if(form_group.find(".text-danger").length == 0){
				form_group.append("<span class='text-danger'>Company Name may only contain alphabetical characters, brackets, hypens and spacing.</span>");
			} else {
				form_group.find("span").text("Company Name may only contain alphabetical characters, brackets, hypens and spacing.");
			}
		} else {
			form_group.removeClass("has-error");
			form_group.find(".text-danger").remove();
		}
	} else {
		form_group.removeClass("has-error");
		form_group.find(".text-danger").remove();
	}
});

$("input[name='txt_code']").blur(function(){
	var form_group = $(this).closest(".form-group");
	var input = $(this);
	if(input.val() != ""){
		if(!IsAlphaNoSpace(input.val())){
			form_group.addClass("has-error");
			if(form_group.find(".text-danger").length == 0){
				form_group.append("<span class='text-danger'>Company Name may only contain alphanumeric characters.</span>");
			} else {
				form_group.find("span").text("Company Name may only contain alphanumeric characters.");
			}
		} else {
			form_group.removeClass("has-error");
			form_group.find(".text-danger").remove();
		}
	} else {
		form_group.removeClass("has-error");
		form_group.find(".text-danger").remove();
	}
});

$("input[data-type='alphadash']").blur(function(){
	var form_group = $(this).closest(".form-group");
	var input = $(this);
	if(input.val() != ""){
		if(!IsAlphaDash(input.val())){
			form_group.addClass("has-error");
			if(form_group.find(".text-danger").length == 0){
				form_group.append("<span class='text-danger'>Company Name may only contain alphanumeric characters and hypens.</span>");
			} else {
				form_group.find("span").text("Company Name may only contain alphanumeric characters and hypens.");
			}
		} else {
			form_group.removeClass("has-error");
			form_group.find(".text-danger").remove();
		}
	} else {
		form_group.removeClass("has-error");
		form_group.find(".text-danger").remove();
	}
});

$("input[name='radio_company_type[]']").change(function(e){

	if($(this).val() == "3"){
		$("input[name='radio_company_type[]']:not([value='3'])").prop("checked", false).removeAttr("checked").trigger("change");
	}

	var $checked = $("input[name='radio_company_type[]']:checked");
	if($checked.length == 0){
		$(this).prop("checked", true);
	} else {
		var shipper = false;
		var consignee = false;
		var agent = false;
		var shipperconsignee = false;

		$checked.each(function(index, elem){
			var company_type = $(elem).val();
			switch(company_type){
				case "0":
				shipper = true;
				break;
				case "1":
				consignee = true;
				if(shipper){
					shipperconsignee = true;
				}
				break;
				case "3":
				agent = true;
				break;
			}
		});

		if(shipper || shipperconsignee){
			$("#div_settings").show();
			$("#div_restrictionrow").show();
			$("#div_shipperradio").show();
			$("#div_agent").hide();
			$("#btn_proceed").show();
			$("input[name='radio_company_type[]'][value='3']").prop("checked", false);
		} else if(consignee) {
			$("#div_settings").hide();
			$("#div_restrictionrow").hide();
			$("#div_shipperradio").hide();
			$("#div_agent").hide();
			$("#btn_proceed").hide();
			$("input[name='radio_company_type[]'][value='3']").prop("checked", false);
		} else if(agent) {
			$("#div_settings").show();
			$("#div_restrictionrow").hide();
			$("#div_shipperradio").hide();
			$("#div_agent").show();
			$("#btn_proceed").hide();
		}
	}

	// var company_type = $(this).val();
	// var select = true;
	// if($(this).prop("checked") == false){
	// 	select = false;
	// 	if($("input[name='radio_company_type[]'][value='1']").prop("checked")){
	// 		$("#div_shipperradio").hide();
	// 		$("#div_restrictionrow").hide();
	// 	}

	// 	$("#div_shipperdetails").hide();
	// }

	// $(".div_companytype").hide();

	// if(select){
	// 	switch(company_type){
	// 		case "0":
	// 		$("input[name='radio_company_type[]'][value='3']").prop("checked", false);
	// 		$("#div_shipper").show();
	// 		$("#div_shipperradio").show();
	// 		$("#div_restrictionrow").show();
	// 		checkShipperConsignee();
	// 		break;
	// 		case "1":
	// 		$("input[name='radio_company_type[]'][value='3']").prop("checked", false);
	// 		checkShipperConsignee();
	// 		if($("input[name='radio_company_type[]'][value='0']:checked").length == 0){
	// 			$("#div_shipper").hide();
	// 			$("#div_shipperradio").hide();
	// 		}
	// 		break;
	// 		case "3":
	// 		$("input[name='radio_company_type[]']:not([value='3'])").prop("checked", false);

	// 		$("#div_agent").show();
	// 		$("#div_shipperradio").hide();
	// 		$("#div_restrictionrow").hide();
	// 		$("#div_shipperdetails").hide();
	// 		$("#btn_proceed").hide();
	// 		break;
	// 	}
	// }

	// $(".div_companytype").hide();
	// var divShow = $(this).attr("data-class");
	// $("#" + divShow).show();

	// if(divShow == "div_shipperconsignee" || divShow == "div_shipper"){
	// 	$("#div_restrictionrow").show();
	// 	$("#div_shipperradio").show();
	// } else {
	// 	$("#div_restrictionrow").hide();
	// 	$("#div_shipperradio").hide();
	// }

	// if(divShow == "div_shipperconsignee"){
	// 	$("#div_shipperdetails").show();
	// } else {
	// 	$("#div_shipperdetails").hide();
	// }

	// $("html, body").animate({ scrollTop: $(document).height() }, 1000);
});

$("input[data-type='alpha']").keydown(function(e){
	if(e.keyCode > 96 && e.keyCode < 123){
		event.preventDefault();
	}

	if(! ((e.keyCode > 64 && e.keyCode < 91) || (e.keyCode==32) || (e.keyCode==0) || (e.keyCode==9) || (e.keyCode==8)) ){
		event.preventDefault();
	}
});

// var options =  {
//   onKeyPress: function(cep, e, field, options) {
//     var masks = ['+6000-0000000', '00-00000000'];
//     console.log(e);
//     var mask = (cep.length <= 10) ? masks[1] : masks[0];
//     $(e.target).mask(mask, options);
// }};

// // $('.crazy_cep').mask('00000-000', options);

// $("input[data-type='telephone']").mask('000000000000', options);

$("input[data-type='telephone']").blur(function(){
	var form_group = $(this).closest(".form-group");
	var input = $(this);
	var text = input.attr('data-title');
	if(input.val() != ""){
		if(!IsTelephone(input.val())){
			form_group.addClass("has-error");
			if(form_group.find(".text-danger").length == 0){
				input.after("<p class='text-danger'>-</p>");
			}
			form_group.find(".text-danger").text(text);
		} else {
			form_group.removeClass("has-error");
			form_group.find(".text-danger").remove();
		}
	} else {
		form_group.removeClass("has-error");
		form_group.find(".text-danger").remove();
	}
});

$("input[name='txt_vesselage']").blur(function(){
	var number = parseInt($(this).val());
	if(number != 0 && !isNaN(number)){
		$(this).val(number);
	} else {
		$(this).val("");
	}
});

$("#btn_add_email").click(function(){
	var adrs = $("#txt_email").val().trim();

	var path = window.location.pathname;
	if(path == "/maintenance/base-company" && $(".email-adrs-row").length == 1){
		swal("Oops!", "Only <strong>1</strong> email address allowed for base company.", "warning");
		return;
	}

	if(adrs != "" && adrs.match(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)){
		var html = emailAdrsHTML(adrs);
		$("#div_emails").append(html);
		$("#txt_email").val("").focus();
	} else {
		$("#txt_email").focus();
		if(adrs.length > 0){
			swal("Oops!", "Email address entered is incorrect.", "warning");
		}
	}
});

$("#btn-edit-email-addr-save").click(function(){
	var adrs = $("#txt_email").val().trim();
	if(adrs != "" && adrs.match(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)){
		var html = emailAdrsHTML(adrs);
		$("#div_emails").append(html);
		$("#txt_email").val("").focus();
	} else {
		$("#txt_email").focus();
		if(adrs.length > 0){
			swal("Oops!", "Email address entered is incorrect.", "warning");
		}
	}
});

$("#btn_add_flag").click(function(){
	var $clone = $(".row_flag:first").clone();
	$clone.find('select').val(0).trigger("change");
	$(".row_flag:last").after($clone);
	$(".flag-group").find(".lbl-remove").show();
});

$("#btn_add_vessel").click(function(){
	var $clone = $(".row_vessel:first").clone();
	$clone.find('select').val(0).trigger("change");
	$(".row_vessel:last").after($clone);
	$(".vessel-group").find(".lbl-remove").show();
});

$(document).on("click", ".lbl-remove", function(){
	$(this).closest(".row").remove();
	if($(".flag-group").find(".row_flag").length == 1){
		$(".flag-group").find(".lbl-remove").hide();
	}
	if($(".vessel-group").find(".row_vessel").length == 1){
		$(".vessel-group").find(".lbl-remove").hide();
	}
});

$(document).on("click", ".adrs-del", function(){
	$(this).closest(".col-md-6").remove();
});

$(document).on("click", ".btn-del-addr", function(){
	$(this).closest(".col-lg-12").remove();
	adrs_count--;

	if(adrs_count <= 0){
		adrs_count = 0;
		$("#txt_address_desc").val("MAIN");
	} else {
		$("#txt_address_desc").val("BRANCH " + adrs_count);
	}
});

$(document).on("click", ".btn-del-email-addr", function(){
	$(this).closest(".col-lg-12").remove();
});

$(".btn-save").click(function(){

	var valid = true;

	if($("input[name='txt_name']").val() == ""){
		$("input[name='txt_name']").focus();
		valid = false;
		return false;
	}

	if($("input[name='txt_code']").length != 0){
		if(!IsAlphaNoSpace($("input[name='txt_code']").val()) && $("input[name='txt_code']").val() != ""){
			$("input[name='txt_code']").focus();
			valid = false;
			return false;
		}
	}

	$("input[data-type='alphadash']").each(function(){
		if(!IsAlphaDash($(this).val()) && $(this).val() != ""){
			$(this).focus();
			valid = false;
			return false;
		}
	});

	$("input[data-type='telephone']").each(function(){
		if(!IsTelephone($(this).val()) && $(this).val() != ""){
			$(this).blur().focus();
			valid = false;
			return false;
		}
	});

	// //Validate tel/fax number
	// $(".telephone").each(function(){
	// 	if($(this).val().length < 9){
	// 		$(this).focus();
	// 		swal("Oops!", $(this).attr("data-title"), "warning");
	// 		valid = false;
	// 		return;
	// 	}
	// });

	if(valid == false){
		return;
	}

	//Save company addresses
	var addressArr = [];
	$(".adrs-row").each(function(){
		addressArr.push($(this).find("span").html());
	});

	if(addressArr.length < 1){
		swal("Oops!", "Company address cannot be empty!", "warning");
		return;
	}

	$("#txt_addressArr").val(htmlEntities(JSON.stringify(addressArr)));

	// //Save email addresses
	var emailArr = [];
	$(".email-adrs-row").each(function(){
		emailArr.push($(this).find("span").html());
	});

    /*
	if(emailArr.length < 1){
		swal("Oops!", "Email address cannot be empty!", "warning");
		return;
    }
    */

	$("#txt_emailArr").val(JSON.stringify(emailArr));

	if(valid == false){
		return;
	}

	var vesselArr = [];
	$("select[name='ddl_vesselname[]']").each(function(){
		var name = $(this).val();
		if(vesselArr.includes(name)){
			swal("Oops!", "Duplicate of vessel name in restriction found!", "warning");
			valid = false;
			return;
		} else {
			vesselArr.push(name);
		}
	});

	var flagArr = [];
	$("select[name='ddl_flag[]']").each(function(){
		var name = $(this).val();
		if(flagArr.includes(name)){
			swal("Oops!", "Duplicate of flag in restriction found!", "warning");
			valid = false;
			return;
		} else {
			flagArr.push(name);
		}
	});

	if(valid){
		// $update = $("input[name='radio_update[]'").is(":checked").length;
		// $("#txt_emailArr").val(JSON.stringify(emailArr));
		$("#txt_submittype").val($(this).val());
		$("input[name='radio_company_type[]']:not(:checked)").removeAttr("checked");
		$("form").submit();
	}
});

$("#btn_add_address").click(function(){

	var path = window.location.pathname;
	if(path == "/maintenance/base-company" && $(".adrs-row").length == 1){
		swal("Oops!", "Only <strong>1</strong> address allowed for base company.", "warning");
		return;
	}

	if(validateAddress()){
		var desc = $("#txt_address_desc").val();
		var address1 = $("#txt_address1").val();
		var address2 = $("#txt_address2").val();
		var address3 = $("#txt_address3").val();

		var adrs = "";

		if(desc){
			adrs += "<strong>" + desc.trim() + "</strong><br>";
		}

		adrs += address1.trim() + "<br>";

		if(address2){
			adrs += address2.trim() + "<br>";
		}
		if(address3){
			adrs += address3.trim();
		}
		var html = adrsHTML(adrs);
		$("#div_addresses").append(html);

		$("[id^=txt_address]").val('');

		adrs_count++;

		$("#txt_address_desc").val("BRANCH " + adrs_count);

	}
});

$(document).on("click", ".btn-edit-addr", function(){

	$("#txt_edit_address_desc, #txt_edit_address0, #txt_edit_address1, #txt_edit_address2").val("");

	edit_elem = $(this).closest(".adrs-row").find("span");

	var address = edit_elem.html();

	if(edit_elem.find("strong").length == 1){
		$("#txt_edit_address_desc").val(edit_elem.find("strong").text());
		address = address.substring(address.indexOf('</strong><br>') + 13);
	}

	var addressArr = address.split("<br>");

	for (i = 0; i < addressArr.length; i++) {
		if(addressArr[i] != ""){
			$("#txt_edit_address" + i).val(addressArr[i]);
		}
	};

	$("#modal-edit-addr").modal("toggle");
});

$(document).on("click", ".btn-edit-email-addr", function(){

	edit_email_elem = $(this).closest(".email-adrs-row").find("span");

	var email = edit_email_elem.html();
	$("#txt_edit_email").val(email);

	$("#modal-edit-email-addr").modal("toggle");
});

$("#btn-edit-email-addr-save").click(function(){
	var adrs = $("#txt_edit_email").val().trim();
	if(adrs != "" && adrs.match(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)){
		edit_email_elem.html(adrs);
		$("#modal-edit-email-addr").modal("toggle");
	} else {
		$("#txt_edit_email").focus();
		if(adrs.length > 0){
			swal("Oops!", "Email address entered is incorrect.", "warning");
		}
	}
});

$("#btn-edit-addr-save").click(function(){
	if(validateEditAddress()){
		var desc = $("#txt_edit_address_desc").val();
		var address1 = $("#txt_edit_address0").val();
		var address2 = $("#txt_edit_address1").val();
		var address3 = $("#txt_edit_address2").val();

		var adrs = "";

		if(desc){
			adrs += "<strong>" + desc.trim() + "</strong><br>";
		}

		adrs += address1.trim() + "<br>";

		if(address2){
			adrs += address2.trim() + "<br>";
		}
		if(address3){
			adrs += address3.trim();
		}

		edit_elem.html(adrs);

		$("#modal-edit-addr").modal("toggle");
	}
});

$('.btn-cancel').click(function() {
	swal({
		title: 'Are your sure?',
		text: 'All your current progress will be lost.',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Proceed',
		reverseButtons: true
	}).then((result) => {
		if(result.value) {
			window.location.href = '/maintenance/companies';
		}
	});
});
