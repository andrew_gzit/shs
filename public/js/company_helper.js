//Check if string consist of alphabet only
function IsAlpha(val){
	return val.match(/^[A-Za-z ]+$/);
}

function IsAlphaNoSpace(val){
	return val.match(/^[A-Za-z0-9]+$/);
}

function IsAlphaDot(val){
	return val.match(/^[a-zA-Z.\-() \s]+$/);
}

function IsAlphaDash(val){
	return val.match(/^[a-zA-Z0-9\-]+$/);
}

function IsTelephone(val){
	return val.match(/^[+]{0,1}[(]{0,1}[0-9]{1,4}[)]{0,1}[-]{0,1}[\s\./0-9]*$/);
}

function adrsHTML(address) {
	return '<div class="col-lg-12"><div class="adrs-row"><span>' + address + '</span><div class="adrs-buttons"><button class="btn btn-sm btn-success btn-edit-addr m-r-5" type="button"><i class="fa fa-edit"></i></button><button class="btn btn-sm btn-danger btn-del-addr" type="button"><i class="fa fa-trash"></i></button></div></div></div>';
}

function emailAdrsHTML(address) {
	return '<div class="col-lg-12"><div class="email-adrs-row"><span>' + address + '</span><div class="adrs-buttons"><button class="btn btn-sm btn-success btn-edit-email-addr m-r-5" type="button"><i class="fa fa-edit"></i></button><button class="btn btn-sm btn-danger btn-del-email-addr" type="button"><i class="fa fa-trash"></i></button></div></div></div>';
}

//Function to set company address
function setAddress(obj){
	obj.forEach(function(elem, index){
		var addr = "";
		if(elem.address != null){
			if(elem.description != null){
				addr = "<strong>" + elem.description + "</strong><br>";
			}
			addr += elem.address;
		}
		// var html = "<div class='col-md-6'><div class='adrs'><span>" + addr + "</span><div class='adrs-edit'><i class='fas fa-edit'></i></div><div class='adrs-del'><i class='fas fa-times'></i></div></div></div>";
		var html = adrsHTML(addr);
		adrs_count++;
		$("#div_addresses").append(html);
	});
}

//Function to set company email
function setEmail(obj){
	obj.forEach(function(elem, index){
		var email = (elem.email != null) ? elem.email : elem;
		// var html = "<div class='col-md-6'><div class='email-adrs'><span>" + email + "</span><div class='adrs-edit'><i class='fas fa-edit'></i></div><div class='adrs-del'><i class='fas fa-times'></i></div></div></div>";
		var html = emailAdrsHTML(email);
		$("#div_emails").append(html);
	});
}

//Escape old input
function unescapeHTML(escapedHTML) {
	return escapedHTML.replace(/&quot;/g, '\"').replace(/&lt;/g,'<').replace(/&gt;/g,'>').replace(/&amp;/g,'&');
}

//Validate entered address
function validateAddress(){
	var valid = true;
	$(".txt-address-required").each(function(){
		if($(this).val().trim() == ""){
			$(this).focus();
			swal("Field required", "Please " + $(this).attr("placeholder").toLowerCase(), "warning");
			valid = false;
			return valid;
		}

		if($(this).attr("data-type")){
			if($(this).attr("data-type") == "alpha"){
				if(!IsAlpha($(this).val())){
					valid = false;
					swal("Oops!", $(this).attr("data-field") + " is entered incorrectly. Please check.", "warning");
					return valid;
				}
			}
		}
	});
	return valid;
}

//Validate entered address (EDIT)
function validateEditAddress(){
	var valid = true;
	$(".txt-edit-address-required").each(function(){
		if($(this).val().trim() == ""){
			swal("Field required", "Please " + $(this).attr("placeholder").toLowerCase(), "warning");
			$(this).focus();
			valid = false;
			return valid;
		}
	});
	return valid;
}

function removeAddress($elem){
	$elem.closest(".col-md-6").remove();
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}