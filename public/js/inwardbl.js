function updateAddress(elem, id){
	$.ajax({
		type: 'POST',
		url: '/bl/create/company/address',
		data: {'id' : id},
		success: function(data) {
			if(data.success) {
				var html = '';

				for(var i = 0; i < data.result.length; i++) {
					var adrs_desc = "";

					if(data.result[i].description != null){
						html += '<option value="' + data.result[i].id + '">' + data.result[i].description + " - " + data.result[i].address.replace(/<br>/g, ', ') + '</option>';
					} else {
						html += '<option value="' + data.result[i].id + '">DEFAULT</option>';
					}
				}

				elem.html(html);
				elem.siblings("textarea").html(data.result[0].address.replace(/<br>/g, '&#13;&#10;'));
			}
		}
	});
}

function validateDecimal(elem, num){
	var value = parseFloat(elem.val());
	if(isNaN(value) || value > 999.99){
		elem.val("");
	} else {
		elem.val(value.toFixed(num));
	}
}

$("#txt_blno").bind('keypress', function (e) {
	var regex = new RegExp("^[a-zA-Z0-9\b]+$");
	var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);
	if (!regex.test(key)) {
		e.preventDefault();
		return false;
	}
});

$("input[name='txt_blno']").change(function(){
	var elem = $(this);
	elem.val(elem.val().trim());
});

$(".ddl-companyname").keyup(function(){
	console.log($(this).val());
	if($(this).val() != ""){
		$(this).closest(".row").find(".bl_address, textarea").removeAttr("disabled");
	} else {
		$(this).closest(".row").find(".bl_address, textarea").prop("disabled", true);
	}
});

$('#pol_id, #pod_id, #fpd_id').change(function() {
	var name = $(this).find('option:selected').attr('data-name');
	var loc = $(this).find('option:selected').attr('data-location');

	if(name == undefined) name = "";

	if(loc == undefined) loc = "";

	//Insert value of location
	$("input[data-id='" + this.id + "']").val(loc);

	$(this).closest(".d-flex").siblings("span").html('<i>' + name + '</i>');
});

$('.bl_address').change(function() {
	var address_id = $(this).val();
	var $this = $(this);
	$.get('/get-address/' + address_id, function(data){
		if(data.success){
			var address = data.address.address;
			address = address.replace(/<br[^>]*>/gi, "\n");
			$this.next().html(address);
		} else {
			swal("Oops!", "Address not found!", "error");
		}
	});
});

$("input[name='txt_weight']").blur(function(){
	validateDecimal($(this), 3);
});

$("input[name='txt_volume']").blur(function(){
	validateDecimal($(this), 4);
});

$('#btn_cancel').click(function() {
	swal({
		title: 'Are your sure?',
		text: 'All your current progress will be lost.',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Proceed',
		reverseButtons: true
	}).then((result) => {
		if(result.value) {
			window.location.href = '/bl?tab=inward';
		}
	});
});