$("#form_format").click(function(){
	$(".loader").fadeIn(500);
	setTimeout(function(){
		setFormat(true);
		window.print();
	}, 1000);
	$(".loader").fadeOut(500);
});

$("#plain_format").click(function(){
	$(".loader").fadeIn(500);
	setTimeout(function(){
		setFormat(false);
		window.print();
	}, 1000);
	$(".loader").fadeOut(500);
});

function setFormat(bool){
	if(bool){
		$("#bl_print_css").text(
			"@media print{" +
			"body {" + 
			"padding: 20mm 0 0 0 !important" +
			"}" +
			"td[class^='container_id'] {" +
			"width: 1.8in !important" +
			"}" +
			"}"
			);
		$(".bl-label, .print-hidden").addClass("opacity-0");
		$(".table-bl, .table-so").addClass("table-form-format");
	} else {
		$("#bl_print_css").text("");
		$(".bl-label, .print-hidden").removeClass("opacity-0");
		$(".table-bl").removeClass("table-form-format");
	}
}