function unescape(escapedHTML) {
	return escapedHTML.replace(/&quot;/g, '\"').replace(/&lt;/g,'<').replace(/&gt;/g,'>').replace(/&amp;/g,'&').replace(/\r\n/g, '\\n').replace(/\n/g, '\\n');
}

$("body").keydown(function(e){

	var keyCode = e.keyCode || e.which;

	var valid = false;

	if(keyCode >= 112 && keyCode <= 123){
		var valid = true;
		e.preventDefault();
	}

	var text = "";
	switch(keyCode){
		case 112:
		text = shortcuts['F1'].description;
		break;
		case 113:
		text = shortcuts['F2'].description;
		break;
		case 114:
		text = shortcuts['F3'].description;
		break;
		case 115:
		text = shortcuts['F4'].description;
		break;
		case 116:
		text = shortcuts['F5'].description;
		break;
		case 117:
		text = shortcuts['F6'].description;
		break;
		case 118:
		text = shortcuts['F7'].description;
		break;
		case 119:
		text = shortcuts['F8'].description;
		break;
		case 120:
		text = shortcuts['F9'].description;
		break;
		case 121:
		text = shortcuts['F10'].description;
		break;
		case 122:
		text = shortcuts['F11'].description;
		break;
		case 123:
		text = shortcuts['F12'].description;
		break;
	}

	if(valid){
		//Check if current focused element is editablecontent, if it is, dont insert text if more than 10 rows
		if($(":focus").is(".editablecontent")){

			// var lht = parseInt($(":focus").height());
			// console.log(lht);
			// var lines = $(":focus").attr('scrollHeight') / lht;

			// var txt_array = $(":focus").val().split("\n");
			// if(txt_array.length > 15){
			// 	return false;
			// } else {

				// Replace new line with break for contenteditable
				text = text.replace(/(?:\r\n|\r|\n)/g, '<br>');

				$(":focus").insertAtCaret(text + "\n");
			// }
		}
	}
});