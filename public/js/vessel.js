

$(document).ready(function() {

	// $(document).on('focus', '.datetimepicker', function() {
	// 	$(this).datetimepicker({
	// 		format: 'YYYY',
	// 		viewMode: 'years',
	// 		dayViewHeaderFormat: 'YYYY',
	// 		maxDate: 'now'
	// 	});
	// });

	$(".yearpicker").datepicker({
		autoclose: true,
		viewMode: 'years',
		minViewMode: 'years',
		format: 'yyyy',
		startDate: '1900',
		endDate: '+0d',
	});

	$('#vessel_type').change(function(){
		if($(this).val() == "General Cargo Vessel") {
			$("#radio_semi_yes").attr('disabled', false);
			$("#radio_semi_yes").prop('checked', true);
			$("#radio_semi_no").attr('disabled', false);
			$("#radio_semi_no").prop('checked', true);

			$('#default_shipment option').first().prop('disabled', true);
			$('#default_shipment option').first().text('FCL (unavailable for GCV w/o semi-container)');
			$('#default_shipment option').last().prop('disabled', false);
			$('#default_shipment option').last().text('LCL');
			$('#default_shipment option').last().prop('selected', true);
		} else {
			$("#radio_semi_yes").attr('disabled', true);
			$("#radio_semi_yes").prop('checked', false);
			$("#radio_semi_no").attr('disabled', true);
			$("#radio_semi_no").prop('checked', false);

			$('#default_shipment option').first().prop('disabled', false);
			$('#default_shipment option').first().text('FCL');
			$('#default_shipment option').first().prop('selected', true);
			$('#default_shipment option').last().prop('disabled', true);
			$('#default_shipment option').last().text('LCL (unavailable for CV)');
		}
	});

	$('input[type=radio]').change(function() {
		if($(this).val() == 1) {
			$('#default_shipment option').first().prop('disabled', false);
			$('#default_shipment option').first().text('FCL');
		} else {
			$('#default_shipment option').first().prop('disabled', true);
			$('#default_shipment option').first().text('FCL (unavailable for GCV w/o semi-container)');
			$('#default_shipment option').last().prop('selected', true);
		}
	});

	$('#btn_cancel').click(function() {
		swal({
			title: 'Are your sure?',
			text: 'All your current progress will be lost.',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Proceed',
			reverseButtons: true
		}).then((result) => {
			if(result.value) {
				window.location.href = '/maintenance/vessels/';
			}
		});
	});

});