var portCount = 0;
var editPortID = '';
var editPortCode = '';
var editDataID = 0;
var last_etd = null;
var validate_url = "/validate/createvoyage";

$(document).ready(function() {

    var datepickerobj = {
        format: 'dd/mm/yyyy',
        autoclose: true,
        // startDate: new Date()
    };

    var datepickerobj1 = {
        format: 'dd/mm/yyyy',
        autoclose: true
    };

    var datetimepickerobj = {
        format: 'DD/MM/YYYY HH:mm:ss'
    };

    var datetimepickerobj1 = {
        format: 'DD/MM/YYYY HH:mm:ss'
    };

    $('.datetimepicker').datepicker(datepickerobj);

    $('.datetimepicker1').datepicker(datepickerobj1);

    $('.closingdatetimepicker').datetimepicker(datetimepickerobj);

    $('.closingdatetimepicker1').datetimepicker(datetimepickerobj1);

    // Handles vessel change event
    // Dynamically sets veseel allowable DWT
    // Disabled/enables Closing Date input based on vessel type, E.G: General Cargo Vessel & Container Vessel
    $('#vessel_id').change(function() {
        var dwt = $(this).find('option:selected').attr('data-dwt');
        var type = $(this).find('option:selected').attr('data-type');
        $('#vessel_dwt').val(dwt);

        if (type == 'Container Vessel') {
            $('#closing_timestamp').prop('disabled', false);
        } else {
            $('#closing_timestamp').prop('disabled', true);
        }
    });

    // Handles booking status change event
    // Disables/enables Cancel Remark input based on selected value
    $('#booking_status').change(function() {
        if ($(this).val() == 2) {
            $('#cancel_remark').prop('disabled', false);
        } else {
            $('#cancel_remark').prop('disabled', true);
        }
    });

    // $('#add_port').click(function() {
    // 	var portId = $('#port option:selected').val();
    // 	var portCode = $('#port option:selected').text();
    // 	var portName = $('#port option:selected').attr('data-name');
    // 	var portETA  = $('#port_eta').val();
    // 	var portATA  = $('#port_ata').val();
    // 	var portETD  = $('#port_etd').val();
    // 	var portATD  = $('#port_atd').val();

    // 	if(portCount == 5) {
    // 		showSwal('error', 'Error', 'Maximum number of port callings reached.');
    // 	} else if($('#port').val() == null) {
    // 		showSwal('error', 'Error', 'A <b>port</b> needs to be selected.');
    // 	} else if(portETA == '') {
    // 		showSwal('error', 'Error', 'An <b>ETA</b> needs to be entered.');
    // 	} else if(portETD == '') {
    // 		showSwal('error', 'Error', 'An <b>ETD</b> needs to be entered.');
    // 	} else {
    // 		var err = insertPort(portId, portCode, portName, portETA, portATA, portETD, portATD);
    // 		if(err.toString() == "false"){
    // 			$(".datetimepicker").datepicker("clearDates");
    // 		}
    // 	}
    // });

    $(document).on('click', '.btn-edit1', function() {

        var voy_dest_id = $(this).closest('tr').attr('data-id');

        $.get("/checkPortAssigned/" + voy_dest_id, function(data) {
            if (data.success) {
                $("#modal_edit_port #port").attr('disabled', '').select2({
                    dropdownParent: $("#modal_edit_port")
                });
            } else {
                $("#modal_edit_port #port").removeAttr('disabled').select2({
                    dropdownParent: $("#modal_edit_port")
                });
            }
        });

        $('#modal_edit_port').modal({
            backdrop: 'static',
            keyboard: false
        });

        editPortCode = $(this).closest('tr').find("td:first-child").text
        editPortID = $(this).closest('tr').attr('data-port');

        var elem = $(this).closest("tr");
        // $('#modal_edit_port #port option[value=' + editPortID + ']').prop('hidden', false);
        $('#modal_edit_port #port option[value=' + editPortID + ']').prop('selected', true);
        $('#modal_edit_port #edit_port_eta').val(elem.children('td:nth-child(3)').text());

        var temp_ata = elem.children('td:nth-child(4)').text();
        var temp_atd = elem.children('td:nth-child(6)').text();

        $('#modal_edit_port #edit_port_ata').val(temp_ata == '-' ? '' : temp_ata);
        $('#modal_edit_port #edit_port_etd').val(elem.children('td:nth-child(5)').text());
        $('#modal_edit_port #edit_port_atd').val(temp_atd == '-' ? '' : temp_atd);
        $("#edit_port_eta, #edit_port_ata, #edit_port_etd, #edit_port_atd").datepicker("destroy").datepicker(datepickerobj);

        // if($("#ports_table").find("tr:not(#no_port)").length > 1){
        // 	//Set last ETD if there are other port added with later ATD
        // 	var temp_max_etd = moment($("#ports_table").find("tr:last > td:nth-child(3)").text(), 'DD/MM/YYYY').toDate();
        // 	$("#edit_port_etd, #edit_port_atd").datepicker("setEndDate", temp_max_etd);
        // }
    });

    $('#cancel_edit').click(function() {
        $('#modal_edit_port').modal('hide');
        // $('#port option[value=' + editPortID + ']').prop('hidden', true).prop("disabled", true);
        $("#port").select2();
        $('#modal_edit_port #port').val(null).select2({
            dropdownParent: $("#modal_edit_port")
        });
        $('#modal_edit_port .datetimepicker').each(function() {
            // $(this).data("DateTimePicker").clear();
            $(this).datepicker("clearDates");
        });
        // swal({
        // 	title: 'Are you sure?',
        // 	text: 'All your current progress will be lost.',
        // 	type: 'warning',
        // 	showCancelButton: true,
        // 	confirmButtonText: 'Proceed',
        // 	reverseButtons: true
        // }).then((result) => {
        // 	if(result.value) {
        // 		$('#port option[value=' + editPortID + ']').prop('hidden', true);
        // 		$('#modal_edit_port #port').val(null);
        // 		$('#modal_edit_port .datetimepicker').each(function() {
        // 			$(this).data("DateTimePicker").clear();
        // 		});

        // 		$('#modal_edit_port').modal('hide');
        // 	}
        // });
    });

    // $('#save_port1').click(function() {
    // 	var portId = $('#modal_edit_port #port option:selected').val();
    // 	var portCode = $('#modal_edit_port #port option:selected').text();
    // 	var portName = $('#modal_edit_port #port option:selected').attr('data-name');
    // 	var portETA  = $('#modal_edit_port #edit_port_eta').val();
    // 	var portATA  = $('#modal_edit_port #edit_port_ata').val();
    // 	var portETD  = $('#modal_edit_port #edit_port_etd').val();
    // 	var portATD  = $('#modal_edit_port #edit_port_atd').val();

    // 	if(portETA == ''){
    // 		showSwal('error', 'Error', 'An <b>ETA</b> needs to be entered.');
    // 		return false;
    // 	}

    // 	if(portETD == ''){
    // 		showSwal('error', 'Error', 'An <b>ETD</b> needs to be entered.');
    // 		return false;
    // 	}

    // 	var parsedETA = moment(portETA, 'DD/MM/YYYY');
    // 	var parsedETD = moment(portETD, 'DD/MM/YYYY');

    // 	var parsedATA = moment(portATA, 'DD/MM/YYYY');
    // 	var parsedATD = moment(portATD, 'DD/MM/YYYY');

    // 	var count = $('#ports_table tbody tr').length;
    // 	var error = false;

    // 	if(parsedETD.isBefore(parsedETA)){
    // 		showSwal('error', 'error', 'Port ETD cannot be earlier than ETA');
    // 		return false;
    // 	}

    // 	if(parsedATA.isBefore(parsedETA) || parsedATA.isAfter(parsedETD)){
    // 		showSwal('error', 'error', 'Port ATA is incorrect.');
    // 		return false;
    // 	}

    // 	if(parsedATD.isBefore(parsedETD)){
    // 		showSwal('error', 'error', 'Port ATD cannot be earlier than ETD');
    // 		return false;
    // 	}

    // 	var editPortCode = $('#modal_edit_port #port option[value="' + editPortID + '"]').text();

    // 	for(var i = 0; i < count; i++) {
    // 		var elem = $('#ports_table tbody tr:nth-child(' + (i + 1) + ')');
    // 		var oldPortCode = elem.find('td:nth-child(1)').text();

    // 		console.log(oldPortCode);

    // 		var oldETA = elem.attr('data-eta');
    // 		var oldETD = elem.attr('data-etd');
    // 		var parsedOldETA = moment(oldETA, 'DD/MM/YYYY');
    // 		var parsedOldETD = moment(oldETD, 'DD/MM/YYYY');

    // 		var oldATD = elem.find("td:nth-child(6)").text();

    // 		if(oldPortCode != editPortCode && parsedETA.isAfter(parsedOldETA)){
    // 			//Check if previous row
    // 			if(oldATD != "-"){
    // 				var parsedOldATD = moment(oldATD, 'DD/MM/YYYY');
    // 				if(parsedETA.isBefore(parsedOldATD)){
    // 					showSwal('error', 'error', 'Port\'s ETA cannot be earlier than ATD of port ' + oldPortCode + '.');
    // 					error = true;
    // 					break;
    // 				}
    // 			}
    // 		}

    // 		if(editPortCode != oldPortCode && parsedATD.isAfter(parsedOldETA) && parsedETA.isBefore(parsedOldETA)){
    // 			showSwal('error', 'error', 'Port\'s ATD cannot be later than ETA of port ' + oldPortCode + '.');
    // 			error = true;
    // 			break;
    // 		}

    // 		// Prevent user from selecting duplicate date for port's ETA
    // 		if(portETA == oldETA && portCode != oldPortCode && oldPortCode != editPortCode) {
    // 			showSwal('error', 'error', 'Port\'s ETA is the same port ' + oldPortCode + '.');
    // 			error = true;
    // 			break;
    // 		}

    // 		// Determines if date is older than selected element's ETA
    // 		if(parsedETA.diff(parsedOldETA, 'days') < 0) {

    // 			var elemPrev = $('#ports_table tbody tr:nth-child(' + (i) + ')');
    // 			oldETD = elemPrev.attr('data-etd');
    // 			parsedOldETD = moment(oldETD, 'DD/MM/YYYY');
    // 			oldPortCode = elemPrev.find('td:nth-child(1)').text();

    // 			if(parsedETD.diff(parsedOldETD, 'days') < 0 && parsedETD.isAfter(parsedOldETA)) {
    // 				if(editPortCode != oldPortCode) {
    // 					showSwal('error', 'Error', 'Port\'s ETD is in between ETA and ETD of port ' + oldPortCode + '.');
    // 					error = true;
    // 					break;
    // 				}
    // 			} else {

    // 				console.log("old port" + oldPortCode);
    // 				console.log("edit port" + editPortCode);

    // 				if(parsedETD.isAfter(parsedOldETA) && parsedETA.isBefore(parsedOldETA) && oldPortCode !== editPortCode && parsedOldETD.isAfter(parsedETA)){
    // 					showSwal('error', 'Error', 'Port\'s ETD cannot be later than ETA of port ' + oldPortCode + '.');
    // 					error = true;
    // 					break;
    // 				}

    // 				action = 'prepend';
    // 				break;
    // 			}
    // 		} else {
    // 			if(parsedETA.diff(parsedOldETD, 'days') < 0) {
    // 				if(editPortCode != oldPortCode) {
    // 					showSwal('error', 'Error', 'Port\'s ETA is earlier than ETD of port ' + oldPortCode + '.');
    // 					error = true;
    // 					break;
    // 				}
    // 			}

    // 			if(parsedETD.diff(parsedOldETD, 'days') < 0) {
    // 				if(editPortCode != oldPortCode) {
    // 					showSwal('error', 'Error', 'Port\'s ETD is in between ETA and ETD of port ' + oldPortCode + '.');
    // 					error = true;
    // 					break;
    // 				}
    // 			}
    // 		}
    // 	}

    // 	if(error == false) {
    // 		editDataID = $('#ports_table tbody tr[data-port=' + editPortID + ']').attr('data-id');

    // 		//Minus port count because insertPort will add the port count again
    // 		var gotError = insertPort(portId, portCode, portName, portETA, portATA, portETD, portATD);

    // 		if(gotError !== true){
    // 			portCount--;
    // 			$('#ports_table tbody tr[data-port=' + editPortID + ']').remove();

    // 			// Shows previously hidden selected option and hides current selected option
    // 			$('#port option[value=' + editPortID + ']').prop('hidden', false).removeAttr("disabled");
    // 			$('#port option[value=' + portId + ']').prop('hidden', true).prop("disabled", true);

    // 			$('#modal_edit_port #port option[value=' + editPortID + ']').prop('hidden', false).removeAttr("disabled");
    // 			$('#modal_edit_port #port option[value=' + portId + ']').prop('hidden', true).prop("disabled", true);

    // 			$('#modal_edit_port #port').val(null).select2({
    // 				dropdownParent: $("#modal_edit_port")
    // 			});
    // 			$('#modal_edit_port .datetimepicker').datepicker("clearDate");
    // 			$('#modal_edit_port').modal('hide');
    // 		}

    // 		// last_etd = moment($("#ports_table").find("tr:last > td:nth-child(5)").text(), 'DD/MM/YYYY').toDate();
    // 		// setLastETD(last_etd);
    // 	}
    // });

    // Handles save button click event
    // Checks if all necessary inputs are filled and valid before submitting form
    $('.btn-save').click(function(e) {
        e.preventDefault();

        var isClosingDisabled = $('#closing_timestamp').prop('disabled');

        if ($('#voyage_id').val() == '') {
            showSwal('error', 'Error', '<b>Voyage No</b> can\'t be empty!');
        } else if ($('#vessel_id').val() == null) {
            showSwal('error', 'Error', '<b>Assigned Vessel</b> can\'t be empty!');
        }
        /*else if($('#vessel_scn').val() == '') {
			showSwal('error', 'Error', '<b>Ship Call Number</b> can\'t be empty!');
		}*/
        else if (isClosingDisabled == false && $('#closing_timestamp').val() == '') {
            showSwal('error', 'Error', '<b>Closing Timestamp</b> can\'t be empty!');
        } else if ($('#booking_status').val() == 2 && $('#cancel_remark').val() == '') {
            showSwal('error', 'Error', '<b>Cancel Remark</b> can\'t be empty!');
        } else if (ports.length < 2) {
            showSwal('error', 'Error', '<b>Port(s) Calling</b> must be at least 2!');
        } else {
            var count = $('#ports_table tbody tr').length;
            for (var i = 0; i < count; i++) {
                var elem = $('#ports_table tbody tr:nth-child(' + (i + 1) + ')');
                var details = [];

                details.push(elem.attr('data-id'));
                details.push(elem.attr('data-port'));
                details.push(elem.find('td:nth-child(3)').text());
                details.push(elem.find('td:nth-child(4)').text());
                details.push(elem.find('td:nth-child(5)').text());
                details.push(elem.find('td:nth-child(6)').text());

                var port = details.join(';');

                $('#voyage_form').append('<input type="hidden" name="port[]" value=' + port + ' />');
            }

            //Make sure voyage no and vessel is unique
            var voy_id = $("#voyage_id").val().trim();
            var ves_id = $("#vessel_id").val();

            //Make sure voyage ETA is no earlier than existing voyages ETD/ATD of last port or same as ETA of first port for same vessel
            var pol_eta = $('#ports_table tbody tr:nth-child(2) td:nth-child(3)').text();

            var closing_date = $('#closing_timestamp:enabled').val();

            $(".btn-save").attr("disabled", "");

            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url: validate_url,
                type: 'POST',
                data: {
                    'voyage_id': voy_id,
                    'vessel_id': ves_id,
                    'pol_eta': pol_eta,
                    'closing_date': closing_date
                },
                success: function(data) {
                    if (!data.success) {
                        showSwal('error', 'Oops!', data.msg);
                        $(".btn-save").removeAttr("disabled");
                    } else {

                        //Format date before pass to backend
                        ports.forEach(function(port) {
                            port.eta = (port.eta.isValid()) ? port.eta.format('DD/MM/YYYY') : null;
                            port.ata = (port.ata.isValid()) ? port.ata.format('DD/MM/YYYY') : null;
                            port.etd = (port.etd.isValid()) ? port.etd.format('DD/MM/YYYY') : null;
                            port.atd = (port.atd.isValid()) ? port.atd.format('DD/MM/YYYY') : null;
                        });
                        //Add port obj into input
                        $("input[name='txt_portcalling']").val(JSON.stringify(ports));

                        $('#voyage_form').submit();
                    }
                },
                error: function(data) {
                    showSwal('error', 'Oops!', "Something went wrong!");
                    $(".btn-save").removeAttr("disabled");
                }
            });
        }
    });

    // $("#port_eta").datepicker().on("changeDate", function(e){
    // 	var minDate = new Date(e.date.valueOf());
    // 	var ata_date = $("#port_ata").datepicker("getDate");
    // 	var etd_date = $("#port_etd").datepicker("getDate");

    // 	$("#port_ata").datepicker('setStartDate', minDate);
    // 	if(minDate > ata_date && ata_date != null){
    // 		$("#port_ata").datepicker("setDate", minDate);
    // 	}

    // 	$("#port_etd").datepicker('setStartDate', minDate);

    // 	if(minDate > etd_date && etd_date != null){
    // 		$("#port_etd").datepicker('setDate', minDate);
    // 	}
    // });

    // $("#edit_port_eta").datepicker().on("changeDate", function(e){
    // 	var minDate = new Date(e.date.valueOf());
    // 	var ata_date = null;

    // 	//Check if edit ATA date is not empty/null, if it is then only retrieve the date
    // 	if($("#edit_port_ata").val() != "-" && $("#edit_port_ata").val() != ""){
    // 		ata_date = $("#edit_port_ata").datepicker("getDate");
    // 	}

    // 	var etd_date = $("#edit_port_etd").datepicker("getDate");

    // 	$("#edit_port_ata").datepicker('setStartDate', minDate);
    // 	if(minDate > ata_date && ata_date != null){
    // 		$("#edit_port_ata").datepicker("setDate", minDate);
    // 	}

    // 	$("#edit_port_etd").datepicker('setStartDate', minDate);

    // 	if(minDate > etd_date && etd_date != null){
    // 		$("#edit_port_etd").datepicker('setDate', minDate);
    // 	}
    // });

    // $("#port_etd").datepicker().on("changeDate", function(e){
    // 	var minDate = new Date(e.date.valueOf());
    // 	var atd_date = $("#port_atd").datepicker("getDate");
    // 	$("#port_atd").datepicker('setStartDate', minDate);
    // 	if(minDate > atd_date && atd_date != null){
    // 		$("#port_atd").datepicker("setDate", minDate);
    // 	}

    // 	last_etd = minDate;
    // });

    // $("#edit_port_etd").datepicker().on("changeDate", function(e){
    // 	var minDate = new Date(e.date.valueOf());
    // 	var atd_date = null;

    // 	//Check if edit ATD date is not empty/null, if it is then only retrieve the date
    // 	if($("#edit_port_atd").val() != "-" && $("#edit_port_atd").val() != ""){
    // 		$("#edit_port_atd").datepicker("getDate");
    // 	}

    // 	$("#edit_port_atd").datepicker('setStartDate', minDate)
    // 	if(minDate > atd_date && atd_date != null){
    // 		$("#edit_port_atd").datepicker("setDate", minDate);
    // 	}
    // });

});

function showSwal(type, title, msg) {
    swal({
        title: title,
        html: msg,
        type: type,
        confirmButtonText: 'OK'
    });
}

function insertPort(portId, portCode, portName, portETA, portATA, portETD, portATD) {
    var parsedETA = moment(portETA, 'DD/MM/YYYY');
    var parsedETD = moment(portETD, 'DD/MM/YYYY');
    var parsedATA = moment(portATA, 'DD/MM/YYYY');
    var parsedATD = moment(portATD, 'DD/MM/YYYY');

    if (portATA == '') portATA = '-';
    if (portATD == '') portATD = '-';

    var error = new Boolean(false);

    if (parsedETD.isSameOrBefore(parsedETA)) {
        showSwal('error', 'error', 'Port ETD cannot be same or earlier than ETA');
        error = new Boolean(true);
        return error;
    }

    if (parsedATA.isBefore(parsedETA) || parsedATA.isAfter(parsedETD)) {
        showSwal('error', 'error', 'Port ATA is incorrect.');
        error = new Boolean(true);
        return error;
    }

    if (parsedATD.isBefore(parsedETD)) {
        showSwal('error', 'error', 'Port ATD cannot be earlier than ETD');
        error = new Boolean(true);
        return error;
    }

    var row = '<tr data-id="' + editDataID + '" data-port="' + portId + '" data-eta="' + portETA + '" data-etd="' + portETD + '">' +
        '<td><b>' + portCode + '</b></td>' +
        '<td>' + portName + '</td>' +
        '<td>' + portETA + '</td>' +
        '<td>' + portATA + '</td>' +
        '<td>' + portETD + '</td>' +
        '<td>' + portATD + '</td>' +
        '<td><button type="button" class="btn btn-info btn-sm btn-fw btn-edit m-r-10">Edit</button>' +
        '<button type="button" class="btn btn-danger btn-sm btn-fw btn-remove" style="padding-left: 6px">Remove</button>' +
        '</td></tr>';

    var count = $('#ports_table tbody tr').length;
    var elem = $('#ports_table tbody');
    var action = 'append';

    if (count == 0) {
        elem.append(row);
    } else {
        for (var i = 0; i < count; i++) {
            console.log(i);
            console.log(count);
            elem = $('#ports_table tbody tr:nth-child(' + (i + 1) + ')');
            var portCode = elem.find('td:nth-child(1)').text();

            var eta_text = elem.find('td:nth-child(4)').text();

            // if(eta_text != "-"){
            // 	var parsedOldATD = moment(eta_text, 'DD/MM/YYYY');
            // 	if(parsedETA.isBefore(parsedOldATD)){
            // 		alert(parsedOldATD);
            // 		showSwal('error', 'error', 'Port\'s ETA cannot be earlier than ETA of Port ' + portCode);
            // 		error = Boolean(true);
            // 		break;
            // 	}
            // }

            var oldETA = elem.attr('data-eta');
            var oldETD = elem.attr('data-etd');
            var parsedOldETA = moment(oldETA, 'DD/MM/YYYY');
            var parsedOldETD = moment(oldETD, 'DD/MM/YYYY');


            // Prevent user from selecting duplicate date for port's ETA
            if (portETA == oldETA && editPortCode != portCode) {
                showSwal('error', 'error', 'Port\'s ETA is the same as port ' + portCode + '.');
                error = Boolean(true);
                break;
            }

            var oldATD = elem.find("td:nth-child(6)").text();

            if (parsedETA.isAfter(parsedOldETA)) {
                //Check if previous row
                if (oldATD != "-") {
                    var parsedOldATD = moment(oldATD, 'DD/MM/YYYY');
                    if (parsedETA.isBefore(parsedOldATD)) {
                        showSwal('error', 'error', 'Port\'s ETA cannot be earlier than ATD of port ' + portCode + '.');
                        error = Boolean(true);
                        break;
                    }
                }
            }

            // Determines if ETA date is older than selected element's ETA
            //Ensure ETA is later than ATD if set
            if (parsedETA.diff(parsedOldETA, 'days') < 0) {

                var elemPrev = $('#ports_table tbody tr:nth-child(' + (i) + ')');
                oldETD = elemPrev.attr('data-etd');
                parsedOldETD = moment(oldETD, 'DD/MM/YYYY');
                portCode = elemPrev.find('td:nth-child(1)').text();

                if (parsedETD.diff(parsedOldETD, 'days') < 0 && parsedETD.isAfter(parsedOldETA)) {
                    showSwal('error', 'Error', 'Port\'s ETD is in between ETA and ETD of port ' + portCode + '.');
                    error = new Boolean(true);
                    break;
                } else {

                    if (parsedETD.isAfter(parsedOldETA, 'day') && parsedETA.isBefore(parsedOldETA, 'day')) {
                        var curPortCode = elem.find('td:nth-child(1)').text();
                        showSwal('error', 'Error', 'Port\'s ETD cannot be later than ETA of port ' + curPortCode + '.');
                        error = new Boolean(true);
                        break;
                    }

                    action = 'prepend';
                    break;
                }

            } else {
                if (parsedETA.diff(parsedOldETD, 'days') < 0) {
                    showSwal('error', 'Error', 'Port\'s ETA is earlier than ETD of port ' + portCode + '.');
                    error = new Boolean(true);
                    break;
                } else {
                    if (parsedETD.diff(parsedOldETD, 'days') < 0) {
                        showSwal('error', 'Error', 'Port\'s ETD is in between ETA and ETD of port ' + portCode + '.');
                        error = new Boolean(true);
                        break;
                    }
                }
            }
        }

        if (error.toString() === "false") {
            if (action == 'append') {
                elem.after(row);
            } else {
                elem.before(row);
            }
        }
    }

    if (error.toString() === "false") {
        $('#port option:selected').prop('hidden', true).prop("disabled", true);
        $('#modal_edit_port #port option[value=' + portId + ']').prop('hidden', true).prop("disabled", true);

        if (jQuery.fn.select2) {
            $("#port").select2();
        }

        $("#port option:first").prop("selected", true).trigger("change");

        $('#port').val(null);
    }

    editDataID = 0;

    $('#no_port').remove();

    return error;
}
