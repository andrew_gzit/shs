$(document).ready(function() {

	$(document).on('focus', '.datetimepicker', function() {
		$(this).datetimepicker({
			format: 'DD/MM/YYYY',
			date: new Date(),
			minDate: new Date()
		});
	});

	$(document).on('click', '.btn-view-more', function() {
		var index = $(this).attr('data-id');
		$('.loader').fadeIn(200);

		$.ajax({
			headers:
		    {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    },
			type: 'POST',
			url: '/voyage',
			data: {'id' : index},
			success: function(data) {
				$('.loader').fadeOut(200);

				if(data.result) {
					if(data.voyage.vessel_scn == null){
						data.voyage.vessel_scn = "-";
					}

					var html = '<div class="row"><div class="col-lg-6"><table class="table table-bordered table-condensed"><tbody><tr class="row-details">' +
					'<th>Voyage ID</th><td>' + data.voyage.voyage_id + '</td></tr><tr>' +
					'<th>Vessel Name</th><td>' + data.voyage.vessel_name + '</td></tr><tr>' +
					'<th>Vessel SCN</th><td>' + data.voyage.vessel_scn + '</td></tr></tbody></table></div>' +
					'<div class="col-lg-6"><table class="table table-bordered table-condensed"><tbody><tr class="row-details">' +
					'<th class="no-wrap">Booking Status</th><td><b>' + data.voyage.booking_status + '</b></td></tr><tr>' +
					'<th>Closing Date</th><td>' + data.voyage.closing_date + '</td></tr></tbody></table></div>' +
					'<div class="col-lg-12"><table class="table table-bordered table-condensed"><tbody><tr>' +
					'<th width="1%">#</th><th>Port(s) Calling</th><th>ETA</th><th>ATA</th></tr>';

					for(var i = 0; i < data.voyage.ports.length; i++) {
						html +=	'<tr><td>' + (i + 1) + '</td>';

						if(data.voyage.ports[i].port_code == data.voyage.pol_code) {
							html += '<td>' + data.voyage.ports[i].port_code + '<span class="badge badge-green m-l-10">POL</span></td>';
						} else {
							html += '<td>' + data.voyage.ports[i].port_code + '</td>';
						}

						html += '<td>' + data.voyage.ports[i].port_eta + '</td>' +
							'<td>' + data.voyage.ports[i].port_ata + '</td>' +
							'</tr>';
					}

					html += '</tbody></table></div></div></div>';

					if(data.voyage.bk_status != 0){
						$('#modal_view_more').find(".btn-booking").hide();
					}

					$('#modal_view_more .modal-body').html(html);
					$('#modal_view_more').modal();
				} else {
					swal({
						title: "An error occured!",
						text: "Please try again.",
						type: "error",
						confirmButtonText: "OK"
					});
				}
			},
			error: function(data) {
				$('.loader').fadeOut(200);
				
				swal({
					title: "An error occured!",
					text: "Please try again.",
					type: "error",
					confirmButtonText: "OK"
				});
			}
		});
	});

	$(document).on('click', '.btn-booking', function() {
		// swal({
		// 	// title: "Are you sure?",
		// 	html: "<b>Booking</b> unavailable yet. Please stand by.",
		// 	type: "warning",
		// 	confirmButtonText: "OK"
		// });
		window.location.replace("/booking/create?voyage=" + $(this).attr("data-id"));
	});

	$(document).on('click', '.btn-reservation', function() {
		// swal({
		// 	// title: "Are you sure?",
		// 	html: "<b>Reservation</b> unavailable yet. Please stand by.",
		// 	type: "warning",
		// 	confirmButtonText: "OK"
		// });
		window.location.replace("/reservation?voyage=" + $(this).attr("data-id"));
	});

	$(document).on('click', '.btn-delete', function() {
		// swal({
		// 	// title: "Are you sure?",
		// 	html: "<b>Deletion</b> unavailable yet. Please stand by.",
		// 	type: "warning",
		// 	confirmButtonText: "OK"
		// });
		swal({
			title: 'Are you sure?',
			text: 'This can\'t be undone.',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Delete',
			reverseButtons: true
		}).then((result) => {
			if(result.value) {
				$('#form_delete').find('input[name="voyage_id"]').val($(this).attr('data-id'));
				$('#form_delete').submit();
			}
		});
	});
});