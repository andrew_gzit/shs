var ports = [];
var edit_port_id = 0; //For saving edit use

$("#add_port").click(function() {

    $(this).prop("disabled", true);

    var portObj = {};
    portObj.id = $('#port option:selected').val();
    portObj.code = $('#port option:selected').text();
    portObj.name = $('#port option:selected').attr('data-name');
    portObj.eta = moment($('#port_eta').val(), 'DD/MM/YYYY');
    portObj.ata = moment($('#port_ata').val(), 'DD/MM/YYYY');
    portObj.etd = moment($('#port_etd').val(), 'DD/MM/YYYY');
    portObj.atd = moment($('#port_atd').val(), 'DD/MM/YYYY');

    if ($('#port').val() == null) {
        showSwal('error', 'Error', 'A <b>port</b> needs to be selected.');
    } else if (!portObj.eta.isValid()) {
        showSwal('error', 'Error', 'An <b>ETA</b> needs to be entered.');
    } else if (!portObj.etd.isValid()) {
        showSwal('error', 'Error', 'An <b>ETD</b> needs to be entered.');
    } else {

        if (portObj.etd.isBefore(portObj.eta)) {
            showSwal('error', 'Error', 'ETD cannot be earlier than ETA.');
            $(this).prop("disabled", false);
            return false;
        }

        if (portObj.ata.isValid()) {
            if (portObj.etd.isBefore(portObj.ata)) {
                showSwal('error', 'Error', 'ETD cannot be earlier than ATA.');
                $(this).prop("disabled", false);
                return false;
            }

            if (portObj.ata.isBefore(portObj.eta)) {
                showSwal('error', 'Error', 'ATA cannot be earlier than ETA.');
                $(this).prop("disabled", false);
                return false;
            }
        }

        if (portObj.atd.isValid()) {
            if (portObj.atd.isBefore(portObj.etd)) {
                showSwal('error', 'Error', 'ATD cannot be earlier than ETD.');
                $(this).prop("disabled", false);
                return false;
            }
        }

        addPort('create', portObj);
    }

    $(this).prop("disabled", false);
});

$(document).on('click', '.btn-edit', function() {

    var row = $(this).closest("tr");
    edit_port_id = row.attr("data-id");

    //Check if port is assigned, disable if it is
    var voy_dest_id = row.attr('data-dest-id');
    $.get("/checkPortAssigned/" + voy_dest_id, function(data) {
        if (data.success) {
            $("#modal_edit_port #port").attr('disabled', '').select2({
                dropdownParent: $("#modal_edit_port")
            });
        } else {
            $("#modal_edit_port #port").removeAttr('disabled').select2({
                dropdownParent: $("#modal_edit_port")
            });
        }
    });

    $('#modal_edit_port #port option:not(option:first-child)').prop('disabled', false);

    $('#modal_edit_port #port option[value=' + edit_port_id + ']').prop('selected', true);

    //Disable other port from dropdown
    ports.forEach(function(port) {
        if (port.id != edit_port_id) {
            $("#modal_edit_port #port option[value='" + port.id + "']").prop('disabled', true);
        } else {

            var tempETA = port.eta.format('DD/MM/YYYY');
            var tempETD = port.etd.format('DD/MM/YYYY');

            console.log(tempETA);

            $('#modal_edit_port #edit_port_eta').datepicker('setStartDate', '').datepicker('setDate', tempETA).val(tempETA);
            $('#modal_edit_port #edit_port_etd').datepicker('setStartDate', '').datepicker('setDate', tempETD).val(tempETD);

            // $('#modal_edit_port #edit_port_eta').datepicker('setDate', tempETA);

            if (port.ata.isValid()) {
                var tempATA = port.ata.format('DD/MM/YYYY');
                $('#modal_edit_port #edit_port_ata').datepicker('setStartDate', '').datepicker('setDate', tempATA).val(tempATA)
                    // $('#modal_edit_port #edit_port_ata').datepicker('setDate', tempATA);
            } else {
                $('#modal_edit_port #edit_port_ata').val('').datepicker('clearDates');
            }

            $('#modal_edit_port #edit_port_etd').val(tempETD);
            // $('#modal_edit_port #edit_port_etd').datepicker('setDate', tempETD);

            if (port.atd.isValid()) {
                var tempATD = port.atd.format('DD/MM/YYYY');
                $('#modal_edit_port #edit_port_atd').datepicker('setStartDate', '').datepicker('setDate', tempATD).val(tempATD);
                // $('#modal_edit_port #edit_port_atd').datepicker('setDate', tempATD);
            } else {
                $('#modal_edit_port #edit_port_atd').val('').datepicker('clearDates');
            }
        }
    });

    $('#modal_edit_port').modal({
        backdrop: 'static',
        keyboard: false
    });
});

$("#save_port").click(function() {

    $(this).prop('disabled', true);

    var edit_obj = {};
    edit_obj.id = $('#modal_edit_port #port').val();
    edit_obj.code = $('#modal_edit_port #port option:selected').text();
    edit_obj.name = $('#modal_edit_port #port option:selected').attr('data-name');
    edit_obj.eta = moment($('#edit_port_eta').val(), 'DD/MM/YYYY');
    edit_obj.ata = moment($('#edit_port_ata').val(), 'DD/MM/YYYY');
    edit_obj.etd = moment($('#edit_port_etd').val(), 'DD/MM/YYYY');
    edit_obj.atd = moment($('#edit_port_atd').val(), 'DD/MM/YYYY');

    if (!edit_obj.eta.isValid()) {
        showSwal('error', 'Error', 'An <b>ETA</b> needs to be entered.');
        $(this).prop('disabled', false);
        return false;
    } else if (!edit_obj.etd.isValid()) {
        showSwal('error', 'Error', 'An <b>ETD</b> needs to be entered.');
        $(this).prop('disabled', false);
        return false;
    }

    if (edit_obj.etd.isBefore(edit_obj.eta)) {
        showSwal('error', 'Error', 'ETD cannot be earlier than ETA.');
        $(this).prop('disabled', false);
        return false;
    }

    if (edit_obj.ata.isValid()) {
        if (edit_obj.ata.isBefore(edit_obj.eta)) {
            showSwal('error', 'Error', 'ATA cannot be earlier than ETA.');
            $(this).prop('disabled', false);
            return false;
        }

        if (edit_obj.etd.isBefore(edit_obj.ata)) {
            showSwal('error', 'Error', 'ETD cannot be earlier than ATA.');
            $(this).prop('disabled', false);
            return false;
        }
    }

    if (edit_obj.atd.isValid()) {
        if (edit_obj.atd.isBefore(edit_obj.etd)) {
            showSwal('error', 'Error', 'ATD cannot be earlier than ETD.');
            $(this).prop('disabled', false);
            return false;
        }
    }

    var tempPort = null;

    ports.forEach(function(port, index) {
        if (port.id == edit_port_id) {
            tempPort = port;
            ports.splice(index, 1); //remove port from array for insertport function validation
        }
    });

    console.log(ports);

    var row = $("#port_" + edit_port_id);

    //If success, remove row
    row.hide();

    var success = addPort('edit', edit_obj);
    if (success === true) {

        //Disable new selected option, remove disable from old option
        $("#port option[value='" + edit_port_id + "']").prop("disabled", false).prop('hidden', false);
        $("#port option[value='" + edit_obj.id + "']").prop("disabled", true).prop('hidden', true);

        $("#modal_edit_port #port").select2().val("").trigger("change");

        row.remove();

        $("#modal_edit_port").modal("toggle");
    } else {

        // alert("add back");
        console.log(tempPort);

        //Add in temp port because insertport function failed
        ports.push(tempPort);

        row.show();

        $(this).prop('disabled', false);
    }

});

function confirmRemove(tr, port_id) {
    swal({
        title: 'Are you sure?',
        text: 'This can\'t be undone.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Delete',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            ports.forEach(function(port, index) {
                if (port.id == port_id) {
                    ports.splice(index, 1);
                }
            });

            tr.remove();

            portCount--;

            $("#port option[value='" + port_id + "']").prop('hidden', false).prop("disabled", false);
            $("#port").select2().val("").trigger("change");
        }
    });
}

$(document).on("click", ".btn-remove", function() {
    var tr = $(this).closest("tr");
    var id = tr.attr("data-id");

    var voy_dest_id = tr.attr('data-dest-id');

    if (voy_dest_id != 0) {
        $.ajax({
            url: "/checkPortAssigned/" + voy_dest_id,
            success: function(data) {
                console.log(data);
                if (data.success) {
                    swal("Unable to remove port.", data.msg, "warning");
                    return false;
                } else {
                    confirmRemove(tr, id);
                    // swal({
                    // 	title: 'Are you sure?',
                    // 	text: 'This can\'t be undone.',
                    // 	type: 'warning',
                    // 	showCancelButton: true,
                    // 	confirmButtonText: 'Delete',
                    // 	reverseButtons: true
                    // }).then((result) => {
                    // 	if(result.value) {
                    // 		ports.forEach(function(port, index){
                    // 			if(port.id == id){
                    // 				ports.splice(index, 1);
                    // 			}
                    // 		});

                    // 		tr.remove();

                    // 		portCount--;

                    // 		$("#port option[value='" + id + "']").prop('hidden', false).prop("disabled", false);
                    // 		$("#port").select2().val("").trigger("change");
                    // 	}
                    // });
                }
            },
        });
    } else {
        confirmRemove(tr, id);
        // swal({
        // 	title: 'Are you sure?',
        // 	text: 'This can\'t be undone.',
        // 	type: 'warning',
        // 	showCancelButton: true,
        // 	confirmButtonText: 'Delete',
        // 	reverseButtons: true
        // }).then((result) => {
        // 	if(result.value) {
        // 		ports.forEach(function(port, index){
        // 			if(port.id == id){
        // 				ports.splice(index, 1);
        // 			}
        // 		});

        // 		tr.remove();

        // 		portCount--;

        // 		$("#port option[value='" + id + "']").prop('hidden', false).prop("disabled", false);
        // 		$("#port").select2().val("").trigger("change");
        // 	}
        // });
    }
});

function addPort(mode, portObj) {
    var valid = true;
    var action = "";
    var elem = "";

    ports.forEach(function(port) {
        //Check if current port ETA is same or earlier
        // if(portObj.eta.isSameOrBefore(port.eta) && portObj.etd.isAfter(port.eta)){
        if (portObj.eta.isSameOrBefore(port.eta) && portObj.etd.isAfter(port.eta)) {
            showSwal('error', 'Error', 'Port ETA cannot be the same or earlier than ETA of port ' + port.code);
            valid = false;
            return false;
        }

        if (portObj.eta.isBefore(port.etd) && portObj.eta.isAfter(port.eta)) {
            showSwal('error', 'Error', 'Port ETA cannot be earlier than ETD of port ' + port.code);
            valid = false;
            return false;
        }

        if (portObj.eta.isBefore(port.etd) && portObj.eta.isAfter(port.eta)) {
            showSwal('error', 'Error', 'Port ETA cannot be earlier than ETD of port ' + port.code);
            valid = false;
            return false;
        }

        if (portObj.atd.isValid()) {
            if (portObj.atd.isAfter(port.eta) && portObj.eta.isBefore(port.eta)) {
                showSwal('error', 'Error', 'Port ATD cannot be later than ETA of port ' + port.code);
                valid = false;
                return false;
            }
        }

        if (port.atd.isValid()) {
            if (portObj.eta.isBefore(port.atd) && portObj.etd.isAfter(port.atd)) {
                showSwal('error', 'Error', 'Port ETA cannot be earlier than ATD of port ' + port.code);
                valid = false;
                return false;
            }
        }
    });

    if (!valid) {
        return false;
    }

    var eta_dates = [];


    if (ports.length > 0) {
        var found = false;
        ports.forEach(function(port) {

            eta_dates.push(port.eta);

            if (portObj.eta.isBefore(port.eta) && !found) {
                action = "prepend";

                elem = $("#port_" + port.id + ":visible");
                console.log(elem);
                found = true;
            }
        });
    }

    var min_date = moment.min(eta_dates);

    ports.forEach(function(port) {
        if (port.eta.isSame(min_date) && portObj.eta.isBefore(port.eta)) {
            action = "prepend";

            elem = $("#port_" + port.id + ":visible");
        }
    });

    // alert("go through");

    tempETA = (portObj.eta.isValid()) ? portObj.eta.format('DD/MM/YYYY') : "-";
    tempATA = (portObj.ata.isValid()) ? portObj.ata.format('DD/MM/YYYY') : "-";
    tempETD = (portObj.etd.isValid()) ? portObj.etd.format('DD/MM/YYYY') : "-";
    tempATD = (portObj.atd.isValid()) ? portObj.atd.format('DD/MM/YYYY') : "-";

    tempDestID = (portObj.dest_id) ? portObj.dest_id : '0';

    var row = '<tr data-dest-id="' + tempDestID + '" data-id="' + portObj.id + '" id="port_' + portObj.id + '">' +
        '<td><b>' + portObj.code + '</b></td>' +
        '<td>' + portObj.name + '</td>' +
        '<td>' + tempETA + '</td>' +
        '<td>' + tempATA + '</td>' +
        '<td>' + tempETD + '</td>' +
        '<td>' + tempATD + '</td>' +
        '<td><button type="button" class="btn btn-info btn-sm btn-fw btn-edit m-r-10">Edit</button>' +
        '<button type="button" class="btn btn-danger btn-sm btn-fw btn-remove" style="padding-left: 6px">Remove</button>' +
        '</td></tr>';

    var tbody = $("#ports_table tbody");
    var no_port = tbody.find("#no_port");

    if (no_port.is(":visible")) {
        no_port.hide();
    }

    if (mode == "edit") {
        var selected_port = $('#modal_edit_port #port option:selected').val();
        $("#port").find("option[value='" + selected_port + "']").prop('hidden', true).prop("disabled", true);
    } else {
        $('#port option:selected').prop('hidden', true).prop("disabled", true);
    }

    if (jQuery.fn.select2) {
        $("#port").select2().val("").trigger("change");
    }

    if (action == "prepend") {
        $(row).insertBefore(elem);
    } else {
        $("#ports_table tbody tr:last-child").after(row);
    }

    ports.push(portObj);

    $(".port-row").find(".datetimepicker").datepicker("clearDates");

    return true;
}
