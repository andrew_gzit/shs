-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 09, 2019 at 08:09 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shs`
--

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

DROP TABLE IF EXISTS `agents`;
CREATE TABLE IF NOT EXISTS `agents` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `port_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agents_company_id_foreign` (`company_id`),
  KEY `agents_port_id_foreign` (`port_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bill_cargos`
--

DROP TABLE IF EXISTS `bill_cargos`;
CREATE TABLE IF NOT EXISTS `bill_cargos` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bl_id` int(11) NOT NULL,
  `bl_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cargo_qty` int(11) DEFAULT NULL,
  `cargo_packing` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cargo_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `markings` longtext COLLATE utf8mb4_unicode_ci,
  `cargo_nature` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uncode` longtext COLLATE utf8mb4_unicode_ci,
  `cargo_desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cargo_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` double NOT NULL,
  `weight_unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `volume` double NOT NULL,
  `temperature` int(11) DEFAULT NULL,
  `detailed_desc` longtext COLLATE utf8mb4_unicode_ci,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `raw_qty` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=125 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bill_cargos`
--

INSERT INTO `bill_cargos` (`id`, `bl_id`, `bl_no`, `cargo_qty`, `cargo_packing`, `cargo_name`, `markings`, `cargo_nature`, `uncode`, `cargo_desc`, `cargo_type`, `weight`, `weight_unit`, `volume`, `temperature`, `detailed_desc`, `remarks`, `raw_qty`, `created_at`, `updated_at`) VALUES
(1, 1, 'test2100029', 1, 'budle', '1 BUDLE', '123', 'General', NULL, 'stc goods', 'General', 123, 'MT', 123, NULL, '23123\r\n123\r\n123\r\n\r\n\r\n123123', NULL, NULL, '2019-04-05 09:44:20', '2019-04-05 09:44:20'),
(2, 2, 'PGBKI0001', 1, NULL, '1 x 20\'GP', 'NIL', 'General', NULL, 'FOODSTUFF', 'General', 10192.5, 'KG', 20, NULL, '1141 CARTONS IF FOODSTUFF\r\n299 CTNS OF BAWANG GORENG', NULL, '1,20\'GP', '2019-04-08 08:08:55', '2019-04-08 08:08:55'),
(4, 5, 'PGKCH0001', 1, NULL, '1 x 20\'GP', '4860<div>5180</div>', 'General', NULL, 'CONTAINER STC.', 'General', 20000, 'KG', 25, NULL, '350 P BAGS X 50KG<div>FINE CORN MEAL (4860)</div><div><br></div><div>50 P BAGS X 50KG</div><div>GOAT FEED (5180)</div><div><br></div><div>HS CODE:1103130000, 230400900</div><div><br></div><div>\"SHIPPED ON BOARD\" DANUM 160 V.60103 ON 08/06/2019&nbsp;</div><div>AT PENANG</div><div><br></div><div>DESTINATION PORT THC PREPAID AT PENANG</div>', NULL, '1,20\'GP', '2019-06-04 06:41:05', '2019-06-04 07:11:24'),
(5, 6, 'PGKCH0002', 1, NULL, '1 x 20\'GP', NULL, 'General', NULL, 'CONTAINER STC.', 'General', 16828.72, 'KG', 20, NULL, '10 PALLETS OF 38,400 PCS<div>EDGE BOARD PROTECTOR</div><div>50 X 50 X 2.5 X 2165MM</div><div><br></div><div>H.S.CODE:4822.90.9000</div>', NULL, '1,20\'GP', '2019-06-04 07:07:50', '2019-06-04 07:11:08'),
(9, 10, 'PGKCH0002A', 1, NULL, '1 x 20\'GP', NULL, 'General', NULL, 'CONTAINER STC.', 'General', 16828.72, 'KG', 20, NULL, '10 PALLETS OF 38,400 PCS<div>EDGE BOARD PROTECTOR</div><div>50 X 50 X 2.5 X 2165MM</div><div><br></div><div>H.S.CODE:4822.90.9000</div>', NULL, '1,20\'GP', '2019-06-10 06:37:36', '2019-06-10 06:37:36'),
(7, 8, 'PGSBW0002', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 17168, 'KG', 28, NULL, '<div><br></div><div>FCL / FCL</div><div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC.</div><div>2304 CTNS</div><div>FOODSTUFF</div><div><br></div>', NULL, '1,20\'GP', '2019-06-07 06:37:19', '2019-06-07 06:37:19'),
(8, 9, 'PGSBW0003', 3, '', '3 x 40\'HC', NULL, 'General', NULL, 'STC', 'General', 31722, 'KG', 228, NULL, '<div><br></div><div>FCL / FCL</div><div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC.</div><div>3237 BUNDLES (453,180 PCS) OF</div><div>PAPER PULP EGG TRAY</div><div>MODEL:UNI</div><div>PACKING:140PCS / BUNDLE</div><div>PACKAGING:PLASTIC BAG PER BUNDLE</div><div>PO NO.:PO/LY/1905-0024</div><div>CIF KUCHING</div><div><br></div>', NULL, '3,40\'HC', '2019-06-07 06:45:14', '2019-06-07 06:45:14'),
(10, 11, 'PGKCH0003', 3, '', '3 x 40\'HC', NULL, 'General', NULL, 'STC', 'General', 31722, 'KG', 228, NULL, '<div><br></div><div>FCL / FCL</div><div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC.</div><div>3237 BUNDLES (453,180 PCS) OF</div><div>PAPER PULP EGG TRAY</div><div>MODEL:UNI</div><div>PACKING:140PCS / BUNDLE</div><div>PACKAGING:PLASTIC BAG PER BUNDLE</div><div>PO NO.:PO/LY/1905-0024</div><div>CIF KUCHING</div><div><br></div>', NULL, '3,40\'HC', '2019-06-11 02:50:01', '2019-06-11 02:50:01'),
(12, 13, 'PGMYY0001', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC', 'General', 11150, 'KG', 23.966, NULL, '<div>FCL / FCL</div><div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div>CONTAINER STC.<div>649 CTNS HACKS PRODUCTS</div><div>208 CTNS BFSB PRODUCTS</div><div>------</div><div>857 CTNS</div><div>====</div><div><br></div><div>INDENT NO.:BG-PO1905/001</div>', NULL, '1,20\'GP', '2019-06-27 05:10:42', '2019-06-27 05:10:42'),
(13, 14, 'PGBTU0001', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 11150, 'KG', 23.966, NULL, '<div>FCL / FCL</div><div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div>CONTAINER STC.</div><div>649 CTNS HACKS PRODUCTS</div><div>208 CTNS BFSB PRODUCTS</div><div>------</div><div>857 CTNS</div><div>====</div><div>INDENT NO.:BG-PO1905/001</div>', NULL, '1,20\'GP', '2019-06-27 06:21:42', '2019-06-27 06:21:42'),
(14, 15, 'PGSBW0004', 4, '', '4 x 40\'HC', '<div><br></div><div><br></div>GRANTED 14 DAYS FREE TIME<div>DETENTION &amp; DEMURRAGE AT POD</div>', 'General', NULL, 'STC', 'General', 47077.6, 'KG', 201.76, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC.&nbsp;</div><div>16 DRUM</div><div>AL XLPE/CWS/LA/PE 19/33KV MV CABLE</div>', NULL, '4,40\'HC', '2019-06-27 08:19:07', '2019-07-01 02:00:02'),
(15, 16, 'PGBKI0003', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 13942.8, 'KG', 25, NULL, '5164 PACK OF<div>BIHUN UDANG 270G X 10</div><div><br></div>', NULL, '1,40\'HC', '2019-07-01 01:37:50', '2019-07-01 01:37:50'),
(16, 17, 'PGTWU0001', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC', 'General', 3916.817, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>349 PCS TYRES</div><div><br></div><div>DESTINATION THC, DOC FEE, CHC</div><div>LCHC, GOPC &amp; EDI FEE PREPAID</div><div>AT PENANG</div><div><br></div><div>GRANTED 18 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-01 03:53:28', '2019-07-01 03:58:44'),
(17, 18, 'PGSDK0001', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 4370.327, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>351 PCS TYRES</div><div>(SILVERSTONE BRAND)</div><div><br></div><div>DESTINATION THC, DOC FEE, CHC, LCHC,</div><div>GOPC &amp; EDI FEE PREPAID AT PENANG</div><div><br></div><div>GRANTED 18 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-01 04:08:41', '2019-07-01 04:08:41'),
(18, 19, 'PGSDK0002', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 5281.932, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>229 PCS TYRES</div><div>(SILVERSTONE BRAND)</div><div><br></div><div>DESTINATION THC, DOC FEE, CHC,</div><div>LCHC, GOPC &amp; EDI FEE PREPAID</div><div>AT PENANG</div><div><br></div><div>T/S AT PORT KLANG &nbsp;BY VESSEL</div><div>MV. \"HARBOUR NEPTUNE V.NE005\"</div><div><br></div><div>GRANTED 18 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD.</div><div><br></div>', NULL, '1,20\'GP', '2019-07-01 04:19:44', '2019-07-01 04:19:44'),
(19, 20, 'PGBKI0004', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 4448.723, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>478 PCS TYRES</div><div><br></div><div>DESTINATION THC, DOC FEE, CHC, LCHC</div><div>&amp; EDI FEE PREPAID AT PENANG</div><div><br></div><div>GRANTED 18 DAYS FREE TIME&nbsp;</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-01 04:26:48', '2019-07-01 04:26:48'),
(20, 21, 'PGBKI0005', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC', 'General', 5055.31, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>369 PCS TYRES</div><div><br></div><div>DESTINATION THC, DOC FEE, CHC,</div><div>LCHC &amp; EDI FEE PREPAID AT PENANG</div><div><br></div><div>GRANTED 18 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-01 04:42:32', '2019-07-01 04:42:32'),
(23, 24, 'PGBKI0007', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 4342.95, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>426 PCS TYRES</div><div><br></div><div>DESTINATION THC, DOC FEE, CHC,</div><div>LCHC, GOPC &amp; EDI FEE PREPAIDN AT PENANG</div><div><br></div><div>GRANTED 18 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-01 06:51:36', '2019-07-01 06:51:36'),
(22, 22, 'PGBKI0006', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC', 'General', 4982.257, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>394 PCS TYRES</div><div><br></div><div>DESTINATION THC, DOC FEE, CHC,</div><div>LCHC &amp; EDI FEE PREPAID AT PENANG</div><div><br></div><div>GRANTED 18 DAYS FREE TIME</div><div>DETENTION 7 DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,20\'GP', '2019-07-01 06:44:17', '2019-07-01 06:44:17'),
(24, 25, 'PGBKI0008', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 4327.447, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>498 PCS TYRES</div><div><br></div><div>DESTINATION THC, DOC FEE, CHC,</div><div>LCHC &amp; &nbsp;EDI FEE PREPAID AT PENANG</div><div><br></div><div>GRANTED 18 DAYS FREE TIME</div><div>DETENTION 7 DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-01 06:57:11', '2019-07-01 06:57:11'),
(25, 26, 'PGSBW0005', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 19520, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>320 PACKAGES OFLAMPUNG ROBUSTA</div><div>COFFEE BEANS</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 18 V.8441\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-01 07:18:05', '2019-07-01 07:18:05'),
(26, 27, 'PGBKI0009', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 19767.7, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1187 CTNS OF</div><div><br></div><div><br></div><div>491 CTNS OF ASAM JAWA</div><div>196 CTNS OF TEPUNG KASTAD</div><div>150 CTNS OF GULA CASTER</div><div>150 CTNS OF GULA MERAH</div><div>200 CTNS OF VEGETABLE CRACKETS</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,20\'GP', '2019-07-01 07:28:25', '2019-07-01 07:28:25'),
(27, 28, 'PGSDK0003', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 18311, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>2697 PACKAGES (12,693 PCS)</div><div>OF FURNITURE</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"HARBOUR NEPTUNE V.NE005\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-01 07:37:19', '2019-07-02 01:12:54'),
(28, 29, 'PGBKI0010', 4, '', '4 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 75168, 'KG', 93.48, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>11,600 CARTONS OF FOODSTUFFS</div><div><br></div><div>H.S. CODE: 2202.99</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '4,20\'GP', '2019-07-01 07:48:12', '2019-07-01 07:48:12'),
(29, 23, 'PGKCH0005', 7, '', '7 x 20\'GP', '<div><br></div><div><br></div><div>SHIPPING MARKS::</div><div>-----------------------</div><div>MSM PERLIS</div><div>SDN BHD</div><div><br></div><div><br></div><div><br></div>', 'General', NULL, 'STC.', 'General', 176.4, 'MT', 126, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>176,400 BAGS</div><div><br></div><div>176,400 BAGS QUALITY REFINED SUGAR</div><div>\"GRADE FP-1\" PACKED IN 1KG X 12</div><div><br></div><div>HS CODE:: 1701.99.1000</div><div><br></div><div>DESTINATION SHIPPING LOCAL CHARGES</div><div>PREPAID IN LOADING PORT</div><div><br></div><div>CARGO RECEIVE DATE: 30/06/2019</div>', NULL, '7,20\'GP', '2019-07-01 08:17:35', '2019-07-02 03:51:29'),
(30, 30, 'PGSBW0006', 3, '', '3 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 56376, 'KG', 70.11, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>8,700 CARTONS OF FOODSTUFFS</div><div><br></div><div>H.S. CODE: 2202.99</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 18 V.8441\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '3,20\'GP', '2019-07-01 08:33:46', '2019-07-01 08:33:46'),
(31, 31, 'PGSBW0007', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 3000, 'KG', 20, NULL, 'FCL / FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC.</div><div>29 PACKAGES OF WOODEN COFFIN &amp; HANDLE</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL MV \"DANUM 18\" V.8441</div><div><br></div><div>GRANTED 14 DAYS FREE TIME DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-01 08:36:13', '2019-07-01 08:36:13'),
(32, 32, 'PGBTU0002', 1, '', '1 x 40\'HC', '<div><br></div><div><br></div>***<div><div><div>10 SET INT STANDEE (4805.93.9090)</div><div>20 PAD LOVING CONTEST INFORMATION (4909.00.0000)</div><div>1 PKT SHELFTALKER (4909.00.0000)</div><div>20 SET WET TISSUE STANDEE (4805.93.9090)</div></div><div>AS PER INVOICE NO.CS000832</div></div>', 'General', NULL, 'STC.', 'General', 7970.03, 'KG', 45, NULL, 'FCL / FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div>CONTAINER STC. 928 PKGS<br></div><div><div>210 CTNS BOX TISSUE (4818.20.0000)</div><div>3 CTNS COTTON &nbsp;(5601.21.0000)</div><div>5 CTNS HANKY PACK( 4818.20.0000)</div><div>60 CTNS JRT (4818.10.0000)</div><div>20 CTNS M-FOLD (4818.20.0000)</div><div>30 CTNS POCKET TISSUE (4818.20.0000)</div><div>21 CTNS SANITARY NAPKIN (9619.00.1200)</div><div>16 CTNS SERVIETTE (4818.30.2000)</div><div>392 PKGS TOILET ROLL (4818.10.0000)</div><div>20 CTNS TOWEL (4818.20.0000)</div><div>100 CTNS WET TISSUE (3307.90.9000)</div><div>&nbsp;***</div><div><br></div></div>', NULL, '1,40\'HC', '2019-07-01 08:43:31', '2019-07-02 03:06:10'),
(33, 33, 'PGBKI0011', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 24779.7, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>990 BAGS OF CLEAN WHEAT - R</div><div><br></div><div>\"DESTINATION THC, DOC FEE, LCHC</div><div>&amp; KK CHC CHARGES PREPAID AT PENANG\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-02 01:33:48', '2019-07-02 01:33:48'),
(34, 34, 'PGSDK0004', 1, '', '1 x 20\'GP', '<div><br></div><div><br></div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"HARBOUR NEPTUNE V.NE005\"</div>', 'General', NULL, 'STC', 'General', 19520, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>320 BAGS OF</div><div>LAMPUNG ROBUSTA COFFEE BEANS&nbsp;</div><div>EK - 60DEF</div><div><br></div><div>THC, D/O FEE, EDI CHC, LCHC,&nbsp;</div><div>SABAH SURCHARGE PREPAID AT PENANG.</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-02 01:50:04', '2019-07-02 01:52:24'),
(35, 35, 'PGSBW0008', 2, '', '2 x 40\'HC', '<div><br></div><div><br></div><div><br></div><div>GRANTED 21 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE&nbsp;</div><div>AT POD</div>', 'General', NULL, 'STC. 1913 PKGS', 'General', 18514.9, 'KG', 90, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div><br></div><div>&nbsp; &nbsp; 25 PCS &nbsp; &nbsp; &nbsp;A-STAND</div><div>1888 PKGS &nbsp; TOILET ROLL</div><div><br></div><div>HS CODE: 4818.10.0000 &amp; 4909.00.0000</div><div><br></div><div>AS PER INVOICE NO. CS000830 AND LN007557</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 18 V.8441\"</div><div><br></div><div><br></div><div><br></div><div><br></div>', NULL, '2,40\'HC', '2019-07-02 02:05:35', '2019-07-03 05:02:00'),
(36, 36, 'PGSDK0005', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 11181, 'KG', 40, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>417 PACKAGES</div><div>OF INNER TUBES AND TYRES</div><div>1618 PCS</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"HARBOUR NEPTUNE V.NE005\"</div><div><br></div><div>\"DESTINATION THC, D/O FEE, EDI, CHC,</div><div>LCHC, SABAH SURCHARGE PREPAID&nbsp;</div><div>AT PENANG\"</div><div><br></div><div><br></div>', NULL, '1,40\'HC', '2019-07-02 02:28:49', '2019-07-02 05:37:29'),
(37, 37, 'PGBKI0012', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 16033, 'KG', 68, NULL, 'FCL / FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC.</div><div>899 CARTONS OF</div><div>(438 UNITS) FURNITURE</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,40\'HC', '2019-07-02 02:52:28', '2019-07-02 02:52:28'),
(38, 40, 'PGMYY0003', 1, '', '1 x 20\'GP', '<div><br></div><div><br></div><div><br></div>T/S AT PORT KLANG BY VESSEL<div>LCT \"DANUM 9\" V.9414</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT</div><div>POD</div><div><br></div><div>DESTINATION PORT THC, CMR,</div><div>DOC FEE AND EDI FEE PREPAID&nbsp;</div><div>AT PENANG</div>', 'General', NULL, 'STC.1450 PACKAGES', 'General', 12927, 'KG', 25, NULL, 'FCL / FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC.&nbsp;</div><div><div>SIRAP BERPERISA,GINSENG TONGKAT ALI COFFEE,</div><div>TEPUNG JAGUNG, SOYA MILK,MIXED NATA JUICE,</div><div>SEDAP MANIS MALT COKLAT,ASAM JAWA,MONOSODIUM&nbsp;</div><div>GLUTAMATE,STRAW,TEELSEED OIL</div><div><br></div><div>HS CODE: 2009 90 9900</div></div><div><br></div>', NULL, '1,20\'GP', '2019-07-02 03:51:21', '2019-07-02 05:35:53'),
(39, 41, 'PGBKI0013', 1, '', '1 x 40\'HC', '<div><br></div><div><br></div><div><br></div>GRANTED 14 DAYS FREE TIME<div>DETENTION &amp; DEMURRAGE&nbsp;</div><div>AT POD</div>', 'General', NULL, 'STC.', 'General', 8500, 'KG', 40, NULL, 'FCL / FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC.</div><div>1 LOT AGRICULTURAL TRACTOR AND HARVESTER PARTS</div>', NULL, '1,40\'HC', '2019-07-02 04:00:02', '2019-07-02 04:00:02'),
(40, 42, 'PGBKI0014', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC. 1174 PKGS', 'General', 9384, 'KG', 45, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1174 PKGS TISSUE PRODUCTS</div><div>(TOILET TISSUE)</div><div><br></div><div>HS CODE 4818.10.0000</div><div><br></div><div>AS PER INVOICE NO: LN007558</div><div><br></div><div>DESTINATION THC, DO FEE, LCHC</div><div>CHC &amp; EDI FEE PREPAID AT PENANG</div><div><br></div><div>GRANTED 21 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,40\'HC', '2019-07-02 04:06:16', '2019-07-02 04:06:16'),
(41, 43, 'PGSBW0009', 4, '', '4 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 99999, 'KG', 88, NULL, 'FCL / FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC.&nbsp;</div><div>(2,160 BAGS) OF DOLOMITE</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV \"DANUM 18\" V.8441</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '4,20\'GP', '2019-07-02 04:08:38', '2019-07-02 04:08:38'),
(42, 44, 'PGBKI0015', 1, '', '1 x 40\'HC', '<div><br></div><div><br></div><div>DESTINATION THC, DO FEE, LCHC,</div><div>CHC &amp; EDI PREPAID AT PENANG</div><div><br></div><div><br></div><div>GRANTED 21 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', 'General', NULL, 'STC. 515 PKGS', 'General', 4552.24, 'KG', 45, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>&nbsp;50 CT BOX TISSUE</div><div>&nbsp;80 CT JRT</div><div>286 CT COTTON</div><div>&nbsp;80 CT TISSUE PRODUCTS (SERVIETTE)</div><div>&nbsp;14 BD TISSUE PRODUCTS (TOILET TISSUE)</div><div>&nbsp; 5 PCS A-STAND</div><div><br></div><div>HS CODE 5601.21.0000</div><div><br></div><div>A PER INVOICE NO: CS000831</div><div><br></div><div><br></div><div><br></div><div><br></div>', NULL, '1,40\'HC', '2019-07-02 04:15:03', '2019-07-02 06:38:11'),
(43, 39, 'PGPKG0001', 5, '', '5 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 100, 'MT', 250, NULL, '<div>SOC CONTAINER OF<br></div><div>GENERAL CARGO</div><div><br></div>', NULL, '5,20\'GP', '2019-07-02 07:28:57', '2019-07-02 07:35:43'),
(45, 46, 'PGSBW0010', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 11948, 'KG', 25, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>716 PKGS (681 CTNS &amp; 35 GUNI)</div><div>GENERAL FOODSTUFF</div><div><br></div><div>HS CODE: 090962</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 18 V.8441</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-02 08:00:26', '2019-07-03 07:12:13'),
(44, 45, 'PGPKG0002', 2, '', '2 x 20\'GP', NULL, 'General', NULL, 'STC', 'General', 40, 'MT', 50, NULL, '<div><div>SOC CONTAINER OF<br></div><div>GENERAL CARGO</div></div>', NULL, '2,20\'GP', '2019-07-02 07:33:40', '2019-07-02 07:36:12'),
(46, 47, 'PGBKI0016', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 14880, 'KG', 25, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1530 PKGS (830 CTNS &amp; 700 BAGS)</div><div>GENERAL FOODSTUFF</div><div>HS CODE: 090962</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-02 08:07:10', '2019-07-02 08:07:10'),
(47, 48, 'PGBKI0017', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 28043, 'KG', 54.22, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>265 PKGS OF HOUSEHOLD FURNITURE.</div><div>(DESCRIPTION AS PE ATTACHED LIST)</div><div><br></div><div>1) HOUSEHOLD FURNITURE</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,40\'HC', '2019-07-03 01:37:34', '2019-07-03 02:37:44'),
(48, 38, 'PGMYY0002', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 27093.5, 'KG', 63.32, NULL, 'FCL/FCL<div>SHIPPER\'S &nbsp;LOAD, COUNT &amp; SEALED</div><div><br></div><div>2916 PKGS OF HOUSEHOLD FURNITURE</div><div><br></div><div>1) HOUSEHOLD FURNITURE</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 9 V.9414\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,40\'HC', '2019-07-03 01:46:53', '2019-07-04 07:28:06'),
(49, 49, 'PGKCH0006', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.. 2158 CARTONS', 'General', 15265.69, 'KG', 31.95, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>2158 CARTONS OF UPIN IPIN FRUIT DRINK</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,20\'GP', '2019-07-03 06:38:38', '2019-07-03 06:38:38'),
(50, 50, 'PGKCH0007', 18, '', '18 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 455.04, 'MT', 450, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>9000 BAGS X 50KG PP WOVEN BAGS OF</div><div>ALUMINIUM SULPHATE (GRANULAR)</div><div><br></div><div>HS CODE 2833.22</div><div><br></div><div>GRANTED 18 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '18,20\'GP', '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(51, 51, 'PGSBW0011', 1, '', '1 x 40\'HC', '<div><br></div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 3 V.3430\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE&nbsp;</div><div>AT POD.</div>', 'General', NULL, 'STC.', 'General', 9920, 'KG', 76, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD COUNT &amp; SEALED</div><div><br></div><div>891 BUNDLES (124,740 PCS) OF</div><div>PAPER PULP EGG TRAY</div><div>MODEL: UNI</div><div>PACKING: 140PCS/BUNDLE</div><div><br></div><div>144 BUNDLES (15,840 PCS) OF</div><div>PAPERPULP EGG TRAY</div><div>MODEL: AA</div><div>PACKING: 110PCS/BUNDLE</div><div><br></div><div>PACKAGING: PLASTIC BAG PER BUNDLE</div><div>CIF SIBU PORT</div><div>HS CODE: 4823 70 0000</div>', NULL, '1,40\'HC', '2019-07-03 07:14:29', '2019-07-04 06:54:26'),
(52, 52, 'PGKCH0008', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 7239, 'KG', 40, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>(1053 CTNS) PENSONIC ELECTRONIC</div><div>GOODS</div><div>HS CODE: 8521909900</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,40\'HC', '2019-07-03 07:22:11', '2019-07-03 07:22:11'),
(53, 53, 'PGMYY0004', 1, '', '1 x 20\'GP', '<div><br></div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE&nbsp;</div><div>AT POD</div>', 'General', NULL, 'STC.', 'General', 18.835, 'MT', 20.518, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>2187 CTNS SUNQUICK PRODUCTS</div><div><br></div><div>HS CODE: 210690</div><div><br></div><div>INDENT NO.: MG-PO1906/002</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 5 V.5414\"</div><div><br></div>', NULL, '1,20\'GP', '2019-07-03 08:24:08', '2019-07-03 08:24:08'),
(54, 54, 'PGSBW0012', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC. 550 BAGS', 'General', 22077, 'KG', 21.25, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>SUPER PALM 550 BAGS X 40KG</div><div><br></div><div>HS CODE: 3105 20 0000</div><div><br></div><div>INVOICE NO. TD-000212041</div><div><br></div><div>T/S AS PORT KLANG BY VESSEL</div><div>MV. \"DANUM 18 V.8441\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,20\'GP', '2019-07-04 02:08:40', '2019-07-04 02:08:40'),
(55, 55, 'PGKCH0009', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 7997, 'KG', 20.939, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>228 CARTONS OF FURNITURE</div><div><br></div><div>HS CODE: 9403.50 000</div><div><br></div><div>GRANTED 14 DAYS FREE TIME&nbsp;</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,20\'GP', '2019-07-04 03:45:20', '2019-07-04 03:45:20'),
(56, 56, 'PGSDK0006', 1, '', '1 x 20\'GP', '<div><br></div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE&nbsp;</div><div>AT POD.</div>', 'General', NULL, 'STC. CARTONS OF', 'General', 8798.6, 'KG', 25, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div><div>SOCOLIC\'S CHOC MALT</div><div>SOCOLIC\'S CLASSIC</div><div>SOCOLIC\'S HOT CHOC</div><div><span style=\"line-height: 1.42857;\">SOCOLIC\'S MALTED MILK</span><br></div><div>SOCOLIC\'S CHUNK INSTANT CHOC RICE</div><div>SOCOLIC\'S PUNCH CHOC CUBE</div></div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \" HARBOUR NEOTUNE V.NE005\"</div><div><br></div><div><br></div>', NULL, '1,20\'GP', '2019-07-04 04:09:24', '2019-07-04 04:09:24'),
(57, 57, 'PGTWU0002', 1, '', '1 x 20\'GP', '<div><br></div><div><br></div><div><br></div><div>SHIPPING MARKS:</div><div>-----------------------</div><div>MSM PERLIS</div><div>SDN BHD</div>', 'General', NULL, 'STC. 21,065 BAGS', 'General', 24000, 'KG', 18, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div><div>21,060 BAGS REFINED SUGAR c/w 5 EMPTY BAGS</div><div><br></div><div>1KG REFINED SUGAR x 21,000 BAGS</div><div>NORMAL GRAIN &nbsp;<span style=\"line-height: 1.42857;\">( 21.00 METRIC TON )</span></div><div><span style=\"line-height: 1.42857;\"><br></span></div><div>50KG REFINED SUGAR x 60 BAGS</div><div>NORMAL GRAIN &nbsp;<span style=\"line-height: 1.42857;\">( 3.00 METRIC TON )</span></div><div><span style=\"line-height: 1.42857;\"><br></span></div><div>HS CODE : 1701.99 1000</div></div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"HARBOUR NEPTUNE V.NE005\"</div><div><br></div>', NULL, '1,20\'GP', '2019-07-04 04:23:40', '2019-07-04 04:23:40'),
(58, 58, 'PGSBW0013', 3, '', '3 x 20\'GP', '<div><br></div><div><br></div><div><br></div><div>SHIPPING MARKS:</div><div>-----------------------</div><div>MSM PERLIS</div><div>SDN BHD</div>', 'General', NULL, 'STC. 50,430 BAGS', 'General', 74920, 'KG', 54, NULL, 'FCL/FCL<div>SHIPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div><div>50,420 BAGS REFINED SUGAR c/w 10 EMPTY BAGS</div><div><br></div><div>1KG REFINED SUGAR x 49,920 BAGS</div><div>NORMAL GRAIN&nbsp;<span style=\"line-height: 1.42857;\">( 49.92 METRIC TON )</span></div><div><span style=\"line-height: 1.42857;\"><br></span></div><div>50KG REFINED SUGAR x 500 BAGS</div><div>NORMAL GRAIN&nbsp;<span style=\"line-height: 1.42857;\">( 25.00 METRIC TON )</span></div><div><span style=\"line-height: 1.42857;\"><br></span></div><div>HS CODE : 1701.99 1000</div></div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 18 V.8441\"</div><div><br></div>', NULL, '3,20\'GP', '2019-07-04 04:41:12', '2019-07-04 04:41:12'),
(59, 59, 'PGSDK0007', 6, '', '6 x 20\'GP', '<div><br></div><div><br></div><div><br></div><div>SHIPPING MARKS:</div><div>-----------------------</div><div>MSM PERLIS</div><div>SDN BHD</div><div><br></div><div>T/S AT PORT KLANG BY&nbsp;<span style=\"line-height: 1.42857;\">VESSEL &nbsp;</span></div><div><span style=\"line-height: 1.42857;\">MV. \"HARBOUR NEPTUNE V.NE005\"</span></div>', 'General', NULL, 'STC. 96,979 BAGS', 'General', 144, 'MT', 108, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div><div>96,964 BAGS REFINED SUGAR c/w 15 EMPTY BAGS</div><div><br></div><div>1KG REFINED SUGAR x 96,000 BAGS</div><div>NORMAL GRAIN&nbsp;<span style=\"line-height: 1.42857;\">( 96.00 METRIC TON )</span></div><div><span style=\"line-height: 1.42857;\"><br></span></div><div>50KG REFINED SUGAR x 944 BAGS</div><div>NORMAL GRAIN&nbsp;<span style=\"line-height: 1.42857;\">( 47.20 METRIC TON )</span></div><div><span style=\"line-height: 1.42857;\"><br></span></div><div>40KG ICING SUGAR x 20 BAGS</div><div>( 0.80 METRIC TON )</div><div><br></div><div>HS CODE : 1701.99 1000</div><div><br></div><div><span style=\"line-height: 1.42857;\">DESTINATION SHIPPING LOCAL CHARGES PREPAID IN</span><br></div><div>LOADING PORT</div></div>', NULL, '6,20\'GP', '2019-07-04 06:36:34', '2019-07-04 06:36:34'),
(60, 60, 'PGKCH0010', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 3549.84, 'KG', 18, NULL, 'PART OF FCL/FCL<div>SHIPPER\'S LOAD, &nbsp;COUNT &amp; SEALED</div><div><br></div><div>375 PKGS PLASTIC HOUSEHOLD WARES</div><div><br></div><div>HS CODE: 3924.90</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div>', NULL, '1,40\'HC', '2019-07-04 06:51:25', '2019-07-05 04:47:58'),
(61, 61, 'PGTWU0003', 1, '', '1 x 40\'HC', '<div><br></div><div><br></div><div>NIL MARK</div>', 'General', NULL, 'STC.', 'General', 8395, 'KG', 68, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>261 CARTONS OF</div><div>(153 UNITS) FURNITURE</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"HARBOUR NEPTUNE V.NE005\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,40\'HC', '2019-07-04 07:10:23', '2019-07-04 07:10:23'),
(62, 62, 'PGKCH0011', 1, '', '1 x 40\'HC', '<div><br></div><div><br></div><div><br></div><div>NIL MARK</div>', 'General', NULL, 'STC.', 'General', 8177, 'KG', 65, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>350 CARTONS OF</div><div>(65 UNITS) FURNITURE</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div>', NULL, '1,40\'HC', '2019-07-04 07:29:37', '2019-07-04 07:29:37'),
(63, 63, 'PGKCH0012', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 17000, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, &nbsp;COUNT &amp; SEALED</div><div><br></div><div>680 BAGS RESIN</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div>', NULL, '1,20\'GP', '2019-07-04 08:00:10', '2019-07-04 08:00:10'),
(64, 64, 'PGKCH0013', 1, '', '1 x 40\'HC', '<div><br></div><div><br></div><div>GRANTED 21 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE</div><div>AT POD</div>', 'General', NULL, 'STC. 836 P\'KGS', 'General', 5325.8, 'KG', 45, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>130 CT BOX TISSUE</div><div>300 BD KITCHEN TOWEL</div><div>230 CT SERVIETTE NORMAL</div><div>176 BD TOILET ROLL</div><div><br></div><div>HS CODE 4818.20.0000</div><div><br></div><div>AS PER INVOICE NO: LN007564</div><div><br></div><div>DESTINATION THC, DOC FEE, CMR &amp;</div><div>EDI FEE PREPAID AT PENANG</div><div><br></div><div><br></div><div><br></div>', NULL, '1,40\'HC', '2019-07-04 08:35:07', '2019-07-04 08:35:07'),
(65, 65, 'PGSBW0014', 1, '', '1 x 20\'GP', '<div><br></div><div><br></div><div>GRANTED 14 DATS FREE TIME</div><div>DETENTION &amp; DEMURRAGE</div><div>AT POD</div>', 'General', NULL, 'STC.', 'General', 24200, 'KG', 24.73, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>(960 BAGS)</div><div>CALCIUM CARBONATE MASTERBATCH</div><div><br></div><div>GRADE:MB8-1A</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 18 V.8441\"</div><div><br></div>', NULL, '1,20\'GP', '2019-07-05 00:57:56', '2019-07-05 03:37:12'),
(66, 66, 'PGKCH0014', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 3913.65, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>329 PCS TOYO TYRES</div><div><br></div><div>DESTINATION THC, DOC FEE, CMR &amp;</div><div>EDI FEE PREPAID AT PENANG</div><div><br></div><div>GRANTED 18 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-05 01:23:07', '2019-07-05 01:23:07'),
(67, 67, 'PGKCH0015', 1, '', '1 x 40\'HC', '<div><br></div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE&nbsp;</div><div>AT POD</div>', 'General', NULL, 'STC.', 'General', 9000, 'KG', 69, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>500 CARTONS</div><div><br></div><div>OF HANDICRAFT MATERIALS</div><div>(HOUSEHOLD PRODUCTS)</div><div><br></div><div>HS CODE: 3924.90 9090</div><div><br></div><div>\"DESTINATION THC, DOC FEE, CMR CHARGES</div><div>&nbsp;&amp; EDI (POD) PREPAID AT PENANG\"</div>', NULL, '1,40\'HC', '2019-07-05 01:39:35', '2019-07-05 01:39:35'),
(68, 68, 'PGKCH0016', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 7201.94, 'KG', 54.54, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>628 PACKAGES &nbsp;OF</div><div>FURNITURE AND HOUSEHOLD PRODUCT</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,40\'HC', '2019-07-05 02:01:30', '2019-07-05 04:43:38'),
(69, 69, 'PGTWU0004', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 24450, 'KG', 22.15, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>2420 PCS OF PLASTER PRODUCTS</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. HARBOUR NEPTUNE V.NE005\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,20\'GP', '2019-07-05 02:17:35', '2019-07-05 02:17:35'),
(70, 70, 'PGKCH0017', 10, '', '10 x 20\'GP', NULL, 'General', NULL, 'STC. 200 BAGS', 'General', 200.6, 'MT', 250, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>200.000 M/TONS PXWF-1T GRADE REFINED</div><div>SUGAR (1000 KG)</div><div><br></div><div><br></div>', NULL, '10,20\'GP', '2019-07-05 02:54:37', '2019-07-05 02:54:37'),
(71, 71, 'PGKCH0018', 1, '', '1 x 40\'HRF', NULL, 'General', NULL, 'STC.', 'General', 17527, 'KG', 58.34, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1901 CTNS DRIED ANCHOVY &amp; DRIED SHRIMP</div><div>(TEMP:-15 DEG.C)</div><div><br></div><div>HS CODE 305630000</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div>', NULL, '1,40\'HRF', '2019-07-05 03:17:21', '2019-07-09 07:03:35'),
(72, 72, 'PGKCH0019', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 27600, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>179 PACKAGES</div><div>176 PKGS-USED &nbsp;HOUSEHOLD &amp; PERSONAL ITEMS</div><div>2 UNIT - USED MOTORCYCLE</div><div>1 UNIT - USED MOTORCAR</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div>', NULL, '1,40\'HC', '2019-07-05 03:34:28', '2019-07-05 05:15:16'),
(73, 73, 'PGKCH0020', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC. 1350 PACKAGES', 'General', 12440, 'KG', 25, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>SIRAP BERPERISA, STRAW, ASAM JAWA,</div><div>MIXED NATA JUICE SEDAP MANIS MALT COKLAT,</div><div>MALT COKLAT, SOYA MILK CEREAL OAT,&nbsp;</div><div>GINSENG TONGKAT ALI, TEELSEED OIL</div><div><br></div><div>HS CODE: 2009 90 9900</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-05 03:48:02', '2019-07-05 03:48:02'),
(74, 78, 'PGKCH0010A', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC. 1,985 DOZ', 'General', 10000, 'KG', 32, NULL, 'PART OF FCL/FCL<div>SHIPPER\'S LOAD, &nbsp;COUNT &amp; SEALED</div><div><br></div><div>PLASTIC HOUSEHOLD PRODUCT</div><div>(BUATAN MALAYSIA)</div><div><br></div><div>HS CODE: 3924.90</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div>', NULL, '1,40\'HC', '2019-07-05 04:04:15', '2019-07-05 07:50:04'),
(75, 91, 'PGKCH0021', 1, '', '1 x 20\'GP', '<div><br></div><div><br></div>DESTINATION PORT THC, CMR,<div>DOC FEE AND EDI FEE</div><div>PREPAID AT PENANG</div>', 'General', NULL, 'STC. 360 PKGS', 'General', 9581, 'KG', 24.706, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>343 PKGS FOODSTUFF</div><div>&nbsp; &nbsp; 4 PKGS HARDWARE (SECURITY SEAL, STAINLESS STEEL)</div><div>&nbsp; 10 PKGS MEDICAL PRODUCTS</div><div>&nbsp; &nbsp; 3 PKGS STATIONERY</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div>', NULL, '1,20\'GP', '2019-07-05 04:54:04', '2019-07-06 01:55:18'),
(76, 77, 'PGPKG0003', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 28, 'MT', 25, NULL, 'SOC CONTAINER OF GENERAL CARGO', NULL, '1,20\'GP', '2019-07-05 05:31:50', '2019-07-05 05:31:50'),
(77, 102, 'PGPKG0004', 3, '', '1 x 20\'GP & 2 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 74, 'MT', 125, NULL, 'SOC CONTAINER OF GENERAL CARGO', NULL, '1,20\'GP;2,40\'HC', '2019-07-05 05:37:00', '2019-07-05 05:37:00'),
(78, 103, 'PGPKG0005', 11, '', '11 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 350, 'MT', 600, NULL, 'SOC CONTAINER OF GENERAL CARGO', NULL, '11,40\'HC', '2019-07-05 05:41:56', '2019-07-05 06:33:08'),
(79, 104, 'PGPKG0006', 4, '', '4 x 20\'GP', NULL, 'General', NULL, 'STC', 'General', 72, 'MT', 100, NULL, 'SOC CONTAINER OF GENERAL CARGO', NULL, '4,20\'GP', '2019-07-05 05:46:26', '2019-07-05 05:46:26'),
(80, 105, 'PGPKG0007', 11, '', '1 x 20\'GP & 3 x 40\'GP & 7 x 40\'HC', NULL, 'General', NULL, 'STC', 'General', 250, 'MT', 300, NULL, 'SOC CONTAINER OF GENERAL CARGO', NULL, '1,20\'GP;3,40\'GP;7,40\'HC', '2019-07-05 05:50:25', '2019-07-05 05:50:25'),
(81, 106, 'PGPKG0008', 2, '', '2 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 40, 'MT', 50, NULL, 'SOC CONTAINER OF GENERAL CARGO', NULL, '2,20\'GP', '2019-07-05 05:54:15', '2019-07-05 05:54:15'),
(82, 107, 'PGPKG0009', 15, '', '6 x 20\'GP & 9 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 350, 'MT', 500, NULL, 'SOC CONTAINER OF GENERAL CARGO', NULL, '6,20\'GP;9,40\'HC', '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(83, 108, 'PGPKG0010', 2, '', '2 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 48, 'MT', 50, NULL, 'SOC CONTAINER OF GENERAL CARGO', NULL, '2,20\'GP', '2019-07-05 06:18:43', '2019-07-05 06:18:43'),
(84, 109, 'PGPKG0011', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 10, 'MT', 25, NULL, 'SOC CONTAINER OF GENERAL CARGO', NULL, '1,20\'GP', '2019-07-05 06:20:19', '2019-07-05 06:20:19'),
(85, 110, 'PGSBW0016', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 5000, 'KG', 25, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>297 PACKAGES OF PLASTIC WARE</div><div><br></div><div>HS CODE: 3924.90 9090</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 3 V.3430\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-05 06:27:38', '2019-07-05 06:27:38'),
(86, 111, 'PGSBW0017', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 20654.4, 'KG', 26.697, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1,355 PACKAGES OF</div><div>SOY SAUCE LIGHT AND DARK SOYA SAUCE</div><div><br></div><div>HS CODE: 2103.10 0000</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 3 V.3430\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,20\'GP', '2019-07-05 06:46:59', '2019-07-05 06:46:59'),
(87, 95, 'PGKCH0025', 3, '', '3 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 6124, 'KG', 150, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>6 LOT OF</div><div>RAW WATER TANK</div><div>UF PRODUCT TANK</div><div>RO PRODUCT TANK</div><div>FRP SUMP PIT TANK</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div>', NULL, '3,40\'HC', '2019-07-05 07:08:16', '2019-07-05 07:08:16'),
(88, 96, 'PGKCH0026', 2, '', '2 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 2420, 'KG', 100, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div><div>6 UNITS OF</div><div>PRE-TREATMENT TANK C/W TRANSFER</div><div>COMPARTMENT</div><div>SLUDGE THICKENER TANK</div><div>PLATFORM FOR PRE-TREATMENT TANK</div></div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div>', NULL, '2,40\'HC', '2019-07-05 07:19:51', '2019-07-05 07:54:04'),
(89, 97, 'PGKCH0027', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC. 21 COILS, 10 PCS & 100 ROLLS', 'General', 17483, 'KG', 25, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>WIRE ROD 5.50MM</div><div>CR SLITTED COIL - 0.80MM X 31.8MM X C</div><div>NOVA TENSION PAD 58/59,&nbsp;</div><div>SIZE: 1.5M X 2M X 8M&nbsp;</div><div>FILAMENT TAPE</div><div><br></div><div>HS CODE: 721391, 721119 &amp; 391990</div><div><br></div><div>GRANTED 14 DAYS FREE TIME&nbsp;</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-05 07:32:30', '2019-07-09 02:02:15'),
(90, 112, 'PGSBW0018', 1, '', '1 x 40\'HC', '<div><br></div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE&nbsp;</div><div>AT POD</div>', 'General', NULL, 'STC', 'General', 10574, 'KG', 76, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1079 BUNDLES (151,060 PCS) OF</div><div>PAPER PULP EGG TRAY</div><div>MODEL : UNI</div><div>PACKING : 140PCS / BUNDLE</div><div><br></div><div>PACKAGING : PLASTIC BAG PER BUNDLE</div><div>CIF SIBU PORT</div><div>HS CODE: 4823 70 0000</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 3 V.3430\"</div>', NULL, '1,40\'HC', '2019-07-05 08:09:19', '2019-07-05 08:09:19'),
(91, 98, 'PGKCH0028', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 15000, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>75 UNITS CASKET</div><div><br></div><div>GRANTED 14 DAYS FREED TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,40\'HC', '2019-07-05 08:19:34', '2019-07-05 08:19:34'),
(92, 100, 'PGKCH0030', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 11500, 'KG', 12, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>282 CTNS JOSS PAPER</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,20\'GP', '2019-07-05 08:29:35', '2019-07-05 08:29:35'),
(93, 99, 'PGKCH0029', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 6700, 'KG', 10, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>20 UNIT CASKET</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div>', NULL, '1,20\'GP', '2019-07-05 08:37:40', '2019-07-05 08:37:40'),
(94, 93, 'PGKCH0023', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 14081.6, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>20 PALLETS OF 26,400 PCS</div><div>EDGE BOARD PROTECTOR</div><div>75 X 75 X 5 X 1050MM</div><div><br></div><div>H,S, CODE: 4822.90.90.00</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div>', NULL, '1,20\'GP', '2019-07-05 08:43:07', '2019-07-06 03:01:43'),
(95, 87, 'PGTWU0005', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 4735.377, 'KG', 20, NULL, 'FCL / FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC.</div><div>151 PCS TYRES</div><div><br></div><div>GRANTED 18 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div>DESTINATION PORT THC, CHC,</div><div>LCHC, DOC FEE AND EDI FEE</div><div>PREPAID AT PENANG</div>', NULL, '1,20\'GP', '2019-07-05 08:49:51', '2019-07-05 08:49:51'),
(96, 75, 'PGKCH0022', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 16632, 'KG', 63.24, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>368 PKGS OF HOUSEHOLD FURNITURE</div><div><br></div><div>1) HOUSEHOLD FURNITURE</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div>', NULL, '1,40\'HC', '2019-07-06 01:25:14', '2019-07-06 01:25:14'),
(97, 89, 'PGMYY0005', 2, '', '2 x 40\'HC', '<div><br></div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE&nbsp;</div><div>AT POD</div>', 'General', NULL, 'STC. 2,270 CARTONS', 'General', 20000, 'KG', 80, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>PLASTIC WARE</div><div>(ALL MADE IN MALAYSIA)</div><div><br></div><div>H.S CODE: 3924.90.9090</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV.. \" DANUM 5 V.60105\"</div><div><br></div><div><br></div>', NULL, '2,40\'HC', '2019-07-06 01:35:36', '2019-07-06 01:36:49'),
(98, 76, 'PGSBW0015', 1, '', '1 x 20\'GP', '<div><br></div><div><br></div>T/S AT PORT KLANG BY VESSEL<div>MV \"DANUM 18\" V.8441</div>', 'General', NULL, 'STC.', 'General', 2145, 'KG', 29.749, NULL, 'FCL / FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC. 170 PKGS</div><div>28 PCS OF MATTRESS</div><div>90 PCS OF PILLOW</div><div>4 SETS OF DIVAN &amp; HEADBOARD</div><div>48 UNITS OF COAT HANGER</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,20\'GP', '2019-07-06 01:40:10', '2019-07-06 01:44:43'),
(99, 101, 'PGKCH0031', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 3090, 'KG', 66.507, NULL, 'FCL / FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC. 72 PKGS</div><div>15 PCS OF MATTRESS C/W SET</div><div>33 PCS OF MATTRESS</div><div>24 SETS OF SOFA</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div>', NULL, '1,40\'HC', '2019-07-06 01:43:12', '2019-07-06 01:43:12'),
(100, 94, 'PGKCH0024', 2, '', '2 x 40\'HC', NULL, 'General', NULL, 'STC.1,562 CTNS', 'General', 28903, 'KG', 134, NULL, 'FCL/FCL<div>SHIPPER\'S LOA, COUNT &amp; SEALED</div><div><br></div><div>1562 CARTONS OF</div><div>(664 UNITS) FURNITURE</div><div><br></div><div>GRANTED 14 DAYS FREE TIME&nbsp;</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div><div><br></div>', NULL, '2,40\'HC', '2019-07-06 01:46:29', '2019-07-06 01:46:29'),
(101, 113, 'PGBKI0018', 1, '', '1 x 20\'GP', '<div><br></div><div><br></div><div>MARKS &amp; NUMBER</div><div>------------------------</div><div>HW</div><div>KK</div><div>6120</div>', 'General', NULL, 'STC.', 'General', 20000, 'KG', 25, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>400 P BAGS X 50KG</div><div>PREMIUM WHEAT</div><div><br></div><div>HS CODE: 1001910000</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,20\'GP', '2019-07-06 02:12:01', '2019-07-06 02:12:01'),
(102, 114, 'PGSBW0019', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 11826.97, 'KG', 66.5, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1050 CTNS FOODSTUFF</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 3 V.3430\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,40\'HC', '2019-07-06 02:22:58', '2019-07-06 02:22:58');
INSERT INTO `bill_cargos` (`id`, `bl_id`, `bl_no`, `cargo_qty`, `cargo_packing`, `cargo_name`, `markings`, `cargo_nature`, `uncode`, `cargo_desc`, `cargo_type`, `weight`, `weight_unit`, `volume`, `temperature`, `detailed_desc`, `remarks`, `raw_qty`, `created_at`, `updated_at`) VALUES
(103, 115, 'PGSBW0020', 2, '', '2 x 40\'GP', '<div><br></div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 3 V.3430\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE</div><div>&nbsp;AT POD</div><div><br></div>', 'General', NULL, 'STC.', 'General', 34500, 'KG', 49.296, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>23 PACKAGES (18400 PCS) OF VENEER</div><div>0.9 X 1220 X 2440 (MM)</div><div><br></div><div>INV NO.: CI 11266</div><div><br></div><div>SHIPPED ON BOARD \"DANUM 171 V.7002\"</div><div>AT PENANG, MALAYSIA ON 10/07/2019</div>', NULL, '2,40\'GP', '2019-07-06 02:37:53', '2019-07-09 00:23:58'),
(104, 116, 'PGBKI0019', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 26000, 'KG', 40, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1300 BAGS OF FISH FEED</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div>', NULL, '1,40\'HC', '2019-07-06 03:06:07', '2019-07-06 03:06:07'),
(105, 117, 'PGSBW0021', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 4623, 'KG', 20, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1) MATTRESS</div><div>2) PLUS TOP SHEET (ALOE VERA)</div><div>3) PILLOW</div><div>4) BOLSTER</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 3 V.3430\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div>', NULL, '1,20\'GP', '2019-07-06 03:18:53', '2019-07-06 03:18:53'),
(106, 118, 'PGPKG0012', 5, '', '4 x 20\'GP & 1 x 40\'HC', NULL, 'General', NULL, 'STC', 'General', 96, 'MT', 150, NULL, 'SOC CONTAINER OF GENERAL CARGO', NULL, '4,20\'GP;1,40\'HC', '2019-07-06 03:52:10', '2019-07-06 03:52:10'),
(107, 119, 'PGBKI0020', 1, '', '1 x 40\'HC', '<div><br></div><div><br></div><div><br></div><div>GRANTED 21 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE&nbsp;</div><div>AT POD</div>', 'General', NULL, 'STC. 1072 PKGS', 'General', 6332, 'MT', 45, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>340 CT BOX TISSUE</div><div>&nbsp;20 BD JRT</div><div>&nbsp;30 CT POCKET TISSUE</div><div>100 CT SERVIETTE NORMAL</div><div>582 GR TOILET ROLL</div><div><br></div><div>HS CODE 4818.10.0000</div><div><br></div><div>AS PER INVOICE NO: LN007569</div><div><br></div><div>DESTINATION THC, DO FEE, LCHC</div><div>CHC &amp; EDI FEE PREPAID AT PENANG.&nbsp;</div>', NULL, '1,40\'HC', '2019-07-06 04:14:07', '2019-07-09 06:55:54'),
(108, 120, 'PGBKI0021', 1, '', '1 x 40\'GP', '<div><br></div><div><br></div>GRANTED 21 DAYS FREE TIME<div>DETENTION &amp; DEMURRAGE&nbsp;</div><div>AT POD</div><div><br></div><div><div><span style=\"line-height: 1.42857;\">DESTINATION THC, DO FEE, LCHC</span></div><div><span style=\"line-height: 1.42857;\">CHC &amp; EDI FEE PREPAID</span></div><div><span style=\"line-height: 1.42857;\">AT PENANG</span></div></div>', 'General', NULL, 'STC.', 'General', 4735.5, 'KG', 45, NULL, 'FCL / FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC. 816 PKGS</div><div>100 CT BOX TISSUE</div><div>&nbsp; 50 CT COTTON</div><div>545 BD TISSUE PRODUCTS (TOILET TISSUE)</div><div>&nbsp; 50 CT TOWEL</div><div>&nbsp; &nbsp; 5 CT HANGER</div><div>&nbsp; 10 RL SKIRTING</div><div>&nbsp; &nbsp; &nbsp;6 PKT A-STAND</div><div>&nbsp; &nbsp;50 CT WET TISSUE</div><div><br></div><div>HS CODE:4818.10.000</div><div>AS PER INVOICE NO:CS000834<br></div><div><span style=\"line-height: 1.42857;\"><br></span></div><div><br></div><div><br></div>', NULL, '1,40\'GP', '2019-07-06 04:14:36', '2019-07-09 07:10:56'),
(109, 121, 'PGSDK0008', 1, '', '1 x 40\'HC', '<div><br></div><div><br></div>GRANTED 21 DAYS FREE TIME<div>DETENTION &amp; DEMURRAGE</div><div>AT POD</div><div><br></div><div>DESTINATION PORT THC, CHC,</div><div>LCHC, GOPC, DOC FEE AND</div><div>EDI FEE PREPAID AT PENANG</div>', 'General', NULL, 'STC.', 'General', 9659, 'KG', 45, NULL, 'FCL / FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC. 949 PKGS</div><div>100 CT BABY DIAPERS&nbsp;</div><div><span style=\"line-height: 1.42857;\">849 BD TOILET ROLL</span></div><div><br></div><div>HS CODE:4818.10.0000</div><div>AS PER INVOICE NO:LN007565</div>', NULL, '1,40\'HC', '2019-07-06 04:19:09', '2019-07-09 07:00:19'),
(110, 122, 'PGSDK0009', 1, '', '1 x 40\'HC', '<div><br></div><div><br></div>GRANTED 21 DAYS FREE TIME<div>DETENTION &amp; DEMURRAGE&nbsp;</div><div>AT POD</div><div><br></div><div>DESTINATION PORT THC, CHC,</div><div>LCHC, GOPC, DOC FEE AND</div><div>EDI FEE PREPAID AT PENANG</div>', 'General', NULL, 'STC.', 'General', 8270.1, 'KG', 45, NULL, 'FCL / FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC. 999 PKGS</div><div>8 CT ADULT DIAPERS</div><div>453 CT BABY DIAPERS</div><div>12 CT BOX TISSUE</div><div>19 CT COTTON PRODUCT</div><div>16 CT POCKET TISSUE</div><div>72 CT SANITARY NAPKIN</div><div>315 BD TOILET ROLL</div><div>104 CT WET TISSUE</div><div>HS CODE:9619.00.1300</div><div>AS PER INVOICE NO:LN007572</div>', NULL, '1,40\'HC', '2019-07-06 04:22:48', '2019-07-06 04:22:48'),
(111, 123, 'PGMYY0006', 1, '', '1 x 40\'HC', '<div><br></div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE</div><div>AT POD</div>', 'General', NULL, 'STC.', 'General', 25558.7, 'KG', 67, NULL, 'FCL / FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC.&nbsp;</div><div>3345 PKGS OF HOUSEHOLD FURNITURE</div><div><br></div><div>1)HOUSEHOLD FURNITURE</div>', NULL, '1,40\'HC', '2019-07-06 04:25:58', '2019-07-06 04:25:58'),
(112, 124, 'PGSBW0022', 3, '', '3 x 40\'HC', '<div><br></div><div><br></div><div>GRANTED 21 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE</div><div>AT POD</div><div><br></div><div>DESTINATION PORT THC, CMR,</div><div>DOC FEE AND EDI FEE&nbsp;</div><div>PREPAID AT PENANG</div><div><br></div><div>AS PER INVOICE NO.LN007575,</div><div>LN007576 AND LN007571</div>', 'General', NULL, 'STC. 2583 PKGS', 'General', 22026.52, 'KG', 135, NULL, '<div>FCL / FCL</div><div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div>CONTAINER STC.</div><div>472<span style=\"white-space:pre\">	</span>CTNS<span style=\"white-space:pre\">	</span>BABY DIAPERS (9619.00.1300)<span style=\"white-space:pre\">				</span></div><div>506<span style=\"white-space:pre\">	</span>CTNS<span style=\"white-space:pre\">	</span>SERVIETTE NORMAL (4818.30.2000)<span style=\"white-space:pre\">				</span></div><div>840<span style=\"white-space:pre\">	</span>BDLS<span style=\"white-space:pre\">	</span>TOILET ROLL&nbsp; (4818.10.0000)<span style=\"white-space:pre\">				</span></div><div>195<span style=\"white-space:pre\">	</span>CTNS<span style=\"white-space:pre\">	</span>BOX TISSUE (4818.20.0000)<span style=\"white-space:pre\">				</span></div><div>10<span style=\"white-space:pre\">	</span>CTNS<span style=\"white-space:pre\">	</span>HANKY PACK (4818.20.0000)<span style=\"white-space:pre\">				</span></div><div>250<span style=\"white-space:pre\">	</span>BDLS<span style=\"white-space:pre\">	</span>KICTHEN TOWEL (4818.20.0000)<span style=\"white-space:pre\">				</span></div><div>60<span style=\"white-space:pre\">	</span>CTNS<span style=\"white-space:pre\">	</span>POCKET TISSUE (4818.20.0000)<span style=\"white-space:pre\">				</span><span style=\"line-height: 1.42857; white-space: pre;\">				</span></div><div>250<span style=\"white-space:pre\">	</span>CTNS<span style=\"white-space:pre\">	</span>WET TISSUE (3307.90.9000)<span style=\"white-space:pre\">				</span></div><div><br></div>', NULL, '3,40\'HC', '2019-07-06 04:33:45', '2019-07-09 06:33:26'),
(124, 126, 'PGBKI0022', 1, '', '1 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 25000, 'KG', 25, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1,000 BAGS CALCIUM CARBONATE</div><div>(GRADE: ZANCARB 4)</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,20\'GP', '2019-07-09 07:52:24', '2019-07-09 07:52:24'),
(113, 125, 'PGBTU0003', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC. 1191 PKGS', 'General', 10510.5, 'MT', 45, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1191 GR TOILET ROLL</div><div><br></div><div>H.S. CODE: 4818.10.0000</div><div><br></div><div>AS PER INVOICE NO: LN007570</div><div><br></div><div>GRANTED 21 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,40\'HC', '2019-07-09 00:48:51', '2019-07-09 06:40:52'),
(114, 127, 'PGSBW0023', 1, '', '1 x 20\'GP', '<div><br></div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE&nbsp;</div><div>AT POD</div>', 'General', NULL, 'STC.', 'General', 11129.64, 'KG', 30.552, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1049 CTNS FOODSTUFF</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 3 V.3430\"</div><div>&nbsp;</div><div>ACTUAL MEASUREMENT:30.5522 M3</div><div><br></div><div><br></div><div><br></div><div><br></div><div><br></div>', NULL, '1,20\'GP', '2019-07-09 01:00:41', '2019-07-09 04:58:45'),
(115, 135, 'PGSBW0027', 3, '', '3 x 20\'GP', NULL, 'General', NULL, 'STC.', 'General', 56376, 'KG', 70.11, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>8,700 CARTONS OF FOODSTUFF</div><div><br></div><div>H.S CODE: 2202.99</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 3 V.3430\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '3,20\'GP', '2019-07-09 01:11:25', '2019-07-09 01:11:25'),
(116, 131, 'PGBKI0025', 1, '', '1 x 40\'GP', NULL, 'General', NULL, 'STC.', 'General', 9307.495, 'KG', 30, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1038 PCS TYRES</div><div><br></div><div>DESTINATION THC, DO FEE, LCHC</div><div>CHC &amp; EDI FEE PREPAID AT PENANG</div><div><br></div><div>GRANTED 18 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,40\'GP', '2019-07-09 01:36:20', '2019-07-09 01:36:20'),
(117, 130, 'PGBKI0024', 3, '', '3 x 40\'GP', NULL, 'General', NULL, 'STC.', 'General', 26574.484, 'KG', 60, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1038 PCS TYRES</div><div><br></div><div>DESTINATION THC, DO FEE, LCHC,</div><div>CHC &amp; EDI FEE PREPAID AT PENANG</div><div><br></div><div>GRANTED 18 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '3,40\'GP', '2019-07-09 01:45:41', '2019-07-09 01:45:41'),
(118, 136, 'PGPKG0013', 5, '', '4 x 20\'GP & 1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 96000, 'KG', 150, NULL, 'SOC CONTAINER OF GENERAL CARGO', NULL, '4,20\'GP;1,40\'HC', '2019-07-09 01:46:20', '2019-07-09 01:46:20'),
(119, 133, 'PGSBW0025', 1, '', '1 x 40\'HC', NULL, 'General', NULL, 'STC.', 'General', 6505.89, 'KG', 30, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, CIOUNT &amp; SEALED</div><div><br></div><div>601 PKGS OF PLASTIC HOUSEHOLD WARES</div><div><br></div><div>H.S. CODE: 3924 90</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 3 V.3430\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE AT POD</div><div><br></div><div><br></div>', NULL, '1,40\'HC', '2019-07-09 03:44:17', '2019-07-09 03:44:17'),
(120, 132, 'PGTWU0006', 1, '', '1 x 20\'GP', '<div><br></div><div><br></div>GRANTED 14 DAYS FREE TIME<div>DETENTION &amp; DEMURRAGE</div><div>AT POD</div><div><br></div><div>DESTINATION PORT THC, CHC,</div><div>LCHC, GOPC, DOC FEE AND</div><div>EDI FEE PREPAID AT</div><div>PENANG</div>', 'General', NULL, 'STC.', 'General', 4989.4, 'KG', 27.46, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>1411 CARTONS INSTANT NOOLDE &amp; PASTE</div><div><br></div><div>HS CODE: 1902 30 40 00, 1902 20 00,</div><div>2103.90 &nbsp;19 00 &amp; 2103 90 12 00</div><div><br></div><div>INVOICE NO&gt; 1907.00040</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV \"DANUM 172\" 7.72016</div><div><br></div>', NULL, '1,20\'GP', '2019-07-09 04:02:03', '2019-07-09 04:52:42'),
(121, 134, 'PGSBW0026', 1, '', '1 x 20\'GP', '<div><br></div><div><br></div><div>MARKING:</div><div>-------------</div><div>PET-01</div><div><br></div><div>T/S AT PORT KLANG BY VESSEL</div><div>MV. \"DANUM 3 V.3430\"</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE&nbsp;</div><div>AT POD</div>', 'General', NULL, 'STC. 520 ROLLS', 'General', 10608, 'KG', 10.088, NULL, 'FCL/FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>520 ROLLS - POLYESTER STRAPS</div><div>PRODUCT MODEL : PET-01</div><div>416 ROLLS - 15.50MM. X 0.90MM.</div><div>(DARK GREEN/SMOOTH) ID CORE 406</div><div>N.W. 7,904.00 KGM &nbsp;G.W. 8,320.00 KGM.</div><div>104 ROLLS - 15.50MM. X 0.85MM.</div><div>(DARK GREEN/EMBOSSED) ID CORE 406</div><div>N.W. 2,184.00 KGM. &nbsp;G.W. 2,288.00 KGM.</div><div><br></div><div>TOTAL NET WEIGHT: 10,088.00 KGM</div><div>TOTAL GROSS WEIGHT: 10,608.00 KGM</div><div><br></div><div><br></div><div><br></div><div><br></div><div><br></div>', NULL, '1,20\'GP', '2019-07-09 04:20:12', '2019-07-09 04:20:12'),
(122, 129, 'PGBKI0023', 8, '', '8 x 20\'GP', '<div>SHIPPING MARKS:<br></div><div>-----------------------</div><div>MSM PERLIS</div><div>SDN BHD</div><div><br></div><div><div>DESTINATION SHIPPING LOCAL CHARGES &nbsp;<span style=\"line-height: 1.42857;\">PREPAID IN&nbsp;</span>LOADING PORT</div></div><div><div><br></div><div><br></div></div><div><div><br></div></div>', 'General', NULL, 'STC 121,535 BAGS', 'General', 195, 'MT', 72, NULL, 'FCL/FCL<div><div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>121,505 BAGS REFINED SUGAR C/W 30 EMPTY BAGS<br></div><div><div>1KG REFINED SUGAR x 120,000 BAGS<br></div><div>NORMAL GRAIN &nbsp;<span style=\"line-height: 1.42857;\">( 120.00 METRIC TON )</span></div><div>50KG REFINED SUGAR x 1,000 BAGS<br></div><div>NORMAL GRAIN &nbsp;<span style=\"line-height: 1.42857;\">( 50.00 METRIC TON )</span></div><div>50KG REFINED SUGAR x 480 BAGS<br></div><div>FINE GRAIN &nbsp;&nbsp;<span style=\"line-height: 1.42857;\">( 24.00 METRIC TON )</span></div><div>40KG ICING SUGAR x 25 BAGS<br></div><div>( 1.00 METRIC TON )</div><div>HS CODE : 1701.99 1000<br></div><div><div><div><br></div></div></div><div><br></div><div><br></div><div><br></div><div><br></div><div><br></div><div><br></div></div></div>', NULL, '8,20\'GP', '2019-07-09 05:07:33', '2019-07-09 07:35:19'),
(123, 128, 'PGSBW0024', 2, '', '2 x 20\'GP', '<div><br></div><div><br></div>T/S AT PORT KLANG BY VESSEL<div>\"DANUM 3\" V.3430</div><div><br></div><div>GRANTED 14 DAYS FREE TIME</div><div>DETENTION &amp; DEMURRAGE</div><div>AT POD</div>', 'General', NULL, 'STC.', 'General', 54000, 'KG', 44, NULL, 'FCL / FCL<div>SHIPPER\'S LOAD, COUNT &amp; SEALED</div><div><br></div><div>CONTAINER STC.</div><div>(1,080 BAGS) OF DOLIMITE</div><div><br></div>', NULL, '2,20\'GP', '2019-07-09 05:08:39', '2019-07-09 05:08:39');

-- --------------------------------------------------------

--
-- Table structure for table `bill_charges`
--

DROP TABLE IF EXISTS `bill_charges`;
CREATE TABLE IF NOT EXISTS `bill_charges` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bl_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `charge_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `charge_unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `charge_rate` double NOT NULL,
  `charge_payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=308 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bill_charges`
--

INSERT INTO `bill_charges` (`id`, `bl_no`, `charge_code`, `charge_unit`, `charge_rate`, `charge_payment`, `created_at`, `updated_at`) VALUES
(1, '3', '1', '20-GP', 600, 'P', '2019-04-11 06:28:10', '2019-04-11 06:28:10'),
(2, '5', '3', '20-GP', 550, 'P', '2019-06-04 06:46:27', '2019-06-04 06:46:27'),
(3, '5', '2', '20-GP', 1061, 'P', '2019-06-04 06:46:42', '2019-06-04 06:46:42'),
(4, '5', '5', '20-GP', 295, 'P', '2019-06-04 06:46:51', '2019-06-04 06:46:51'),
(5, '6', '3', '20-GP', 550, 'P', '2019-06-04 07:08:09', '2019-06-04 07:08:09'),
(12, '6', '9', '20-GP', 1061, 'P', '2019-06-11 02:31:20', '2019-06-11 02:31:20'),
(7, '6', '5', '20-GP', 295, 'P', '2019-06-04 07:08:29', '2019-06-04 07:08:29'),
(8, '6', '6', '20-GP', 36.25, 'P', '2019-06-04 07:08:36', '2019-06-04 07:08:36'),
(9, '6', '7', '1-SET', 175, 'P', '2019-06-04 07:08:44', '2019-06-04 07:08:44'),
(10, '6', '8', '1-SET', 30, 'P', '2019-06-04 07:08:56', '2019-06-04 07:08:56'),
(13, '7', '3', '40-HC', 1100, 'C', '2019-06-11 02:34:18', '2019-06-11 02:34:18'),
(14, '7', '9', '40-HC', 2122, 'C', '2019-06-11 02:34:27', '2019-06-11 02:34:27'),
(15, '8', '3', '20-GP', 1000, 'C', '2019-06-11 02:47:19', '2019-06-11 02:47:19'),
(16, '8', '9', '20-GP', 1061, 'C', '2019-06-11 02:47:28', '2019-06-11 02:47:28'),
(17, '9', '3', '20-GP', 550, 'P', '2019-06-11 02:48:50', '2019-06-11 02:48:50'),
(18, '9', '9', '20-GP', 1061, 'P', '2019-06-11 02:49:05', '2019-06-11 02:49:05'),
(19, '13', '3', '20-GP', 1150, 'P', '2019-06-27 05:05:47', '2019-06-27 05:05:47'),
(20, '13', '9', '20-GP', 999, 'P', '2019-06-27 05:05:55', '2019-06-27 05:05:55'),
(23, '14', '3', '20-GP', 1150, 'P', '2019-06-27 06:23:14', '2019-06-27 06:23:14'),
(24, '14', '9', '20-GP', 999, 'P', '2019-06-27 06:23:37', '2019-06-27 06:23:37'),
(25, '15', '3', '40-HC', 1400, 'P', '2019-07-01 01:58:47', '2019-07-01 01:58:47'),
(26, '15', '9', '40-HC', 1998, 'P', '2019-07-01 01:58:56', '2019-07-01 01:58:56'),
(27, '31', '3', '20-GP', 800, 'P', '2019-07-01 08:36:29', '2019-07-01 08:36:29'),
(28, '31', '9', '20-GP', 999, 'P', '2019-07-01 08:36:36', '2019-07-01 08:36:36'),
(67, '32', '3', '40-HC', 1925, 'P', '2019-07-03 02:33:43', '2019-07-03 02:33:43'),
(30, '32', '9', '40-HC', 1998, 'P', '2019-07-01 08:43:53', '2019-07-01 08:43:53'),
(31, '32', '5', '40-HC', 440, 'P', '2019-07-01 08:44:03', '2019-07-01 08:44:03'),
(182, '32', '7', '1-SET', 175, 'P', '2019-07-03 05:46:03', '2019-07-03 05:46:03'),
(33, '32', '8', '1-SET', 30, 'P', '2019-07-01 08:44:23', '2019-07-01 08:44:23'),
(34, '40', '3', '20-GP', 1600, 'P', '2019-07-02 03:51:37', '2019-07-02 03:51:37'),
(35, '40', '9', '20-GP', 999, 'P', '2019-07-02 03:51:44', '2019-07-02 03:51:44'),
(36, '40', '5', '20-GP', 295, 'P', '2019-07-02 03:51:53', '2019-07-02 03:51:53'),
(37, '40', '6', '20-GP', 37, 'P', '2019-07-02 03:52:05', '2019-07-02 03:52:05'),
(38, '40', '7', '1-SET', 175, 'P', '2019-07-02 03:52:18', '2019-07-02 03:52:18'),
(39, '40', '8', '1-SET', 30, 'P', '2019-07-02 03:52:30', '2019-07-02 03:52:30'),
(40, '45', '3', '20-GP', 420.5, 'P', '2019-07-02 08:24:10', '2019-07-02 08:24:10'),
(41, '27', '3', '20-GP', 750, 'P', '2019-07-02 08:25:45', '2019-07-02 08:25:45'),
(42, '27', '9', '20-GP', 999, 'P', '2019-07-02 08:25:52', '2019-07-02 08:25:52'),
(43, '26', '3', '20-GP', 800, 'P', '2019-07-02 08:28:37', '2019-07-02 08:28:37'),
(44, '26', '9', '20-GP', 999, 'P', '2019-07-02 08:28:44', '2019-07-02 08:28:44'),
(54, '38', '3', '40-HC', 2700, 'C', '2019-07-03 02:25:12', '2019-07-03 02:25:12'),
(55, '38', '9', '40-HC', 1998, 'C', '2019-07-03 02:25:33', '2019-07-03 02:25:33'),
(47, '48', '3', '40-HC', 1313, 'P', '2019-07-03 02:18:42', '2019-07-03 02:18:42'),
(48, '48', '9', '40-HC', 1998, 'P', '2019-07-03 02:18:49', '2019-07-03 02:18:49'),
(53, '47', '3', '20-GP', 800, 'C', '2019-07-03 02:23:30', '2019-07-03 02:23:30'),
(56, '47', '9', '20-GP', 999, 'C', '2019-07-03 02:25:57', '2019-07-03 02:25:57'),
(58, '37', '9', '40-HC', 1998, 'C', '2019-07-03 02:26:42', '2019-07-03 02:26:42'),
(57, '37', '3', '40-HC', 1400, 'C', '2019-07-03 02:26:31', '2019-07-03 02:26:31'),
(59, '29', '3', '20-GP', 750, 'P', '2019-07-03 02:27:42', '2019-07-03 02:27:42'),
(60, '29', '9', '20-GP', 999, 'P', '2019-07-03 02:27:58', '2019-07-03 02:27:58'),
(61, '35', '3', '40-HC', 1313, 'P', '2019-07-03 02:28:47', '2019-07-03 02:28:47'),
(62, '35', '9', '40-HC', 1998, 'P', '2019-07-03 02:28:55', '2019-07-03 02:28:55'),
(63, '35', '5', '40-HC', 440, 'P', '2019-07-03 02:29:06', '2019-07-03 02:29:06'),
(64, '35', '6', '40-HC', 77, 'P', '2019-07-03 02:29:17', '2019-07-03 02:29:17'),
(65, '35', '7', '1-SET', 175, 'P', '2019-07-03 02:29:24', '2019-07-03 02:29:24'),
(66, '35', '8', '1-SET', 30, 'P', '2019-07-03 02:29:43', '2019-07-03 02:29:43'),
(68, '17', '3', '20-GP', 1300, 'P', '2019-07-03 02:41:55', '2019-07-03 02:41:55'),
(69, '17', '9', '20-GP', 999, 'P', '2019-07-03 02:45:03', '2019-07-03 02:45:03'),
(70, '17', '5', '20-GP', 295, 'P', '2019-07-03 02:45:15', '2019-07-03 02:45:15'),
(71, '17', '12', '20-GP', 150, 'P', '2019-07-03 02:45:23', '2019-07-03 02:45:23'),
(72, '17', '11', '20-GP', 50, 'P', '2019-07-03 02:45:31', '2019-07-03 02:45:31'),
(73, '17', '10', '20-GP', 148, 'P', '2019-07-03 02:45:42', '2019-07-03 02:45:42'),
(74, '17', '7', '1-SET', 175, 'P', '2019-07-03 02:45:50', '2019-07-03 02:45:50'),
(75, '17', '8', '1-SET', 30, 'P', '2019-07-03 02:45:57', '2019-07-03 02:45:57'),
(76, '19', '3', '20-GP', 1300, 'P', '2019-07-03 02:46:17', '2019-07-03 02:46:17'),
(77, '19', '9', '20-GP', 999, 'P', '2019-07-03 02:46:25', '2019-07-03 02:46:25'),
(78, '19', '5', '20-GP', 295, 'P', '2019-07-03 02:46:35', '2019-07-03 02:46:35'),
(79, '19', '11', '20-GP', 50, 'P', '2019-07-03 02:46:42', '2019-07-03 02:46:42'),
(80, '19', '12', '20-GP', 150, 'P', '2019-07-03 02:46:51', '2019-07-03 02:46:51'),
(81, '19', '10', '20-GP', 148, 'P', '2019-07-03 02:46:59', '2019-07-03 02:46:59'),
(82, '19', '7', '1-SET', 175, 'P', '2019-07-03 02:47:07', '2019-07-03 02:47:07'),
(83, '19', '8', '1-SET', 30, 'P', '2019-07-03 02:47:15', '2019-07-03 02:47:15'),
(84, '18', '3', '20-GP', 1300, 'P', '2019-07-03 02:48:38', '2019-07-03 02:48:38'),
(85, '18', '9', '20-GP', 999, 'P', '2019-07-03 02:48:44', '2019-07-03 02:48:44'),
(86, '18', '5', '20-GP', 295, 'P', '2019-07-03 02:48:56', '2019-07-03 02:48:56'),
(87, '18', '11', '20-GP', 50, 'P', '2019-07-03 02:49:02', '2019-07-03 02:49:02'),
(88, '18', '12', '20-GP', 150, 'P', '2019-07-03 02:49:13', '2019-07-03 02:49:13'),
(89, '18', '10', '20-GP', 148, 'P', '2019-07-03 02:49:21', '2019-07-03 02:49:21'),
(90, '18', '7', '1-SET', 175, 'P', '2019-07-03 02:49:29', '2019-07-03 02:49:29'),
(91, '18', '8', '1-SET', 30, 'P', '2019-07-03 02:49:38', '2019-07-03 02:49:38'),
(92, '25', '3', '20-GP', 700, 'P', '2019-07-03 02:58:41', '2019-07-03 02:58:41'),
(93, '25', '9', '20-GP', 999, 'P', '2019-07-03 02:58:48', '2019-07-03 02:58:48'),
(94, '25', '5', '20-GP', 295, 'P', '2019-07-03 02:59:27', '2019-07-03 02:59:27'),
(95, '25', '11', '20-GP', 50, 'P', '2019-07-03 03:13:44', '2019-07-03 03:13:44'),
(96, '25', '10', '20-GP', 148, 'P', '2019-07-03 03:13:55', '2019-07-03 03:13:55'),
(97, '25', '7', '1-SET', 175, 'P', '2019-07-03 03:14:02', '2019-07-03 03:14:02'),
(98, '25', '8', '1-SET', 30, 'P', '2019-07-03 03:14:15', '2019-07-03 03:14:15'),
(99, '24', '3', '20-GP', 700, 'P', '2019-07-03 03:15:16', '2019-07-03 03:15:16'),
(100, '24', '9', '20-GP', 999, 'P', '2019-07-03 03:15:22', '2019-07-03 03:15:22'),
(101, '24', '5', '20-GP', 295, 'P', '2019-07-03 03:15:30', '2019-07-03 03:15:30'),
(102, '24', '11', '20-GP', 50, 'P', '2019-07-03 03:15:35', '2019-07-03 03:15:35'),
(103, '24', '10', '20-GP', 148, 'P', '2019-07-03 03:15:43', '2019-07-03 03:15:43'),
(104, '24', '7', '1-SET', 175, 'P', '2019-07-03 03:15:51', '2019-07-03 03:15:51'),
(105, '24', '7', '1-SET', 30, 'P', '2019-07-03 03:15:57', '2019-07-03 03:15:57'),
(106, '22', '3', '20-GP', 700, 'P', '2019-07-03 03:16:19', '2019-07-03 03:16:19'),
(107, '22', '9', '20-GP', 999, 'P', '2019-07-03 03:16:25', '2019-07-03 03:16:25'),
(108, '22', '4', '20-GP', 295, 'P', '2019-07-03 03:16:35', '2019-07-03 03:16:35'),
(109, '22', '11', '20-GP', 50, 'P', '2019-07-03 03:16:40', '2019-07-03 03:16:40'),
(110, '22', '10', '20-GP', 148, 'P', '2019-07-03 03:16:49', '2019-07-03 03:16:49'),
(111, '22', '7', '1-SET', 175, 'P', '2019-07-03 03:16:57', '2019-07-03 03:16:57'),
(112, '22', '8', '1-SET', 30, 'P', '2019-07-03 03:17:11', '2019-07-03 03:17:11'),
(113, '21', '3', '20-GP', 700, 'P', '2019-07-03 03:17:32', '2019-07-03 03:17:32'),
(114, '21', '9', '20-GP', 999, 'P', '2019-07-03 03:17:37', '2019-07-03 03:17:37'),
(115, '21', '5', '20-GP', 295, 'P', '2019-07-03 03:17:44', '2019-07-03 03:17:44'),
(116, '21', '11', '20-GP', 50, 'P', '2019-07-03 03:17:49', '2019-07-03 03:17:49'),
(117, '21', '10', '20-GP', 148, 'P', '2019-07-03 03:17:56', '2019-07-03 03:17:56'),
(118, '21', '7', '1-SET', 175, 'P', '2019-07-03 03:18:02', '2019-07-03 03:18:02'),
(119, '21', '8', '1-SET', 30, 'P', '2019-07-03 03:18:15', '2019-07-03 03:18:15'),
(120, '20', '3', '20-GP', 700, 'P', '2019-07-03 03:18:31', '2019-07-03 03:18:31'),
(121, '20', '9', '20-GP', 999, 'P', '2019-07-03 03:18:36', '2019-07-03 03:18:36'),
(122, '20', '5', '20-GP', 295, 'P', '2019-07-03 03:18:43', '2019-07-03 03:18:43'),
(123, '20', '11', '20-GP', 50, 'P', '2019-07-03 03:18:48', '2019-07-03 03:18:48'),
(124, '20', '10', '20-GP', 148, 'P', '2019-07-03 03:18:55', '2019-07-03 03:18:55'),
(125, '20', '7', '1-SET', 175, 'P', '2019-07-03 03:19:04', '2019-07-03 03:19:04'),
(126, '20', '8', '1-SET', 30, 'P', '2019-07-03 03:19:11', '2019-07-03 03:19:11'),
(127, '41', '3', '40-HC', 1313, 'P', '2019-07-03 03:19:45', '2019-07-03 03:19:45'),
(128, '41', '9', '40-HC', 1998, 'P', '2019-07-03 03:19:52', '2019-07-03 03:19:52'),
(129, '23', '3', '20-GP', 550, 'P', '2019-07-03 03:20:19', '2019-07-03 03:20:19'),
(130, '23', '9', '20-GP', 999, 'P', '2019-07-03 03:20:25', '2019-07-03 03:20:25'),
(131, '23', '6', '20-GP', 36.25, 'P', '2019-07-03 03:20:31', '2019-07-03 03:20:31'),
(132, '23', '7', '1-SET', 175, 'P', '2019-07-03 03:20:38', '2019-07-03 03:20:38'),
(133, '23', '8', '1-SET', 30, 'P', '2019-07-03 03:20:45', '2019-07-03 03:20:45'),
(134, '46', '3', '20-GP', 800, 'P', '2019-07-03 03:21:37', '2019-07-03 03:21:37'),
(135, '46', '9', '20-GP', 999, 'P', '2019-07-03 03:21:42', '2019-07-03 03:21:42'),
(136, '43', '3', '20-GP', 800, 'P', '2019-07-03 03:21:59', '2019-07-03 03:21:59'),
(137, '43', '9', '20-GP', 999, 'P', '2019-07-03 03:22:04', '2019-07-03 03:22:04'),
(138, '44', '3', '40-HC', 1225, 'P', '2019-07-03 03:22:30', '2019-07-03 03:22:30'),
(139, '44', '9', '40-HC', 1998, 'P', '2019-07-03 03:22:36', '2019-07-03 03:22:36'),
(140, '44', '5', '40-HC', 440, 'P', '2019-07-03 03:22:43', '2019-07-03 03:22:43'),
(141, '44', '11', '40-HC', 100, 'P', '2019-07-03 03:22:49', '2019-07-03 03:22:49'),
(142, '44', '10', '40-HC', 260, 'P', '2019-07-03 03:22:58', '2019-07-03 03:22:58'),
(143, '44', '7', '1-SET', 175, 'P', '2019-07-03 03:23:05', '2019-07-03 03:23:05'),
(144, '44', '8', '1-SET', 30, 'P', '2019-07-03 03:23:13', '2019-07-03 03:23:13'),
(145, '30', '3', '20-GP', 800, 'P', '2019-07-03 03:23:53', '2019-07-03 03:23:53'),
(146, '30', '9', '20-GP', 999, 'P', '2019-07-03 03:23:58', '2019-07-03 03:23:58'),
(147, '42', '3', '40-HC', 1225, 'P', '2019-07-03 03:24:16', '2019-07-03 03:24:16'),
(148, '42', '9', '40-HC', 1998, 'P', '2019-07-03 03:24:21', '2019-07-03 03:24:21'),
(149, '42', '5', '40-HC', 440, 'P', '2019-07-03 03:24:28', '2019-07-03 03:24:28'),
(150, '42', '11', '40-HC', 100, 'P', '2019-07-03 03:24:34', '2019-07-03 03:24:34'),
(151, '42', '10', '40-HC', 260, 'P', '2019-07-03 03:24:43', '2019-07-03 03:24:43'),
(152, '42', '7', '1-SET', 175, 'P', '2019-07-03 03:24:49', '2019-07-03 03:24:49'),
(153, '42', '8', '1-SET', 30, 'P', '2019-07-03 03:24:56', '2019-07-03 03:24:56'),
(154, '33', '3', '20-GP', 750, 'P', '2019-07-03 03:25:43', '2019-07-03 03:25:43'),
(155, '33', '9', '20-GP', 999, 'P', '2019-07-03 03:25:48', '2019-07-03 03:25:48'),
(156, '33', '5', '20-GP', 295, 'P', '2019-07-03 03:26:02', '2019-07-03 03:26:02'),
(157, '33', '11', '20-GP', 50, 'P', '2019-07-03 03:26:08', '2019-07-03 03:26:08'),
(158, '33', '10', '20-GP', 148, 'P', '2019-07-03 03:26:19', '2019-07-03 03:26:19'),
(159, '33', '7', '1-SET', 175, 'P', '2019-07-03 03:26:28', '2019-07-03 03:26:28'),
(160, '33', '8', '1-SET', 30, 'P', '2019-07-03 03:26:37', '2019-07-03 03:26:37'),
(161, '16', '3', '40-HC', 1313, 'P', '2019-07-03 03:26:54', '2019-07-03 03:26:54'),
(162, '16', '9', '40-HC', 1998, 'P', '2019-07-03 03:27:02', '2019-07-03 03:27:02'),
(163, '34', '3', '20-GP', 1400, 'P', '2019-07-03 03:27:20', '2019-07-03 03:27:20'),
(164, '34', '9', '20-GP', 999, 'P', '2019-07-03 03:27:26', '2019-07-03 03:27:26'),
(165, '34', '5', '20-GP', 295, 'P', '2019-07-03 03:27:35', '2019-07-03 03:27:35'),
(166, '34', '11', '20-GP', 50, 'P', '2019-07-03 03:27:40', '2019-07-03 03:27:40'),
(167, '34', '12', '20-GP', 150, 'P', '2019-07-03 03:27:46', '2019-07-03 03:27:46'),
(168, '34', '10', '20-GP', 148, 'P', '2019-07-03 03:27:53', '2019-07-03 03:27:53'),
(169, '34', '7', '1-SET', 175, 'P', '2019-07-03 03:27:59', '2019-07-03 03:27:59'),
(170, '34', '8', '1-SET', 30, 'P', '2019-07-03 03:28:09', '2019-07-03 03:28:09'),
(171, '36', '3', '40-HC', 2450, 'P', '2019-07-03 03:28:42', '2019-07-03 03:28:42'),
(172, '36', '9', '40-HC', 1998, 'P', '2019-07-03 03:28:47', '2019-07-03 03:28:47'),
(173, '36', '5', '40-HC', 440, 'P', '2019-07-03 03:28:54', '2019-07-03 03:28:54'),
(174, '36', '11', '40-HC', 100, 'P', '2019-07-03 03:28:59', '2019-07-03 03:28:59'),
(175, '36', '10', '40-HC', 260, 'P', '2019-07-03 03:29:13', '2019-07-03 03:29:13'),
(176, '36', '12', '40-HC', 300, 'P', '2019-07-03 03:29:22', '2019-07-03 03:29:22'),
(177, '36', '7', '1-SET', 175, 'P', '2019-07-03 03:29:30', '2019-07-03 03:29:30'),
(178, '36', '8', '1-SET', 30, 'P', '2019-07-03 03:29:37', '2019-07-03 03:29:37'),
(179, '28', '3', '20-GP', 1400, 'P', '2019-07-03 03:29:58', '2019-07-03 03:29:58'),
(180, '28', '9', '20-GP', 999, 'P', '2019-07-03 03:30:03', '2019-07-03 03:30:03'),
(181, '23', '5', '20-GP', 295, 'P', '2019-07-03 05:45:35', '2019-07-03 05:45:35'),
(183, '52', '3', '40-HC', 963, 'P', '2019-07-05 00:51:26', '2019-07-05 00:51:26'),
(184, '52', '9', '40-HC', 1998, 'P', '2019-07-05 00:51:34', '2019-07-05 00:51:34'),
(185, '54', '3', '20-GP', 800, 'P', '2019-07-05 00:51:50', '2019-07-05 00:51:50'),
(186, '54', '9', '20-GP', 999, 'P', '2019-07-05 00:51:56', '2019-07-05 00:51:56'),
(187, '58', '3', '20-GP', 800, 'P', '2019-07-05 00:52:15', '2019-07-05 00:52:15'),
(188, '58', '9', '20-GP', 999, 'P', '2019-07-05 00:52:20', '2019-07-05 00:52:20'),
(189, '57', '3', '20-GP', 1400, 'P', '2019-07-05 00:52:41', '2019-07-05 00:52:41'),
(190, '57', '9', '20-GP', 999, 'P', '2019-07-05 00:52:46', '2019-07-05 00:52:46'),
(191, '56', '3', '20-GP', 1400, 'P', '2019-07-05 00:53:27', '2019-07-05 00:53:27'),
(192, '56', '9', '20-GP', 999, 'P', '2019-07-05 00:53:34', '2019-07-05 00:53:34'),
(193, '55', '3', '20-GP', 800, 'C', '2019-07-05 00:54:17', '2019-07-05 00:54:17'),
(194, '55', '9', '20-GP', 999, 'C', '2019-07-05 00:54:27', '2019-07-05 00:54:27'),
(195, '53', '3', '20-GP', 1600, 'P', '2019-07-05 01:03:01', '2019-07-05 01:03:01'),
(196, '53', '9', '20-GP', 999, 'P', '2019-07-05 01:03:09', '2019-07-05 01:03:09'),
(197, '51', '3', '40-HC', 1400, 'P', '2019-07-05 01:03:55', '2019-07-05 01:03:55'),
(198, '51', '9', '40-HC', 1998, 'P', '2019-07-05 01:04:02', '2019-07-05 01:04:02'),
(199, '59', '3', '20-GP', 1400, 'P', '2019-07-05 01:04:24', '2019-07-05 01:04:24'),
(200, '59', '9', '20-GP', 999, 'P', '2019-07-05 01:04:29', '2019-07-05 01:04:29'),
(201, '59', '5', '20-GP', 295, 'P', '2019-07-05 01:04:44', '2019-07-05 01:04:44'),
(202, '59', '11', '20-GP', 50, 'P', '2019-07-05 01:04:50', '2019-07-05 01:04:50'),
(203, '59', '10', '20-GP', 148, 'P', '2019-07-05 01:04:59', '2019-07-05 01:04:59'),
(204, '59', '12', '20-GP', 150, 'P', '2019-07-05 01:05:06', '2019-07-05 01:05:06'),
(205, '59', '7', '1-SET', 175, 'P', '2019-07-05 01:05:13', '2019-07-05 01:05:13'),
(206, '59', '8', '1-SET', 30, 'P', '2019-07-05 01:05:51', '2019-07-05 01:05:51'),
(207, '50', '3', '20-GP', 550, 'P', '2019-07-05 01:06:13', '2019-07-05 01:06:13'),
(208, '50', '9', '20-GP', 999, 'P', '2019-07-05 01:06:18', '2019-07-05 01:06:18'),
(209, '50', '5', '20-GP', 295, 'P', '2019-07-05 01:06:26', '2019-07-05 01:06:26'),
(210, '63', '3', '20-GP', 550, 'P', '2019-07-05 01:07:07', '2019-07-05 01:07:07'),
(211, '63', '9', '20-GP', 999, 'P', '2019-07-05 01:07:12', '2019-07-05 01:07:12'),
(212, '64', '3', '40-HC', 875, 'P', '2019-07-05 01:10:29', '2019-07-05 01:10:29'),
(213, '64', '9', '40-HC', 1998, 'P', '2019-07-05 01:10:35', '2019-07-05 01:10:35'),
(214, '49', '3', '20-GP', 550, 'P', '2019-07-05 01:10:55', '2019-07-05 01:10:55'),
(215, '49', '9', '20-GP', 999, 'P', '2019-07-05 01:11:00', '2019-07-05 01:11:00'),
(216, '65', '3', '20-GP', 800, 'P', '2019-07-05 01:11:36', '2019-07-05 01:11:36'),
(217, '65', '9', '20-GP', 999, 'P', '2019-07-05 01:11:42', '2019-07-05 01:11:42'),
(218, '66', '3', '20-GP', 500, 'P', '2019-07-05 01:41:54', '2019-07-05 01:41:54'),
(219, '66', '9', '20-GP', 999, 'P', '2019-07-05 01:41:59', '2019-07-05 01:41:59'),
(220, '66', '5', '20-GP', 295, 'P', '2019-07-05 01:42:07', '2019-07-05 01:42:07'),
(221, '66', '6', '20-GP', 36.25, 'P', '2019-07-05 01:42:13', '2019-07-05 01:42:13'),
(222, '66', '7', '1-SET', 175, 'P', '2019-07-05 01:42:19', '2019-07-05 01:42:19'),
(223, '66', '8', '1-SET', 30, 'P', '2019-07-05 01:42:26', '2019-07-05 01:42:26'),
(224, '67', '3', '40-HC', 963, 'P', '2019-07-05 01:42:56', '2019-07-05 01:42:56'),
(225, '67', '9', '40-HC', 1998, 'P', '2019-07-05 01:43:02', '2019-07-05 01:43:02'),
(226, '67', '5', '40-HC', 440, 'P', '2019-07-05 01:43:07', '2019-07-05 01:43:07'),
(227, '67', '6', '40-HC', 72.5, 'P', '2019-07-05 01:43:12', '2019-07-05 01:43:12'),
(228, '67', '7', '1-SET', 175, 'P', '2019-07-05 01:43:18', '2019-07-05 01:43:18'),
(229, '67', '8', '1-SET', 30, 'P', '2019-07-05 01:43:25', '2019-07-05 01:43:25'),
(230, '69', '3', '20-GP', 1400, 'P', '2019-07-05 03:19:08', '2019-07-05 03:19:08'),
(231, '69', '9', '20-GP', 999, 'P', '2019-07-05 03:19:14', '2019-07-05 03:19:14'),
(232, '71', '3', '40-HRF', 5775, 'P', '2019-07-05 03:24:55', '2019-07-05 03:24:55'),
(234, '71', '9', '40-HRF', 1998, 'P', '2019-07-05 03:25:51', '2019-07-05 03:25:51'),
(235, '70', '3', '20-GP', 550, 'P', '2019-07-05 03:26:05', '2019-07-05 03:26:05'),
(236, '70', '9', '20-GP', 999, 'P', '2019-07-05 03:26:12', '2019-07-05 03:26:12'),
(237, '72', '3', '40-HC', 963, 'P', '2019-07-05 03:57:09', '2019-07-05 03:57:09'),
(238, '72', '9', '40-HC', 1998, 'P', '2019-07-05 03:57:15', '2019-07-05 03:57:15'),
(239, '73', '3', '20-GP', 550, 'P', '2019-07-05 03:57:31', '2019-07-05 03:57:31'),
(240, '73', '9', '20-GP', 999, 'P', '2019-07-05 03:57:36', '2019-07-05 03:57:36'),
(241, '97', '3', '20-GP', 550, 'P', '2019-07-05 07:42:35', '2019-07-05 07:42:35'),
(242, '97', '9', '20-GP', 999, 'P', '2019-07-05 07:42:40', '2019-07-05 07:42:40'),
(243, '96', '3', '40-HC', 963, 'P', '2019-07-05 07:42:59', '2019-07-05 07:42:59'),
(244, '96', '9', '40-HC', 1998, 'P', '2019-07-05 07:43:10', '2019-07-05 07:43:10'),
(245, '95', '3', '40-HC', 963, 'P', '2019-07-05 07:43:43', '2019-07-05 07:43:43'),
(246, '95', '9', '40-HC', 1998, 'P', '2019-07-05 07:43:49', '2019-07-05 07:43:49'),
(247, '98', '3', '40-HC', 963, 'P', '2019-07-05 08:25:36', '2019-07-05 08:25:36'),
(248, '98', '9', '40-HC', 1998, 'P', '2019-07-05 08:25:42', '2019-07-05 08:25:42'),
(249, '100', '3', '20-GP', 550, 'P', '2019-07-05 08:38:31', '2019-07-05 08:38:31'),
(250, '100', '9', '20-GP', 999, 'P', '2019-07-05 08:38:36', '2019-07-05 08:38:36'),
(251, '62', '3', '40-HC', 1050, 'C', '2019-07-05 08:41:28', '2019-07-05 08:41:28'),
(252, '62', '9', '40-HC', 1998, 'C', '2019-07-05 08:41:35', '2019-07-05 08:41:35'),
(293, '105', '3', '20-GP', 418, 'P', '2019-07-06 02:12:27', '2019-07-06 02:12:27'),
(255, '61', '3', '40-HC', 2500, 'C', '2019-07-05 08:42:29', '2019-07-05 08:42:29'),
(256, '61', '9', '40-HC', 1998, 'C', '2019-07-05 08:42:42', '2019-07-05 08:42:42'),
(257, '99', '3', '20-GP', 550, 'P', '2019-07-05 08:43:11', '2019-07-05 08:43:11'),
(258, '99', '9', '20-GP', 999, 'P', '2019-07-05 08:43:20', '2019-07-05 08:43:20'),
(259, '93', '3', '20-GP', 550, 'P', '2019-07-06 01:19:20', '2019-07-06 01:19:20'),
(260, '93', '9', '20-GP', 999, 'P', '2019-07-06 01:19:26', '2019-07-06 01:19:26'),
(261, '93', '5', '20-GP', 295, 'P', '2019-07-06 01:19:33', '2019-07-06 01:19:33'),
(262, '93', '6', '20-GP', 36.25, 'P', '2019-07-06 01:19:39', '2019-07-06 01:19:39'),
(263, '93', '7', '1-SET', 175, 'P', '2019-07-06 01:19:46', '2019-07-06 01:19:46'),
(264, '93', '8', '1-SET', 30, 'P', '2019-07-06 01:19:52', '2019-07-06 01:19:52'),
(265, '76', '3', '20-GP', 1000, 'C', '2019-07-06 01:40:22', '2019-07-06 01:40:22'),
(266, '76', '9', '20-GP', 999, 'C', '2019-07-06 01:40:29', '2019-07-06 01:40:29'),
(267, '101', '3', '40-HC', 1200, 'C', '2019-07-06 01:43:22', '2019-07-06 01:43:22'),
(268, '101', '9', '40-HC', 1998, 'C', '2019-07-06 01:43:30', '2019-07-06 01:43:30'),
(269, '89', '3', '40-HC', 2700, 'P', '2019-07-06 01:50:23', '2019-07-06 01:50:23'),
(270, '89', '9', '40-HC', 1998, 'P', '2019-07-06 01:50:28', '2019-07-06 01:50:28'),
(271, '75', '3', '40-HC', 1100, 'C', '2019-07-06 01:51:49', '2019-07-06 01:51:49'),
(272, '75', '9', '40-HC', 1998, 'C', '2019-07-06 01:51:56', '2019-07-06 01:51:56'),
(273, '74', '3', '20-GP', 550, 'P', '2019-07-06 01:54:02', '2019-07-06 01:54:02'),
(274, '74', '9', '20-GP', 999, 'P', '2019-07-06 01:54:09', '2019-07-06 01:54:09'),
(275, '74', '4', '20-GP', 295, 'P', '2019-07-06 01:54:16', '2019-07-06 01:54:16'),
(276, '74', '6', '20-GP', 36.25, 'P', '2019-07-06 01:54:21', '2019-07-06 01:54:21'),
(277, '74', '7', '1-SET', 175, 'P', '2019-07-06 01:54:31', '2019-07-06 01:54:31'),
(278, '74', '8', '1-SET', 30, 'P', '2019-07-06 01:54:39', '2019-07-06 01:54:39'),
(279, '87', '3', '20-GP', 1300, 'P', '2019-07-06 01:57:46', '2019-07-06 01:57:46'),
(280, '87', '9', '20-GP', 999, 'P', '2019-07-06 01:57:51', '2019-07-06 01:57:51'),
(281, '87', '5', '20-GP', 295, 'P', '2019-07-06 01:57:58', '2019-07-06 01:57:58'),
(282, '87', '11', '20-GP', 50, 'P', '2019-07-06 01:58:03', '2019-07-06 01:58:03'),
(283, '87', '10', '20-GP', 148, 'P', '2019-07-06 01:58:10', '2019-07-06 01:58:10'),
(284, '87', '12', '20-GP', 150, 'P', '2019-07-06 01:58:18', '2019-07-06 01:58:18'),
(285, '87', '7', '1-SET', 175, 'P', '2019-07-06 01:58:25', '2019-07-06 01:58:25'),
(286, '87', '8', '1-SET', 30, 'P', '2019-07-06 01:58:32', '2019-07-06 01:58:32'),
(287, '94', '3', '40-HC', 1050, 'C', '2019-07-06 02:02:03', '2019-07-06 02:02:03'),
(288, '94', '9', '40-HC', 1998, 'C', '2019-07-06 02:02:10', '2019-07-06 02:02:10'),
(289, '77', '3', '20-GP', 438.9, 'P', '2019-07-06 02:08:52', '2019-07-06 02:08:52'),
(290, '106', '3', '20-GP', 459.8, 'P', '2019-07-06 02:09:10', '2019-07-06 02:09:10'),
(291, '109', '3', '20-GP', 459.8, 'P', '2019-07-06 02:09:26', '2019-07-06 02:09:26'),
(303, '108', '3', '20-GP', 376.2, 'P', '2019-07-06 03:35:13', '2019-07-06 03:35:13'),
(294, '105', '3', '40-HC', 836, 'P', '2019-07-06 02:12:47', '2019-07-06 02:12:47'),
(295, '105', '3', '40-GP', 836, 'P', '2019-07-06 02:13:47', '2019-07-06 02:13:47'),
(296, '104', '3', '20-GP', 376.2, 'P', '2019-07-06 02:14:07', '2019-07-06 02:14:07'),
(297, '60', '3', '40-HC', 1050, 'C', '2019-07-06 02:16:24', '2019-07-06 02:16:24'),
(298, '60', '9', '40-HC', 1998, 'C', '2019-07-06 02:16:33', '2019-07-06 02:16:33'),
(301, '68', '3', '40-HC', 963, 'P', '2019-07-06 02:19:42', '2019-07-06 02:19:42'),
(302, '68', '9', '40-HC', 1998, 'P', '2019-07-06 02:19:48', '2019-07-06 02:19:48'),
(304, '64', '6', '40-HC', 72.5, 'P', '2019-07-09 05:37:36', '2019-07-09 05:37:36'),
(305, '64', '5', '40-HC', 440, 'P', '2019-07-09 05:37:46', '2019-07-09 05:37:46'),
(306, '64', '7', '1-SET', 175, 'P', '2019-07-09 05:37:53', '2019-07-09 05:37:53'),
(307, '64', '8', '1-SET', 30, 'P', '2019-07-09 05:38:00', '2019-07-09 05:38:00');

-- --------------------------------------------------------

--
-- Table structure for table `bill_ladings`
--

DROP TABLE IF EXISTS `bill_ladings`;
CREATE TABLE IF NOT EXISTS `bill_ladings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bc_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bl_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vessel_id` int(11) NOT NULL,
  `vessel_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voyage_id` int(11) NOT NULL,
  `pol_id` int(11) NOT NULL,
  `pol_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pod_id` int(11) NOT NULL,
  `pod_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpd_id` int(11) NOT NULL,
  `fpd_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipper_id` int(11) NOT NULL,
  `shipper_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipper_add_id` int(11) NOT NULL,
  `shipper_add` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consignee_id` int(11) NOT NULL,
  `consignee_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consignee_add_id` int(11) NOT NULL,
  `consignee_add` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notify_id` int(11) NOT NULL,
  `notify_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notify_add_id` int(11) NOT NULL,
  `notify_add` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bl_date` datetime NOT NULL,
  `freight_term` int(11) NOT NULL,
  `cont_ownership` int(11) DEFAULT NULL,
  `telex_status` int(11) NOT NULL,
  `telex_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_bl` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=137 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bill_ladings`
--

INSERT INTO `bill_ladings` (`id`, `bc_no`, `bl_no`, `vessel_id`, `vessel_type`, `voyage_id`, `pol_id`, `pol_name`, `pod_id`, `pod_name`, `fpd_id`, `fpd_name`, `shipper_id`, `shipper_name`, `shipper_add_id`, `shipper_add`, `consignee_id`, `consignee_name`, `consignee_add_id`, `consignee_add`, `notify_id`, `notify_name`, `notify_add_id`, `notify_add`, `bl_date`, `freight_term`, `cont_ownership`, `telex_status`, `telex_path`, `no_of_bl`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, 'test2100029', 1, 'LCL', 1, 1, 'TEST', 1, 'PENANG', 1, 'PENANG', 2, 'etest', 7, '123\r\n123', 3, 'edd', 8, '123\r\n123', 3, 'edd', 8, '123\r\n123', '2019-04-13 16:03:41', 0, NULL, 0, NULL, 3, '2019-04-08 08:05:59', '2019-04-05 09:43:19', '2019-04-08 08:05:59'),
(2, NULL, 'PGBKI0001', 3, 'FCL', 1, 1, 'PENANG', 3, 'KOTA KINABALU', 3, 'KOTA KINABALU', 4, 'SYARIKAT PERKAPALAN SOO HUP SENG SDN BHD', 9, '10300 PENANG MALAYSIA', 3, 'KINA ENTERPRISE (SABAH) SDN BHD', 8, '22, LOT B, JALAN NOUNTUN, 88450\r\nINNAM, KOTA KINABALU, SABAH.\r\nP.O.BOX 98, 89257 TAMPARULI, SABAH', 3, 'KINA ENTERPRISE (SABAH) SDN BHD', 8, 'SAME AS CONSIGNEE', '2019-04-10 14:18:15', 0, 1, 0, NULL, 3, '2019-06-04 06:22:03', '2019-04-08 08:07:07', '2019-06-04 06:22:03'),
(4, NULL, 'PGSBW0001', 4, 'FCL', 2, 1, 'PENANG', 5, 'SIBU', 5, 'SIBU', 4, 'SYARIKAT PERKAPALAN SOO HUP SENG SDN BHD', 9, '10300 PENANG MALAYSIA', 3, 'KINA ENTERPRISE (SABAH) SDN BHD', 8, '123\r\n123', 4, 'SYARIKAT PERKAPALAN SOO HUP SENG SDN BHD', 9, '10300 PENANG MALAYSIA', '2019-04-17 12:09:50', 0, 1, 0, NULL, 3, '2019-06-04 06:20:41', '2019-04-11 06:19:13', '2019-06-04 06:20:41'),
(3, NULL, 'PGBKI0002', 3, 'FCL', 1, 1, 'PENANG', 3, 'KOTA KINABALU', 3, 'KOTA KINABALU', 2, 'NAMYE FOOD INDUSTRIES SDN BHD', 7, '123\r\n123', 3, 'KINA ENTERPRISE (SABAH) SDN BHD', 8, '123\r\n123', 3, 'KINA ENTERPRISE (SABAH) SDN BHD', 8, '123\r\n123', '2019-04-13 14:17:47', 0, 1, 0, NULL, 3, '2019-06-04 06:22:03', '2019-04-11 06:17:47', '2019-06-04 06:22:03'),
(5, NULL, 'PGKCH0001', 6, 'FCL', 4, 1, 'PENANG', 2, 'KUCHING', 2, 'KUCHING', 5, 'SOON SOON OILMILLS SDN BHD', 12, 'PRAI INDUSTRIAL ESTATE, 13600\r\nPRAI PENANG, MALAYSIA', 6, 'SEBERANG DISTRIBUTORS SDN BHD', 14, 'A-10-2 YOSHI SQUARE, JLN PELABUHAN\r\n93450 KUCHING, SARAWAK', 6, 'SEBERANG DISTRIBUTORS SDN BHD', 14, 'A-10-2 YOSHI SQUARE, JLN PELABUHAN\r\n93450 KUCHING, SARAWAK', '2019-06-09 14:47:04', 0, 1, 0, NULL, 3, '2019-06-27 04:57:39', '2019-06-04 06:38:55', '2019-06-27 04:57:39'),
(6, NULL, 'PGKCH0002', 6, 'FCL', 4, 1, 'PENANG', 2, 'KUCHING', 2, 'KUCHING', 7, 'EP VENTURES SDN BHD', 15, 'MK 6, 14000 BUKIT TENGAH,\r\nBUKIT MERTAJAM, PENANG', 8, 'MATER-PACK (SARAWAK) SDN BHD', 17, 'KTLD BINTAWA INDUSTRIAL ESTATE,\r\nJALAN KELULI, 93450 KUCHING,\r\nSARAWAK, MALAYSIA', 8, 'MATER-PACK (SARAWAK) SDN BHD', 17, 'KTLD BINTAWA INDUSTRIAL ESTATE,\r\nJALAN KELULI, 93450 KUCHING,\r\nSARAWAK, MALAYSIA', '2019-06-09 08:35:20', 0, 1, 0, NULL, 3, '2019-06-27 04:57:39', '2019-06-04 07:06:12', '2019-06-27 04:57:39'),
(7, NULL, 'PGKCH0003', 6, 'FCL', 4, 1, 'PENANG', 2, 'KUCHING', 2, 'KUCHING', 7, 'PROFIT ACTION SDN BHD', 15, '6110, TINGKAT SELAMAT 9,\r\nKAMPUNG SELAMAT, 13300\r\nTASEK GELUGOR SEBERANG PERAI\r\nUTARA.', 8, 'WEY SING FURNITURE TRADING SDN BHD', 17, 'LOT 8034, BLOCK 59, MUARA LAND DISTRICT,KOTA SAMARAHAN INDUSTRIAL \r\nESTATE, KG.TANJUNG BUNDONG, \r\n94300 KOTA SAMARAHAN, KUCHING, SARAWAK.\r\nTEL:082-865913', 8, 'MATER-PACK (SARAWAK) SDN BHD', 17, 'KTLD BINTAWA INDUSTRIAL ESTATE,\r\nJALAN KELULI, 93450 KUCHING,\r\nSARAWAK, MALAYSIA', '2019-06-09 10:34:02', 1, 1, 0, NULL, 3, '2019-06-27 04:57:39', '2019-06-07 00:37:29', '2019-06-27 04:57:39'),
(8, NULL, 'PGSBW0002', 6, 'FCL', 4, 1, 'PENANG', 5, 'SIBU', 5, 'SIBU', 7, 'AS TENG TENG TRADING SDN BHD', 15, 'LOT 103, JALAN TIMUR 4,\r\nKAWASAN PERUAHAAN MERGONG 2-A,\r\n05150 ALOR SETAR, KEDAH, MALAYSIA', 8, 'PAN KHING TRADING CO', 17, '16B, BUNGA DAHLIA\r\n96000 SIBU, SARAWAK\r\nTEL:084-341416\r\nFAX:084-327816', 8, 'MATER-PACK (SARAWAK) SDN BHD', 17, '16B, BUNGA DAHLIA\r\n96000 SIBU, SARAWAK\r\nTEL:084-341416\r\nFAX:084-327816', '2019-06-09 10:47:08', 1, 1, 0, NULL, 3, '2019-06-27 04:57:39', '2019-06-07 06:35:28', '2019-06-27 04:57:39'),
(9, NULL, 'PGSBW0003', 6, 'FCL', 4, 1, 'PENANG', 2, 'KUCHING', 2, 'KUCHING', 7, 'MULTI-CHOICE (M) SDN BHD', 15, 'NO.987, JALAN PERUSAHAAN, KAWASAN\r\nPERUSAHAAN, 13600 PRAI, \r\nPULAU PINANG, MALAYSIA', 8, 'QL LIVESTOCK FARMING SDN BHD', 17, 'GROUND FLOOR & 1ST FLOOR, SUBLOT 2490,\r\nFORTUNE ROAD, ROCK ROAD,\r\n93200 KUCHING, SARAWAK\r\nTEL:082-252760/082-240068\r\nFAX:082-410646\r\nATTN: DR.FU', 8, 'MATER-PACK (SARAWAK) SDN BHD', 17, 'GROUND FLOOR & 1ST FLOOR, SUBLOT 2490,\r\nFORTUNE ROAD, ROCK ROAD,\r\n93200 KUCHING, SARAWAK\r\nTEL:082-252760/082-240068\r\nFAX:082-410646\r\nATTN: DR.FU', '2019-06-09 10:48:40', 0, 1, 0, NULL, 3, '2019-06-11 02:50:39', '2019-06-07 06:43:02', '2019-06-11 02:50:39'),
(10, NULL, 'PGKCH0002A', 6, 'FCL', 4, 1, 'PENANG', 2, 'KUCHING', 2, 'KUCHING', 7, 'EP VENTURES SDN BHD', 15, 'MK 6, 14000 BUKIT TENGAH,\r\nBUKIT MERTAJAM, PENANG', 8, 'MATER-PACK (SARAWAK) SDN BHD', 17, 'KTLD BINTAWA INDUSTRIAL ESTATE,\r\nJALAN KELULI, 93450 KUCHING,\r\nSARAWAK, MALAYSIA', 8, 'MATER-PACK (SARAWAK) SDN BHD', 17, 'KTLD BINTAWA INDUSTRIAL ESTATE,\r\nJALAN KELULI, 93450 KUCHING,\r\nSARAWAK, MALAYSIA', '2019-06-09 08:35:20', 0, 1, 0, NULL, 3, '2019-06-10 06:37:50', '2019-06-10 06:37:36', '2019-06-10 06:37:50'),
(11, NULL, 'PGKCH0003', 6, 'FCL', 4, 1, 'PENANG', 2, 'KUCHING', 2, 'KUCHING', 7, 'MULTI-CHOICE (M) SDN BHD', 15, 'NO.987, JALAN PERUSAHAAN, KAWASAN\r\nPERUSAHAAN, 13600 PRAI, \r\nPULAU PINANG, MALAYSIA', 8, 'QL LIVESTOCK FARMING SDN BHD', 17, 'GROUND FLOOR & 1ST FLOOR, SUBLOT 2490,\r\nFORTUNE ROAD, ROCK ROAD,\r\n93200 KUCHING, SARAWAK\r\nTEL:082-252760/082-240068\r\nFAX:082-410646\r\nATTN: DR.FU', 8, 'MATER-PACK (SARAWAK) SDN BHD', 17, 'GROUND FLOOR & 1ST FLOOR, SUBLOT 2490,\r\nFORTUNE ROAD, ROCK ROAD,\r\n93200 KUCHING, SARAWAK\r\nTEL:082-252760/082-240068\r\nFAX:082-410646\r\nATTN: DR.FU', '2019-06-09 10:50:01', 0, 1, 0, NULL, 3, '2019-06-11 02:51:16', '2019-06-11 02:50:01', '2019-06-11 02:51:16'),
(12, NULL, 'PGKCH0004', 6, 'FCL', 4, 1, 'PENANG', 2, 'KUCHING', 2, 'KUCHING', 7, 'PROFIT ACTION SDN BHD', 15, '6110, TINGKAT SELAMAT 9,\r\nKAMPUNG SELAMAT, 13300\r\nTASEK GELUGOR SEBERANG PERAI\r\nUTARA.', 8, 'WEY SING FURNITURE TRADING SDN BHD', 17, 'LOT 8034, BLOCK 59, MUARA LAND DISTRICT,KOTA SAMARAHAN INDUSTRIAL \r\nESTATE, KG.TANJUNG BUNDONG, \r\n94300 KOTA SAMARAHAN, KUCHING, SARAWAK.\r\nTEL:082-865913', 8, 'MATER-PACK (SARAWAK) SDN BHD', 17, 'KTLD BINTAWA INDUSTRIAL ESTATE,\r\nJALAN KELULI, 93450 KUCHING,\r\nSARAWAK, MALAYSIA', '2019-06-09 10:51:08', 0, 1, 0, NULL, 3, '2019-06-27 04:57:39', '2019-06-11 02:51:08', '2019-06-27 04:57:39'),
(13, NULL, 'PGMYY0001', 4, 'FCL', 7, 1, 'PENANG, MALAYSIA', 6, 'BINTULU SARAWAK', 6, 'BINTULU SARAWAK', 9, 'BARKATH STORES (PENANG) SDN BHD', 18, 'MINYAK 1, KAWASAN PERINDUSTRIAN BUKIT\r\nMINYAK, 14100, SIMPANG AMPAT, SPT, PENANG', 10, 'MOH HENG CO. SDN BHD', 19, 'KIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 19, 'KIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', '2019-07-04 14:18:09', 0, 1, 0, NULL, 3, '2019-06-27 06:23:51', '2019-06-27 05:02:46', '2019-06-27 06:23:51'),
(14, NULL, 'PGBTU0001', 4, 'FCL', 7, 1, 'PENANG, MALAYSIA', 6, 'BINTULU, SARAWAK', 6, 'BINTULU, SARAWAK', 9, 'BARKATH STORES (PENANG) SDN BHD', 23, '1207, LENGKOK PERINDUSTRIAN \r\nBUKIT MINYAK 1, KAWASAN PERINDUSTRIAN BUKIT MINYAK, \r\n14100, SIMPANG AMPAT, SPT, PENANG', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT KIDURONG LIGHT INDUSTRIAL ESTATE, 97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICTKIDURONG LIGHT INDUSTRIAL ESTATE,97000 BINTULU, SARAWAK, MALAYSIA.', '2019-07-04 16:43:52', 0, 1, 0, NULL, 3, NULL, '2019-06-27 06:19:58', '2019-07-02 08:43:52'),
(15, NULL, 'PGSBW0004', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 15, 'SOUTHERN CABLE SDN.BHD.', 29, 'LOT 42, JALAN MERBAU PULAS,\r\nKAWASAN PERUSAHAAN KUALA KETIL,\r\n09300 KUALA KETIL, BALING, KEDAH MALAYSIA', 14, 'SHOREFIELD SDN BHD', 28, 'LOT 1025, BLOCK 7, MTLD, LRG DAMAK LAUT 7A\r\nSEJINGKAT INDS PARK, 93050 KUCHING,SARAWAK, E,MALAYISIA.\r\nTEL:082-432375 FAX:082-433990', 14, 'SHOREFIELD SDN BHD', 28, 'LOT 1025, BLOCK 7, MTLD, LRG DAMAK LAUT 7A\r\nSEJINGKAT INDS PARK, 93050 KUCHING,SARAWAK, E,MALAYISIA.\r\nTEL:082-432375 FAX:082-433990', '2019-07-04 10:00:12', 0, 1, 0, NULL, 3, NULL, '2019-06-27 08:15:09', '2019-07-01 02:00:12'),
(16, NULL, 'PGBKI0003', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 7, 'TWENTY-TWENTY FOOD INDUSTRY SDN BHD', 30, 'NO. 1, JALAN KAMPUNG KEDUNDUNG,\r\nPOKOK SENA, MK JABI,06400 ALOR SETAR, KEDAH DARUL AMAN, WEST MALAYSIA, MALAYSIA..', 10, 'HUNG TAI ENTERPRISE (SABAH) SDN BHD', 22, 'LOT 53 LOK KAWI INDUSTRIAL ESTATE\r\nPH2, 89600 KINARUT, SABAH.\r\nTEL: 088-750557\r\nFAX: 088-751557', 8, 'HUNG TAI ENTERPRISE (SABAH) SDN BHD', 31, 'LOT 53 LOK KAWI INDUSTRIAL ESTATE\r\nPH2, 89600 KINARUT, SABAH.\r\nTEL: 088-750557\r\nFAX: 088-751557', '2019-07-04 15:13:34', 0, 1, 0, NULL, 3, NULL, '2019-07-01 01:35:12', '2019-07-02 07:13:34'),
(17, NULL, 'PGTWU0001', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 8, 'TAWAU SABAH', 8, 'TAWAU SABAH', 16, 'SILVERSTONE BERHAD', 34, 'LOT 5831, KAMUNTING INDUSTRIAL ESTATE II,\r\nP.O.BOX 2, 34600 KAMUNTING PERAK\r\nDARUL RIDZUAN, MALAYSIA', 22, 'ZHENG SENG TYRE ENTERPRISE', 42, 'TB 11756, NEW HUA DAT LIGHT INDUSTRIAL\r\nESTATE, BATU 2 1/2, JALAN APAS, 91000 TAWAU, SABAH\r\nATTN: MR.TAN TEL:089-754780', 18, 'SILVERSTONE MARKETING SDN BHD', 36, 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN\r\nJALAN MENGGATAL / JALAN TURAN\r\n88450 KOTA KINABALU, SABAH. TEL:088-490026', '2019-07-04 10:07:10', 0, 1, 0, NULL, 3, NULL, '2019-07-01 03:50:57', '2019-07-03 02:07:10'),
(18, NULL, 'PGSDK0001', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 7, 'SANDAKAN SABAH', 7, 'SANDAKAN SABAH', 16, 'SILVERSTONE BERHAD', 34, 'LOT 5831, KAMUNTING INDUSTRIAL ESTATE II,\r\nP.O.BOX 2, 34600 KAMUNTING PERAK\r\nDARUL RIDZUAN, MALAYSIA', 21, 'SABAH TIRE SERVICE CENTRE', 39, 'C/O CHIA HANDLING & FORWARDING SERVICES SDN BHD\r\nLOT 116, TAMAN PERTAMA PHASE , PO BOX NO.1948\r\n90722 SANDAKAN, SABAH', 18, 'SILVERSTONE MARKETING SDN BHD', 36, 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN\r\nJALAN MENGGATAL / JALAN TURAN\r\n88450 KOTA KINABALU, SABAH. TEL:088-490026', '2019-07-04 10:04:56', 0, 1, 0, NULL, 3, NULL, '2019-07-01 04:04:21', '2019-07-03 02:04:56'),
(19, NULL, 'PGSDK0002', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 7, 'SANDAKAN SABAH', 7, 'SANDAKAN SABAH', 16, 'SILVERSTONE BERHAD', 34, 'LOT 5831, KAMUNTING INDUSTRIAL ESTATE II,\r\nP.O.BOX 2, 34600 KAMUNTING PERAK\r\nDARUL RIDZUAN, MALAYSIA', 20, 'PENG TAH ENTERPRISE SDN BHD', 38, 'C/O CHIA HANDLING & FORWARDING SERVICES SDN BHD\r\nLOT 116, TAMAN PERTAMA PHASE P.O.BOX NO.1948\r\n90722 SANDAKAN, SABAH', 18, 'SILVERSTONE MARKETING SDN BHD', 36, 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN\r\nJALAN MENGGATAL / JALAN TURAN\r\n88450 KOTA KINABALU, SABAH. TEL:088-490026', '2019-07-04 10:06:14', 0, 1, 0, NULL, 3, NULL, '2019-07-01 04:14:04', '2019-07-03 02:06:14'),
(20, NULL, 'PGBKI0004', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 16, 'SILVERSTONE BERHAD', 34, 'LOT 5831, KAMUNTING INDUSTRIAL ESTATE II,\r\nP.O.BOX 2, 34600 KAMUNTING PERAK\r\nDARUL RIDZUAN, MALAYSIA', 19, 'NSA TRADING SDN BHD (612553-P)', 37, 'BATU 11, NT04316962, KG PANJUT,\r\n89208 TUARAN, SABAH', 18, 'SILVERSTONE MARKETING SDN BHD', 36, 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN\r\nJALAN MENGGATAL / JALAN TURAN\r\n88450 KOTA KINABALU, SABAH. TEL:088-490026', '2019-07-03 12:23:37', 0, 1, 0, NULL, 3, NULL, '2019-07-01 04:23:37', '2019-07-01 04:23:37'),
(21, NULL, 'PGBKI0005', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 16, 'SILVERSTONE BERHAD', 34, 'LOT 5831, KAMUNTING INDUSTRIAL ESTATE II,P.O.BOX 2, 34600 KAMUNTING PERAK DARUL RIDZUAN, MALAYSIA', 19, 'NSA TRADING SDN BHD (612553-P)', 37, 'BATU 11, NT04316962, KG PANJUT,\r\n89208 TUARAN, SABAH', 18, 'SILVERSTONE MARKETING SDN BHD', 36, 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN\r\nJALAN MENGGATAL / JALAN TURAN\r\n88450 KOTA KINABALU, SABAH. \r\nTEL:088-490026', '2019-07-03 13:01:44', 0, 1, 0, NULL, 3, NULL, '2019-07-01 04:36:55', '2019-07-01 05:01:44'),
(22, NULL, 'PGBKI0006', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 16, 'SILVERSTONE BERHAD', 34, 'LOT 5831, KAMUNTING INDUSTRIAL ESTATE II,\r\nP.O.BOX 2, 34600 KAMUNTING PERAK\r\nDARUL RIDZUAN, MALAYSIA', 17, 'KNY TYRES SDN BHD', 47, 'MILE 7 1/2, JALAN TUARAN\r\n88400, KOTA KINABALU, SABAH', 18, 'SILVERSTONE MARKETING SDN BHD', 36, 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN\r\nJALAN MENGGATAL / JALAN TURAN\r\n88450 KOTA KINABALU, SABAH. TEL:088-490026', '2019-07-04 09:59:01', 0, 1, 0, NULL, 3, NULL, '2019-07-01 06:40:28', '2019-07-03 01:59:01'),
(23, NULL, 'PGKCH0005', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 7, 'MSM PERLIS SDN BHD', 30, '(FORMERLY KNOWN AS KILANG GULA\r\nFELDA PERLIS SDN BHD)\r\nP.O. BOX 42, 01700 KANGAR\r\nPERLIS MALAYSIA', 8, 'J.O.E. SUPPLIER SDN BHD', 31, 'NO. 58 KOTA SENTOSA\r\nJALAN PENRISSEN\r\n93250 KUCHING, SARAWAK.', 8, 'J.O.E. SUPPLIER SDN BHD', 31, 'NO. 58 KOTA SENTOSA\r\nJALAN PENRISSEN\r\n93250 KUCHING, SARAWAK.', '2019-07-04 11:50:55', 0, 1, 0, NULL, 3, NULL, '2019-07-01 06:47:21', '2019-07-02 03:50:55'),
(30, NULL, 'PGSBW0006', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 26, 'REX TRADING SDN.BHD.', 45, 'PLOT 126, JALAN PERINDUSTRIAN BUKIT MINYAK 5,\r\n14100 SIMPANG AMPAT, SPT,\r\nPENANG, MALAYSIA', 13, 'MINSIANG TRADING SDN BHD', 27, '21 & 23, LORONG DING LIK KONG 6,\r\n96000 SIBU, SARAWAK.\r\nTEL: 084-334682', 13, 'MINSIANG TRADING SDN BHD', 27, '21 & 23, LORONG DING LIK KONG 6,\r\n96000 SIBU, SARAWAK.\r\nTEL: 084-334682', '2019-07-03 16:34:01', 0, 1, 0, NULL, 3, NULL, '2019-07-01 08:29:41', '2019-07-01 08:34:01'),
(24, NULL, 'PGBKI0007', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 16, 'SILVERSTONE BERHAD', 34, 'LOT 5831, KAMUNTING INDUSTRIAL ESTATE II,\r\nP.O.BOX 2, 34600 KAMUNTING PERAK\r\nDARUL RIDZUAN, MALAYSIA', 24, 'SYN JIN TYRES & BATTERIES SDN BHD', 43, 'LOT 25, HAI OU LIGHT INDUSTRIES\r\nLORONG TARAP, JALAN KOLOMBONG\r\n88450 KOTA KINABALU, SABAH', 18, 'SILVERSTONE MARKETING SDN BHD', 36, 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN\r\nJALAN MENGGATAL / JALAN TURAN\r\n88450 KOTA KINABALU, SABAH. TEL:088-490026', '2019-07-04 10:00:05', 0, 1, 0, NULL, 3, NULL, '2019-07-01 06:48:13', '2019-07-03 02:00:05'),
(25, NULL, 'PGBKI0008', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 16, 'SILVERSTONE BERHAD', 34, 'LOT 5831, KAMUNTING INDUSTRIAL ESTATE II,\r\nP.O.BOX 2, 34600 KAMUNTING PERAK\r\nDARUL RIDZUAN, MALAYSIA', 25, 'VINFIELD ENTERPRISE SDN BHD', 44, 'NT 213160993, MILE 5 1/2, KG.MAGORIBUNG\r\nPENAMPANG, 88816 KOTA KINABALU, SABAH\r\nTEL:088-728989', 18, 'SILVERSTONE MARKETING SDN BHD', 36, 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN\r\nJALAN MENGGATAL / JALAN TURAN\r\n88450 KOTA KINABALU, SABAH. TEL:088-490026', '2019-07-04 10:01:06', 0, 1, 0, NULL, 3, NULL, '2019-07-01 06:54:16', '2019-07-03 02:01:06'),
(26, NULL, 'PGSBW0005', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 10, 'FAIRTRADE COMMODITIES SDN BHD', 22, 'NO. 322A, JALAN SLIM, \r\nOFF JALAN PERAK JELUTONG\r\n11600 PENANG.', 10, 'NGU BROTHERS TRADING CO', 22, 'LOT 263, BLOCK 106,\r\nSARIKEI LAND DISTRICT,\r\nJALAN BAZAAR, 96100 SARIKEI,\r\nSARAWAK.', 10, 'NGU BROTHERS TRADING CO', 22, 'LOT 263, BLOCK 106,\r\nSARIKEI LAND DISTRICT,\r\nJALAN BAZAAR, 96100 SARIKEI,\r\nSARAWAK.', '2019-07-03 15:14:01', 0, 1, 0, NULL, 3, NULL, '2019-07-01 07:14:01', '2019-07-01 07:14:01'),
(27, NULL, 'PGBKI0009', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 10, 'NAMYE FOOD INDUSTRIES SDN BHD', 22, 'PT, 760 JALAN PERUSAHAAN SATU\r\nKAWASAN PERUSAHAAN PARIT BUNTAR\r\n34200 PARIT BUNTAR PERAK', 10, 'KIM GUAN HAP KEE SDN BHD', 22, '12, LORONG MINYAK, OFF 5.5. MILES\r\nJALAN TUARAN, NOUNTUN BAHARU,\r\n88450 INANA, KOTA KINABALU, SABAH.', 10, 'KIM GUAN HAP KEE SDN BHD', 22, '12, LORONG MINYAK, OFF 5.5. MILES\r\nJALAN TUARAN, NOUNTUN BAHARU,\r\n88450 INANA, KOTA KINABALU, SABAH.', '2019-07-03 15:25:22', 0, 1, 0, NULL, 3, NULL, '2019-07-01 07:25:22', '2019-07-01 07:25:22'),
(28, NULL, 'PGSDK0003', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 7, 'SANDAKAN SABAH', 7, 'SANDAKAN SABAH', 10, 'NT CENTURY FURNITURE SDN BHDC', 22, 'C/O TSH CENTURY TRADING\r\nNO. 3046, JALAN BUKIT CHANGKAT,\r\n14300 NIBONG TEBAL,S.P.S. PULAU PINANG.', 10, 'MERWOOD PROGRESS SDN.BHD.', 22, 'LOT 16, HOCK SENG INDUSTRIAL\r\nMILE 3 1/2 JALAN BOMBA\r\nSANDAKAN, SABAH.', 10, 'MERWOOD PROGRESS SDN.BHD.', 22, 'LOT 16, HOCK SENG INDUSTRIAL\r\nMILE 3 1/2 JALAN BOMBA\r\nSANDAKAN, SABAH.', '2019-07-03 15:21:34', 0, 1, 0, NULL, 3, NULL, '2019-07-01 07:34:37', '2019-07-02 07:21:34'),
(29, NULL, 'PGBKI0010', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 26, 'REX TRADING SDN.BHD.', 45, 'PLOT 126, JALAN PERINDUSTRIAN BUKIT MINYAK 5,\r\n14100 SIMPANG AMPAT, SPT,\r\nPENANG, MALAYSIA', 27, 'TRANSBORNEO MARKETING SDN BHD', 46, 'LOT 13A, LORONG 2A KKIP TIMUR, JALAN\r\n2 KKIP TIMUR, INDUSTRIAL ZONE 7, PHASE 1(IZ7PH1), KKIP TIMUR,\r\n88460 KOTA KINABALU, SABAH', 27, 'TRANSBORNEO MARKETING SDN BHD', 46, 'LOT 13A, LORONG 2A KKIP TIMUR, JALAN\r\n2 KKIP TIMUR, INDUSTRIAL ZONE 7, PHASE 1(IZ7PH1), KKIP TIMUR,\r\n88460 KOTA KINABALU, SABAH', '2019-07-04 10:27:25', 0, 1, 0, NULL, 3, NULL, '2019-07-01 07:42:52', '2019-07-03 02:27:25'),
(31, NULL, 'PGSBW0007', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 28, 'SEASON CASKET INDUSTRY SDN BHD', 48, 'NO.5, TAMAN BARU, 08000 SUNGAI PETANI, KEDAH', 29, 'FOO SOU CASKET SDN BHD', 49, 'NO.13, HII KAH TUNG ROAD,\r\nP.O.BOX 1482,\r\n96008 SIBU SARAWAK', 29, 'FOO SOU CASKET SDN BHD', 49, 'NO.13, HII KAH TUNG ROAD,\r\nP.O.BOX 1482,\r\n96008 SIBU SARAWAK', '2019-07-04 16:34:41', 0, 1, 0, NULL, 3, NULL, '2019-07-01 08:34:41', '2019-07-01 08:34:41'),
(32, NULL, 'PGBTU0002', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 6, 'BINTULU SARAWAK', 6, 'BINTULU SARAWAK', 30, 'NIBONG TEBAL ENTERPRISE SDN BHD', 50, 'LOT 7278, JALAN PERUSAHAAN 3, KAWASAN\r\nPERINDUSTRIAN PARIT BUNTAR,\r\n34200 PARIT BUNTAR, PERAK', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028 & 1029, BLOCK 26, \r\nKUDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028 & 1029, BLOCK 26, \r\nKUDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK', '2019-07-04 10:09:16', 0, 1, 0, NULL, 3, NULL, '2019-07-01 08:42:27', '2019-07-03 02:09:16'),
(33, NULL, 'PGBKI0011', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 10, 'FAIRTRADE COMMODITIES SDN BHD', 22, 'NO.322A, JALAN SLIM, OFF JALAN PERAK JELUTONG, 11600 PENANG.', 10, 'HOCK  MIN COFFEE FACTORY SDN.BHD.', 22, 'LOT 27, JALAN 1G KKIP, SELANTAN INDUSTRIAL ZONE 4, 88460 KOTA KINABALU, SABAH P.O. BOX 15820,\r\n88867 KOTA KINABALU', 10, 'FEI YANG LOGISTICS SDN BHD', 22, 'LOT 35, 2ND FLOOR, BLOCK E,\r\nRUANG SINGGAH MATA 3, ASIA CITY\r\n88000 KOTA KINABALU, SABAH.\r\nTEL: 088 255 215', '2019-07-04 15:08:17', 0, 1, 0, NULL, 3, NULL, '2019-07-02 01:29:32', '2019-07-02 07:08:17'),
(34, NULL, 'PGSDK0004', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 7, 'SANDAKAN SABAH', 7, 'SANDAKAN SABAH', 10, 'FAIRTRADE COMMODITIES SDN BHD', 22, 'NO. 322A, JALAN SLIM ,\r\nOFF JALAN PERAK JELUTONG\r\n11600 PENANG', 10, 'MAKNA MAJUBINA SDN BHD', 22, 'CL075321872, JLN HIEW NGEE FATT,\r\nMILE 9, JLN LABUK,\r\n90000 SANDAKAN, SABAH.', 10, 'MAKNA MAJUBINA SDN BHD', 22, 'CL075321872, JLN HIEW NGEE FATT,\r\nMILE 9, JLN LABUK,\r\n90000 SANDAKAN, SABAH.', '2019-07-04 09:51:29', 0, 1, 0, NULL, 3, NULL, '2019-07-02 01:44:25', '2019-07-02 01:51:29'),
(35, NULL, 'PGSBW0008', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 10, 'NIBONG TEBAL ENTERPRISE SDN BHD', 22, 'LOT 7278, JALAN PERUSAHAAN 3,\r\nKAWASAN PERINDUSTRIAN \r\nPARIT BUNTAR.\r\n34200 PARIT BUNTAR, PERAK.', 10, 'MOH HENG CO. SDN BHD', 22, 'NO. 1, LORONG LANANG BARAT 7,\r\n96000 SIBU, SARAWAK.\r\nTEL NO: 019-8228917  (MR. LEE)', 10, 'MOH HENG CO. SDN BHD', 22, 'NO. 1, LORONG LANANG BARAT 7,\r\n96000 SIBU, SARAWAK.\r\nTEL NO: 019-8228917  (MR. LEE)', '2019-07-04 13:05:55', 0, 1, 0, NULL, 3, NULL, '2019-07-02 01:59:30', '2019-07-03 05:05:55'),
(36, NULL, 'PGSDK0005', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 7, 'SANDAKAN SABAH', 7, 'SANDAKAN SABAH', 10, 'EVERTHROUGH RUBBER PRODUCTS SDN. BHD.', 22, '(COMPANY REGISTRATION NO: \r\n167407-A) LOT 3, KAMUNTING INDUSTRIAL ESTATE P.O. BOX 10.34600 KAMUNTING, TAIPING, PERAK, MALAYSIA.', 10, 'CENTRAL UNION ENTERPRISES SDN BHD', 22, 'LOT S23, SIBUGA INDUSTRIAL CENTRE,\r\nJALAN LINTAS SIBUGA, MILE 8,\r\nLABUK ROAD, \r\n90000 SANDAKAN, SABAH.', 10, 'CENTRAL UNION ENTERPRISES SDN BHD', 22, 'LOT S23, SIBUGA INDUSTRIAL CENTRE,\r\nJALAN LINTAS SIBUGA, MILE 8,\r\nLABUK ROAD, \r\n90000 SANDAKAN, SABAH.', '2019-07-04 11:28:31', 0, 1, 0, NULL, 3, NULL, '2019-07-02 02:23:04', '2019-07-03 03:28:31'),
(37, NULL, 'PGBKI0012', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 31, 'JATI HOME FURNITURE MARKETING SDN BHD', 51, 'LOT A82(44), JALAN 1B/3, KAWASAN PERUSAHAAN\r\nMIEL SUNGAI LALANG, 08000 SUNGAI PETANI, KEDAH', 32, 'HOMEMART DONGGONGON SDN. BHD.', 52, 'DONGGONGON JALAN PENAMPANG\r\nTAMBUNAN, 89500 PENAMPANG, \r\nKOTA KINABALU, SABAH\r\nTEL:088-717223   FAX: 088-718164', 32, 'HOMEMART DONGGONGON SDN. BHD.', 52, 'DONGGONGON JALAN PENAMPANG\r\nTAMBUNAN, 89500 PENAMPANG, \r\nKOTA KINABALU, SABAH.\r\nTEL:088-717223   FAX: 088-718164', '2019-07-04 15:45:58', 1, 1, 0, NULL, 3, NULL, '2019-07-02 02:50:54', '2019-07-02 07:45:58'),
(38, NULL, 'PGMYY0002', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 9, 'MIRI SARAWAK', 9, 'MIRI SARAWAK', 11, 'PROFIT ACTION SDN.BHD.', 25, '6110, TINGKAT SELAMAT 9,\r\nKAMPUNG SELAMAT,\r\n13300 TASEK GELUGOR SEBERANG PERAI UTARA', 8, 'WEY SING FURNITURE SDN. BHD.', 31, 'LOT 1930-1933, BLOCK 5, K.B.L.D.\r\nJALAN MAIGOLD, SENADIN INDUSTRIAL\r\nAREA, 98000 MIRI, SARAWAK.', 8, 'WEY SING FURNITURE SDN. BHD.', 31, 'LOT 1930-1933, BLOCK 5, K.B.L.D.\r\nJALAN MAIGOLD, SENADIN INDUSTRIAL\r\nAREA, 98000 MIRI, SARAWAK.', '2019-07-04 15:28:31', 1, 1, 0, NULL, 3, NULL, '2019-07-02 03:02:46', '2019-07-04 07:28:31'),
(39, NULL, 'PGPKG0001', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 37, 'HYUNDAI MERCHANT MARINE (MALAYSIA) SDN BHD', 57, 'UNIT 1.3A1, 1ST FLOOR, WISMA LEADER\r\nNO.8 JALAN LARUT, 10050, PENANG, MALAYSIA', 38, 'HYUNDAI MERCHANT MARINE (M) SDN BHD', 58, '11TH FLOOR, EAST WING, WISMA COSPLANT 2, NO.7, JALAN SS16/1, 47500 SUBANG JAYA, SELANGPR DARUL\r\nEHSAN', 38, 'HYUNDAI MERCHANT MARINE (M) SDN BHD', 58, '11TH FLOOR, EAST WING, WISMA COSPLANT 2, NO.7, JALAN SS16/1, 47500 SUBANG JAYA, SELANGPR DARUL\r\nEHSAN', '2019-07-04 15:35:26', 0, 1, 0, NULL, 3, NULL, '2019-07-02 03:03:47', '2019-07-02 07:35:26'),
(40, NULL, 'PGMYY0003', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 9, 'MIRI SARAWAK', 9, 'MIRI SARAWAK', 33, 'GOLD LEAF MACNUFACTURING SDN BHD', 53, '1203 PERMATANG BERAH\r\nTITI MUKIM, TELOK AIR TAWAR\r\n13050 BUTTERWORTH, PENANG, WEST MALAYSIA', 34, 'GOLD LEAF MANUFACTURING SDN.BHD', 54, '(MIRI BRANCH) LOT 939, SHOPHOUSE CENTRE,\r\nSUNGAI DARAM, TMN.TUNKU, BLOCK 5 RANCAR\r\nLAND DISTRICT, 98000 MIRI', 34, 'GOLD LEAF MANUFACTURING SDN.BHD', 54, '(MIRI BRANCH) LOT 939, SHOPHOUSE CENTRE,\r\nSUNGAI DARAM, TMN.TUNKU, BLOCK 5 RANCAR\r\nLAND DISTRICT, 98000 MIRI', '2019-07-04 14:40:24', 0, 1, 0, NULL, 3, NULL, '2019-07-02 03:49:41', '2019-07-02 06:40:24'),
(41, NULL, 'PGBKI0013', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 10, 'WONG AGRICULTURAL ENGINEERING SDN.BHD.', 22, 'BATU 13 1/4, JALAN KODIANG, \r\n06000 JITRA, KEDAH DARUL AMAN.', 10, 'GUAN ON RICE MILL SDN BHD (54235-P)', 22, 'B6,TAMAN CENTURY OLD, PENAMPANG ROAD, 88300 KOTA KINABALU, SABAH.\r\nH/P:016-8331972', 10, 'GUAN ON RICE MILL SDN BHD (54235-P)', 22, 'B6-TAMAN CENTURY OLD, PENAMPANG ROAD, 88300 KOTA KINABALU, SABAH.\r\nH/P:016-8331972', '2019-07-04 16:35:23', 0, 1, 0, NULL, 3, NULL, '2019-07-02 03:58:35', '2019-07-02 08:35:23'),
(42, NULL, 'PGBKI0014', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 10, 'NIBONG TEBAL PAPER MILL SDN BHD.', 22, '886, JALAN BANDAR BARU,\r\nSUNGAI KECIL,\r\n14300 NIBONG TEBAL, P.W., PENANG,\r\nMALAYSIA.', 10, 'NIBONG TEBAL ENTERPRISE SDN BHD', 22, 'LOT 3A, INDUSTRIAL ZONE 7 (1Z7)\r\nKKIP TIMUR JALAN SEPANGGAR\r\n88460 KOTA KINABALU, SABAH.', 10, 'NIBONG TEBAL ENTERPRISE SDN BHD', 22, 'LOT 3A, INDUSTRIAL ZONE 7 (1Z7)\r\nKKIP TIMUR JALAN SEPANGGAR\r\n88460 KOTA KINABALU, SABAH.', '2019-07-04 12:01:56', 0, 1, 0, NULL, 3, NULL, '2019-07-02 04:01:56', '2019-07-02 04:01:56'),
(43, NULL, 'PGSBW0009', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 35, 'NANYANG PHOSPHORUS FERTILIZERS SDN BHD', 55, 'NO.176, 1ST FLOOR, JALAN DATO BANDAR TUNGGAL,\r\n70000 SEREMBAN, NEGERI SEMBILAN, MALAYISA', 36, 'NANYANG PHOSPHORUS FERTILIZERS SDN. BHD', 56, 'LOT 1630, LORONG RENTAP 8,\r\nSARIKEI LIGHT INDUSTRIAL ESTATE, OFF\r\nJALAN RENTAP, SARIKEI, 96100 SARAWAK', 36, 'NANYANG PHOSPHORUS FERTILIZERS SDN. BHD', 56, 'LOT 1630, LORONG RENTAP 8,\r\nSARIKEI LIGHT INDUSTRIAL ESTATE, OFF\r\nJALAN RENTAP, SARIKEI, 96100 SARAWAK', '2019-07-04 16:30:41', 0, 1, 0, NULL, 3, NULL, '2019-07-02 04:06:49', '2019-07-02 08:30:41'),
(44, NULL, 'PGBKI0015', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 10, 'NIBONG TEBAL PAPER MILL SDN BHD.', 22, '886, JALAN BANDAR BARU,\r\nSUNGAI KECIL,\r\n14300 NIBONG TEBAL, S.P.S.\r\nPENANG, MALAYSIA.', 10, 'NIBONG TEBAL ENTERPRISE SDN BHD', 22, 'LOT 3A, INDUSTRIAL; ZONE 7 (IZ7)\r\nKKIP TIMUR JALAN SEPANGGAR\r\n88460 KOTA KINABALU, SABAH', 10, 'NIBONG TEBAL ENTERPRISE SDN BHD', 22, 'LOT 3A, INDUSTRIAL; ZONE 7 (IZ7)\r\nKKIP TIMUR JALAN SEPANGGAR\r\n88460 KOTA KINABALU, SABAH', '2019-07-04 11:22:17', 0, 1, 0, NULL, 3, NULL, '2019-07-02 04:11:07', '2019-07-03 03:22:17'),
(45, NULL, 'PGPKG0002', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 39, 'INTERASIA LINES (M) SDN BHD', 59, '55-18-C, MENARA NORTHAM, JALAN\r\nSULTAN SHAH, 10500 PENANG', 40, 'INTERASIA LINES (M) SDN BHD.', 60, 'SUITE 7.01, LEVEL 7, IMS 2, 88, JALAN BATAI LAUT 4, 41300 KLANG, SELANGOR', 40, 'INTERASIA LINES (M) SDN BHD.', 60, 'SUITE 7.01, LEVEL 7, IMS 2, 88, JALAN BATAI LAUT 4, 41300 KLANG, SELANGOR', '2019-07-04 16:24:01', 0, 1, 0, NULL, 3, NULL, '2019-07-02 07:32:39', '2019-07-02 08:24:01'),
(46, NULL, 'PGSBW0010', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 10, 'NAM SENG FOOD INDUSTRIES SDN BHD', 22, 'NO. 136, JALAN ARUMUGAM PILLAI,\r\n14000 BUKIT MERTAJAM,PENANG,\r\nMALAYSIA.', 10, 'CHIONG KEE TRADING SDN BHD', 22, 'NO. 13, LORONG DING LIK KWONG 8,\r\nSUNGAI MERAH, 96000 SIBU,\r\nSARAWAK, MALAYSIA.', 10, 'OCEAN FORWARDING AGENCY', 22, 'NO. 5, 2ND FLOOR, KHOO PENG LOONG ROAD, 96000 SIBU, SARAWAK.\r\nTEL: 084-321732  FAX: 084-320362', '2019-07-04 15:12:03', 0, 1, 0, NULL, 3, NULL, '2019-07-02 07:57:20', '2019-07-03 07:12:03'),
(47, NULL, 'PGBKI0016', 4, 'FCL', 7, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 10, 'NAM SENG FOOD INDUSTRIES SDN BHD', 22, 'NO. 136, JALAN ARUMUGAM PILLAI,\r\n14000 BUKIT MERTAJAM, PENANG,\r\nMALAYSIA.', 10, 'MYXO CO SDN BHD', 22, 'LOT 20, LORONG DURIAN 3,\r\nKIAN YAP INDUSTRIES ESTATE,\r\nKOLOMBONG, 88882 KOTA KINABALU,\r\nSABAH.', 10, 'MYXO CO SDN BHD', 22, 'LOT 20, LORONG DURIAN 3,\r\nKIAN YAP INDUSTRIES ESTATE,\r\nKOLOMBONG, 88882 KOTA KINABALU,\r\nSABAH.', '2019-07-04 10:23:46', 1, 1, 0, NULL, 3, NULL, '2019-07-02 08:04:38', '2019-07-03 02:23:46'),
(48, NULL, 'PGBKI0017', 4, 'FCL', 7, 1, 'PENANG, MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 10, 'PROFIT ACTION SDN BHD', 22, '6110, TINGKAT SELAMAT 9,\r\nKAMPUNG SELAMAT, 13300 TASEK\r\nGELUGOR, SEBERANG PERAI UTARA.', 10, 'CHAN FURNITURE (MALAYSIA) SDN BHD', 22, 'NO. 26, KARAMUNSING WAREHOUSE,\r\n88000  KOTA KINABALU,\r\nSABAH.\r\nTEL: 088-317866  FAX: 088-250326', 10, 'CHAN FURNITURE (MALAYSIA) SDN BHD', 22, 'NO. 26, KARAMUNSING WAREHOUSE,\r\n88000  KOTA KINABALU,\r\nSABAH.\r\nTEL: 088-317866  FAX: 088-250326', '2019-07-04 16:40:44', 0, 1, 0, NULL, 3, NULL, '2019-07-02 08:13:12', '2019-07-03 08:40:44'),
(49, NULL, 'PGKCH0006', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 47, 'PERAGA POTENSI TRADING SDN.BHD.', 67, 'NO. 275 & 277, JLN KIP 3,\r\nKAWASAN PERINDUSTRIAN KEPONG,\r\n52100 KUALA LUMPUR\r\nTEL: 03-6276 9389', 48, 'SAN SWEE FATT  ENTERPRISE SDN BHD', 68, '(WHOLESALE DEPARTMENT)\r\nLOT 820, BLOCK 24, JALAN MUARA TUANG,  94300 KUCHING, SARAWAK.  \r\nTEL: +082-627- 626', 48, 'SAN SWEE FATT  ENTERPRISE SDN BHD', 68, '(WHOLESALE DEPARTMENT)\r\nLOT 820, BLOCK 24, JALAN MUARA TUANG,  94300 KUCHING, SARAWAK.  \r\nTEL: +082-627- 626', '2019-07-07 16:44:58', 0, 1, 0, NULL, 3, NULL, '2019-07-03 06:33:14', '2019-07-04 08:44:58'),
(50, NULL, 'PGKCH0007', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 45, 'TAIKO ALUMINA SDN BHD', 65, 'LOT 169, JALAN TALI AIR, SIMPANG LIMA,\r\nPARIT BUNTAR INDUSTRIAL ESTATE\r\n34200 PARIT BUNTAR, PERAK', 46, 'CMS INFRA TRADING SDN BHD', 66, 'C/O SYARIKAT CHAN LIEN HOE SDN BHD (92370-A)\r\nLOT 1333, JALAN PERDANA, PENDING INDUSTRIAL ESTATE,\r\n93450 KUCHING, SARAWAK', 46, 'CMS INFRA TRADING SDN BHD', 66, 'C/O SYARIKAT CHAN LIEN HOE SDN BHD (92370-A)\r\nLOT 1333, JALAN PERDANA, PENDING INDUSTRIAL ESTATE,\r\n93450 KUCHING, SARAWAK', '2019-07-07 16:15:47', 0, 1, 0, NULL, 3, NULL, '2019-07-03 06:45:07', '2019-07-05 08:15:47'),
(51, NULL, 'PGSBW0011', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 43, 'MULTI CHOICE (M) SDN BHD', 63, 'NO. 987, JALAN PERUSAHAAN, KAWASAN PERUSAHAAN,\r\n13600 PRAI, PULAU PINANG, MALAYSIA.', 44, 'SENG HUONG FARM', 64, '633, SUBLOT 22, NO. 3, 1ST FLOOR, LORONG SUNGAI MERAH 2D2,\r\n96000 SIBU, SARAWAK, EAST MALAYSIA.\r\nTEL: 084-212 263/016-870 9068  \r\nFAX: 084-211 431', 44, 'SENG HUONG FARM', 64, '633, SUBLOT 22, NO. 3, 1ST FLOOR, LORONG SUNGAI MERAH 2D2,\r\n96000 SIBU, SARAWAK, EAST MALAYSIA.\r\nTEL: 084-212 263/016-870 9068  \r\nFAX: 084-211 431', '2019-07-07 15:09:33', 0, 1, 0, NULL, 3, NULL, '2019-07-03 07:09:33', '2019-07-03 07:09:33'),
(52, NULL, 'PGKCH0008', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 41, 'PENSONIC SALES & SERVICE SDN BHD', 61, '1165 LORONG PERINDUSTRIAN BUKIT MINYAK 16\r\nTAMAN PERINDUSTRIAN BUKIT MINYAK\r\n14100 SIMPANG AMPAT PENANG MALAYSIA', 42, 'PENSONIC SALES & SERVICE SDN BHD -SW (162419-M)', 62, 'GRD & 1ST FLOOR, 253 JALAN DATUK\r\nWEE KHENG CHIANG, 93450 KUCHING, SARAWAK.\r\nTEL: 082-424003/256003  FAX: 082-424939', 42, 'PENSONIC SALES & SERVICE SDN BHD -SW (162419-M)', 62, 'GRD & 1ST FLOOR, 253 JALAN DATUK\r\nWEE KHENG CHIANG, 93450 KUCHING, SARAWAK.\r\nTEL: 082-424003/256003  FAX: 082-424939', '2019-07-07 15:19:24', 0, 1, 0, NULL, 3, NULL, '2019-07-03 07:19:24', '2019-07-03 07:19:24'),
(53, NULL, 'PGMYY0004', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 9, 'MIRI SARAWAK', 9, 'MIRI SARAWAK', 9, 'BARKATH STORES (PENANG) SDN BHD', 23, '1207, LENGKOK PERINDUSTRIAN BUKIT\r\nMINYAK 1, KAWASAN PERINDUSTRIAN BUKIT\r\nMINYAK, 14100, SIMPANG AMPAT, SPT, PENANG', 23, 'MOH HENG CO. SDN BHD.', 41, 'LOT 3692-3694, BLOCK 6\r\nPERMY TECHNOLOGY PARK, KBLD\r\n98000 MIRI SARAWAK', 23, 'MOH HENG CO. SDN BHD.', 41, 'LOT 3692-3694, BLOCK 6\r\nPERMY TECHNOLOGY PARK, KBLD\r\n98000 MIRI SARAWAK.', '2019-07-07 16:19:09', 0, 1, 0, NULL, 3, NULL, '2019-07-03 08:19:09', '2019-07-03 08:19:09'),
(54, NULL, 'PGSBW0012', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 12, 'FARMSAFE CORPORATION SDN BHD', 26, 'B-09-03,PJ8, BLOCK B WEST NO. 23\r\nJALAN BARAT, SEKSYEN 8,\r\n46050 PETALING JAYA, SELANGOR DARUL EHSAN.', 13, 'KENSO PELITA NARUB-TAMIN SDN BHD', 27, 'NO. 1J. GROUND FLOOR,\r\nLORONG 20 JALAN TUNKU ABDUL RAHMAN\r\n96000 SIBU, SARAWAK.', 13, 'KENSO PELITA NARUB-TAMIN SDN BHD', 27, 'NO. 1J. GROUND FLOOR,\r\nLORONG 20 JALAN TUNKU ABDUL RAHMAN\r\n96000 SIBU, SARAWAK.', '2019-07-07 10:04:07', 0, 1, 0, NULL, 3, NULL, '2019-07-04 02:04:07', '2019-07-04 02:04:07'),
(55, NULL, 'PGKCH0009', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 13, 'SKY RAILWAY FURNITURE TRADING SDN BHD', 27, '2909, JALAN SUNGAI DAUN,\r\n14300 NIBONG TEBAL S.P.(S),\r\nPULAU PINANG.', 10, 'SIN SERI CAHAYA SDN BHD', 22, 'LOT 6950, NO. 5, GROUND FLOOR,\r\n4-1/2 MILE, JALAN MATANG,\r\n93050 KUCHING, SARAWAK.\r\nTEL: 082-645833', 10, 'SIN SERI CAHAYA SDN BHD', 22, 'LOT 6950, NO. 5, GROUND FLOOR,\r\n4-1/2 MILE, JALAN MATANG,\r\n93050 KUCHING, SARAWAK.\r\nTEL: 082-645833', '2019-07-07 11:41:21', 1, 1, 0, NULL, 3, NULL, '2019-07-04 03:41:21', '2019-07-04 03:41:21'),
(56, NULL, 'PGSDK0006', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 7, 'SANDAKAN SABAH', 7, 'SANDAKAN SABAH', 51, 'NBC FOOD INDUSTRIES SDN BHD', 71, 'A87, JALAN 1B-3, KAWASAN PERUSAHAAN MIEL,\r\nSUNGAI LALANG, 08000 SUNGAI PETANI,\r\nKEDAH, DARUL AMAN.', 52, 'SYARIKAT PERDAGANGAN YAW HON SDN BHD', 72, 'LOT 51, 1ST FLOOR, BDC LIGHT INDUSTRIAL ESTATE,\r\nMILE 1 1/2, LABUK ROAD, P.O. BOX 449,\r\nSANDAKAN, SABAH, MALAYSIA.', 52, 'SYARIKAT PERDAGANGAN YAW HON SDN BHD', 72, 'LOT 51, 1ST FLOOR, BDC LIGHT INDUSTRIAL ESTATE,\r\nMILE 1 1/2, LABUK ROAD, P.O. BOX 449,\r\nSANDAKAN, SABAH, MALAYSIA.', '2019-07-07 12:03:00', 0, 1, 0, NULL, 3, NULL, '2019-07-04 04:03:00', '2019-07-04 04:03:00'),
(57, NULL, 'PGTWU0002', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 8, 'TAWAU SABAH', 8, 'TAWAU SABAH', 10, 'MSM PERLIS SDN BHD', 22, '(FORMERLY KNOWN AS KILANG GULA FELDA PERLIS SDN BHD)\r\nP.O.BOX 42, 01700 KANGAR,\r\nPERLIS MALAYSIA', 10, 'PERNIAGAAN AWM (TWU) SDN BHD', 22, 'TB 10400, 10401, 10402,\r\nNEW DAT LIGHT INDUSTRIAL ESTATE,\r\nJALAN APAS, 91000 TAWAU, SABAH.', 10, 'PERNIAGAAN AWM (TWU) SDN BHD', 22, 'TB 10400, 10401, 10402,\r\nNEW DAT LIGHT INDUSTRIAL ESTATE,\r\nJALAN APAS, 91000 TAWAU, SABAH.', '2019-07-06 12:05:27', 0, 1, 0, NULL, 3, NULL, '2019-07-04 04:19:19', '2019-07-06 04:05:27'),
(58, NULL, 'PGSBW0013', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 53, 'MSM PERLIS SDN BHD', 73, '(FORMERLY KNOWN AS KILANG GULA FELDA PERLIS SDN BHD)\r\nP.O.BOX 42, 01700 KANGAR, PERLIS, MALAYSIA.', 55, 'SYARIKAT PERSATUAN JUALAN RUNCHIT BHD', 75, 'NO. 9, LOT 26, MERANTI LANE,\r\nEK DEE ROAD, PO BOX 884,\r\n96008 SIBU, SARAWAK.', 55, 'SYARIKAT PERSATUAN JUALAN RUNCHIT BHD', 75, 'NO. 9, LOT 26, MERANTI LANE,\r\nEK DEE ROAD, PO BOX 884,\r\n96008 SIBU, SARAWAK.', '2019-07-06 12:05:40', 0, 1, 0, NULL, 3, NULL, '2019-07-04 04:34:19', '2019-07-06 04:05:40'),
(59, NULL, 'PGSDK0007', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 7, 'SANDAKAN SABAH', 7, 'SANDAKAN SABAH', 53, 'MSM PERLIS SDN BHD', 73, '(FORMERLY KNOWN AS KILANG GULA FELDA PERLIS SDN BHD)\r\nP.O.BOX 42, 01700 KANGAR, PERLIS, MALAYSIA.', 56, 'CHUNG KWONG (SANDAKAN) SDN BHD', 76, 'LOT 167,SEDCO LIGHT INDUSTRIAL ESTATE,\r\nMILE 3 1/2, JALAN UTARA, PO BOX 3,\r\n90700 SANDAKAN, SABAH.', 56, 'CHUNG KWONG (SANDAKAN) SDN BHD', 76, 'LOT 167,SEDCO LIGHT INDUSTRIAL ESTATE,\r\nMILE 3 1/2, JALAN UTARA, PO BOX 3,\r\n90700 SANDAKAN, SABAH.', '2019-07-06 12:05:10', 0, 1, 0, NULL, 3, NULL, '2019-07-04 06:27:57', '2019-07-06 04:05:10'),
(60, NULL, 'PGKCH0010', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 57, 'GUAN HONG PLASTIC INDUSTRIES SDN BHD', 77, '8394, KAWASAN PERINDUSTRIAN KAMPUNG TELUK,\r\nSUNGAI DUA, 13800 BUTTERWORTH, PENANG.', 58, 'CHUN THONG HUA SDN BHD', 78, 'LOT 1286, SECTION 66, KTLD, JALAN DAGER/\r\nPERBADANAN, BINTAWA INDUSTRIAL ESTATE,\r\n93450 KUCHING, SARAWAK.', 58, 'CHUN THONG HUA SDN BHD', 78, 'LOT 1286, SECTION 66, KTLD, JALAN DAGER/\r\nPERBADANAN, BINTAWA INDUSTRIAL ESTATE,\r\n93450 KUCHING, SARAWAK.', '2019-07-07 12:46:13', 1, 1, 0, NULL, 3, NULL, '2019-07-04 06:46:40', '2019-07-05 04:46:13'),
(61, NULL, 'PGTWU0003', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 8, 'TAWAU SABAH', 8, 'TAWAU SABAH', 31, 'JATI HOME FURNITURE MARKETING SDN BHD', 51, 'LOT A82(44), JALAN 1B/3, KAWASAN PERUSAHAAN\r\nMIEL SUNGAI LALANG, 08000 SUNGAI PETANI, KEDAH', 59, 'CF CHAN FURNITURE SDN BHD', 79, 'TB 4202, TMN NORDIN BATU 1 3/4,\r\nJALAN KUHARA, 91016 TAWAU, SABAH.\r\nTEL: 089-768881, 089-768882  FAX: 089-768889', 59, 'CF CHAN FURNITURE DN BHD', 79, 'TB 4202, TMN NORDIN BATU 1 3/4,\r\nJALAN KUHARA, 91016 TAWAU, SABAH.\r\nTEL: 089-768881, 089-768882  FAX: 089-768889', '2019-07-07 11:39:05', 1, 1, 0, NULL, 3, NULL, '2019-07-04 07:06:05', '2019-07-05 03:39:05'),
(62, NULL, 'PGKCH0011', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 31, 'JATI HOME FURNITURE MARKETING SDN BHD', 51, 'LOT A82(44), JALAN 1B/3, KAWASAN PERUSAHAAN\r\nMIEL SUNGAI LALANG, 08000 SUNGAI PETANI, KEDAH', 10, 'EMPIRE FURNITURE CENTRE', 22, 'LOT 115, SL 9, JALAN STAPOK UTAMA,\r\nTAMAN VICTORIA, SUNGAI MAONG ULU,\r\n93250 KUCHING, SARAWAK.\r\nTEL: 082-681437', 10, 'EMPIRE FURNITURE CENTRE', 22, 'LOT 115, SL 9, JALAN STAPOK UTAMA,\r\nTAMAN VICTORIA, SUNGAI MAONG ULU,\r\n93250 KUCHING, SARAWAK.\r\nTEL: 082-681437', '2019-07-07 15:27:03', 1, 1, 0, NULL, 3, NULL, '2019-07-04 07:27:03', '2019-07-04 07:27:03'),
(63, NULL, 'PGKCH0012', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 61, 'KENG CHEONG TRADING CO. SDN BHD', 81, 'NO. 22, JALAN KIKIK. TAMAN INDERAWASIH\r\n13600 PERAI, PENANG', 58, 'KENG CHEONG TRADING CO. SDN BHD', 78, 'LOT 8013, SECTION 64, 159, SUNG HONG\r\nGARDEN OFF JALAN TUN RAZAK,\r\n93300 KUCHING, SARAWAK.', 10, 'KENG CHEONG TRADING CO. SDN BHD', 22, 'LOT 8013, SECTION 64, 159, SUNG HONG\r\nGARDEN OFF JALAN TUN RAZAK,\r\n93300 KUCHING, SARAWAK.', '2019-07-07 15:57:09', 0, 1, 0, NULL, 3, NULL, '2019-07-04 07:57:09', '2019-07-04 07:57:09'),
(64, NULL, 'PGKCH0013', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 62, 'NIBONG TEBAL PAPER MILL SDN BHD', 82, '886 JALAN BANDAR BARU,\r\nSUNGAI KECIL,,\r\n14300 NIBONG TEBAL,, S.P.S. PENANG', 10, 'NIBONG TEBAL ENTERPRISE SDN BHD', 22, 'LOT 938, LORONG DEMAK LAUT 7A,\r\nDEMAK LAUT INDUSTRIAL PARK,\r\nJALAN BAKO, 93050 KUCHING, SARAWAK.', 10, 'METRO SEGAK FORWARDING AGENCY SDN BHD (1111154-A0', 22, 'A5-2, 1ST FLOOR, BLOCK A, YOSHI SQUARE,\r\nJALAN PELABUHAN, 93450 KUCHING, \r\nSARAWAK.', '2019-07-07 16:29:50', 0, 1, 0, NULL, 3, NULL, '2019-07-04 08:29:50', '2019-07-04 08:29:50'),
(65, NULL, 'PGSBW0014', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 63, 'BIOPLUS INDUSTRIES SDN BHD', 83, 'PLOT 303 AND 304, JALAN PKNK 3/1,\r\nKAWASAN PERINDUSTRIAN SUNGAI PETANI,\r\n08000 SUNGAI PETANI, KEDAH, MALAYSIA.', 64, 'SANHUPCHOON SAWMILL SDN BHD', 84, '177B, 3RD FL. KPG NYABOR RD.,\r\nP.O. BOX 1297, 96008 SIBU,\r\nSARAWAK, MALAYSIA.\r\nTEL:084-336619 FAX: 084-313389', 64, 'SANHUPCHOON SAWMILL SDN BHD', 84, '177B, 3RD FL. KPG NYABOR RD.,\r\nP.O. BOX 1297, 96008 SIBU,\r\nSARAWAK, MALAYSIA.', '2019-07-07 11:37:01', 0, 1, 0, NULL, 3, NULL, '2019-07-05 00:53:43', '2019-07-05 03:37:01'),
(66, NULL, 'PGKCH0014', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 65, 'TOYO TYRE S & M MALAYSIA SDN BHD', 85, 'PT 23101, JALAN TEMBAGA KUNING\r\nKAWASAN PERINDUSTRIAN KAMUNTING RAYA\r\n34600 KAMUNTING, PERAK.', 66, 'TOYO TYRE S & M MALAYSIA SDN. BHD.', 86, 'C/O KOBE SHIPPING & FORWARDING AGENCIES SDN BHD\r\nLOT 1394, SECTION 66, K.T.L.D. JALAN MERANTI\r\nPENDING INDUSTRIAL ESTATE, 93450 KUCHING, SARAWAK.', 66, 'TOYO TYRE S & M MALAYSIA SDN. BHD.', 86, 'C/O KOBE SHIPPING & FORWARDING AGENCIES SDN BHD\r\nLOT 1394, SECTION 66, K.T.L.D. JALAN MERANTI\r\nPENDING INDUSTRIAL ESTATE, 93450 KUCHING, SARAWAK.', '2019-07-07 09:19:14', 0, 1, 0, NULL, 3, NULL, '2019-07-05 01:19:14', '2019-07-05 01:19:14'),
(67, NULL, 'PGKCH0015', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'RAINBOW HANDICRAFT SDN.BHD.', 22, 'B7, SUSURAN PERUSAHAAN 1,\r\nTAMAN BANDAR BARU MERGONG,\r\nLEBUHRAYA SULTANAH BAHIYAH,\r\n05150 ALOR SETAR,KEDAH DARUL AMAN', 10, 'RAINBOW HANTARAN SDN.BHD.', 22, 'NO.391, TAMAN LEE LING,\r\nJALAN MATANG, \r\n93250 KUCHING, SARAWAK\r\nTEL: 082-648813 HP: 0165226118', 10, 'SAL FORWARDING AGENCY', 22, 'LOT 983 SECTION 66\r\nJALAN TAMBATAN PENDING \r\nINDUSTRIAL ESTATE\r\n93250 KUCHING SARAWAK', '2019-07-07 09:36:15', 0, 1, 0, NULL, 3, NULL, '2019-07-05 01:36:15', '2019-07-05 01:36:15'),
(68, NULL, 'PGKCH0016', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 67, 'LISBLE DESIGN SDN BHD', 87, 'NO. 2946, BAGAN BUAYA, CHANGKAT,\r\n14300 NIBONG TEBAL, SEBERANG PERAI SELATAN,\r\nPULAU PINANG.', 68, 'LISBLE FURNITURE TRADING SDN BHD', 88, 'LOT 823, LORONG DEMAK LAUT 3A1,\r\nBLOCK 7, MUARA TEBAS LAND DISTRICT,\r\nDEMAK LAUT INDUSTRIAL PARK, 93050 KUCHING, SARAWAK', 68, 'LISBLE FURNITURE TRADING SDN BHD', 88, 'LOT 823, LORONG DEMAK LAUT 3A1,\r\nBLOCK 7, MUARA TEBAS LAND DISTRICT,\r\nDEMAK LAUT INDUSTRIAL PARK, 93050 KUCHING, SARAWAK', '2019-07-07 10:19:26', 0, 1, 0, NULL, 3, NULL, '2019-07-05 01:59:01', '2019-07-06 02:19:26'),
(69, NULL, 'PGTWU0004', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 8, 'TAWAU SABAH', 8, 'TAWAU SABAH', 7, 'UNITY MARKETING (MALAYSIA) SDN BHD', 30, 'C/O NO. 3, JLN MUTIARA 1/10,\r\nTAMAN MUTIARA INDAH,\r\n47100 PUCHONG', 8, 'WATER WORLD (T181155)', 31, '(GST NO.001222819840)\r\nNO. 4204, MILE 2, JALAN SIN ON.\r\nP.O. BOX. 61689, 91026\r\nTAWAU, SABAH.', 8, 'WATER WORLD (T181155)', 31, '(GST NO.001222819840)\r\nNO. 4204, MILE 2, JALAN SIN ON.\r\nP.O. BOX. 61689, 91026\r\nTAWAU, SABAH.', '2019-07-07 11:18:59', 0, 1, 0, NULL, 3, NULL, '2019-07-05 02:14:25', '2019-07-05 03:18:59'),
(70, NULL, 'PGKCH0017', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 53, 'MSM PRAI BERHAD (3573-D0', 73, '798 MAIN ROAD,\r\n13600 PRAI, SEBERANG PRAI,\r\nPULAU PINANG, MALAYSIA.', 69, 'MSM KUCHING DEPOT', 89, 'C/O LEE CHUAN HONG CO. SDN. BHD.\r\nLOT 753, SECTION 64, JALAN SUNGAI PRIOK,\r\nOFF JALAN PENDING, 93450 KUCHING, SARAWAK.', 10, 'LEE CHUAN HONG CO SDN BHD', 22, 'LOT 753, SECTION 64, JALAN SUNGAI PRIOK, OFF JALAN PENDING,\r\n93450 KUCHING, SARAWAK.', '2019-07-07 08:59:05', 0, 1, 0, NULL, 3, NULL, '2019-07-05 02:46:29', '2019-07-06 00:59:05'),
(71, NULL, 'PGKCH0018', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'JWC MARINE PRODUCTS SDN BHD (1020383-D)', 22, 'NO. 14 & 16, LORONG INDUSTRI 7,\r\nKAWASAN INDUSRIES BUKIT PANCHOR,14300 NIBONG TEBAL,\r\nPENANG, MALAYSIA.', 10, 'CHOP SIANG HENG', 22, 'NO. 6, GARTAK STREET,\r\nKUCHING, SARAWAK,\r\nP.O. BOX 170.\r\nTEL: 082-242782', 10, 'CHOP SIANG HENG', 22, 'NO. 6, GARTAK STREET,\r\nKUCHING, SARAWAK,\r\nP.O. BOX 170.\r\nTEL: 082-242782', '2019-07-07 15:03:23', 0, 1, 0, NULL, 3, NULL, '2019-07-05 03:13:42', '2019-07-09 07:03:23'),
(72, NULL, 'PGKCH0019', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 70, 'SASTRIA ENTERPRISE', 90, 'NO. 307, JALAN ZAMRUD 10,\r\nTAMAN PEKAN BARU,\r\n08000 SUNGAI PETANI, KEDAH.', 10, 'BINTANG FREIGHT SER. & SUPP.SDN BHD.', 22, 'LOT 493, 1ST FLOOR, LORONG 11A,\r\nJALAN ANG CHEONG HO,\r\n93100 KUCHING, SARAWAK.\r\nTEL: 082-411633,   FAX: 082-411632', 10, 'BINTANG FREIGHT SER. & SUPP.SDN BHD.', 22, 'LOT 493, 1ST FLOOR, LORONG 11A,\r\nJALAN ANG CHEONG HO,\r\n93100 KUCHING, SARAWAK.\r\nTEL: 082-411633,   FAX: 082-411632', '2019-07-07 13:15:08', 0, 1, 0, NULL, 3, NULL, '2019-07-05 03:29:06', '2019-07-05 05:15:08'),
(73, NULL, 'PGKCH0020', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 71, 'GOLD LEAF MANUFACTURING SDN BHD', 91, '1203 PERMATANG BERAH, TITI MUKIM,\r\nTELOK AIR TAWAR, 13050 BUTTERWORTH,\r\nPENANG WEST MALAYSIA', 72, 'GOLD LEAF MANUFACTURING SDN.BHD.', 92, '(KUCHING BRANCH) 277A, JALAN SEMABA\r\n5TH MILE, SEMABA LIGHT INDUSTRIAL PARK,\r\n93250 KUCHING, SARAWAK', 72, 'GOLD LEAF MANUFACTURING SDN.BHD.', 92, '(KUCHING BRANCH) 277A, JALAN SEMABA\r\n5TH MILE, SEMABA LIGHT INDUSTRIAL PARK,\r\n93250 KUCHING, SARAWAK', '2019-07-07 08:52:25', 0, 1, 0, NULL, 3, NULL, '2019-07-05 03:44:28', '2019-07-06 00:52:25'),
(74, NULL, 'PGKCH0021', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'QUANTERM LOGISTICS SDN BHD  (386230-T)', 22, 'PENANG CHINESE TOWN HALL\r\nROOM 707, 7TH FLOOR, \r\n22LEBUH PITT, 10200 PENANG,\r\nMALAYSIA.', 10, 'QUANTERM LOGISTICS SDN BHD', 22, 'LOT NO. 720, BLOCK 7, LORONG\r\nDEMAK LAUT 2A, DEMAK LAUT \r\nINDUSTRIAL PARK, 93050 KUCHING,\r\nSARAWAK, MALAYSIA.', 10, 'QUANTERM LOGISTICS SDN BHD', 22, 'LOT NO. 720, BLOCK 7, LORONG\r\nDEMAK LAUT 2A, DEMAK LAUT \r\nINDUSTRIAL PARK, 93050 KUCHING,\r\nSARAWAK, MALAYSIA.', '2019-07-07 09:09:06', 0, 1, 0, NULL, 3, NULL, '2019-07-05 03:57:32', '2019-07-06 01:09:06'),
(75, NULL, 'PGKCH0022', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 94, 'PROFIT ACTION SDN BHD', 114, '6110, TINGKAT SELAMAT 9,\r\nKAMPUNG SELAMAT, 13300 TASEK GELUGOR,\r\nSEBERANG PERAI UTARA.', 95, 'WEY SING FURNITURE TRADING SDN.BHD.', 115, 'LOT 8034, BLOCK 59, MUARA TUANG LAND DISTRICT, KOTA SAMARAHAN INDUSTRIAL ESTATE, KG. TANJUNG BUNDONG, 94300 KOTA SAMARAHAN,\r\nKUCHING, SARAWAK', 95, 'WEY SING FURNITURE TRADING SDN.BHD.', 115, 'LOT 8034, BLOCK 59, MUARA TUANG LAND DISTRICT, KOTA SAMARAHAN INDUSTRIAL ESTATE, KG. TANJUNG BUNDONG, 94300 KOTA SAMARAHAN,\r\nKUCHING, SARAWAK', '2019-07-07 08:04:10', 1, 1, 0, NULL, 3, NULL, '2019-07-05 03:58:39', '2019-07-09 00:04:10'),
(76, NULL, 'PGSBW0015', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 96, 'AUSSIE SLEEP MARKETING SDN.BHD.', 116, 'NO.2400, TINGKAT SERINDIT 1, TAMAN DESA JAWI,\r\n14200 SUNGAI BAKAP, PULAU PINANG', 98, 'YHL COMFORT FURNITURE SDN.BHD. (1125690-H)', 118, 'NO.40-50, LORONG 2, JALAN LING KAI CHENG,96000 SIBU, SARAWAK\r\nTEL:084-335561 FAX:084-331562', 98, 'YHL COMFORT FURNITURE SDN.BHD. (1125690-H)', 118, 'NO.40-50, LORONG 2, JALAN LING KAI CHENG,96000 SIBU, SARAWAK\r\nTEL:084-335561 FAX:084-331562', '2019-07-06 12:48:44', 1, 1, 0, NULL, 3, NULL, '2019-07-05 03:59:40', '2019-07-06 04:48:44'),
(77, NULL, 'PGPKG0003', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 73, 'ECONSHIP MARINE SDN BHD', 93, 'SUITE #21-06, LEVEL 21, CENTRO, NO.8, JALAN BATU\r\nTIGA LAMA, 41300 KLANG, SELANG, MALAYSIA', 74, 'ECONSHIP MARINE SDN BHD.', 94, 'KCX SOCIAL OFFICE 67-1, JALAN SETIA UTAMA AT\r\nU13/AT SETIA ALAM, SEKSYEN U13,\r\n40170 SHAH ALAM, SELANGOR', 74, 'ECONSHIP MARINE SDN BHD.', 94, 'KCX SOCIAL OFFICE 67-1, JALAN SETIA UTAMA AT\r\nU13/AT SETIA ALAM, SEKSYEN U13,\r\n40170 SHAH ALAM, SELANGOR', '2019-07-06 13:32:32', 0, 1, 0, NULL, 3, NULL, '2019-07-05 04:00:46', '2019-07-05 05:32:32'),
(78, NULL, 'PGKCH0010A', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 57, 'EE-LIAN ENTERPRISE (M)  SDN.BHD.', 77, '1027, LENGKOK PERINDUSTRIAN BKT.MINYAK 1, KAWASAN PERINDUSTRIAN BKT. MINYAK,\r\n14100 SIMPANG AMPAT, PENANG,\r\nMALAYSIA.', 58, 'CHUN THONG HUA SDN BHD', 78, 'LOT 1286, SECTION 66, KTLD, JLN DAGER 1 PERBADANAN, BINTAMA INDUSTRIAL ESTATE,  93450 KUCHING, \r\nP.O. BOX. 247., 93702 KUCHING, \r\nSARAWAK', 58, 'CHUN THONG HUA SDN BHD', 78, 'LOT 1286, SECTION 66, KTLD, JLN DAGER 1 PERBADANAN, BINTAMA INDUSTRIAL ESTATE,  93450 KUCHING, \r\nP.O. BOX. 247., 93702 KUCHING, \r\nSARAWAK', '2019-07-07 15:30:20', 1, 1, 0, NULL, 3, NULL, '2019-07-05 04:04:15', '2019-07-09 07:30:20'),
(79, NULL, 'PGKCH0011', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', '2019-07-07 12:04:16', 0, 1, 0, NULL, 3, '2019-07-05 04:39:16', '2019-07-05 04:04:16', '2019-07-05 04:39:16'),
(80, NULL, 'PGKCH0012', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', '2019-07-07 12:06:02', 0, 1, 0, NULL, 3, '2019-07-05 04:39:26', '2019-07-05 04:06:02', '2019-07-05 04:39:26'),
(81, NULL, 'PGKCH0013', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', '2019-07-07 12:06:55', 0, 1, 0, NULL, 3, '2019-07-05 04:39:38', '2019-07-05 04:06:55', '2019-07-05 04:39:38');
INSERT INTO `bill_ladings` (`id`, `bc_no`, `bl_no`, `vessel_id`, `vessel_type`, `voyage_id`, `pol_id`, `pol_name`, `pod_id`, `pod_name`, `fpd_id`, `fpd_name`, `shipper_id`, `shipper_name`, `shipper_add_id`, `shipper_add`, `consignee_id`, `consignee_name`, `consignee_add_id`, `consignee_add`, `notify_id`, `notify_name`, `notify_add_id`, `notify_add`, `bl_date`, `freight_term`, `cont_ownership`, `telex_status`, `telex_path`, `no_of_bl`, `deleted_at`, `created_at`, `updated_at`) VALUES
(82, NULL, 'PGKCH0014', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', '2019-07-07 12:07:42', 0, 1, 0, NULL, 3, '2019-07-05 04:39:46', '2019-07-05 04:07:42', '2019-07-05 04:39:46'),
(83, NULL, 'PGKCH0015', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', '2019-07-07 12:08:38', 0, 1, 0, NULL, 3, '2019-07-05 04:40:08', '2019-07-05 04:08:38', '2019-07-05 04:40:08'),
(84, NULL, 'PGKCH0016', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', '2019-07-07 12:09:42', 0, 1, 0, NULL, 3, '2019-07-05 04:37:54', '2019-07-05 04:09:42', '2019-07-05 04:37:54'),
(85, NULL, 'PGKCH0017', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', '2019-07-07 12:13:59', 0, 1, 0, NULL, 3, '2019-07-05 04:20:47', '2019-07-05 04:13:59', '2019-07-05 04:20:47'),
(86, NULL, 'PGKCH0018', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', '2019-07-07 12:25:40', 0, 1, 0, NULL, 3, '2019-07-05 04:26:11', '2019-07-05 04:14:50', '2019-07-05 04:26:11'),
(87, NULL, 'PGTWU0005', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 8, 'TAWAU SABAH', 8, 'TAWAU SABAH', 16, 'SILVERSTONE BERHAD', 34, 'LOT 5831, KAMUNTING INDUSTRIAL ESTATE II,\r\nP.O.BOX 2, 34600 KAMUNTING PERAK\r\nDARUL RIDZUAN, MALAYSIA', 10, 'WENG FOOK TYRES & BATTERIES SDN BHD', 22, 'MDLD 4651, LOT 73, TECK GUAN \r\nINDUSTRIAL ESTATE KM3,\r\nJALAN SEGAMA LAHAD DATU, SABAH.', 18, 'SILVERSTONE MARKETING SDN BHD', 36, 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN\r\nJALAN MENGGATAL / JALAN TURAN\r\n88450 KOTA KINABALU, SABAH. \r\nTEL:088-490026', '2019-07-06 09:21:24', 0, 1, 0, NULL, 3, NULL, '2019-07-05 04:15:37', '2019-07-09 01:21:24'),
(88, NULL, 'PGKCH0019', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', '2019-07-07 12:16:31', 0, 1, 0, NULL, 3, '2019-07-05 04:26:20', '2019-07-05 04:16:31', '2019-07-05 04:26:20'),
(89, NULL, 'PGMYY0005', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 9, 'MIRI SARAWAK', 9, 'MIRI SARAWAK', 10, 'SIANG HENG PLASTIC WARE SDN BHD', 22, 'LOT 7282 (PT666) JALAN PERUSAHAAN 1\r\nKAWASAN PERUSAHAAN PARIT BUNTAR\r\n34200 PARIT BUNTAR, PERAK.', 10, 'MIRI DEPARTMENTAL SDN BHD', 22, 'LOT 49, NO. 54, PIASAU INDUSTRIAL\r\nESTATE, 98000 MIRI, SARAWAK.', 10, 'MIRI DEPARTMENTAL SDN BHD', 22, 'LOT 49, NO. 54, PIASAU INDUSTRIAL\r\nESTATE, 98000 MIRI, SARAWAK.', '2019-07-07 11:00:37', 0, 1, 0, NULL, 3, NULL, '2019-07-05 04:18:00', '2019-07-06 03:00:37'),
(90, NULL, 'PGKCH0020', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 7, 'EP VENTURES SDN BHD', 30, 'PLOT 5, LOT 1061, JALAN PENGKALAN,\r\nMK 6, 14000 BUKIT TENGAH,\r\nBUKIT MERTAJAM, PENANG', 8, 'MATER-PACK (SARAWAK) SDN BHD', 31, 'LOT 1270-1271, SECTION 66,\r\nKTLD BINTAWA INDUSTRIAL ESTATE,\r\nJALAN KELULI, 93450 KUCHING, SARAWAK, MALAYSIA', 8, 'MATER-PACK (SARAWAK) SDN BHD', 31, 'LOT 1270-1271, SECTION 66,\r\nKTLD BINTAWA INDUSTRIAL ESTATE,\r\nJALAN KELULI, 93450 KUCHING, SARAWAK, MALAYSIA', '2019-07-06 12:53:07', 0, 1, 0, NULL, 3, '2019-07-05 04:53:22', '2019-07-05 04:53:07', '2019-07-05 04:53:22'),
(91, NULL, 'PGKCH0021', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 7, 'EP VENTURES SDN BHD', 30, 'PLOT 5, LOT 1061, JALAN PENGKALAN,\r\nMK 6, 14000 BUKIT TENGAH,\r\nBUKIT MERTAJAM, PENANG', 8, 'MATER-PACK (SARAWAK) SDN BHD', 31, 'LOT 1270-1271, SECTION 66,\r\nKTLD BINTAWA INDUSTRIAL ESTATE,\r\nJALAN KELULI, 93450 KUCHING, SARAWAK, MALAYSIA', 8, 'MATER-PACK (SARAWAK) SDN BHD', 31, 'LOT 1270-1271, SECTION 66,\r\nKTLD BINTAWA INDUSTRIAL ESTATE,\r\nJALAN KELULI, 93450 KUCHING, SARAWAK, MALAYSIA', '2019-07-07 12:53:42', 0, 1, 0, NULL, 3, '2019-07-05 04:54:23', '2019-07-05 04:53:42', '2019-07-05 04:54:23'),
(92, NULL, 'PGKCH0022', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 7, 'EP VENTURES SDN BHD', 30, 'PLOT 5, LOT 1061, JALAN PENGKALAN,\r\nMK 6, 14000 BUKIT TENGAH,\r\nBUKIT MERTAJAM, PENANG', 8, 'MATER-PACK (SARAWAK) SDN BHD', 31, 'LOT 1270-1271, SECTION 66,\r\nKTLD BINTAWA INDUSTRIAL ESTATE,\r\nJALAN KELULI, 93450 KUCHING, SARAWAK, MALAYSIA', 8, 'MATER-PACK (SARAWAK) SDN BHD', 31, 'LOT 1270-1271, SECTION 66,\r\nKTLD BINTAWA INDUSTRIAL ESTATE,\r\nJALAN KELULI, 93450 KUCHING, SARAWAK, MALAYSIA', '2019-07-07 12:54:48', 0, 1, 0, NULL, 3, '2019-07-05 04:54:58', '2019-07-05 04:54:48', '2019-07-05 04:54:58'),
(93, NULL, 'PGKCH0023', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 7, 'EP VENTURES SDN BHD', 30, 'PLOT 5, LOT 1061, JALAN PENGKALAN,\r\nMK 6, 14000 BUKIT TENGAH,\r\nBUKIT MERTAJAM, PENANG', 8, 'MATER-PACK (SARAWAK) SDN BHD', 31, 'LOT 1270-1271, SECTION 66,\r\nKTLD BINTAWA INDUSTRIAL ESTATE,\r\nJALAN KELULI, 93450 KUCHING, SARAWAK, MALAYSIA', 8, 'MATER-PACK (SARAWAK) SDN BHD', 31, 'LOT 1270-1271, SECTION 66,\r\nKTLD BINTAWA INDUSTRIAL ESTATE,\r\nJALAN KELULI, 93450 KUCHING, SARAWAK, MALAYSIA', '2019-07-07 11:01:33', 0, 1, 0, NULL, 3, NULL, '2019-07-05 04:55:18', '2019-07-06 03:01:33'),
(94, NULL, 'PGKCH0024', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 31, 'JATI HOME FURNITURE MARKETING SDN BHD', 51, 'LOT A82(44), JALAN 1B/3, KAWASAN PERUSAHAAN\r\nMIEL SUNGAI LALANG, 08000 SUNGAI PETANI, KEDAH', 10, 'JATI HOME FURNITURE (SARAWAK) SDN BHD', 22, '192A, LORONG 10, JALAN STAKAN, \r\nBATU 7, KOTA SENTOSA, \r\nJALAN PENRISSEN, 93250 KUCHING, SARAWAK.\r\nTEL: 082 618689  FAX: 082 618689', 10, 'JATI HOME FURNITURE (SARAWAK) SDN BHD', 22, '192A, LORONG 10, JALAN STAKAN, \r\nBATU 7, KOTA SENTOSA, \r\nJALAN PENRISSEN, 93250 KUCHING, SARAWAK.\r\nTEL: 082 618689  FAX: 082 618689', '2019-07-07 15:39:31', 1, 1, 0, NULL, 3, NULL, '2019-07-05 04:56:20', '2019-07-09 07:39:31'),
(95, NULL, 'PGKCH0025', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 91, 'VEOLIA WATER TECHNOLOGIES SDN BHD', 111, '68, JALAN ICON CITY\r\n14000 BUKIT MERTAJAM', 92, 'TAIYO YUDEN SARAWAK SDN BHD', 112, 'LOT 977, BLOCK 12,SAMA JYA FREE\r\nINDUSTRIAL ZONE, KUCHING 93450 SARAWAK\r\nPTC: SIMON (H/P: 012 897 6022)', 10, 'AIRSEAL FREIGHT FORWARDERS COMPANY', 22, 'LOT 7586, 3RD FLOOR, SIM HUNG ENG\r\nBUILDING, KWONG LEE BANK ROAD,\r\n93450 KUCHING\r\nTEL: 082 484 778  H/P: 016 862 2778', '2019-07-07 15:04:39', 0, 1, 0, NULL, 3, NULL, '2019-07-05 04:57:05', '2019-07-05 07:04:39'),
(96, NULL, 'PGKCH0026', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 91, 'VEOLIA WATER TECHNOLOGIES SDN BHD', 111, '68, JALAN ICON CITY\r\n14000 BUKIT MERTAJAM', 92, 'TAIYO YUDEN SARAWAK SDN BHD', 112, 'LOT 977, BLOCK 12,SAMA JYA FREE\r\nINDUSTRIAL ZONE, KUCHING 93450 SARAWAK\r\nPTC: SIMON (H/P: 012 897 6022)', 10, 'AIRSEAL FREIGHT FORWARDERS COMPANY', 22, 'LOT 7586, 3RD FLOOR, SIM HUNG ENG BUILDING,, KWONG LEE BANK ROAD\r\n93450 KUCHING\r\nTEL:082 484 778/HP:016 862 2778', '2019-07-07 15:42:51', 0, 1, 0, NULL, 3, NULL, '2019-07-05 04:57:44', '2019-07-05 07:42:51'),
(97, NULL, 'PGKCH0027', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'LEADER STEEL SDN BHD', 22, 'WISMA LEADER STEEL, PLOT 85, \r\nLORONG PERUSAHAAN UTAMA,\r\nKAWASAN PERUSAHAAN BUKIT TENGAH,14000 BUKIT TENGAH, \r\nSEBERANG PERAI TENGAH, PULAU PINANG, MALAYSIA.', 10, 'LEADER STEEL SDN BHD', 22, 'LOT 841, BLOCK 7, MTLD, DEMAK LAUT\r\nINDUSTRIAL PARK, PHASE 3, \r\n93050 KUCHING, SARAWAK, MALAYSIA.', 10, 'LEADER STEEL SDN BHD', 22, 'LOT 841, BLOCK 7, MTLD, DEMAK LAUT\r\nINDUSTRIAL PARK, PHASE 3, \r\n93050 KUCHING, SARAWAK, MALAYSIA.', '2019-07-07 09:01:45', 0, 1, 0, NULL, 3, NULL, '2019-07-05 04:58:23', '2019-07-06 01:01:45'),
(98, NULL, 'PGKCH0028', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'CHANG QING WOODEN MANUFACTURES SDN BHD', 22, 'NO. 3288, TINGKAT SELAMAT 5,\r\nKAMPUNG SELAMAT,\r\n13300 TASEK GELUGOR, S.P.U.', 10, 'CHAN FUNERAL SERVICES', 22, 'NO. 11-12 LOTS 1915 & 1916,\r\nJALAN BURUNG RAWA, 3 1/3 MILE OFF\r\nJALAN BATU RAWA,\r\n93250 KUCHING, SARAWAK.', 10, 'METRO SEGAK FORWARDING AGENCY SDN BHD', 22, 'LOT 132, BLOCK 5, JALAN BAKO,\r\nMUARA TEBAS,\r\n93050 KUCHING, SARAWAK.', '2019-07-07 16:18:01', 0, 1, 0, NULL, 3, NULL, '2019-07-05 04:59:01', '2019-07-05 08:18:01'),
(113, NULL, 'PGBKI0018', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 99, 'SEBERANG FLOUR MILL SDN BHD', 119, '2425 LORONG PERUSAHAAN 2\r\nPRAI INDUSTRIAL ESTATE\r\n13600 PRAI, PENANG, MALAYSIA', 10, 'HING WAH SAUCE & FOODSTUFF (SABAH) SDN BHD', 22, 'LOT 53 & 55, SUDC LIGHT INDUSTRIAL\r\nESTATE, LOK KAWI, P.O. BOX 10051,\r\n88801 KOTA KINABALU, SABAH.', 10, 'HING WAH SAUCE & FOODSTUFF (SABAH) SDN BHD', 22, 'LOT 53 & 55, SUDC LIGHT INDUSTRIAL\r\nESTATE, LOK KAWI, P.O. BOX 10051,\r\n88801 KOTA KINABALU, SABAH.', '2019-07-10 10:07:40', 0, 1, 0, NULL, 3, NULL, '2019-07-06 02:07:40', '2019-07-06 02:07:40'),
(99, NULL, 'PGKCH0029', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'SINSEESENG CASKET SDN BHD', 22, '780L, JALAN BAKAR SAMPAH,\r\n08000 SUNGAI PETANI, KEDAH.', 10, 'SHIN TRADING', 22, 'SUBLOT 9, FIRST FLOOR, \r\nLOT 1336,  BLOCK 9, BDLD,\r\nJALAN KUCHING/SERIAN ROAD,\r\nSARAWAK', 10, 'METRO SEGAK FORWAARDING AGENCY SDN BHD', 22, 'LOT 132, BLOCK 5, JALAN BAKO,\r\nMUARA TEBAS,\r\n93050 KUCHING, SARAWAK.', '2019-07-07 16:35:53', 0, 1, 0, NULL, 3, NULL, '2019-07-05 04:59:57', '2019-07-05 08:35:53'),
(136, NULL, 'PGPKG0013', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 77, 'INFINITY BULK LOGISTICS SDN BHD (609736-H)', 97, 'NO.2, JALAN PUSAT PERNIAGAAN RAJA UDA 1,\r\nPUSAT PERNIAGAAN RAJA UDA, 12300 BUTTERWORTH,\r\nPENANG', 78, 'INFINITY LINES SDN BHD', 98, 'NO.2, JALAN KASUARINA 8/KS07 BANDAR BOTANIC\r\n41200 KLANG', 78, 'INFINITY LINES SDN BHD', 98, 'NO.2, JALAN KASUARINA 8/KS07 BANDAR BOTANIC\r\n41200 KLANG', '2019-07-10 09:44:36', 0, 1, 0, NULL, 3, NULL, '2019-07-09 01:44:36', '2019-07-09 01:44:36'),
(100, NULL, 'PGKCH0030', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 10, 'HEAP SENG HIN', 22, 'NO. 6135, KAMPUNG BARU, \r\nSUNGAI PUYU,\r\n13400 BUTTERWORTH, PENANG.', 10, 'TAI HOCK SHEN XIANG TRADING', 22, 'LOT 440, SECTION 10,\r\nRUBBER ROAD,\r\n93150 KUCHING, SARAWAK.', 10, 'METRO SEGAK FORWARDING AGENCY SDN BHD', 22, 'LOT 132, BLOCK 5, JALAN BAKO,\r\nMUARA TEBAS,\r\n93050 KUCHING, SARAWAK.', '2019-07-07 16:27:10', 0, 1, 0, NULL, 3, NULL, '2019-07-05 05:00:48', '2019-07-05 08:27:10'),
(101, NULL, 'PGKCH0031', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 2, 'KUCHING SARAWAK', 2, 'KUCHING SARAWAK', 96, 'AUSSIE SLEEP MARKETING SDN.BHD.', 116, 'NO.2400, TINGKAT SERINDIT 1, TAMAN DESA JAWI,\r\n14200 SUNGAI BAKAP, PULAU PINANG', 97, 'EMPIRE FURNITURE CENTRE (A73065)', 117, 'LOT 115, SL 9, JALAN STAPOK UTAMA,\r\nTAMAN VICTORIA, SG.MAONG ULU,\r\n93250 KUCHING, SARAWAK\r\nTEL:082-681437 FAX:082-681437', 97, 'EMPIRE FURNITURE CENTRE (A73065)', 117, 'LOT 115, SL 9, JALAN STAPOK UTAMA,\r\nTAMAN VICTORIA, SG.MAONG ULU,\r\n93250 KUCHING, SARAWAK\r\nTEL:082-681437 FAX:082-681437', '2019-07-06 12:48:30', 1, 1, 0, NULL, 3, NULL, '2019-07-05 05:01:22', '2019-07-06 04:48:30'),
(102, NULL, 'PGPKG0004', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 75, 'YANG MING LINE (M) SDN BHD', 95, 'LOT 13.01, LEVEL 13, MENARA KWSP, NO.38,\r\nJALAN SULTAN AHMAD SHAH, 10500 PENANG, MALAYSIA', 76, 'YANG MING LINE (M) SD.BHD.', 96, 'SUITE 12.01, LEVEL 12, MENARA TREND,IMS\r\nNO.68, JALAN BATAI LAUT 4, TAMAN INTAN\r\n41300 KLANG, SELANGOR', 76, 'YANG MING LINE (M) SD.BHD.', 96, 'SUITE 12.01, LEVEL 12, MENARA TREND,IMS\r\nNO.68, JALAN BATAI LAUT 4, TAMAN INTAN\r\n41300 KLANG, SELANGOR', '2019-07-06 13:36:03', 0, 1, 0, NULL, 3, NULL, '2019-07-05 05:36:03', '2019-07-05 05:36:03'),
(103, NULL, 'PGPKG0005', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 37, 'HYUNDAI MERCHANT MARINE (MALAYSIA) SDN BHD', 57, 'UNIT 1.3A1, 1ST FLOOR, WISMA LEADER\r\nNO.8 JALAN LARUT, 10050, PENANG, MALAYSIA', 38, 'HYUNDAI MERCHANT MARINE (M) SDN BHD', 58, '11TH FLOOR, EAST WING, WISMA COSPLANT 2,\r\nNO.7, JALAN SS16/1, 47500 SUBANG JAYA, SELANGPR DARUL\r\nEHSAN', 38, 'HYUNDAI MERCHANT MARINE (M) SDN BHD', 58, '11TH FLOOR, EAST WING, WISMA COSPLANT 2,\r\nNO.7, JALAN SS16/1, 47500 SUBANG JAYA, SELANGPR DARUL\r\nEHSAN', '2019-07-06 14:30:28', 0, 1, 0, NULL, 3, NULL, '2019-07-05 05:39:26', '2019-07-05 06:30:28'),
(104, NULL, 'PGPKG0006', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 77, 'INFINITY BULK LOGISTICS SDN BHD (609736-H)', 97, 'NO.2, JALAN PUSAT PERNIAGAAN RAJA UDA 1,\r\nPUSAT PERNIAGAAN RAJA UDA, 12300 BUTTERWORTH,\r\nPENANG', 78, 'INFINITY LINES SDN BHD', 98, 'NO.2, JALAN KASUARINA 8/KS07 BANDAR BOTANIC\r\n41200 KLANG', 78, 'INFINITY LINES SDN BHD', 98, 'NO.2, JALAN KASUARINA 8/KS07 BANDAR BOTANIC\r\n41200 KLANG', '2019-07-06 13:45:33', 0, 1, 0, NULL, 3, NULL, '2019-07-05 05:45:33', '2019-07-05 05:45:33'),
(105, NULL, 'PGPKG0007', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 39, 'INTERASIA LINES (M) SDN BHD', 59, '55-18-C, MENARA NORTHAM, JALAN\r\nSULTAN SHAH, 10500 PENANG', 40, 'INTERASIA LINES (M) SDN BHD.', 60, 'SUITE 7.01, LEVEL 7, IMS 2, 88, JALAN BATAI LAUT\r\n4, 41300 KLANG, SELANGOR', 40, 'INTERASIA LINES (M) SDN BHD.', 60, 'SUITE 7.01, LEVEL 7, IMS 2, 88, JALAN BATAI LAUT\r\n4, 41300 KLANG, SELANGOR', '2019-07-06 13:48:15', 0, 1, 0, NULL, 3, NULL, '2019-07-05 05:48:15', '2019-07-05 05:48:15'),
(106, NULL, 'PGPKG0008', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 79, 'SINOKOR MALAYSIA SDN BHD C/O SIMBA LOGISTICS (MALAYSIA) SDN BHD', 99, 'UNIT 9.05, 9TH FLOOR, MENARA BOUSTEAD,NO.39, JALAN SULTAN AHMAD SHAH 10500 PENANG', 80, 'SINOKOR MALAYSIA SDN BHD', 100, 'SUITE 15-07, LEVEL 15, CENTRO NO.8, JALAN\r\nBATU TIGA LAMA, 41300 KLANG SELANGOR', 80, 'SINOKOR MALAYSIA SDN BHD', 100, 'SUITE 15-07, LEVEL 15, CENTRO NO.8, JALAN\r\nBATU TIGA LAMA, 41300 KLANG SELANGOR', '2019-07-06 14:54:16', 0, 1, 0, NULL, 3, NULL, '2019-07-05 05:53:30', '2019-07-05 06:54:16'),
(107, NULL, 'PGPKG0009', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 81, 'ORIENT OVERSEAS CONTAINER LINE (M) SDN BHD', 101, 'SUITE 1502, MWE PLAZA, NO.8, LEBUH FARQUHAR,\r\n10200 PENANG MALAYSIA', 82, 'ORIENT OVERSEAS CONTAINER LINE (M) SDN.BHD.', 102, 'UNIT 10.2. LEVEL 10, MENARA AXIS\r\nNO.2, JALAN 51A/223, 46100 PETALING JAYA,\r\nSELANGOR MALAYSIA', 82, 'ORIENT OVERSEAS CONTAINER LINE (M) SDN.BHD.', 102, 'UNIT 10.2. LEVEL 10, MENARA AXIS\r\nNO.2, JALAN 51A/223, 46100 PETALING JAYA,\r\nSELANGOR MALAYSIA', '2019-07-06 13:59:09', 0, 1, 0, NULL, 3, NULL, '2019-07-05 05:59:09', '2019-07-05 05:59:09'),
(108, NULL, 'PGPKG0010', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 84, 'MAXICONT SHIPPING AGENCIES SDN BHD', 104, 'MENARA BHL, 51-16-A,\r\nJALAN SULTAN AHMAD SHAH,\r\n10250 PENANG', 85, 'MAXICONT SHIPPING AGENCIES SDN BHD.', 105, 'LOT 9 CENTRO-15TH FLOOR, NO.8M,\r\nJALAN BATU TIGA LAMA, 41300 SELANGOR,\r\nMALAYSIA', 85, 'MAXICONT SHIPPING AGENCIES SDN BHD.', 105, 'LOT 9 CENTRO-15TH FLOOR, NO.8M,\r\nJALAN BATU TIGA LAMA, 41300 SELANGOR,\r\nMALAYSIA', '2019-07-06 16:41:51', 0, 1, 0, NULL, 3, '2019-07-09 00:58:41', '2019-07-05 06:18:00', '2019-07-09 00:58:41'),
(109, NULL, 'PGPKG0011', 6, 'FCL', 8, 1, 'PENANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 86, 'AK SHIPPING SDN BHD', 106, 'LEVEL 16, UNIT 16-H, WISMA BOON SIEW,\r\nNO.1, PENANG ROAD, 10000 PENANG\r\nMALAYSIA', 83, 'VASCO MARITIME GOODRICH ASIA PACIFIC SDN BHD', 103, 'CENTRO LEVEL 27-5, NO.8, JALAN BATU TIGA LAMA,\r\n41300 KLANG, SELANGOR, MALAYSIA', 83, 'VASCO MARITIME GOODRICH ASIA PACIFIC SDN BHD', 103, 'CENTRO LEVEL 27-5, NO.8, JALAN BATU TIGA LAMA,\r\n41300 KLANG, SELANGOR, MALAYSIA', '2019-07-06 14:52:54', 0, 1, 0, NULL, 3, NULL, '2019-07-05 06:19:50', '2019-07-05 06:52:54'),
(110, NULL, 'PGSBW0016', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 87, 'JINTYE CORPORATION  SDN BHD', 107, '6, 8, 10 JALAN PALA 14,\r\nKAWASAN PERINDUSTRIAN PERMATANG TINGGI,\r\n14100 BUKIT MERTAJAM.', 88, 'SIAWCOM MARKETING SDN BHD', 108, 'NO. 22, GROUND FLOOR, JALAN MERDEKA\r\nBARAT, 96000 SIBU, SARAWAK.\r\nTEL: 016-8861997', 88, 'SIAWCOM MARKETING SDN BHD', 108, 'NO. 22, GROUND FLOOR, JALAN MERDEKA\r\nBARAT, 96000 SIBU, SARAWAK.\r\nTEL: 016-8861997', '2019-07-10 14:29:09', 1, 1, 0, NULL, 3, NULL, '2019-07-05 06:24:44', '2019-07-05 06:29:09'),
(111, NULL, 'PGSBW0017', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 89, 'KONG GUAN SAUCE & FOOD MANUFACTURING', 109, 'CO. SDN BHD (21903-H)\r\n2463 SOLOK PERUSAHAAN SATU, PRAI\r\nINDUSTRIAL ESTATE,13600 PRAI, PROVINCE WELLESLEY.', 90, 'DAYET SDN BHD', 110, 'NO. 14 & 16 UPPER LANANG\r\nLORONG 29A, 96000 SIBU, SARAWAK.\r\nTEL: (084) 213-535  FAX: (084) 253-012', 90, 'DAYET SDN BHD', 110, 'NO. 14 & 16 UPPER LANANG\r\nLORONG 29A, 96000 SIBU, SARAWAK.\r\nTEL: (084) 213-535  FAX: (084) 253-012', '2019-07-10 14:41:25', 0, 1, 0, NULL, 3, NULL, '2019-07-05 06:41:25', '2019-07-05 06:41:25'),
(112, NULL, 'PGSBW0018', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 43, 'MULTI CHOICE (M) SDN BHD', 63, 'NO. 987, JALAN PERUSAHAAN, KAWASAN PERUSAHAAN,\r\n13600 PRAI, PULAU PINANG, MALAYSIA.', 93, 'HOO KIONG FARM', 113, 'NO. 7, LANE 19, JALAN WONG KING HUO,\r\n96000 SIBU, SARAWAK.\r\nTEL: 084-327 727  FAX: 084-340 727  ATTN: MR. WONG', 93, 'HOO KIONG FARM', 113, 'NO. 7, LANE 19, JALAN WONG KING HUO,\r\n96000 SIBU, SARAWAK.\r\nTEL: 084-327 727  FAX: 084-340 727  ATTN: MR. WONG', '2019-07-10 16:00:59', 0, 1, 0, NULL, 3, NULL, '2019-07-05 08:00:59', '2019-07-05 08:00:59'),
(114, NULL, 'PGSBW0019', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 101, 'HWA EIK TRADING (KEDAH) SDN BHD', 121, 'LOT 182, JALAN PINANG JAYA\r\nKEPALA BATS, 06200 ALOR SETAR\r\nKEDAH.', 102, 'MINSIANG TRADING SDN BHD', 122, 'NO. 21 & 23, DING LIK KONG 6,\r\n96000 SIBU, SARAWAK.\r\nTEL: 084-334682  FAX: 084-333682', 102, 'MINSIANG TRADING SDN BHD', 122, 'NO. 21 & 23, DING LIK KONG 6,\r\n96000 SIBU, SARAWAK.\r\nTEL: 084-334682  FAX: 084-333682', '2019-07-10 10:20:06', 1, 1, 0, NULL, 3, NULL, '2019-07-06 02:20:06', '2019-07-06 02:20:06'),
(115, NULL, 'PGSBW0020', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 10, 'WELLEY TIMBER INDUSTRIES SDN BHD', 22, 'PLOT A26, LORONG BAKAU,\r\nKAWASAN PERUSAHAAN PERABOT SUNGAI BAONG, \r\n14200 SUNGAI BAKAP S.P.S. PENANG', 10, 'KIONG SING VENEER SDN BHD', 22, 'NO. SYARIKAT: 1015334-M\r\nNO. 23, LORONG DING LIK KWONG 8,\r\n96000 SIBU, SARAWAK, MALAYSIA.', 10, 'KIONG SING VENEER SDN BHD', 22, 'NO. SYARIKAT: 1015334-M\r\nNO. 23, LORONG DING LIK KWONG 8,\r\n96000 SIBU, SARAWAK, MALAYSIA.', '2019-07-10 08:23:14', 0, 1, 0, NULL, 3, NULL, '2019-07-06 02:33:44', '2019-07-09 00:23:14'),
(116, NULL, 'PGBKI0019', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 10, 'ZHEN PENN SDN.BHD.', 22, 'LOT 3603, LORONG IKS SIMPANG AMPAT  C,\r\nTAMAN PERINDUSTRIAN IKS \r\nSIMPANG AMPAT ,  14100 SIMPANG AMPAT, PULAU PINANG,\r\nTEL NO.: 04-5681887 / 04-5681888\r\nFAX NO.: 04-5681889', 103, 'HUNG TAI ENTERPRISE (SABAH) SDN. BHD.', 123, 'LOT 53, LOK KAWI INDUSTRI ESTATE\r\nPHASE 2, 89600 KINARUT, SABAH', 103, 'HUNG TAI ENTERPRISE (SABAH) SDN. BHD.', 123, 'LOT 53, LOK KAWI INDUSTRI ESTATE\r\nPHASE 2, 89600 KINARUT, SABAH', '2019-07-10 11:29:59', 0, 1, 0, NULL, 3, NULL, '2019-07-06 03:04:10', '2019-07-09 03:29:59'),
(117, NULL, 'PGSBW0021', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 104, 'FIBRE STAR (M) SDN BHD', 124, 'NO. 3843, MK 11, LUAR DESA JAWI,\r\n14200 SUNGAI JAWI, S.P.S.\r\nPENANG, MALAYSIA.', 10, 'APPLE FURNITURE & DECORATE', 22, 'NO. 24, GROUND & 1ST FLOOR,\r\nLORONG SALIM 5A, 96000 SIBU, SARAWAK. (LOT 4155 BLOCK 18 SEDUAN LAND DISTRICT) \r\nTEL: 016-8705838  MR. LAU', 10, 'APPLE FURNITURE & DECORATE', 22, 'NO. 24, GROUND & 1ST FLOOR,\r\nLORONG SALIM 5A, 96000 SIBU, SARAWAK. (LOT 4155 BLOCK 18 SEDUAN LAND DISTRICT) \r\nTEL: 016-8705838  MR. LAU', '2019-07-10 14:14:20', 1, 1, 0, NULL, 3, NULL, '2019-07-06 03:16:40', '2019-07-09 06:14:20'),
(118, NULL, 'PGPKG0012', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 4, 'PORT KLANG MALAYSIA', 77, 'INFINITY BULK LOGISTICS SDN BHD (609736-H)', 97, 'NO.2, JALAN PUSAT PERNIAGAAN RAJA UDA 1,\r\nPUSAT PERNIAGAAN RAJA UDA, 12300 BUTTERWORTH,\r\nPENANG', 78, 'INFINITY LINES SDN BHD', 98, 'NO.2, JALAN KASUARINA 8/KS07 BANDAR BOTANIC\r\n41200 KLANG', 78, 'INFINITY LINES SDN BHD', 98, 'NO.2, JALAN KASUARINA 8/KS07 BANDAR BOTANIC\r\n41200 KLANG', '2019-07-10 11:50:36', 0, 1, 0, NULL, 3, NULL, '2019-07-06 03:50:36', '2019-07-06 03:50:36'),
(119, NULL, 'PGBKI0020', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 62, 'NIBONG TEBAL PAPER MILL SDN BHD', 82, '886 JALAN BANDAR BARU,\r\nSUNGAI KECIL,\r\n14300 NIBONG TEBAL, S.P.S. PENANG', 105, 'NIBONG TEBAL ENTERPRISE SDN. BHD.,', 125, 'LOT 3A, INDUSTRIAL ZONE 7 (IZ7)\r\nKKIP TIMUR JALAN SEPANGGAR\r\n88460 KOTA KINABALU, SABAH.', 105, 'NIBONG TEBAL ENTERPRISE SDN. BHD.,', 125, 'LOT 3A, INDUSTRIAL ZONE 7 (IZ7)\r\nKKIP TIMUR JALAN SEPANGGAR\r\n88460 KOTA KINABALU, SABAH.', '2019-07-10 14:50:50', 0, 1, 0, NULL, 3, NULL, '2019-07-06 04:08:03', '2019-07-09 06:50:50'),
(120, NULL, 'PGBKI0021', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 62, 'NIBONG TEBAL PAPER MILL SDN BHD', 82, '886 JALAN BANDAR BARU,\r\nSUNGAI KECIL,\r\n14300 NIBONG TEBAL,, S.P.S. PENANG', 105, 'NIBONG TEBAL ENTERPRISE SDN. BHD.,', 125, 'LOT 3A, INDUSTRIAL ZONE 7 (IZ7)\r\nKKIP TIMUR JALAN SEPANGGAR\r\n88460 KOTA KINABALU, SABAH.', 105, 'NIBONG TEBAL ENTERPRISE SDN. BHD.,', 125, 'LOT 3A, INDUSTRIAL ZONE 7 (IZ7)\r\nKKIP TIMUR JALAN SEPANGGAR\r\n88460 KOTA KINABALU, SABAH.', '2019-07-10 15:07:54', 0, 1, 0, NULL, 3, NULL, '2019-07-06 04:12:25', '2019-07-09 07:07:54'),
(121, NULL, 'PGSDK0008', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 7, 'SANDAKAN SABAH', 7, 'SANDAKAN SABAH', 30, 'NIBONG TEBAL ENTERPRISE SDN BHD', 50, 'LOT 7278, JALAN PERUSAHAAN 3, KAWASAN\r\nPERINDUSTRIAN PARIT BUNTAR,\r\n34200 PARIT BUNTAR, PERAK', 106, 'JAYA UNI\'ANG (S) SDN BHD C/O', 126, 'CHIA HANDLING & FORWARDING SERVICES SDN BHD\r\nLOT 116, TAMAN PERTAMA PHASE, P.O.BOX NO.1948,90722 SANDAKAN, SABAH', 106, 'JAYA UNI\'ANG (S) SDN BHD C/O', 126, 'CHIA HANDLING & FORWARDING SERVICES SDN BHD\r\nLOT 116, TAMAN PERTAMA PHASE, P.O.BOX NO.1948,90722 SANDAKAN, SABAH', '2019-07-10 14:59:08', 0, 1, 0, NULL, 3, NULL, '2019-07-06 04:17:25', '2019-07-09 06:59:08'),
(122, NULL, 'PGSDK0009', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 7, 'SANDAKAN SABAH', 7, 'SANDAKAN SABAH', 30, 'NIBONG TEBAL ENTERPRISE SDN BHD', 50, 'LOT 7278, JALAN PERUSAHAAN 3, KAWASAN\r\nPERINDUSTRIAN PARIT BUNTAR,\r\n34200 PARIT BUNTAR, PERAK', 106, 'JAYA UNI\'ANG (S) SDN BHD C/O', 126, 'CHIA HANDLING & FORWARDING\r\nSERVICES SDN BHD\r\nLOT 116, TAMAN PERTAMA PHASE, \r\nP.O. BOX NO. 1948,\r\n90722 SANDAKAN, SABAH.', 106, 'JAYA UNI\'ANG (S) SDN BHD C/O', 126, 'CHIA HANDLING & FORWARDING\r\nSERVICES SDN BHD\r\nLOT 116, TAMAN PERTAMA PHASE, \r\nP.O. BOX NO. 1948,\r\n90722 SANDAKAN, SABAH.', '2019-07-10 15:06:20', 0, 1, 0, NULL, 3, NULL, '2019-07-06 04:20:30', '2019-07-09 07:06:20'),
(123, NULL, 'PGMYY0006', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 9, 'MIRI SARAWAK', 9, 'MIRI SARAWAK', 94, 'PROFIT ACTION SDN BHD', 114, '6110, TINGKAT SELAMAT 9,\r\nKAMPUNG SELAMAT, 13300 TASEK GELUGOR,\r\nSEBERANG PERAI UTARA.', 95, 'WEY SING FURNITURE SDN.BHD', 115, 'LOT 1930-1933, BLOCK 5,\r\nK.B.L.D. JALAN MAIGOLD, SENADIN INDUSTRIAL AREA 98000 MIRI, SARAWAK', 95, 'WEY SING FURNITURE TRADING SDN.BHD.', 115, 'LOT 1930-1933, BLOCK 5,\r\nK.B.L.D. JALAN MAIGOLD, SENADIN INDUSTRIAL AREA 98000 MIRI, SARAWAK', '2019-07-10 08:04:47', 1, 1, 0, NULL, 3, NULL, '2019-07-06 04:24:47', '2019-07-09 00:04:47'),
(124, NULL, 'PGSBW0022', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 30, 'NIBONG TEBAL ENTERPRISE SDN BHD', 50, 'LOT 7278, JALAN PERUSAHAAN 3, KAWASAN\r\nPERINDUSTRIAN PARIT BUNTAR,\r\n34200 PARIT BUNTAR, PERAK', 23, 'MOH HENG CO. SDN BHD.', 41, 'NO.1,  LORONG LANANG BARAT 7,	\r\nTEL NO : 019-8228917 (MR.LEE)	\r\n96000 SIBU, SARAWAK', 10, 'MOH HENG CO. SDN BHD', 22, 'NO.1,  LORONG LANANG BARAT 7,	\r\nTEL NO : 019-8228917 (MR.LEE)	\r\n96000 SIBU, SARAWAK', '2019-07-10 14:26:42', 0, 1, 0, NULL, 3, NULL, '2019-07-06 04:30:15', '2019-07-09 06:26:42'),
(125, NULL, 'PGBTU0003', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 6, 'BINTULU SARAWAK', 6, 'BINTULU SARAWAK', 30, 'NIBONG TEBAL ENTERPRISE SDN BHD', 50, 'LOT 7278, JALAN PERUSAHAAN 3, KAWASAN\r\nPERINDUSTRIAN PARIT BUNTAR,\r\n34200 PARIT BUNTAR, PERAK', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, & 1029, BLOCK 26,\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', 10, 'MOH HENG CO. SDN BHD', 22, 'LOT 1028, & 1029, BLOCK 26,\r\nKIDURONG LIGHT INDUSTRIAL ESTATE,\r\n97000 BINTULU, SARAWAK, MALAYSIA', '2019-07-10 14:39:54', 0, 1, 0, NULL, 3, NULL, '2019-07-09 00:46:36', '2019-07-09 06:39:54'),
(126, NULL, 'PGBKI0022', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 10, 'EAGLE CHEMICALS  SDN. BHD. (378042-A)', 22, '51-13-C, MENARABHL, JALAN SULTAN AHMAD SHAH, 10050 GEORGETOWN,\r\nPENANG, MALAYSIA.\r\nTEL: 604-296 6688  FAX: 604-296 0906', 10, 'CND INDUSTRIES SDN BHD', 22, 'LOT 10, PHASE 1, BLOCK A,\r\nEXPORT ORIENTED INDUSTRIAL ZONE,\r\nKOTA KINABALU INDUSTRIAL PARK,\r\nOFF SEPANGAR BAY,\r\nKOTA KINABALU, SABAH.', 10, 'CND INDUSTRIES SDN BHD', 22, 'LOT 10, PHASE 1, BLOCK A,\r\nEXPORT ORIENTED INDUSTRIAL ZONE,\r\nKOTA KINABALU INDUSTRIAL PARK,\r\nOFF SEPANGAR BAY,\r\nKOTA KINABALU, SABAH.', '2019-07-10 15:49:00', 1, 1, 0, NULL, 3, NULL, '2019-07-09 00:56:33', '2019-07-09 07:49:00'),
(127, NULL, 'PGSBW0023', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 7, 'KASIHKU MKT SDN BHD', 30, 'NO. 298, LORONG PERAK 14,\r\nKAW. PERUSAHAAN MERGONG 11,\r\nSEBERANG JALAN PUTRA,\r\n05150 ALOR SETAR, KEDAH.', 102, 'MINSIANG TRADING SDN BHD', 122, 'NO. 21 & 23, DING LIK KONG 6,\r\n96000 SIBU, SARAWAK.\r\nTEL: 084-334682  FAX: 084-333682', 102, 'MINSIANG TRADING SDN BHD', 122, 'NO. 21 & 23, DING LIK KONG 6,\r\n96000 SIBU, SARAWAK.\r\nTEL: 084-334682  FAX: 084-333682', '2019-07-10 12:58:20', 1, 1, 0, NULL, 3, NULL, '2019-07-09 00:56:51', '2019-07-09 04:58:20'),
(128, NULL, 'PGSBW0024', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 35, 'NANYANG PHOSPHORUS FERTILIZERS SDN BHD', 55, 'NO.176, 1ST FLOOR, JALAN DATO BANDAR TUNGGAL,\r\n07000 SEREMBAN, NEGERI SEMBILAN, MALAYISA', 36, 'NANYANG PHOSPHORUS FERTILIZERS SDN. BHD', 56, 'LOT 1630, LORONG RENTAP 8,\r\nSARIKEI LIGHT INDUSTRIAL ESTATE, OFF\r\nJALAN RENTAP, SARIKEI, 96100 SARAWAK', 36, 'NANYANG PHOSPHORUS FERTILIZERS SDN. BHD', 56, 'LOT 1630, LORONG RENTAP 8,\r\nSARIKEI LIGHT INDUSTRIAL ESTATE, OFF\r\nJALAN RENTAP, SARIKEI, 96100 SARAWAK', '2019-07-10 13:07:09', 0, 1, 0, NULL, 3, NULL, '2019-07-09 00:59:03', '2019-07-09 05:07:09'),
(129, NULL, 'PGBKI0023', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 53, 'MSM PERLIS SDN BHD', 73, '(FORMERLY KNOWN AS KILANG GULA FELDA PERLIS SDN BHD)\r\nP.O.BOX 42, 01700 KANGAR, PERLIS, MALAYSIA.', 69, 'SYARIKAT KIAT HONG (SABAH) SDN BHD', 89, 'LOT 46, DBKK NO. 12,\r\nJALAN KILANG, KOLOMBONG,\r\n88450 KOTA KINABALU, SABAH.', 69, 'SYARIKAT KIAT HONG (SABAH) SDN BHD', 89, 'LOT 46, DBKK NO. 12,\r\nJALAN KILANG, KOLOMBONG,\r\n88450 KOTA KINABALU, SABAH.', '2019-07-10 13:19:55', 0, 1, 0, NULL, 3, NULL, '2019-07-09 01:02:19', '2019-07-09 05:19:55'),
(130, NULL, 'PGBKI0024', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 18, 'SILVERSTONE MARKETING SDN BHD', 36, 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN\r\nJALAN MENGGATAL / JALAN TURAN\r\n88450 KOTA KINABALU, SABAH. TEL:088-490026', 18, 'SILVERSTONE MARKETING SDN BHD', 36, 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN\r\nJALAN MENGGATAL / JALAN TURAN\r\n88450 KOTA KINABALU, SABAH. TEL:088-490026', 18, 'SILVERSTONE MARKETING SDN BHD', 36, 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN\r\nJALAN MENGGATAL / JALAN TURAN\r\n88450 KOTA KINABALU, SABAH. TEL:088-490026', '2019-07-10 09:03:17', 0, 1, 0, NULL, 3, NULL, '2019-07-09 01:03:17', '2019-07-09 01:03:17'),
(131, NULL, 'PGBKI0025', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 3, 'KOTA KINABALU SABAH', 3, 'KOTA KINABALU SABAH', 16, 'SILVERSTONE BERHAD', 34, 'LOT 5831, KAMUNTING INDUSTRIAL ESTATE II,\r\nP.O.BOX 2, 34600 KAMUNTING PERAK\r\nDARUL RIDZUAN, MALAYSIA', 18, 'SILVERSTONE MARKETING SDN BHD', 36, 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN\r\nJALAN MENGGATAL / JALAN TURAN\r\n88450 KOTA KINABALU, SABAH. TEL:088-490026', 18, 'SILVERSTONE MARKETING SDN BHD', 36, 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN\r\nJALAN MENGGATAL / JALAN TURAN\r\n88450 KOTA KINABALU, SABAH. TEL:088-490026', '2019-07-10 09:33:57', 0, 1, 0, NULL, 3, NULL, '2019-07-09 01:04:04', '2019-07-09 01:33:57'),
(132, NULL, 'PGTWU0006', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 8, 'TAWAU SABAH', 8, 'TAWAU SABAH', 10, 'FAS TRACK GLOBAL LOGISTICS SDN BHD', 22, '22-03, (2ND FLOOR) LINTANG BATU\r\nMAUNG DUA,\r\n11960 BAYAN LEPAS, PENANG, MALAYSIA.', 10, 'JACPHENIE SHIPPING & FREIGHT FORWARDING SDN BHD', 22, 'NO. 16, JALAN KAU SIN, OFF MILE 6, JALAN APAS, 91000 TAWAU, SABAH, MALAYSIA.\r\nTEL: 016-826 0563', 10, 'TONG GUAN PRODUCTS SDN BHD (014121-V)', 22, 'NO. 2195, TAMAN ANSON, MILES 1 3/4,\r\nJALAN KUHARA, 91000 TAWAU, SABAH.', '2019-07-10 12:51:47', 0, 1, 0, NULL, 3, NULL, '2019-07-09 01:04:59', '2019-07-09 04:51:47'),
(133, NULL, 'PGSBW0025', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 57, 'GUAN HONG PLASTIC INDUSTRIES SDN BHD', 77, '8394, KAWASAN PERINDUSTRIAN KAMPUNG TELUK,\r\nSUNGAI DUA, 13800 BUTTERWORTH, PENANG.', 10, 'HWA THONG LEATHER & PLASTIC INDUSTRIES SDN BHD', 22, 'LOT 560, UPPER LANANG ROAD, \r\nSEDC IND. ESTATE,\r\nP.O. BOX 483, 96007 SIBU, SARAWAK.\r\nTEL: 084-213604  FAX: 084-211928', 10, 'HWA THONG LEATHER & PLASTIC INDUSTRIES SDN BHD', 22, 'LOT 560, UPPER LANANG ROAD, \r\nSEDC IND. ESTATE,\r\nP.O. BOX 483, 96007 SIBU, SARAWAK.\r\nTEL: 084-213604  FAX: 084-211928', '2019-07-10 11:40:47', 1, 1, 0, NULL, 3, NULL, '2019-07-09 01:05:47', '2019-07-09 03:40:47'),
(134, NULL, 'PGSBW0026', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 10, 'HIGH Q PACK MARKETING SERVICES', 22, 'NO. 8, JALAN BUKIT KAYA 6,\r\nTAMAN BUKIT KAYA,\r\n01000 KANGAR, PERLIS, MALAYSIA.\r\nTEL: 0194411830', 10, 'PANSAR COMPANY SDN.BHD.', 22, 'WISMA PANSAR, 23-27, JALAN BENGKEL,\r\nP.O. BOX 319, 96007 SIBU, SARAWAK.\r\nTEL: 084-333 366  FAX: 084-314 555', 10, 'PANSAR COMPANY SDN.BHD.', 22, 'WISMA PANSAR, 23-27, JALAN BENGKEL,\r\nP.O. BOX 319, 96007 SIBU, SARAWAK.\r\nTEL: 084-333 366  FAX: 084-314 555', '2019-07-10 12:10:10', 0, 1, 0, NULL, 3, NULL, '2019-07-09 01:06:25', '2019-07-09 04:10:10'),
(135, NULL, 'PGSBW0027', 3, 'FCL', 9, 1, 'PENANG MALAYSIA', 5, 'SIBU SARAWAK', 5, 'SIBU SARAWAK', 26, 'REX TRADING SDN.BHD.', 45, 'PLOT 126, JALAN PERINDUSTRIAN BUKIT MINYAK 5,\r\n14100 SIMPANG AMPAT, SPT,\r\nPENANG, MALAYSIA', 102, 'MINSIANG TRADING SDN BHD', 122, 'NO. 21 & 23, DING LIK KONG 6,\r\n96000 SIBU, SARAWAK.\r\nTEL: 084-334682  FAX: 084-333682', 102, 'MINSIANG TRADING SDN BHD', 122, 'NO. 21 & 23, DING LIK KONG 6,\r\n96000 SIBU, SARAWAK.\r\nTEL: 084-334682  FAX: 084-333682', '2019-07-10 09:08:25', 0, 1, 0, NULL, 3, NULL, '2019-07-09 01:08:25', '2019-07-09 01:08:25');

-- --------------------------------------------------------

--
-- Table structure for table `bill_numbers`
--

DROP TABLE IF EXISTS `bill_numbers`;
CREATE TABLE IF NOT EXISTS `bill_numbers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `current_number` int(11) NOT NULL,
  `vessel_default` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bill_numbers`
--

INSERT INTO `bill_numbers` (`id`, `current_number`, `vessel_default`, `created_at`, `updated_at`) VALUES
(1, 1, 'Container Vessel', NULL, NULL),
(2, 2, 'General Cargo Vessel', NULL, '2019-04-05 09:43:19');

-- --------------------------------------------------------

--
-- Table structure for table `bl_formats`
--

DROP TABLE IF EXISTS `bl_formats`;
CREATE TABLE IF NOT EXISTS `bl_formats` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
CREATE TABLE IF NOT EXISTS `bookings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `booking_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bookingparty_id` int(10) UNSIGNED NOT NULL,
  `shipper_id` int(10) UNSIGNED NOT NULL,
  `voyage_id` int(10) UNSIGNED NOT NULL,
  `shipment_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpd_id` int(10) UNSIGNED DEFAULT NULL,
  `mvessel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `pol_id` int(10) UNSIGNED NOT NULL,
  `eta_pol` datetime NOT NULL,
  `pod_id` int(10) UNSIGNED NOT NULL,
  `eta_pod` datetime NOT NULL,
  `eta_fpd` datetime NOT NULL,
  `confirm` int(11) NOT NULL DEFAULT '0',
  `cargo_qty` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cargo_packing` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `haulge_request_date` datetime DEFAULT NULL,
  `empty_delivery_loc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cy_cutoff` datetime DEFAULT NULL,
  `cargo_desc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `temperature` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `uncode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cargo_nature` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `res_id` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bookings_bookingparty_id_foreign` (`bookingparty_id`),
  KEY `bookings_shipper_id_foreign` (`shipper_id`),
  KEY `bookings_voyage_id_foreign` (`voyage_id`),
  KEY `bookings_pol_id_foreign` (`pol_id`),
  KEY `bookings_fpd_id_foreign` (`pod_id`),
  KEY `bookings_res_id_foreign` (`res_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `date`, `booking_no`, `bookingparty_id`, `shipper_id`, `voyage_id`, `shipment_type`, `fpd_id`, `mvessel`, `status`, `pol_id`, `eta_pol`, `pod_id`, `eta_pod`, `eta_fpd`, `confirm`, `cargo_qty`, `cargo_packing`, `haulge_request_date`, `empty_delivery_loc`, `cy_cutoff`, `cargo_desc`, `temperature`, `weight`, `uncode`, `cargo_nature`, `remarks`, `res_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '2019-04-08 00:00:00', 'SYTM00001', 4, 4, 2, 'FCL', 5, NULL, 0, 1, '2019-04-17 00:00:00', 5, '2019-04-23 00:00:00', '2019-04-23 00:00:00', 0, '1,20\'GP;', '1 x 20\'GP', NULL, 'MUTUAL SPACE', '2019-04-17 16:27:01', 'PVC FLOORING', NULL, 1, NULL, 'General', 'REQUEST UPGRADED CONTAINER', NULL, '2019-04-08 08:39:46', '2019-04-08 08:24:02', '2019-04-08 08:39:46'),
(2, '2019-04-08 00:00:00', 'SYTM00002', 4, 4, 2, 'FCL', 5, '5', 3, 1, '2019-04-17 00:00:00', 4, '2019-04-18 00:00:00', '2019-04-27 00:00:00', 0, '1,20\'GP;', NULL, NULL, 'MUTUAL SPACE', '2019-04-17 16:27:01', 'PVC FLOORING', NULL, 1, NULL, 'General', 'REQUEST UPGRADED CONTAINER', NULL, '2019-06-04 06:23:13', '2019-04-08 08:39:46', '2019-06-04 06:23:13'),
(3, '2019-06-04 00:00:00', 'SYTM00003', 7, 7, 4, 'FCL', 2, NULL, 0, 1, '2019-06-08 00:00:00', 2, '2019-06-10 00:00:00', '2019-06-10 00:00:00', 0, NULL, NULL, NULL, NULL, '2019-06-08 00:00:00', NULL, NULL, NULL, NULL, 'General', NULL, NULL, NULL, '2019-06-04 07:20:22', '2019-06-04 07:20:22'),
(4, '2019-06-04 00:00:00', 'SYTM00004', 5, 5, 4, 'FCL', 2, NULL, 0, 1, '2019-06-08 00:00:00', 2, '2019-06-10 00:00:00', '2019-06-10 00:00:00', 0, NULL, NULL, NULL, NULL, '2019-06-08 00:00:00', NULL, NULL, NULL, NULL, 'General', NULL, NULL, NULL, '2019-06-04 07:20:43', '2019-06-04 07:20:43');

-- --------------------------------------------------------

--
-- Table structure for table `booking_amendments`
--

DROP TABLE IF EXISTS `booking_amendments`;
CREATE TABLE IF NOT EXISTS `booking_amendments` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `revision` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_amendments_booking_id_foreign` (`booking_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cargo_details`
--

DROP TABLE IF EXISTS `cargo_details`;
CREATE TABLE IF NOT EXISTS `cargo_details` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cargo_id` int(11) NOT NULL,
  `cgo` int(11) NOT NULL,
  `container_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seal_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=264 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cargo_details`
--

INSERT INTO `cargo_details` (`id`, `cargo_id`, `cgo`, `container_no`, `seal_no`, `type`, `size`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'SYLU 2434835', 'SYL 597679', 'GP', 20, '2019-04-08 08:08:55', '2019-04-08 08:08:55'),
(2, 3, 1, '123', '123', 'GP', 20, '2019-04-12 05:11:54', '2019-04-12 05:11:54'),
(3, 3, 1, '123', '123', 'GP', 20, '2019-04-12 05:11:54', '2019-04-12 05:11:54'),
(4, 4, 1, 'SYLU 2042433', 'SYL 597445', 'GP', 20, '2019-06-04 06:41:05', '2019-06-04 06:41:05'),
(5, 5, 1, 'SYLU 2429130', 'SYL 597907', 'GP', 20, '2019-06-04 07:07:50', '2019-06-04 07:07:50'),
(12, 9, 1, 'SYLU 2429130', 'SYL 597907', 'GP', 20, '2019-06-10 06:37:36', '2019-06-10 06:37:36'),
(7, 7, 1, 'SYLU 2039492', 'SYL 597920', 'GP', 20, '2019-06-07 06:37:19', '2019-06-07 06:37:19'),
(8, 8, 1, 'SYLU 5017579', 'SYL 597858', 'HC', 40, '2019-06-07 06:45:14', '2019-06-07 06:45:14'),
(9, 8, 1, 'SYLU 5005388', 'SYL 597851', 'HC', 40, '2019-06-07 06:45:14', '2019-06-07 06:45:14'),
(10, 8, 1, 'SYLU 5005650', 'SYL 597866', 'HC', 40, '2019-06-07 06:45:14', '2019-06-07 06:45:14'),
(11, 6, 1, 'SYLU 5011384', 'SYL 597860', 'GP', 40, '2019-06-07 06:49:57', '2019-06-07 06:49:57'),
(13, 10, 1, 'SYLU 5017579', 'SYL 597858', 'HC', 40, '2019-06-11 02:50:01', '2019-06-11 02:50:01'),
(14, 10, 1, 'SYLU 5005388', 'SYL 597851', 'HC', 40, '2019-06-11 02:50:01', '2019-06-11 02:50:01'),
(15, 10, 1, 'SYLU 5005650', 'SYL 597866', 'HC', 40, '2019-06-11 02:50:01', '2019-06-11 02:50:01'),
(16, 11, 1, 'SYLU2417890', 'SYL597525', 'GP', 20, '2019-06-27 05:05:22', '2019-06-27 05:05:22'),
(17, 12, 1, 'SYLU2417890', 'SYL597525', 'GP', 20, '2019-06-27 05:10:42', '2019-06-27 05:10:42'),
(18, 13, 1, 'SYLU2417890', 'SYL 597525', 'GP', 20, '2019-06-27 06:21:42', '2019-06-27 06:21:42'),
(26, 16, 1, 'SYLU2032836', 'SYL334268', 'GP', 20, '2019-07-01 03:53:28', '2019-07-01 03:53:28'),
(21, 15, 1, 'SYLU5009995', 'SYL597954', 'HC', 40, '2019-07-01 01:37:50', '2019-07-01 01:37:50'),
(22, 14, 1, 'SYLU5019381', 'SYL599274', 'HC', 40, '2019-07-01 01:58:10', '2019-07-01 01:58:10'),
(23, 14, 1, 'SYLU5020485', 'SYL599283', 'HC', 40, '2019-07-01 01:58:11', '2019-07-01 01:58:11'),
(24, 14, 1, 'SYLU5018683', 'SYL599295', 'HC', 40, '2019-07-01 01:58:11', '2019-07-01 01:58:11'),
(25, 14, 1, 'CAIU8763818', 'SYL599276', 'HC', 40, '2019-07-01 01:58:11', '2019-07-01 01:58:11'),
(27, 17, 1, 'SYLU2035521', 'SYL597543', 'GP', 20, '2019-07-01 04:08:41', '2019-07-01 04:08:41'),
(28, 18, 1, 'SYLU2411594', 'SYL597565', 'GP', 20, '2019-07-01 04:19:44', '2019-07-01 04:19:44'),
(29, 19, 1, 'SYLU2012803', 'SYL597551', 'GP', 20, '2019-07-01 04:26:48', '2019-07-01 04:26:48'),
(30, 20, 1, 'SYLU2022545', 'SYL597535', 'GP', 20, '2019-07-01 04:42:32', '2019-07-01 04:42:32'),
(31, 21, 1, 'SYLU2019346', 'SYL597586', 'GP', 20, '2019-07-01 06:42:04', '2019-07-01 06:42:04'),
(32, 22, 1, 'SYLU2019346', 'SYL597586', 'GP', 20, '2019-07-01 06:44:17', '2019-07-01 06:44:17'),
(33, 23, 1, 'SYLU2044550', 'SYL597533', 'GP', 20, '2019-07-01 06:51:36', '2019-07-01 06:51:36'),
(34, 24, 1, 'SYLU2424312', 'SYL597523', 'GP', 20, '2019-07-01 06:57:11', '2019-07-01 06:57:11'),
(35, 25, 1, 'SYLU 2024229', 'SYL 597697', 'GP', 20, '2019-07-01 07:18:05', '2019-07-01 07:18:05'),
(36, 26, 1, 'SYLU 2411300', 'SYL 599265', 'GP', 20, '2019-07-01 07:28:25', '2019-07-01 07:28:25'),
(37, 27, 1, 'SYLU 2024661', 'SYL 597538', 'GP', 20, '2019-07-01 07:37:19', '2019-07-01 07:37:19'),
(38, 28, 1, 'SYLU2041144', 'SYL597579', 'GP', 20, '2019-07-01 07:48:12', '2019-07-01 07:48:12'),
(39, 28, 1, 'SYLU2429989', 'SYL597526', 'GP', 20, '2019-07-01 07:48:12', '2019-07-01 07:48:12'),
(40, 28, 1, 'SYLU2431097', 'SYL597553', 'GP', 20, '2019-07-01 07:48:12', '2019-07-01 07:48:12'),
(41, 28, 1, 'SYLU2427820', 'SYL597994', 'GP', 20, '2019-07-01 07:48:12', '2019-07-01 07:48:12'),
(42, 29, 1, 'SYLU 2044673', 'SYL599284', 'GP', 20, '2019-07-01 08:17:35', '2019-07-01 08:17:35'),
(43, 29, 1, 'SYLU 2428180', 'SYL599298', 'GP', 20, '2019-07-01 08:17:35', '2019-07-01 08:17:35'),
(44, 29, 1, 'SYLU 2428093', 'SYL599287', 'GP', 20, '2019-07-01 08:17:35', '2019-07-01 08:17:35'),
(45, 29, 1, 'SYLU 2424966', 'SYL599267', 'GP', 20, '2019-07-01 08:17:35', '2019-07-01 08:17:35'),
(46, 29, 1, 'SYLU 2418140', 'SYL599299', 'GP', 20, '2019-07-01 08:17:35', '2019-07-01 08:17:35'),
(47, 29, 1, 'SYLU 2432426', 'SYL599268', 'GP', 20, '2019-07-01 08:17:35', '2019-07-01 08:17:35'),
(48, 29, 1, 'SYLU 2410746', 'SYL599270', 'GP', 20, '2019-07-01 08:17:35', '2019-07-01 08:17:35'),
(49, 30, 1, 'SYLU2429612', 'SYL597634', 'GP', 20, '2019-07-01 08:33:46', '2019-07-01 08:33:46'),
(50, 30, 1, 'SYLU2416024', 'SYL597522', 'GP', 20, '2019-07-01 08:33:46', '2019-07-01 08:33:46'),
(51, 30, 1, 'SYLU2412646', 'SYL599279', 'GP', 20, '2019-07-01 08:33:46', '2019-07-01 08:33:46'),
(52, 31, 1, 'SYLU2433314', 'SYL 597587', 'GP', 20, '2019-07-01 08:36:13', '2019-07-01 08:36:13'),
(53, 32, 1, 'SYLU5017692', 'SYL597645', 'HC', 40, '2019-07-01 08:43:31', '2019-07-01 08:43:31'),
(54, 33, 1, 'SYLU 2430439', 'SYL 597633', 'GP', 20, '2019-07-02 01:33:48', '2019-07-02 01:33:48'),
(55, 34, 1, 'SYLU 2419168', 'SYL 597643', 'GP', 20, '2019-07-02 01:50:04', '2019-07-02 01:50:04'),
(83, 50, 1, 'SYLU 2033719', 'SYL 597521', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(82, 49, 1, 'SYLU 2435153', 'SYL597531', 'GP', 20, '2019-07-03 06:38:38', '2019-07-03 06:38:38'),
(69, 43, 1, 'TEMU 1526103', 'INTACT', 'GP', 20, '2019-07-02 07:28:57', '2019-07-02 07:28:57'),
(59, 37, 1, 'SYLU5015982', 'SYL597583', 'HC', 40, '2019-07-02 02:52:28', '2019-07-02 02:52:28'),
(60, 38, 1, 'SYLU2038541', 'SYL597582', 'GP', 20, '2019-07-02 03:51:21', '2019-07-02 03:51:21'),
(61, 39, 1, 'SYLU5004479', 'SYL569710', 'HC', 40, '2019-07-02 04:00:02', '2019-07-02 04:00:02'),
(62, 40, 1, 'SYLU5017835', 'SYL597649', 'HC', 40, '2019-07-02 04:06:16', '2019-07-02 04:06:16'),
(63, 41, 1, 'SYLU2431857', 'SYL599272', 'GP', 20, '2019-07-02 04:08:38', '2019-07-02 04:08:38'),
(64, 41, 1, 'SYLU2424539', 'SYL599292', 'GP', 20, '2019-07-02 04:08:38', '2019-07-02 04:08:38'),
(65, 41, 1, 'SYLU2031233', 'SYL599291', 'GP', 20, '2019-07-02 04:08:38', '2019-07-02 04:08:38'),
(66, 41, 1, 'SYLU2428236', 'SYL569703', 'GP', 20, '2019-07-02 04:08:38', '2019-07-02 04:08:38'),
(67, 42, 1, 'SYLU5013784', 'SYL597571', 'HC', 40, '2019-07-02 04:15:03', '2019-07-02 04:15:03'),
(68, 36, 1, 'SYLU 5014414', 'SYL 597948', 'HC', 40, '2019-07-02 05:37:29', '2019-07-02 05:37:29'),
(74, 44, 1, 'TGBU 2641992', 'INTACT', 'GP', 20, '2019-07-02 07:33:40', '2019-07-02 07:33:40'),
(70, 43, 1, 'TEMU 1629626', 'INTACT', 'GP', 20, '2019-07-02 07:28:57', '2019-07-02 07:28:57'),
(71, 43, 1, 'TGBU 2485781', 'INTACT', 'GP', 20, '2019-07-02 07:28:57', '2019-07-02 07:28:57'),
(72, 43, 1, 'TLLU 2537192', 'INTACT', 'GP', 20, '2019-07-02 07:28:57', '2019-07-02 07:28:57'),
(73, 43, 1, 'TLLU 2581425', 'INTACT', 'GP', 20, '2019-07-02 07:28:57', '2019-07-02 07:28:57'),
(75, 44, 1, 'IAAU 2740741', 'INTACT', 'GP', 20, '2019-07-02 07:33:40', '2019-07-02 07:33:40'),
(76, 45, 1, 'SYLU 2029597', 'SYL599269', 'GP', 20, '2019-07-02 08:00:26', '2019-07-02 08:00:26'),
(77, 46, 1, 'SYLU 2012022', 'SYL 597585', 'GP', 20, '2019-07-02 08:07:10', '2019-07-02 08:07:10'),
(78, 47, 1, 'CAIU 8838780', 'SYL 597566', 'HC', 40, '2019-07-03 01:37:34', '2019-07-03 01:37:34'),
(79, 48, 1, 'CAIU 8854590', 'SYL 597527', 'HC', 40, '2019-07-03 01:46:53', '2019-07-03 01:46:53'),
(80, 35, 1, 'SYLU 5016926', 'SYL 597646', 'HC', 40, '2019-07-03 05:02:00', '2019-07-03 05:02:00'),
(81, 35, 1, 'CAIU 8853228', 'SYL597548', 'HC', 40, '2019-07-03 05:02:00', '2019-07-03 05:02:00'),
(84, 50, 1, 'SYLU 2412630', 'SYL 597560', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(85, 50, 1, 'SYLU 2424436', 'SYL 597576', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(86, 50, 1, 'SYLU 2043297', 'SYL 599257', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(87, 50, 1, 'SYLU 2022843', 'SYL 597589', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(88, 50, 1, 'SYLU 2038480', 'SYL 597641', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(89, 50, 1, 'SYLU 2416976', 'SYL 597847', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(90, 50, 1, 'SYLU 2016331', 'SYL 599312', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(91, 50, 1, 'SYLU 2412517', 'SYL 599305', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(92, 50, 1, 'SYLU 2407681', 'SYL 599290', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(93, 50, 1, 'SYLU 2032435', 'SYL 599288', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(94, 50, 1, 'SYLU 2406387', 'SYL 599300', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(95, 50, 1, 'SYLU 2022817', 'SYL 599281', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(96, 50, 1, 'SYLU 2018313', 'SYL 599271', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(97, 50, 1, 'SYLU 2419189', 'SYL 599332', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(98, 50, 1, 'SYLU 2412440', 'SYL 599296', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(99, 50, 1, 'SYLU 2040066', 'SYL 599273', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(100, 50, 1, 'SYLU 2418393', 'SYL 597606', 'GP', 20, '2019-07-03 07:01:18', '2019-07-03 07:01:18'),
(101, 51, 1, 'SYLU5009320', 'SYL599263', 'HC', 40, '2019-07-03 07:14:29', '2019-07-03 07:14:29'),
(102, 52, 1, 'SYLU5023720', 'SYL599323', 'HC', 40, '2019-07-03 07:22:11', '2019-07-03 07:22:11'),
(103, 53, 1, 'SYLU 2434706', 'SYL599259', 'GP', 20, '2019-07-03 08:24:08', '2019-07-03 08:24:08'),
(104, 54, 1, 'SYLU 2033956', 'SYL599351', 'GP', 20, '2019-07-04 02:08:40', '2019-07-04 02:08:40'),
(105, 55, 1, 'SYLU2028585', 'SYL 599330', 'GP', 20, '2019-07-04 03:45:20', '2019-07-04 03:45:20'),
(106, 56, 1, 'SYLU2424302', 'SYL599353', 'GP', 20, '2019-07-04 04:09:24', '2019-07-04 04:09:24'),
(107, 57, 1, 'SYLU 2032306', 'SYL599280', 'GP', 20, '2019-07-04 04:23:40', '2019-07-04 04:23:40'),
(108, 58, 1, 'SYLU 2040960', 'SYL599286', 'GP', 20, '2019-07-04 04:41:12', '2019-07-04 04:41:12'),
(109, 58, 1, 'SYLU 2010360', 'SYL599256', 'GP', 20, '2019-07-04 04:41:12', '2019-07-04 04:41:12'),
(110, 58, 1, 'SYLU 2043379', 'SYL599252', 'GP', 20, '2019-07-04 04:41:12', '2019-07-04 04:41:12'),
(111, 59, 1, 'SYLU 2036174', 'SYL599251', 'GP', 20, '2019-07-04 06:36:34', '2019-07-04 06:36:34'),
(112, 59, 1, 'SYLU 2410068', 'SYL599293', 'GP', 20, '2019-07-04 06:36:34', '2019-07-04 06:36:34'),
(113, 59, 1, 'SYLU 2026176', 'SYL599297', 'GP', 20, '2019-07-04 06:36:34', '2019-07-04 06:36:34'),
(114, 59, 1, 'SYLU 2409302', 'SYL599294', 'GP', 20, '2019-07-04 06:36:34', '2019-07-04 06:36:34'),
(115, 59, 1, 'SYLU 2036507', 'SYL599277', 'GP', 20, '2019-07-04 06:36:34', '2019-07-04 06:36:34'),
(116, 59, 1, 'SYLU 2032203', 'SYL599289', 'GP', 20, '2019-07-04 06:36:34', '2019-07-04 06:36:34'),
(117, 60, 1, 'SYLU5007708', 'SYL599376', 'HC', 40, '2019-07-04 06:51:25', '2019-07-04 06:51:25'),
(118, 61, 1, 'SYLU5013803', 'SYL599313', 'HC', 40, '2019-07-04 07:10:23', '2019-07-04 07:10:23'),
(119, 62, 1, 'SYLU5010222', 'SYL599261', 'HC', 40, '2019-07-04 07:29:37', '2019-07-04 07:29:37'),
(120, 63, 1, 'SYLU 2427857', 'SYL599316', 'GP', 20, '2019-07-04 08:00:10', '2019-07-04 08:00:10'),
(121, 64, 1, 'SYLU5016079', 'SYL599326', 'HC', 40, '2019-07-04 08:35:07', '2019-07-04 08:35:07'),
(122, 65, 1, 'SYLU 2034757', 'SYL 599491', 'GP', 20, '2019-07-05 00:57:56', '2019-07-05 00:57:56'),
(123, 66, 1, 'SYLU2436519', 'SYL599350', 'GP', 20, '2019-07-05 01:23:07', '2019-07-05 01:23:07'),
(124, 67, 1, 'SYLU5013465', 'SYL569732', 'HC', 40, '2019-07-05 01:39:35', '2019-07-05 01:39:35'),
(125, 68, 1, 'SYLU 5012020', 'SYL 569707', 'HC', 40, '2019-07-05 02:01:30', '2019-07-05 02:01:30'),
(126, 69, 1, 'SYLU2031716', 'SYL599264', 'GP', 20, '2019-07-05 02:17:35', '2019-07-05 02:17:35'),
(127, 70, 1, 'SYLU2434748', 'SYL 599345', 'GP', 20, '2019-07-05 02:54:37', '2019-07-05 02:54:37'),
(128, 70, 1, 'SYLU2426850', 'SYL 299369', 'GP', 20, '2019-07-05 02:54:37', '2019-07-05 02:54:37'),
(129, 70, 1, 'SYLU2413302', 'SYL 599355', 'GP', 20, '2019-07-05 02:54:37', '2019-07-05 02:54:37'),
(130, 70, 1, 'SYLU2430070', 'SYL 599324', 'GP', 20, '2019-07-05 02:54:37', '2019-07-05 02:54:37'),
(131, 70, 1, 'SYLU2023916', 'SYL 599329', 'GP', 20, '2019-07-05 02:54:37', '2019-07-05 02:54:37'),
(132, 70, 1, 'SYLU2044755', 'SYL 599356', 'GP', 20, '2019-07-05 02:54:37', '2019-07-05 02:54:37'),
(133, 70, 1, 'SYLU2012423', 'SYL 599306', 'GP', 20, '2019-07-05 02:54:37', '2019-07-05 02:54:37'),
(134, 70, 1, 'SYLU2044884', 'SYL 599344', 'GP', 20, '2019-07-05 02:54:37', '2019-07-05 02:54:37'),
(135, 70, 1, 'SYLU2421294', 'SYL 599352', 'GP', 20, '2019-07-05 02:54:37', '2019-07-05 02:54:37'),
(136, 70, 1, 'SYLU2433248', 'SYL 599347', 'GP', 20, '2019-07-05 02:54:37', '2019-07-05 02:54:37'),
(137, 71, 1, 'SYLU8000097', 'SYL599456', 'HRF', 40, '2019-07-05 03:17:21', '2019-07-05 03:17:21'),
(138, 72, 1, 'SYLU5009763', 'SYL569740', 'HC', 40, '2019-07-05 03:34:28', '2019-07-05 03:34:28'),
(139, 73, 1, 'SYLU2021410', 'SYL599349', 'GP', 20, '2019-07-05 03:48:02', '2019-07-05 03:48:02'),
(140, 74, 1, 'SYLU5007708', 'SYL599376', 'HC', 40, '2019-07-05 04:04:15', '2019-07-05 04:04:15'),
(141, 75, 1, 'SYLU 2421083', 'SYL599410', 'GP', 20, '2019-07-05 04:54:04', '2019-07-05 04:54:04'),
(142, 76, 1, 'UESU 2256094', '20\'GP', 'GP', 20, '2019-07-05 05:31:50', '2019-07-05 05:31:50'),
(143, 77, 1, 'FCIU 6614407', '20\'GP', 'GP', 20, '2019-07-05 05:37:00', '2019-07-05 05:37:00'),
(144, 77, 1, 'OCGU 8054371', '40\'HC', 'HC', 40, '2019-07-05 05:37:00', '2019-07-05 05:37:00'),
(145, 77, 1, 'YMMU 6091176', '40\'HC', 'HC', 40, '2019-07-05 05:37:00', '2019-07-05 05:37:00'),
(146, 78, 1, 'BSIU 9951991', '40\'HC', 'HC', 40, '2019-07-05 05:41:56', '2019-07-05 05:41:56'),
(147, 78, 1, 'CAIU 4912364', '40\'HC', 'HC', 40, '2019-07-05 05:41:56', '2019-07-05 05:41:56'),
(148, 78, 1, 'CAIU 7130137', '40\'HC', 'HC', 40, '2019-07-05 05:41:56', '2019-07-05 05:41:56'),
(149, 78, 1, 'GAOU 6203808', '40\'HC', 'HC', 40, '2019-07-05 05:41:56', '2019-07-05 05:41:56'),
(150, 78, 1, 'HMMU 6138173', '40\'HC', 'HC', 40, '2019-07-05 05:41:56', '2019-07-05 05:41:56'),
(151, 78, 1, 'HMMU 6183207', '40\'HC', 'HC', 40, '2019-07-05 05:41:56', '2019-07-05 05:41:56'),
(152, 78, 1, 'HMMU 6197406', '40\'HC', 'HC', 40, '2019-07-05 05:41:56', '2019-07-05 05:41:56'),
(153, 78, 1, 'KOCU 4005500', '40\'HC', 'HC', 40, '2019-07-05 05:41:56', '2019-07-05 05:41:56'),
(154, 78, 1, 'KOCU4041730', '40\'HC', 'HC', 40, '2019-07-05 05:41:56', '2019-07-05 05:41:56'),
(155, 78, 1, 'TCKU 6097985', '40\'HC', 'HC', 40, '2019-07-05 05:41:56', '2019-07-05 05:41:56'),
(156, 78, 1, 'TCNU 1699541', '40\'HC', 'HC', 40, '2019-07-05 05:41:56', '2019-07-05 05:41:56'),
(199, 87, 1, 'SYLU5009697', 'SYL599346', 'HC', 40, '2019-07-05 07:08:16', '2019-07-05 07:08:16'),
(198, 87, 1, 'SYLU5007288', 'SYL599340', 'HC', 40, '2019-07-05 07:08:16', '2019-07-05 07:08:16'),
(197, 86, 1, 'SYLU2426254', 'SYL599358', 'GP', 20, '2019-07-05 06:46:59', '2019-07-05 06:46:59'),
(161, 79, 1, 'ICKU 3001700', '20\'GP', 'GP', 20, '2019-07-05 05:46:26', '2019-07-05 05:46:26'),
(162, 79, 1, 'ISMU 2007958', '20\'GP', 'GP', 20, '2019-07-05 05:46:26', '2019-07-05 05:46:26'),
(163, 79, 1, 'ISMU 2008297', '20\'GP', 'GP', 20, '2019-07-05 05:46:26', '2019-07-05 05:46:26'),
(164, 79, 1, 'ISMU 2010674', '20\'GP', 'GP', 20, '2019-07-05 05:46:26', '2019-07-05 05:46:26'),
(165, 80, 1, 'BSIU 2810950', '20\'GP', 'GP', 20, '2019-07-05 05:50:25', '2019-07-05 05:50:25'),
(166, 80, 1, 'IAAU 4683297', '40\'GP', 'GP', 40, '2019-07-05 05:50:25', '2019-07-05 05:50:25'),
(167, 80, 1, 'IAAU 4684483', '40\'GP', 'GP', 40, '2019-07-05 05:50:25', '2019-07-05 05:50:25'),
(168, 80, 1, 'IAAU 4685999', '40\'GP', 'GP', 40, '2019-07-05 05:50:25', '2019-07-05 05:50:25'),
(169, 80, 1, 'DRYU 9029841', '40\'HC', 'HC', 40, '2019-07-05 05:50:25', '2019-07-05 05:50:25'),
(170, 80, 1, 'IAAU 1704190', '40\'HC', 'HC', 40, '2019-07-05 05:50:25', '2019-07-05 05:50:25'),
(171, 80, 1, 'IAAU 1718635', '40\'HC', 'HC', 40, '2019-07-05 05:50:25', '2019-07-05 05:50:25'),
(172, 80, 1, 'IAAU 1752990', '40\'HC', 'HC', 40, '2019-07-05 05:50:25', '2019-07-05 05:50:25'),
(173, 80, 1, 'IAAU 1753560', '40\'HC', 'HC', 40, '2019-07-05 05:50:25', '2019-07-05 05:50:25'),
(174, 80, 1, 'SEGU 6622572', '40\'HC', 'HC', 40, '2019-07-05 05:50:25', '2019-07-05 05:50:25'),
(175, 80, 1, 'TCKU 6574115', '40\'HC', 'HC', 40, '2019-07-05 05:50:25', '2019-07-05 05:50:25'),
(176, 81, 1, 'SKLU 1408956', '20\'GP', 'GP', 20, '2019-07-05 05:54:15', '2019-07-05 05:54:15'),
(177, 81, 1, 'TEMU 2315213', '20\'GP', 'GP', 20, '2019-07-05 05:54:15', '2019-07-05 05:54:15'),
(178, 82, 1, 'OOLU 0178509', '20\'GP', 'GP', 20, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(179, 82, 1, 'OOLU 0314694', '20\'GP', 'GP', 20, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(180, 82, 1, 'OOLU 0391561', '20\'GP', 'GP', 20, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(181, 82, 1, 'OOLU 0562052', '20\'GP', 'GP', 20, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(182, 82, 1, 'OOLU 0679183', '20\'GP', 'GP', 20, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(183, 82, 1, 'OOLU 1925173', '20\'GP', 'GP', 20, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(184, 82, 1, 'OOCU 7005474', '40\'HC', 'HC', 40, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(185, 82, 1, 'OOCU 7688719', '40\'HC', 'HC', 40, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(186, 82, 1, 'OOCU 7762185', '40\'HC', 'HC', 40, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(187, 82, 1, 'OOLU 81089525', '40\'HC', 'HC', 40, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(188, 82, 1, 'OOLU 8164970', '40\'HC', 'HC', 40, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(189, 82, 1, 'OOLU 8577969', '40\'HC', 'HC', 40, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(190, 82, 1, 'OOLU 9320234', '40\'HC', 'HC', 40, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(191, 82, 1, 'OOLU 9945540', '40\'HC', 'HC', 40, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(192, 82, 1, 'OOLU 9998556', '40\'HC', 'HC', 40, '2019-07-05 06:02:01', '2019-07-05 06:02:01'),
(193, 83, 1, 'BSIU 2619898', '20\'GP', 'GP', 20, '2019-07-05 06:18:43', '2019-07-05 06:18:43'),
(194, 83, 1, 'MXCU 0056780', '20\'GP', 'GP', 20, '2019-07-05 06:18:43', '2019-07-05 06:18:43'),
(195, 84, 1, 'VMLU 3503118', '20\'GP', 'GP', 20, '2019-07-05 06:20:19', '2019-07-05 06:20:19'),
(196, 85, 1, 'SYLU2421843', 'SYL599389', 'GP', 20, '2019-07-05 06:27:38', '2019-07-05 06:27:38'),
(200, 87, 1, 'SYLU5017408', 'SYL599278', 'HC', 40, '2019-07-05 07:08:16', '2019-07-05 07:08:16'),
(201, 88, 1, 'SYLU5013341', 'SYL599310', 'HC', 40, '2019-07-05 07:19:51', '2019-07-05 07:19:51'),
(202, 88, 1, 'SYLU5013995', 'SYL599320', 'HC', 40, '2019-07-05 07:19:51', '2019-07-05 07:19:51'),
(203, 89, 1, 'SYLU 2432010', 'SYL 599498', 'GP', 20, '2019-07-05 07:32:30', '2019-07-05 07:32:30'),
(204, 90, 1, 'SYLU5011316', 'SYL599307', 'HC', 40, '2019-07-05 08:09:19', '2019-07-05 08:09:19'),
(205, 91, 1, 'SYLU 5001382', 'SYL 599337', 'HC', 40, '2019-07-05 08:19:34', '2019-07-05 08:19:34'),
(206, 92, 1, 'SYLU 2017106', 'SYL 569733', 'GP', 20, '2019-07-05 08:29:35', '2019-07-05 08:29:35'),
(207, 93, 1, 'SYLU 2417910', 'SYL 569734', 'GP', 20, '2019-07-05 08:37:40', '2019-07-05 08:37:40'),
(208, 94, 1, 'SYLU 2421083', 'SYL 599338', 'GP', 20, '2019-07-05 08:43:07', '2019-07-05 08:43:07'),
(209, 95, 1, 'SYLU 2411697', 'SYL 597622', 'GP', 20, '2019-07-05 08:49:51', '2019-07-05 08:49:51'),
(210, 96, 1, 'SYLU 5012667', 'SYL 569709', 'HC', 40, '2019-07-06 01:25:14', '2019-07-06 01:25:14'),
(211, 97, 1, 'SYLU 5010941', 'SYL599318', 'HC', 40, '2019-07-06 01:35:36', '2019-07-06 01:35:36'),
(212, 97, 1, 'SYLU 5003852', 'SYL599325', 'HC', 40, '2019-07-06 01:35:36', '2019-07-06 01:35:36'),
(213, 98, 1, 'SYLU 2035753', 'SYL 599258', 'GP', 20, '2019-07-06 01:40:10', '2019-07-06 01:40:10'),
(214, 99, 1, 'SYLU 5003343', 'SYL 569731', 'HC', 40, '2019-07-06 01:43:12', '2019-07-06 01:43:12'),
(215, 100, 1, 'SYLU5017270', 'SYL599363', 'HC', 40, '2019-07-06 01:46:29', '2019-07-06 01:46:29'),
(216, 100, 1, 'SYLU5012651', 'SYL599364', 'HC', 40, '2019-07-06 01:46:29', '2019-07-06 01:46:29'),
(217, 101, 1, 'SYLU2405400', 'SYL569649', 'GP', 20, '2019-07-06 02:12:01', '2019-07-06 02:12:01'),
(218, 102, 1, 'SYLU 5008200', 'SYL 599260', 'HC', 40, '2019-07-06 02:22:58', '2019-07-06 02:22:58'),
(219, 103, 1, 'SYLU4003781', 'SYL599327', 'GP', 40, '2019-07-06 02:37:54', '2019-07-06 02:37:54'),
(220, 103, 1, 'SYLU4003565', 'SYL599319', 'GP', 40, '2019-07-06 02:37:54', '2019-07-06 02:37:54'),
(221, 104, 1, 'SYLU5015508', '599393', 'HC', 40, '2019-07-06 03:06:07', '2019-07-06 03:06:07'),
(222, 105, 1, 'SYLU2043764', 'SYL 599309', 'GP', 20, '2019-07-06 03:18:53', '2019-07-06 03:18:53'),
(223, 106, 1, 'ICKU 3004191', '20G\'P', 'GP', 20, '2019-07-06 03:52:10', '2019-07-06 03:52:10'),
(224, 106, 1, 'ISMU 2927180', '20\'GP', 'GP', 20, '2019-07-06 03:52:10', '2019-07-06 03:52:10'),
(225, 106, 1, 'ISMU 2927787', '20\'GP', 'GP', 20, '2019-07-06 03:52:10', '2019-07-06 03:52:10'),
(226, 106, 1, 'ISMU 3005564', '20\'GP', 'GP', 20, '2019-07-06 03:52:10', '2019-07-06 03:52:10'),
(227, 106, 1, 'ISMU 9001530', '40\'HC', 'HC', 40, '2019-07-06 03:52:11', '2019-07-06 03:52:11'),
(228, 107, 1, 'SYLU5001063', '64912', 'HC', 40, '2019-07-06 04:14:07', '2019-07-06 04:14:07'),
(229, 108, 1, 'SYLU 4003694', 'SYL 599282', 'GP', 40, '2019-07-06 04:14:36', '2019-07-06 04:14:36'),
(230, 109, 1, 'SYLU 5003050', 'SYL 599321', 'HC', 40, '2019-07-06 04:19:09', '2019-07-06 04:19:09'),
(231, 110, 1, 'SYLU 5014559', 'SYL 599253', 'HC', 40, '2019-07-06 04:22:48', '2019-07-06 04:22:48'),
(232, 111, 1, 'CAIU 8757271', 'SYL 599390', 'HC', 40, '2019-07-06 04:25:58', '2019-07-06 04:25:58'),
(233, 112, 1, 'SYLU 5012570', 'SYL 599379', 'HC', 40, '2019-07-06 04:33:45', '2019-07-06 04:33:45'),
(234, 112, 1, 'SYLU 5013043', 'SYL 599303', 'HC', 40, '2019-07-06 04:33:45', '2019-07-06 04:33:45'),
(235, 112, 1, 'SYLU 5017985', 'SYL 599396', 'HC', 40, '2019-07-06 04:33:45', '2019-07-06 04:33:45'),
(236, 113, 1, 'SYLU 5004416', 'SYL 599342', 'HC', 40, '2019-07-09 00:48:51', '2019-07-09 00:48:51'),
(237, 114, 1, 'SYLU 2406509', 'SYL 599383', 'GP', 20, '2019-07-09 01:00:41', '2019-07-09 01:00:41'),
(238, 115, 1, 'SYLU2420976', 'SYL599460', 'GP', 20, '2019-07-09 01:11:25', '2019-07-09 01:11:25'),
(239, 115, 1, 'SYLU2035964', 'SYL599463', 'GP', 20, '2019-07-09 01:11:25', '2019-07-09 01:11:25'),
(240, 115, 1, 'SYLU2018695', 'SYL599474', 'GP', 20, '2019-07-09 01:11:25', '2019-07-09 01:11:25'),
(241, 116, 1, 'SYLU4002492', 'SYL597629', 'GP', 40, '2019-07-09 01:36:20', '2019-07-09 01:36:20'),
(242, 117, 1, 'SYLU4005927', 'SYL597601', 'GP', 40, '2019-07-09 01:45:41', '2019-07-09 01:45:41'),
(243, 117, 1, 'SYLU4004155', 'SYL597899', 'GP', 40, '2019-07-09 01:45:41', '2019-07-09 01:45:41'),
(244, 117, 1, 'SYLU4005275', 'SYL599285', 'GP', 40, '2019-07-09 01:45:41', '2019-07-09 01:45:41'),
(245, 118, 1, 'ICKU 3004191', '20\'GP', 'GP', 20, '2019-07-09 01:46:20', '2019-07-09 01:46:20'),
(246, 118, 1, 'ISMU 2927180', '20\'GP', 'GP', 20, '2019-07-09 01:46:20', '2019-07-09 01:46:20'),
(247, 118, 1, 'ISMU 2927787', '20\'GP', 'GP', 20, '2019-07-09 01:46:20', '2019-07-09 01:46:20'),
(248, 118, 1, 'ISMU 3005564', '20\'GP', 'GP', 20, '2019-07-09 01:46:20', '2019-07-09 01:46:20'),
(249, 118, 1, 'ISMU 9001530', '40\'HC', 'HC', 40, '2019-07-09 01:46:20', '2019-07-09 01:46:20'),
(250, 119, 1, 'SYLU5012400', 'SYL599359', 'HC', 40, '2019-07-09 03:44:17', '2019-07-09 03:44:17'),
(251, 120, 1, 'SYLU 2425089', 'SYL599385', 'GP', 20, '2019-07-09 04:02:03', '2019-07-09 04:02:03'),
(252, 121, 1, 'SYLU2420636', 'SYL372902', 'GP', 20, '2019-07-09 04:20:12', '2019-07-09 04:20:12'),
(253, 122, 1, 'SYLU 2408570', 'SYL599262', 'GP', 20, '2019-07-09 05:07:33', '2019-07-09 05:07:33'),
(254, 122, 1, 'SYLU 2433818', 'SYL599275', 'GP', 20, '2019-07-09 05:07:33', '2019-07-09 05:07:33'),
(255, 122, 1, 'SYLU 2424190', 'SYL599255', 'GP', 20, '2019-07-09 05:07:33', '2019-07-09 05:07:33'),
(256, 122, 1, 'SYLU 2436458', 'SYL599343', 'GP', 20, '2019-07-09 05:07:33', '2019-07-09 05:07:33'),
(257, 122, 1, 'SYLU 2424816', 'SYL599365', 'GP', 20, '2019-07-09 05:07:33', '2019-07-09 05:07:33'),
(258, 122, 1, 'SYLU 2025170', 'SYL599302', 'GP', 20, '2019-07-09 05:07:33', '2019-07-09 05:07:33'),
(259, 122, 1, 'SYLU 2040451', 'SYL599335', 'GP', 20, '2019-07-09 05:07:33', '2019-07-09 05:07:33'),
(260, 122, 1, 'SYLU 2417680', 'SYL599368', 'GP', 20, '2019-07-09 05:07:33', '2019-07-09 05:07:33'),
(261, 123, 1, 'SYLU 2431836', 'SYL 599402', 'GP', 20, '2019-07-09 05:08:39', '2019-07-09 05:08:39'),
(262, 123, 1, 'SYLU 2011900', 'SYL 599473', 'GP', 20, '2019-07-09 05:08:39', '2019-07-09 05:08:39'),
(263, 124, 1, 'SYLU 2013965', 'SYL 599435', 'GP', 20, '2019-07-09 07:52:24', '2019-07-09 07:52:24');

-- --------------------------------------------------------

--
-- Table structure for table `charges`
--

DROP TABLE IF EXISTS `charges`;
CREATE TABLE IF NOT EXISTS `charges` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `charges`
--

INSERT INTO `charges` (`id`, `code`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'OFT', 'OCEAN FREIGHT', '2019-06-04 06:22:48', '2019-04-11 06:25:38', '2019-06-04 06:22:48'),
(2, 'BAF', 'BUNKER ADJUSTMENT CHARGES', '2019-06-07 00:35:56', '2019-06-04 06:42:15', '2019-06-07 00:35:56'),
(3, 'OF', 'OCEAN FREIGHT', NULL, '2019-06-04 06:43:36', '2019-06-04 06:43:42'),
(4, 'THC', 'TERMINAL HANDLING CHARGES', NULL, '2019-06-04 06:44:08', '2019-06-04 06:44:08'),
(5, 'DTHC', 'DESTINATION THC', NULL, '2019-06-04 06:44:41', '2019-06-04 06:44:41'),
(6, 'CMR', 'CHANNEL MAINTENANCE RECOVERY CHARGES', NULL, '2019-06-04 06:49:08', '2019-06-04 06:49:08'),
(7, 'DO', 'DOCUMENTATION FEE', NULL, '2019-06-04 06:49:28', '2019-06-04 06:49:28'),
(8, 'DEDI', 'DESTINATION EDI', NULL, '2019-06-04 06:49:41', '2019-06-04 06:49:41'),
(9, 'BAF', 'BUNKER ADJUSTMENT FACTOR', NULL, '2019-06-07 00:36:28', '2019-06-07 00:36:28'),
(10, 'LCHC', 'LOCAL CONTAINER HANDLING CHARGES', NULL, '2019-07-03 02:43:09', '2019-07-03 02:43:09'),
(11, 'CHC', 'CRANE HIRE CHARGES', NULL, '2019-07-03 02:43:34', '2019-07-03 02:43:34'),
(12, 'GOPC', 'SABAH PORT SURCHARGE', NULL, '2019-07-03 02:44:44', '2019-07-03 02:44:44');

-- --------------------------------------------------------

--
-- Table structure for table `charge_cargos`
--

DROP TABLE IF EXISTS `charge_cargos`;
CREATE TABLE IF NOT EXISTS `charge_cargos` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `blcharge_id` int(10) UNSIGNED NOT NULL,
  `cargo_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `charge_cargos_blcharge_id_foreign` (`blcharge_id`),
  KEY `charge_cargos_cargo_id_foreign` (`cargo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `charge_details`
--

DROP TABLE IF EXISTS `charge_details`;
CREATE TABLE IF NOT EXISTS `charge_details` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `charge_id` int(10) UNSIGNED NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_size` int(11) NOT NULL,
  `unit_price` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `charge_details_charge_id_foreign` (`charge_id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `charge_details`
--

INSERT INTO `charge_details` (`id`, `charge_id`, `unit`, `unit_size`, `unit_price`, `created_at`, `updated_at`) VALUES
(1, 1, 'GP', 20, NULL, '2019-04-11 06:25:38', '2019-04-11 06:25:38'),
(2, 1, 'HC', 40, NULL, '2019-04-11 06:25:38', '2019-04-11 06:25:38'),
(3, 2, 'GP', 20, NULL, '2019-06-04 06:42:15', '2019-06-04 06:42:15'),
(4, 3, 'GP', 20, NULL, '2019-06-04 06:43:36', '2019-06-04 06:43:36'),
(5, 3, 'HC', 40, NULL, '2019-06-04 06:43:36', '2019-06-04 06:43:36'),
(6, 4, 'GP', 20, NULL, '2019-06-04 06:44:08', '2019-06-04 06:44:08'),
(7, 4, 'HC', 40, NULL, '2019-06-04 06:44:08', '2019-06-04 06:44:08'),
(8, 2, 'HC', 40, NULL, '2019-06-04 06:44:20', '2019-06-04 06:44:20'),
(9, 5, 'GP', 20, NULL, '2019-06-04 06:44:41', '2019-06-04 06:44:41'),
(10, 5, 'HC', 40, NULL, '2019-06-04 06:44:41', '2019-06-04 06:44:41'),
(11, 6, 'GP', 20, NULL, '2019-06-04 06:49:08', '2019-06-04 06:49:08'),
(12, 6, 'HC', 40, NULL, '2019-06-04 06:49:08', '2019-06-04 06:49:08'),
(13, 7, 'SET', 1, NULL, '2019-06-04 06:49:28', '2019-06-04 06:49:28'),
(14, 8, 'SET', 1, NULL, '2019-06-04 06:49:41', '2019-06-04 06:49:41'),
(15, 9, 'GP', 20, NULL, '2019-06-07 00:36:28', '2019-06-07 00:36:28'),
(16, 9, 'HC', 40, NULL, '2019-06-07 00:36:28', '2019-06-07 00:36:28'),
(17, 10, 'GP', 20, NULL, '2019-07-03 02:43:09', '2019-07-03 02:43:09'),
(18, 10, 'HC', 40, NULL, '2019-07-03 02:43:09', '2019-07-03 02:43:09'),
(19, 10, 'MT', 1, NULL, '2019-07-03 02:43:09', '2019-07-03 02:43:09'),
(20, 11, 'GP', 20, NULL, '2019-07-03 02:43:34', '2019-07-03 02:43:34'),
(21, 11, 'GP', 40, NULL, '2019-07-03 02:43:34', '2019-07-03 02:43:34'),
(22, 11, 'HC', 40, NULL, '2019-07-03 02:43:34', '2019-07-03 02:43:34'),
(23, 12, 'GP', 20, NULL, '2019-07-03 02:44:44', '2019-07-03 02:44:44'),
(24, 12, 'HC', 40, NULL, '2019-07-03 02:44:44', '2019-07-03 02:44:44'),
(25, 3, 'RF', 20, NULL, '2019-07-05 03:23:41', '2019-07-05 03:23:41'),
(27, 3, 'HRF', 40, NULL, '2019-07-05 03:24:39', '2019-07-05 03:24:39'),
(28, 9, 'RF', 20, NULL, '2019-07-05 03:25:35', '2019-07-05 03:25:35'),
(29, 9, 'HRF', 40, NULL, '2019-07-05 03:25:35', '2019-07-05 03:25:35'),
(30, 3, 'GP', 40, NULL, '2019-07-06 02:13:29', '2019-07-06 02:13:29');

-- --------------------------------------------------------

--
-- Table structure for table `charge_vessels`
--

DROP TABLE IF EXISTS `charge_vessels`;
CREATE TABLE IF NOT EXISTS `charge_vessels` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `charge_id` int(10) UNSIGNED NOT NULL,
  `vessel_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `charge_vessels_charge_id_foreign` (`charge_id`),
  KEY `charge_vessels_vessel_id_foreign` (`vessel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roc_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) NOT NULL,
  `port_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ba_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `roc_no`, `type`, `port_code`, `ba_code`, `telephone`, `fax`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'test%', '1', 0, NULL, NULL, NULL, NULL, '2019-04-05 09:26:03', '2019-04-05 08:26:37', '2019-04-05 09:26:03'),
(2, 'NAMYE FOOD INDUSTRIES SDN BHD', '1', 0, NULL, NULL, NULL, NULL, '2019-06-04 06:32:12', '2019-04-05 09:38:18', '2019-06-04 06:32:12'),
(3, 'KINA ENTERPRISE (SABAH) SDN BHD', '123', 1, NULL, NULL, NULL, NULL, '2019-06-04 06:32:08', '2019-04-05 09:38:33', '2019-06-04 06:32:08'),
(4, 'SYARIKAT PERKAPALAN SOO HUP SENG SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, '2019-06-04 06:32:13', '2019-04-08 08:23:27', '2019-06-04 06:32:13'),
(5, 'SOON SOON OILMILLS SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-06-04 06:34:37', '2019-06-04 06:34:37'),
(6, 'SEBERANG DISTRIBUTORS SDN BHD', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-06-04 06:36:07', '2019-06-04 06:38:16'),
(7, 'EP VENTURES SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-06-04 06:51:07', '2019-06-04 06:51:07'),
(8, 'MATER-PACK (SARAWAK) SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-06-04 06:52:08', '2019-06-04 07:05:46'),
(9, 'BARKATH STORES (PENANG) SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-06-27 05:00:45', '2019-06-27 05:00:45'),
(10, 'MOH HENG CO. SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-06-27 05:02:13', '2019-06-27 05:02:13'),
(11, 'PROFIR ACTION SDN.BHD.', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-06-27 07:57:10', '2019-06-27 07:58:21'),
(12, 'FARMSAFE CORPORATION SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-06-27 08:07:14', '2019-06-27 08:07:14'),
(13, 'KENSO PELITA NARUB-TAMIN SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-06-27 08:12:56', '2019-06-27 08:12:56'),
(14, 'SHOREFIELD SDN BHD', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-01 01:45:48', '2019-07-01 01:45:48'),
(15, 'SOUTHERN CABLE SDN.BHD.', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-01 01:47:00', '2019-07-01 01:47:00'),
(16, 'SILVERSTONE BERHAD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-01 03:17:59', '2019-07-01 03:17:59'),
(17, 'KNY TYRES SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-01 03:18:51', '2019-07-01 03:18:51'),
(18, 'SILVERSTONE MARKETING SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-01 03:20:02', '2019-07-01 03:20:02'),
(19, 'NSA TRADING SDN BHD (612553-P)', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-01 03:22:40', '2019-07-01 03:22:40'),
(20, 'PENG TAH ENTERPRISE SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-01 03:24:11', '2019-07-01 03:24:11'),
(21, 'SABAH TIRE SERVICE CENTRE', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-01 03:25:23', '2019-07-01 03:25:23'),
(22, 'ZHENG SENG TYRE ENTERPRISE', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-01 03:26:34', '2019-07-01 03:28:06'),
(23, 'MOH HENG CO. SDN BHD.', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-01 03:27:46', '2019-07-01 03:27:46'),
(24, 'SYN JIN TYRES & BATTERIES SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-01 03:29:12', '2019-07-01 03:29:12'),
(25, 'VINFIELD ENTERPRISE SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-01 03:30:08', '2019-07-01 03:30:08'),
(26, 'REX TRADING SDN.BHD.', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-01 03:31:03', '2019-07-01 03:31:03'),
(27, 'TRANSBORNEO MARKETING SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-01 03:32:12', '2019-07-01 03:32:12'),
(28, 'SEASON CASKET INDUSTRY SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-01 08:32:15', '2019-07-01 08:32:15'),
(29, 'FOO SOU CASKET SDN BHD', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-01 08:33:43', '2019-07-01 08:33:43'),
(30, 'NIBONG TEBAL ENTERPRISE SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-01 08:41:10', '2019-07-01 08:41:10'),
(31, 'JATI HOME FURNITURE MARKETING SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-02 02:43:45', '2019-07-02 02:43:45'),
(32, 'HOMEMART DONGGONGON SDN. BHD.', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-02 02:50:20', '2019-07-02 02:50:20'),
(33, 'GOLD LEAF MACNUFACTURING SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-02 03:47:10', '2019-07-02 03:47:10'),
(34, 'GOLD LEAF MANUFACTURING SDN.BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-02 03:48:26', '2019-07-02 03:48:26'),
(35, 'NANYANG PHOSPHORUS FERTILIZERS SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-02 04:04:36', '2019-07-02 04:04:36'),
(36, 'NANYANG PHOSPHORUS FERTILIZERS SDN. BHD', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-02 04:06:18', '2019-07-02 04:06:18'),
(37, 'HYUNDAI MERCHANT MARINE (MALAYSIA) SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-02 06:49:53', '2019-07-02 06:49:53'),
(38, 'HYUNDAI MERCHANT MARINE (M) SDN BHD', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-02 07:18:21', '2019-07-02 07:18:21'),
(39, 'INTERASIA LINES (M) SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-02 07:23:51', '2019-07-02 07:23:51'),
(40, 'INTERASIA LINES (M) SDN BHD.', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-02 07:24:54', '2019-07-02 07:24:54'),
(41, 'PENSONIC SALES & SERVICE SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-03 04:36:13', '2019-07-03 04:36:13'),
(42, 'PENSONIC SALES & SERVICE SDN BHD -SW (162419-M)', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-03 04:40:26', '2019-07-03 04:40:26'),
(43, 'MULTI CHOICE (M) SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-03 04:44:12', '2019-07-03 04:44:12'),
(44, 'SENG HUONG FARM', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-03 04:48:38', '2019-07-03 04:48:38'),
(45, 'TAIKO ALUMINA SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-03 04:53:25', '2019-07-03 04:53:25'),
(46, 'CMS INFRA TRADING SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-03 04:56:05', '2019-07-03 04:56:05'),
(47, 'LOGWIN AIR +OCEAN MALAYSIA SDN.BHD.', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-03 06:22:00', '2019-07-03 06:22:00'),
(48, 'SAN SWEE FATT EBTERPRISE SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-03 06:24:47', '2019-07-03 06:24:47'),
(49, 'SKY RAILWAY FURNITURE TRADING SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-04 03:51:15', '2019-07-04 03:51:15'),
(50, 'SIN SERI CAHAYA SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-04 03:54:20', '2019-07-04 03:54:20'),
(51, 'NBC FOOD INDUSTRIES SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-04 03:58:45', '2019-07-04 03:58:45'),
(52, 'SYARIKAT PERDAGANGAN YAW HON SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-04 04:01:48', '2019-07-04 04:01:48'),
(53, 'MSM PERLIS SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-04 04:27:46', '2019-07-04 04:27:46'),
(54, 'PERNIAGAAN AWM (TWU) SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-04 04:30:15', '2019-07-04 04:30:15'),
(55, 'SYARIKAT PERSATUAN JUALAN RUNCHIT BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-04 04:33:15', '2019-07-04 04:33:15'),
(56, 'CHUNG KWONG (SANDAKAN) SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-04 06:25:09', '2019-07-04 06:25:09'),
(57, 'GUAN HONG PLASTIC INDUSTRIES SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-04 06:42:33', '2019-07-04 06:42:33'),
(58, 'CHUN THONG HUA SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-04 06:45:12', '2019-07-04 06:45:12'),
(59, 'CF CHAN FURNITURE DN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-04 07:04:32', '2019-07-04 07:04:32'),
(60, 'EMPIRE FURNITURE CENTRE', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-04 07:22:22', '2019-07-04 07:22:22'),
(61, 'KENG CHEONG TRADING CO. SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-04 07:43:38', '2019-07-04 07:43:38'),
(62, 'NIBONG TEBAL PAPER MILL SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-04 08:18:20', '2019-07-04 08:18:20'),
(63, 'BIOPLUS INDUSTRIES SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 00:41:27', '2019-07-05 00:41:27'),
(64, 'SANHUPCHOON SAWMILL SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-05 00:52:24', '2019-07-05 00:52:24'),
(65, 'TOYO TYRE S & M MALAYSIA SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 01:11:54', '2019-07-05 01:11:54'),
(66, 'TOYO TYRE S & M MALAYSIA SDN. BHD.', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-05 01:16:55', '2019-07-05 01:16:55'),
(67, 'LISBLE DESIGN SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 01:51:47', '2019-07-05 01:51:47'),
(68, 'LISBLE FURNITURE TRADING SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-05 01:57:36', '2019-07-05 01:57:36'),
(69, 'MSM KUCHING DEPT', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-05 02:42:07', '2019-07-05 02:42:07'),
(70, 'SASTRIA ENTERPRISE', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 03:23:34', '2019-07-05 03:23:34'),
(71, 'GOLD LEAF MANUFACTURING SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 03:41:22', '2019-07-05 03:41:22'),
(72, 'GOLD LEAF MANUFACTURING SDN.BHD.', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-05 03:43:28', '2019-07-05 03:43:28'),
(73, 'ECONSHIP MARINE SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 05:28:51', '2019-07-05 05:28:51'),
(74, 'ECONSHIP MARINE SDN BHD.', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-05 05:30:13', '2019-07-05 05:30:13'),
(75, 'YANG MING LINE (M) SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 05:34:10', '2019-07-05 05:34:10'),
(76, 'YANG MING LINE (M) SD.BHD.', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-05 05:35:24', '2019-07-05 05:35:24'),
(77, 'INFINITY BULK LOGISTICS SDN BHD (609736-H)', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 05:44:10', '2019-07-05 05:44:10'),
(78, 'INFINITY LINES SDN BHD', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-05 05:45:02', '2019-07-05 05:45:02'),
(79, 'SINOKOR MALAYSIA SDN BHD C/O SIMBA LOGISTICS (MALAYSIA) SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 05:52:16', '2019-07-05 05:52:16'),
(80, 'SINOKOR MALAYSIA SDN BHD', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-05 05:53:07', '2019-07-05 05:53:07'),
(81, 'ORIENT OVERSEAS CONTAINER LINE (M) SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 05:57:33', '2019-07-05 05:57:33'),
(82, 'ORIENT OVERSEAS CONTAINER LINE (M) SDN.BHD.', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-05 05:58:45', '2019-07-05 05:58:45'),
(83, 'VASCO MARITIME GOODRICH ASIA PACIFIC SDN BHD', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-05 06:09:02', '2019-07-05 06:09:02'),
(84, 'MAXICONT SHIPPING AGENCIES SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 06:11:25', '2019-07-05 06:11:25'),
(85, 'MAXICONT SHIPPING AGENCIES SDN BHD.', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-05 06:12:43', '2019-07-05 06:12:43'),
(86, 'AK SHIPPING SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 06:16:26', '2019-07-05 06:16:26'),
(87, 'JINTYE CORPORATION  SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 06:21:15', '2019-07-05 06:21:15'),
(88, 'SIAWCOM MARKETING SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-05 06:23:18', '2019-07-05 06:23:18'),
(89, 'KONG GUAN SAUCE & FOOD MANUFACTURING', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 06:38:04', '2019-07-05 06:38:04'),
(90, 'DAYET SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-05 06:39:57', '2019-07-05 06:39:57'),
(91, 'VEOLIA WATER TECHNOLOGIES SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-05 06:58:32', '2019-07-05 06:58:32'),
(92, 'TAIYO YUDEN SARAWAK SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-05 07:01:58', '2019-07-05 07:01:58'),
(93, 'HOO KIONG FARM', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-05 07:59:32', '2019-07-05 07:59:32'),
(94, 'PROFIT ACTION SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-06 01:18:43', '2019-07-06 01:18:43'),
(95, 'WEY SING FURNITURE TRADING SDN.BHD.', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-06 01:21:05', '2019-07-06 01:21:05'),
(96, 'AUSSIE SLEEP MARKETING SDN.BHD.', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-06 01:35:02', '2019-07-06 01:35:02'),
(97, 'EMPIRE FURNITURE CENTRE (A73065)', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-06 01:36:00', '2019-07-06 01:36:00'),
(98, 'YHL COMFORT FURNITURE SDN.BHD. (1125690-H)', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-06 01:36:56', '2019-07-06 01:36:56'),
(99, 'SEBERANG FLOUR MILL SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-06 02:01:03', '2019-07-06 02:01:03'),
(100, 'HING WAH SAUCE & FOODSTUFF (SABAH) SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-06 02:03:14', '2019-07-06 02:03:14'),
(101, 'HWA EIK TRADING (KEDAH) SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-06 02:16:53', '2019-07-06 02:16:53'),
(102, 'MINSIANG TRADING SDN BHD', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-06 02:18:35', '2019-07-06 02:18:35'),
(103, 'HUNG TAI ENTERPRISE (SABAH) SDN. BHD.', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-06 03:00:47', '2019-07-06 03:00:47'),
(104, 'FIBRE STAR (M) SDN BHD', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-06 03:13:05', '2019-07-06 03:13:05'),
(105, 'NIBONG TEBAL ENTERPRISE SDN. BHD.,', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-07-06 04:06:14', '2019-07-06 04:06:14'),
(106, 'JAYA UNI\'ANG (S) SDN BHD C/O', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2019-07-06 04:16:37', '2019-07-06 04:16:37');

-- --------------------------------------------------------

--
-- Table structure for table `company_addresses`
--

DROP TABLE IF EXISTS `company_addresses`;
CREATE TABLE IF NOT EXISTS `company_addresses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_addresses_company_id_foreign` (`company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=127 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_addresses`
--

INSERT INTO `company_addresses` (`id`, `company_id`, `description`, `address`, `created_at`, `updated_at`) VALUES
(2, 1, 'MAIN', '123<br>', '2019-04-05 08:26:43', '2019-04-05 08:26:43'),
(11, 2, 'MAIN', '123<br>123<br>', '2019-06-04 02:54:39', '2019-06-04 02:54:39'),
(8, 3, 'MAIN', '123<br>123<br>', '2019-04-08 07:57:46', '2019-04-08 07:57:46'),
(10, 4, '165, 1ST FLOOR, VICTORIA STREET', '10300 PENANG MALAYSIA<br>', '2019-04-12 04:10:15', '2019-04-12 04:10:15'),
(33, 5, NULL, '2448 LORONG PERUSAHAAN 2,<br>PRAI INDUSTRIAL ESTATE, 13600<br>PRAI PENANG, MALAYSIA', '2019-07-01 01:51:58', '2019-07-01 01:51:58'),
(32, 6, NULL, '1ST FLOOR, LOT 2302,<br>A-10-2 YOSHI SQUARE, JLN PELABUHAN<br>93450 KUCHING, SARAWAK', '2019-07-01 01:48:47', '2019-07-01 01:48:47'),
(30, 7, NULL, 'PLOT 5, LOT 1061, JALAN PENGKALAN,<br>MK 6, 14000 BUKIT TENGAH,<br>BUKIT MERTAJAM, PENANG', '2019-07-01 01:47:51', '2019-07-01 01:47:51'),
(31, 8, NULL, 'LOT 1270-1271, SECTION 66,<br>KTLD BINTAWA INDUSTRIAL ESTATE,<br>JALAN KELULI, 93450 KUCHING, SARAWAK, MALAYSIA', '2019-07-01 01:48:29', '2019-07-01 01:48:29'),
(23, 9, 'BRANCH 1', '1207, LENGKOK PERINDUSTRIAN BUKIT<br>MINYAK 1, KAWASAN PERINDUSTRIAN BUKIT<br>MINYAK, 14100, SIMPANG AMPAT, SPT, PENANG', '2019-06-27 06:43:04', '2019-06-27 06:43:04'),
(22, 10, 'BRANCH 1', 'LOT 1028, BLOCK 26, KEMENA LAND DISTRICT<br>KIDURONG LIGHT INDUSTRIAL ESTATE,<br>97000 BINTULU, SARAWAK, MALAYSIA', '2019-06-27 06:42:31', '2019-06-27 06:42:31'),
(25, 11, 'MAIN', '6110, TINGKAT SELAMAT 9,<br>KAMPUNG SELAMAT,<br>13300 TASEK GELUGOR SEBERANG PERAI UTARA<br>', '2019-06-27 07:58:22', '2019-06-27 07:58:22'),
(26, 12, 'MAIN', 'B-09-03,PJ8, BLOCK B WEST NO. 23<br>JALAN BARAT, SEKSYEN 8,<br>46050 PETALING JAYA, SELANGOR DARUL EHSAN.', '2019-06-27 08:07:14', '2019-06-27 08:07:14'),
(27, 13, 'MAIN', 'NO. 1J. GROUND FLOOR,<br>LORONG 20 JALAN TUNKU ABDUL RAHMAN<br>96000 SIBU, SARAWAK.', '2019-06-27 08:12:56', '2019-06-27 08:12:56'),
(28, 14, 'MAIN', 'LOT 1025, BLOCK 7, MTLD, LRG DAMAK LAUT 7A<br>SEJINGKAT INDS PARK, 93050 KUCHING,SARAWAK, E,MALAYISIA.<br>TEL:082-432375 FAX:082-433990<br>', '2019-07-01 01:45:48', '2019-07-01 01:45:48'),
(29, 15, 'MAIN', 'LOT 42, JALAN MERBAU PULAS,<br>KAWASAN PERUSAHAAN KUALA KETIL,<br>09300 KUALA KETIL, BALING, KEDAH MALAYSIA', '2019-07-01 01:47:00', '2019-07-01 01:47:00'),
(34, 16, 'MAIN', 'LOT 5831, KAMUNTING INDUSTRIAL ESTATE II,<br>P.O.BOX 2, 34600 KAMUNTING PERAK<br>DARUL RIDZUAN, MALAYSIA', '2019-07-01 03:17:59', '2019-07-01 03:17:59'),
(47, 17, 'MAIN', 'MILE 7 1/2, JALAN TUARAN<br>88400, KOTA KINABALU, SABAH<br>', '2019-07-01 06:39:41', '2019-07-01 06:39:41'),
(36, 18, 'MAIN', 'MILE 8 1/2 NT 013023035, KG, RAMPAYAN<br>JALAN MENGGATAL / JALAN TURAN<br>88450 KOTA KINABALU, SABAH. TEL:088-490026<br>', '2019-07-01 03:20:02', '2019-07-01 03:20:02'),
(37, 19, 'MAIN', 'BATU 11, NT04316962, KG PANJUT,<br>89208 TUARAN, SABAH<br>', '2019-07-01 03:22:40', '2019-07-01 03:22:40'),
(38, 20, 'MAIN', 'C/O CHIA HANDLING &amp; FORWARDING SERVICES SDN BHD<br>LOT 116, TAMAN PERTAMA PHASE P.O.BOX NO.1948<br>90722 SANDAKAN, SABAH', '2019-07-01 03:24:11', '2019-07-01 03:24:11'),
(39, 21, 'MAIN', 'C/O CHIA HANDLING &amp; FORWARDING SERVICES SDN BHD<br>LOT 116, TAMAN PERTAMA PHASE , PO BOX NO.1948<br>90722 SANDAKAN, SABAH', '2019-07-01 03:25:23', '2019-07-01 03:25:23'),
(42, 22, 'MAIN', 'TB 11756, NEW HUA DAT LIGHT INDUSTRIAL<br>ESTATE, BATU 2 1/2, JALAN APAS, 91000 TAWAU, SABAH<br>ATTN: MR.TAN TEL:089-754780', '2019-07-01 03:28:06', '2019-07-01 03:28:06'),
(41, 23, 'MAIN', 'LOT 3692-3694, BLOCK 6<br>PERMY TECHNOLOGY PARK, KBLD<br>98000 MIRI SARAWAK<br>', '2019-07-01 03:27:46', '2019-07-01 03:27:46'),
(43, 24, 'MAIN', 'LOT 25, HAI OU LIGHT INDUSTRIES<br>LORONG TARAP, JALAN KOLOMBONG<br>88450 KOTA KINABALU, SABAH', '2019-07-01 03:29:12', '2019-07-01 03:29:12'),
(44, 25, 'MAIN', 'NT 213160993, MILE 5 1/2, KG.MAGORIBUNG<br>PENAMPANG, 88816 KOTA KINABALU, SABAH<br>TEL:088-728989', '2019-07-01 03:30:08', '2019-07-01 03:30:08'),
(45, 26, 'MAIN', 'PLOT 126, JALAN PERINDUSTRIAN BUKIT MINYAK 5,<br>14100 SIMPANG AMPAT, SPT,<br>PENANG, MALAYSIA', '2019-07-01 03:31:03', '2019-07-01 03:31:03'),
(46, 27, 'MAIN', 'LOT 13A, LORONG 2A KKIP TIMUR, JALAN<br>2 KKIP TIMUR, INDUSTRIAL ZONE 7, PHASE 1(IZ7PH1), KKIP TIMUR,<br>88460 KOTA KINABALU, SABAH', '2019-07-01 03:32:12', '2019-07-01 03:32:12'),
(48, 28, 'MAIN', 'NO.5, TAMAN BARU, 08000 SUNGAI PETANI, KEDAH<br>', '2019-07-01 08:32:15', '2019-07-01 08:32:15'),
(49, 29, 'MAIN', 'NO.13, HII KAH TUNG ROAD,<br>P.O.BOX 1482,<br>96008 SIBU SARAWAK', '2019-07-01 08:33:43', '2019-07-01 08:33:43'),
(50, 30, 'MAIN', 'LOT 7278, JALAN PERUSAHAAN 3, KAWASAN<br>PERINDUSTRIAN PARIT BUNTAR,<br>34200 PARIT BUNTAR, PERAK', '2019-07-01 08:41:10', '2019-07-01 08:41:10'),
(51, 31, 'MAIN', 'LOT A82(44), JALAN 1B/3, KAWASAN PERUSAHAAN<br>MIEL SUNGAI LALANG, 08000 SUNGAI PETANI, KEDAH<br>', '2019-07-02 02:43:45', '2019-07-02 02:43:45'),
(52, 32, 'MAIN', 'DONGGONGON JALAN PENAMPANG<br>TAMBUNAN, 89500 PENANMPANG, KOTA KINABALU, SABAH<br>TEL:088-717223 FAX: 088-718164', '2019-07-02 02:50:20', '2019-07-02 02:50:20'),
(53, 33, 'MAIN', '1203 PERMATANG BERAH<br>TITI MUKIM, TELOK AIR TAWAR<br>13050 BUTTERWORTH, PENANG, MALAYSIA', '2019-07-02 03:47:10', '2019-07-02 03:47:10'),
(54, 34, 'MAIN', '(MIRI BRANCH) LOT 939, SHOPHOUSE CENTRE,<br>SUNGAI DARAM, TMN.TUNKU, BLOCK 5 RANCAR<br>LAND DISTRICT, 98000 MIRI<br>', '2019-07-02 03:48:26', '2019-07-02 03:48:26'),
(55, 35, 'MAIN', 'NO.176, 1ST FLOOR, JALAN DATO BANDAR TUNGGAL,<br>07000 SEREMBAN, NEGERI SEMBILAN, MALAYISA<br>', '2019-07-02 04:04:36', '2019-07-02 04:04:36'),
(56, 36, 'MAIN', 'LOT 1630, LORONG RENTAP 8,<br>SARIKEI LIGHT INDUSTRIAL ESTATE, OFF<br>JALAN RENTAP, SARIKEI, 96100 SARAWAK<br>', '2019-07-02 04:06:18', '2019-07-02 04:06:18'),
(57, 37, 'MAIN', 'UNIT 1.3A1, 1ST FLOOR, WISMA LEADER<br>NO.8 JALAN LARUT, 10050, PENANG, MALAYSIA<br>', '2019-07-02 06:49:53', '2019-07-02 06:49:53'),
(58, 38, 'MAIN', '11TH FLOOR, EAST WING, WISMA COSPLANT 2,<br>NO.7, JALAN SS16/1, 47500 SUBANG JAYA, SELANGPR DARUL<br>EHSAN', '2019-07-02 07:18:21', '2019-07-02 07:18:21'),
(59, 39, 'MAIN', '55-18-C, MENARA NORTHAM, JALAN<br>SULTAN SHAH, 10500 PENANG<br>', '2019-07-02 07:23:51', '2019-07-02 07:23:51'),
(60, 40, 'MAIN', 'SUITE 7.01, LEVEL 7, IMS 2, 88, JALAN BATAI LAUT<br>4, 41300 KLANG, SELANGOR<br>', '2019-07-02 07:24:54', '2019-07-02 07:24:54'),
(61, 41, 'MAIN', '1165 LORONG PERINDUSTRIAN BUKIT MINYAK 16<br>TAMAN PERINDUSTRIAN BUKIT MINYAK<br>14100 SIMPANG AMPAT PENANG MALAYSIA', '2019-07-03 04:36:13', '2019-07-03 04:36:13'),
(62, 42, 'MAIN', 'GRD &amp; 1ST FLOOR, 253 JALAN DATUK<br>WEE KHENG CHIANG, 93450 KUCHING, SARAWAK.<br>TEL: 082-424003/256003  FAX: 082-424939', '2019-07-03 04:40:26', '2019-07-03 04:40:26'),
(63, 43, 'MAIN', 'NO. 987, JALAN PERUSAHAAN, KAWASAN PERUSAHAAN,<br>13600 PRAI, PULAU PINANG, MALAYSIA.<br>', '2019-07-03 04:44:12', '2019-07-03 04:44:12'),
(64, 44, 'MAIN', '633, SUBLOT 22, NO. 3, 1ST FLOOR, LORONG SUNGAI MERAH 2D2,<br>96000 SIBU, SARAWAK, EAST MALAYSIA.<br>TEL: 084-212 263/016-870 9068  FAX: 084-211 431', '2019-07-03 04:48:38', '2019-07-03 04:48:38'),
(65, 45, 'MAIN', 'LOT 169, JALAN TALI AIR, SIMPANG LIMA,<br>PARIT BUNTAR INDUSTRIAL ESTATE<br>34200 PARIT BUNTAR, PERAK<br>', '2019-07-03 04:53:25', '2019-07-03 04:53:25'),
(66, 46, 'MAIN', 'C/O SYARIKAT CHAN LIEN HOE SDN BHD (92370-A0<br>LOT 1333, JALAN PERDANA, PENDING INDUSTRIAL ESTATE,<br>93450 KUCHING, SARAWAK', '2019-07-03 04:56:05', '2019-07-03 04:56:05'),
(67, 47, 'MAIN', '3-2-1, NOVA PLACE, JALAN AHMAD NOR<br>11600 JELUTONG, PENANG, MALAYSIA.<br>', '2019-07-03 06:22:00', '2019-07-03 06:22:00'),
(68, 48, 'MAIN', '(WHOLESALE DEPARTMENT)<br>LOT 820, BLOCK 24, JALAN MUARA TUANG,<br>94300 KUCHING, SARAWAK.  TEL: +082-627- 626', '2019-07-03 06:24:47', '2019-07-03 06:24:47'),
(69, 49, 'MAIN', '2909, JALAN SUNGAI DAUN,<br>14300 NIBONG TEBAL S.P.(S),<br>PULAU PINANG.', '2019-07-04 03:51:15', '2019-07-04 03:51:15'),
(70, 50, 'MAIN', 'LOT 6950, NO. 5, GROUND FLOOR,<br>4-1/2 MILE, JALAN MATANG,<br>93050 KUCHING, SARAWAK. TEL: 082-645833', '2019-07-04 03:54:20', '2019-07-04 03:54:20'),
(71, 51, 'MAIN', 'A87, JALAN 1B-3, KAWASAN PERUSAHAAN MIEL,<br>SUNGAI LALANG, 08000 SUNGAI PETANI,<br>KEDAH, DARUL AMAN.', '2019-07-04 03:58:45', '2019-07-04 03:58:45'),
(72, 52, 'MAIN', 'LOT 51, 1ST FLOOR, BDC LIGHT INDUSTRIAL ESTATE,<br>MILE 1 1/2, LABUK ROAD, P.O. BOX 449,<br>SANDAKAN, SABAH, MALAYSIA.', '2019-07-04 04:01:48', '2019-07-04 04:01:48'),
(73, 53, 'MAIN', '(FORMERLY KNOWN AS KILANG GULA FELDA PERLIS SDN BHD)<br>P.O.BOX 42, 01700 KANGAR, PERLIS, MALAYSIA.<br>', '2019-07-04 04:27:46', '2019-07-04 04:27:46'),
(74, 54, 'MAIN', 'TB 10400, 10401,10402,<br>NEW DAT LIGHT INDUSTRIAL ESTATE,<br>JALAN APAS, 91000 TAWAU, SABAH.', '2019-07-04 04:30:15', '2019-07-04 04:30:15'),
(75, 55, 'MAIN', 'NO. 9, LOT 26, MERANTI LANE,<br>EK DEE ROAD, PO BOX 884,<br>96008 SIBU, SARAWAK.', '2019-07-04 04:33:15', '2019-07-04 04:33:15'),
(76, 56, 'MAIN', 'LOT 167,SEDCO LIGHT INDUSTRIAL ESTATE,<br>MILE 3 1/2, JALAN UTARA, PO BOX 3,<br>90700 SANDAKAN, SABAH.<br>', '2019-07-04 06:25:09', '2019-07-04 06:25:09'),
(77, 57, 'MAIN', '8394, KAWASAN PERINDUSTRIAN KAMPUNG TELUK,<br>SUNGAI DUA, 13800 BUTTERWORTH, PENANG.<br>', '2019-07-04 06:42:33', '2019-07-04 06:42:33'),
(78, 58, 'MAIN', 'LOT 1286, SECTION 66, KTLD, JALAN DAGER/<br>PERBADANAN, BINTAWA INDUSTRIAL ESTATE,<br>93450 KUCHING, SARAWAK.', '2019-07-04 06:45:12', '2019-07-04 06:45:12'),
(79, 59, 'MAIN', 'TB 4202, TMN NORDIN BATU 1 3/4,<br>JALAN KUHARA, 91016 TAWAU, SABAH.<br>TEL: 089-768881, 089-768882  FAX: 089-768889', '2019-07-04 07:04:32', '2019-07-04 07:04:32'),
(80, 60, 'MAIN', 'LOT 115, SL 9, JALAN STAPOK UTAMA,<br>TAMAN VICTORIA, SUNGAI MAONG ULU,<br>93250 KUCHING, SARAWAK', '2019-07-04 07:22:22', '2019-07-04 07:22:22'),
(81, 61, 'MAIN', 'NO. 22, JALAN KIKIK. TAMAN INDERAWASIH<br>13600 PERAI, PENANG<br>', '2019-07-04 07:43:38', '2019-07-04 07:43:38'),
(82, 62, 'MAIN', '886 JALAN BANDAR BARU,<br>SUNGAI KECIL,,<br>14300 NIBONG TEBAL,, S.P.S. PENANG', '2019-07-04 08:18:20', '2019-07-04 08:18:20'),
(83, 63, 'MAIN', 'PLOT 303 AND 304, JALAN PKNK 3/1,<br>KAWASAN PERINDUSTRIAN SUNGAI PETANI,<br>08000 SUNGAI PETANI, KEDAH, MALAYSIA.', '2019-07-05 00:41:27', '2019-07-05 00:41:27'),
(84, 64, 'MAIN', '177B, 3RD FL. KPG NYABOR RD.,<br>P.O. BOX 1297, 96008 SIBU,<br>SARAWAK, MALAYSIA.', '2019-07-05 00:52:24', '2019-07-05 00:52:24'),
(85, 65, 'MAIN', 'PT 23101, JALAN TEMBAGA KUNING<br>KAWASAN PERINDUSTRIAN KAMUNTING RAYA<br>34600 KAMUNTING, PERAK.', '2019-07-05 01:11:54', '2019-07-05 01:11:54'),
(86, 66, 'MAIN', 'C/O KOBE SHIPPING &amp; FORWARDING AGENCIES SDN BHD<br>LOT 1394, SECTION 66, K.T.L.D. JALAN MERANTI<br>PENDING INDUSTRIAL ESTATE, 93450 KUCHING, SARAWAK.<br>', '2019-07-05 01:16:55', '2019-07-05 01:16:55'),
(87, 67, 'MAIN', 'NO. 2946, BAGAN BUAYA, CHANGKAT,<br>14300 NIBONG TEBAL, SEBERANG PERAI SELATAN,<br>PULAU PINANG.', '2019-07-05 01:51:47', '2019-07-05 01:51:47'),
(88, 68, 'MAIN', 'LOT 823, LORONG DEMAK LAUT 3A1,<br>BLOCK 7, MUARA TEBAS LAND DISTRICT,<br>DEMAK LAUT INDUSTRIAL PARK, 93050 KUCHING, SARAWAK', '2019-07-05 01:57:36', '2019-07-05 01:57:36'),
(89, 69, 'MAIN', 'C/O LEE CHUAN HONG CO. SDN. BHD.<br>LOT 753, SECTION 64, JALAN SUNGAI PRIOK,<br>OFF JALAN PENDING, 93450 KUCHING, SARAWAK.', '2019-07-05 02:42:07', '2019-07-05 02:42:07'),
(90, 70, 'MAIN', 'NO. 307, JALAN ZAMRUD 10,<br>TAMAN PEKAN BARU,<br>08000 SUNGAI PETANI, KEDAH.', '2019-07-05 03:23:34', '2019-07-05 03:23:34'),
(91, 71, 'MAIN', '1203 PERMATANG BERAH, TITI MUKIM,<br>TELOK AIR TAWAR, 13050 BUTTERWORTH,<br>PENANG WEST MALAYSIA', '2019-07-05 03:41:22', '2019-07-05 03:41:22'),
(92, 72, 'MAIN', '(KUCHING BRANCH) 277A, JALAN SEMABA<br>5TH MILE, SEMABA LIGHT INDUSTRIAL PARK,<br>93250 KUCHING, SARAWAK', '2019-07-05 03:43:28', '2019-07-05 03:43:28'),
(93, 73, 'MAIN', 'SUITE #21-06, LEVEL 21, CENTRO, NO.8, JALAN BATU<br>TIGA LAMA, 41300 KLANG, SELANG, MALAYSIA<br>', '2019-07-05 05:28:51', '2019-07-05 05:28:51'),
(94, 74, 'MAIN', 'KCX SOCIAL OFFICE 67-1, JALAN SETIA UTAMA AT<br>U13/AT SETIA ALAM, SEKSYEN U13,<br>40170 SHAH ALAM, SELANGOR', '2019-07-05 05:30:13', '2019-07-05 05:30:13'),
(95, 75, 'MAIN', 'LOT 13.01, LEVEL 13, MENARA KWSP, NO.38,<br>JALAN SULTAN AHMAD SHAH, 10500 PENANG, MALAYSIA<br>', '2019-07-05 05:34:10', '2019-07-05 05:34:10'),
(96, 76, 'MAIN', 'SUITE 12.01, LEVEL 12, MENARA TREND,IMS<br>NO.68, JALAN BATAI LAUT 4, TAMAN INTAN<br>41300 KLANG, SELANGOR', '2019-07-05 05:35:24', '2019-07-05 05:35:24'),
(97, 77, 'MAIN', 'NO.2, JALAN PUSAT PERNIAGAAN RAJA UDA 1,<br>PUSAT PERNIAGAAN RAJA UDA, 12300 BUTTERWORTH,<br>PENANG', '2019-07-05 05:44:10', '2019-07-05 05:44:10'),
(98, 78, 'MAIN', 'NO.2, JALAN KASUARINA 8/KS07 BANDAR BOTANIC<br>41200 KLANG<br>', '2019-07-05 05:45:02', '2019-07-05 05:45:02'),
(99, 79, 'MAIN', 'UNIT 9.05, 9TH FLOOR, MENARA BOUSTEAD,<br>NO.39, JALAN SULTAN AHMAD SHAH<br>10500 PENANG', '2019-07-05 05:52:16', '2019-07-05 05:52:16'),
(100, 80, 'MAIN', 'SUITE 15-07, LEVEL 15, CENTRO NO.8, JALAN<br>BATU TIGA LAMA, 41300 KLANG SELANGOR<br>', '2019-07-05 05:53:07', '2019-07-05 05:53:07'),
(101, 81, 'MAIN', 'SUITE 1502, MWE PLAZA, NO.8, LEBUH FARQUHAR,<br>10200 PENANG MALAYSIA<br>', '2019-07-05 05:57:33', '2019-07-05 05:57:33'),
(102, 82, 'MAIN', 'UNIT 10.2. LEVEL 10, MENARA AXIS<br>NO.2, JALAN 51A/223, 46100 PETALING JAYA,<br>SELANGOR MALAYSIA', '2019-07-05 05:58:45', '2019-07-05 05:58:45'),
(103, 83, 'MAIN', 'CENTRO LEVEL 27-5, NO.8, JALAN BATU TIGA LAMA,<br>41300 KLANG, SELANGOR, MALAYSIA<br>', '2019-07-05 06:09:02', '2019-07-05 06:09:02'),
(104, 84, 'MAIN', 'MENARA BHL, 51-16-A,<br>JALAN SULTAN AHMAD SHAH,<br>10250 PENANG', '2019-07-05 06:11:25', '2019-07-05 06:11:25'),
(105, 85, 'MAIN', 'LOT 9 CENTRO-15TH FLOOR, NO.8M,<br>JALAN BATU TIGA LAMA, 41300 SELANGOR,<br>MALAYSIA<br>', '2019-07-05 06:12:43', '2019-07-05 06:12:43'),
(106, 86, 'MAIN', 'LEVEL 16, UNIT 16-H, WISMA BOON SIEW,<br>NO.1, PENANG ROAD, 10000 PENANG<br>MALAYSIA', '2019-07-05 06:16:26', '2019-07-05 06:16:26'),
(107, 87, 'MAIN', '6, 8, 10 JALAN PALA 14,<br>KAWASAN PERINDUSTRIAN PERMATANG TINGGI,<br>14100 BUKIT MERTAJAM.', '2019-07-05 06:21:15', '2019-07-05 06:21:15'),
(108, 88, 'MAIN', 'NO. 22, GROUND FLOOR, JALAN MERDEKA<br>BARAT, 96000 SIBU, SARAWAK.<br>TEL: 016-8861997', '2019-07-05 06:23:18', '2019-07-05 06:23:18'),
(109, 89, 'MAIN', 'CO. SDN BHD (21903-H)<br>2463 SOLOK PERUSAHAAN SATU, PRAI<br>INDUSTRIAL ESTATE,13600 PRAI, PROVINCE WELLESLEY.', '2019-07-05 06:38:04', '2019-07-05 06:38:04'),
(110, 90, 'MAIN', 'NO. 14 &amp; 16 UPPER LANANG<br>LORONG 29A, 96000 SIBU, SARAWAK.<br>TEL: (084) 213-535  FAX: (084) 253-012', '2019-07-05 06:39:57', '2019-07-05 06:39:57'),
(111, 91, 'MAIN', '68, JALAN ICON CITY<br>14000 BUKIT MERTAJAM<br>', '2019-07-05 06:58:32', '2019-07-05 06:58:32'),
(112, 92, 'MAIN', 'LOT 977, BLOCK 12,SAMA JYA FREE<br>INDUSTRIAL ZONE, KUCHING 93450 SARAWAK<br>PTC: SIMON (H/P: 012 897 6022)', '2019-07-05 07:01:58', '2019-07-05 07:01:58'),
(113, 93, 'MAIN', 'NO. 7, LANE 19, JALAN WONG KING HUO,<br>96000 SIBU, SARAWAK.<br>TEL: 084-327 727  FAX: 084-340 727  ATTN: MR. WONG', '2019-07-05 07:59:32', '2019-07-05 07:59:32'),
(114, 94, 'MAIN', '6110, TINGKAT SELAMAT 9,<br>KAMPUNG SELAMAT, 13300 TASEK GELUGOR,<br>SEBERANG PERAI UTARA.', '2019-07-06 01:18:43', '2019-07-06 01:18:43'),
(115, 95, 'MAIN', 'LOT 8034, BLOCK 59, MUARA TUANG LAND<br>DISTRICT, KOTA SAMARAHAN INDUSTRIAL<br>ESTATE, KG. TANJUNG BUNDONG, 94300 KOTA SAMARAHAN', '2019-07-06 01:21:05', '2019-07-06 01:21:05'),
(116, 96, 'MAIN', 'NO.2400, TINGKAT SERINDIT 1, TAMAN DESA JAWI,<br>14200 SUNGAI BAKAP, PULAU PINANG<br>', '2019-07-06 01:35:02', '2019-07-06 01:35:02'),
(117, 97, 'MAIN', 'LOT 115, SL 9, JALAN STAPOK UTAMA,<br>TAMAN VICTORIA, SG.MAONG ULU,<br>93250 KUCHING, SARAWAK', '2019-07-06 01:36:00', '2019-07-06 01:36:00'),
(118, 98, 'MAIN', 'NO.40-50, LORONG 2, JALAN LING KAI CHENG,<br>96000 SIBU, SARAWAK<br>', '2019-07-06 01:36:56', '2019-07-06 01:36:56'),
(119, 99, 'MAIN', '2425 LORONG PERUSAHAAN 2<br>PRAI INDUSTRIAL ESTATE<br>13600 PRAI, PENANG, MALAYSIA', '2019-07-06 02:01:03', '2019-07-06 02:01:03'),
(120, 100, 'MAIN', 'LOT 53 &amp; 55, SUDC LIGHT INDUSTRIAL ESTATE<br>LOK KAWI, P.O. BOX 10051,<br>88801 KOTA KINABALU, SABAH.', '2019-07-06 02:03:14', '2019-07-06 02:03:14'),
(121, 101, 'MAIN', 'LOT 182, JALAN PINANG JAYA<br>KEPALA BATS, 06200 ALOR SETAR<br>KEDAH.', '2019-07-06 02:16:53', '2019-07-06 02:16:53'),
(122, 102, 'MAIN', 'NO. 21 &amp; 23, DING LIK KONG 6,<br>96000 SIBU, SARAWAK.<br>TEL: 084-334682  FAX: 084-333682', '2019-07-06 02:18:35', '2019-07-06 02:18:35'),
(123, 103, 'MAIN', 'LOT 53, LOK KAWI INDUSTRI ESTATE<br>PHASE 2, 89600 KINARUT, SABAH<br>', '2019-07-06 03:00:47', '2019-07-06 03:00:47'),
(124, 104, 'MAIN', 'NO. 3843, MK 11, LUAR DESA JAWI,<br>14200 SUNGAI JAWI, S.P.S.<br>PENANG, MALAYSIA.', '2019-07-06 03:13:05', '2019-07-06 03:13:05'),
(125, 105, 'MAIN', 'LOT 3A, INDUSTRIAL ZONE 7 (IZ7)<br>KKIP TIMUR JALAN SEPANGGAR<br>88460 KOTA KINABALU, SABAH.<br>', '2019-07-06 04:06:14', '2019-07-06 04:06:14'),
(126, 106, 'MAIN', 'CHIA HANDLING &amp; FORWARDING SERVICES SDN BHD<br>LOT 116, TAMAN PERTAMA PHASE, P.O.BOX NO.1948<br>90722 SANDAKAN, SABAH', '2019-07-06 04:16:37', '2019-07-06 04:16:37');

-- --------------------------------------------------------

--
-- Table structure for table `company_emails`
--

DROP TABLE IF EXISTS `company_emails`;
CREATE TABLE IF NOT EXISTS `company_emails` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_emails_company_id_foreign` (`company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=127 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_emails`
--

INSERT INTO `company_emails` (`id`, `company_id`, `email`, `created_at`, `updated_at`) VALUES
(2, 1, '123@2143', '2019-04-05 08:26:43', '2019-04-05 08:26:43'),
(11, 2, '123@123', '2019-06-04 02:54:39', '2019-06-04 02:54:39'),
(8, 3, '123@123', '2019-04-08 07:57:46', '2019-04-08 07:57:46'),
(10, 4, 'cs.ctnr@soship.com.my', '2019-04-12 04:10:15', '2019-04-12 04:10:15'),
(33, 5, 'cus_service@tegasmarine.com', '2019-07-01 01:51:58', '2019-07-01 01:51:58'),
(32, 6, 'cus_service@tegasmarine.com', '2019-07-01 01:48:47', '2019-07-01 01:48:47'),
(30, 7, 'cus_service@tegasmarine.com', '2019-07-01 01:47:51', '2019-07-01 01:47:51'),
(31, 8, 'cus_service@tegasmarine.com', '2019-07-01 01:48:29', '2019-07-01 01:48:29'),
(23, 9, 'doc@tegasmarine.com', '2019-06-27 06:43:04', '2019-06-27 06:43:04'),
(22, 10, 'doc@tegasmarine.com', '2019-06-27 06:42:31', '2019-06-27 06:42:31'),
(25, 11, 'doc@tegasmarine.com.my', '2019-06-27 07:58:22', '2019-06-27 07:58:22'),
(26, 12, 'doc@tegasmarine.com.my', '2019-06-27 08:07:14', '2019-06-27 08:07:14'),
(27, 13, 'doc@temasmarine.com.my', '2019-06-27 08:12:56', '2019-06-27 08:12:56'),
(28, 14, 'DOC@TEGASMARINE.COM', '2019-07-01 01:45:48', '2019-07-01 01:45:48'),
(29, 15, 'DOC@TEGASMARINE.COM', '2019-07-01 01:47:00', '2019-07-01 01:47:00'),
(34, 16, 'doc@tegasmarine.com', '2019-07-01 03:17:59', '2019-07-01 03:17:59'),
(47, 17, 'doc@tegasmarine.com', '2019-07-01 06:39:41', '2019-07-01 06:39:41'),
(36, 18, 'doc@tegasmarine.com', '2019-07-01 03:20:02', '2019-07-01 03:20:02'),
(37, 19, 'doc@tegasmarine.com', '2019-07-01 03:22:40', '2019-07-01 03:22:40'),
(38, 20, 'doc@tegasmarine.com', '2019-07-01 03:24:11', '2019-07-01 03:24:11'),
(39, 21, 'doc@tegasmarine.com', '2019-07-01 03:25:23', '2019-07-01 03:25:23'),
(42, 22, 'doc@tegasmarine.com', '2019-07-01 03:28:06', '2019-07-01 03:28:06'),
(41, 23, 'doc@tegasmarine.com', '2019-07-01 03:27:46', '2019-07-01 03:27:46'),
(43, 24, 'doc@tegasmarine.com', '2019-07-01 03:29:12', '2019-07-01 03:29:12'),
(44, 25, 'doc@tegasmarine.com', '2019-07-01 03:30:08', '2019-07-01 03:30:08'),
(45, 26, 'doc@tegasmarine.com', '2019-07-01 03:31:03', '2019-07-01 03:31:03'),
(46, 27, 'doc@tegasmarine.com', '2019-07-01 03:32:12', '2019-07-01 03:32:12'),
(48, 28, 'doc@tegasmarine.com', '2019-07-01 08:32:15', '2019-07-01 08:32:15'),
(49, 29, 'doc@tegasmarine.com', '2019-07-01 08:33:43', '2019-07-01 08:33:43'),
(50, 30, 'doc@tegasmarine.com', '2019-07-01 08:41:10', '2019-07-01 08:41:10'),
(51, 31, 'doc@tegasmarine.com', '2019-07-02 02:43:45', '2019-07-02 02:43:45'),
(52, 32, 'doc@tegasmarine.com', '2019-07-02 02:50:20', '2019-07-02 02:50:20'),
(53, 33, 'doc@tegasmarine.com', '2019-07-02 03:47:10', '2019-07-02 03:47:10'),
(54, 34, 'doc@tegasmarine.com', '2019-07-02 03:48:26', '2019-07-02 03:48:26'),
(55, 35, 'doc@tegasmarine.com', '2019-07-02 04:04:36', '2019-07-02 04:04:36'),
(56, 36, 'doc@tegasmarine.com', '2019-07-02 04:06:18', '2019-07-02 04:06:18'),
(57, 37, 'doc@tegasmarine.com', '2019-07-02 06:49:53', '2019-07-02 06:49:53'),
(58, 38, 'doc@tegasmarine.com', '2019-07-02 07:18:21', '2019-07-02 07:18:21'),
(59, 39, 'doc@tegasmarine.com', '2019-07-02 07:23:51', '2019-07-02 07:23:51'),
(60, 40, 'DOC@TEGASMARINE.COM', '2019-07-02 07:24:54', '2019-07-02 07:24:54'),
(61, 41, 'tegasmarine@gmail.com', '2019-07-03 04:36:13', '2019-07-03 04:36:13'),
(62, 42, 'tegasmarine@gmail.com', '2019-07-03 04:40:26', '2019-07-03 04:40:26'),
(63, 43, 'tegasmarine@gmail.com', '2019-07-03 04:44:12', '2019-07-03 04:44:12'),
(64, 44, 'tegasmarine@gmail.com', '2019-07-03 04:48:38', '2019-07-03 04:48:38'),
(65, 45, 'tegasmarine@gmail.com', '2019-07-03 04:53:25', '2019-07-03 04:53:25'),
(66, 46, 'tegasmarine@gmail.com', '2019-07-03 04:56:05', '2019-07-03 04:56:05'),
(67, 47, 'tegasmarine@gmail.com.my', '2019-07-03 06:22:00', '2019-07-03 06:22:00'),
(68, 48, 'tegasmarine@gmail.com.my', '2019-07-03 06:24:47', '2019-07-03 06:24:47'),
(69, 49, 'tegasmarine@gmail.com', '2019-07-04 03:51:15', '2019-07-04 03:51:15'),
(70, 50, 'tegasmarine@gmail.com', '2019-07-04 03:54:20', '2019-07-04 03:54:20'),
(71, 51, 'tegasmarine@gmail.com', '2019-07-04 03:58:45', '2019-07-04 03:58:45'),
(72, 52, 'tegasmarine@gmail.com', '2019-07-04 04:01:48', '2019-07-04 04:01:48'),
(73, 53, 'tegasmarine@gamil.com', '2019-07-04 04:27:46', '2019-07-04 04:27:46'),
(74, 54, 'tegasmarine@gmail.com', '2019-07-04 04:30:15', '2019-07-04 04:30:15'),
(75, 55, 'tegasmarine@gmail.com', '2019-07-04 04:33:15', '2019-07-04 04:33:15'),
(76, 56, 'tegasmarine@gmail.com', '2019-07-04 06:25:09', '2019-07-04 06:25:09'),
(77, 57, 'tegas@gmail.com', '2019-07-04 06:42:33', '2019-07-04 06:42:33'),
(78, 58, 'tegas@gmail.com', '2019-07-04 06:45:12', '2019-07-04 06:45:12'),
(79, 59, 'tegas@gmail.com', '2019-07-04 07:04:32', '2019-07-04 07:04:32'),
(80, 60, 'tegas@gmail.com', '2019-07-04 07:22:22', '2019-07-04 07:22:22'),
(81, 61, 'tegas@gmail.com', '2019-07-04 07:43:38', '2019-07-04 07:43:38'),
(82, 62, 'tegas@gmail.com', '2019-07-04 08:18:20', '2019-07-04 08:18:20'),
(83, 63, 'tegas@gmail.com', '2019-07-05 00:41:27', '2019-07-05 00:41:27'),
(84, 64, 'tegas@gmail.com', '2019-07-05 00:52:24', '2019-07-05 00:52:24'),
(85, 65, 'TEGAS@GMAIL.COM', '2019-07-05 01:11:54', '2019-07-05 01:11:54'),
(86, 66, 'tegas@gmail.com', '2019-07-05 01:16:55', '2019-07-05 01:16:55'),
(87, 67, 'tegas@gmail.com', '2019-07-05 01:51:47', '2019-07-05 01:51:47'),
(88, 68, 'tegas@gmail.com', '2019-07-05 01:57:36', '2019-07-05 01:57:36'),
(89, 69, 'tegas@gmail.com', '2019-07-05 02:42:07', '2019-07-05 02:42:07'),
(90, 70, 'tegas@gmail.com', '2019-07-05 03:23:34', '2019-07-05 03:23:34'),
(91, 71, 'tegas@gmail.com', '2019-07-05 03:41:22', '2019-07-05 03:41:22'),
(92, 72, 'tegas@gmail.com', '2019-07-05 03:43:28', '2019-07-05 03:43:28'),
(93, 73, 'doc@tegasmarine.com', '2019-07-05 05:28:51', '2019-07-05 05:28:51'),
(94, 74, 'doc@tegasmarine.com', '2019-07-05 05:30:13', '2019-07-05 05:30:13'),
(95, 75, 'doc@tegasmarine.com', '2019-07-05 05:34:10', '2019-07-05 05:34:10'),
(96, 76, 'DOC@TEGASMARINE.COM', '2019-07-05 05:35:24', '2019-07-05 05:35:24'),
(97, 77, 'DOC@TEGASMARINE', '2019-07-05 05:44:10', '2019-07-05 05:44:10'),
(98, 78, 'DOC@TEGASMARINE.COM', '2019-07-05 05:45:02', '2019-07-05 05:45:02'),
(99, 79, 'doc@tegasmarine.com', '2019-07-05 05:52:16', '2019-07-05 05:52:16'),
(100, 80, 'DOC@TEGASMARINE.COM', '2019-07-05 05:53:07', '2019-07-05 05:53:07'),
(101, 81, 'DOC@TEGASMARINE.COM', '2019-07-05 05:57:33', '2019-07-05 05:57:33'),
(102, 82, 'DOC@TEGASMARINE.COM', '2019-07-05 05:58:45', '2019-07-05 05:58:45'),
(103, 83, 'DOC@TEGASMARINE.COM', '2019-07-05 06:09:02', '2019-07-05 06:09:02'),
(104, 84, 'doc@tegasmarine.com', '2019-07-05 06:11:25', '2019-07-05 06:11:25'),
(105, 85, 'doc@tegasmarine.com', '2019-07-05 06:12:43', '2019-07-05 06:12:43'),
(106, 86, 'DOC@TEGASMARINE.COM', '2019-07-05 06:16:26', '2019-07-05 06:16:26'),
(107, 87, 'tegas@gmail.com', '2019-07-05 06:21:15', '2019-07-05 06:21:15'),
(108, 88, 'tegas@gmail.com', '2019-07-05 06:23:18', '2019-07-05 06:23:18'),
(109, 89, 'tegas@gmail.com', '2019-07-05 06:38:04', '2019-07-05 06:38:04'),
(110, 90, 'tegas@gmail.com', '2019-07-05 06:39:57', '2019-07-05 06:39:57'),
(111, 91, 'tegas@gmail.com', '2019-07-05 06:58:32', '2019-07-05 06:58:32'),
(112, 92, 'tegas@gmail.com', '2019-07-05 07:01:58', '2019-07-05 07:01:58'),
(113, 93, 'tegas@gmail.com', '2019-07-05 07:59:32', '2019-07-05 07:59:32'),
(114, 94, 'tegas@gmail.com', '2019-07-06 01:18:43', '2019-07-06 01:18:43'),
(115, 95, 'tegas@gmail.com', '2019-07-06 01:21:05', '2019-07-06 01:21:05'),
(116, 96, 'DOC@TEGASMARINE.COM', '2019-07-06 01:35:02', '2019-07-06 01:35:02'),
(117, 97, 'DOC@TEGASMARINE.COM', '2019-07-06 01:36:00', '2019-07-06 01:36:00'),
(118, 98, 'DOC@TEGASMARINE.COM', '2019-07-06 01:36:56', '2019-07-06 01:36:56'),
(119, 99, 'tegas@gmail.com', '2019-07-06 02:01:03', '2019-07-06 02:01:03'),
(120, 100, 'tegas@gmail.com', '2019-07-06 02:03:14', '2019-07-06 02:03:14'),
(121, 101, 'TEGAS@GMAIL.COM', '2019-07-06 02:16:53', '2019-07-06 02:16:53'),
(122, 102, 'TEDGAS@GMAIL.COM', '2019-07-06 02:18:35', '2019-07-06 02:18:35'),
(123, 103, 'tegas@gmail.com', '2019-07-06 03:00:47', '2019-07-06 03:00:47'),
(124, 104, 'tedgas@gmail.com', '2019-07-06 03:13:05', '2019-07-06 03:13:05'),
(125, 105, 'tegas@gmail.com', '2019-07-06 04:06:14', '2019-07-06 04:06:14'),
(126, 106, 'DOC@TEGASMARINE.COM', '2019-07-06 04:16:37', '2019-07-06 04:16:37');

-- --------------------------------------------------------

--
-- Table structure for table `consignees`
--

DROP TABLE IF EXISTS `consignees`;
CREATE TABLE IF NOT EXISTS `consignees` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `consignees_company_id_foreign` (`company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `consignees`
--

INSERT INTO `consignees` (`id`, `company_id`, `code`, `telephone`, `fax`, `pic`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 3, 'KINA', '132312313', '123131313', 'test', '2019-06-04 06:32:08', '2019-04-05 09:38:33', '2019-06-04 06:32:08'),
(2, 4, 'SHS', '04-2626871', '04-2632063', 'JIAMIN', NULL, '2019-04-08 08:23:27', '2019-04-08 08:23:27'),
(3, 6, '2302', '04-2631808', NULL, 's', NULL, '2019-06-04 06:38:16', '2019-06-04 06:38:16'),
(4, 8, '1270', '04-2631808', NULL, 'TENG', NULL, '2019-06-04 07:05:46', '2019-06-04 07:05:46'),
(5, 9, 'BARKATH', '042631808', NULL, 'tegas', NULL, '2019-06-27 05:00:45', '2019-06-27 05:00:45'),
(6, 10, '1028', '042631808', NULL, 'tegas', NULL, '2019-06-27 05:02:13', '2019-06-27 05:02:13'),
(7, 11, 'TEGA', '604-2631808', NULL, 'MR LIM', NULL, '2019-06-27 07:58:21', '2019-06-27 07:58:21'),
(8, 12, 'TEGA', '604-2631808', NULL, 'MR LIM', NULL, '2019-06-27 08:07:14', '2019-06-27 08:07:14'),
(9, 13, 'TEGA', '601-2631808', NULL, 'MR LIM', NULL, '2019-06-27 08:12:56', '2019-06-27 08:12:56'),
(10, 14, 'SHOREFIELD', '04-2631808', NULL, 'TEGAS', NULL, '2019-07-01 01:45:48', '2019-07-01 01:45:48'),
(11, 16, 'SILVERSTONE', '042631808', NULL, 'rex', NULL, '2019-07-01 03:17:59', '2019-07-01 03:17:59'),
(12, 17, 'KNY', '042631808', NULL, 'kny', NULL, '2019-07-01 03:18:51', '2019-07-01 06:39:41'),
(13, 18, 'SIL', '042631808', NULL, 'SIL', NULL, '2019-07-01 03:20:02', '2019-07-01 03:20:02'),
(14, 19, 'NSA', '042631808', NULL, 'NSA', NULL, '2019-07-01 03:22:40', '2019-07-01 03:22:40'),
(15, 20, 'PENGTAH', '042631808', NULL, 'peng', NULL, '2019-07-01 03:24:11', '2019-07-01 03:24:11'),
(16, 21, 'tire', '042631808', NULL, 'saba', NULL, '2019-07-01 03:25:23', '2019-07-01 03:25:23'),
(17, 23, 'MHMIRI', '042631808', NULL, 'MOH HENG', NULL, '2019-07-01 03:27:46', '2019-07-01 03:27:46'),
(18, 22, 'ZHENG', '042631808', NULL, 'zheng', NULL, '2019-07-01 03:28:06', '2019-07-01 03:28:06'),
(19, 24, 'SYN', '042631808', NULL, 'SYN', NULL, '2019-07-01 03:29:12', '2019-07-01 03:29:12'),
(20, 25, 'VIN', '042631808', NULL, 'SYN', NULL, '2019-07-01 03:30:08', '2019-07-01 03:30:08'),
(21, 27, 'TRANS', '042631808', NULL, 'TRR', NULL, '2019-07-01 03:32:12', '2019-07-01 03:32:12'),
(22, 29, 'FOOSOU', '042631808', NULL, 'CHAI', NULL, '2019-07-01 08:33:43', '2019-07-01 08:33:43'),
(23, 32, 'HOMEMART', '042632808', NULL, 'home', NULL, '2019-07-02 02:50:20', '2019-07-02 02:50:20'),
(24, 34, 'GLMIRI', '042631808', NULL, 'gl', NULL, '2019-07-02 03:48:26', '2019-07-02 03:48:26'),
(25, 36, 'NANYANG', '042631808', NULL, 'nan', NULL, '2019-07-02 04:06:18', '2019-07-02 04:06:18'),
(26, 38, 'HMMPK', '04-2631808', NULL, 'hmm', NULL, '2019-07-02 07:18:21', '2019-07-02 07:18:21'),
(27, 40, 'IALPK', '042631808', NULL, 'GRACE', NULL, '2019-07-02 07:24:54', '2019-07-02 07:24:54'),
(28, 42, 'PEN', '604-2631808', '604-2631808', 'MR LIM', NULL, '2019-07-03 04:40:26', '2019-07-03 04:40:26'),
(29, 44, 'SENG', '604-2631808', '604-2631808', 'MR LIM', NULL, '2019-07-03 04:48:38', '2019-07-03 04:48:38'),
(30, 46, 'CMS', '604-2631808', NULL, 'MR LIM', NULL, '2019-07-03 04:56:05', '2019-07-03 04:56:05'),
(31, 48, 'SWEE', '604-2631808', NULL, 'MR LIM', NULL, '2019-07-03 06:24:47', '2019-07-03 06:24:47'),
(32, 50, 'CAHAYA', '604-2631808', NULL, 'MR LIM', NULL, '2019-07-04 03:54:20', '2019-07-04 03:54:20'),
(33, 52, 'YAWHON', '604-2631808', NULL, 'MR LIM', NULL, '2019-07-04 04:01:48', '2019-07-04 04:01:48'),
(34, 54, 'AWMTWU', '604-2631808', NULL, 'LIM', NULL, '2019-07-04 04:30:15', '2019-07-04 04:30:15'),
(35, 55, 'RUNCHITSBW', '604-2631808', NULL, 'LIM', NULL, '2019-07-04 04:33:15', '2019-07-04 04:33:15'),
(36, 56, 'CKWONGSDK', '042631808', NULL, 'LIM', NULL, '2019-07-04 06:25:09', '2019-07-04 06:25:09'),
(37, 58, 'CHUNKCH', '6042631808', NULL, 'LIM', NULL, '2019-07-04 06:45:12', '2019-07-04 06:45:12'),
(38, 59, 'CFTWU', '6042631808', NULL, 'LIM', NULL, '2019-07-04 07:04:32', '2019-07-04 07:04:32'),
(39, 64, 'SAWMILL', '604-2631808', NULL, 'LIM', NULL, '2019-07-05 00:52:24', '2019-07-05 00:52:24'),
(40, 66, 'TYRE', '604-2631808', NULL, 'LIM', NULL, '2019-07-05 01:16:55', '2019-07-05 01:16:55'),
(41, 68, 'LISBLEKCH', '604-2631808', NULL, 'LIM', NULL, '2019-07-05 01:57:36', '2019-07-05 01:57:36'),
(42, 69, 'MSMKCH', '604-2631808', NULL, 'LIM', NULL, '2019-07-05 02:42:07', '2019-07-05 02:42:07'),
(43, 72, 'GOLDKCH', '604-2631808', NULL, 'LIM', NULL, '2019-07-05 03:43:28', '2019-07-05 03:43:28'),
(44, 74, 'ECON', '042631808', NULL, 'tegas', NULL, '2019-07-05 05:30:13', '2019-07-05 05:30:13'),
(45, 76, 'YMLPKG', '042631808', NULL, 'TEGAS', NULL, '2019-07-05 05:35:24', '2019-07-05 05:35:24'),
(46, 78, 'INFI', '042631808', NULL, 'TEGAS', NULL, '2019-07-05 05:45:02', '2019-07-05 05:45:02'),
(47, 80, 'SNR', '04-2631808', NULL, 'TEGAS', NULL, '2019-07-05 05:53:07', '2019-07-05 05:53:07'),
(48, 82, 'OOCLPK', '042631808', NULL, 'TEGAS', NULL, '2019-07-05 05:58:45', '2019-07-05 05:58:45'),
(49, 83, 'VASCO', '042631808', NULL, 'TEGAS', NULL, '2019-07-05 06:09:02', '2019-07-05 06:09:02'),
(50, 85, 'MAXI', '042631808', NULL, 'tegas', NULL, '2019-07-05 06:12:43', '2019-07-05 06:12:43'),
(51, 88, 'SIAWSBW', '604-2631808', NULL, 'LIM', NULL, '2019-07-05 06:23:18', '2019-07-05 06:23:18'),
(52, 90, 'DAYETSBW', '604-2631808', NULL, 'LIM', NULL, '2019-07-05 06:39:57', '2019-07-05 06:39:57'),
(53, 92, 'TAIYOKCH', '604-2631808', NULL, 'LIM', NULL, '2019-07-05 07:01:58', '2019-07-05 07:01:58'),
(54, 93, 'HOOSBW', '604-2631808', NULL, 'LIM', NULL, '2019-07-05 07:59:32', '2019-07-05 07:59:32'),
(55, 95, 'WEYSING', '604-2631808', NULL, 'TEGAS', NULL, '2019-07-06 01:21:05', '2019-07-06 01:21:05'),
(56, 97, 'EMPIRE', '042631808', NULL, 'SHS', NULL, '2019-07-06 01:36:00', '2019-07-06 01:36:00'),
(57, 98, 'YHL', '042631808', NULL, 'MANDY', NULL, '2019-07-06 01:36:56', '2019-07-06 01:36:56'),
(58, 102, 'MINSIANGSBW', '604-2631808', NULL, 'TEGAS', NULL, '2019-07-06 02:18:35', '2019-07-06 02:18:35'),
(59, 103, 'HUNGBKI', '604-2631808', NULL, 'TEGAS', NULL, '2019-07-06 03:00:47', '2019-07-06 03:00:47'),
(60, 105, 'NIBONGENTKK', '604-2631808', NULL, 'TEGAS', NULL, '2019-07-06 04:06:14', '2019-07-06 04:06:14'),
(61, 106, 'JAYASDK', '042631808', NULL, 'JAYA', NULL, '2019-07-06 04:16:37', '2019-07-06 04:16:37');

-- --------------------------------------------------------

--
-- Table structure for table `default_charges`
--

DROP TABLE IF EXISTS `default_charges`;
CREATE TABLE IF NOT EXISTS `default_charges` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `shipper_id` int(10) UNSIGNED NOT NULL,
  `charge_id` int(10) UNSIGNED NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_size` int(11) NOT NULL,
  `price` double NOT NULL,
  `pol` int(10) UNSIGNED DEFAULT NULL,
  `final_dest` int(10) UNSIGNED DEFAULT NULL,
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `default_charges_shipper_id_foreign` (`shipper_id`),
  KEY `default_charges_charge_id_foreign` (`charge_id`),
  KEY `default_charges_pol_foreign` (`pol`),
  KEY `default_charges_final_dest_foreign` (`final_dest`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `default_charges`
--

INSERT INTO `default_charges` (`id`, `shipper_id`, `charge_id`, `unit`, `unit_size`, `price`, `pol`, `final_dest`, `payment`, `created_at`, `updated_at`) VALUES
(4, 6, 2, 'GP', 20, 1061, 1, 2, 'P', '2019-06-04 07:53:11', '2019-06-04 07:53:11'),
(3, 6, 3, 'GP', 20, 550, 1, 2, 'P', '2019-06-04 07:51:54', '2019-06-04 07:52:03');

-- --------------------------------------------------------

--
-- Table structure for table `inward_bl`
--

DROP TABLE IF EXISTS `inward_bl`;
CREATE TABLE IF NOT EXISTS `inward_bl` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vessel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipment_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voyage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bl_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pol_id` int(10) UNSIGNED DEFAULT NULL,
  `pol_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pod_id` int(10) UNSIGNED DEFAULT NULL,
  `pod_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpd_id` int(10) UNSIGNED DEFAULT NULL,
  `fpd_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipper_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipper_add` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consignee_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consignee_add` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notify_add` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_bl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_packages` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `containers` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cargo_desc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detailed_desc` text COLLATE utf8mb4_unicode_ci,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight_unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `volume` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `date_of_issue` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inward_bl_pol_id_foreign` (`pol_id`),
  KEY `inward_bl_pod_id_foreign` (`pod_id`),
  KEY `inward_bl_fpd_id_foreign` (`fpd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_06_12_082146_create_bl_formats_table', 1),
(4, '2018_06_14_054951_create_charges_table', 1),
(5, '2018_06_14_064310_create_companies_table', 1),
(6, '2018_06_14_065314_create_shippers_table', 1),
(7, '2018_06_14_065351_create_consignees_table', 1),
(8, '2018_06_14_071837_create_company_addresses_table', 1),
(9, '2018_06_14_072609_create_company_emails_table', 1),
(10, '2018_06_14_073209_create_ports_table', 1),
(11, '2018_06_14_073828_create_bill_ladings_table', 1),
(12, '2018_06_14_074125_create_agents_table', 1),
(13, '2018_06_14_074514_create_vessels_table', 1),
(14, '2018_06_14_075152_create_shipper_restrictions_table', 1),
(15, '2018_06_24_081937_create_shortcut_keys_table', 1),
(16, '2018_06_24_082003_create_packings_table', 1),
(17, '2018_06_28_021830_create_voyages_table', 1),
(18, '2018_06_28_021854_create_voyage_destinations_table', 1),
(19, '2018_07_02_074146_create_chargedetails_table', 1),
(20, '2018_07_02_081800_create_default_charges_table', 1),
(21, '2018_07_25_172824_create_bl_number', 1),
(22, '2018_08_03_152759_create_bl_cargos_table', 1),
(23, '2018_08_03_152845_create_bl_charges_table', 1),
(24, '2018_08_03_152859_create_cargo_details_table', 1),
(25, '2018_08_20_163234_create_reservations_table', 1),
(26, '2018_08_21_002708_create_bookings_table', 1),
(27, '2018_08_24_114720_alter_bookings_table', 1),
(28, '2018_08_24_132252_create_booking_amendments_table', 1),
(29, '2018_08_24_140944_alter_bookings_table1', 1),
(30, '2018_08_24_182542_alter_amendment_table', 1),
(31, '2018_08_30_102921_add_booking_id_reservation', 1),
(32, '2018_09_26_203518_alter_bl_table', 1),
(33, '2018_09_27_134832_add_res_id', 1),
(34, '2018_10_03_073132_add_description_address', 1),
(35, '2018_10_07_182436_add_mship_col', 1),
(36, '2018_10_24_112531_alter_mvessel_col', 1),
(37, '2018_10_25_211057_rename_telex_path', 1),
(38, '2018_10_31_124619_create_inward_b_l_table', 1),
(39, '2018_11_02_135427_add_softdelete_inw', 1),
(40, '2018_11_02_154157_add_no_packages_col', 1),
(41, '2018_11_12_013509_add_port_column_inward', 1),
(42, '2018_11_26_170932_alter_inwardbl_table', 1),
(43, '2018_11_26_214126_alter_text_inw', 1),
(44, '2019_01_02_153859_rename_booking_cols', 1),
(45, '2019_01_02_161430_add_eta_bookings', 1);

-- --------------------------------------------------------

--
-- Table structure for table `packings`
--

DROP TABLE IF EXISTS `packings`;
CREATE TABLE IF NOT EXISTS `packings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `packing_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ports`
--

DROP TABLE IF EXISTS `ports`;
CREATE TABLE IF NOT EXISTS `ports` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bl_prefix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `station_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ports`
--

INSERT INTO `ports` (`id`, `name`, `code`, `location`, `location_code`, `bl_prefix`, `station_code`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'PENANG, MALAYSIA', 'MYPEN', 'Penang Malaysia', 'PM', 'PG', 'P14', NULL, '2019-04-05 09:38:57', '2019-07-05 05:37:57'),
(2, 'KUCHING, SARAWAK', 'MYKCH', 'Kuching Sarawak', 'KS', 'KCH', 'Y44', NULL, '2019-04-05 09:39:12', '2019-07-05 05:38:05'),
(3, 'KOTA KINABALU, SABAH', 'MYBKI', 'Kota Kinabalu Sabah', 'KKS', 'BKI', 'S87', NULL, '2019-04-08 07:52:49', '2019-07-05 05:38:13'),
(4, 'PORT KLANG, MALAYSIA', 'MYPKG', 'Port Klang Malaysia', 'PKM', 'PKG', 'CT1', NULL, '2019-04-08 08:19:19', '2019-07-05 05:38:25'),
(5, 'SIBU, SARAWAK', 'MYSBW', 'Sibu Sarawak', 'SS', 'SBW', 'Y13', NULL, '2019-04-08 08:19:40', '2019-07-05 05:38:31'),
(6, 'BINTULU, SARAWAK', 'MYBTU', 'Bintulu Sarawak', 'BS', 'BTU', 'Y11', NULL, '2019-06-27 04:38:57', '2019-07-05 05:38:38'),
(7, 'SANDAKAN, SABAH', 'MYSDK', 'Sandakan Sabah', 'SS', 'SDK', 'S14', NULL, '2019-06-27 04:42:29', '2019-07-05 05:38:47'),
(8, 'TAWAU, SABAH', 'MYTWU', 'Tawau Sabah', 'TS', 'TWU', 'S12', NULL, '2019-06-27 04:42:54', '2019-07-05 05:38:54'),
(9, 'MIRI, SARAWAK', 'MYMYY', 'Miri Sarawak', 'MS', 'MYY', 'Y12', NULL, '2019-06-27 04:48:14', '2019-07-05 05:39:01');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `shipper_id` int(10) UNSIGNED NOT NULL,
  `voyage_id` int(10) UNSIGNED DEFAULT NULL,
  `pod_id` int(10) UNSIGNED NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reservations_shipper_id_foreign` (`shipper_id`),
  KEY `reservations_voyage_id_foreign` (`voyage_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `restrictions`
--

DROP TABLE IF EXISTS `restrictions`;
CREATE TABLE IF NOT EXISTS `restrictions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `shipper_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `restrictions_shipper_id_foreign` (`shipper_id`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `restrictions`
--

INSERT INTO `restrictions` (`id`, `shipper_id`, `type`, `value`, `created_at`, `updated_at`) VALUES
(5, 4, 'type', 'General Cargo Vessel', '2019-06-04 06:34:37', '2019-06-04 06:34:37'),
(3, 3, 'name', 'HARBOUR EXPRESS', '2019-04-08 08:23:27', '2019-04-08 08:23:27'),
(4, 3, 'type', 'General Cargo Vessel', '2019-04-08 08:23:27', '2019-04-08 08:23:27'),
(7, 6, 'type', '0', '2019-06-04 06:51:07', '2019-06-04 06:51:07'),
(9, 8, 'type', '0', '2019-06-04 07:05:46', '2019-06-04 07:05:46'),
(10, 9, 'type', '0', '2019-06-27 05:00:45', '2019-06-27 05:00:45'),
(11, 10, 'type', '0', '2019-06-27 05:02:13', '2019-06-27 05:02:13'),
(13, 12, 'type', '0', '2019-06-27 07:58:22', '2019-06-27 07:58:22'),
(14, 13, 'type', '0', '2019-06-27 08:07:14', '2019-06-27 08:07:14'),
(15, 14, 'type', '0', '2019-06-27 08:12:56', '2019-06-27 08:12:56'),
(16, 15, 'type', '0', '2019-07-01 01:47:00', '2019-07-01 01:47:00'),
(17, 16, 'type', '0', '2019-07-01 03:17:59', '2019-07-01 03:17:59'),
(18, 17, 'type', '0', '2019-07-01 03:18:51', '2019-07-01 03:18:51'),
(19, 18, 'type', '0', '2019-07-01 03:20:02', '2019-07-01 03:20:02'),
(20, 19, 'type', '0', '2019-07-01 03:22:40', '2019-07-01 03:22:40'),
(21, 20, 'type', '0', '2019-07-01 03:24:11', '2019-07-01 03:24:11'),
(22, 21, 'type', '0', '2019-07-01 03:25:23', '2019-07-01 03:25:23'),
(25, 24, 'type', '0', '2019-07-01 03:28:06', '2019-07-01 03:28:06'),
(24, 23, 'type', '0', '2019-07-01 03:27:46', '2019-07-01 03:27:46'),
(26, 25, 'type', '0', '2019-07-01 03:29:12', '2019-07-01 03:29:12'),
(27, 26, 'type', '0', '2019-07-01 03:30:08', '2019-07-01 03:30:08'),
(28, 27, 'type', '0', '2019-07-01 03:31:03', '2019-07-01 03:31:03'),
(29, 28, 'type', '0', '2019-07-01 03:32:12', '2019-07-01 03:32:12'),
(30, 29, 'type', '0', '2019-07-01 08:32:15', '2019-07-01 08:32:15'),
(31, 30, 'type', '0', '2019-07-01 08:41:10', '2019-07-01 08:41:10'),
(32, 31, 'type', '0', '2019-07-02 02:43:45', '2019-07-02 02:43:45'),
(33, 32, 'type', '0', '2019-07-02 02:50:20', '2019-07-02 02:50:20'),
(34, 33, 'type', '0', '2019-07-02 03:47:10', '2019-07-02 03:47:10'),
(35, 34, 'type', '0', '2019-07-02 03:48:26', '2019-07-02 03:48:26'),
(36, 35, 'type', '0', '2019-07-02 04:04:36', '2019-07-02 04:04:36'),
(37, 36, 'type', '0', '2019-07-02 06:49:53', '2019-07-02 06:49:53'),
(38, 37, 'type', '0', '2019-07-02 07:23:51', '2019-07-02 07:23:51'),
(39, 38, 'type', '0', '2019-07-03 04:36:13', '2019-07-03 04:36:13'),
(40, 39, 'type', '0', '2019-07-03 04:40:26', '2019-07-03 04:40:26'),
(41, 40, 'type', '0', '2019-07-03 04:44:12', '2019-07-03 04:44:12'),
(42, 41, 'type', '0', '2019-07-03 04:48:38', '2019-07-03 04:48:38'),
(43, 42, 'type', '0', '2019-07-03 04:53:25', '2019-07-03 04:53:25'),
(44, 43, 'type', '0', '2019-07-03 04:56:05', '2019-07-03 04:56:05'),
(45, 44, 'type', '0', '2019-07-03 06:22:00', '2019-07-03 06:22:00'),
(46, 45, 'type', '0', '2019-07-03 06:24:47', '2019-07-03 06:24:47'),
(47, 46, 'type', '0', '2019-07-04 03:51:15', '2019-07-04 03:51:15'),
(48, 47, 'type', '0', '2019-07-04 03:54:20', '2019-07-04 03:54:20'),
(49, 48, 'type', '0', '2019-07-04 03:58:45', '2019-07-04 03:58:45'),
(50, 49, 'type', '0', '2019-07-04 04:01:48', '2019-07-04 04:01:48'),
(51, 50, 'type', '0', '2019-07-04 04:27:46', '2019-07-04 04:27:46'),
(52, 51, 'type', '0', '2019-07-04 04:30:15', '2019-07-04 04:30:15'),
(53, 52, 'type', '0', '2019-07-04 04:33:15', '2019-07-04 04:33:15'),
(54, 53, 'type', '0', '2019-07-04 06:25:09', '2019-07-04 06:25:09'),
(55, 54, 'type', '0', '2019-07-04 06:42:33', '2019-07-04 06:42:33'),
(56, 55, 'type', '0', '2019-07-04 06:45:12', '2019-07-04 06:45:12'),
(57, 56, 'type', '0', '2019-07-04 07:04:32', '2019-07-04 07:04:32'),
(58, 57, 'type', '0', '2019-07-04 07:22:22', '2019-07-04 07:22:22'),
(59, 58, 'type', '0', '2019-07-04 07:43:38', '2019-07-04 07:43:38'),
(60, 59, 'type', '0', '2019-07-04 08:18:20', '2019-07-04 08:18:20'),
(61, 60, 'type', '0', '2019-07-05 00:41:27', '2019-07-05 00:41:27'),
(62, 61, 'type', '0', '2019-07-05 00:52:24', '2019-07-05 00:52:24'),
(63, 62, 'type', '0', '2019-07-05 01:11:54', '2019-07-05 01:11:54'),
(64, 63, 'type', '0', '2019-07-05 01:16:55', '2019-07-05 01:16:55'),
(65, 64, 'type', '0', '2019-07-05 01:51:47', '2019-07-05 01:51:47'),
(66, 65, 'type', '0', '2019-07-05 01:57:36', '2019-07-05 01:57:36'),
(67, 66, 'type', '0', '2019-07-05 02:42:07', '2019-07-05 02:42:07'),
(68, 67, 'type', '0', '2019-07-05 03:23:34', '2019-07-05 03:23:34'),
(69, 68, 'type', '0', '2019-07-05 03:41:22', '2019-07-05 03:41:22'),
(70, 69, 'type', '0', '2019-07-05 03:43:28', '2019-07-05 03:43:28'),
(71, 70, 'type', '0', '2019-07-05 05:28:51', '2019-07-05 05:28:51'),
(72, 71, 'type', '0', '2019-07-05 05:34:10', '2019-07-05 05:34:10'),
(73, 72, 'type', '0', '2019-07-05 05:44:10', '2019-07-05 05:44:10'),
(74, 73, 'type', '0', '2019-07-05 05:52:16', '2019-07-05 05:52:16'),
(75, 74, 'type', '0', '2019-07-05 05:57:33', '2019-07-05 05:57:33'),
(76, 75, 'type', '0', '2019-07-05 06:11:25', '2019-07-05 06:11:25'),
(77, 76, 'type', '0', '2019-07-05 06:16:26', '2019-07-05 06:16:26'),
(78, 77, 'type', '0', '2019-07-05 06:21:15', '2019-07-05 06:21:15'),
(79, 78, 'type', '0', '2019-07-05 06:23:18', '2019-07-05 06:23:18'),
(80, 79, 'type', '0', '2019-07-05 06:38:04', '2019-07-05 06:38:04'),
(81, 80, 'type', '0', '2019-07-05 06:39:57', '2019-07-05 06:39:57'),
(82, 81, 'type', '0', '2019-07-05 06:58:32', '2019-07-05 06:58:32'),
(83, 82, 'type', '0', '2019-07-05 07:01:58', '2019-07-05 07:01:58'),
(84, 83, 'type', '0', '2019-07-05 07:59:32', '2019-07-05 07:59:32'),
(85, 84, 'type', '0', '2019-07-06 01:18:43', '2019-07-06 01:18:43'),
(86, 85, 'type', '0', '2019-07-06 01:21:05', '2019-07-06 01:21:05'),
(87, 86, 'type', '0', '2019-07-06 01:35:02', '2019-07-06 01:35:02'),
(88, 87, 'type', '0', '2019-07-06 02:01:03', '2019-07-06 02:01:03'),
(89, 88, 'type', '0', '2019-07-06 02:03:14', '2019-07-06 02:03:14'),
(90, 89, 'type', '0', '2019-07-06 02:16:53', '2019-07-06 02:16:53'),
(91, 90, 'type', '0', '2019-07-06 02:18:35', '2019-07-06 02:18:35'),
(92, 91, 'type', '0', '2019-07-06 03:00:47', '2019-07-06 03:00:47'),
(93, 92, 'type', '0', '2019-07-06 03:13:05', '2019-07-06 03:13:05'),
(94, 93, 'type', '0', '2019-07-06 04:06:14', '2019-07-06 04:06:14');

-- --------------------------------------------------------

--
-- Table structure for table `shippers`
--

DROP TABLE IF EXISTS `shippers`;
CREATE TABLE IF NOT EXISTS `shippers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voyage_updates` int(11) NOT NULL,
  `delivery_updates` int(11) NOT NULL,
  `handling_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shippers_company_id_foreign` (`company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shippers`
--

INSERT INTO `shippers` (`id`, `company_id`, `code`, `telephone`, `fax`, `pic`, `voyage_updates`, `delivery_updates`, `handling_method`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '042222222', '042222222', 'a', 0, 0, 'Direct Delivery', '2019-04-05 09:26:03', '2019-04-05 08:26:38', '2019-04-05 09:26:03'),
(2, 2, 'NAMYE', '04-2622868', '04-2622869', 'ANNIE', 0, 0, 'Direct Delivery', '2019-06-04 06:32:12', '2019-04-05 09:38:18', '2019-06-04 06:32:12'),
(3, 4, 'SHS', '04-2626871', '04-2632063', 'JIAMIN', 0, 0, 'Direct Delivery', NULL, '2019-04-08 08:23:27', '2019-04-08 08:23:27'),
(4, 5, '2448', '04-2631808', NULL, 's', 0, 0, 'Direct Delivery', NULL, '2019-06-04 06:34:37', '2019-06-04 06:34:37'),
(5, 6, '2302', '04-2631808', NULL, 's', 0, 0, 'Direct Delivery', '2019-06-04 06:38:16', '2019-06-04 06:36:07', '2019-06-04 06:38:16'),
(6, 7, '1061', '04-2631808', NULL, 'teng', 0, 0, 'Direct Delivery', NULL, '2019-06-04 06:51:07', '2019-06-04 06:51:07'),
(7, 8, '1270', '04-2631808', NULL, 'TENG', 0, 0, 'Direct Delivery', '2019-06-04 07:05:46', '2019-06-04 06:52:08', '2019-06-04 07:05:46'),
(8, 8, '1270', '04-2631808', NULL, 'TENG', 0, 0, 'Direct Delivery', NULL, '2019-06-04 07:05:46', '2019-06-04 07:05:46'),
(9, 9, 'BARKATH', '042631808', NULL, 'tegas', 0, 0, 'Direct Delivery', NULL, '2019-06-27 05:00:45', '2019-06-27 05:00:45'),
(10, 10, '1028', '042631808', NULL, 'tegas', 0, 0, 'Direct Delivery', NULL, '2019-06-27 05:02:13', '2019-06-27 05:02:13'),
(11, 11, 'TEGA', '604-2631808', NULL, 'MR LIM', 0, 0, 'Direct Delivery', '2019-06-27 07:58:21', '2019-06-27 07:57:10', '2019-06-27 07:58:21'),
(12, 11, 'TEGA', '604-2631808', NULL, 'MR LIM', 0, 0, 'Direct Delivery', NULL, '2019-06-27 07:58:21', '2019-06-27 07:58:21'),
(13, 12, 'TEGA', '604-2631808', NULL, 'MR LIM', 0, 0, 'Direct Delivery', NULL, '2019-06-27 08:07:14', '2019-06-27 08:07:14'),
(14, 13, 'TEGA', '601-2631808', NULL, 'MR LIM', 0, 0, 'Direct Delivery', NULL, '2019-06-27 08:12:56', '2019-06-27 08:12:56'),
(15, 15, 'CABLE', '042631808', NULL, 'TEGAS', 0, 0, 'Direct Delivery', NULL, '2019-07-01 01:47:00', '2019-07-01 01:47:00'),
(16, 16, 'SILVERSTONE', '042631808', NULL, 'rex', 0, 0, 'Direct Delivery', NULL, '2019-07-01 03:17:59', '2019-07-01 03:17:59'),
(17, 17, 'KNY', '042631808', NULL, 'kny', 0, 0, 'Direct Delivery', NULL, '2019-07-01 03:18:51', '2019-07-01 06:39:41'),
(18, 18, 'SIL', '042631808', NULL, 'SIL', 0, 0, 'Direct Delivery', NULL, '2019-07-01 03:20:02', '2019-07-01 03:20:02'),
(19, 19, 'NSA', '042631808', NULL, 'NSA', 0, 0, 'Direct Delivery', NULL, '2019-07-01 03:22:40', '2019-07-01 03:22:40'),
(20, 20, 'PENGTAH', '042631808', NULL, 'peng', 0, 0, 'Direct Delivery', NULL, '2019-07-01 03:24:11', '2019-07-01 03:24:11'),
(21, 21, 'tire', '042631808', NULL, 'saba', 0, 0, 'Direct Delivery', NULL, '2019-07-01 03:25:23', '2019-07-01 03:25:23'),
(22, 22, 'ZHENG', '042631808', NULL, 'zheng', 0, 0, 'Direct Delivery', '2019-07-01 03:28:06', '2019-07-01 03:26:34', '2019-07-01 03:28:06'),
(23, 23, 'MHMIRI', '042631808', NULL, 'MOH HENG', 0, 0, 'Direct Delivery', NULL, '2019-07-01 03:27:46', '2019-07-01 03:27:46'),
(24, 22, 'ZHENG', '042631808', NULL, 'zheng', 0, 0, 'Direct Delivery', NULL, '2019-07-01 03:28:06', '2019-07-01 03:28:06'),
(25, 24, 'SYN', '042631808', NULL, 'SYN', 0, 0, 'Direct Delivery', NULL, '2019-07-01 03:29:12', '2019-07-01 03:29:12'),
(26, 25, 'VIN', '042631808', NULL, 'SYN', 0, 0, 'Direct Delivery', NULL, '2019-07-01 03:30:08', '2019-07-01 03:30:08'),
(27, 26, 'REX', '042631808', NULL, 'SWEE NEE', 0, 0, 'Direct Delivery', NULL, '2019-07-01 03:31:03', '2019-07-01 03:31:03'),
(28, 27, 'TRANS', '042631808', NULL, 'TRR', 0, 0, 'Direct Delivery', NULL, '2019-07-01 03:32:12', '2019-07-01 03:32:12'),
(29, 28, 'SEASON', '042631808', NULL, 'chai', 0, 0, 'Direct Delivery', NULL, '2019-07-01 08:32:15', '2019-07-01 08:32:15'),
(30, 30, 'NIBONG7278', '042631808', NULL, 'nibong', 0, 0, 'Direct Delivery', NULL, '2019-07-01 08:41:10', '2019-07-01 08:41:10'),
(31, 31, 'JATI', '042631808', NULL, 'jati', 0, 0, 'Direct Delivery', NULL, '2019-07-02 02:43:45', '2019-07-02 02:43:45'),
(32, 32, 'HOMEMART', '042632808', NULL, 'home', 0, 0, 'Direct Delivery', NULL, '2019-07-02 02:50:20', '2019-07-02 02:50:20'),
(33, 33, 'GOLDLEAF', '042631808', NULL, 'gold leaf', 0, 0, 'Direct Delivery', NULL, '2019-07-02 03:47:10', '2019-07-02 03:47:10'),
(34, 34, 'GLMIRI', '042631808', NULL, 'gl', 0, 0, 'Direct Delivery', NULL, '2019-07-02 03:48:26', '2019-07-02 03:48:26'),
(35, 35, 'NANYANG', '042631808', NULL, 'nan', 0, 0, 'Direct Delivery', NULL, '2019-07-02 04:04:36', '2019-07-02 04:04:36'),
(36, 37, 'HMM', '042631808', NULL, 'hmm', 0, 0, 'Direct Delivery', NULL, '2019-07-02 06:49:53', '2019-07-02 06:49:53'),
(37, 39, 'IAL', '042631808', NULL, 'GRACE', 0, 0, 'Direct Delivery', NULL, '2019-07-02 07:23:51', '2019-07-02 07:23:51'),
(38, 41, 'PEN', '604-2631808', '604-2631808', 'MRLIM', 0, 0, 'Direct Delivery', NULL, '2019-07-03 04:36:13', '2019-07-03 04:36:13'),
(39, 42, 'PEN', '604-2631808', '604-2631808', 'MR LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-03 04:40:26', '2019-07-03 04:40:26'),
(40, 43, 'CHOI', '604-2631808', '604-2631808', 'MRL LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-03 04:44:12', '2019-07-03 04:44:12'),
(41, 44, 'SENG', '604-2631808', '604-2631808', 'MR LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-03 04:48:38', '2019-07-03 04:48:38'),
(42, 45, 'TAIKO', '604-2631808', NULL, 'MR LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-03 04:53:25', '2019-07-03 04:53:25'),
(43, 46, 'CMS', '604-2631808', NULL, 'MR LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-03 04:56:05', '2019-07-03 04:56:05'),
(44, 47, 'LOGWIN', '604-2631808', NULL, 'MR LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-03 06:22:00', '2019-07-03 06:22:00'),
(45, 48, 'SWEE', '604-2631808', NULL, 'MR LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-03 06:24:47', '2019-07-03 06:24:47'),
(46, 49, 'SKY', '604-2631808', NULL, 'MR LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-04 03:51:15', '2019-07-04 03:51:15'),
(47, 50, 'CAHAYA', '604-2631808', NULL, 'MR LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-04 03:54:20', '2019-07-04 03:54:20'),
(48, 51, 'NBC', '601-2631808', NULL, 'MR LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-04 03:58:45', '2019-07-04 03:58:45'),
(49, 52, 'YAWHON', '604-2631808', NULL, 'MR LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-04 04:01:48', '2019-07-04 04:01:48'),
(50, 53, 'MSMPERLIS', '604-2621808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-04 04:27:46', '2019-07-04 04:27:46'),
(51, 54, 'AWMTWU', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-04 04:30:15', '2019-07-04 04:30:15'),
(52, 55, 'RUNCHITSBW', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-04 04:33:15', '2019-07-04 04:33:15'),
(53, 56, 'CKWONGSDK', '042631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-04 06:25:09', '2019-07-04 06:25:09'),
(54, 57, 'GUAN', '6042631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-04 06:42:33', '2019-07-04 06:42:33'),
(55, 58, 'CHUNKCH', '6042631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-04 06:45:12', '2019-07-04 06:45:12'),
(56, 59, 'CFTWU', '6042631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-04 07:04:32', '2019-07-04 07:04:32'),
(57, 60, 'EMPIREKCH', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-04 07:22:22', '2019-07-04 07:22:22'),
(58, 61, 'KCHEONG', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-04 07:43:38', '2019-07-04 07:43:38'),
(59, 62, 'NTPM886', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-04 08:18:20', '2019-07-04 08:18:20'),
(60, 63, 'BIO', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 00:41:27', '2019-07-05 00:41:27'),
(61, 64, 'SAWMILL', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 00:52:24', '2019-07-05 00:52:24'),
(62, 65, 'TOYO', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 01:11:54', '2019-07-05 01:11:54'),
(63, 66, 'TYRE', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 01:16:55', '2019-07-05 01:16:55'),
(64, 67, 'LISBLE', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 01:51:47', '2019-07-05 01:51:47'),
(65, 68, 'LISBLEKCH', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 01:57:36', '2019-07-05 01:57:36'),
(66, 69, 'MSMKCH', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 02:42:07', '2019-07-05 02:42:07'),
(67, 70, 'SASTRIA', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 03:23:34', '2019-07-05 03:23:34'),
(68, 71, 'GOLD', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 03:41:22', '2019-07-05 03:41:22'),
(69, 72, 'GOLDKCH', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 03:43:28', '2019-07-05 03:43:28'),
(70, 73, 'ECONS', '042631808', NULL, 'tegas', 0, 0, 'Direct Delivery', NULL, '2019-07-05 05:28:51', '2019-07-05 05:28:51'),
(71, 75, 'YML', '04-2631808', NULL, 'TEGAS', 0, 0, 'Direct Delivery', NULL, '2019-07-05 05:34:10', '2019-07-05 05:34:10'),
(72, 77, 'INFINITY', '042631808', NULL, 'TEGAS', 0, 0, 'Direct Delivery', NULL, '2019-07-05 05:44:10', '2019-07-05 05:44:10'),
(73, 79, 'SINOKOR', '042631808', NULL, 'tegas', 0, 0, 'Direct Delivery', NULL, '2019-07-05 05:52:16', '2019-07-05 05:52:16'),
(74, 81, 'OOCL', '042631808', NULL, 'TEGAS', 0, 0, 'Direct Delivery', NULL, '2019-07-05 05:57:33', '2019-07-05 05:57:33'),
(75, 84, 'MAX', '04-2631808', NULL, 'tegas', 0, 0, 'Direct Delivery', NULL, '2019-07-05 06:11:25', '2019-07-05 06:11:25'),
(76, 86, 'AK', '042631808', NULL, 'TEGAS', 0, 0, 'Direct Delivery', NULL, '2019-07-05 06:16:26', '2019-07-05 06:16:26'),
(77, 87, 'JINTYE', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 06:21:15', '2019-07-05 06:21:15'),
(78, 88, 'SIAWSBW', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 06:23:18', '2019-07-05 06:23:18'),
(79, 89, 'KGUAN', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 06:38:04', '2019-07-05 06:38:04'),
(80, 90, 'DAYETSBW', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 06:39:57', '2019-07-05 06:39:57'),
(81, 91, 'VEOLIA', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 06:58:32', '2019-07-05 06:58:32'),
(82, 92, 'TAIYOKCH', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 07:01:58', '2019-07-05 07:01:58'),
(83, 93, 'HOOSBW', '604-2631808', NULL, 'LIM', 0, 0, 'Direct Delivery', NULL, '2019-07-05 07:59:32', '2019-07-05 07:59:32'),
(84, 94, 'PROFIT', '604-2631808', NULL, 'TEGAS', 0, 0, 'Direct Delivery', NULL, '2019-07-06 01:18:43', '2019-07-06 01:18:43'),
(85, 95, 'WEYSING', '604-2631808', NULL, 'TEGAS', 0, 0, 'Direct Delivery', NULL, '2019-07-06 01:21:05', '2019-07-06 01:21:05'),
(86, 96, 'AUSSIE', '042631808', NULL, 'MANDY', 0, 0, 'Direct Delivery', NULL, '2019-07-06 01:35:02', '2019-07-06 01:35:02'),
(87, 99, 'SEBERANG', '604-2631808', NULL, 'TEGAS', 0, 0, 'Direct Delivery', NULL, '2019-07-06 02:01:03', '2019-07-06 02:01:03'),
(88, 100, 'HINGBKI', '604-2631808', NULL, 'TEGAS', 0, 0, 'Direct Delivery', NULL, '2019-07-06 02:03:14', '2019-07-06 02:03:14'),
(89, 101, 'HWAEIK', '604-2631808', NULL, 'TEGAS', 0, 0, 'Direct Delivery', NULL, '2019-07-06 02:16:53', '2019-07-06 02:16:53'),
(90, 102, 'MINSIANGSBW', '604-2631808', NULL, 'TEGAS', 0, 0, 'Direct Delivery', NULL, '2019-07-06 02:18:35', '2019-07-06 02:18:35'),
(91, 103, 'HUNGBKI', '604-2631808', NULL, 'TEGAS', 0, 0, 'Direct Delivery', NULL, '2019-07-06 03:00:47', '2019-07-06 03:00:47'),
(92, 104, 'FIBRE', '604-2631808', NULL, 'TEGAS', 0, 0, 'Direct Delivery', NULL, '2019-07-06 03:13:05', '2019-07-06 03:13:05'),
(93, 105, 'NIBONGENTKK', '604-2631808', NULL, 'TEGAS', 0, 0, 'Direct Delivery', NULL, '2019-07-06 04:06:14', '2019-07-06 04:06:14');

-- --------------------------------------------------------

--
-- Table structure for table `shortcut_keys`
--

DROP TABLE IF EXISTS `shortcut_keys`;
CREATE TABLE IF NOT EXISTS `shortcut_keys` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `shortcut_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'tegas', '$2y$10$pUogOSrQB8lArdnPkckr8.v3GPwU0eOa3k4annF46FipjEAsnRJFO', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vessels`
--

DROP TABLE IF EXISTS `vessels`;
CREATE TABLE IF NOT EXISTS `vessels` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dwt` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semicontainer_service` int(11) NOT NULL,
  `manufactured_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_shipment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vessels`
--

INSERT INTO `vessels` (`id`, `name`, `type`, `dwt`, `semicontainer_service`, `manufactured_year`, `flag`, `default_shipment`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'DANUM 159', 'Container Vessel', '200', 0, '2019', 'Malaysia', 'FCL', NULL, '2019-04-05 09:37:58', '2019-04-11 06:15:19'),
(2, 'DANUM 171', 'Container Vessel', '1', 0, '2010', 'Malaysia', 'FCL', '2019-04-08 07:38:49', '2019-04-08 07:38:40', '2019-04-08 07:38:49'),
(3, 'DANUM 171', 'Container Vessel', '200', 0, '2011', 'Malaysia', 'FCL', NULL, '2019-04-08 07:42:28', '2019-04-08 07:42:28'),
(4, 'HARBOUR RUBY', 'Container Vessel', '200', 0, '2015', 'Malaysia', 'FCL', NULL, '2019-04-08 08:17:24', '2019-06-27 04:29:52'),
(5, 'DANUM 18', 'Container Vessel', '200', 0, '2009', 'Malaysia', 'FCL', '2019-06-04 06:21:09', '2019-04-08 08:25:55', '2019-06-04 06:21:09'),
(6, 'DANUM 160', 'Container Vessel', '300', 0, '1996', 'Malaysia', 'FCL', NULL, '2019-06-04 06:21:37', '2019-06-04 06:21:37');

-- --------------------------------------------------------

--
-- Table structure for table `voyages`
--

DROP TABLE IF EXISTS `voyages`;
CREATE TABLE IF NOT EXISTS `voyages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `voyage_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pol_id` int(10) UNSIGNED DEFAULT NULL,
  `eta_pol` datetime DEFAULT NULL,
  `ata_pol` datetime DEFAULT NULL,
  `etd_pol` datetime NOT NULL,
  `atd_pol` datetime DEFAULT NULL,
  `vessel_id` int(10) UNSIGNED NOT NULL,
  `scn` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `booking_status` int(11) NOT NULL,
  `closing_timestamp` datetime DEFAULT NULL,
  `cancel_remark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `voyages_pol_id_foreign` (`pol_id`),
  KEY `voyages_vessel_id_foreign` (`vessel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `voyages`
--

INSERT INTO `voyages` (`id`, `voyage_id`, `pol_id`, `eta_pol`, `ata_pol`, `etd_pol`, `atd_pol`, `vessel_id`, `scn`, `booking_status`, `closing_timestamp`, `cancel_remark`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '71019', 1, '2019-04-12 00:00:00', NULL, '2019-04-13 00:00:00', NULL, 3, NULL, 0, '2019-04-09 16:06:30', NULL, '2019-06-04 06:22:13', '2019-04-05 09:39:30', '2019-06-04 06:22:13'),
(2, 'HE054', 1, '2019-04-17 00:00:00', NULL, '2019-04-17 00:00:00', NULL, 4, NULL, 0, '2019-04-16 16:39:15', NULL, '2019-06-04 06:23:19', '2019-04-08 08:21:01', '2019-06-04 06:23:19'),
(3, '8434', 4, '2019-04-22 00:00:00', NULL, '2019-04-23 00:00:00', NULL, 5, NULL, 0, '2019-04-21 16:38:28', NULL, '2019-06-04 06:20:59', '2019-04-08 08:33:15', '2019-06-04 06:20:59'),
(4, '60103', 1, '2019-06-08 00:00:00', '2019-06-09 00:00:00', '2019-06-09 00:00:00', '2019-06-10 00:00:00', 6, '196044', 0, '2019-06-07 14:47:34', NULL, NULL, '2019-06-04 06:25:13', '2019-06-10 06:47:34'),
(5, 'TESTING', 1, '2019-06-09 00:00:00', '2019-06-09 00:00:00', '2019-06-11 00:00:00', '2019-06-11 00:00:00', 6, NULL, 0, '2019-06-08 17:16:09', NULL, '2019-06-04 09:17:24', '2019-06-04 09:15:24', '2019-06-04 09:17:24'),
(6, '59121', 1, '2019-06-15 00:00:00', '2019-06-15 00:00:00', '2019-06-16 00:00:00', '2019-06-16 00:00:00', 1, '196045', 0, '2019-06-14 14:50:05', NULL, NULL, '2019-06-10 06:50:05', '2019-06-10 06:50:05'),
(7, 'RB065', 1, '2019-07-03 00:00:00', '2019-07-03 00:00:00', '2019-07-04 00:00:00', '2019-07-04 00:00:00', 4, '197072', 0, '2019-07-02 14:47:00', NULL, NULL, '2019-06-27 04:35:24', '2019-07-01 06:47:00'),
(8, '60105', 1, '2019-07-06 00:00:00', '2019-07-06 00:00:00', '2019-07-07 00:00:00', '2019-07-07 00:00:00', 6, '197041', 0, '2019-07-05 08:39:06', NULL, NULL, '2019-07-03 00:37:32', '2019-07-03 00:39:06'),
(9, '71025', 1, '2019-07-09 00:00:00', '2019-07-10 00:00:00', '2019-07-10 00:00:00', '2019-07-12 00:00:00', 3, '197045', 0, '2019-07-08 14:28:56', NULL, NULL, '2019-07-05 04:02:47', '2019-07-05 06:28:56');

-- --------------------------------------------------------

--
-- Table structure for table `voyage_destinations`
--

DROP TABLE IF EXISTS `voyage_destinations`;
CREATE TABLE IF NOT EXISTS `voyage_destinations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `voyage_id` int(10) UNSIGNED NOT NULL,
  `port` int(10) UNSIGNED NOT NULL,
  `eta` datetime NOT NULL,
  `ata` datetime DEFAULT NULL,
  `etd` datetime NOT NULL,
  `atd` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `voyage_destinations_voyage_id_foreign` (`voyage_id`),
  KEY `voyage_destinations_port_foreign` (`port`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `voyage_destinations`
--

INSERT INTO `voyage_destinations` (`id`, `voyage_id`, `port`, `eta`, `ata`, `etd`, `atd`, `created_at`, `updated_at`) VALUES
(18, 6, 4, '2019-06-16 00:00:00', '2019-06-16 00:00:00', '2019-06-17 00:00:00', '2019-06-17 00:00:00', '2019-06-10 06:50:05', '2019-06-10 06:50:05'),
(17, 6, 1, '2019-06-15 00:00:00', '2019-06-15 00:00:00', '2019-06-16 00:00:00', '2019-06-16 00:00:00', '2019-06-10 06:50:05', '2019-06-10 06:50:05'),
(16, 4, 5, '2019-06-14 00:00:00', '2019-06-15 00:00:00', '2019-06-17 00:00:00', '2019-06-18 00:00:00', '2019-06-07 06:32:32', '2019-06-07 06:32:32'),
(10, 4, 1, '2019-06-08 00:00:00', '2019-06-09 00:00:00', '2019-06-09 00:00:00', '2019-06-10 00:00:00', '2019-06-04 06:37:33', '2019-06-10 06:47:34'),
(20, 6, 5, '2019-06-22 00:00:00', '2019-06-22 00:00:00', '2019-06-26 00:00:00', '2019-06-26 00:00:00', '2019-06-10 06:50:05', '2019-06-10 06:50:05'),
(19, 6, 2, '2019-06-17 00:00:00', '2019-06-17 00:00:00', '2019-06-21 00:00:00', '2019-06-21 00:00:00', '2019-06-10 06:50:05', '2019-06-10 06:50:05'),
(9, 4, 2, '2019-06-10 00:00:00', '2019-06-10 00:00:00', '2019-06-11 00:00:00', '2019-06-14 00:00:00', '2019-06-04 06:25:13', '2019-06-04 06:25:13'),
(21, 7, 1, '2019-07-03 00:00:00', '2019-07-03 00:00:00', '2019-07-04 00:00:00', '2019-07-04 00:00:00', '2019-06-27 04:35:24', '2019-06-27 04:35:24'),
(22, 7, 3, '2019-07-10 00:00:00', '2019-07-10 00:00:00', '2019-07-11 00:00:00', '2019-07-11 00:00:00', '2019-06-27 04:35:24', '2019-06-27 04:35:24'),
(23, 7, 4, '2019-07-04 00:00:00', '2019-07-04 00:00:00', '2019-07-05 00:00:00', '2019-07-05 00:00:00', '2019-06-27 04:39:36', '2019-06-27 04:39:36'),
(24, 7, 6, '2019-07-11 00:00:00', '2019-07-11 00:00:00', '2019-07-12 00:00:00', '2019-07-12 00:00:00', '2019-06-27 04:40:27', '2019-06-27 04:40:27'),
(25, 7, 5, '2019-07-08 00:00:00', '2019-07-08 00:00:00', '2019-07-10 00:00:00', '2019-07-10 00:00:00', '2019-06-27 04:46:38', '2019-06-27 04:46:38'),
(26, 7, 7, '2019-07-15 00:00:00', '2019-07-15 00:00:00', '2019-07-17 00:00:00', '2019-07-17 00:00:00', '2019-06-27 04:46:38', '2019-06-27 04:46:38'),
(27, 7, 8, '2019-07-17 00:00:00', '2019-07-17 00:00:00', '2019-07-20 00:00:00', '2019-07-20 00:00:00', '2019-06-27 04:47:26', '2019-06-27 04:47:26'),
(28, 7, 9, '2019-07-27 00:00:00', '2019-07-28 00:00:00', '2019-08-01 00:00:00', '2019-08-02 00:00:00', '2019-06-27 04:57:25', '2019-06-27 04:57:25'),
(29, 7, 2, '2019-08-09 00:00:00', '2019-08-10 00:00:00', '2019-08-14 00:00:00', '2019-08-15 00:00:00', '2019-07-01 06:47:00', '2019-07-01 06:47:00'),
(30, 8, 1, '2019-07-06 00:00:00', '2019-07-06 00:00:00', '2019-07-07 00:00:00', '2019-07-07 00:00:00', '2019-07-03 00:37:32', '2019-07-03 00:37:32'),
(31, 8, 4, '2019-07-08 00:00:00', '2019-07-08 00:00:00', '2019-07-09 00:00:00', '2019-07-09 00:00:00', '2019-07-03 00:37:32', '2019-07-03 00:37:32'),
(32, 8, 2, '2019-07-10 00:00:00', '2019-07-10 00:00:00', '2019-07-14 00:00:00', '2019-07-14 00:00:00', '2019-07-03 00:37:32', '2019-07-03 00:37:32'),
(33, 8, 5, '2019-07-15 00:00:00', '2019-07-15 00:00:00', '2019-07-19 00:00:00', '2019-07-19 00:00:00', '2019-07-03 00:37:32', '2019-07-03 00:37:32'),
(34, 8, 7, '2019-07-20 00:00:00', '2019-07-21 00:00:00', '2019-07-26 00:00:00', '2019-07-27 00:00:00', '2019-07-03 00:37:32', '2019-07-03 00:37:32'),
(35, 8, 8, '2019-07-29 00:00:00', '2019-07-30 00:00:00', '2019-08-01 00:00:00', '2019-08-02 00:00:00', '2019-07-03 00:37:32', '2019-07-03 00:37:32'),
(36, 8, 9, '2019-08-08 00:00:00', '2019-08-09 00:00:00', '2019-08-14 00:00:00', '2019-08-15 00:00:00', '2019-07-03 00:39:06', '2019-07-03 00:39:06'),
(37, 9, 1, '2019-07-09 00:00:00', '2019-07-10 00:00:00', '2019-07-10 00:00:00', '2019-07-12 00:00:00', '2019-07-05 04:02:47', '2019-07-05 06:28:56'),
(38, 9, 4, '2019-07-12 00:00:00', '2019-07-14 00:00:00', '2019-07-15 00:00:00', '2019-07-15 00:00:00', '2019-07-05 04:02:47', '2019-07-05 04:02:47'),
(39, 9, 3, '2019-07-16 00:00:00', '2019-07-17 00:00:00', '2019-07-22 00:00:00', '2019-07-23 00:00:00', '2019-07-05 04:02:47', '2019-07-05 04:02:47'),
(40, 9, 5, '2019-07-24 00:00:00', '2019-07-25 00:00:00', '2019-07-29 00:00:00', '2019-07-30 00:00:00', '2019-07-05 04:02:47', '2019-07-05 04:02:47'),
(41, 9, 6, '2019-08-01 00:00:00', '2019-08-02 00:00:00', '2019-08-03 00:00:00', '2019-08-04 00:00:00', '2019-07-05 04:02:47', '2019-07-05 04:02:47'),
(42, 9, 7, '2019-08-05 00:00:00', '2019-08-06 00:00:00', '2019-08-07 00:00:00', '2019-08-08 00:00:00', '2019-07-05 04:02:47', '2019-07-05 04:02:47'),
(43, 9, 8, '2019-08-09 00:00:00', '2019-08-10 00:00:00', '2019-08-11 00:00:00', '2019-08-12 00:00:00', '2019-07-05 04:02:47', '2019-07-05 04:02:47'),
(44, 9, 9, '2019-08-13 00:00:00', '2019-08-14 00:00:00', '2019-08-15 00:00:00', '2019-08-16 00:00:00', '2019-07-05 04:02:47', '2019-07-05 04:02:47');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
