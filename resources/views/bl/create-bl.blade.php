@extends("layouts.nav")
@section('page_title', 'Create BL')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('BillLadingController@index') }}">Bill of Lading</a></li>
	<li class="breadcrumb-item active">Create Bill of Lading</li>
	{{-- <li class="breadcrumb-item active">
		<button id="btn_duplicate" type="button" class="btn btn-success"><i class="fa fa-copy m-r-10"></i>Duplicate Other BL</button>
	</li> --}}
</ol>
@stop

@section('content')
<h1 class="page-header">
	Create Bill of Lading
	@if(!empty($bk))
	<span class="f-s-18">{{ $bk->booking_no }}</span>
	@endif
</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<div id="errMsg" class="alert alert-danger" style="display: none;">
</div>

<form id="bl_form" action="{{ action('BillLadingController@store') }}" enctype="multipart/form-data" method="POST">
	@csrf
	<div class="panel">
		<div class="panel-body">
			<h4>BL Details</h4>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Vessel</label>
						<select id="vessel_select" class="form-control form-control-lg ddl" name="vessel_id">
							<option disabled="" selected="" value>-- select an option --</option>
							@foreach($vessels AS $vessel)
							<option value="{{ $vessel->id }}" {{ (!empty($vessel_detail)) ? (($vessel_detail->id == $vessel->id) ? 'selected' : '') : ''}}>{{ $vessel->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Shipment Type</label>
						<select id="vessel_type" class="form-control form-control-lg" name="vessel_type">
							<option value="FCL">FCL</option>
							<option value="LCL">LCL</option>
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Voyage</label>
						<select id="voyage_select" class="ddl form-control form-control-lg" name="voyage_id">
							<option disabled="" selected="" value="-">-- select an option --</option>
							@foreach($voyages AS $voyage)
							<option value="{{ $voyage->id }}" {{ (!empty($voyage_detail)) ? (($voyage_detail->id == $voyage->id) ? 'selected' : '') : ''}}>{{ $voyage->voyage_id }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<!-- <div class="col-lg-3 col-md-3">
					<div class="form-group m-b-10">
						<label>Booking Number</label>
						<label id="remove_bk" class="pull-right lbl-remove" style="display: none">Remove</label>
						<select id="booking_number" class="form-control form-control-lg ddl" name="booking_number">
							<option selected value>-- select an option --</option>
							@foreach($booking AS $booking)
							<option value="{{ $booking->id }}">{{ $booking->booking_no }}</option>
							@endforeach
						</select>
					</div>
				</div> -->
			</div>
		</div>
	</div>
	<div class="panel">
		<div class="panel-body">
			<div class="row">

				<div class="col-lg-4 mb-2">
					<label class="no-wrap f-w-700 text-uppercase" style="margin-bottom: .3rem">Port of Loading</label>
					<div class="d-flex">
						<div class="form-group m-r-10 mb-0">
							<select style="max-width: 110px" id="pol_id" disabled="" class="form-control form-control-lg m-b-5" name="pol_id">
								<option disabled selected value>None</option>
							</select>
						</div>
						<div style="flex: 1" class="form-group mb-0">
							<input type="text" class="form-control form-control-lg" name="pol_name" data-id="pol_id" placeholder="Enter Location">
						</div>
					</div>
					<span><i>{{ (!empty($voyage_detail)) ? $voyage_detail->pol_name : '' }}</i></span>
				</div>

				<div class="col-lg-4 mb-2">
					<label class="no-wrap f-w-700 text-uppercase" style="margin-bottom: .3rem">Port of Discharge</label>
					<div class="d-flex">
						<div class="form-group m-r-10 mb-0">
							<select style="max-width: 110px" disabled="" id="pod_id" class="form-control form-control-lg m-b-5" name="pod_id">
								<option disabled selected value>None</option>
							</select>
						</div>
						<div style="flex: 1" class="form-group mb-0">
							<input type="text" class="form-control form-control-lg" name="pod_name" data-id="pod_id" placeholder="Enter Location">
						</div>
					</div>
					<span><i>{{ (!empty($voyage_detail)) ? $voyage_detail->pol_name : '' }}</i></span>
				</div>
				
				<div class="col-lg-4 mb-2">
					<label class="no-wrap f-w-700 text-uppercase" style="margin-bottom: .3rem">Final Port of Discharge</label>
					<div class="d-flex">
						<div class="form-group m-r-10 mb-0">
							<select style="max-width: 110px" disabled="" id="fpd_id" class="form-control form-control-lg m-b-5" name="fpd_id">
								<option disabled selected value>None</option>
							</select>
						</div>
						<div style="flex: 1" class="form-group mb-0">
							<input type="text" class="form-control form-control-lg" name="fpd_name" data-id="fpd_id" placeholder="Enter Location">
						</div>
					</div>
					<span><i>{{ (!empty($voyage_detail)) ? $voyage_detail->fpd_name : '' }}</i></span>
				</div>

			</div>
			<hr>
			<div class="row">
				<div class="col-lg-4 col-md-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Shipper Code</label>
								<select id="shipper_code" class="ddl form-control form-control-lg m-b-5" name="shipper_id" required>
									<option disabled selected value>-- select an option --</option>
									@foreach($shippers AS $shipper)
									<option value="{{ $shipper->company_id }}" data-name="{{ $shipper->name }}" {{ (!empty($lading)) ? (($lading->shipper_id == $shipper->company_id) ? 'selected' : '') : ''}} >{{ $shipper->code . ' - ' . $shipper->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-10">
								<label>Shipper Name</label>
								<input type="text" class="form-control form-control-lg" name="shipper_name" {{ (empty($lading)) ? 'disabled' : '' }} value="{{ (!empty($lading)) ? $lading->shipper_name : '' }}" required />
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Shipper Address</label>
								<select class="form-control form-control-lg bl_address m-b-5" name="shipper_add_id" {{ (empty($lading)) ? 'disabled' : '' }} >
									@if(!empty($lading))
									@foreach($shippers AS $shipper)
									@if($shipper->company_id == $lading->shipper_id)
									@foreach($shipper->getAddresses AS $add)
									@if($loop->iteration == 0)
									<option value="{{ $add->id }}" {{ ($lading->shipper_add_id == $add->id) ? 'selected' : '' }} >DEFAULT ADDRESS</option>
									@else
									<option value="{{ $add->id }}" {{ ($lading->shipper_add_id == $add->id) ? 'selected' : '' }} >{{ str_replace('<br>', ',', $add->address) }}</option>
									@endif
									@endforeach
									@endif
									@endforeach
									@endif
								</select>
								<textarea name="shipper_add" class="form-control form-control-lg" rows="4" {{ (empty($lading)) ? 'disabled' : '' }} >{{ (!empty($lading)) ? $lading->shipper_add : '' }}</textarea>
							</div>
						</div>
					</div>
					<div class="loader">
						<div class="loader-item text-center">
							<i class="fas fa-3x fa-spinner fa-spin"></i>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Consignee Code</label>
								<select id="consignee_code" class="ddl form-control form-control-lg m-b-5" name="consignee_id" required>
									<option disabled selected value>-- select an option --</option>
									@foreach($consignees AS $consignee)
									<option value="{{ $consignee->company_id }}" data-name="{{ $consignee->name }}" {{ (!empty($lading)) ? (($lading->consignee_id == $consignee->company_id) ? 'selected' : '') : ''}} >{{ $consignee->code . ' - ' . $consignee->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-10">
								<label>Consignee Name</label>
								<input type="text" class="form-control form-control-lg" name="consignee_name" {{ (empty($lading)) ? 'disabled' : '' }} value="{{ (!empty($lading)) ? $lading->consignee_name : '' }}" required/>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Consignee Address</label>
								<select class="form-control form-control-lg bl_address m-b-5" name="consignee_add_id" {{ (empty($lading)) ? 'disabled' : '' }} >
									@if(!empty($lading))
									@foreach($consignees AS $consignee)
									@if($consignee->company_id == $lading->consignee_id)
									@foreach($consignee->getAddresses AS $add)
									<option value="{{ $add->id }}" {{ ($lading->consignee_add_id == $add->id) ? 'selected' : '' }} >{{ str_replace('<br>', ',', $add->address) }}</option>
									@endforeach
									@endif
									@endforeach
									@endif
								</select>
								<textarea name="consignee_add" class="form-control form-control-lg" rows="4" {{ (empty($lading)) ? 'disabled' : '' }} >{{ (!empty($lading)) ? $lading->consignee_add : '' }}</textarea>
							</div>
						</div>
					</div>
					<div class="loader">
						<div class="loader-item text-center">
							<i class="fas fa-3x fa-spinner fa-spin"></i>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Notify Party Code</label>
								<select id="notify_code" class="ddl form-control form-control-lg m-b-5" name="notify_id" required>
									<option disabled selected value>-- select an option --</option>
									@foreach($notify_parties AS $np)
									<option value="{{ $np->comp_id }}" data-name="{{ $np->name }}" {{(!empty($lading)) ? (($lading->notify_id == $np->comp_id) ? 'selected' : '') : ''}} >{{ $np->code . ' - ' . $np->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-10">
								<label>Notify Party Name</label>
								<input type="text" class="form-control form-control-lg" name="notify_name" {{ (empty($lading)) ? 'disabled' : '' }} value="{{ (!empty($lading)) ? $lading->notify_name : '' }}" required/>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Notify Party Address</label>
								<select class="form-control form-control-lg bl_address m-b-5" name="notify_add_id" {{ (empty($lading)) ? 'disabled' : '' }}>
									@if(!empty($lading))
									@foreach($companies AS $company)
									@if($company->id == $lading->notify_id)
									@foreach($company->getAddresses AS $add)
									<option value="{{ $add->id }}" {{ ($lading->notify_add_id == $add->id) ? 'selected' : '' }} >{{ str_replace('<br>', ',', $add->address) }}</option>
									@endforeach
									@endif
									@endforeach
									@endif
								</select>
								<textarea name="notify_add" class="form-control form-control-lg" rows="4" {{ (empty($lading)) ? 'disabled' : '' }} >{{ (!empty($lading)) ? $lading->notify_add : '' }}</textarea>
							</div>
						</div>
					</div>
					<div class="loader">
						<div class="loader-item text-center">
							<i class="fas fa-3x fa-spinner fa-spin"></i>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>BL Date</label>
						<input id="bl_date" type="text" class="form-control form-control-lg datetimepicker" name="bl_date" placeholder="Enter BL Date" />
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Freight Term</label>
						<select class="form-control form-control-lg" name="freight_term">
							<option value="0">Prepaid</option>
							<option value="1">Collect</option>
							<option value="2">Payable</option>
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-5">
						<label>Container Ownership</label>
						<select class="form-control form-control-lg m-b-5" name="cont_owner" {{ (!empty($vessel_detail)) ? (($vessel_detail->semicontainer_service != 1) ? 'disabled' : '') : ''}}>
							<option value="1">Carrier-Owned Container (COC)</option>
							<option value="0">Shipper-Owned Container (SOC)</option>
						</select>
						<span><i>(only for FCL shipment)</i></span>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Telex Release</label>
						<select id="telex_option" class="form-control form-control-lg" name="telex_release">
							<option value="0">No</option>
							<option value="1">Yes</option>
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Telex Release Attachment</label>
						<div class="input-group input-group-lg">
							<input id="telex_attachment" type="file" accept="application/pdf" class="form-control" name="telex_attachment" disabled />
							<div class="input-group-append">
								<span id="attachment_preview" class="input-group-text full-height" data-toggle="tooltip" title="Click to view preview of attachment"><i class="fas fa-file-image"></i></span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>No. of BL</label>
						<input id="no_of_bl" type="number" class="form-control form-control-lg" name="no_of_bl" value="3" min="1" />
					</div>
				</div>
			</div>
		</div>
	</div>

	<input type="hidden" name="booking_number" value="{{ (!empty($bk)) ? $bk->id : ''}}">
	<input type="hidden" name="bl_no" value="{{ (!empty($lading)) ? $lading->bl_no : ''}}" />
	<button type="button" id="btn_cancel" class="btn btn-danger btn-action m-r-10">Cancel</button>
	<button type="button" class="btn btn-primary btn-action btn-save">Save</button>
	<button type="button" class="btn btn-success btn-action btn-save pull-right" data-step="cargo">Proceed to Cargo</button>
</form>

<div class="modal fade" id="modal_preview">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Telex Attachment Preview</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<object id="embed_pdf" style="height: 700px; width: 100%" type='application/pdf'></object>
				<!-- <img src="" width="100%" /> -->
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-success" data-dismiss="modal">Close</a>
			</div>
		</div>
	</div>
</div>
@stop

@section('page_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />

<script src="/js/bill_lading.js?v=1"></script>

<script>
	$(document).ready(function() {

		var url = "/bl/create";
		var query_booking = false;
		var bk_voyage_id = 0;

		var prev_voyage = "";
		var prev_vessel = "";

		$("input").attr("autocomplete", "off");

		var set_date = "{{ !empty($voyage_detail) ? $voyage_detail->pol_etd : '' }}";

		if(set_date == ""){
			set_date = new Date();
		}

		$(".datetimepicker").datetimepicker({
			format: "DD/MM/YYYY",
			date: set_date,
			minDate: new Date()
		});

		$(".ddl").select2();



		$("#vessel_type").change(function(){
			if($(this).val() == "LCL"){
				$("select[name='cont_owner']").attr("disabled", "");
			} else {
				$("select[name='cont_owner']").removeAttr("disabled");
			}
		});

		$('#shipper_code, #consignee_code, #notify_code').change(function() {
			var id = $(this).find('option:selected').val();
			var name = $(this).find('option:selected').attr('data-name');

			var elem = $(this);

			elem.closest('.col-lg-4').find('.loader').css('border-radius', '10px');
			elem.closest('.col-lg-4').find('.loader').fadeIn(200);

			$.ajax({
				type: 'POST',
				url: '/bl/create/company/address',
				data: {'id' : id},
				success: function(data) {
					if(data.success) {
						var html = '';

						for(var i = 0; i < data.result.length; i++) {
							var adrs_desc = "";

							if(data.result[i].description != null){
								html += '<option value="' + data.result[i].id + '">' + data.result[i].description + " - " + data.result[i].address.replace(/<br>/g, ', ') + '</option>';
							} else {
								html += '<option value="' + data.result[i].id + '">DEFAULT</option>';
							}
						}
						// if(i == 0) {
						// 	html += '<option value="' + data.result[i].id + '">' + adrs_desc + '</option>';
						// } else {
							
						// }

						elem.closest('.col-lg-12').next().find('input').prop('disabled', false);
						elem.closest('.col-lg-12').next().find('input').val(name);
						elem.closest('.col-lg-12').next().next().find('.bl_address').prop('disabled', false);
						elem.closest('.col-lg-12').next().next().find('textarea').prop('disabled', false);

						elem.closest('.col-lg-12').next().next().find('.bl_address').html(html);
						elem.closest('.col-lg-12').next().next().find('textarea').html(data.result[0].address.replace(/<br>/g, '&#13;&#10;'));
					} else {
						swal({
							title: "Error",
							text: "Please try again.",
							type: "error",
							confirmButtonText: "OK"
						});
					}

					$('.loader').fadeOut(200);
				},
				error: function(data) {
					console.log("Not Successful. " + data);
					
					swal({
						title: "Error",
						html: "Something went wrong!<br>Please try again.",
						type: "error",
						confirmButtonText: "OK"
					});

					$('.loader').fadeOut(200);
				}
			});
		});

		var old_value = $("#booking_number").val();

		$("#booking_number").change(function(){
			var elem = $(this);
			if(elem.val() != old_value && elem.val() != ""){

				$.get('/get-booking/' + elem.val(), function(data){
					if(data.success){
						console.log(data);
						var booking = data.booking;

						if($("#pol_id option[value='" + booking.pol_id + "']").length == 0){
							swal("Unable to select this booking number", "The following port is not in the port rotation of selected vessel/voyage : <br>" + booking.pol, "warning");
							return false;
						}

						if($("#pol_id option[value='" + booking.fpd_id + "']").length == 0){
							swal("Unable to select this booking number", "The following port is not in the port rotation of selected vessel/voyage : <br>" + booking.fpd, "warning");
							return false;
						}

						if(booking.voyage_id != $("#voyage_select").val() || booking.get_voyage.vessel_id != $("#vessel_select").val()){
							swal({
								title: 'Oops!',
								html: 'Booking number entered does not match with selected vessel/voyage. <br><br>Click <strong>Proceed</strong> to update vessel/voyage info for the selected booking when BL is saved',
								type: 'warning',
								showCancelButton: true,
								confirmButtonText: 'Proceed',
								reverseButtons: true
							}).then((result) => {
								if(result.value) {
									old_value = elem.val();

									//Update port and shipper according to selected booking
									$("#pol_id").val(booking.pol_id).trigger("change");
									$("#pod_id").val(booking.fpd_id).trigger("change");
									$("#fpd_id").val(booking.fpd_id).trigger("change");
									$("#shipper_code").val(booking.shipper_id).trigger("change");
								} else {
									elem.val(old_value).trigger("change");
								}
							});
						} else {
							$("#pol_id").val(booking.pol_id).trigger("change");
							$("#pod_id").val(booking.fpd_id).trigger("change");
							$("#fpd_id").val(booking.fpd_id).trigger("change");
							$("#shipper_code").val(booking.shipper_id).trigger("change");
							$("#remove_bk").show();
						}
					}
				});
			} else if (elem.val() == "") {

			}
		});

		$("#remove_bk").click(function(){
			$("#booking_number").val("").trigger("change");
			$(this).hide();
		});

		@if(!empty($lading))
		query_booking = true;
		$("#vessel_select").val("{{ $lading->getVessel->id }}").trigger("change").select2();
		setTimeout(function(){
			// $("#voyage_select").val("{{ $lading->voyage_id }}").trigger("change").select2();
		}, 1000);
		@endif

		$('#voyage_select').on('select2:open', function() {
			prev_voyage = $(this).val();
		}).change(function() {

			var voyage_id = $(this).val();
			retrieveDestinations(voyage_id);

			// var stateObj = { foo: "bar" };
			// history.pushState(null, "", "/bl/create?voyage=2");

			// var elem = this;
			// 
			// var vessel_id = $('#vessel_select').val();

			// if(voyage_id != prev_voyage){
			// 	if(voyage_id == ""){
			// 		$(elem).val(prev_voyage).trigger("change");
			// 	} else {
			// 		swal({
			// 			title: 'Are your sure?',
			// 			text: 'Switching voyages will refresh the page. Continue?',
			// 			type: 'warning',
			// 			showCancelButton: true,
			// 			confirmButtonText: 'Proceed',
			// 			reverseButtons: true
			// 		}).then((result) => {
			// 			if(result.value) {
			// 				// @if(!empty($vessel_detail) && !empty($lading))
			// 				// window.location.href = '{{ action('BillLadingController@create') }}?bl={{ $lading->bl_no }}&voyage=' + voyage_id + '&vessel=' + vessel_id;
			// 				// @elseif(empty($vessel_detail) && !empty($lading))
			// 				// window.location.href = '{{ action('BillLadingController@create') }}?bl={{ $lading->bl_no }}&voyage=' + voyage_id;
			// 				// @elseif(!empty($vessel_detail) && empty($lading))
			// 				// window.location.href = '{{ action('BillLadingController@create') }}?vessel=' + vessel_id + '&voyage=' + voyage_id;
			// 				// @else
			// 				// window.location.href = '{{ action('BillLadingController@create') }}?voyage=' + voyage_id;
			// 				// @endif
			// 			} else {
			// 				$(elem).val(prev_voyage).trigger("change");
			// 			}
			// 		});
			// 	}
			// }
		});



		$('#vessel_select').on('select2:open', function() {
			prev_vessel = $(this).val();
		}).change(function() {
			var elem = this;
			var voyage_id = $('#voyage_select').val();
			var vessel_id = $('#vessel_select').val();

			$.get("/get-vessel-shipment/null/" + vessel_id, function(data){
				if(data[0] == "FCL"){
					$("#vessel_type").find("option[value='LCL']").attr("disabled", "");
					$("#vessel_type").find("option[value='FCL']").prop("disabled", false).trigger("change");
					// $("#vessel_type").find("option:enabled").prop('selected', true).removeAttr("disabled").trigger("change");
				} else if (data[0] == "LCL"){
					$("#vessel_type").find("option[value='FCL']").attr("disabled", "");
					$("#vessel_type").find("option:enabled").prop('selected', true).trigger("change");
				} else {
					$("#vessel_type").find("option").removeAttr("disabled");
					$("#vessel_type").css("pointer-events", "auto");
					if(bk_default_shipment != ""){
						$("#vessel_type option[value='" + bk_default_shipment + "']").prop("selected", true).trigger("change");
					} else {
						$("#vessel_type").find("option[value='" + data[1] + "']").prop('selected', true).trigger('change');
					}
				}

				console.log(bk_vessel, vessel_id);

				if(bk_vessel == vessel_id){
					$("#vessel_type").css("pointer-events", "none");
				}
			});

			if(vessel_id != prev_vessel){
				if(vessel_id == ""){
					$(elem).val(prev_voyage).trigger("change");
				} else {
					//If voyage ID is not selected or does not have booking ID as query param
					if(voyage_id != "-" && voyage_id != null && query_booking === false){
						swal({
							title: 'Are you sure?',
							text: 'Switching vessels will remove the selected ports. Continue?',
							type: 'warning',
							showCancelButton: true,
							confirmButtonText: 'Proceed',
							reverseButtons: true
						}).then((result) => {
							if(result.value) {
								retrieveVoyages(vessel_id);
							} else {
								//Set value back to old option
								$(elem).val(prev_vessel).trigger("change");
							}
						});
					} else {
						retrieveVoyages(vessel_id);
					}

					// if(voyage_id != ""){
					// 	$.ajax({
					// 		type: "POST",
					// 		data: {id: $(this).val()},
					// 		url: '/bl/voyages/list',
					// 		success: function(data){
					// 			console.log(data);
					// 			if(data.success == true) {
					// 				var html = '<option>-- select an option --</option>';

					// 				for(var i = 0; i < data.result.length; i++) {
					// 					html += '<option value="' + data.result[i].id + '" data-scn="' + data.result[i].scn + '">' + data.result[i].voyage_id + '</option>';
					// 				}

					// 				$('#voyage_select').html(html);
					// 			} else {
					// 				alert("No voyage found for this vessel.");
					// 			}
					// 		}
					// 	});

						// swal({
						// 	title: 'Are you sure?',
						// 	text: 'Switching vessels will refresh the page. Continue?',
						// 	type: 'warning',
						// 	showCancelButton: true,
						// 	confirmButtonText: 'Proceed',
						// 	reverseButtons: true
						// }).then((result) => {
						// 	if(result.value) {
						// 		// window.location.href = '{{ action('BillLadingController@create') }}?vessel=' + vessel_id;

						// 		// @if(!empty($voyage_detail) && !empty($lading))
						// 		// alert("here");
						// 		// window.location.href = '{{ action('BillLadingController@create') }}?bl={{ $lading->bl_no }}&voyage=' + voyage_id + '&vessel=' + vessel_id;
						// 		// @elseif(empty($voyage_detail) && !empty($lading))
						// 		// window.location.href = '{{ action('BillLadingController@create') }}?bl={{ $lading->bl_no }}&vessel=' + vessel_id;
						// 		// @elseif(!empty($voyage_detail) && empty($lading))
						// 		// alert("here1");
						// 		// window.location.href = '{{ action('BillLadingController@create') }}?vessel=' + vessel_id;
						// 		// @else
						// 		// window.location.href = '{{ action('BillLadingController@create') }}?vessel=' + vessel_id;
						// 		// @endif
						// 	} else {
						// 		$(elem).val(prev_vessel).trigger("change");
						// 	}
						// });
					// } else {

					// }
				}
			}
		});

		var date_now = moment();

		//Retrieve list of voyage by vessel ID
		function retrieveVoyages(vessel_id){
			$.ajax({
				type: "POST",
				data: {id: vessel_id},
				url: '/bl/voyages/list',
				success: function(data){
					console.log(data);
					if(data.success == true) {
						var html = '<option value="-" disabled="" selected="">-- select an option --</option>';
						var added = 0;

						for(var i = 0; i < data.result.length; i++) {
							var date_etd = moment(data.result[i].etd_pol).add(7, "days");

							if((data.result[i].booking_status == 0 && date_etd.isAfter(date_now)) || bk_voyage_id == data.result[i].id){
								added++;
								html += '<option value="' + data.result[i].id + '">' + data.result[i].voyage_id + '</option>';
							}
						}

						if(added > 0) {
							$('#voyage_select').html(html).select2().removeAttr("disabled");
							errMsg(false);
						} else {
							$('#voyage_select').html('<option value="-">None</option>').select2().attr("disabled", "");
							errMsg(true, "No voyages found for the selected vessel.");
						}
					} else {
						errMsg(true, "No voyages found for the selected vessel.");
						$('#voyage_select').html('<option value="-">None</option>').select2().attr("disabled", "");
					}

					$("#pol_id, #pod_id, #fpd_id").html('<option>None</option>').attr("disabled", "").trigger("change");
				}
			});
			// query_booking = false;
		}

		//Retrieve list of destinations by voyage ID
		function retrieveDestinations(voyage_id){
			$.get("/get-destinations/" + voyage_id, function(data){
				if(data.success){
					var destinations = data.dest;

					var option_html = "<option disabled selected>None</option>";

					var bl_date = moment(destinations[0].etd).format("DD/MM/YYYY");

					destinations.forEach(function(value, index){
						option_html += "<option value='" + value.id + "' data-name='" + value.name + "' data-location='" + value.location.toUpperCase() + "'>" + value.code + "</option>";
					});

					$("#bl_date").val(bl_date);
					$("#pol_id, #pod_id, #fpd_id").html(option_html).removeAttr("disabled");
					$("#fpd_id").find("option:last").attr("selected", "selected").trigger("change");
					$("#pol_id").find("option:nth-child(2)").attr("selected", "selected").trigger("change");
					$("#pod_id").trigger("change");
				}
			});
		}

		//Set error message
		function errMsg(bool, msg){
			if(bool){
				$("#errMsg").text(msg).show();
			} else {
				$("#errMsg").hide();
			}
		}



		$(document).on('change', '.bl_address', function() {
			var address_id = $(this).val();
			var $this = $(this);
			$.get('/get-address/' + address_id, function(data){
				if(data.success){
					var address = data.address.address;
					address = address.replace(/<br[^>]*>/gi, "\n");
					// .replace(/, /g, '&#13;&#10;')
					$this.next().html(address);
				} else {
					swal("Oops!", "Address not found!", "error");
				}
			});
		});

		function readURL(input) {
			if(input.files[0].type != "application/pdf"){
				swal("Oops!", "Uploaded file is not in pdf format!", "warning");
				$("#telex_attachment").val("");
				return;
			}

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('#modal_preview').find('.modal-body object').attr('data', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}

		// Telex release functions and validation
		$("#telex_attachment").change(function() {
			readURL(this);
		});

		$('#telex_option').change(function() {
			if($(this).val() === "1") {
				$('#telex_attachment').prop('disabled', false);
			} else {
				$('#telex_attachment').prop('disabled', true);
			}
		});

		$('#attachment_preview').click(function() {
			if($('#telex_attachment').get(0).files.length === 1 && $('#telex_option').val() === "1") {
				$('#modal_preview').modal();
			} else if($('#telex_attachment').get(0).files.length === 0 && $('#telex_option').val() === "1") {
				swal({
					title: 'No attachment to preview!',
					text: 'Have you uploaded one yet?',
					type: 'info',
					confirmButtonText: 'OK'
				});
			}else {
				swal({
					title: 'Telex release not available!',
					text: 'Have you already made a telex release for this BL?',
					type: 'info',
					confirmButtonText: 'OK'
				});
			}
		});

		$('#no_of_bl').change(function() {
			if($(this).val() < 1) {
				$(this).val(1);
			}
		});

		$('#btn_cancel').click(function() {
			swal({
				title: 'Are your sure?',
				text: 'All your current progress will be lost.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Proceed',
				reverseButtons: true
			}).then((result) => {
				if(result.value) {
					window.location.href = '{{ action('BillLadingController@index') }}';
				}
			});
		});

		$(document).on('click', '.btn-save', function(e) {

			$(this).prop("disabled", true);

			e.preventDefault();

			var nextStep = $(this).attr('data-step');
			console.log(nextStep);

			var error = true;
			var error_msg = '';

			if($('#vessel_select').val() == null) {
				error_msg = 'A <b>vessel</b> needs to be selected.';
			} else if($('#voyage_select').val() == null || $("#voyage_select").val() == "-") {
				error_msg = 'A <b>voyage</b> needs to be selected.';
			} else if($('select[name=pod_id]').val() == null) {
				error_msg = 'A <b>POD</b> needs to be selected.';
			} else if($('#shipper_code').val() == null) {
				error_msg = 'A <b>shipper</b> needs to be selected.';
			} else if($('#consignee_code').val() == null) {
				error_msg = 'A <b>consignee</b> needs to be selected.';
			} else if($('#notify_code').val() == null) {
				error_msg = 'A <b>notify party</b> needs to be selected.';
			} else if($('#shipper_name').val() == '' || $('#shipper_name').val() == ' ') {
				error_msg = 'Shipper can\'t be empty.';
			} else if($('#consignee_name').val() == '' || $('#consignee_name').val() == ' ') {
				error_msg = 'Consignee can\'t be empty.';
			} else if($('#notify_name').val() == '' || $('#notify_name').val() == ' ') {
				error_msg = 'Notify party can\'t be empty.';
			} else if($('#bl_date').val() == '') {
				error_msg = 'BL date can\'t be empty.';
			} else if($("#pol_id").val() === $("#fpd_id").val()){
				error_msg = 'FPD cannot be the same as POL.';
			} else if($("input[name='pol_name']").val() == '' || $("input[name='pod_name']").val() == '' || $("input[name='fpd_name']").val() == ''){
				error_msg = 'All port locations is required.';
			} else {
				error = false;
			}

			if(error) {
				swal({
					title: 'Error',
					html: error_msg,
					type: 'error',
					confirmButtonText: 'OK'
				});

				$(this).removeAttr("disabled");

			} else {
				if(nextStep) {
					$('#bl_form').attr('action', '{{ action('BillLadingController@store') }}?next=cargo');
					$('#bl_form').submit();
				} else {
					$('#bl_form').submit();
				}
			}
		});

		var bk_default_shipment = "";
		var bk_vessel = 0;

		@if(!empty($bk))
		bk_default_shipment = "{{ $bk->shipment_type }}";
		$("#vessel_type").css("pointer-events", "none");
		query_booking = true;
		bk_vessel = "{{ $bk->getVoyage->vessel->id }}";
		bk_voyage_id = "{{ $bk->voyage_id }}";
		$("#vessel_select").val("{{ $bk->getVoyage->vessel->id }}").trigger("change");
		setShipmentType("{{ $bk->shipment_type }}");

		setTimeout(function(){
			$("#voyage_select").val("{{ $bk->voyage_id }}").trigger("change").removeAttr("disabled");

			setTimeout(function(){
				$("#pol_id").val("{{ $bk->pol_id }}").trigger("change");
				$("#pod_id").val("{{ $bk->pod_id }}").trigger("change");
				$("#fpd_id").val("{{ $bk->fpd_id }}").trigger("change");
				$("#shipper_code").val("{{ $bk->shipper_id }}").trigger("change");
				$("#pol_id, #pod_id, #fpd_id").removeAttr("disabled");
			}, 500);
			
		}, 1000);
		@endif

		@if(!empty($lading))
		console.log("{{ $lading->getVessel->id }}");
		// $("#vessel_select").val("{{ $lading->getVessel->id }}").trigger("change").select2();
		$("#pol_id, #pod_id, #fpd_id").removeAttr("disabled");
		@if($lading->getVessel->type == "Container Vessel")
		$("#vessel_type option[value='LCL']").text("LCL (unavailable for CV)").prop("disabled", true);
		@else
		@if($lading->semicontainer_service == 0)
		$("#vessel_type").find("option[value='FCL']").text("FCL (unavailable for GCV w/o semi-container service)").prop("disabled", true);
		$("#vessel_type option[value='LCL']").prop("selected", true).trigger("change");
		@endif
		@endif
		@endif

		@if(!empty($vessel_detail) && empty($bk))
		@if($vessel_detail->type == 'General Cargo Vessel' && $vessel_detail->semicontainer_service != 1)
		$('#vessel_type option:first-child').prop('disabled', true);
		$('#vessel_type option:first-child').text('FCL (unavailable for GCV w/o semi-container service)');
		$('#vessel_type option:last-child').prop('selected', true).trigger("change");
		@elseif($vessel_detail->type == 'Container Vessel')
		$('#vessel_type option:last-child').prop('disabled', true);
		$('#vessel_type option:last-child').text('LCL (unavailable for CV)');
		$('#vessel_type option:first-child').prop('selected', true).trigger("change");
		$("select[name='cont_owner']").removeAttr("disabled");
		@else
		$('#vessel_type option:last-child').text('FCL');
		$('#vessel_type option:last-child').text('LCL');

		@if($vessel_detail->default_shipment == 'FCL')
		$('#vessel_type option:first-child').prop('selected', true);
		@else
		$('#vessel_type option:last-child').prop('selected', true).trigger("change");
		@endif

		@endif
		@endif

		$( window ).resize(function() {
			$(".ddl").select2();
		});

		function setShipmentType(type){
			if(type == "FCL"){
				$("#vessel_type").find("option[value='LCL']").attr("disabled", "");
				$("#vessel_type").find("option:enabled").prop('selected', true).trigger("change");
			} else if (type == "LCL"){
				$("#vessel_type").find("option[value='FCL']").attr("disabled", "");
				$("#vessel_type").find("option:enabled").prop('selected', true).trigger("change");
			} else {
				$("#vessel_type").find("option").removeAttr("disabled");
			}
		}

		@if(!empty($voyage_detail))
		$("#voyage_select").trigger("change");
		@endif
	});
</script>
@stop