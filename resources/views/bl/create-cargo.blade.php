@extends("layouts.nav")
@section('page_title', 'BL Cargo')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('BillLadingController@cargo', $hash) }}">BL Cargo</a></li>
	<li class="breadcrumb-item active">Add Cargo</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Add Cargo</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<form id="cargo_form" class="cargo_form" action="{{ action('BillLadingController@store_cargo', $hash) }}" method="POST" autocomplete="off">
	@csrf
	<div class="row">
		<div class="col-lg-12 col-xl-12">
			<div class="panel">
				<div class="panel-body p-20">
					<h4>{{ $bl->vessel_type }} Shipment</h4>
					<hr>
					@if($bl->vessel_type == "FCL")
					<table id="qty_table" class="table table-bordered text-center">
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th colspan=7 class="text-center">Container Type</th>
							</tr>
							<tr>
								<th></th>
								<th></th>
								<th>GP</th>
								<th>HC</th>
								<th>RF</th>
								<th>HRF</th>
								<th>FR</th>
								<th>TK</th>
								<th>OT</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th rowspan="3">Size</th>
								<th>20</th>
								<td><input id="20GP" data-type="20'GP" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="20HC" data-type="20'HC" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="20RF" data-type="20'RF" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="20HRF" data-type="20'HRF" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="20FR" data-type="20'FR" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="20TK" data-type="20'TK" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="20OT" data-type="20'OT" type="number" class="cargo-qty" value="0" /></td>
							</tr>
							<tr>
								<th>40</th>
								<td><input id="40GP" data-type="40'GP" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="40HC" data-type="40'HC" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="40RF" data-type="40'RF" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="40HRF" data-type="40'HRF" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="40FR" data-type="40'FR" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="40TK" data-type="40'TK" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="40OT" data-type="40'OT" type="number" class="cargo-qty" value="0" /></td>
                            </tr>
                            <tr>
								<th>45</th>
								<td><input id="45GP" data-type="45'GP" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="45HC" data-type="45'HC" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="45RF" data-type="45'RF" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="45HRF" data-type="45'HRF" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="45FR" data-type="45'FR" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="45TK" data-type="45'TK" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="45OT" data-type="45'OT" type="number" class="cargo-qty" value="0" /></td>
							</tr>
						</tbody>
					</table>
					<button id="btn_reset" type="button" class="btn btn-success">Reset Quantity</button>
					@else
					<div class="row">
						<div class="col-lg-2">
							<div class="form-group m-b-20">
								<label>Quantity</label>
								<input id="cargo_qty" name="cargo_qty" type="number" min="1" data-title="Quantity" class="required form-control form-control-lg" placeholder="Quantity" />
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group m-b-20">
								<label>Packing</label>
								<input id="cargo_packing" name="cargo_packing" type="text" data-title="Packing" class="required form-control form-control-lg" placeholder="Packing" required />
							</div>
						</div>
					</div>
					@endif
					<!-- <hr>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group m-b-20">
								@if($bl->vessel_type == "FCL")
								<label>Qty / Size & Type</label>
								@else
								<label>Qty / Packing</label>
								@endif
								<input id="cargo_name" type="text" name="cargo_name" class="form-control form-control-lg" readonly />
							</div>
						</div>
					</div> -->
					<hr>
					<div class="cargo-container">
						<!-- added dynamically -->
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12 col-xl-12">
			<div class="panel">
				<div class="panel-body p-20">
					<h4>Cargo Details</h4>
					<hr>

					<div class="row">
						<div class="col-lg-4">
							<div class="form-group m-b-20">
								<label>Markings</label>
								<div id="div_markings" contenteditable="true" data-name="markings" class="create editablecontent"></div>
								<!-- <textarea maxrow="5" name="markings" class="form-control form-control-lg" rows="5" placeholder="Enter Markings"></textarea> -->
							</div>
							<div class="form-group m-b-20">
								<label>UN Code / Class</label>
								<div id="div_uncode" contenteditable="true" data-name="uncode" class="create editablecontent"></div>

								<!-- <textarea maxrow="5" name="uncode" class="form-control form-control-lg" rows="5" placeholder="Enter UN Code / Class"></textarea> -->
							</div>
							@if($bl->vessel_type == "FCL")
							<div class="form-group m-b-20">
								<label>Temperature</label>
								<input name="temperature" type="number" class="form-control form-control-lg" placeholder="Temperature" />
							</div>
							@endif
						</div>
						<div class="col-lg-8">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group m-b-20">
										<label>Cargo Description</label>
										<input maxlength="50" name="cargo_desc" type="text" data-title="Cargo Description" class="required form-control form-control-lg" placeholder="Enter Cargo Description" />
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group m-b-20">
										<label >Cargo Type</label>
										<select name="cargo_type" class="form-control form-control-lg m-b-5" >
											<option value="General">General</option>
											<option value="Container">Container</option>
											<option value="Timber">Timber</option>
											<option value="Veneer">Veneer</option>
											<option value="Plywood">Plywood</option>
											<option value="Bar">Bar</option>
											<option value="Wire Rod">Wire Rod</option>
											<option value="Coil">Coil</option>
											<option value="Plate">Plate</option>
											<option value="Jumbo Bags">Jumbo Bags</option>
										</select>
									</div>
								</div>
								<div class="col-lg-3">
									<div class="form-group m-b-20">
										<label>Weight</label>
										<input type="number" data-type="number" name="weight" class="form-control form-control-lg" min="0" value="0.000" step=".001">
									</div>
								</div>
								<div class="col-lg-3">
									<div class="form-group m-b-20">
										<label>MT/KG</label>
										@if(env("SITE") == "SHS")
										<select name="weight_unit" class="form-control form-control-lg m-b-5">
											<option value="MT">MT</option>
											<option value="KG">KG</option>
										</select>
										@else
										<select name="weight_unit" class="form-control form-control-lg m-b-5">
											<option value="KG">KG</option>
											<option value="MT">MT</option>
										</select>
										@endif
									</div>
								</div>
								<div class="col-lg-3">
									<div class="form-group m-b-20">
										<label>Volume M3</label>
										<input type="number" data-type="number" name="volume" class="form-control form-control-lg" min="0" value="0.000" step=".001">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-12">
									<div class="form-group m-b-20" style="overflow-x: auto">
										<label>Detailed Description</label>
										<div id="div_detaileddesc" placeholder="Enter Detailed Description" contenteditable="true" data-name="detailed_desc" class="create editablecontent detaileddesc"></div>
										<!-- <textarea style="width: 700px" maxrow="15" name="detailed_desc" placeholder="Enter Detailed Description" class="form-control form-control-lg" rows="15" ></textarea> -->
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<input id="detailed_desc" name="detailed_desc" hidden="">
	<input id="markings" name="markings" hidden="">
	<input id="uncode" name="uncode" hidden="">

	<input id="cargo_name" type="text" name="cargo_name" hidden="" readonly />
	<input name="bl_id" type="hidden" value="{{ $bl->id }}" />
	<input name="bl_no" type="hidden" value="{{ $bl->bl_no }}" />
	<input id="raw_qty" name="raw_qty" type="hidden" />
	<button type="button" id="btn_cancel" class="btn btn-danger btn-action m-r-10">Back to Cargo Details</button>
	<button type="button" class="btn btn-primary btn-action btn-save">Save</button>
</form>
@stop

@section('page_script')
<script type="text/javascript" src="/js/shortcut.js"></script>
<script type="text/javascript" src="/js/bl_cargo.js?v=1"></script>

<script>
	$(function(){
		return_url = "{{ action('BillLadingController@cargo', $hash) }}";

		shipment_type = "{{ $bl->vessel_type }}";

		is_semicontainer = "{{ $bl->getVessel->semicontainer_service }}";
	});
</script>
@stop
