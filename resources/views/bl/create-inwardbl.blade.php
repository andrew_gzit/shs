@extends("layouts.nav")
@section('page_title', 'Create Inward BL')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('BillLadingController@index') }}#inward">Bill of Lading</a></li>
</ol>
@stop

@section('content')
<h1 class="page-header">
	Create Inward Bill of Lading
</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<form id="form_inwardbl" autocomplete="off" method="POST" action="{{ action('InwardBLController@store') }}">
	@csrf
	<div class="panel">
		<div class="panel-body">
			<h4>Inward BL Details</h4>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Vessel</label>
						<select id="txt_vessel" data-title="Vessel" class="form-control form-control-lg ddl required" name="txt_vessel">
							<option disabled="" selected="" value>-- select an option --</option>
							@foreach($vessels AS $vessel)
							<option value="{{ $vessel->name }}">{{ $vessel->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Voyage</label>
						<select  id="txt_voyage" data-title="Voyage" disabled="" class="ddl form-control form-control-lg required" name="txt_voyage">
							<option disabled="" selected="" value="-">-- select an option --</option>
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>BL No.</label>
						<input id="txt_blno" type="text" data-title="BL No." class="form-control form-control-lg required" placeholder="Enter BL No." name="txt_blno">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-4 mb-2">
					<label class="no-wrap f-w-700 text-uppercase" style="margin-bottom: .3rem">Port of Loading</label>
					<div class="d-flex">
						<div class="form-group m-r-10 mb-0">
							<select style="max-width: 110px" id="pol_id" disabled="" data-title="POL" class="ddl-port form-control form-control-lg m-b-5" name="pol_id">
								<option disabled selected value>None</option>
							</select>
						</div>
						<div style="flex: 1" class="form-group mb-0">
							<input type="text" data-title="POL Location" class="form-control form-control-lg required" name="txt_polname" data-id="pol_id" placeholder="Enter Location">
						</div>
					</div>
					<span></span>
				</div>
				<div class="col-lg-4 mb-2">
					<label class="no-wrap f-w-700 text-uppercase" style="margin-bottom: .3rem">Port of Discharge</label>
					<div class="d-flex">
						<div class="form-group m-r-10 mb-0">
							<select style="max-width: 110px" disabled="" id="pod_id" data-title="POD" class="ddl-port form-control form-control-lg m-b-5" name="pod_id">
								<option disabled selected value>None</option>
							</select>
						</div>
						<div style="flex: 1" class="form-group mb-0">
							<input type="text" data-title="POD Location" class="form-control form-control-lg required" name="txt_podname" data-id="pod_id" placeholder="Enter Location">
						</div>
					</div>
					<span></span>
				</div>
				<div class="col-lg-4 mb-2">
					<label class="no-wrap f-w-700 text-uppercase" style="margin-bottom: .3rem">Final Port of Discharge</label>
					<div class="d-flex">
						<div class="form-group m-r-10 mb-0">
							<select style="max-width: 110px" disabled="" id="fpd_id" data-title="FPD" class="ddl-port form-control form-control-lg m-b-5" name="fpd_id">
								<option disabled selected value>None</option>
							</select>
						</div>
						<div style="flex: 1" class="form-group mb-0">
							<input type="text" data-title="FPD Location" class="form-control form-control-lg required" name="txt_fpdname" data-id="fpd_id" placeholder="Enter Location">
						</div>
					</div>
					<span></span>
				</div>
			</div>

			<hr>

			<div class="row">
				<div class="col-lg-4 col-md-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Shipper Code</label>
								<select id="shipper_code" class="ddl-companycode ddl form-control form-control-lg m-b-5" name="shipper_id">
									<option disabled selected value>-- select an option --</option>
									@foreach($shippers AS $comp)
									<option value="{{ $comp->company_id }}" data-name="{{ $comp->getName() }}">{{ $comp->code }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-10">
								<label>Shipper Name</label>
								<input id="shipper_name" data-title="Shipper Name" type="text" class="ddl-companyname form-control form-control-lg required" name="txt_shippername" placeholder="Enter Shipper Name" />
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Shipper Address</label>
								<select disabled="" class="form-control form-control-lg bl_address m-b-5" name="shipper_add_id">
									<option disabled selected value>-- select an option --</option>
								</select>
								<textarea disabled="" name="txt_shipperadd" data-title="Shipper Address" class="required form-control form-control-lg" rows="4" placeholder="Enter Address"></textarea>
							</div>
						</div>
					</div>
					<div class="loader">
						<div class="loader-item text-center">
							<i class="fas fa-3x fa-spinner fa-spin"></i>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Consignee Code</label>
								<select id="consignee_code" class="ddl-companycode ddl form-control form-control-lg m-b-5" name="consignee_id">
									<option disabled selected value>-- select an option --</option>
									@foreach($consignees AS $cons)
									<option value="{{ $cons->id }}" data-name="{{ $cons->name }}">{{ $cons->getCompanyOfType()->code }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-10">
								<label>Consignee Name</label>
								<input type="text" data-title="Consignee Name" class="ddl-companyname form-control form-control-lg required" name="txt_consigneename" placeholder="Enter Consignee Name"/>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Consignee Address</label>
								<select disabled="" class="form-control form-control-lg bl_address m-b-5" name="consignee_add_id">
									<option disabled selected value>-- select an option --</option>
								</select>
								<textarea disabled="" name="txt_consigneeadd" data-title="Consignee Address" class="form-control form-control-lg required" rows="4" placeholder="Enter Address"></textarea>
							</div>
						</div>
					</div>
					<div class="loader">
						<div class="loader-item text-center">
							<i class="fas fa-3x fa-spinner fa-spin"></i>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Notify Party Code</label>
								<select id="notify_code" class="ddl-companycode ddl form-control form-control-lg m-b-5" name="notify_id">
									<option disabled selected value>-- select an option --</option>
									@foreach($consignees AS $cons)
									<option value="{{ $cons->id }}" data-name="{{ $cons->name }}">{{ $cons->getCompanyOfType()->code }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-10">
								<label>Notify Party Name</label>
								<input type="text" data-title="Notify Party Name" class="ddl-companyname form-control form-control-lg required" name="txt_notifyname" placeholder="Enter Notify Party Name"/>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Notify Party Address</label>
								<select disabled="" class="form-control form-control-lg bl_address m-b-5" name="notify_add_id">
									<option disabled selected value>-- select an option --</option>
								</select>
								<textarea disabled="" name="txt_notifyadd" data-title="Notify Party Address" class="form-control form-control-lg required" rows="4" placeholder="Enter Address"></textarea>
							</div>
						</div>
					</div>
					<div class="loader">
						<div class="loader-item text-center">
							<i class="fas fa-3x fa-spinner fa-spin"></i>
						</div>
					</div>
				</div>
			</div>

			<hr>

			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>No. of BL</label>
						<input type="number" min="1" data-title="No. of BL" class="form-control form-control-lg required" placeholder="Enter No. of BL" id="txt_no_of_bl" name="txt_no_of_bl" value="3">
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="panel">
		<div class="panel-body">
			<h4>Cargo Details</h4>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>No. of Packages</label>
						<input type="text" name="txt_no_packages" data-title="No. of Packages" class="required form-control form-control-lg" placeholder="Enter No. of Packages">
					</div>
					<div class="form-group">
						<label>No. of Containers / Markings</label>
						<textarea data-title="No. of Containers / Markings" class="form-control form-control-lg required" rows="5" placeholder="Enter No. of Containers / Markings" name="txt_containers"></textarea>
					</div>
				</div>
				<div class="col-lg-4">
					<!-- <div class="form-group">
						<label>Cargo Description</label>
						<input data-title="Cargo Description" type="text" class="form-control form-control-lg required" placeholder="Enter Cargo Description" name="txt_cargodesc">
					</div> -->
					<div class="form-group">
						<label>Cargo Description</label>
						<textarea data-title="Detailed Description" class="form-control form-control-lg required" rows="5" placeholder="Enter Cargo Description" name="txt_detailedesc"></textarea>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-row">
						<div class="col">
							<div class="form-group">
								<label>Weight</label>
								<input data-title="Weight" type="text" class="form-control form-control-lg required" placeholder="Enter Weight" name="txt_weight">
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label>MT/KG</label>
								<select class="form-control form-control-lg" name="txt_weightunit">
									<option value="MT">MT</option>
									<option value="KG">KG</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Volume</label>
								<input data-title="Volume" type="text" class="form-control form-control-lg required" placeholder="Enter Volume" name="txt_volume">
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<button type="button" id="btn_cancel" class="btn btn-danger btn-action m-r-10">Cancel</button>
	<button type="button" id="btn_save" class="btn btn-primary btn-action">Save</button>
</form>
@stop

@section("page_script")
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script src="/js/inwardbl.js"></script>

<script type="text/javascript">
	$(function(){
		var empty_option = "<option disabled='' selected=''>-- select an option --</option>";

		$(".ddl").select2({
			tags: true
		});

		$(".ddl-port").select2();

		$("#txt_vessel").on('select2:select', function (e) {
			retrieveVoyages($(this).val());
		});

		$("#txt_voyage").on('select2:select', function (e) {
			var voy_id = $(this).find("option:selected").attr('data-voy-id');
			if(voy_id != null){
				retrieveDestinations(voy_id);
			} else {
				//Custom voyage, show all ports for user to select
				$.get("/get-ports", function(data){
					$(".ddl-port").html(data).select2().prop("disabled", false);
				});
			}
		});

		$(".ddl-companycode").change(function(){
			var row = $(this).closest(".row");

			var loader = row.siblings(".loader");

			loader.fadeIn(200);

			var comp_name = $(this).find("option:selected").attr('data-name');
			row.find(".ddl-companyname").val(comp_name).trigger("change").trigger("keyup");
			updateAddress(row.find(".bl_address"), $(this).val());

			loader.fadeOut(200);
		});

		$('#txt_no_of_bl').change(function() {
			if($(this).val() < 1) {
				$(this).val(1);
			}
		});

		$("#btn_save").click(function(){

			$(this).prop("disabled", true);

			var valid = true;

			$(".required").each(function(){
				if($(this).val() == "" || $(this).val() == null){
					valid = false;
					$(this).focus();
					swal("Oops!", $(this).attr("data-title") + " is required.", "warning");
					$("#btn_save").removeAttr("disabled");
					return false;
				}
			});

			if(valid){

				if($("#pol_id").val() == $("#fpd_id").val()){
					swal("Oops!", "FPD cannot be the same as POL", "warning");
					valid = false;
					$("#btn_save").removeAttr("disabled");
					return false;
				}

				$.get("/inward-bl-check/" + $("#txt_blno").val(), function(data){
					if(!data.success){
						$("#txt_blno").focus();
						swal("Oops!", "BL No has been used.", "warning");
						$(this).removeAttr("disabled");
					} else {
						if(valid){
							$("#form_inwardbl").submit();
						}
					}
				});
			}
		});

		function retrieveVoyages(vessel_name){
			$.ajax({
				type: "GET",
				url: '/voyage-list/' + vessel_name,
				success: function(data){
					if(data.success == true) {
						var html = empty_option;
						var added = 0;

						for(var i = 0; i < data.voyages.length; i++) {
							if(data.voyages[i].booking_status == 0){
								added++;
								html += '<option value="' + data.voyages[i].voyage_id + '" data-voy-id=' + data.voyages[i].id  +'>' + data.voyages[i].voyage_id + '</option>';
							}
						}

						if(added > 0) {
							$('#txt_voyage').html(html).removeAttr("disabled");
						} else {
							$('#txt_voyage').html(empty_option);
						}
					} else {
						$('#txt_voyage').html(empty_option);
					}
				}
			});
			$("#txt_voyage").removeAttr("disabled");
		}

		function retrieveDestinations(voyage_id){
			$.get("/get-destinations/" + voyage_id, function(data){
				if(data.success){
					var destinations = data.dest;

					var option_html = "<option disabled selected>None</option>";
					destinations.forEach(function(value, index){
						option_html += "<option value='" + value.id + "' data-name='" + value.name + "' data-location='" + value.location.toUpperCase() + "'>" + value.code + "</option>";
					});

					$("#pol_id, #pod_id, #fpd_id").html(option_html).removeAttr("disabled");
					$("#fpd_id").find("option:last").attr("selected", "selected").trigger("change");
					$("#pol_id").find("option:nth-child(2)").attr("selected", "selected").trigger("change");
					$("#pod_id").trigger("change");
				}
			});
		}

		$( window ).resize(function() {
			$(".ddl").select2();
		});

	});
</script>
@stop