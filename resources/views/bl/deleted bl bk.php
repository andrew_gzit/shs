@foreach($outward_deleted AS $deleted_ea)
						<tr>
							<td>{{ $deleted_ea->bl_no }}</td>
							<td>{{ optional($deleted_ea->getVessel)->name }}</td>
							<td>{{ optional($deleted_ea->getVoyage)->voyage_id }}</td>
							<td>{{ !empty($deleted_ea->pol) ? $deleted_ea->pol->code : '' }}</td>
							<td>{{ !empty($deleted_ea->pod) ? $deleted_ea->pod->code : '' }}</td>
							<td>{{ ($deleted_ea->cust_s_name) ? $deleted_ea->cust_s_name : optional($deleted_ea->shipper)->name }}</td>
							<td class="no-wrap">{{ $deleted_ea->deleted_at->format('d/m/Y h:i A') }}</td>
							<td width="1%">
								<form style="display: inline" method="POST" action="{{ action('BillLadingController@recover_bl', Hashids::encode($deleted_ea->id)) }}">
									@csrf
									<button type="button" data-bl-no="{{ $deleted_ea->bl_no }}" class="btn btn-success btn-recover">
										<i class="fas fa-undo"></i>
									</button>
								</form>
							</td>
						</tr>
						@endforeach