@extends("layouts.nav")
@section('page_title', 'Edit BL')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('BillLadingController@index') }}">Bill of Lading</a></li>
	<li class="breadcrumb-item active">Edit Bill of Lading</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Edit Bill of Lading 
	<span class="f-s-18">
		{{ $bl->bl_no }}
	</span>
</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<form id="bl_form" action="{{ action('BillLadingController@update', $bl->id) }}" enctype="multipart/form-data" method="POST">
	@method('PUT')
	@csrf
	<div class="panel">
		<div class="panel-body">
			<h4>BL Details</h4>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Vessel</label>
						<a id="btn_changevessel" class="pull-right" href="javascript:;">Change Vessel</a>
						<input type="hidden" name="vessel_id" value="{{ $bl->vessel_id }}">
						<select disabled="" id="vessel_select" class="form-control form-control-lg ddl">
							<option disabled selected value>-- select an option --</option>
							@foreach($vessels AS $vessel)
							<option value="{{ $vessel->id }}" {{ ($bl->vessel_id == $vessel->id) ? 'selected' : '' }}>{{ $vessel->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Shipment Type</label>
						<select id="vessel_type" style="pointer-events: none" class="form-control form-control-lg" name="vessel_type">
							<option value="FCL">FCL</option>
							<option value="LCL" {{ ($bl->vessel_type == "LCL") ? 'selected' : '' }}>LCL</option>
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Voyage</label>
						<input type="hidden" name="voyage_id" value="{{ $bl->voyage_id }}">
						<select disabled="" id="voyage_select" class="form-control form-control-lg ddl">
							<option disabled value>-- select an option --</option>
							@foreach($voyages AS $voy)
							<option value="{{ $voy->id }}" {{ ($bl->voyage_id == $voy->id) ? 'selected' : '' }}>{{ $voy->voyage_id }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel">
		<div class="panel-body">
			<div class="row">

				<div class="col-lg-4 mb-2">
					<label class="no-wrap f-w-700 text-uppercase" style="margin-bottom: .3rem">Port of Loading</label>
					<div class="d-flex">
						<div class="form-group m-r-10 mb-0">
							<select style="max-width: 110px" id="pol_id" class="form-control form-control-lg m-b-5" name="pol_id">
								<option disabled selected value>None</option>
								@foreach($voyage->destinations AS $dest)
								<option value="{{ $dest->port }}" data-name="{{ $dest->getPort->name }}" data-location="{{ strtoupper($dest->getPort->location) }}" {{ ($bl->pol_id == $dest->port) ? 'selected' : '' }}>{{ $dest->getPort->code }}</option>
								@endforeach
							</select>
						</div>
						<div style="flex: 1" class="form-group mb-0">
							<input type="text" class="form-control form-control-lg" name="pol_name" data-id="pol_id" placeholder="Enter Location" value="{{ $bl->pol_name }}">
						</div>
					</div>
					<span><i>{{ $bl->pol->name }}</i></span>
				</div>

				<div class="col-lg-4 mb-2">
					<label class="no-wrap f-w-700 text-uppercase" style="margin-bottom: .3rem">Port of Discharge</label>
					<div class="d-flex">
						<div class="form-group m-r-10 mb-0">
							<select style="max-width: 110px" id="pod_id" class="form-control form-control-lg m-b-5" name="pod_id">
								<option disabled selected value>None</option>
								@foreach($voyage->destinations AS $dest)
								<option value="{{ $dest->port }}" data-name="{{ $dest->getPort->name }}" data-location="{{ strtoupper($dest->getPort->location) }}" {{ ($bl->pod_id == $dest->port) ? 'selected' : '' }}>{{ $dest->getPort->code }}</option>
								@endforeach
							</select>
						</div>
						<div style="flex: 1" class="form-group mb-0">
							<input type="text" class="form-control form-control-lg" name="pod_name" data-id="pod_id" placeholder="Enter Location" value="{{ $bl->pod_name }}">
						</div>
					</div>
					<span><i>{{ $bl->pod->name }}</i></span>
				</div>


				<div class="col-lg-4 mb-2">
					<label class="no-wrap f-w-700 text-uppercase" style="margin-bottom: .3rem">Final Port of Discharge</label>
					<div class="d-flex">
						<div class="form-group m-r-10 mb-0">
							<select style="max-width: 110px" id="fpd_id" class="form-control form-control-lg m-b-5" name="fpd_id">
								<option disabled selected value>None</option>
								@foreach($voyage->destinations AS $dest)
								<option value="{{ $dest->port }}" data-name="{{ $dest->getPort->name }}" data-location="{{ strtoupper($dest->getPort->location) }}" {{ ($bl->fpd_id == $dest->port) ? 'selected' : '' }}>{{ $dest->getPort->code }}</option>
								@endforeach
							</select>
						</div>
						<div style="flex: 1" class="form-group mb-0">
							<input type="text" class="form-control form-control-lg" name="fpd_name" data-id="fpd_id" placeholder="Enter Location" value="{{ $bl->fpd_name }}">
						</div>
					</div>
					<span><i>{{ $bl->fpd->name }}</i></span>
				</div>

			</div>
			<hr>
			<div class="row">
				<div class="col-lg-4 col-md-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Shipper Code</label>
								<select id="shipper_code" class="ddl form-control form-control-lg m-b-5" name="shipper_id" required>
									<option disabled selected value>-- select an option --</option>
									@foreach($shippers AS $shipper)
									<option value="{{ $shipper->company_id }}" data-name="{{ $shipper->name }}" {{ ($bl->shipper_id == $shipper->company_id) ? 'selected' : '' }}>{{ $shipper->code . ' - ' . $shipper->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-10">
								<label>Shipper Name</label>
								<input type="text" class="form-control form-control-lg" name="shipper_name" value="{{ $bl->shipper_name }}" />
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Shipper Address</label>
								<select class="form-control form-control-lg bl_address m-b-5" name="shipper_add_id" >
									@foreach($bl->shipperAddresses AS $add)
									<option value="{{ $add->id }}">
										@if($add->description == null)
										DEFAULT
										@else
										{{ $add->description . ' - ' . str_replace('<br>', ', ', $add->address) }}
										@endif
									</option>
									@endforeach
								</select>
								<textarea name="shipper_add" class="form-control form-control-lg" rows="4" >{{ $bl->shipper_add }}</textarea>
							</div>
						</div>
					</div>
					<div class="loader">
						<div class="loader-item text-center">
							<i class="fas fa-3x fa-spinner fa-spin"></i>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Consignee Code</label>
								<select id="consignee_code" class="ddl form-control form-control-lg m-b-5" name="consignee_id" required>
									<option disabled selected value>-- select an option --</option>
									@foreach($consignees AS $consignee)
									<option value="{{ $consignee->company_id }}" data-name="{{ $consignee->name }}" {{ ($bl->consignee_id == $consignee->company_id) ? 'selected' : '' }} >{{ $consignee->code . ' - ' . $consignee->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-10">
								<label>Consignee Name</label>
								<input type="text" class="form-control form-control-lg" name="consignee_name" value="{{ $bl->consignee_name }}" />
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Consignee Address</label>
								<select class="form-control form-control-lg bl_address m-b-5" name="consignee_add_id" >
									@foreach($bl->consigneeAddresses AS $add)
									<option value="{{ $add->id }}" >
										@if($add->description == null)
										DEFAULT
										@else
										{{ $add->description . ' - ' . str_replace('<br>', ', ', $add->address) }}
										@endif
									</option>
									@endforeach
								</select>
								<textarea name="consignee_add" class="form-control form-control-lg" rows="4" >{{ $bl->consignee_add }}</textarea>
							</div>
						</div>
					</div>
					<div class="loader">
						<div class="loader-item text-center">
							<i class="fas fa-3x fa-spinner fa-spin"></i>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Notify Party Code</label>
								<select id="notify_code" class="ddl form-control form-control-lg m-b-5" name="notify_id" required>
									<option disabled selected value>-- select an option --</option>
									@foreach($notify_parties AS $np)
									<option value="{{ $np->comp_id }}" data-name="{{ $np->name }}" {{ ($bl->notify_id == $np->comp_id) ? 'selected' : '' }} >{{ $np->code . ' - ' . $np->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-10">
								<label>Notify Party Name</label>
								<input type="text" class="form-control form-control-lg" name="notify_name" value="{{ $bl->notify_name }}" />
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Notify Party Address</label>
								<select class="form-control form-control-lg bl_address m-b-5" name="notify_add_id" >
									@foreach($bl->notifyAddresses AS $add)
									<option value="{{ $add->id }}" >
										@if($add->description == null)
										DEFAULT
										@else
										{{ $add->description . ' - ' . str_replace('<br>', ', ', $add->address) }}
										@endif
									</option>
									@endforeach
								</select>
								<textarea name="notify_add" class="form-control form-control-lg" rows="4">{{ $bl->notify_add }}</textarea>
							</div>
						</div>
					</div>
					<div class="loader">
						<div class="loader-item text-center">
							<i class="fas fa-3x fa-spinner fa-spin"></i>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>BL Date</label>
						<input id="bl_date" type="text" class="form-control form-control-lg datetimepicker" name="bl_date" placeholder="Enter BL Date" value="{{ date('d/m/Y', strtotime($bl->bl_date)) }}"/>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Freight Term</label>
						<select class="form-control form-control-lg" name="freight_term">
							<option value="0" {{ ($bl->freight_term == 0) ? 'selected' : '' }}>Prepaid</option>
							<option value="1" {{ ($bl->freight_term == 1) ? 'selected' : '' }}>Collect</option>
							<option value="2" {{ ($bl->freight_term == 2) ? 'selected' : '' }}>Payable</option>
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-5">
						<label>Container Ownership</label>
						<select class="form-control form-control-lg m-b-5" name="cont_owner" {{ ($vessel->default_shipment != 'FCL') ? 'disabled' : ''}}>
							<option value="0" {{ ($bl->cont_ownership == 0) ? 'selected' : '' }}>Shipper-Owned Container (SOC)</option>
							<option value="1" {{ ($bl->cont_ownership == 1) ? 'selected' : '' }}>Carrier-Owned Container (COC)</option>
						</select>
						<span><i>(only for FCL shipment)</i></span>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Telex Release</label>
						<select id="telex_option" class="form-control form-control-lg" name="telex_release">
							<option value="0">No</option>
							<option value="1" {{ ($bl->telex_status == 1) ? 'selected' : '' }}>Yes</option>
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Telex Release Attachment</label>
						<div class="input-group input-group-lg">
							<input id="telex_attachment" type="file" accept="application/pdf" class="form-control" name="telex_attachment" disabled />
							<div class="input-group-append">
								<span id="attachment_preview" class="input-group-text full-height" data-toggle="tooltip" title="Click to view preview of attachment"><i class="fas fa-file-image"></i></span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>No. of BL</label>
						<input id="no_of_bl" type="number" class="form-control form-control-lg" name="no_of_bl" min=1 value="{{ $bl->no_of_bl }}" />
					</div>
				</div>
			</div>
		</div>
	</div>

	<button type="button" id="btn_cancel" class="btn btn-danger btn-action m-r-10">Cancel</button>
	<button type="button" class="btn btn-primary btn-action btn-save m-r-10" data-step="save">Save</button>

	<div class="pull-right">
		<button type="button" class="btn btn-success btn-action btn-save m-r-10" data-step="cargo">Cargo</button>
		<button type="button" class="btn btn-warning btn-action btn-save m-r-10" data-step="charges">Charges</button>
		<button type="button" class="btn btn-primary btn-save btn-action m-r-10" data-step="so">Print Shipping Order</button>
		<button type="button" class="btn btn-primary btn-save btn-action" data-step="bl">Print BL</button>
	</div>

	<input hidden name="step">
	<input hidden name="bl_format">
	<input hidden name="cname">

	{{-- <button type="button" class="btn btn-success btn-action btn-save pull-right" data-step="cargo">Proceed to Cargo</button> --}}
</form>

<div class="modal fade" id="modal_preview">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Telex Attachment Preview</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<object id="embed_pdf" style="height: 700px; width: 100%" type='application/pdf' data="/uploads/{{ $bl->telex_path }}"></object>
				<!-- <img src="" width="100%" /> -->
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-success" data-dismiss="modal">Close</a>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modal_changevessel" style="overflow:hidden;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Change Vessel / Voyage</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Vessel</label>
							<select id="change_vessel" class="form-control form-control-lg select2">
								<option disabled="" selected="" value="NULL">-- select an option --</option>
								@foreach($vessels AS $vessel)
								<option value="{{ $vessel->id }}">{{ $vessel->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Voyage</label>
							<select disabled="" id="change_voyage" class="form-control form-control-lg select2">
								<option disabled="" selected="" value="-">-- select an option --</option>
								@foreach($voyages AS $voyage)
								<option value="{{ $voyage->id }}">{{ $voyage->voyage_id }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
				<a id="btn_update" href="javascript:;" class="btn btn-primary">Update</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_print">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Print BL</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<table id="table_print" class="table table-borderless text-black m-t-10 f-s-14">
					<tbody>
						<tr>
							<td width="1%" class="no-wrap"><strong>BL Format</strong></td>
							<td>
								<select class="form-control form-control-lg" id="blformat">
									<option value="apollo">Apollo Agencies (1980)</option>
									<option value="malsuria">Malsuria</option>
								</select>
							</td>
						</tr>
						<tr>
							<td width="1%" class="no-wrap"><strong>Company Name</strong></td>
							<td>
								<input id="cname1" autocomplete="off" type="text" class="txt_companyname form-control form-control-lg m-b-10" maxlength="100" placeholder="Enter Company Name">
								<input id="cname2" autocomplete="off" type="text" class="txt_companyname form-control form-control-lg" maxlength="100" placeholder="Enter Company Name">
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
				<input type="hidden" name="bl_id">
				<button id="btn_proceed" type="button" class="btn btn-primary">Proceed</button>
			</div>
		</div>
	</div>
</div>

@stop

@section('page_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />

<script src="/js/bill_lading.js?v=1"></script>

<script>
	$(document).on('focus', '.datetimepicker', function() {
		$(this).datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$("#vessel_type").change(function(){
		if($(this).val() == "LCL"){
			$("select[name='cont_owner']").attr("disabled", "");
		} else {
			$("select[name='cont_owner']").removeAttr("disabled");
		}
	});

	$("#change_vessel").on("select2:select", function(){
		var vessel_id = $(this).val();
		if(vessel_id != "NULL"){
			$.ajax({
				type: "POST",
				data: {id: vessel_id},
				url: '/bl/voyages/list',
				success: function(data){
					if(data.success == true) {
						var html = '<option value="-">-- select an option --</option>';
						var added_count = 0;

						var date_now = moment().format('YYYY-MM-DD 00:00:00');

						for(var i = 0; i < data.result.length; i++) {
							var date_etd = moment(data.result[i].etd_pol).add(7, "days");

							if(data.result[i].booking_status == 0 && date_etd.isAfter(date_now)){
								added_count++;
								html += '<option value="' + data.result[i].id + '">' + data.result[i].voyage_id + '</option>';
							}
						}

						if(added_count > 0) {
							//Reinitialize select2 after adding in new options
							$('#change_voyage').removeAttr("disabled").select2("destroy").html(html).select2({
								dropdownParent: $('#modal_changevessel'),
								width: '100%'
							});
						} else {
							$("#change_voyage, #btn_update").attr("disabled", "");
						}
					} else {
						$("#change_voyage, #btn_update").attr("disabled", "");
					}
				}
			});
		}
	});

	var bl_id = "{{ $bl->id }}";
	var fpd_id = "{{ $bl->fpd_id }}";

	$("#btn_update").click(function(){
		// var vessel_id = $("#change_vessel").val();
		var voyage_id = $("#change_voyage").val();
		if(voyage_id != "-" || voyage_id == "null"){
			var url = "/validate-voyage/" + bl_id + "/" + voyage_id;
			$.get(url, function(data){
				if(data.success){
					$("#vessel_select").val(data.ves_id).trigger("change");
					$("input[name='vessel_id']").val(data.ves_id);

					var new_voy = data.voy_id;
					$("input[name='voyage_id']").val(new_voy);

					//Retrieve new set of voyage based on selected vessel
					$.ajax({
						type: "POST",
						data: {id: data.ves_id},
						url: '/bl/voyages/list',
						success: function(data){
							if(data.success == true) {
								var html = "";

								for(var i = 0; i < data.result.length; i++) {
									html += '<option value="' + data.result[i].id + '">' + data.result[i].voyage_id + '</option>';
								}

								$('#voyage_select').html(html).val(new_voy).trigger("change.select2");
							}
						}
					});

					$.get("/get-destinations/" + data.voy_id, function(data2){
						var option_html = "";
						var options = data2.dest;

						options.forEach(function(elem, index){
							option_html += "<option value='" + elem.id + "' data-name='" + elem.name + "' data-location='" + elem.location.toUpperCase() + "'>" + elem.code + "</option>";
						});

						var bl_pol = "{{ $bl->pol_id }}";
						var bl_pod = "{{ $bl->pod_id }}";
						var bl_fpd = "{{ $bl->fpd_id }}";

						//Set select option after adding new ports
						$("#pol_id").html(option_html).val(bl_pol).trigger("change");
						$("#pod_id").html(option_html).val(bl_pod).trigger("change");
						$("#fpd_id").html(option_html).val(bl_fpd).trigger("change");
					});

					$("#modal_changevessel").modal("toggle");
				} else {
					swal("Unable to update vessel/voyage", data.msg, "warning");
				}
			});
		}
	});

	$("#btn_changevessel").click(function(){
		$("#modal_changevessel").modal("toggle");
	});

	$(".select2").select2({
		dropdownParent: $('#modal_changevessel'),
		width: '100%'
	});

	$(".ddl").select2();

	$('#pol_id, #pod_id, #fpd_id').change(function() {
		var name = $(this).find('option:selected').attr('data-name');
		$(this).next().html('<i>' + name + '</i>');
	});

	$(document).ready(function() {

		@if($bl->getVessel->type == "General Cargo Vessel")
		@if($bl->getVessel->semicontainer_service != 1)
		$("#vessel_type").find("option[value='FCL']").text("FCL (unavailable for GCV w/o semi-container service)").prop("disabled", true);
		$("#vessel_type option[value='LCL']").prop("selected", true).trigger("change");
		@else
		$("#vessel_type").find("option").prop("disabled", false);
		if($("#vessel_type").val() == "LCL"){
			$("select[name='cont_owner']").attr("disabled", "");
		}
		@endif
		@else
		$("#vessel_type option[value='LCL']").text("LCL (unavailable for CV)").prop("disabled", true);
		@endif

		@if(!empty($vessel_detail))
		@if($vessel_detail->type == 'General Cargo Vessel' && $vessel_detail->semicontainer_service != 1)
		$('#vessel_type option:first-child').prop('disabled', true);
		$('#vessel_type option:first-child').text('FCL (unavailable for GCV w/o semi-container service)');
		$('#vessel_type option:last-child').prop('selected', true);
		@elseif($vessel_detail->type == 'Container Vessel')
		$('#vessel_type option:last-child').prop('disabled', true);
		$('#vessel_type option:last-child').text('LCL (unavailable for CV)');
		$('#vessel_type option:first-child').prop('selected', true);
		@else
		$('#vessel_type option:last-child').text('FCL');
		$('#vessel_type option:last-child').text('LCL');

		@if($vessel_detail->default_shipment == 'FCL')
		$('#vessel_type option:first-child').prop('selected', true);
		@else
		$('#vessel_type option:last-child').prop('selected', true);
		@endif
		@endif
		@endif

		var shipment_type = "{{ $bl->vessel_type }}";

		var prev_voyage;
		var prev_vessel;

		$('#voyage_select').on('focus', function() {
			prev_voyage = this.value;
		}).change(function() {
			// var elem = this;
			// var voyage_id = $(this).val();
			// var vessel_id = $('#vessel_select').val();

			// swal({
			// 	title: 'Are your sure?',
			// 	text: 'Switching voyages will refresh the page. Continue?',
			// 	type: 'warning',
			// 	showCancelButton: true,
			// 	confirmButtonText: 'Proceed',
			// 	reverseButtons: true
			// }).then((result) => {
			// 	if(result.value) {
			// 		window.location.href = '{{ action('BillLadingController@create') }}?voyage=' + voyage_id + '&vessel=' + vessel_id;
			// 	} else {
			// 		elem.value = prev_voyage;
			// 	}
			// });
		});

		$('#vessel_select').on('focus', function() {
			prev_vessel = this.value;
		}).change(function() {
			// var elem = this;
			// var vessel_id = $('#vessel_select').val();

			// swal({
			// 	title: 'Are your sure?',
			// 	text: 'Switching vessels will refresh the page. Continue?',
			// 	type: 'warning',
			// 	showCancelButton: true,
			// 	confirmButtonText: 'Proceed',
			// 	reverseButtons: true
			// }).then((result) => {
			// 	if(result.value) {
			// 		window.location.href = '{{ action('BillLadingController@create') }}?vessel=' + vessel_id;
			// 	} else {
			// 		elem.value = prev_vessel;
			// 	}
			// });
		});

		$('#no_of_bl').change(function() {
			if($(this).val() < 1) {
				$(this).val(1);
			}
		});

		$('#shipper_code, #consignee_code, #notify_code').change(function() {
			var id = $(this).find('option:selected').val();
			var name = $(this).find('option:selected').attr('data-name');

			var elem = $(this);

			elem.closest('.col-lg-4').find('.loader').css('border-radius', '10px');
			elem.closest('.col-lg-4').find('.loader').fadeIn(200);

			$.ajax({
				type: 'POST',
				url: '/bl/create/company/address',
				data: {'id' : id},
				success: function(data) {
					if(data.success) {
						var html = '';

						for(var i = 0; i < data.result.length; i++) {
							var adrs_desc = "";

							if(data.result[i].description != null){
								html += '<option value="' + data.result[i].id + '">' + data.result[i].description + " - " + data.result[i].address.replace(/<br>/g, ', ') + '</option>';
							} else {
								html += '<option value="' + data.result[i].id + '">DEFAULT</option>';
							}
						}

						elem.closest('.col-lg-12').next().find('input').prop('disabled', false);
						elem.closest('.col-lg-12').next().find('input').val(name);
						elem.closest('.col-lg-12').next().next().find('.bl_address').prop('disabled', false);
						elem.closest('.col-lg-12').next().next().find('textarea').prop('disabled', false);

						elem.closest('.col-lg-12').next().next().find('.bl_address').html(html);
						elem.closest('.col-lg-12').next().next().find('textarea').html(data.result[0].address.replace(/<br>/g, '&#13;&#10;'));
					} else {
						swal({
							title: "An error occured!",
							text: "Please try again.",
							type: "error",
							confirmButtonText: "OK"
						});
					}

					$('.loader').fadeOut(200);
				},
				error: function(data) {
					console.log("Not Successful. " + data);

					swal({
						title: "An error occured!",
						text: "Please try again.",
						type: "error",
						confirmButtonText: "OK"
					});

					$('.loader').fadeOut(200);
				}
			});
		});

		$(document).on('change', '.bl_address', function() {
			var address_id = $(this).val();
			var $this = $(this);
			$.get('/get-address/' + address_id, function(data){
				if(data.success){
					var address = data.address.address;
					address = address.replace(/<br[^>]*>/gi, "\n");
					// .replace(/, /g, '&#13;&#10;')
					$this.next().html(address);
				} else {
					swal("Oops!", "Address not found!", "error");
				}
			});
		});

		function readURL(input) {
			if(input.files[0].type != "application/pdf"){
				swal("Oops!", "Uploaded file is not in pdf format!", "warning");
				$("#telex_attachment").val("");
				$('#modal_preview').find('.modal-body object').attr('data', '');
				return;
			}

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('#modal_preview').find('.modal-body object').attr('data', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}


		// Telex release functions and validation
		$("#telex_attachment").change(function() {
			readURL(this);
		});

		$('#telex_option').change(function() {
			if($(this).val() === "1") {
				$('#telex_attachment').prop('disabled', false);
			} else {
				$('#telex_attachment').prop('disabled', true);
			}
		});

		//Remove disable if has telex
		@if($bl->telex_status)
		$("#telex_attachment").removeAttr("disabled");
		@endif

		$('#attachment_preview').click(function() {
			if(($('#telex_attachment').get(0).files.length === 1 || $("#embed_pdf").attr("data") !== "") && $('#telex_option').val() === "1") {
				$('#modal_preview').modal();
			} else if($('#telex_attachment').get(0).files.length === 0 && $('#telex_option').val() === "1") {
				swal({
					title: 'No attachment to preview!',
					text: 'Have you uploaded one yet?',
					type: 'info',
					confirmButtonText: 'OK'
				});
			}else {
				swal({
					title: 'Telex release not available!',
					text: 'Have you already made a telex release for this BL?',
					type: 'info',
					confirmButtonText: 'OK'
				});
			}
		});

		$('#btn_cancel').click(function() {
			swal({
				title: 'Are your sure?',
				text: 'All your current progress will be lost.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Proceed',
				reverseButtons: true
			}).then((result) => {
				if(result.value) {
					window.location.href = '{{ action('BillLadingController@index') }}';
				}
			});
		});

		$(document).on('click', '.btn-save', function(e) {
			e.preventDefault();

			$(this).prop("disabled", true);

			var nextStep = $(this).attr('data-step');
			console.log(nextStep);

			var error = false;
			var error_msg = '';
			
			if($('#shipper_name').val() == '' || $('#shipper_name').val() == ' ') {
				error = true;
				error_msg = 'Shipper can\'t be empty.';
			} else if($('#consignee_name').val() == '' || $('#consignee_name').val() == ' ') {
				error = true;
				error_msg = 'Consignee can\'t be empty.';
			} else if($('#notify_name').val() == '' || $('#notify_name').val() == ' ') {
				error = true;
				error_msg = 'Notify party can\'t be empty.';
			} else if($('#bl_date').val() == '') {
				error = true;
				error_msg = 'BL date can\'t be empty.';
			}

			if(error) {
				swal({
					title: 'Error',
					text: error_msg,
					type: 'error',
					confirmButtonText: 'OK'
				});

				$(this).removeAttr("disabled");

			} else {

				$("input[name='step']").val(nextStep);

				if(nextStep == "bl"){
					$("#modal_print").modal("toggle");
					$(this).removeAttr("disabled");
				} else {
					$('#bl_form').submit();
				}
				
			}
		});

		//For print BL proceed button, pass BL format info to hidden textbox then submit form
		$("#btn_proceed").click(function(){
			$(this).prop("disabled", true);
			var bl_format = $("#blformat").val();
			var company_name = [$("#cname1").val(), $("#cname2").val()];
			$("input[name='bl_format']").val(bl_format);
			$("input[name='cname']").val(company_name.toString());
			$("#bl_form").submit();
		});

		@if(session('no-voyage'))
		swal({
			title: 'Error',
			html: '{!! session()->pull('no-voyage') !!}',
			type: 'error',
			showCancelButton: true,
			confirmButtonText: 'Create Voyage',
			reverseButtons: true
		}).then((result) => {
			if(result.value) {
				window.location.href = '{{ action('VoyageController@create') }}';
			} else {
				window.location.href = '{{ action('BillLadingController@index') }}';
			}
		});
		@endif
	});
</script>
@stop