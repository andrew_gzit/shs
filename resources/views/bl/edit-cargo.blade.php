@extends("layouts.nav")
@section('page_title', 'BL Cargo')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('BillLadingController@cargo', $hashBL) }}">BL Cargo</a></li>
	<li class="breadcrumb-item active">Edit Cargo</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Edit Cargo</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<form id="cargo_form" class="cargo_form" action="{{ action('BillLadingController@update_cargo', [$hashBL, $hashCG]) }}" method="POST">
	@csrf
	<div class="row">
		<div class="col-lg-12 col-xl-12">
			<div class="panel">
				<div class="panel-body p-20">
					<h4>{{ $vessel }} Shipment</h4>
					<hr>
					@if($vessel == "FCL")
					<table id="qty_table" class="table table-bordered text-center">
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th colspan=7 class="text-center">Container Type</th>
							</tr>
							<tr>
								<th></th>
								<th></th>
								<th>GP</th>
								<th>HC</th>
								<th>RF</th>
								<th>HRF</th>
								<th>FR</th>
								<th>TK</th>
								<th>OT</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th rowspan="3">Size</th>
								<th>20</th>
								<td><input id="20GP" data-type="20'GP" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="20HC" data-type="20'HC" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="20RF" data-type="20'RF" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="20HRF" data-type="20'HRF" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="20FR" data-type="20'FR" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="20TK" data-type="20'TK" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="20OT" data-type="20'OT" type="number" class="cargo-qty" value="0" /></td>
							</tr>
							<tr>
								<th>40</th>
								<td><input id="40GP" data-type="40'GP" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="40HC" data-type="40'HC" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="40RF" data-type="40'RF" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="40HRF" data-type="40'HRF" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="40FR" data-type="40'FR" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="40TK" data-type="40'TK" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="40OT" data-type="40'OT" type="number" class="cargo-qty" value="0" /></td>
                            </tr>
                            <tr>
								<th>45</th>
								<td><input id="45GP" data-type="45'GP" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="45HC" data-type="45'HC" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="45RF" data-type="45'RF" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="45HRF" data-type="45'HRF" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="45FR" data-type="45'FR" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="45TK" data-type="45'TK" type="number" class="cargo-qty" value="0" /></td>
								<td><input id="45OT" data-type="45'OT" type="number" class="cargo-qty" value="0" /></td>
							</tr>
						</tbody>
					</table>
					<button id="btn_reset" type="button" class="btn btn-success">Reset Quantity</button>
					@else
					<div class="row">
						<div class="col-lg-2">
							<div class="form-group m-b-20">
								<label>Quantity</label>
								<input id="cargo_qty" min="1" name="cargo_qty" type="number" data-title="Quantity" class="required form-control form-control-lg" placeholder="Quantity" value="{{ $cargo->cargo_qty }}" />
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group m-b-20">
								<label>Packing</label>
								<input id="cargo_packing" name="cargo_packing" type="text" data-title="Packing" class="required form-control form-control-lg" placeholder="Packing" value="{{ $cargo->cargo_packing }}" required />
							</div>
						</div>
					</div>
					@endif
					<hr>
					<!-- <div class="row">
						<div class="col-lg-6">
							<div class="form-group m-b-20">
								@if($vessel == "FCL")
								<label>Qty / Size & Type</label>
								@else
								<label>Qty / Packing</label>
								@endif
								<input id="cargo_name" type="text" name="cargo_name" class="form-control form-control-lg" value="{{ $cargo->cargo_name }}" readonly />
							</div>
						</div>
					</div> -->
					<div class="cargo-container">
						@if(count($grouped) > 0)
						@foreach($grouped AS $key => $container)
						<div id="div_{{ $key }}">
							@foreach($container AS $ea)
							<div class="row container-row">
								<div class="col-lg-3">
									<div class="form-group m-b-10">
										<label>Container No.</label>
										<input type="hidden" name="cargo_detail_id[]" class="cargo_id" value="{{ $ea->id }}">
										<input type="text" name="container_no[]" class="form-control form-control-lg" placeholder="Container No." value="{{ $ea->container_no }}" />
									</div>
								</div>
								<div class="col-lg-3">
									<div class="form-group m-b-10">
										<label>Seal No.</label>
										<input type="text" name="seal_no[]" class="form-control form-control-lg" placeholder="Seal No." value="{{ $ea->seal_no }}" />
									</div>
								</div>
								<div class="col-lg-3">
									<div class="form-group m-b-10">
										<label>Size</label>
										<input type="text" name="container_size[]" class="form-control form-control-lg" placeholder="Size" value="{{ $ea->size }}" readonly />
									</div>
								</div>
								<div class="col-lg-3">
									<div class="form-group m-b-10">
										<label>Type</label>
										<input type="text" name="container_type[]" class="form-control form-control-lg" placeholder="Type" value="{{ $ea->type }}" readonly />
									</div>
								</div>
							</div>
							@endforeach
						</div>
						@endforeach
						@else


						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12 col-xl-12">
			<div class="panel">
				<div class="panel-body p-20">
					<h4>Cargo Details</h4>
					<hr>
					<div class="row">

						<div class="col-lg-4">
							<div class="form-group m-b-16">
								<label>Markings</label>

								<div id="div_markings" contenteditable="true" data-name="markings" class="create editablecontent">{!! nl2br($cargo->markings) !!}</div>

								<!-- 	<textarea name="markings" class="form-control form-control-lg" rows="5" maxrow="5" placeholder="Enter Markings">{{ $cargo->markings }}</textarea> -->
							</div>
							<div class="form-group m-b-20">
								<label>UN Code / Class</label>

								<div id="div_uncode" contenteditable="true" data-name='uncode' class="create editablecontent">{!! nl2br($cargo->uncode) !!}</div>

								<!-- <textarea name="uncode" class="form-control form-control-lg" rows="5" maxrow="5" placeholder="Enter UN Code / Class">{{ $cargo->uncode }}</textarea> -->
							</div>

							@if($vessel == "FCL")
							<div class="form-group m-b-20">
								<label>Temperature</label>
								<input name="temperature" type="number" class="form-control form-control-lg" placeholder="Temperature" {{ ($vessel != 'FCL') ? 'disabled' : '' }} value="{{ $cargo->temperature }}" />
							</div>
							@endif

						</div>

						<div class="col-lg-8">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group m-b-20">
										<label>Cargo Description</label>
										<input maxlength="50" name="cargo_desc" type="text" data-title="Cargo Description" class="required form-control form-control-lg" placeholder="Cargo Description" value="{{ $cargo->cargo_desc }}" />
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group m-b-20">
										<label >Cargo Type</label>
										<select name="cargo_type" class="form-control form-control-lg m-b-5" >
											<option value="General" {{ ($cargo->cargo_type == "General") ? 'selected' : '' }} >General</option>
											<option value="Timber" {{ ($cargo->cargo_type == "Timber") ? 'selected' : '' }} >Timber</option>
											<option value="Veneer" {{ ($cargo->cargo_type == "Veneer") ? 'selected' : '' }} >Veneer</option>
											<option value="Plywood" {{ ($cargo->cargo_type == "Plywood") ? 'selected' : '' }} >Plywood</option>
											<option value="Bar" {{ ($cargo->cargo_type == "Bar") ? 'selected' : '' }} >Bar</option>
											<option value="Wire Rod" {{ ($cargo->cargo_type == "Wire Rod") ? 'selected' : '' }} >Wire Rod</option>
											<option value="Coil" {{ ($cargo->cargo_type == "Coil") ? 'selected' : '' }} >Coil</option>
											<option value="Plate" {{ ($cargo->cargo_type == "Plate") ? 'selected' : '' }} >Plate</option>
											<option value="Jumbo Bags" {{ ($cargo->cargo_type == "Jumbo Bags") ? 'selected' : '' }} >Jumbo Bags</option>
										</select>
									</div>
								</div>
								<div class="col-lg-3">
									<div class="form-group m-b-20">
										<label>Weight</label>
										<input type="number" data-type="number" name="weight" class="form-control form-control-lg" min="0.000" value="0.000" step=".001" value="{{ $cargo->weight }}">
									</div>
								</div>
								<div class="col-lg-3">
									<div class="form-group m-b-20">
										<label>MT/KG</label>
										@if(env("SITE") == "SHS")
										<select name="weight_unit" class="form-control form-control-lg m-b-5">
											<option value="MT">MT</option>
											<option value="KG" {{ ($cargo->weight_unit == "KG") ? 'selected' : '' }}>KG</option>
										</select>
										@else
										<select name="weight_unit" class="form-control form-control-lg m-b-5">
											<option value="KG">KG</option>
											<option value="MT" {{ ($cargo->weight_unit == "MT") ? 'selected' : '' }}>MT</option>
										</select>
										@endif
									</div>
								</div>
								<div class="col-lg-3">
									<div class="form-group m-b-20">
										<label>Volume M3</label>
										<input type="number" data-type="number" name="volume" class="form-control form-control-lg" min="0.000" value="0.000" step=".001" value="{{ $cargo->volume }}">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group m-b-20" style="overflow-x: auto">
										<label>Detailed Description</label>

										<div id="div_detaileddesc" contenteditable="true" data-name="detailed_desc" class="create editablecontent detaileddesc">{!! nl2br($cargo->detailed_desc) !!}</div>

										<!-- <textarea style="width: 700px; font-family: 'Helvetica'" name="detailed_desc" maxrow="15" placeholder="Enter Detailed Description" class="form-control form-control-lg" rows="15" >{{ $cargo->detailed_desc }}</textarea> -->
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>


	<input id="detailed_desc" name="detailed_desc" hidden="">
	<input id="markings" name="markings" hidden="">
	<input id="uncode" name="uncode" hidden="">

	<input id="cargo_name" hidden="" type="text" name="cargo_name" class="form-control form-control-lg" value="{{ $cargo->cargo_name }}" readonly />
	<input id="raw_qty" name="raw_qty" type="hidden" class="form-control form-control-lg" value="{!! $cargo->raw_qty !!}" />
	<button type="button" id="btn_cancel" class="btn btn-danger btn-action m-r-10">Back to Cargo Details</button>
	<button type="button" class="btn btn-primary btn-action btn-save">Save</button>
</form>
@stop

@section('page_script')
<script type="text/javascript" src="/js/shortcut.js"></script>
<script type="text/javascript" src="/js/bl_cargo.js?v=1"></script>

<script>

	$(function(){
		return_url = "{{ action('BillLadingController@cargo', $hashBL) }}";

		container_type_obj = {
			"20GP": parseInt("{{ isset($grouped['20GP']) ? count($grouped['20GP']) : '0' }}"),
			"20HC": parseInt("{{ isset($grouped['20HC']) ? count($grouped['20HC']) : '0' }}"),
			"20RF": parseInt("{{ isset($grouped['20RF']) ? count($grouped['20RF']) : '0' }}"),
			"20HRF": parseInt("{{ isset($grouped['20HRF']) ? count($grouped['20HRF']) : '0' }}"),
			"20FR": parseInt("{{ isset($grouped['20FR']) ? count($grouped['20FR']) : '0' }}"),
			"20TK": parseInt("{{ isset($grouped['20TK']) ? count($grouped['20TK']) : '0' }}"),
			"20OT": parseInt("{{ isset($grouped['20OT']) ? count($grouped['20OT']) : '0' }}"),
			"40GP": parseInt("{{ isset($grouped['40GP']) ? count($grouped['40GP']) : '0' }}"),
			"40HC": parseInt("{{ isset($grouped['40HC']) ? count($grouped['40HC']) : '0' }}"),
			"40RF": parseInt("{{ isset($grouped['40RF']) ? count($grouped['40RF']) : '0' }}"),
			"40HRF": parseInt("{{ isset($grouped['40HRF']) ? count($grouped['40HRF']) : '0' }}"),
			"40FR": parseInt("{{ isset($grouped['40FR']) ? count($grouped['40FR']) : '0' }}"),
			"40TK": parseInt("{{ isset($grouped['40TK']) ? count($grouped['40TK']) : '0' }}"),
            "40OT": parseInt("{{ isset($grouped['40OT']) ? count($grouped['40OT']) : '0' }}"),
            "45GP": parseInt("{{ isset($grouped['45GP']) ? count($grouped['45GP']) : '0' }}"),
			"45HC": parseInt("{{ isset($grouped['45HC']) ? count($grouped['45HC']) : '0' }}"),
			"45RF": parseInt("{{ isset($grouped['45RF']) ? count($grouped['45RF']) : '0' }}"),
			"45HRF": parseInt("{{ isset($grouped['45HRF']) ? count($grouped['45HRF']) : '0' }}"),
			"45FR": parseInt("{{ isset($grouped['45FR']) ? count($grouped['45FR']) : '0' }}"),
			"45TK": parseInt("{{ isset($grouped['45TK']) ? count($grouped['45TK']) : '0' }}"),
			"45OT": parseInt("{{ isset($grouped['45OT']) ? count($grouped['45OT']) : '0' }}")
		};

		is_semicontainer = "{{ $bl->getVessel->semicontainer_service }}";
	});

	var raw = "{!! $cargo->raw_qty !!}";

	getQuantities();

	function getQuantities() {
		var qty = 0;
		var type = "";
		var semi_split = raw.split(";");

		for(var i = 0; i < semi_split.length; i++) {
			if(semi_split[i] != "") {
				var comma_split = semi_split[i].split(",");
				qty = comma_split[0];
				type = comma_split[1];

				$('.cargo-qty[data-type="' + type + '"]').val(qty);
			}
		}
	}

	@if(count($grouped) == 0)
	$(".cargo-qty").trigger("change");
	@endif


	$("input[name='weight']").val("{{ $cargo->weight }}");
	$("input[name='volume']").val("{{ $cargo->volume }}");
</script>
@stop
