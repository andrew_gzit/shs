@extends("layouts.nav")
@section('page_title', 'Edit Inward BL')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('BillLadingController@index') }}">Bill of Lading</a></li>
</ol>
@stop

@section('content')
<h1 class="page-header">
	Edit Inward Bill of Lading
	<span class="f-s-18">
		{{ $bl->bl_no }}
	</span>
</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<form id="form_inwardbl" autocomplete="off" method="POST" action="{{ action('InwardBLController@update', Hashids::encode($bl->id)) }}">
	@csrf
	<div class="panel">
		<div class="panel-body">
			<h4>Inward BL Details</h4>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Vessel</label>
						<select id="txt_vessel" data-title="Vessel" class="form-control form-control-lg ddl required" name="txt_vessel">
							<option disabled="" selected="" value>-- select an option --</option>
							<!-- <option value="{{ $bl->vessel }}" selected="">{{ $bl->vessel }}</option> -->
							@foreach($vessels AS $vessel)
							<option value="{{ $vessel->name }}">{{ $vessel->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>Voyage</label>
						<select id="txt_voyage" data-title="Voyage" class="ddl required form-control form-control-lg" name="txt_voyage">
							<option disabled="" value="-">-- select an option --</option>
							<option value="{{ $bl->voyage }}" selected="">{{ $bl->voyage }}</option>
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-10">
						<label>BL No.</label>
						<input id="txt_blno" type="text" data-title="BL No." class="required form-control form-control-lg" placeholder="Enter BL No." name="txt_blno" value="{{ $bl->bl_no }}">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-4 mb-2">
					<label class="no-wrap f-w-700 text-uppercase" style="margin-bottom: .3rem">Port of Loading</label>
					<div class="d-flex">
						<div class="form-group m-r-10 mb-0">
							<select style="max-width: 110px" id="pol_id" disabled="" data-title="POL" class="ddl-port form-control form-control-lg m-b-5" name="pol_id">
								<option disabled selected value>None</option>
							</select>
						</div>
						<div style="flex: 1" class="form-group mb-0">
							<input type="text" class="required form-control form-control-lg" data-title="POL Location" name="txt_polname" data-id="pol_id" placeholder="Enter Location" value="{{ $bl->pol_name }}">
						</div>
					</div>
					<span></span>
				</div>
				<div class="col-lg-4 mb-2">
					<label class="no-wrap f-w-700 text-uppercase" style="margin-bottom: .3rem">Port of Discharge</label>
					<div class="d-flex">
						<div class="form-group m-r-10 mb-0">
							<select style="max-width: 110px" disabled="" id="pod_id" data-title="POD" class="ddl-port form-control form-control-lg m-b-5" name="pod_id">
								<option disabled selected value>None</option>
							</select>
						</div>
						<div style="flex: 1" class="form-group mb-0">
							<input type="text" class="required form-control form-control-lg" data-title="POD Location" name="txt_podname" data-id="pod_id" placeholder="Enter Location" value="{{ $bl->pod_name }}">
						</div>
					</div>
					<span></span>
				</div>
				<div class="col-lg-4 mb-2">
					<label class="no-wrap f-w-700 text-uppercase" style="margin-bottom: .3rem">Final Port of Discharge</label>
					<div class="d-flex">
						<div class="form-group m-r-10 mb-0">
							<select style="max-width: 110px" disabled="" id="fpd_id" data-title="FPD" class="ddl-port form-control form-control-lg m-b-5" name="fpd_id">
								<option disabled selected value>None</option>
							</select>
						</div>
						<div style="flex: 1" class="form-group mb-0">
							<input type="text" class="required form-control form-control-lg" data-title="FPD Location" name="txt_fpdname" data-id="fpd_id" placeholder="Enter Location" value="{{ $bl->fpd_name }}">
						</div>
					</div>
					<span></span>
				</div>
			</div>

			<hr>

			<div class="row">
				<div class="col-lg-4 col-md-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Shipper Code</label>
								<select id="shipper_code" class="ddl-companycode ddl form-control form-control-lg m-b-5" name="shipper_id">
									<option disabled selected value>-- select an option --</option>
									@foreach($shippers AS $comp)
									<option data-name="{{ $comp->getName() }}">{{ $comp->code }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-10">
								<label>Shipper Name</label>
								<input id="shipper_name" data-title="Shipper Name" type="text" class="ddl-companyname form-control form-control-lg required" name="txt_shippername" placeholder="Enter Shipper Name" value="{{ $bl->shipper_name }}" />
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Shipper Address</label>
								<select class="form-control form-control-lg bl_address m-b-5" name="shipper_add_id">
									<option disabled selected value>-- select an option --</option>
								</select>
								<textarea name="txt_shipperadd" data-title="Shipper Address" class="required form-control form-control-lg" rows="4" placeholder="Enter Address">{{ $bl->shipper_add }}</textarea>
							</div>
						</div>
					</div>
					<div class="loader">
						<div class="loader-item text-center">
							<i class="fas fa-3x fa-spinner fa-spin"></i>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Consignee Code</label>
								<select id="consignee_code" class="ddl-companycode ddl form-control form-control-lg m-b-5" name="consignee_id">
									<option disabled selected value>-- select an option --</option>
									@foreach($consignees AS $cons)
									<option data-name="{{ $cons->name }}">{{ $cons->getCompanyOfType()->code }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-10">
								<label>Consignee Name</label>
								<input type="text" data-title="Consignee Name" class="ddl-companyname form-control form-control-lg required" name="txt_consigneename" placeholder="Enter Consignee Name" value="{{ $bl->consignee_name }}"/>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Consignee Address</label>
								<select class="form-control form-control-lg bl_address m-b-5" name="consignee_add_id">
									<option disabled selected value="-">-- select an option --</option>
								</select>
								<textarea name="txt_consigneeadd" data-title="Consignee Address" class="form-control form-control-lg required" rows="4" placeholder="Enter Address">{{ $bl->consignee_add }}</textarea>
							</div>
						</div>
					</div>
					<div class="loader">
						<div class="loader-item text-center">
							<i class="fas fa-3x fa-spinner fa-spin"></i>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Notify Party Code</label>
								<select id="notify_code" class="ddl form-control form-control-lg m-b-5" name="notify_id">
									<option disabled selected value="-">-- select an option --</option>
									@foreach($consignees AS $cons)
									<option data-name="{{ $cons->name }}">{{ $cons->getCompanyOfType()->code }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-10">
								<label>Notify Party Name</label>
								<input type="text" data-title="Notify Party Name" class="form-control form-control-lg required" name="txt_notifyname" placeholder="Enter Notify Party Name" value="{{ $bl->notify_name }}" />
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group m-b-20">
								<label>Notify Party Address</label>
								<select class="form-control form-control-lg bl_address m-b-5" name="notify_add_id">
									<option disabled selected value="-">-- select an option --</option>
								</select>
								<textarea name="txt_notifyadd" data-title="Notify Party Address" class="form-control form-control-lg required" rows="4" placeholder="Enter Address">{{ $bl->notify_add }}</textarea>
							</div>
						</div>
					</div>
					<div class="loader">
						<div class="loader-item text-center">
							<i class="fas fa-3x fa-spinner fa-spin"></i>
						</div>
					</div>
				</div>
			</div>

			<hr>

			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>No. of BL</label>
						<input type="number" min="1" class="form-control form-control-lg" placeholder="Enter No. of BL" id="txt_no_of_bl" name="txt_no_of_bl" value="3">
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="panel">
		<div class="panel-body">
			<h4>Cargo Details</h4>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>No. of Packages</label>
						<input type="text" data-title="No. of Packages" name="txt_no_packages" class="required form-control form-control-lg" placeholder="Enter No. of Packages" value="{{ $bl->no_packages }}">
					</div>
					<div class="form-group">
						<label>No. of Containers / Markings</label>
						<textarea class="required form-control form-control-lg" rows="5" placeholder="Enter No. of Containers / Markings" name="txt_containers">{{ $bl->containers }}</textarea>
					</div>
				</div>
				<div class="col-lg-4">
					<!-- <div class="form-group">
						<label>Cargo Description</label>
						<input type="text" data-title="Cargo Description" class="form-control form-control-lg" placeholder="Enter Cargo Description" name="txt_cargodesc" value="{{ $bl->cargo_desc }}">
					</div> -->
					<div class="form-group">
						<label>Cargo Description</label>
						<textarea data-title="Detailed Description" class="required form-control form-control-lg" rows="5" placeholder="Enter Cargo Description" name="txt_detailedesc">{{ $bl->detailed_desc }}</textarea>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-row">
						<div class="col">
							<div class="form-group">
								<label>Weight</label>
								<input type="text" data-title="Weight" class="form-control form-control-lg required" placeholder="Enter Weight" name="txt_weight" value="{{ $bl->weight }}">
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label>MT/KG</label>
								<select class="form-control form-control-lg" name="txt_weightunit">
									<option value="MT">MT</option>
									<option value="KG" {{ $bl->weight_unit == "KG" ? 'selected' : '' }} >KG</option>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-lg-6">
								<label>Volume</label>
								<input type="text" data-title="Volume" class="form-control form-control-lg required" placeholder="Enter Volume" name="txt_volume" value="{{ $bl->volume }}">
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<button type="button" id="btn_cancel" class="btn btn-danger btn-action m-r-10">Cancel</button>
	<button type="button" id="btn_save" class="btn btn-primary btn-action">Save</button>
</form>
@stop

@section("page_script")
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script src="/js/inwardbl.js"></script>

<script type="text/javascript">
	$(function(){
		var empty_option = "<option disabled='' selected=''>-- select an option --</option>";
		var vessel = "{{ $bl->vessel }}";
		var voyage = "{{ $bl->voyage }}";
		var bl_no = "{{ $bl->bl_no }}";

		$(".ddl").select2({
			tags: true
		});

		$("#txt_vessel").on('change', function (e) {
			retrieveVoyages($(this).val());
		});

		$("#txt_voyage").on('change', function (e) {
			var voy_id = $(this).find("option:selected").attr('data-voy-id');
			console.log(voy_id);
			if(voy_id != null){
				retrieveDestinations(voy_id);
			} else {
				//Custom voyage, show all ports for user to select
				$.get("/get-ports", function(data){
					$(".ddl-port").html(data).select2().prop("disabled", false).trigger("change");
				});
			}
		});

		$(".loader").last().fadeIn(100);

		if($("#txt_vessel").find("option[value='{{ $bl->vessel }}']").length == 0){
			var newOption = new Option('{{ $bl->vessel }}', '{{ $bl->vessel }}', true, true);
			$('#txt_vessel').append(newOption).trigger('change');
		}

		$("#txt_vessel").val("{{ $bl->vessel }}").trigger("change");

		setTimeout(function(){
			if($("#txt_voyage").find("option[value='{{ $bl->voyage }}']").length == 0){
				var newOption = new Option('{{ $bl->voyage }}', '{{ $bl->voyage }}', true, true);
				$('#txt_voyage').append(newOption).trigger('change');
			}
			
			$("#txt_voyage").val("{{ $bl->voyage }}").trigger("change");
		}, 1000);

		//Select ports based on saved inward bl details
		setTimeout(function(){
			$("#pol_id").val("{{ $bl->pol_id }}").trigger("change");
			$("#pod_id").val("{{ $bl->pod_id }}").trigger("change");
			$("#fpd_id").val("{{ $bl->fpd_id }}").trigger("change");

			$("input[name='txt_polname']").val("{{ $bl->pol_name }}");
			$("input[name='txt_podname']").val("{{ $bl->pod_name }}");
			$("input[name='txt_fpdname']").val("{{ $bl->fpd_name }}");
			$(".loader").last().fadeOut(100);
		}, 1500);


		$(".ddl-companycode").change(function(){
			var row = $(this).closest(".row");
			var comp_name = $(this).find("option:selected").attr('data-name');
			row.find(".ddl-companyname").val(comp_name);
		});

		$('#txt_no_of_bl').change(function() {
			if($(this).val() < 1) {
				$(this).val(1);
			}
		});

		$("#btn_save").click(function(){

			$(this).prop("disabled", true);

			var valid = true;

			$(".required").each(function(){
				if($(this).val() == "" || $(this).val() == null){
					valid = false;
					$(this).focus();
					swal("Oops!", $(this).attr("data-title") + " is required.", "warning");
					$("#btn_save").removeAttr("disabled");
					return false;
				}
			});

			if(valid){
				if($("#pol_id").val() == $("#fpd_id").val() && $("#pol_id").val() != null){
					swal("Oops!", "FPD cannot be the same as POL", "warning");
					valid = false;
					$("#btn_save").removeAttr("disabled");
					return false;
				}

				if(bl_no !== $("#txt_blno").val()){
					$.get("/inward-bl-check/" + $("#txt_blno").val(), function(data){
						if(!data.success){
							$("#txt_blno").focus();
							swal("Oops!", "BL No has been used.", "warning");
						} else {
							if(valid){
								$("#form_inwardbl").submit();
							}
						}
					});
				} else {
					$("#form_inwardbl").submit();
				}
			}
		});

		function retrieveVoyages(vessel_name){
			$.ajax({
				type: "GET",
				url: '/voyage-list/' + vessel_name,
				success: function(data){
					if(data.success == true) {
						var html = empty_option;
						var added = 0;

						for(var i = 0; i < data.voyages.length; i++) {
							if(data.voyages[i].booking_status == 0){
								added++;
								html += '<option value="' + data.voyages[i].voyage_id + '" data-voy-id=' + data.voyages[i].id  +'>' + data.voyages[i].voyage_id + '</option>';
							}
						}

						if(added > 0) {
							$('#txt_voyage').html(html);
						} else {
							$('#txt_voyage').html(empty_option);
						}
					} else {
						if(vessel_name == vessel){
							var html = empty_option;

							var data = {
								id: voyage,
								text: voyage,
							};

							var newOption = new Option(data.text, data.id, true, true);
							$('#txt_voyage').html(html).append(newOption).trigger('change');

						} else {
							$('#txt_voyage').html(empty_option);
						}
					}
				}
			});
			$("#txt_voyage").removeAttr("disabled");
		}

		function retrieveDestinations(voyage_id){
			$.get("/get-destinations/" + voyage_id, function(data){
				if(data.success){
					var destinations = data.dest;

					var option_html = "<option disabled selected>None</option>";
					destinations.forEach(function(value, index){
						option_html += "<option value='" + value.id + "' data-name='" + value.name + "' data-location='" + value.location.toUpperCase() + "'>" + value.code + "</option>";
					});

					$("#pol_id, #pod_id, #fpd_id").html(option_html).removeAttr("disabled");
					$("#fpd_id").find("option:last").attr("selected", "selected").trigger("change");
					$("#pol_id").find("option:nth-child(2)").attr("selected", "selected").trigger("change");
					$("#pod_id").trigger("change");
				}
			});
		}

	});
</script>
@stop