<html>
<head>


    <style>
    *, ::after, ::before {
        box-sizing: border-box;
    }

    .top3{
        height: 20mm;
        width: 105mm;
    }

    .top-bl{
        width: 105mm;
        text-align: center;
    }

    .bl-label{
        display: block;
        margin-bottom: 5px;
        font-size: 9px;
        font-weight: 400;
        padding-top: 0px;
        color: #fff;
    }

    .bl-content{
        display: block;
        padding-left: 20px;
        padding-bottom: 5px;
        font-weight: 400 !important;
        font-size: 11px !important;
    }

    .no-bottom-border{
        border-bottom: none;
    }

    .table-apollo-header{
        font-size: 9px;
        font-weight: 400 !important;
    }

    .table-apollo-header td{
        border: none !important;
    }

    .table-apollo-header .container-markings{
        width: 50mm;
    }

    .table-apollo-header .num-packages{
        width: 43mm;
    }

    .table-apollo-header .description-goods{
    }

    .table-apollo-header .width{
        width: 25mm;
    }

    .bl-company h2{
        padding-top: 1in;
    }

    .p-0{
        padding: 0 !important;
    }

    .bl-lbl-goods {
        display: block;
        text-align: center;
        font-size: 13px;
        font-weight: 400;
        margin-top: 10px;
    }

    td{
        border: 1px solid #fff;
        padding: 5px;
        vertical-align: top;
    }

    main{
        padding: 5px;
        font-size: 11px;
    }

    .table-borderless td{
        border: 0;
    }

    .detailed_desc{
        /*white-space: pre;*/
    }

    .print-hidden{
        color: white;
    }


    @page { margin-top: 450px; margin-bottom: 300px; margin-left: 0; margin-right: 0; size: A4 }
    header { position: fixed; top: -453px; left: -3px; right: 0px; height: 500px; width: 100% }
    footer { position: fixed; bottom: -300px; left: 0px; right: 0px; height: 300px; }
    p { page-break-after: always; }
    p:last-child { page-break-after: never; }

    .table{
        width: 100%;
        border-collapse: collapse;
    }
</style>
</head>

<body>

    @foreach($blArr AS $bl)

    <header>
        <table class="report-container no-bottom-border">
            <thead class="report-header">
                <tr>
                    <th class="report-header-cell no-bottom-border">
                        <div class="header-info">
                            <table class="table  bg-white text-black">
                                <tr>
                                    <td class="top3" colspan="2">
                                        <label class="bl-label">Shipper</label>
                                        <span class="bl-content">
                                            {{ $bl->shipper_name }}<br>
                                            {!! nl2br($bl->shipper_add) !!}
                                        </span>
                                    </td>
                                    <td class="top-bl" rowspan="3" colspan="2">
                                        <div class="bl-company">
                                            <h2 class="print-hidden">BILL OF LADING</h2>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="top3" colspan="2">
                                        <label class="bl-label">Consignee</label>
                                        <span class="bl-content">
                                            {{ $bl->consignee_name }}<br>
                                            {!! nl2br($bl->consignee_add) !!}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="top3" colspan="2">
                                        <label class="bl-label">Notify Party</label>
                                        <span class="bl-content">
                                            @if($bl->notify_add == $bl->consignee_add)
                                            SAME AS CONSIGNEE
                                            @else
                                            {{ $bl->notify_name }}<br>
                                            {!! nl2br($bl->notify_add) !!}
                                            @endif
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="1%" width="25%">
                                        <label class="bl-label">Local Vessel</label>
                                        <span class="bl-content">&nbsp;</span>
                                    </td>
                                    <td height="1%">
                                        <label class="bl-label">From</label>
                                        <span class="bl-content">&nbsp;</span>
                                    </td>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="bl-label">(Ocean) Vessel (Or Substituted equivalent)</label>
                                        <span class="bl-content">{{ $bl->getVessel->name }}</span>
                                    </td>
                                    <td>
                                        <label class="bl-label">Voy. No.</label>
                                        <span class="bl-content">{{ $bl->getVoyage->voyage_id }}</span>
                                    </td>
                                    <td>
                                        <label class="bl-label">Freight Payable at</label>
                                        <span class="bl-content">PENANG</span>
                                    </td>
                                    <td>
                                        <label class="bl-label">B/L No.</label>
                                        <span class="bl-content">{{ $bl->bl_no }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="bl-label">Port of Loading</label>
                                        <span class="bl-content">{{ strtoupper($bl->pol_name) }}</span>
                                    </td>
                                    <td>
                                        <label class="bl-label">Port of Discharge</label>
                                        <span class="bl-content">{{ strtoupper($bl->pod_name) }}</span>
                                    </td>
                                    <td>
                                        <label class="bl-label">Final Destination (if on carriage)</label>
                                        <span class="bl-content">{{ strtoupper($bl->fpd_name) }}</span>
                                    </td>
                                    <td>
                                        <label class="bl-label">No. of Original Bs/L</label>
                                        <span class="bl-content">{{ $bl->getNoBL() }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="p-0 no-bottom-border">
                                        <label class="bl-lbl-goods print-hidden">PARTICULARS FURNISHED BY SHIPPER OF GOODS</label>
                                        <table class="table table-apollo-header table-borderless table-valign-top m-b-0">
                                            <tbody class="print-hidden">
                                                <tr>
                                                    <td class="container-markings">Marks and No., Container No.<br>Serial No.</td>
                                                    <td class="num-packages">Number and kind of packages</td>
                                                    <td class="description-goods">Description of goods said to obtain</td>
                                                    <td class="width">Gross weight <br>said to be</td>
                                                    <td class="width">Measurement <br>said to be</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </th>
                </tr>
            </thead>
        </table>
    </header>

    <footer>
        <span class="bl-content" style="padding-left: 5px">TOTAL NO. OF PACKAGES (IN WORDS) : {{ $bl->getNoPackages() }}</span>
        <table class="table">
            <tr>
                <td rowspan="4"></td>
                <td style="height: 1in; width: 50%"></td>
            </tr>
            <tr>
                <td style="height: .3in;">
                    <label class="bl-label">Freight Charges</label>
                </td>
            </tr>
            <tr>
                <td style="height: .3in;">
                    <label class="bl-label">Place and date of issue</label>
                    <span class="bl-content">{{ $bl->pol_name . ', ' . $bl->getBL_POL()->eta->format('d/m/Y') }}</span>
                </td>
            </tr>
            <tr>
                <td style="height: 1.5in;"></td>
            </tr>
        </table>
    </footer>

    <main>
        @foreach($bl->getCargos AS $cargo)
        <div style="display: inline-block; width: 170px; position: absolute; word-wrap: all; word-break: break-all;">
            @if($bl->vessel_type == "FCL")
            @foreach($cargo->containers AS $cont)
            {{ $cont->container_no . '/' . $cont->seal_no }}
            @endforeach
            @endif

            {{ $cargo->markings }}<br>
            {{ $cargo->uncode }}<br>
            {{ $cargo->temperature }}
        </div>
        <div style="display: inline-block; position: absolute; width: 100px; left: 610px;">
            {{ number_format($cargo->weight, 3) . ' ' . $cargo->weight_unit }}
        </div>
        <div style="display: inline-block; position: absolute; width: 100px; left: 710px;">
            {{ number_format($cargo->volume, 4) }}
        </div>
        <div style="float: left; margin-left: 200px; white-space: pre; width: 400px;">
            {{ $cargo->cargo_name . ' ' .  $cargo->cargo_desc}}
            <br>
            {!! nl2br($cargo->detailed_desc) !!}
        </div>
        @endforeach
    </main>

    @endforeach

</body>
</html>