@extends("layouts.nav")
@section('page_title', 'BL')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item">
		<button id="btn_filter" type="button" class="btn btn-primary m-r-5"><i class="fa fa-sliders-h m-r-10"></i>Filter</button>
		<a id="btn_create_bl" href="javascript:(0);" class="btn btn-primary"><i class="fa fa-plus m-r-10"></i>Create BL</a>
		<button type="button" id="btn-upload-edi" style="display: none" class="btn btn-primary btn-inward m-r-5">
			<i class="fa fa-file m-r-10"></i>Upload EDI
		</button>
		<a style="display: none" href="{{ action('InwardBLController@create') }}" class="btn btn-primary btn-inward"><i class="fa fa-plus m-r-10"></i>Create Inward BL</a>
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Bill of Lading</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

@if(session('success'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('success') }}
</div>
@endif

@if(session('error'))
<div class="alert alert-danger">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('error') }}
</div>
@endif

<div id="div_filter" class="panel" style="display: none">
	<div class="panel-body">
		<div class="row">
			<div class="col-xl-6">
				<div class="row">
					<div class="col-xl-4 col-md-4">
						<div class="form-group">
							<label>Vessel</label>
							<div class="input-group input-group-lg">
								<select id="vessel_select" class="form-control form-control-lg" name="vessel_id" >
									<option value="ALL">All Vessels</option>
									@foreach($vessels AS $vessel)
									<option value="{{ $vessel->id }}">{{ $vessel->name }}</option>
									@endforeach
								</select>
								<div class="input-group-append">
									<span id="vessel_info" class="input-group-text full-height" data-toggle="tooltip" title="Click to view details about selected vessel"><i class="fas fa-info-circle"></i></span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-md-4">
						<div class="form-group">
							<label>Voyage</label>
							<div class="input-group input-group-lg">
								<select disabled="" id="voyage_select" class="form-control form-control-lg" name="voyage_id" >
									<option selected value=NULL>-- select an option --</option>
									@foreach($voyages AS $voyage)
									<option value="{{ $voyage->id }}" data-scn="{{ $voyage->scn }}">{{ $voyage->voyage_id }}</option>
									@endforeach
								</select>
								<div class="input-group-append">
									<span id="voyage_info" class="input-group-text full-height" data-toggle="tooltip" title="Click to view details about selected voyage"><i class="fas fa-info-circle"></i></span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-md-4">
						<div class="form-group">
							<label>Ship Call Number (SCN)</label>
							<input id="scn" type="text" class="form-control form-control-lg" style="opacity: 1;" value="-" readonly />
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-6">
				<div class="row">
					<div class="col-xl-3 col-md-4">
						<div class="form-group">
							<label>POL</label>
							<select id="pol_select" class="form-control form-control-lg filter ddl">
								<option value="ALL">All POL</option>
								@foreach($ports AS $port)
								<option value="{{ $port->id }}">{{ $port->code }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-xl-3 col-md-4">
						<div class="form-group">
							<label>POD</label>
							<select id="pod_select" class="form-control form-control-lg filter ddl">
								<option value="ALL">All POD</option>
								@foreach($ports AS $port)
								<option value="{{ $port->id }}">{{ $port->code }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-xl-6 col-md-4">
						<div class="form-group">
							<label>Shipper</label>
							<select id="shipper_select" class="form-control form-control-lg filter ddl">
								<option value="ALL">All Shipper</option>
								@foreach($shippers AS $ea)
								<option value="{{ $ea->id }}">{{ $ea->getCompanyOfType()->code . ' - ' . $ea->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-footer">
		<div class="row justify-content-center">
			<div class="col-lg-2">
				<a href="{{ action('BillLadingController@index') }}" class="btn btn-default btn-block">Reset</a>
			</div>
		</div>
	</div>
</div>

<ul class="nav nav-tabs">
	<li class="nav-items">
		<a href="#default-tab-1" data-toggle="tab" class="nav-link active">
			<span class="d-sm-none">Outward</span>
			<span class="d-sm-block d-none">Outward</span>
		</a>
	</li>
	<li class="nav-items">
		<a href="#default-tab-2" data-toggle="tab" class="nav-link">
			<span class="d-sm-none">Inward</span>
			<span class="d-sm-block d-none">Inward</span>
		</a>
	</li>
</ul>
<div class="tab-content">
	<div class="tab-pane fade active show" id="default-tab-1">

		<div class="row">
			<div class="col-md-10">
				<ul class="nav nav-pills">
					<li class="nav-items">
						<a href="#nav-pills-tab-active-bl" data-toggle="tab" class="nav-link active">
							<span class="d-sm-none">Active BLs</span>
							<span class="d-sm-block d-none">Active BLs</span>
						</a>
					</li>
					<li class="nav-items">
						<a href="#nav-pills-tab-deleted-bl" data-toggle="tab" class="nav-link">
							<span class="d-sm-none">Deleted BLs</span>
							<span class="d-sm-block d-none">Deleted BLs</span>
						</a>
					</li>
				</ul>
			</div>
			<div class="col-md-2 text-right">
            <form id="form_delete" method="POST" class="clearfix" action="{{ action('BillLadingController@batch_delete') }}">
					@csrf
					@method('DELETE')
					<input type="text" hidden="" id="txt_bl_id" name="txt_bl_id">
					<button id="btn_batchdelete" disabled="" type="button" class="btn btn-danger m-b-15"><i class="fa fa-trash m-r-10"></i>Delete</button>
				</form>
			</div>
		</div>

		<div class="tab-content p-0">
			<div class="tab-pane fade active show" id="nav-pills-tab-active-bl">

				<div class="">
					<table id="table_bl_outward" class="table table-bordered text-center table-valign-middle f-s-14 m-b-0">
						<thead class="thead-custom">
							<tr>
								<th width="1%"><input type="checkbox" id="chk_all"></th>
								<th width="1%" class="no-wrap">BL No.</th>
								<th>Vessel</th>
								<th>Voyage</th>
								<th>POL</th>
								<th>POD</th>
								<th>Shipper</th>
								<th width="1%" class="no-wrap">Telex Release</th>
								<th width="1%"></th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
			<div class="tab-pane fade" id="nav-pills-tab-deleted-bl">
				<table id="table_bl_outward_deleted" class="table table-bordered text-center table-valign-middle f-s-14 m-b-0">
					<thead class="thead-custom">
						<tr>
							<th width="1%" class="no-wrap">BL No.</th>
							<th>Vessel</th>
							<th>Voyage</th>
							<th>POL</th>
							<th>POD</th>
							<th>Shipper</th>
							<th width="1%">Deleted At</th>
							<th width="1%"></th>
						</tr>
					</thead>
					<tbody>
                    @foreach($outward_deleted AS $deleted_ea)
						<tr>
							<td>{{ $deleted_ea->bl_no }}</td>
							<td>{{ optional($deleted_ea->getVessel)->name }}</td>
							<td>{{ optional($deleted_ea->getVoyage)->voyage_id }}</td>
							<td>{{ !empty($deleted_ea->pol) ? $deleted_ea->pol->code : '' }}</td>
							<td>{{ !empty($deleted_ea->pod) ? $deleted_ea->pod->code : '' }}</td>
							<td>{{ ($deleted_ea->cust_s_name) ? $deleted_ea->cust_s_name : optional($deleted_ea->shipper)->name }}</td>
							<td class="no-wrap">{{ $deleted_ea->deleted_at->format('d/m/Y h:i A') }}</td>
							<td width="1%">
								<form style="display: inline" method="POST" action="{{ action('BillLadingController@recover_bl', Hashids::encode($deleted_ea->id)) }}">
									@csrf
									<button type="button" data-bl-no="{{ $deleted_ea->bl_no }}" class="btn btn-success btn-recover">
										<i class="fas fa-undo"></i>
									</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="tab-pane fade" id="default-tab-2">

		<form id="form_inwbl" method="POST" style="display: inline" action="{{ action('InwardBLController@delete') }}">
			@csrf
			@method('DELETE')
			<div class="row">
				<div class="col-md-10">
					<ul class="nav nav-pills">
						<li class="nav-items">
							<a href="#nav-pills-tab-active-bl" data-toggle="tab" class="nav-link active">
								<span class="d-sm-none">Active BLs</span>
								<span class="d-sm-block d-none">Active BLs</span>
							</a>
						</li>
					</ul>
				</div>
				<div class="col-md-2">
					<div class="clearfix">
						<div class="pull-right">
							<button disabled="" id="btn_delete_inw" type="button" class="btn btn-danger m-b-15"><i class="fa fa-trash m-r-10"></i>Delete</button>
						</div>
					</div>
				</div>
			</div>

			<div class="table-responsive">
				<table id="table_bl_inward" class="table table-bordered table-valign-middle text-center f-s-14 m-b-0">
					<thead class="thead-custom">
						<tr>
							<th width="1%">
								<input type="checkbox" id="chk_inwbl_all">
							</th>
							<th width="1%">BL No.</th>
							<th>Vessel</th>
							<th>Voyage</th>
							<th>POL</th>
							<th>POD</th>
							<th>Shipper</th>
							<th>Consignee</th>
							<th width="1%"></th>
						</tr>
					</thead>
					<tbody>
						@foreach($inward AS $inw)
						<tr>
							<td width="1%"><input type="checkbox" id="chk_inw_{{ $inw->id }}" name="chk_inw[]" value="{{ $inw->id }}"></td>
							<td width="1%">{{ $inw->bl_no }}</td>
							<td>{{ $inw->vessel }}</td>
							<td>{{ $inw->voyage }}</td>
							<td>{{ $inw->pol_name }}</td>
							<td>{{ $inw->fpd_name }}</td>
							<td>{{ $inw->shipper_name }}</td>
							<td>{{ $inw->consignee_name }}</td>
							<td width="1%">
								<div class="dropdown">
									<button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fa fa-cog"></i>
									</button>
									<div class="dropdown-menu dropdown-menu-right f-s-14" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="{{ action('InwardBLController@edit', Hashids::encode($inw->id)) }}">BL Details</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item btn-savepdf" href="{{ action('InwardBLController@noa_pdf', $inw->id ) }}">Generate PDF</a>
										<a class="dropdown-item btn-email" href="{{ action('InwardBLController@email_consignee', $inw->id ) }}">Email to Consignee</a>
									</div>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</form>
	</div>

</div>

<div class="modal fade" id="modal_details">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"></h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<!-- -->
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_print">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Print BL</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form method="POST" autocomplete="off" action="{{ action('BillLadingController@set_format_bl') }}?bl=true">
				@csrf
				<div class="modal-body">
					<table id="table_print" class="table table-borderless text-black m-t-10 f-s-14">
						<!-- <thead>
							<tr>
								<th width="1%">BL No.</th>
								<th>BL Format</th>
							</tr>
						</thead> -->
						<tbody>
							<tr>
								<td width="1%" class="no-wrap"><strong>BL No.</strong></td>
								<td id="td_blNo"></td>
							</tr>
							<tr>
								<td width="1%" class="no-wrap"><strong>BL Format</strong></td>
								<td>
									<select class="form-control form-control-lg" name="txt_blformat">
										@if(env("SITE") == "SHS")
										<option value="apollo">Apollo Agencies (1980)</option>
										<option value="malsuria">Malsuria</option>
										@else
										<option value="malsuria">Malsuria</option>
										<option value="apollo">Apollo Agencies (1980)</option>
										@endif
									</select>
								</td>
							</tr>
							<tr>
								<td width="1%" class="no-wrap"><strong>Company Name</strong></td>
								<td>
									<input type="text" class="txt_companyname form-control form-control-lg m-b-10" name="txt_companyname1" maxlength="100" placeholder="Enter Company Name">
									<input type="text" class="txt_companyname form-control form-control-lg" name="txt_companyname2" maxlength="100" placeholder="Enter Company Name">
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<input type="hidden" name="bl_id">
					<button type="submit" class="btn btn-primary">Proceed</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal" id="modal_email">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<form id="form_email" data-parsley-validate="true" autocomplete="off">
				<div class="modal-header">
					<h4 class="modal-title">Email Consignee</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Email</label>
						<input id="txt_email" type="email" data-parsley-type="email" data-parsley-required="true" class="form-control form-control-lg" placeholder="Enter Email">
					</div>
					<div class="form-group">
						<label>Reference</label>
						<input id="txt_ref" type="text" data-parsley-required="true" class="form-control form-control-lg" placeholder="Enter Reference">
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<input type="hidden" name="bl_id">
					<button id="btn_send_email" type="submit" class="btn btn-primary">Send Email</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_edi">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Upload EDI</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">

				<form id="form_edi" action="/uploadEdi" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="input-group mb-3">
						<input type="file" accept="application/pdf" class="form-control" style="line-height: 1.2" name="file_edi">
						<div class="input-group-append">
							<button id="btn_submit_edi" class="btn btn-primary" type="button">Submit</button>
						</div>
					</div>
					<input hidden="" name="save">

					<table class="f-s-14 table table-bordered table-striped table-valign-middle table-center">
						<thead>
							<tr>
								<th width="1%"><input type="checkbox" id="chk_edi_all"></th>
								<th width="1%" class="no-wrap">BL No.</th>
								<th>Shipper</th>
								<th>Consignee</th>
								<th>Notify Party</th>
								<th width="1%">POL</th>
								<th width="1%">POD</th>
							</tr>
						</thead>
						<tbody id="tbody_edi">

						</tbody>
					</table>
				</form>

			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
				<button id="btn_save_inward" type="submit" style="display: none" class="btn btn-primary">Save Inward BL</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('page_script')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<link href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css" rel="stylesheet" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script src="https://cdn.datatables.net/plug-ins/1.10.16/sorting/datetime-moment.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.1/parsley.js"></script>
<script>
	$(document).ready(function() {

        $.fn.dataTable.moment('DD/MM/YYYY HH:ii A');

        // Ajax Datatable
        const tableOutwards = $("#table_bl_outward").DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            ajax: {
                url: "{{ action('BillLadingController@billLadingDT') }}",
                datatType: "json",
                type: "POST",
                data: function(d){
                    d.filter = filterData()
                }
            },
            columns: [
                {
                    data: 'checkbox'
                },
                {
                    data: 'bl_no'
                },
                {
                    data: 'vessel'
                },
                {
                    data: 'voyage'
                },
                {
                    data: 'pol'
                },
                {
                    data: 'pod'
                },
                {
                    data: 'shipper'
                },
                {
                    data: 'telex_release'
                },
                {
                    data: 'actions'
                }
            ],
            columnDefs: [{
                targets: [0,1,2,3,4,5,6,7,8],
                orderable: false
            }],
            aaSorting: [
                [8, 'desc']
            ],
        });


		$(document).on('click', '.btn-savepdf', function(){
			$.ajax({
				type: "POST",
				url: "/noa_pdf/1",
				success: function(response){
					alert('ok');
				}
			});
		});

		//Open upload EDI modal
		$("#btn-upload-edi").click(function(){
			$("#modal_edi").modal("toggle");
		});

		$(document).on("change", ".chk_edi", function(){
			if($(".chk_edi").length == $(".chk_edi:checked").length){
				$("#chk_edi_all").prop("checked", true);
			} else {
				$("#chk_edi_all").prop("checked", false);
			}

			if($(".chk_edi:checked").length > 0){
				$("#btn_save_inward").removeAttr("disabled");
			} else {
				$("#btn_save_inward").attr("disabled", "");
			}
		});
		$("#chk_edi_all").change(function(){
			if($(this).is(":checked")){
				$(".chk_edi").prop("checked", true).trigger('change');
			} else {
				$(".chk_edi").prop("checked", false).trigger('change');
			}
		});

		$("#btn_submit_edi").click(function(){
			$(".loader:last").css("z-index", 99999).fadeIn(100);
			var formData = new FormData($("#form_edi")[0]);

			$.ajax({
				type: "POST",
				url: "/uploadEdi",
				data: formData,
				contentType: false,
				processData: false,
				success: function(data){
					$("#tbody_edi").html("");
					if(data.success){
						var bl_Arr = data.data;
						var html = "";

						$.each(bl_Arr, function(key, bl) {
							html += "<tr>" +
							"<td><input type='checkbox' class='chk_edi' name='chk_edi[]' value='" + bl.bl_no + "' checked=''></td>" +
							"<td>" + bl.bl_no + "</td>" +
							"<td>" + bl.shipper_name + "</td>" +
							"<td>" + bl.consignee_name + "</td>" +
							"<td>" + bl.notify_name + "</td>" +
							"<td >" + bl.pol_name + "</td>" +
							"<td >" + bl.pod_name + "</td>" +
							"</tr>";
						});

						$("#tbody_edi").html(html);
						$("#btn_save_inward").show();
					} else {
						swal("Oops!", "Unable to process the uploaded EDI file. <br>Please check if the file uploaded is correct.", "warning");
						$("#btn_save_inward").hide();
					}
					$(".loader:last").fadeOut(100);
				},
				error: function(data){
					swal("Oops!", "Unable to process the uploaded EDI file. <br>Please check if the file uploaded is correct.", "warning");
					$(".loader:last").fadeOut(100);
				}
			});
		});

		$("#btn_save_inward").click(function(){
			$("input[name='save']").val("true");
			$("#form_edi").submit();
		});

		//Hide create outward BL button if inward nav pill selected
		$("a[href='#default-tab-2']").click(function(){
			$("#div_filter").slideUp();
			$("#btn_create_bl, #btn_filter").hide();
			$(".btn-inward").show();

			window.location.hash = "inward"
		});

		if(window.location.hash == "#inward"){
			$("a[href='#default-tab-2']").trigger("click");
		}

		//Show create outward BL button if outward nav pill selected
		$("a[href='#default-tab-1']").click(function(){
			$("#btn_create_bl, #btn_filter").show();
			$(".btn-inward").hide();
			window.location.hash = "";
		});

		@if(session('inward'))
		$("a[href='#default-tab-2']").trigger("click");
		@endif

		$("#btn_filter").click(function(){
			$("#div_filter").slideToggle();
			$(".ddl").select2();
		});

		$("#btn_delete_inw").click(function(){
			console.log($("[id='^chk_inw_']:checked"));
			if($("[id^='chk_inw_']:checked").length > 0){

				var checkedBL = [];

				$("[id^='chk_inw_']:checked").each(function(){
					checkedBL.push($(this).closest("tr").find("td:nth-child(2)").text());
				});

				swal({
					title: "Are you sure?",
					html: "You are about to deleted the selected Inward BL: <br><strong>" + checkedBL.join(", ") + "</strong>",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'Continue',
					cancelButtonText: 'Cancel',
					reverseButtons: true
				}).then((result) => {
					if (result.value) {
						$("#form_inwbl").submit();
					// $("#txt_bl_id").val(JSON.stringify(bl_Id));
					// $("#form_delete").submit();
				}
			});

			}
		});

		$(document).on("change", "[id^='chk_inw_'], #chk_inwbl_all", function(){
			if($("[id^='chk_inw_']").length == $("[id^='chk_inw_']:checked").length){
				$("#chk_inwbl_all").prop("checked", true);
			} else {
				$("#chk_inwbl_all").prop("checked", false);
			}

			if($("[id^='chk_inw_']:checked").length > 0){
				$("#btn_delete_inw").removeAttr("disabled");
			} else {
				$("#btn_delete_inw").attr("disabled", "");
			}
		});

		$("#table_bl_inward").DataTable({
			"ordering": false,
		});

		$("#chk_inwbl_all").change(function(){
			if($(this).is(":checked")){
				$("[id^='chk_inw_']").prop("checked", true);
			} else {
				$("[id^='chk_inw_']").prop("checked", false);
			}
		});

		var url = "";

		$(document).on("click", ".btn-email", function(e){
			e.preventDefault();
			url = $(this).attr("href");
			$("#txt_email, #txt_ref").val("");
			$("#btn_send_email").prop("disabled", false);
			$("#form_email").parsley().reset();
			$("#modal_email").modal("toggle");
			$("#txt_email").focus();
		});

		$("#form_email").on('submit', function(e){
			e.preventDefault();
			var form = $(this);

			form.parsley().validate();

			if(form.parsley().isValid()){
				$("#btn_send_email").prop("disabled", "disabled");

				var email_str = $("#txt_email").val();
				var ref_str = $("#txt_ref").val();

				var promise = $.ajax({
					type: "POST",
					url: url,
					data: {
						email: email_str,
						ref: ref_str
					}
				});

				promise.done(function(data){
					console.log(data);
					$("#modal_email").modal("toggle");
					if(data.success){
						swal("Email sent!", "Email has been sent to " + email_str, "success");
					} else {
						swal("Oops!", "Email was not sent out!" , "error");
					}
				});

				promise.fail(function(data){
					console.log(data);
					swal("Oops!", "Email was not sent out!" , "error");
				});
			}
		});

		$.fn.dataTable.moment('DD/MM/YYYY hh:mm A');
		$("#table_bl_outward_deleted").DataTable({
			"columnDefs": [
			{ "orderable": false, "targets": [0,1,2,3,4,5,7] }
			],
			"order": [[ 6, "desc" ]],
		});

		// $("#table_bl_outward").DataTable({
		// 	"ordering": false
		// });

		$(document).on("change", ".chk_bl", function(){
			if($(".chk_bl").length == $(".chk_bl:checked").length){
				$("#chk_all").prop("checked", true);
			} else {
				$("#chk_all").prop("checked", false);
			}

			if($(".chk_bl:checked").length > 0){
				$("#btn_batchdelete").removeAttr("disabled");
			} else {
				$("#btn_batchdelete").attr("disabled", "");
			}
		});

		$("#btn_batchdelete").click(function(){
			var bl_Arr = [];
			var bl_Id = [];

			$(".chk_bl:checked").each(function(elem, index){
				bl_Arr.push($(this).closest("tr").find("td:nth-child(2)").text());
				bl_Id.push($(this).val());
			});

			swal({
				title: "Are you sure?",
				html: "You are about to deleted the selected BL: <br><strong>" + bl_Arr.join(", ") + "</strong>",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Continue',
				cancelButtonText: 'Cancel',
				focusCancel: true
			}).then((result) => {
				if (result.value) {
					$("#txt_bl_id").val(JSON.stringify(bl_Id));
					$("#form_delete").submit();
				}
			});
		});

		$("#chk_all").change(function(){
			if($(this).is(":checked")){
				$(".chk_bl").prop("checked", true).trigger('change');
			} else {
				$(".chk_bl").prop("checked", false);
			}
		});

		$(document).on("click", ".btn-print", function(){
			var bl_no = $(this).attr("data-bl-no");
			var bl_id = $(this).attr("data-bl-id");
			$("#td_blNo").text(bl_no);
			$(".txt_companyname").val("");
			$("input[name='bl_id']").val(bl_id);
			// addBLPrint(bl_no, bl_id);
			$("#modal_print").modal("toggle");
		});

		function addBLPrint(bl_no, bl_id){
			// $("#row_clone").nextAll().remove();
			// var $row = $("#row_clone").clone();
			// $row.show().find("td:first").text(bl_no);
			// $("#td_blNo").text(bl_no);
			// $row.find("select").attr("name", "format_id[]");
			// $row.append("<input type='hidden' name='bl_id' value='" + bl_id + "'>");
			// $("#row_clone").after($row);
		}

		$('[data-toggle="tooltip"]').tooltip();

		$('#vessel_select').change(function() {
			if($(this).val() != "ALL"){
				$('#voyage_select').html('<option selected value>-- select an option --</option>');
				$('#voyage_select').prop('disabled', true);

				if($(this).val() != 0) {
					// $('.loader').fadeIn(200);

					$.ajax({
						type: "POST",
						data: {id: $(this).val()},
						url: '/bl/voyages/list',
						success: function(data){
							if(data.success == true) {
								var html = '<option selected value=NULL>-- select an option --</option>';

								for(var i = 0; i < data.result.length; i++) {
									html += '<option value="' + data.result[i].id + '" data-scn="' + data.result[i].scn + '">' + data.result[i].voyage_id + '</option>';
								}

								$('#voyage_select').html(html);
								$('#voyage_select').prop('disabled', false);

								loadBL();

							} else {
								// swal({
								// 	title: 'Error',
								// 	html: 'Selected vessel does not seem to have any voyages.<br>Create a voyage now for selected vessel?',
								// 	type: 'error',
								// 	showCancelButton: true,
								// 	confirmButtonText: 'Create Voyage',
								// 	reverseButtons: true
								// }).then((result) => {
								// 	if(result.value) {
								// 		window.location.href = '{{ action('VoyageController@create') }}';
								// 	}
								// });
							}

							// $('.loader').fadeOut(200);
						}
					});
				}
			}
		});

		$('#voyage_select, #vessel_select').change(function() {
			if(this.id == "vessel_select" && $(this).val() == "ALL"){
				$("#voyage_select").attr("disabled", "");
				// loadBL();
			} else {
				var scn = $('#voyage_select').find('option:selected').attr('data-scn');
				if(scn == "null"){
					scn = "-";
				}

				$('#scn').val(scn);

				if($(this).val() != 0) {

					loadBL();
				}
			}
		});

		$('#voyage_info').click(function() {
			if($('#voyage_select').val() != 'NULL'){
				var index = $('#voyage_select').val();
				console.log(index);
				$('.loader').fadeIn(200);

				$.ajax({
					type: 'POST',
					url: '/voyage',
					data: {'id' : index},
					success: function(data) {
						$('.loader').fadeOut(200);

						if(data.result) {
							var html = '<div class="row"><div class="col-lg-6"><table class="table table-bordered table-condensed"><tbody><tr class="row-details">' +
							'<th>Voyage ID</th><td>' + data.voyage.voyage_id + '</td></tr><tr>' +
							'<th>Vessel Name</th><td>' + data.voyage.vessel_name + '</td></tr><tr>' +
							'<th>Vessel SCN</th><td>' + data.voyage.vessel_scn + '</td></tr></tbody></table></div>' +
							'<div class="col-lg-6"><table class="table table-bordered table-condensed"><tbody><tr class="row-details">' +
							'<th>Booking Status</th><td>' + data.voyage.booking_status + '</td></tr><tr>' +
							'<th>Closing Date</th><td>' + data.voyage.closing_date + '</td></tr></tbody></table></div>' +
							'<div class="col-lg-12"><table class="table table-bordered table-condensed"><tbody><tr>' +
							'<th width="1%">#</th><th>Port(s) Calling</th><th>ETA</th><th>ATA</th></tr>';

							for(var i = 0; i < data.voyage.ports.length; i++) {
								html +=	'<tr><td>' + (i + 1) + '</td>';

								if(data.voyage.ports[i].port_code == data.voyage.pol_code) {
									html += '<td>' + data.voyage.ports[i].port_code + '<span class="badge badge-green m-l-10">POL</span></td>';
								} else {
									html += '<td>' + data.voyage.ports[i].port_code + '</td>';
								}

								html += '<td>' + data.voyage.ports[i].port_eta + '</td>' +
								'<td>' + data.voyage.ports[i].port_ata + '</td>' +
								'</tr>';
							}

							html += '</tbody></table></div></div></div>';

							$('#modal_details .modal-header h4').html('Voyage Details - ' + $('#voyage_select option:selected').text());
							$('#modal_details .modal-body').html(html);
							$('#modal_details').modal();
						} else {
							swal({
								title: "An error occured!",
								text: "Please try again.",
								type: "error",
								confirmButtonText: "OK"
							});
						}
					},
					error: function(data) {
						$('.loader').fadeOut(200);

						swal({
							title: "An error occured!",
							text: "Please try again.",
							type: "error",
							confirmButtonText: "OK"
						});
					}
				});
			} else {
				swal({
					title: 'No selected voyage!',
					text: 'Have you selected a voyage yet?',
					type: 'info',
					confirmButtonText: 'OK'
				});
			}
		});

		$('#vessel_info').click(function() {
			if($('#vessel_select').val() != ""){
				var index = $('#vessel_select').val();
				console.log(index);
				$('.loader').fadeIn(200);

				$.ajax({
					type: 'POST',
					url: '/bl/vessel/details',
					data: {'id' : index},
					success: function(data) {
						$('.loader').fadeOut(200);
						console.log(data.vessel);

						if(data.result) {
							var html = '<table class="table table-bordered table-condensed">' +
							'<tbody>' +
							'<tr>' +
							'<th>Vessel Name</th><td>' + data.vessel.name + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>Vessel Type</th><td>' + data.vessel.type + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>Vessel DWT</th><td>' + data.vessel.dwt + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>Semicontainer Service</th><td>' + data.vessel.semicontainer + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>Manufactured Year</th><td>' + data.vessel.manu_year + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>Flag</th><td><img class="flag-img" src="/img/flags/' + data.vessel.flag.replace(/ /g, '-') + '.png" />' + data.vessel.flag + '</td>' +
							'</tr>' +
							'<tr>' +
							'<th>Default Shipment</th><td>' + data.vessel.default_shipment + '</td>' +
							'</tr>' +
							'</tbody></table>';

							$('#modal_details .modal-header h4').html('Vessel Details - ' + $('#vessel_select option:selected').text());
							$('#modal_details .modal-body').html(html);
							$('#modal_details').modal();
						} else {
							swal({
								title: "An error occured!",
								text: "Please try again.",
								type: "error",
								confirmButtonText: "OK"
							});
						}
					},
					error: function(data) {
						$('.loader').fadeOut(200);

						swal({
							title: "An error occured!",
							text: "Please try again.",
							type: "error",
							confirmButtonText: "OK"
						});
					}
				});
			} else {
				swal({
					title: 'No selected vessel!',
					text: 'Have you selected a vessel yet?',
					type: 'info',
					confirmButtonText: 'OK'
				});
			}
		});

		$('#btn_create_bl').click(function() {
			// if($('#vessel_select').val() == "NULL" && $('#voyage_select').val() == "NULL" || $("#vessel_select").val() == "ALL") {
			// 	swal({
			// 		title: 'No voyage/vessel selected!',
			// 		text: 'You need to select a voyage or vessel to create a BL',
			// 		type: 'warning',
			// 		confirmButtonText: 'OK'
			// 	});
			// } else if($('#vessel_select').val() != "NULL" && $('#voyage_select').val() == "NULL"){
			// 	var id = $('#vessel_select').val();

			// 	window.location.href = "{{ action('BillLadingController@create') }}?vessel=" + id;
			// } else if($('#vessel_select').val() == "NULL" && $('#voyage_select').val() != "NULL"){
			// 	var id = $('#voyage_select').val();

			// 	window.location.href = "{{ action('BillLadingController@create') }}?voyage=" + id;
			// } else if($('#vessel_select').val() != "NULL" && $('#voyage_select').val() != "NULL"){
			// 	var voyage_id = $('#voyage_select').val();
			// 	var vessel_id = $('#vessel_select').val();

			// 	window.location.href = "{{ action('BillLadingController@create') }}?voyage=" + voyage_id + "&vessel=" + vessel_id;
			// }

			var url = "{{ action('BillLadingController@create') }}";

			var vessel_id = $("#vessel_select").val();
			var voyage_id = $("#voyage_select").val();
			var first_query = false;

			if(vessel_id != "ALL"){
				url += "?vessel=" + vessel_id;
				first_query = true;
			}

			if(voyage_id != "NULL"){
				if(first_query){
					url += "&voyage=" + voyage_id;
				} else {
					url += "?voyage=" + voyage_id;
				}
			}

			window.location.href = url;
		});

		$(document).on('click', '.btn-part', function() {
			var blID = $(this).attr('data-id');

			$.ajax({
				type: 'POST',
				url: '/bl/part',
				data: {'id' : blID},
				success: function(data) {
					if(data.success) {
						swal({
							title: "Success",
							html: "Successfully parted BL.<br>The page will now refresh.",
							type: "success",
							confirmButtonText: "OK"
						}).then((result) => {
							if(result.value) {
								document.location.reload();
							}
						});
					} else {
						swal({
							title: "Error",
							text: "Please try again.",
							type: "error",
							confirmButtonText: "OK"
						});
					}

					$('.loader').fadeOut(200);
				},
				error: function(data) {
					console.log("Not Successful. " + data);

					swal({
						title: "Error",
						html: "Something went wrong!<br>Please try again.",
						type: "error",
						confirmButtonText: "OK"
					});

					$('.loader').fadeOut(200);
				}
			});
		});

		// $(document).on('click', '.btn-charges', function() {
		// 	swal({
		// 		// title: "Are you sure?",
		// 		html: "<b>BL Charges</b> temporarily unavailable. Please stand by.<br>Squashing some bugs in the system.",
		// 		type: "warning",
		// 		confirmButtonText: "OK"
		// 	});
		// });

		$(document).on("click", ".btn-recover", function() {
			var bl_no = $(this).attr("data-bl-no");
			var form = $(this).closest("form");
			swal({
				title: "Recover " + bl_no + "?",
				html: "Press Proceed to recover selected BL",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Proceed',
				cancelButtonText: 'Cancel',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					form.submit();
				}
			});
		});

		// $(document).on('click', '.btn-copy', function() {
		// 	swal({
		// 		// title: "Are you sure?",
		// 		html: "<b>BL Copy</b> temporarily unavailable. Please stand by.<br>Squashing some bugs in the system.",
		// 		type: "warning",
		// 		confirmButtonText: "OK"
		// 	});
		// });

		$(".filter").change(function(){
			loadBL();
		});

        function filterData()
        {
            return {
                _token: "{{ csrf_token() }}",
                vessel_id: $('#vessel_select').val(),
                voyage_id: $('#voyage_select').val(),
                pol_id: $("#pol_select").val(),
				pod_id: $("#pod_select").val(),
                shipper_id: $("#shipper_select").val()
            }
        }

		function loadBL(){

            tableOutwards.ajax.reload();

            return false;

			$.ajax({
				type: "POST",
				data: {
					voyage_id: $('#voyage_select').val(),
					vessel_id: $('#vessel_select').val(),
					pol_id: $("#pol_select").val(),
					pod_id: $("#pod_select").val(),
					shipper_id: $("#shipper_select").val()
				},
				url: '/bl/list',
				success: function(data){
					if(data.success == true) {
						var html = '';

						console.log(data.result.length);
						for(var i = 0; i < data.result.length; i++) {
							html += '<tr>' +
							'<td><input type="checkbox" class="chk_bl" name="chk_bl[]" value="' + data.result[i].id + '"></td>' +
							'<td>' + data.result[i].bl_no + '</td>' +
							'<td>' + data.result[i].vessel_name + '</td> ' +
							'<td>' + data.result[i].voyage_name + '</td> ' +
							'<td>' + data.result[i].pol.code + '</td>' +
							'<td>' + data.result[i].pod.code + '</td>' +
							'<td>' + data.result[i].shipper_name + '</td>' +
							'<td>' + data.result[i].status + '</td>' +
							'<td>' +
							'<div class="dropdown">' +
							'<button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
							'<i class="fa fa-cog"></i>' +
							'</button>' +
							'<div class="dropdown-menu dropdown-menu-right f-s-14" aria-labelledby="dropdownMenuButton">' +
							'<a class="dropdown-item" href="/bl/edit/' + data.result[i].hashID + '" >BL Details</a>' +
							'<a class="dropdown-item" href="/bl/' + data.result[i].hashID + '/cargo" class="btn-cargo" data-id="' + data.result[i].id + '">Cargo</a>' +
							'<a class="dropdown-item" href="/bl/' + data.result[i].hashID + '/charges" class="btn-charges" data-id="' + data.result[i].id + '">Charges</a>' +
							'<div class="dropdown-divider"></div>' +
							'<a class="dropdown-item btn-part" href="javascript:;" data-id="' + data.result[i].id + '"><i class="fas fa-clone m-r-10"></i>Part BL</a>' +
							'<a class="dropdown-item btn-copy" href="{{ action('BillLadingController@create') }}?bl=' + data.result[i].bl_no + '"><i class="fas fa-copy m-r-10"></i>Copy BL</a>' +
							'<div class="dropdown-divider"></div>' +
							'<a class="dropdown-item btn-print-so" href="/bl/' + data.result[i].hashID + '/print-so"><i class="fas fa-print m-r-10"></i>Print Shipping Order</a>' +
							'<a class="dropdown-item btn-print" data-bl-id="' + data.result[i].id + '" data-bl-no="' + data.result[i].bl_no + '" href="javascript:;"><i class="fas fa-print m-r-10"></i>Print BL</a>' +
							'</div>' +
							'</div>' +
							'</td>' +
							'</tr>'
						}

						html += '</tr>';

						$("#table_bl_outward").DataTable().destroy();
						$('#table_bl_outward').find('tbody').html(html);
						$("#table_bl_outward").DataTable({
							"ordering": false
						});
					} else {
						$('#table_bl_outward').find('tbody').html('<tr><td colspan="9" class="text-center">No BL to display.</td></tr>');
					}

					$('.loader').fadeOut(200);
				}
			});
		}

		$( window ).resize(function() {
			$(".ddl").select2();
		});
	});
</script>
@stop
