@extends("layouts.nav")
@section('page_title', 'Cargo Details')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item">
		<a href="{{ action('BillLadingController@create_cargo', $hash) }}"><button id="btn-create-bl" type="button" class="btn btn-primary">Add Cargo</button></a>
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">
	Cargo Details 
	<span class="f-s-18">
		{{ $bl->bl_no }}
	</span> 
</h1>

@if(session('msg'))
    <div class="alert {{ session('class') }}">
    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('msg') }}
    </div>
@endif

@if(session('success'))
    <div class="alert alert-success">
    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('success') }}
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger">
    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('error') }}
    </div>
@endif

<div class="row">
	<div class="col-lg-6">
		<div class="table-responsive">
			<table width="100%" class="table f-s-14 m-b-0">
				<thead class="thead-custom">
					<th>#</th>
					<th>Cargo Description</th>
					<th></th>
				</thead>
			</table>
		</div>
		<div class="table-responsive cargo-table m-b-20">
			<table id="cargo_table" width="100%" class="table table-hover f-s-14 m-b-0">
				<tbody class="bg-white">
					@if(count($cargos) > 0)
	    			@foreach($cargos as $cargo)
					<tr class="{{ ($loop->iteration == 1) ? 'hover' : '' }}" data-index="{{ $loop->index }}" data-cargo="{{ $cargo }}" data-container="{{ $cargo->containers }}" >
						<td>{{ $loop->iteration }}</td>
						<td>{{ $cargo->cargo_name }} {{ $cargo->cargo_desc }}</td>
						<td width="155px">
							<a href="{{ action('BillLadingController@edit_cargo', [$hash, Hashids::encode($cargo->id)]) }}" class="btn btn-success btn-fw btn-xs">Edit</a>
							<form method="POST" class="d-inline">
								@method('DELETE')
								@csrf
								<a href="javascript:;" class="btn btn-danger btn-fw btn-xs btn-delete" data-id="{{ $cargo->id }}">Delete</a>
							</form>
						</td>
					</tr>
					@endforeach
					@else
					<tr><td colspan="2" class="text-center">No descriptions to display.</td></tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="table-responsive">
			<table width="100%" class="table f-s-14 m-b-0">
				<thead class="thead-custom">
					<th>Cgo</th>
					<th>Container No.</th>
					<th>Seal No.</th>
					<th>Type</th>
					<th>Size</th>
				</thead>
			</table>
		</div>
		<div class="table-responsive cargo-table m-b-20">
			<table id="container_table" width="100%" class="table table-bordered f-s-14 m-b-0">
				<tbody class="bg-white">
					@if($cargos->isNotEmpty())
						@if(count($cargos[0]->containers) > 0)
							@foreach($cargos[0]->containers as $container)
							<tr>
								<td>{{ $container->cgo }}</td>
								<td>{{ $container->container_no }}</td>
								<td>{{ $container->seal_no }}</td>
								<td>{{ $container->type }}</td>
								<td>{{ $container->size }}</td>
							</tr>
							@endforeach
						@else
							<tr><td colspan="5" class="text-center">No details to display.</td></tr>
						@endif
					@else
						<tr><td colspan="5" class="text-center">No details to display.</td></tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="panel">
	<div class="panel-body">
		<div class="row">
			<div class="col-lg-3">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group m-b-16">
							<label>Markings</label>
							<div id="markings" class="editablecontent" >
								{!! ($f_cargo) ? nl2br($f_cargo->markings) : '' !!}
							</div>
							<!-- <textarea id="markings" class="form-control form-control-lg" rows="10" readonly >{{ ($f_cargo) ? $f_cargo->markings : '' }}</textarea> -->
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group m-b-20">
							<label>Temperature</label>
							<input id="temperature" type="text" class="form-control form-control-lg" value="{{ ($f_cargo) ? $f_cargo->temperature : '' }}" readonly />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group m-b-20">
							<label>Cargo Nature</label>
							<input id="cargo_nature" type="text" class="form-control form-control-lg" value="{{ ($f_cargo) ? $f_cargo->cargo_nature : '' }}" readonly />
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group m-b-20">
							<label>UN Code / Class</label>
							<div id="uncode" class="editablecontent" >
								{!! ($f_cargo) ? nl2br($f_cargo->uncode) : '' !!}
							</div>

							<!-- <textarea id="uncode" class="form-control form-control-lg" rows="5" readonly >{{ ($f_cargo) ? $f_cargo->uncode : '' }}</textarea> -->
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group m-b-20">
							<label>Cargo Description</label>
							<input id="cargo_desc" type="text" class="form-control form-control-lg" value="{{ ($f_cargo) ? $f_cargo->cargo_desc : '' }}" readonly />
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form-group m-b-20">
							<label >Cargo Type</label>
							<input id="cargo_type" type="text" class="form-control form-control-lg" value="{{ ($f_cargo) ? $f_cargo->cargo_type : '' }}" readonly />
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form-group m-b-20">
							<label class="pull-right">Weight</label>
							<input id="weight" type="number" class="form-control form-control-lg" value="{{ ($f_cargo) ? $f_cargo->weight : '' }}" readonly />
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form-group m-b-20">
							<label class="pull-right">KG/MT</label>
							<input id="weight_unit" type="text" class="form-control form-control-lg" value="{{ ($f_cargo) ? $f_cargo->weight_unit : '' }}" readonly />
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form-group m-b-20">
							<label class="pull-right">Volume M3</label>
							<input id="volume" type="number" class="form-control form-control-lg" value="{{ ($f_cargo) ? $f_cargo->volume : '' }}" readonly />
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group m-b-20">
							<div id="detailed_desc" class="editablecontent" >{!! ($f_cargo) ? nl2br($f_cargo->detailed_desc) : '' !!}
							</div>
						</div>
					</div>
					<!-- <div class="col-lg-12">
						<div class="form-group m-b-20">
							<label>Remarks</label>
							<textarea id="remarks" class="form-control form-control-lg" rows="5" readonly >{{ ($f_cargo) ? $f_cargo->remarks : '' }}</textarea>
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <button type="button" id="btn_back" class="btn btn-success btn-action m-r-10">Back to BL</button> -->
<a href="{{ action('BillLadingController@edit', $bl->hash) }}"><button class="btn btn-success btn-action m-r-10">Return to BL Details</button></a>
<a href="{{ action('BillLadingController@charges', $hash) }}?step=charges"><button type="button" class="btn btn-success btn-action pull-right" data-step="charges">Proceed to Charges</button></a>

@stop

@section('page_script')
<script>
	$(document).ready(function() {
		$('#cargo_table tr').click(function() {
			$('#cargo_table tr').removeClass('hover');
			$(this).addClass('hover');

			var index = $(this).attr('data-index');
			var cargo = JSON.parse($(this).attr('data-cargo'));
			var container = JSON.parse($(this).attr('data-container'));

			$('#cargo_name').val(cargo.cargo_name);
			$('#markings').html(cargo.markings);
			$('#cargo_nature').val(cargo.cargo_nature);
			$('#uncode').html(cargo.uncode);
			$('#cargo_desc').val(cargo.cargo_desc);
			$('#cargo_type').val(cargo.cargo_type);
			$('#weight').val(cargo.weight);
			$('#weight_unit').val(cargo.weight_unit);
			$('#volume').val(cargo.volume);
			$('#temperature').val(cargo.temperature);
			$('#detailed_desc').html(cargo.detailed_desc);
			$('#remarks').val(cargo.remarks);

			if(container.length > 0) {
				html = '';
				for(var i = 0; i < container.length; i++) {
					html += '<tr>' +
						'<td>' + container[i].cgo + '</td>' +
						'<td>' + container[i].container_no + '</td>' +
						'<td>' + container[i].seal_no + '</td>' +
						'<td>' + container[i].type + '</td>' +
						'<td>' + container[i].size + '</td>' +
						'</tr>';
				}

				$('#container_table tbody').html(html);
			} else {
				$('#container_table tbody').html('<tr><td colspan="5" class="text-center">No details to display.</td></tr>');
			}
			
		});
	});

	// $('#btn_back').click(function() {
	// 	window.location.replace('{{ action('BillLadingController@index') }}');
	// });

	$(document).on('click', '.btn-delete', function() {
		swal({
			title: 'Warning',
			text: 'Are you sure? This can\'t be undone.',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Confirm',
			reverseButtons: true
		}).then((result) => {
			if(result.value) {
				var url = "/bl/cargo/delete/" + $(this).attr("data-id");
				$(this).closest("form").attr("action", url).submit();
			}
		});
	});
</script>
@stop