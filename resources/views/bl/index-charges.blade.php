@extends("layouts.nav")
@section('page_title', 'BL Charges')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('BillLadingController@index') }}">Bill of Lading</a></li>
	<li class="breadcrumb-item active">Charges Details</li>
</ol>
@stop

@section('content')
<h1 class="page-header">
	Charges Details
	<span class="f-s-18">
		{{ $bl->bl_no }}
	</span>
</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<div class="panel panel-inverse">
	<div class="panel-body p-20">
		<table id="charge_table" class="table table-bordered table-condensed table-valign-middle f-s-14">
			<thead class="thead-custom">
				<th>Charge Code</th>
				<th>Unit</th>
				<th>Cargo</th>
				<th width="1%" class="no-wrap">Amount (RM)</th>
				<th width="1%">Payment</th>
				<th></th>
			</thead>
			<tbody class="bg-white">
				@foreach($bill_charges AS $c)
				<tr data-id="{{ $c->id }}">
					<td>{{ $c->getCharge->code }}</td>
					<td>{{ $c->charge_unit }}</td>
					<td>
						@if(count($c->getChargeCargo) == 0 || count($c->getChargeCargo) == count($bl->getCargos))
						All
						@else
						@foreach($c->getChargeCargo AS $cargo)
						{{ $cargo->getCargo->cargo_name . ' ' . $cargo->getCargo->cargo_desc }}
						@endforeach
						@endif
					</td>
					<td>{{ $c->charge_rate }}</td>
					<td>{{ $c->charge_payment }}</td>
					<td width="1%">
					 	<button type="button" class="btn btn-success btn-edit" style="width:100%;margin:2px;">
							Edit
						</button>
						<button type="button" data-billchargeid="{{ $c->id }}" class="btn btn-danger btn-delete-charge" style="width:100%;margin: 2px;">
							Delete
						</button>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<form id="form_charge" method="POST" action="{{ action('BillLadingController@store_charges', $hash) }}" autocomplete="off">
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body p-20">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Charge Code</label>
						<select id="ddl_chargecode" class="ddl form-control form-control-lg" name="ddl_chargecode">
							<option disabled="" selected="" value="null">-- select an option --</option>
							@foreach($cgs AS $charge)
							<option value="{{ $charge->id }}" data-code="{{ $charge->code }}">{{ $charge->code }} - {{ $charge->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>Payment</label>
						<select id="ddl_payment" class="form-control form-control-lg" name="ddl_payment">
							<option value="P">Prepaid</option>
							<option value="C">Collect</option>
						</select>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-2 col-6">
					<div class="form-group m-b-0">
						<label>Amount</label>
					</div>
				</div>
				<div class="col-md-2 col-6">
					<div class="form-group m-b-0">
						<label>Type</label>
					</div>
				</div>
				<div class="col-md-2 col-6">
					<div class="form-group m-b-0">
						<label>Cargo</label>
					</div>
				</div>
			</div>
			<div class="row div-charges">
				<div class="col-md-2">
					<div class="form-group">
						<input id="inp_amt" type="number" class="form-control form-control-lg m-b-5 inp_amt" name="txt_amount[]" placeholder="Enter Amount">
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<select id="ddl_unit" class="form-control form-control-lg ddl_unit" name="ddl_unit[]">
							<option disabled="" selected="">None</option>
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<select multiple="multiple" class="form-control form-control-lg ddl_cargo" name="ddl_cargo[]">
							@foreach($bl->getCargos AS $cargo)
							<option value="{{ $cargo->id }}">{{ $cargo->cargo_name . ' ' . $cargo->cargo_desc }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class='col-md-1 d-flex' style="align-items: center">
					<div class='form-group'>
						<label class='lbl-remove' style="display: none">REMOVE</label>
					</div>
				</div>
			</div>
			<div class="row">
				<button type="button" id="btn_add_charges" class="btn btn-primary m-l-10">Add Charges</button>
			</div>
		</div>
	</div>

	<input hidden id="txt_charge" name="txt_charge">
	<button type="button" id="btn_cancel" class="btn btn-danger btn-action m-r-10">Back to Cargo Details</button>
	<button id="btn_save" type="button" class="btn btn-primary btn-action">Save</button>
</form>

<div class="modal fade" id="modal-edit">
	<div class="modal-dialog" style="max-width: 1000px">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Charge</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form autocomplete="off" id="edit_charge_form" action="{{ action('BillLadingController@edit_charges', $hash) }}" method="POST">
				@csrf
				<div class="modal-body">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>Charge Code</label>
								<input type="text" readonly="true" id="txt_chargecode" class="form-control form-control-lg" disabled>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Type</label>
								<input type="text" readonly="true" id="txt_chargetype" class="form-control form-control-lg" disabled>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Amount</label>
								<input type="number" class="form-control form-control-lg inp_amt" id="txt_edit_amount" name="txt_edit_amount" placeholder="Enter Amount">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Payment</label>
								<select id="ddl_edit_payment" class="form-control form-control-lg" name="charge_payment" disabled>
									<option value="P">Prepaid</option>
									<option value="C">Collect</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="edit_charge_id" type="hidden" name="edit_charge_id" />
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="button" id="btn_edit_save" class="btn btn-primary">Save Changes</button>
				</div>
			</form>
		</div>
	</div>
</div>

<form id="charge_form" action="{{ action('BillLadingController@store_charges', $hash) }}" method="POST">
	@csrf
	<input id="charge_id" type="hidden" name="charge_id" />
	<input id="charge_code" type="hidden" name="charge_code" />
	<input id="charge_unit" type="hidden" name="charge_unit" />
	<input id="charge_rate" type="hidden" name="charge_rate" />
	<input id="charge_payment" type="hidden" name="charge_payment" />
	<input id="charge_count" type="hidden" name="count" />
	{{-- <input name="bl_no" type="hidden" class="form-control form-control-lg" value="{{ $bl_no }}" /> --}}

</form>
@stop

@section('page_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
<link href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css" rel="stylesheet" />

<link rel="stylesheet" href="/css/BsMultiSelect.min.css">
<script type="text/javascript" src="/js/BsMultiSelect.min.js"></script>

<script type="text/javascript">
	$(function(){

		$(".ddl_cargo").bsMultiSelect({
			useCss: true
		});

		$(".ddl").select2();

		$("#charge_table").DataTable({
			"columnDefs": [
			{ "orderable": false, "targets": [1,2,3,4] }
			]
		});

		$(document).on("blur", ".inp_amt", function(){
			var value = $(this).val();
			if(!value.match(/^[0-9]\d*(\.)?\d{0,2}$/)){
				$(this).val("");
			}
		});

		$(".btn-delete-charge").click(function(){
			swal({
				title: 'Are you sure?',
				html: 'Any <b>unsaved</b> progress will be lost.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Proceed',
				reverseButtons: true
			}).then((result) => {
				if(result.value) {
					var row = $(this).closest("tr");
					var id = $(this).attr("data-billchargeid");
					$.ajax({
						type: "GET",
						url: "/bl/charges/delete/" + id,
						success: function(response){
							if(response.success){
								swal("Success", "Charges deleted!", "success");
								row.remove();
							}
						}
					})
				}
			});

		});


		$('#btn_cancel').click(function() {
			swal({
				title: 'Are you sure?',
				html: 'Any <b>unsaved</b> progress will be lost.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Proceed',
				reverseButtons: true
			}).then((result) => {
				if(result.value) {
					window.location.replace('{{ action('BillLadingController@cargo', $hash) }}');
				}
			});
		});

		var bl_id = "{{ $bl->id }}";

		$("#ddl_chargecode, #ddl_payment").change(function(){
			var updateSelect = true;
			if(this.id == "ddl_chargecode"){
				updateSelect = false;
			}
			var charge_id = $("#ddl_chargecode").val();
			console.log(charge_id);
			if(charge_id != null){
				var payment = $("#ddl_payment").val();
				var url = "/charge-detail/" + charge_id + "/" + bl_id + "/" + payment;
				$.get(url, function(response){
					var optionArr = "<option disabled='' selected=''>-- select an option --</option>";

					$.each(response, function(name, value) {
						optionArr += "<optgroup label='" + name + "'>";
						value.forEach(function(element, index){
							optionArr += "<option data-default='" + element.default + "' value='" + element.unit_size + "-" + element.unit + "'>" + element.text + "</option>";
						});
						optionArr += "</optgroup>";
					});

					$(".ddl_unit").each(function(){
						// console.log("val here: " + $(this).val());
						var selected = $(this).val();
						$(this).find("option, optgroup").remove();
						$(this).append(optionArr);
						if(selected != null && updateSelect){
							var amt_elem = $(this).closest(".div-charges").find("#inp_amt");
							if(amt_elem.val() == ""){
								$(this).val(selected).trigger("change");
							}
							$(this).val(selected);
						}
					});
				});
			}
		});

		$(document).on("change", ".ddl_unit", function(){
			var amt = $(this).find("option:selected").attr("data-default");
			if(amt != 0){
				$(this).closest(".div-charges").find("#inp_amt").val(amt);
			}
		});

		$("#ddl_chargecode").trigger("change");

		$("#btn_add_charges").click(function() {
			// $('.dataTables_empty').parent().remove();

			// var amt = parseInt($('#inp_amt').val());

			// if(isNaN(amt)) {
			// 	$("#inp_amt").focus();
			// 	// swal({
			// 	// 	title: 'Error',
			// 	// 	html: 'Amount can\'t be empty.',
			// 	// 	type: 'error',
			// 	// 	confirmButtonText: 'OK'
			// 	// });
			// } else {
			// 	var html = '<tr data-id="-1" data-payment="' + $('#ddl_payment').val() + '">' +
			// 	'<td>' + $('#ddl_chargecode').find('option:selected').attr('data-code') + '</td>' +
			// 	'<td>' + $('#ddl_unit').val() + '</td>' +
			// 	'<td>' + amt.toFixed(2) + '</td>';

			// 	if($('#ddl_payment').val() == 'P') {
			// 		html += '<td>' + amt.toFixed(2) + '</td>' +
			// 		'<td>-</td>';
			// 	} else {
			// 		html += '<td>-</td>' +
			// 		'<td>' + amt.toFixed(2) + '</td>';
			// 	}

			// 	html += '<td width="1%" class="no-wrap">' +
			// 	'<div class="d-flex">' +
			// 	'<button type="button" class="btn btn-success btn-sm btn-fw m-r-5 btn-edit">Edit</button>' +
			// 	'<button type="button" class="btn btn-danger btn-sm btn-fw btn-delete">Delete</button>' +
			// 	'</div>' +
			// 	'</td>' +
			// 	"</tr>";

			// 	$('#charge_table tbody').append(html);
			// }

			$clone = $(".div-charges:first").clone();
			$clone.find("input").val("");
			$clone.find(".lbl-remove").show();

			$clone.find(".dashboardcode-bsmultiselect")
			.remove();

			var count = $(".div-charges").length;

			$clone.find(".ddl_cargo")
			.bsMultiSelect({
				useCss: true,
				containerClass: 'dashboardcode-bsmultiselect ms_' + count,
			}).attr("name", "ddl_cargo" + count + "[]");


			$(".div-charges:last").after($clone);
			$clone.find("input").focus();

			if($(".div-charges").length > 1){
				$(".lbl-remove:first").show();
			}
		});

		$(document).on("click", ".lbl-remove", function(){
			$(this).closest(".div-charges").remove();

			if($(".div-charges").length > 1){
				$(".lbl-remove:first").show();
			} else {
				$(".lbl-remove:first").hide();
			}
		});

		$(document).on("keydown", "input[data-type='number']", function(e){
			if(!((e.keyCode > 95 && e.keyCode < 106) || (e.keyCode > 47 && e.keyCode < 58) || e.keyCode == 8 || e.keyCode == 9) ) {
				e.preventDefault();
				return false;
			}
		});

		$('#btn_save').click(function(e) {

			e.preventDefault();

			$(this).prop("disabled", true);

			// var ids = [];
			// var payments = [];
			// var codes = [];
			// var units = [];
			// var rates = [];
			// var count = 0;

			// $('#charge_table tbody tr').each(function() {
			// 	ids.push($(this).attr('data-id'));
			// 	payments.push($(this).attr('data-payment'));
			// 	codes.push($(this).find('td:nth-child(1)').text());
			// 	units.push($(this).find('td:nth-child(2)').text());
			// 	rates.push($(this).find('td:nth-child(3)').text());

			// 	count++;
			// });

			// $('#charge_id').val(ids);
			// $('#charge_code').val(codes);
			// $('#charge_unit').val(units);
			// $('#charge_rate').val(rates);
			// $('#charge_payment').val(payments);
			// $('#charge_count').val(count);

			// $('#charge_form').submit();

			if($("#ddl_chargecode").val() == null){
				$("#ddl_chargecode").select2('open');
				$(this).prop("disabled", false);
				return false;
			}

			var chargeArr = [];
			var valid = true;
			var submitForm = true;
			$(".div-charges").each(function(){
				var obj = {};

				obj.type = $(this).find("select").val();
				obj.price = $(this).find("input").val();
				obj.cargo = $(this).find(".ddl_cargo").val();

				chargeArr.forEach(function(elem){
					if($(elem.cargo).filter(obj.cargo).length == 1){
						swal("Oops!", "Duplicate cargo found.", "warning");
						valid = false;
						return false;
					}

					if(elem.type == obj.type){
						if($(elem.cargo).filter(obj.cargo).length == 1 || (elem.cargo.length == 0 && obj.cargo.length == 0)){
							swal("Oops!", "Duplicate charge type found.", "warning");
							valid = false;
							return false;
						}
					}
				});

				if(obj.price == ""){
					$(this).find("input").focus();
					valid = false;
				}

				if(!valid){
					submitForm = false;
					return false;
				} else {
					chargeArr.push(obj);
				}
			});


			if(submitForm){
				$("#txt_charge").val(JSON.stringify(chargeArr));
				$("#form_charge").submit();
			} else {
				$(this).prop("disabled", false);
			}

		});

		var row;

		$(document).on("click", ".btn-edit", function(){
			row = $(this).closest("tr");
			var charge_code = row.find('td:nth-child(1)').text();
			var charge_type = row.find('td:nth-child(2)').text();
			var price = row.find('td:nth-child(4)').text();
			var charge_payment = row.find('td:nth-child(5)').text();

			$('#edit_charge_id').val(row.attr('data-id'));

			$("#txt_chargecode").val(charge_code);
			$("#txt_chargetype").val(charge_type);

			$("#txt_edit_amount").val(price);
			$("#ddl_edit_payment").val(charge_payment);

			$("#modal-edit").modal("toggle");
		});

		$('#btn_edit_save').click(function() {
			var amt = parseInt($('#txt_edit_amount').val());
			var payment = $('#ddl_edit_payment').val();

			console.log(payment);

			if(isNaN(amt)) {
				swal({
					title: 'Error',
					html: 'Amount can\'t be empty.',
					type: 'error',
					confirmButtonText: 'OK'
				});
			} else {
				if(row.attr('data-id') == -1) {
					row.find('td:nth-child(3)').text(amt.toFixed(2));

					if(payment == 'P') {
						row.find('td:nth-child(4)').text(amt.toFixed(2));
						row.find('td:nth-child(5)').text('-');
					} else {
						row.find('td:nth-child(4)').text('-');
						row.find('td:nth-child(5)').text(amt.toFixed(2));
					}

					$("#modal-edit").modal("toggle");
				} else {
					$('#edit_charge_form').submit();
				}
			}
		});

		$(document).on("click", ".btn-delete", function(e){
			e.preventDefault();

			swal({
				title: 'Are you sure?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Delete',
				reverseButtons: true
			}).then((result) => {
				if(result.value) {
					var link = $(this).attr('href');
					var elem = $(this).parents('tr');

					if(elem.attr('data-id') == -1) {
						elem.remove();
					} else {
						window.location.replace(link);
					}
				}
			});
		});
	});
</script>
@stop
