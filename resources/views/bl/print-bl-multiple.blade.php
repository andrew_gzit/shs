@extends("layouts.nav")
@section('page_title', 'Print BL')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li>
		@if(!empty($from))
		@if($from == "BL")
		<a href="{{ action('BillLadingController@index') }}" class="btn-hidden"><button type="button" id="btn-print" style="display: inline-block" class="btn btn-white m-r-5">Return to BL List</button></a>
		@else
		<a href="{{ action('BillLadingController@edit', $from) }}" class="btn-hidden"><button type="button" id="btn-print" style="display: inline-block" class="btn btn-white m-r-5">Return to BL Details</button></a>
		@endif
		@else
		<a href="{{ action('PrintController@index_bl') }}" class="btn-hidden"><button type="button" id="btn-print" style="display: inline-block" class="btn btn-white m-r-5">Return to Schedule</button></a>
		@endif

		<div class="dropdown" style="display: inline-block;">
			<button class="btn btn-success dropdown-toggle btn-hidden" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-print m-r-5"></i> Print
			</button>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
				<a class="dropdown-item" id="plain_format" href="#">Plain Format</a>
				<a class="dropdown-item" id="form_format" href="#">Form Format</a>
			</div>
		</div>
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Print Bill of Lading</h1>

<ul class="nav nav-tabs">
	@if($merged_filename != null)
	<li class="nav-items">
		<a href="#merged" data-toggle="tab" class="nav-link">
			<span class="d-sm-none">ALL</span>
			<span class="d-sm-block d-none">ALL</span>
		</a>
	</li>
	@endif

    {{--
	@foreach($blArr AS $bill_lading)
	<li class="nav-items">
		<a href="#{{ $bill_lading->bl_no }}" data-toggle="tab" class="nav-link">
			<span class="d-sm-none">Tab 1</span>
			<span class="d-sm-block d-none">{{ $bill_lading->bl_no }}</span>
		</a>
	</li>
    @endforeach
    --}}

    @foreach($podPdfs AS $key => $filename)
	<li class="nav-items">
		<a href="#{{ $key }}" data-toggle="tab" class="nav-link">
			<span class="d-sm-none">Tab 1</span>
			<span class="d-sm-block d-none">{{ $key }}</span>
		</a>
	</li>
	@endforeach
</ul>

<div class="tab-content">
@if($merged_filename != null)
<div class="tab-pane fade" id="merged">
	<object data="/bl_export/{{ $merged_filename }}.pdf" type="application/pdf" style="width: 100%; height: 80vh">
		alt : <a href="data/test.pdf">{{ $merged_filename }}.pdf</a>
	</object>
</div>
@endif

{{--
@foreach($blArr AS $bill_lading)
<div class="tab-pane fade" id="{{ $bill_lading->bl_no }}">
	<object data="{{ $bill_lading->filename }}" type="application/pdf" style="width: 100%; height: 80vh">
		alt : <a href="data/test.pdf">{{ $bill_lading->bl_no }}.pdf</a>
	</object>
</div>
@endforeach
--}}

@foreach($podPdfs AS $key => $filename)
<div class="tab-pane fade" id="{{ $key }}">
	<object data="{{ $filename }}" type="application/pdf" style="width: 100%; height: 80vh">
		alt : <a href="data/test.pdf">{{ $key }}.pdf</a>
	</object>
</div>
@endforeach
</div>


<form id="form_bl" method="POST">
	@csrf
	<input hidden="" name="txt_blformat" value="{{ $bl_format }}">
	<input hidden="" name="bl_id" value="{{ $bl_id }}">
	<input hidden="" name="txt_companyname1" value="{{ $company_name1 }}">
	<input hidden="" name="txt_companyname2" value="{{ $company_name2 }}">
	<input id="format_type" hidden="" name="format_type">
</form>
@stop

@section("page_script")
<!-- <script type="text/javascript" src="/js/print-format.js"></script> -->

<script type="text/javascript">
	$(function(){
		$("#form_format").click(function(){
			$("#format_type").val("form");
			$("#form_bl").submit();
		});

		$("#plain_format").click(function(){
			$("#format_type").val("plain");
			$("#form_bl").submit();
		});

		$(".nav-tabs .nav-items:first").find("a").addClass("active");
		$(".tab-pane:first").addClass("active show");
	});
</script>

<style id="bl_print_css" type="text/css">

</style>
@stop
