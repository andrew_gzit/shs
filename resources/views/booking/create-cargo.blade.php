@extends("layouts.nav")
@section('page_title', "Cargo - $booking->booking_no")

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('BookingController@index') }}">Booking</a></li>
	<li class="breadcrumb-item"><a href="{{ action('BookingController@edit', $hash) }}">{{ $booking->booking_no }}</a></li>
	<li class="breadcrumb-item active">Cargo - {{ $booking->booking_no }}</li>
</ol>
@stop

@section("content")
<h1 class="page-header">Cargo - {{ $booking->booking_no }}</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<form autocomplete="off" method="POST" action="{{ action('BookingController@store_cargo', $booking->booking_no) }}">
	@csrf
	<div class="row">
		<div class="col-lg-12 col-xl-12">
			<div class="panel">
				<div class="panel-body p-20">
					<h4>{{ $booking->shipment_type }} Shipment</h4>
					<hr>
					@if($booking->shipment_type == "FCL")
					<div class="row">
						<div class="col-md-6">
							<table id="qty_table" class="table table-bordered text-center">
								<thead>
									<tr>
										<th></th>
										<th></th>
										<th colspan=7 class="text-center">Container Type</th>
									</tr>
									<tr>
										<th></th>
										<th></th>
										<th>GP</th>
										<th>HC</th>
										<th>RF</th>
										<th>HRF</th>
										<th>FR</th>
										<th>TK</th>
										<th>OT</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th rowspan="3">Size</th>
										<th>20</th>
										<td><input data-type="20'GP" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="20'HC" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="20'RF" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="20'HRF" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="20'FR" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="20'TK" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="20'OT" type="number" class="cargo-qty" value="0" /></td>
									</tr>
									<tr>
										<th>40</th>
										<td><input data-type="40'GP" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="40'HC" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="40'RF" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="40'HRF" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="40'FR" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="40'TK" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="40'OT" type="number" class="cargo-qty" value="0" /></td>
                                    </tr>
                                    <tr>
										<th>45</th>
										<td><input data-type="45'GP" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="45'HC" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="45'RF" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="45'HRF" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="45'FR" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="45'TK" type="number" class="cargo-qty" value="0" /></td>
										<td><input data-type="45'OT" type="number" class="cargo-qty" value="0" /></td>
									</tr>
								</tbody>
							</table>
							<button id="btn_reset" type="button" class="btn btn-success">Reset Quantity</button>
						</div>
						<div class="col-md-6">
							@if($booking->pol_id == $booking->NBCT())
							<div class="form-group">
								<label>Container Yard Cutoff</label>
								<input id="cy_cutoff" type="text" class="form-control form-control-lg" name="cy_cutoff" placeholder="Enter Cutoff Date" value="{{ empty($booking->cy_cutoff) ? '' : $booking->cy_cutoff->format('d/m/Y') }}">
							</div>
							@endif
							<div class="form-group">
								<label>Haulge Request Date</label>
								<input id="haulge_request_date" type="text" class="datetime form-control form-control-lg" name="haulge_request_date" placeholder="Enter Date" value="{{ empty($booking->haulge_request_date) ? '' : $booking->haulge_request_date->format('d/m/Y') }}">
                            </div>
                            <div class="form-group">
								<label>Yard</label>
								<input type="text" class="form-control form-control-lg" name="yard" placeholder="Enter Yard" value="{{ $booking->yard }}">
							</div>
							<div class="form-group">
								<label>Empty Delivery Location</label>
								<textarea class="form-control form-control-lg" name="empty_delivery_location" placeholder="Enter Location" rows="4">{{ $booking->empty_delivery_loc }}</textarea>
								<!-- <input type="text" class="form-control form-control-lg" name="empty_delivery_location" placeholder="Enter Location" value="{{ $booking->empty_delivery_loc }}"> -->
							</div>
						</div>
					</div>
					@else
					<div class="row">
						<div class="col-md-6">
							<div class="form-group m-b-20">
								<label>Quantity</label>
								<input id="cargo_qty" data-type='number' name="cargo_qty" type="number" class="form-control form-control-lg" placeholder="Quantity" min="1" value="{{ $booking->cargo_qty }}" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group m-b-20">
								<label>Packing</label>
								<input id="cargo_packing" name="cargo_packing" type="text" class="form-control form-control-lg" placeholder="Packing" value="{{ $booking->cargo_packing }}" required />
							</div>
						</div>
					</div>
					@endif
					<!-- <hr>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>QTY / PACKING</label>
								<input id="cargo_name" type="text" name="cargo_name" class="form-control form-control-lg" value="{{ ($booking->shipment_type == 'FCL') ? $booking->cargo_packing : $booking->cargo_qty . ' ' . $booking->cargo_packing }}" readonly />
							</div>
						</div>
					</div> -->
					<input id="cargo_name" type="text" hidden name="cargo_name" class="form-control form-control-lg" value="{{ optional($booking)->cargo_packing }}">
				</div>
			</div>
		</div>
		<div class="col-lg-12 col-xl-12">
			<div class="panel">
				<div class="panel-body p-20">
					<h4>Cargo Details</h4>
					<hr>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Cargo Description</label>
								<input type="text" maxlength="70" data-title="Cargo Description" class="required form-control form-control-lg" name="cargo_desc" placeholder="Enter Cargo Description" value="{{ (!empty($booking->cargo_desc)) ? $booking->cargo_desc : '' }}">
							</div>
							@if($booking->shipment_type == "FCL")
							<div class="form-group">
								<label>Temperature</label>
								<input type="number" class="form-control form-control-lg" name="temperature" placeholder="Enter Temperature" value="{{ (!empty($booking->temperature)) ? $booking->temperature : '' }}">
							</div>
							@endif
							@if($booking->shipment_type == "LCL")
							<div class="form-group">
								<label>Weight (MT)</label>
								<input type="number" data-title="Weight (MT)" class="required form-control form-control-lg" name="weight" placeholder="Enter Weight" value="{{ (!empty($booking->weight)) ? $booking->weight : '' }}">
							</div>
							@else
							<input hidden name="weight">
							@endif
							<!-- <div class="form-group">
								<label>Tues</label>
								<input type="text" readonly="" class="form-control form-control-lg" name="weight" placeholder="0" value="{{ (!empty($booking->weight)) ? $booking->weight : '' }}">
							</div> -->
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>UN Code / Class</label>
								<textarea name="uncode" rows="5" class="form-control form-control-lg" placeholder="Enter UN Code / Class">{{ (!empty($booking->uncode)) ? $booking->uncode : '' }}</textarea>
							</div>
							<!-- <div class="form-group">
								<label>Cargo Nature</label>
								<select id="cargo_nature" class="form-control form-control-lg" name="cargo_nature">
									<option value="General">General</option>
									<option value="Dangerous" {{ (!empty($booking->uncode)) ? 'selected' : '' }}>Dangerous</option>
								</select>
							</div> -->
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Remarks</label>
								<textarea name="remarks" rows="5" class="form-control form-control-lg" placeholder="Enter Remarks">{{ (!empty($booking->remarks)) ? $booking->remarks : '' }}</textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
			<a href="{{ action('BookingController@index') }}"><button type="button" class="btn btn-success btn-action">Return to Booking List</button></a>
			<div class="pull-right">
				<input type="text" id="txt_button" hidden name="txt_button">
				<button id="btn-transfer" type="button" class="btn btn-success btn-action m-r-10">Transfer to BL</button>
				<button id="btn-save" type="button" class="btn btn-primary btn-action m-r-10">Save</button>
				<button id="btn-confirm" type="button" data-value="confirm" class="btn btn-primary btn-action pull-right">Confirm</button>
			</div>
		</div>
	</div>

	<input id="raw_qty" name="raw_qty" type="hidden" class="form-control form-control-lg" />
</form>
@stop

@section("page_script")
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />

<script type="text/javascript">
	$(function(){

		var shipment_type = "{{ $booking->shipment_type }}";
		var vessel_type = "{{ $booking->getVoyage->vessel->semicontainer_service }}";
		var raw = "";

		var allow = parseInt("{{ $booking->getVoyage->vessel->dwt - $booking->getVoyage->totalUsedWeight() }}");
		var old_weight = parseInt("{{ $booking->weight }}");

		if(old_weight != null && isNaN(old_weight) == false){
			allow += old_weight;
		} else {
			old_weight = allow;
		}

		$(".datetime").datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
			startDate: new Date()
		});

		$("textarea[name='uncode']").blur(function(){
			if($(this).val().trim() != ""){
				$("#cargo_nature").val("Dangerous").trigger("change");
			} else {
				$("#cargo_nature").val("General").trigger("change");
			}
		});

		function validateForm(){
			var valid = true;
			$(".required").each(function(){
				if($(this).val().trim() == ""){
					swal("Oops!", $(this).attr("data-title") + " is required.", "warning");
					valid = false;
					return false;
				}
			});

			var input_weight = $("input[name='weight']").val();

			if(input_weight > parseInt(allow) && input_weight > old_weight){
				swal("Oops!", "Entered weight has exceeded allowable DWT of " + allow + " MT", "warning");
				valid = false;
				return false;
			}

			if(shipment_type == "FCL"){
				if($("#cargo_name").val().trim() == ""){
					//valid = false;
					//swal("Oops!", "There should be at least 1 container filled.", "warning");
					//return false;
				}
			}

			if(valid == true){
				return "true";
			} else {
				return "false";
			}
		}

		$("#btn-transfer").click(function(){
			var valid = validateForm();
			if(valid == "true"){
				swal({
					title: "Transfer booking to bl",
					text: "This booking will be saved and transferred to a new BL",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'Continue',
					cancelButtonText: 'Cancel',
					reverseButtons: true
				}).then((result) => {
					if (result.value) {
						$("#txt_button").val("transfer");
						$(this).closest("form").submit();
					}
				});
			}
		});

		$("#btn-save").click(function(){
			var valid = validateForm();
			if(valid == "true"){
				$(this).closest("form").submit();
			}
		});

		$("#btn-confirm").click(function(e){
			var $btn = $(this);
			var valid = true;

			$(".required").each(function(index, elem){
				if($(this).val().trim() == ""){
					$(this).focus();
					swal("Oops", $(this).attr("data-title") + " is required.", "warning");
					valid = false;
					return false;
				}
			});

			var input_weight = $("input[name='weight']").val();

			if(input_weight > parseInt(allow) && input_weight > old_weight){
				swal("Oops!", "Entered weight has exceeded allowable DWT of " + allow + " MT", "warning");
				valid = false;
				return false;
			}

			if(valid){
				swal({
					title: "Comfirm booking",
					text: "Future edits made to this booking will be stored as amendments",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'Continue',
					cancelButtonText: 'Cancel',
					reverseButtons: true
				}).then((result) => {
					if (result.value) {
						$("#txt_button").val("confirm");
						$btn.closest("form").submit();
					}
				});
			}
		});

		$(document).on("keydown", "input[data-type='number']", function(e){
			if(!((e.keyCode > 95 && e.keyCode < 106) || (e.keyCode > 47 && e.keyCode < 58) || e.keyCode == 8 || e.keyCode == 9) ) {
				e.preventDefault();
				return false;
			}
		});

		$("input[name='weight']").blur(function(){
			var entered_val = parseFloat($(this).val()).toFixed(3);

			if(isNaN(entered_val)){
				$(this).val(0);
			} else if($(this).val() > 99999.000){
				$(this).val("99999.000");
			} else {
				$(this).val(entered_val);
			}
		});

		$("#cargo_qty").change(function(){
			var number = parseInt($(this).val());
			if(number > 0 && !isNaN(number)){
				$(this).val(number);
			} else {
				$(this).val("");
			}
		});

		$('#cargo_qty, #cargo_packing').change(function() {
			$('#cargo_name').val($('#cargo_qty').val() + ' ' + $('#cargo_packing').val().toUpperCase());
		});

		$(".cargo-qty").blur(function() {
			var entered_value = $(this).val();
			if(entered_value == ""){
				$(this).val(0);
			} else {
				$(this).val(parseInt(entered_value));
			}
		}).focus(function() {
			if($(this).val() == 0){
				$(this).val("");
			} else {
				$(this).select();
			}
		});

		$('.cargo-qty').change(function() {
			var desc = '';
			var html = '';
			var raw_qty = '';
			var qty = 0;
			var count = 0;

			//For FCL shipment we will calculate the TUES
			var tues_count = 0;

			$('.cargo-qty').each(function(index) {

				var entered_value = parseInt($(this).val());

				if(entered_value != 0) {
					if(count > 0) {
						desc += ' & ';
					}

					desc += entered_value + ' x ' + $(this).attr('data-type');
					raw_qty += entered_value + ',' + $(this).attr('data-type') + ';';

					var split = $(this).attr('data-type').split("'");

					//Calculate number of TUES
					if(shipment_type == "FCL"){
						if(split[0] == 40){
							//If semicontainer, 40footer is 25mt, 20footer is 20mt
							if(vessel_type == 1){
								tues_count += parseInt(entered_value * 25);
							} else {
								tues_count += parseInt(entered_value * 2);
							}
						} else {
							if(vessel_type == 1){
								tues_count += parseInt(entered_value * 20);
							} else {
								tues_count += parseInt(entered_value);
							}
						}
					}

					// for(var i = 0; i < entered_value; i++) {
					// 	html += '<div class="row container-row">' +
					// 	'<div class="col-lg-3">' +
					// 	'<div class="form-group m-b-10">' +
					// 	'<label>Container No.</label>' +
					// 	'<input type="text" name="container_no[]" class="form-control form-control-lg" placeholder="Container No." />' +
					// 	'</div>' +
					// 	'</div>' +
					// 	'<div class="col-lg-3">' +
					// 	'<div class="form-group m-b-10">' +
					// 	'<label>Seal No.</label>' +
					// 	'<input type="text" name="seal_no[]" class="form-control form-control-lg" placeholder="Seal No." />' +
					// 	'</div>' +
					// 	'</div>' +
					// 	'<div class="col-lg-3">' +
					// 	'<div class="form-group m-b-10">' +
					// 	'<label>Size</label>' +
					// 	'<input type="text" name="container_size[]" class="form-control form-control-lg" placeholder="Size" value="' + split[0] + '" readonly />' +
					// 	'</div>' +
					// 	'</div>' +
					// 	'<div class="col-lg-3">' +
					// 	'<div class="form-group m-b-10">' +
					// 	'<label>Type</label>' +
					// 	'<input type="text" name="container_type[]" class="form-control form-control-lg" placeholder="Type" value="' + split[1] + '" readonly />' +
					// 	'</div>' +
					// 	'</div></div>';
					// }

					// $('.cargo-container').html(html);
					count++;
				}
			});

			//Only assign TUES value if it is FCL shipment
			if(shipment_type == "FCL"){
				$("input[name='weight']").val(tues_count);
			}

			$('#cargo_name').val(desc);
			$('#raw_qty').val(raw_qty);
		});

		$('#btn_reset').click(function() {
			swal({
				title: 'Are your sure?',
				text: 'Resetting quantity will erase all your progress. Proceed?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Proceed',
				reverseButtons: true
			}).then((result) => {
				if(result.value) {
					$('.cargo-qty').val(0);
					$('#cargo_name').val('');
					$('.cargo-container').html('');
				}
			});
		});

		if($("#cy_cutoff").length == 1){
			$("#cy_cutoff").datepicker({
				format: 'dd/mm/yyyy',
				autoclose: true,
				startDate: new Date()
			});
		}

		// $("#haulge_request_date").val("{{ !empty($booking->haulge_request_date) ? $booking->haulge_request_date->format('d/m/Y') : '' }}");
		// $("#cy_cutoff").val("{{ !empty($booking->cy_cutoff) ? $booking->cy_cutoff->format('d/m/Y h:i A') : '' }}");


		if(shipment_type == "FCL"){
			raw = "{!! $booking->cargo_qty !!}";
			getQuantities();
		}

		function getQuantities() {
			var qty = 0;
			var type = "";
			var semi_split = raw.split(";");


			for(var i = 0; i < semi_split.length; i++) {
				if(semi_split[i] != "") {
					var comma_split = semi_split[i].split(",");
					qty = comma_split[0];
					type = comma_split[1];

					console.log(type + " " + qty);

					$('.cargo-qty[data-type="' + type + '"]').val(qty);
				}
			}
        }

        $(".cargo-qty").trigger("change");

	});
</script>
@stop
