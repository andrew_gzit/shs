@extends("layouts.nav")
@section('page_title', 'Edit Booking')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('BookingController@index') }}">Booking</a></li>
	<li class="breadcrumb-item active">Edit Booking</li>
</ol>
@stop

@section("content")
<h1 class="page-header">Edit Booking - {{ $bk->booking_no }}</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<form method="POST" action="{{ action('BookingController@update', $bk->id) }}">
	@method('PUT')
	@csrf
	<div class="panel">
		<div class="panel-body p-20">
			<h4>Booking Details</h4>
			<hr>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label>Date</label>
						<input id="txt_date" type="text" class="required datetime form-control form-control-lg" name="txt_date">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Shipper</label>
						<select id="txt_shipper" class="form-control form-control-lg m-b-5" name="shipper_id">
							<option disabled="" selected="" value="-">-- select an option --</option>
							@foreach($shippers AS $ship)
							<option value="{{ $ship->comp_id }}" data-name="{{ $ship->name }}">{{ $ship->code . ' - ' . $ship->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Booking Party</label>
						<select id="txt_bookingparty" class="form-control form-control-lg m-b-5" name="bookingparty_id">
							<option disabled="" selected="" value="-">-- select an option --</option>
							@foreach($shippers AS $ship)
							<option value="{{ $ship->comp_id }}" data-name="{{ $ship->name }}" {{ ($bk->bookingparty_id == $ship->id) ? 'selected' : '' }}>{{ $ship->code . ' - ' . $ship->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel">
		<div class="panel-body p-20">
			<h4>Voyage Details</h4>
			<hr>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label>Port of Loading</label>
						<select id="filter_pol" class="filter form-control form-control-lg m-b-5 ddl" name="pol">
							<option disabled="" selected="" value="-">-- select an option --</option>
							@foreach($ports AS $port)
							<option value="{{ $port->id }}" data-name="{{ $port->name }}" {{ ($port->code == 'MYPEN') ? 'selected' : '' }}>
								{{ $port->code }}{{--  - {{ $port->name }} --}}
							</option>
							@endforeach
						</select>
						<div class="m-t-5 port-name">
							<span>@foreach($ports AS $port) {{ ($port->code == 'MYPEN') ? $port->name : '' }} @endforeach</span>
						</div>
					</div>
					<div class="form-group">
						<label>Port of Discharge</label>
						<select id="filter_pod" class="filter form-control form-control-lg m-b-5 ddl" name="pod">
							<option disabled="" selected="" value="-">-- select an option --</option>
							@foreach($ports AS $port)
							<option value="{{ $port->id }}" data-name="{{ $port->name }}">{{ $port->code }}{{--  - {{ $port->name }} --}}</option>
							@endforeach
						</select>
						<div class="m-t-5 port-name">
							<span></span>
						</div>
					</div>
					<div class="form-group">
						<label>Final Port of Discharge</label>
						<select id="ddl_fpd" class="form-control form-control-lg" name="fpd" disabled>
							<option disabled="" selected="" value="-">-- select an option --</option>
						</select>
						<div class="m-t-5 port-name">
							<span></span>
						</div>
					</div>
					<div id="div_eta_fpd" class="form-group" style="display: none">
						<label>ETA Final Port of Discharge</label>
						<input id="txt_eta_fpd" type="text" class="eta_fpd form-control form-control-lg" placeholder="Enter ETA FPD" name="eta_fpd">
					</div>
					<div class="form-group">
						<label>Shipment Type</label>
						<select id="shipment_type" class="form-control form-control-lg" name="shipment_type" disabled>
							<option value="FCL">FCL</option>
							<option value="LCL">LCL</option>
						</select>
					</div>
					<div class="form-group">
						<label>Mother Vessel</label>
						<select id="mother_vessel" class="ddl-mv form-control form-control-lg" name="mvessel" disabled>
							<option disabled="" selected="" value="-">-- select an option --</option>
							<!-- <option value="none">None</option> -->
							@foreach($vessels AS $vessel)
							<option value="{{ $vessel->name }}">{{ $vessel->name }}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="col-md-10">
					<table class="table table-bordered table-condensed table-striped table-valign-middle f-s-14 m-t-20">
						<thead>
							<tr>
								<th width="1%"></th>
								<th>Vessel</th>
								<th>Voyage</th>
								<th>ETA POL</th>
								<th>ETA POD</th>
								<th>Vessel Capacity (TEUS/DWT)</th>
								<th>Port Rotations</th>
							</tr>
						</thead>
						<tbody id="tbody_voy">
							@if(count($voyages) > 0)

							@else
							<tr>
								<td colspan='7' class='text-center'>No voyage found.</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<input type="text" id="txt_button" hidden name="txt_button">

	<button type="button" class="btn btn-danger btn-action btn_cancel m-r-10">Cancel</button>
	<button type="button" data-value="save" class="btn-save btn btn-primary btn-action m-r-10">Save</button>

	<div class="pull-right">
		<button type="button" data-value="cargo" class="btn-save btn btn-success btn-action">Cargo</button>
	</div>
<!-- @if($bk->confirm == 0)
<button id="btn-confirm" type="button" data-value="confirm" class="btn btn-primary btn-action pull-right">Confirm</button>
@endif -->
</form>
@stop

@section("page_script")
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script type="text/javascript" src="/js/booking.js"></script>
<script type="text/javascript">
	$(function(){

		var voy_id = "{{ $bk->voyage_id }}";

		var edit = true;

		$("input").attr("autocomplete", "off");

		$('.btn_cancel').click(function(e) {
			e.preventDefault();
			swal({
				title: 'Are you sure?',
				text: 'Any unsaved progress will be lost.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Proceed',
				reverseButtons: true
			}).then((result) => {
				if(result.value) {
					window.location.replace('{{ action('BookingController@index') }}');
				}
			});
		});

		$("#filter_pol, #filter_pod, #ddl_fpd").change(function() {
			var name = $(this).find('option:selected').attr('data-name');
			$(this).closest(".form-group").find(".port-name > span").text(name);
		});

		$("#txt_shipper").val("{{ $bk->shipper_id }}").trigger("change");
		$("#txt_bookingparty").val("{{ $bk->bookingparty_id }}").trigger("change");
		$("#filter_pol").val("{{ $bk->pol_id }}").trigger('change');

		$(".datetime").datetimepicker({
			format: "DD/MM/YYYY",
			minDate: new Date()
		});

		$(".eta_fpd").datetimepicker({
			format: "DD/MM/YYYY",
			useCurrent: false,
			minDate: new Date()
		});

		$("#txt_date").val("{{ $bk->date->format('d/m/Y') }}");

		$("#txt_date").on('dp.change', function() {
			if($(this).val() == ""){
				$(this).data("DateTimePicker").destroy();
				$(this).datetimepicker({
					format: "DD/MM/YYYY",
					minDate: moment().millisecond(0).second(0).minute(0).hour(0)
				});
			}
			updateVoyage();
		});

		$(".filter").change(function(e){
			updateVoyage(this.id);
		});


		$("#filter_pod").val("{{ $bk->pod_id }}").trigger("change");

		//Load selected vessel

		$(".loader").last().fadeIn(500);

		var voy_id = "{{ $bk->voyage_id }}";
		var shipment_type = "{{ $bk->shipment_type }}";
		var fpd = "{{ $bk->fpd_id }}";
		var pod = "{{ $bk->pod_id }}";
		var eta_fpd = "{{ $bk->eta_fpd->format('d/m/Y') }}";

		setTimeout(function(){
			$("input[name='voyage_id'][value='" + voy_id + "']").click();

			setTimeout(function(){
				//If FPD is not same, means has ETA FPD and mother vessel
				if(fpd != pod){
					$("#ddl_fpd").val(fpd).trigger("change");

					$("#txt_eta_fpd").val(eta_fpd);

					if ($('#mother_vessel').find("option[value='" + "{{ $bk->mvessel }}" + "']").length) {
						$('#mother_vessel').val("{{ $bk->mvessel }}").trigger('change');
					} else {
						var newOption = new Option("{{ $bk->mvessel }}", "{{ $bk->mvessel }}", true, true);
						$('#mother_vessel').append(newOption).trigger('change');
					}
				}

				$(".loader").last().fadeOut(500);
			}, 500);
		}, 800);

		$("#txt_shipper, #txt_bookingparty").change(function(){
			//Check restriction only if selected value is not the same as other party
			if($("#txt_shipper").val() != $("#txt_bookingparty").val() && $("input[name='voyage_id']:checked").length > 0){
				var voyage_id = $("input[name='voyage_id']:selected").val();
				if(voyage_id != voy_id){
					checkRestriction($(this).val(), $("input[name='voyage_id']:checked").val(), this.id);
				}
			}
		});

		$(document).on("change", "input[name='voyage_id']", function(){
			var voyage_elem = $(this);
			var voyage_id = voyage_elem.val();
			$("#shipment_type").removeAttr("disabled");

			$.get("/get-vessel-shipment/" + voyage_id, function(data){
				if(data[0] == "FCL"){
					$("#shipment_type").find("option[value='LCL']").attr("disabled", "");
					$("#shipment_type").find("option:enabled").prop('selected', true).trigger("change");
				} else if (data[0] == "LCL"){
					$("#shipment_type").find("option[value='FCL']").attr("disabled", "");
					$("#shipment_type").find("option:enabled").prop('selected', true).trigger("change");
				} else {
					$("#shipment_type").find("option").removeAttr("disabled");
				}

				if(data[1] == null){
					$("#shipment_type").find("option:enabled").prop("selected", true).trigger("change");
				} else {
					$("#shipment_type").val(shipment_type).trigger("change");
				}
			});

			//Prevent checking restriction upon entering edit booking page
			if(voyage_id != voy_id){
				var company_id = $("#txt_shipper").val();
				var bk_party_id = $("#txt_bookingparty").val();

				if(company_id != null && bk_party_id != null){
					if(company_id == bk_party_id){
						checkRestriction(company_id, voyage_id, "both");
					} else {
						checkRestriction(company_id, voyage_id, "txt_shipper");
						checkRestriction(bk_party_id, voyage_id, "txt_bookingparty");
					}
				}
			}

			//If has data_transhipment, enable transhipment input & mother vessel
			var ts_json = voyage_elem.attr("data-transhipment");
			// var html_select = '<option selected="" value="none" data-name="">None</option>';
			var html_select = '';
			if(ts_json != ""){
				$("#ddl_fpd").removeAttr("disabled");
				var ts_object = JSON.parse(ts_json);
				ts_object.forEach(function(elem, index){
					html_select += "<option value='" + elem.id + "' data-name='" + elem.name + "'>" + elem.code + "</option>";
				});
				$("#ddl_fpd").html(html_select);
			} else {
				//Dont disable fpd, select pod here instead
				var pod_id = $("#filter_pod").val();
				var option = $("#filter_pod").find("option:selected")

				$("#ddl_fpd").html("<option value='" + option.attr("value") + "' data-name='" + option.attr("data-name") + "'>" + option.text())
				.trigger("change").removeAttr("disabled");
			}

			// $.get("/get-destinations/" + voyage_id, function(data){
			// 	if(data.success){
			// 		var dest = data.dest;
			// 		var html = "";
			// 		dest.forEach(function(elem, index){
			// 			html += "<option value='" + elem.id + "'>" + elem.code + " - " + elem.name + "</option>";
			// 		});
			// 		$(".ports").empty().append(html);
			// 		$("select[name='pol']").find("option[value='" + data.pol + "']").prop("selected", true).trigger("change");
			// 	} else {

			// 	}
			// });

		});


		$(".btn-save").click(function(e){
			e.preventDefault();
			var value = $(this).attr("data-value");
			$("#txt_button").val(value);
			var valid = true;

			if($("input[name='voyage_id']:checked").length == 0){
				swal("Oops", "Voyage has not been selected yet.", "warning");
				valid = false;
				return false;
			}

			//If transhipment port is selected, mother vessel must select too
			if($("#ddl_fpd").val() != $("#filter_pod").val()){
				if($("#txt_eta_fpd").val() == ""){
					swal("Oops", "ETA Final Port of Discharge is required.", "warning");
					valid = false;
					return false;
				}

				if($("#mother_vessel").val() == null){
					swal("Oops", "Mother vessel is required.", "warning");
					valid = false;
					return false;
				}
			}

			$(".required").each(function(index, elem){
				if($(this).val().trim() == ""){
					swal("Oops", $(this).attr("data-title") + " is required.", "warning");
					valid = false;
					return false;
				}
			});

			if(valid){
				$(this).closest("form").submit();
			}
		});

		$(".ddl").select2();

		$(".ddl-mv").select2({
			tags: true
		});

		$( window ).resize(function() {
			$(".ddl").select2();

			$(".ddl-mv").select2({
				tags: true
			});
		});

		$("#txt_shipper, #txt_bookingparty").select2();

	});
</script>
@stop
