@extends("layouts.nav")
@section('page_title', 'Booking')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item">
		<a href="{{ action('BookingController@create') }}"><button type="button" class="btn btn-primary"><i class="fa fa-plus m-r-10"></i>Create  Booking</button></a>
	</li>
</ol>
@stop

@section("content")
<h1 class="page-header">Booking</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<div class="panel">
	<form id="form_filter" method="GET">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group m-b-0">
						<label>Vessel</label>
						<select id="vessel_filter" class="form-control form-control-lg select2" name="vessel">
							<option value="-">-- select an option -- </option>
							@foreach($vessels AS $ves)
							<option value="{{ $ves->id ?? $ves->name }}">{{ $ves->name }}</option>
                            @endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group m-b-0">
						<label>Voyage</label>
						<select id="voyage_filter" class="form-control form-control-lg select2" name="voyage">
							<option value="-">-- select an option -- </option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row justify-content-center">
				<div class="col-lg-2">
					<a href="{{ action('BookingController@index') }}" class="btn btn-default btn-block">Reset</a>
				</div>
				<div class="col-lg-2">
					<button id="btn_filter" type="button" class="btn btn-primary btn-block">Apply Filter</button>
				</div>
			</div>
		</div>
	</form>
</div>

<ul class="nav nav-tabs">
	<li class="nav-items">
		<a href="#default-tab-1" data-toggle="tab" class="nav-link active">
			<span class="d-sm-none">Booking List</span>
			<span class="d-sm-block d-none">Booking List</span>
		</a>
	</li>
	<li class="nav-items">
		<a href="#default-tab-2" data-toggle="tab" class="nav-link">
			<span class="d-sm-none">Cancelled Bookings</span>
			<span class="d-sm-block d-none">Cancelled Bookings</span>
		</a>
	</li>
</ul>
<div class="tab-content">
	<div class="tab-pane fade active show" id="default-tab-1">


		<div class="clearfix">
            @if(!empty($query_vessel))
            <a href="javascript:(0);" onclick="location.reload()" class="btn btn-success m-b-15 pull-right"><i class="fas fa-sync-alt m-r-10"></i>Refresh</a>
            @endif
            <div id="exportBookingListExcel"></div><div id="exportBookingListExcel"></div>
		</div>

		<div class="">
			<table id="tbl_booking" class="table table-bordered table-valign-middle f-s-14 bg-white m-b-0">
				<thead class="thead-custom">
					<tr>
						<th width="1%" class="text-center">#</th>
						<th width="1%">Date</th>
						<th width="1%" class="no-wrap">Booking No.</th>
						<th>Shipper</th>
						<th>Booking Party</th>
                        <th>Vessel</th>
                        <th>Mvessel</th>
						<th>Voyage</th>
						<th width="1%">Status</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($bookings1 AS $bk)
					<tr>
						<td>{{ $bk->id }}</td>
						<td>{{ $bk->date->format('d/m/Y') }}</td>
						<td>{{ $bk->booking_no }}</td>
						<td>{{ $bk->getShipper->getName() }}</td>
						<td>{{ $bk->getBookingParty->getName() }}</td>
                        <td>{{ $bk->getVoyage->vessel->name }}</td>
                        <td>{{ $bk->mvessel ?? 'N/A' }}</td>
						<td>{{ $bk->getVoyage->voyage_id }}</td>
						<td>
							@switch($bk->status)
							@case($bk::status_open)
							<span class="text-green f-w-600">{{ $bk::type[$bk->status] }}</span>
							@break
							@case($bk::status_closed)
							<span class="text-red f-w-600">{{ $bk::type[$bk->status] }}</span>
							@break
							@case($bk::status_transferred)
							<span class="text-orange f-w-600">{{ $bk::type[$bk->status] }}</span>
							@break
							@endswitch
						</td>
						<td width="1%">
							<div class="dropdown">
								<button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog"></i>
								</button>
								<div class="dropdown-menu dropdown-menu-right f-s-14" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item btn-cargo" href="{{ action('BookingController@create_cargo', Hashids::encode($bk->id)) }}">Cargo</a>
									<a class="dropdown-item btn-email" href="javascript:(0);" data-id="{{ $bk->id }}">Email to Booking Party</a>
									<a class="dropdown-item btn-transfer" data-id="{{ $bk->id }}" target="_blank" href="{{ action('BillLadingController@create') }}?booking={{ $bk->id }}">Transfer to BL</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item btn-viewmore" data-id="{{ $bk->id }}" href="javascript:(0);">
										<i class="fas fa-info-circle m-r-10"></i>View More
									</a>
									<a class="dropdown-item btn-edit" data-transfer="{{ ($bk->status == $bk::status_transferred) ? 'true' : 'false' }}" href="{{ action('BookingController@edit', Hashids::encode($bk->id)) }}">
										<i class="fas fa-edit m-r-10"></i>Edit Booking
									</a>
									<a class="dropdown-item btn-delete" href="javascript:(0);">
										<form method="POST" class="d-inline" action="{{ action('BookingController@delete', $bk->id) }}">
											@method('DELETE')
											@csrf
										</form>
										<i class="fas fa-trash m-r-10"></i>
										Cancel Booking
									</a>
								</div>
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="tab-pane fade" id="default-tab-2">
		<div class="table-responsive">
            <div id="exportCancelledBookingListExcel" class="clearfix"></div>
			<table id="tbl_cancelled_booking" class="table table-bordered table-valign-middle f-s-14 bg-white m-b-0">
				<thead class="thead-custom">
					<tr>
						<th>Date</th>
						<th>Booking No.</th>
						<th>Shipper</th>
						<th>Booking Party</th>
                        <th>Vessel</th>
                        <th>Mvessel</th>
						<th>Voyage</th>
						<th>Status</th>
						<th>Created Date</th>
						<th>Cancelled Date</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($deleted AS $bk)
					<tr>
						<td>{{ $bk->date->format('d/m/Y') }}</td>
						<td>{{ $bk->booking_no }}</td>
						<td>{{ $bk->getShipper->code }}</td>
						<td>{{ $bk->getBookingParty->code }}</td>
                        <td>{{ $bk->getVoyage->vessel->name }}</td>
                        <td>{{ $bk->mvessel ?? 'N/A' }}</td>
						<td>{{ $bk->getVoyage->voyage_id }}</td>
						<td><span class="text-red f-w-600">{{ $bk::type[$bk->status] }}</span></td>
						<td>{{ $bk->created_at->format('d/m/Y h:i A') }}</td>
						<td>{{ $bk->deleted_at->format('d/m/Y h:i A') }}</td>
						<td width="1%">
							@if($bk->checkVoyageExist())
							<form style="display: inline" method="POST" action="{{ action('BookingController@recover_bk', Hashids::encode($bk->id)) }}">
								@csrf
								<button type="button" data-bk-no="{{ $bk->booking_no }}" class="btn btn-success btn-recover">Recover</button>
							</form>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-view">
	<div class="modal-dialog" style="max-width: 800px">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">View Booking</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs">
					<li class="nav-items">
						<a href="#tab-info" data-toggle="tab" class="nav-link active">
							<span class="d-sm-none">Booking Information</span>
							<span class="d-sm-block d-none">Booking Information</span>
						</a>
					</li>
					<li class="nav-items">
						<a href="#tab-amend" data-toggle="tab" class="nav-link">
							<span class="d-sm-none">Amendments</span>
							<span class="d-sm-block d-none">Amendments</span>
						</a>
					</li>
				</ul>
				<div class="tab-content m-b-0 p-l-0 p-r-0 text-black">
					<div class="tab-pane fade active show" id="tab-info">
						<div class="row">
							<div class="col-md-6">
								<table class="table table-bordered table-condensed f-s-14 m-b-0">
									<tbody>
										<tr>
											<td width="1%" class="no-wrap f-w-600">Date</td>
											<td id="bk_date"></td>
										</tr>
										<tr>
											<td width="1%" class="no-wrap f-w-600">Booking No.</td>
											<td id="bk_bk_no"></td>
										</tr>
										<tr>
											<td width="1%" class="no-wrap f-w-600">Voyage</td>
											<td id="bk_voyage"></td>
										</tr>
										<tr>
											<td width="1%" class="no-wrap f-w-600">Vessel</td>
											<td id="bk_vessel"></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-md-6">
								<table class="table table-bordered table-condensed f-s-14 m-b-0">
									<tbody>
										<tr>
											<td width="1%" class="no-wrap f-w-600">Booking Party</td>
											<td id="bk_bk_party"></td>
										</tr>
										<tr>
											<td width="1%" class="no-wrap f-w-600">Shipper</td>
											<td id="bk_shipper"></td>
										</tr>
										<tr>
											<td width="1%" class="no-wrap f-w-600">Shipment Type</td>
											<td id="bk_shipment"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<hr>
						<div class="row">
							<!-- <div class="col-md-6">
								<table class="table table-bordered table-condensed f-s-14">
									<tbody>
										<tr>
											<td width="1%" class="no-wrap f-w-600">Freight Term</td>
											<td id="bk_freight_term"></td>
										</tr>
									</tbody>
								</table>
							</div> -->
							<div class="col-md-6">
								<table class="table table-bordered table-condensed f-s-14 m-b-0">
									<tbody>
										<tr>
											<td width="1%" class="no-wrap f-w-600">Port of Loading</td>
											<td id="bk_pol"></td>
										</tr>
										<tr class="tr_pod">
											<td width="1%" class="no-wrap f-w-600">Port of Discharge</td>
											<td id="bk_pod"></td>
										</tr>
										<tr>
											<td width="1%" class="no-wrap f-w-600">Final Port of Discharge</td>
											<td id="bk_fpd"></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-md-6">
								<table class="table table-bordered table-condensed f-s-14 m-b-0">
									<tbody>
										<tr>
											<td width="1%" class="no-wrap f-w-600">ETA POL</td>
											<td id="bk_eta_pol"></td>
										</tr>
										<tr class="tr_pod">
											<td width="1%" class="no-wrap f-w-600">ETA POD</td>
											<td id="bk_eta_pod"></td>
										</tr>
										<tr>
											<td width="1%" class="no-wrap f-w-600">ETA FPD</td>
											<td id="bk_eta_fpd"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-12">
								<table class="table table-bordered table-condensed f-s-14">
									<tbody>
										<tr>
											<td width="1%" class="no-wrap f-w-600">Cargo Description</td>
											<td id="bk_cargo_desc"></td>
										</tr>
										<tr>
											<td width="1%" class="no-wrap f-w-600" id="bk_weight_label"></td>
											<td id="bk_weight_content"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="tab-amend">
						<table class="table table-bordered table-striped table-condensed f-s-14 m-b-0">
							<thead>
								<tr>
									<th width="1%" class="no-wrap">#</th>
									<th>Changes</th>
								</tr>
							</thead>
							<tbody id="tbody_amendment">

							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
			</div>
		</div>
	</div>
</div>
@stop

@section("page_script")
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<link href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css" rel="stylesheet" />

<script src="https://cdn.datatables.net/plug-ins/1.10.16/sorting/datetime-moment.js"></script>

<script type="text/javascript">
	$(function(){

		$(document).on("click", ".btn-email", function(e){
			var id = $(this).attr("data-id");
			$.ajax({
				url: "/email-booking/" + id,
				type: "POST",
				success: function(data){
					console.log(data);
					if(data.success){
						window.open(data.path);
					}
				},
				error: function(data){

				}
			});
		});

		$.fn.dataTable.moment('DD/MM/YYYY');
		$.fn.dataTable.moment('DD/MM/YYYY hh:mm A');

		var tb = $("#tbl_booking").DataTable({
			"columnDefs": [
			{ "orderable": false, "targets": [2,3,4,5,6,7,8] }
			],
			"order": [[ 0, "desc" ]],
        });

        var buttons = new $.fn.dataTable.Buttons(tb, {
            buttons: [
                {
                    extend: 'excelHtml5',
                    filename: 'BookingList.xlsx',
                    title: 'Booking List',
                    text: 'Download Booking List',
                    className: 'btn btn-link pull-right',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                    }
                }
            ]
        }).container().appendTo($('#exportBookingListExcel'));

        var tcb = $("#tbl_cancelled_booking").DataTable({
			"columnDefs": [
			{ "orderable": false, "targets": [2,3,4,5,6,7,8] }
			],
			"order": [[ 0, "desc" ]],
        });

        var buttons = new $.fn.dataTable.Buttons(tcb, {
            buttons: [
                {
                    extend: 'excelHtml5',
                    filename: 'CancelledBookingList.xlsx',
                    title: 'Cancelled Booking List',
                    text: 'Download Cancelled Booking List',
                    className: 'btn btn-link pull-right',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                    }
                }
            ]
        }).container().appendTo($('#exportCancelledBookingListExcel'));

		$(document).on("click", ".btn-transfer", function(e){
			e.preventDefault();
			var booking_id = $(this).attr("data-id");
			var url = $(this).attr("href");
			//Check if weight is filled in
			$.get('/checkTransferBL/' + booking_id, function(data){
				if(data.success){
					window.location.href = url;
				} else {
					swal("Oops!", "Unable to transfer to BL. <br>" + data.msg, "error");
				}
			});
		});

		$(document).on("click", ".btn-edit", function(e){
			e.preventDefault();
			var transferred = $(this).attr('data-transfer');
			var href = $(this).attr('href');
			if(transferred == "true"){
				swal({
					title: "Edit Booking",
					text: "The selected booking has been transferred to a BL. Proceed with editing?",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'Continue',
					cancelButtonText: 'Cancel',
					reverseButtons: true
				}).then((result) => {
					if (result.value) {
						window.location.href = href;
					}
				});
			} else {
				window.location.href = href;
			}
		});

		$(document).on("click", ".btn-delete", function(){
			var $btn = $(this);
			swal({
				title: "Confirm cancel booking?",
				text: "Press Continue to proceed",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Continue',
				cancelButtonText: 'Cancel',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					$btn.find("form").submit();
				}
			});
		});

		$(document).on("click", ".btn-viewmore", function(){
			var bk_id = $(this).attr("data-id");
			$.get("/get-booking/" + bk_id, function(data){
				if(data.success){
					var bk = data.booking;
					var amendments = data.amendments;
					var amend_html = "<tr>";
					var counter = 1;

					$("#bk_bk_no").text(bk.booking_no);
					$("#bk_date").text(bk.bk_date);
					$("#bk_voyage").text(bk.bk_voyage);
					$("#bk_vessel").text(bk.bk_vessel);
					$("#bk_bk_party").text(bk.bk_bookingparty);
					$("#bk_shipper").text(bk.bk_shipper);
					$("#bk_eta_pol").text(bk.etapol);
					$("#bk_eta_fpd").text(bk.etafpd);
					$("#bk_pol").text(bk.pol);
					$("#bk_fpd").text(bk.fpd);
					$("#bk_shipment").text(bk.shipment_type);
					$("#bk_cargo_desc").text(bk.cargo_desc);

					if(bk.shipment_type == "FCL"){
						$("#bk_weight_label").text("No. of Container");
						$("#bk_weight_content").text(bk.cargo_packing);
					} else {
						$("#bk_weight_label").text("Weight");
						$("#bk_weight_content").text(bk.weight);
					}

					if(bk.fpd_id != bk.pod_id){
						$(".tr_pod").show();
						$("#bk_pod").text(bk.pod);
						$("#bk_eta_pod").text(bk.etapod);
					} else {
						$(".tr_pod").hide();
					}


					for (var property in amendments) {
						amend_html += "<td>" + counter + "</td><td>";
						var arr = amendments[property];
						arr.forEach(function(elem1, index1){
							amend_html += "<p>" + elem1.type + ": <s>" + elem1.old_value + "</s> " + elem1.value + "</p>";
						});
						amend_html += "</td></tr>";
						counter++;
					}

					$("#tbody_amendment").empty().append(amend_html);
					$("#modal-view").modal("toggle");
				}
			});
		});

		$(document).on("click", ".btn-recover", function(){
			var form = $(this).closest("form");
			var bk_no = $(this).attr("data-bk-no");
			swal({
				title: "Recover " + bk_no + "?",
				html: "Press Proceed to recover selected booking",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Proceed',
				cancelButtonText: 'Cancel',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					form.submit();
				}
			});
		});

		$(".select2").select2();

		$("#btn_filter").click(function(){
			$(".select2").each(function(){
				if($(this).val() == "-"){
					$(this).removeAttr("name");
				}
			});

			$("#form_filter").submit();
		});

		$("#vessel_filter").change(function(){
			$.ajax({
				type: "POST",
				data: {id: $(this).val()},
				url: '/bl/voyages/list',
				success: function(data){
					if(data.success == true) {
						var html = '<option selected value="-">-- select an option --</option>';

						for(var i = 0; i < data.result.length; i++) {
							html += '<option value="' + data.result[i].id + '">' + data.result[i].voyage_id + '</option>';
						}

						$('#voyage_filter').html(html);
					} else {
						$('#voyage_filter').html('<option selected value="-">-- select an option --</option>');
					}
				}
			});
		});

		@if(!empty($query_vessel))
		$("#vessel_filter").val("{{ $query_vessel }}").trigger("change");

		@if(!empty($query_voyage))
		setTimeout(function(){
			$("#voyage_filter").val("{{ $query_voyage }}").trigger("change");
		}, 1000);
		@endif

		@endif
	});
</script>
@stop
