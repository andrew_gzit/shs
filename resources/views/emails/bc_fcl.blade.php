<style type="text/css">
table td:nth-child(2)::before{
	content: " : ";
	padding-right: 10px;
}
</style>

<div style="font-size: 14px">

	<h3 style="margin-top: 60px;">BOOKING CONFIRMATION</h3>

	<img src="img/logo.png" class="pull-right" width="80" style="position: absolute; right: 0px; top: 0px; image-rendering: pixelated;">

	<table style="width: 100%; margin-bottom: 20px">
		<tr>
			<td width="100">DATE</td>
			<td>{{ $bk->created_at->format("d M Y") }}</td>
		</tr>
		<tr>
			<td>FROM</td>
			<td>{{ $base_company->name }}</td>
		</tr>
		<tr>
			<td>EMAIL</td>
			<td>{{ $base_company->getEmails()->first()->email }}</td>
		</tr>
	</table>

	<table style="width: 100%">
		<tr>
			<td width="100">BOOKING NO.</td>
			<td>{{ $bk->booking_no }}</td>
		</tr>
		<tr>
			<td>BOOKING PARTY</td>
			<td>{{ $bk->getBookingParty->getName() }}</td>
		</tr>
		<tr>
			<td>SHIPPER</td>
			<td>{{ $bk->getShipper->getName() }}</td>
		</tr>
	</table>

	<h5>ROUTE INFORMATION</h5>

	<table style="width: 100%">
		<tr>
			<td width="150">INTENDED VESSEL / VOY.</td>
			<td colspan="3">{{ strtoupper($bk->getVoyage->vessel->name) }}</td>
		</tr>
		<tr>
			<td>PORT OF LOADING</td>
			<td>{{ $bk->getPol->name }}</td>
			<td width="30" style="text-align: right;">ETA:</td>
			<td>{{ $bk->eta_pol->format("d M Y") }}</td>
		</tr>
		<tr>
			<td>PORT OF DISCHARGE</td>
			<td>{{ $bk->getFpd->name }}</td>
			<td width="30" style="text-align: right;">ETA:</td>
			<td>{{ $bk->eta_fpd->format("d M Y") }}</td>
		</tr>
		<tr>
			<td>OPERATOR CODE</td>
			<td colspan="3">&nbsp;</td>
		</tr>
	</table>

	<h5>CARGO & CONTAINER INFORMATION</h5>

	<table style="width: 100%">
		<tr>
			<td width="150">BOOKING QTY SIZE / TYPE</td>
			<td>{{ $bk->cargo_packing }}</td>
		</tr>
		<tr>
			<td>COMMODITY</td>
			<td>{{ $bk->cargo_desc }}</td>
		</tr>
		<tr>
			<td>CARGO NATURE</td>
			<td>{{ $bk->cargo_nature }}</td>
		</tr>
	</table>

	<div style="margin-bottom: 50px">
		<h5>REMARKS:-</h5>
		<i>Transit time, schedules routing and/or mode of transport, if provided in this booking confirmation, are indicative only and it shall not be undertaken that the Goods shall arrive or be available at the Port of Discharge at any particular time nor that it shall be carried by the schedule routing or the mode of transport indicated.</i>
	</div>

	<div style="width: 100%; text-align: right">
		<strong style="margin-bottom: 0 !important;">Syarikat Perkapalan Soo Hup Seng Sdn Bhd <span style="font-size: 12px">(29218-D)</span></strong>
		<p>165, 1st Floor, Victoria Street, 10350 Penang, Malaysia.<br>
			Tel. +6 04 - 2626 871 &nbsp; &nbsp; Fax. +6 04 - 2620 564<br>
			Email. mktg@soship.com.my / cs.ctnr@soship.com.my<br>
			www.soship.com.my
		</p>
	</div>

</div>
