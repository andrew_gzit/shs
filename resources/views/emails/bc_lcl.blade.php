<style type="text/css">
.td_underline{
	border-bottom: 1px solid #000;
}

.td_title, td:first-child{
	position: relative;
}

.td_title::after, td:first-child::after{
	content: " : ";
	position: absolute;
	right: 5px;
}

.p-l-10{
	padding-left: 10px;
}

.text-right{
	text-align: right;
}

.p-r-10{
	padding-right: 10px;
}
</style>

<div style="font-size: 14px">

	<div style="text-align: center; margin-top: 80px; margin-bottom: 20px">
		<i style="font-family: serif; display: block; font-size: 18px">{{ $base_company->name }}</i>
		{!! nl2br($base_company->getAddresses()->first()->address) !!}<br>
		Tel : {{ $base_company->telephone }}<br>
		Fax : {{ $base_company->fax }}<br>
		Email : {{ $base_company->getEmails()->first()->email }}
	</div>

	<table style="width: 100%; margin-bottom: 20px">
		<tr>
			<td class="td_title" style="width: 70px">TO</td>
			<td class="td_underline" style="width: 100px; white-space: nowrap;">{{ $bk->getBookingParty->getName() }}</td>
			<td class="td_title p-l-10 p-r-10 text-right" style="width: 70px">DATE</td>
			<td class="td_underline">{{ $bk->created_at->format("d.m.Y") }}</td>
		</tr>
		<tr>
			<td class="td_title" style="width: 70px">ATTN</td>
			<td class="td_underline">{{ $bk->pic }}</td>
			<td class="td_title p-l-10 p-r-10 text-right" style="width: 70px">FROM</td>
			<td class="td_underline">{{ $base_company->name }}</td>
		</tr>
		<tr>
			<td class="td_title" style="width: 70px">FAX NO</td>
			<td class="td_underline">&nbsp;</td>
			<td class="td_title p-l-10 p-r-10 text-right" style="width: 70px">NOTE</td>
			<td class="td_underline">&nbsp;</td>
		</tr>
	</table>

	<p style="text-transform: uppercase; text-decoration: underline; font-weight: 600;">RE: BOOKING CONFIRMATION BY CONVENTIONAL FROM {{ $bk->getPol->location }} TO {{ $bk->getFpd->location }}</p>

	<p>We thank you for your support and kindly be advised as follow:-</p>

	<table style="width: 100%; margin-bottom: 20px">
		<tr>
			<td width="150">SHIPPING ORDER NO</td>
			<td style="font-weight: 600">{{ $bk->booking_no }}</td>
		</tr>
		<tr>
			<td>VESSEL / VOY</td>
			<td style="font-weight: 600; text-transform: uppercase;">{{ $bk->getVoyage->vessel->name }} {{ $bk->getVoyage->voyage_id }}</td>
		</tr>
		<tr>
			<td>SCN</td>
			<td style="font-weight: 600">{{ $bk->getVoyage->scn }}</td>
		</tr>
		<tr>
			<td>ETA {{ $bk->getPol->location }}</td>
			<td>{{ $bk->eta_pol->format('d.m.y') }} (SUBJECT ALL GOING WELL)</td>
		</tr>
		<tr>
			<td>ETA {{ $bk->getFpd->location }}</td>
			<td>{{ $bk->eta_fpd->format('d.m.y') }}</td>
		</tr>
		<tr>
			<td>BERTHING</td>
			<td>{{ $bk->getPol->code }} ({{ $bk->getPol->station_code }})</td>
		</tr>
		<tr>
			<td>AGENT CODE</td>
			<td>SHS - PS0136</td>
		</tr>
	</table>

	<table style="width: 100%; margin-bottom: 20px">
		<tr>
			<td width="150">DISCHARGING PORT</td>
			<td style="font-weight: 600" colspan="3">Y35 - PENDING PORT</td>
		</tr>
		<tr>
			<td>SHIPPER</td>
			<td colspan="3">{{ $bk->getShipper->getName() }}</td>
		</tr>
		<tr>
			<td style='vertical-align: top;'>QTY / COMMODITY</td>
			<td style='vertical-align: top;'>{{ $bk->cargo_qty . " " . $bk->cargo_packing . " " . $bk->cargo_desc }}<br>({{ strtoupper($bk->cargo_nature) }})</td>
			<td style='text-align: right; vertical-align: top'>UN NO :</td>
			<td style='vertical-align: top;'>{!! nl2br($bk->uncode) !!}</td>
		</tr>
	</table>

	<strong>REMARKS :</strong>
	<p>
		{{ $bk->remarks }}
	</p>
	<p style="margin-bottom: 40px">
		KINDLY SEND YOUR CARGOES TO PPSB COMMENCING ON: TBA
	</p>
	<p>
		* Thank you for your continuous support. Will keep you well posted of latest progress.
	</p>

</div>