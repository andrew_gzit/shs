<style type="text/css">
table td:nth-child(2)::before, table td:nth-child(4)::before{
	content: " : ";
	padding-right: 10px;
}

table td:nth-child(1), table td:nth-child(3){
	width: 1%;
	white-space: nowrap;
}

.tbl_charge{
	text-align: left;
	width: 100%;
	padding-top: 10px;
	border-top: 1px solid #000;
	border-bottom: 1px solid #000;
	margin-bottom: 80px;
}

</style>

<div style="font-size: 14px;">

	<h3>{{ $bl->bc_name }}</h3>
	<p>
		{!! $bl->bc_add !!}
		TEL : {{ $bl->bc_telephone }} FAX : {{ $bl->bc_fax }}<br>
		EMAIL : {{ $bl->bc_email }}
	</p>

	<h3>NOTICE OF ARRIVAL</h3>
	<hr style="background-color: #000;">

	<table style="width: 100%; margin-bottom: 20px">
		<tr>
			<td>Date</td>
			<td colspan="3">{{ $bl->date_now }}</td>
		</tr>
		<tr>
			<td>Our Reference</td>
			<td colspan="3">{{ $bl->ref }}</td>
		</tr>
		<tr>
			<td style="vertical-align: top">From</td>
			<td colspan="3" style="padding-bottom: 15px">SCN: {{ $bl->bc_portcode }} &nbsp; &nbsp; AGENT CODE: {{ $bl->bc_bacode }}</td>
		</tr>
		<tr>
			<td>To</td>
			<td colspan="3">{{ $bl->to }}</td>
		</tr>
		<tr>
			<td>Email</td>
			<td colspan="3">{{ $bl->email }}</td>
		</tr>
		<tr>
			<td>Bill of Lading</td>
			<td colspan="3">{{ $bl->bl_no }}</td>
		</tr>
		<tr>
			<td>Port of Origin</td>
			<td colspan="3">{{ $bl->pol_name }}</td>
		</tr>
		<tr>
			<td>Vessel</td>
			<td>{{ $bl->vessel }}</td>
			<td>Voyage</td>
			<td>{{ $bl->voyage }}</td>
		</tr>
		<tr>
			<td>ETA</td>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td>Weight</td>
			<td colspan="3">{{ $bl->weight }} {{ $bl->weight_unit }}</td>
		</tr>
		<tr>
			<td>Measurement</td>
			<td colspan="3">{{ $bl->volume }} M3</td>
		</tr>
		<tr>
			<td style="vertical-align: top">Content</td>
			<td colspan="3">
				{{ $bl->containers }}
				<div style="padding-left: 18px; white-space: pre-line;"> 
					{{ $bl->no_packages }}<br>
					{{ ($bl->cargo_desc) ? $bl->cargo_desc . '<br><br>' : '' }}
					{!! nl2br($bl->detailed_desc) !!}
				</div>
			</td>
		</tr>
	</table>

	<p>Cheque Payable To 'SYARIKAT PERKAPALAN SOO HUP SENG SDN BHD'</p>

</div>
