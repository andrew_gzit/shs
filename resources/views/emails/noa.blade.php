<style type="text/css">
	body{
		font-family: sans-serif;
		font-size: 13px;
		margin: 20px 0.5in 0 0.5in;
	}

	table{
		width: 100%;
		border-spacing: 0;
		vertical-align: top;
	}

	table.table-header td{
		padding-bottom: 10px
	}

	table.table-content{
		margin-bottom: 20px
	}

	.header{
		text-align: center;
		margin-bottom: 10px
	}

	.header span{
		font-size: 13px;
	}

	.title{
		font-weight: bold;
		text-decoration: underline;
	}

	.important{
		font-weight: bold;
		font-size: 15px;
	}
</style>

<body>

	<div class="header">
		<h2>{{ $bl->bc_name }} <span>({{ $bl->bc_roc_no }})</span></h2>
		<strong>{{ strip_tags($bl->bc_add) }} <br><span style="padding-right: 10px">Tel : {{ $bl->bc_telephone }}</span> Fax : {{ $bl->bc_fax }} <br> E-mail : {{ $bl->bc_email }}</strong>
	</div>

	<table class="table-header">
		<tr>
			<td>TO:</td>
			<td colspan="2">&nbsp;<br>{{ $bl->to }} <br> 
				<div > 
					{{ $bl->consignee_add }}
				</div>
			</td>
		</tr>
		<tr>
			<td>TEL:</td>
			<td></td>
			<td>FAX:</td>
			<td></td>
			<td colspan="2"></td>
		</tr>
	</table>

	<p>Dear Sirs / Madam,</p>
	<p class="title">RE: NOTICE OF ARRIVAL</p>

	<table class="table-content">
		<tr>
			<td>Vessel / Voy</td>
			<td colspan="2">: {{ $bl->vessel }}</td>
		</tr>
		<tr>
			<td>B/L No</td>
			<td colspan="4">: {{ $bl->bl_no }}</td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td>Shipper</td>
			<td>: {{ $bl->shipper_name }}</td>
		</tr>
		<tr>
			<td>Consignee</td>
			<td>: {{ $bl->to }}</td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td>Commodity</td>
			<td>: </td>
		</tr>
		<tr>
			<td>Container no:</td>
			<td>: </td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td>POL</td>
			<td>: {{ $bl->pol_name }}</td>
		</tr>
		<tr>
			<td>POD</td>
			<td>: {{ $bl->fpd_name }}</td>
		</tr>
		<tr>
			<td>ETA POD</td>
			<td>: </td>
		</tr>
		<tr>
			<td>SCN</td>
			<td>: </td>
		</tr>
		<tr>
			<td>Vessel ID</td>
			<td>: </td>
		</tr>
		<tr>
			<td>Agent Code</td>
			<td>: {{ $bl->bc_bacode }}</td>
		</tr>
	</table>

	<p>
		Please be advised that the above consignment(s) which is / are consigned to your good self will be arriving at Penang as per above. Please arrange to clear your cargo(es) / container(s) within PPSB free storage period – we will not be responsible for any charges arising from the delay in clearance.
		<br><br>
		Kindly surrender the original B/L to {{ $bl->bc_name }} to obtain the Delivery Order.
	</p>

	<p>Thank you.<br><br></p>

	<p>Yours Sincerely,<br>{{ $bl->bc_name }}<br><br><br></p>

	<p>As Agent for Carrier</p>

</body>