<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="/css/style.min.css" rel="stylesheet">

<h3 class="text-uppercase f-w-600">Booking Confirmation</h3>

<table class="table">
	<tr>
		<td>DATE</td>
		<td>{{ $bk->created_at->format("d M Y") }}</td>
	</tr>
	<tr>
		<td>FROM</td>
		<td>TAN KG / Syarikat Perkapalan Soo Hup Seng Sdn Bhd</td>
	</tr>
	<tr>
		<td>EMAIL</td>
		<td>mktg@soship.com.my</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>BOOKING NO.</td>
		<td>{{ $bk->booking_no }}</td>
	</tr>
	<tr>
		<td>BOOKING PARTY</td>
		<td>{{ $bk->getBookingParty->getName() }}</td>
	</tr>
	<tr>
		<td>SHIPPER</td>
		<td>{{ $bk->getShipper->getName() }}</td>
	</tr>
</table>

<h5>ROUTE INFORMATION</h5>

<table class="table">
	<tr>
		<td>INTENDED VESSEL / VOY.</td>
		<td colspan="3">{{ strtoupper($bk->getVoyage->vessel->name) }}</td>
	</tr>
	<tr>
		<td>PORT OF LOADING</td>
		<td>{{ $bk->getPol->name }}</td>
		<td>ETA</td>
		<td>{{ $bk->eta_pol->format("d M Y") }}</td>
	</tr>
	<tr>
		<td>PORT OF DISCHARGE</td>
		<td>{{ $bk->getFpd->name }}</td>
			<td>ETA</td>
		<td>{{ $bk->eta_fpd->format("d M Y") }}</td>
	</tr>
	<tr>
		<td>OPERATOR CODE</td>
		<td colspan="3">&nbsp;</td>
	</tr>
</table>

<h5>CARGO & CONTAINER INFORMATION</h5>

<table>
	<tr>
		<td>BOOKING QTY SIZE / TYPE</td>
		<td>{{ $bk->cargo_packing }}</td>
	</tr>
</table>
