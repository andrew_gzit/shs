<style type="text/css">
.td_underline{
	border-bottom: 1px solid #000;
}

.td_title, td:first-child{
	position: relative;
}

.td_title::after, td:first-child::after{
	content: " : ";
	position: absolute;
	right: 5px;
}

.p-l-10{
	padding-left: 10px;
}

.text-right{
	text-align: right;
}

.p-r-10{
	padding-right: 10px;
}
</style>

<div style="font-size: 14px">

    <div style="text-align: center; margin-top: 80px; margin-bottom: 20px">
        <span style="display: block;">
            <i style="font-family: serif; font-size: 18px">{{ $base_company->name }}</i>
            <small>{{ !empty($base_company->roc_no) ? "($base_company->roc_no)" : "" }}</small>
        </span>
		{!! nl2br($base_company->getAddresses()->first()->address) !!}<br>
		Tel : {{ $base_company->telephone }}<br>
		Fax : {{ $base_company->fax ?? 'N/A' }}<br>
		Email : {{ $base_company->getEmails()->first()->email }}
    </div>

    <div style="text-align: center; margin-top: 20px; margin-bottom: 20px;"><u><b>BOOKING CONFIRMATION</b></u></div>

	<table style="width: 100%; margin-bottom: 20px">
		<tr>
			<td class="td_title" style="width: 120px">TO</td>
			<td class="td_underline" style="width: 100px; white-space: nowrap;">{{ $bk->getBookingParty->getName() }}</td>
            <td class="td_title p-l-10 p-r-10 text-right" style="width: 120px">BOOKING REF NO</td>
			<td class="td_underline" style="width: 100px; white-space: nowrap;">{{ $bk->booking_no }}</td>
		</tr>
		<tr>
			<td class="td_title" style="width: 120px">ATTN</td>
            <td class="td_underline">{{ $bk->pic }}</td>
            <td class="td_title p-l-10 p-r-10 text-right" style="width: 120px">DATE</td>
			<td class="td_underline">{{ $bk->created_at->format("d/m/Y") }}</td>
		</tr>
		<tr>
			<td class="td_title" style="width: 120px">FAX NO</td>
			<td class="td_underline">&nbsp;</td>
			<td class="td_title p-l-10 p-r-10 text-right" style="width: 120px">AGENT NO</td>
			<td class="td_underline">&nbsp;</td>
		</tr>
    </table>

    <hr />

    <table style="width: 100%; margin-bottom: 20px">
		<tr>
			<td class="td_title" style="width: 120px">SHIPPER/FWDG AGENT</td>
			<td class="td_underline" style="width: 100px; white-space: nowrap;">{{ $bk->getShipper->getName() }}</td>
            <td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="td_title" style="width: 120px">COMMODITY</td>
            <td class="td_underline">{{ $bk->cargo_desc }}</td>
            <td class="td_title p-l-10 p-r-10 text-right" style="width: 120px">SCN</td>
			<td class="td_underline">{{ $bk->getVoyage->scn }}</td>
		</tr>
		<tr>
			<td rowspan="2" class="td_title" style="width: 120px">FEEDER VSL</td>
			<td rowspan="2" class="td_underline">{{ $bk->getVoyage->vessel->name }} V.{{ $bk->getVoyage->voyage_id }}</td>
            <td class="td_title p-l-10 p-r-10 text-right" style="width: 120px">Port of Loading</td>
            <td class="td_underline">{{ $bk->getPol->location }}</td>
        </tr>
        <tr>
            <td class="td_title p-l-10 p-r-10 text-right" style="width: 120px">ETA</td>
            <td class="td_underline">{{ $bk->eta_pol->format("d/m/Y") }}</td>
        </tr>
        @if (!empty($bk->mvessel)) {
            <tr>
                <td rowspan="2" class="td_title" style="width: 120px">MOTHER VSL</td>
                <td rowspan="2" class="td_underline">{{ $bk->mvessel }}</td>
                <td class="td_title p-l-10 p-r-10 text-right" style="width: 120px">Transhipment Port</td>
                <td class="td_underline">{{ $bk->getPod->location }}</td>
            </tr>
            <tr>
                <td class="td_title p-l-10 p-r-10 text-right" style="width: 120px">ETA</td>
                <td class="td_underline">{{ $bk->eta_pod->format("d/m/Y") }}</td>
            </tr>
        }
        @endif
        <tr>
			<td rowspan="2" class="td_title" style="width: 120px">PORT OF DISCHARGE</td>
			<td rowspan="2" class="td_underline">{{ $bk->getFpd->location }} ({{ $bk->getFpd->station_code }})</td>
			<td class="td_title p-l-10 p-r-10 text-right" style="width: 120px">Port of Discharge</td>
			<td class="td_underline">{{ $bk->getFpd->location }}</td>
        </tr>
        <tr>
            <td class="td_title p-l-10 p-r-10 text-right" style="width: 120px">ETA</td>
            <td class="td_underline">{{ $bk->eta_fpd->format("d/m/Y") }}</td>
        </tr>
        <tr>
			<td class="td_title" style="width: 120px">CLOSING TIME</td>
			<td class="td_underline">{{ empty($bk->getVoyage->closing_timestamp) ? 'N/A' : $bk->getVoyage->closing_timestamp->format('d/m/Y \@Hi\H\R\S') }}</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
    </table>

    <hr />

    @php
        $packings = [];

        if (!empty($bk->cargo_packing)) {
            $packings = explode('&', $bk->cargo_packing);
        }
    @endphp

    <table style="width: 100%; margin-bottom: 20px">
		<tr>
			<td class="td_title" style="width: 120px">NO OF CONTR(s)</td>
			<td class="td_underline">{{ $packings[0] ?? " " }}</td>
            <td class="td_title p-l-10 p-r-10 text-right" style="width: 120px">YARD</td>
			<td class="td_underline">{{ $bk->yard }}</td>
        </tr>
		<tr>
			<td>&nbsp;</td>
            <td class="td_underline">{{ $packings[1] ?? " " }}</td>
            <td>&nbsp;</td>
			<td class="td_underline">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="td_underline">{{ $packings[2] ?? " " }}</td>
			<td>&nbsp;</td>
			<td class="td_underline">&nbsp;</td>
        </tr>
        @if (count($packings) > 3)
            @for($i = 3; $i < count($packings); $i++)
                <tr>
                    <td>&nbsp;</td>
                    <td class="td_underline">{{ $packings[$i] ?? "" }}</td>
                    <td>&nbsp;</td>
                    <td class="td_underline">&nbsp;</td>
                </tr>
            @endfor
        @endif
        <tr align="top">
			<td class="td_title" style="width: 120px">REMARKS</td>
			<td class="td_underline" colspan="3">
                {{ $bk->remarks }}
            </td>
        </tr>
    </table>

	<p style="width: 60%">
        Thank you for your valuable and continuous support.
        Should there be any changes to the above, kindly advise by returned fax
        in order to avoid any unforeseen charges incurred.
    </p>

    <p>
        Thanks & Best Regards <br/>
        FADILAH
    </p>
</div>
