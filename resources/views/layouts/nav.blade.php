<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>@yield('page_title') | SHS Admin</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta content="" name="description" />
	<meta content="" name="author" />

	<!-- ================== BEGIN BASE CSS STYLE ================== -->

	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	
	<link href="/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" />
	<link href="/css/animate.css" rel="stylesheet" />
	<link href="/css/default.css" rel="stylesheet" id="theme">
	<link href="/css/style.min.css" rel="stylesheet">
	<link href="/css/style-responsive.min.css" rel="stylesheet">
	<link href="/css/custom.css" rel="stylesheet">

	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script async src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
	<!-- ================== END BASE JS ================== -->
</head>
<body>

	<!-- begin #page-container -->
	<div id="page-container" class="page-container fade p-t-0 {{ (Session::get('minified') == 'on') ? 'page-sidebar-minified' : '' }}">
		<!-- begin #header -->
		<div id="header" class="header navbar-default">
			<!-- begin navbar-header -->
			<div class="navbar-header">
				<a href="{{ action('NormalController@index') }}" class="navbar-brand"><img width="30px" src="/img/logo.png" /> 
					@if(Session::get('minified') == 'on')
					<span style="display: none">
						@else
						<span>
							@endif
							<b>SHS</b> Admin<br>
							<span class="version">D01.00.15</span>
						</span>
					</a>
					<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- end navbar-header -->
			</div>
			<!-- end #header -->

			<!-- begin #sidebar -->
			<div id="sidebar" class="sidebar f-s-14">
				<!-- begin sidebar scrollbar -->
				<div data-scrollbar="true" data-height="100%">
					<!-- begin sidebar user -->
					<ul class="nav">
						<li class="nav-profile">
							<a href="javascript:;" data-toggle="nav-profile">
								<div class="info">
									SHS User
									<small>Administrator</small>
								</div>
							</a>
						</li>
					</ul>
					<!-- end sidebar user -->
					<!-- begin sidebar nav -->
					<ul class="nav">
						<li class="nav-header f-s-14" style="padding-bottom: 15px">Navigation</li>
						<li>
							<a href="{{ action('NormalController@index') }}">
								<i class="fas fa-home"></i> 
								<span>Dashboard</span> 
							</a>
						</li>
						<li>
							<a href="{{ action('VoyageController@index') }}">
								{{-- <a href="javascript:;" class="btn-voyage"> --}}
									<i class="fas fa-ship"></i> 
									<span>Voyage Schedule</span> 
								</a>
							</li>
							<li>
								<a href="{{ action('ReservationController@index') }}">
									<i class="fas fa-clipboard"></i> 
									<span>Reservation</span> 
								</a>
							</li>
							<li>
								<a href="{{ action('BookingController@index') }}">
									<i class="fas fa-book"></i> 
									<span>Booking</span> 
								</a>
							</li>
							<li>
								<a href="{{ action('BillLadingController@index') }}">
									<i class="fas fa-file-alt"></i> 
									<span>Bill of Lading</span> 
								</a>
							</li>
							{{-- <li>
								<a href="javascript:;">
									<i class="fas fa-money-check-alt"></i> 
									<span>Charges</span> 
								</a>
							</li> --}}
							<li class="has-sub">
								<a href="javascript:;">
									<b class="caret"></b>
									<i class="fas fa-print"></i>
									<span>Print</span>
								</a>
								<ul class="sub-menu">
									<li><a href="{{ action('PrintController@index_voyage') }}">Voyage Schedule</a></li>
									<li><a href="{{ action('PrintController@index_bl') }}">Shipping Documents</a></li>
									<li><a href="{{ action('PrintController@index_cargostatus') }}">Cargo Status Report</a></li>
								</ul>
							</li>
							<li class="has-sub">
								<a href="javascript:;">
									<b class="caret"></b>
									<i class="fas fa-wrench"></i>
									<span>Maintenance</span>
								</a>
								<ul class="sub-menu">
									<li><a href="{{ action('MaintenanceController@index_basecompany') }}">Base Company</a></li>
									<li><a href="{{ action('MaintenanceController@index_company') }}">Companies</a></li>
									{{-- <li><a href="javascript:;">Users</a></li> --}}
									<li><a href="{{ action('MaintenanceController@index_vessel') }}">Vessels</a></li>
									<li><a href="{{ action('MaintenanceController@index_charge') }}">Charges</a></li>
									<li><a href="{{ action('MaintenanceController@index_port') }}">Ports</a></li>
									{{-- <li><a href="javascript:;">BL Formats</a></li> --}}
									<li><a href="{{ action('MaintenanceController@index_shortcut') }}">Shortcut Keys</a></li>
									{{-- <li><a href="{{ action('MaintenanceController@index_packing_type') }}">Packing Types</a></li> --}}
								</ul>
							</li>
							<li>
								<a href="{{ action('NormalController@logout') }}">
									<i class="fas fa-power-off"></i>
									<span>Logout</span> 
								</a>
							</li>
							<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
						</ul>
						<!-- end sidebar nav -->
					</div>
					<!-- end sidebar scrollbar -->
				</div>
				<div class="sidebar-bg"></div>
				<!-- end #sidebar -->

				<!-- begin #content -->
				<div id="content" class="content">
					@yield('breadcrumb')

					@yield('content')
				</div>
				<!-- end #content -->

				<div class="loader">
					<div class="loader-item text-center">
						<i class="fas fa-3x fa-spinner fa-spin"></i>
					</div>
				</div>
			</div>
			<!-- end page container -->

			<!-- ================== BEGIN BASE JS ================== -->
			<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
			<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
			<script src="/plugins/popper/popper.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<!--[if lt IE 9]>
		<script src="../assets/crossbrowserjs/html5shiv.js"></script>
		<script src="../assets/crossbrowserjs/respond.min.js"></script>
		<script src="../assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
	<script src="/plugins/moment/moment.min.js"></script>
	<script src="/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
	<script src="/js/sweetalert2.all.min.js"></script>

	<script src="/js/apps.js"></script>
	<!-- ================== END BASE JS ================== -->

	<script>

		jQuery.fn.extend({
			insertAtCaret: function(myValue){
				// return this.each(function(i) {
					// console.log(myValue);
					// if (document.selection) {
					// 	this.focus();
					// 	var sel = document.selection.createRange();
					// 	sel.html = myValue;
					// 	this.focus();
					// }
					// else if (this.selectionStart || this.selectionStart == '0') {
					// 	var startPos = this.selectionStart;
					// 	var endPos = this.selectionEnd;
					// 	var scrollTop = this.scrollTop;
					// 	this.html = this.html.substring(0, startPos)+myValue+this.html.substring(endPos,this.html.length);
					// 	this.focus();
					// 	this.selectionStart = startPos + myValue.length;
					// 	this.selectionEnd = startPos + myValue.length;
					// 	this.scrollTop = scrollTop;
					// } else {
					// 	this.html += myValue;
					// 	this.focus();
					// }

					var sel, range;
					if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
        	range = sel.getRangeAt(0);
        	range.deleteContents();

            // Range.createContextualFragment() would be useful here but is
            // only relatively recently standardized and is not supported in
            // some browsers (IE9, for one)
            var el = document.createElement("div");
            el.innerHTML = myValue;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ( (node = el.firstChild) ) {
            	lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);

            // Preserve the selection
            if (lastNode) {
            	range = range.cloneRange();
            	range.setStartAfter(lastNode);
            	range.collapse(true);
            	sel.removeAllRanges();
            	sel.addRange(range);
            }
        }
    } else if (document.selection && document.selection.type != "Control") {
        // IE < 9
        document.selection.createRange().pasteHTML(myValue);
    }

				// });
			}
		});

		function setActive(){
			var pathname = window.location.href;
			var paths = pathname.split('/');

			//Remove slash for index page
			if(pathname.slice("-1") == "/"){
				pathname = pathname.substring(0, pathname.length-1);
			}

			// the tilde that preceeds the expression is shortcut for >= 0
			// tilde flips the sign of the value
			// e.g. indexOf() returns -1 if word not found, tilde will add 1 to value and flip the sign thus becoming 0.
			if(paths.length > 4) {
				if(~paths[4].indexOf('create') || ~paths[4].indexOf('filter') || ~paths[4].indexOf('edit')) {
					paths.splice(4);
					pathname = paths.join('/');
				}
			}

			if(paths.length > 5) {
				if(~paths[5].indexOf('create') || ~paths[5].indexOf('edit')) {
					console.log('indeisde');
					paths.splice(5);
					pathname = paths.join('/');
				}
			}

			$link = $("a[href='" + pathname + "']");

			//If link is inside a submenu, show it and add active class to parent menu
			if($link.closest("ul").hasClass("sub-menu")){
				$link.closest("ul").show().closest("li").addClass("expand active");
			}
			
			$link.closest("li").addClass("active");
		}

		$(document).ready(function() {
			App.init();

			setActive();

			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$(".sidebar-minify-btn").click(function(){
				if(!$("#page-container").hasClass("page-sidebar-minified")){
					$(".navbar-brand span").hide();
					$(".sub-menu").hide();
					_minified = "on";
				} else {
					$(".navbar-brand span").show();
					_minified = "off";
				}

				$.ajax({
					type: "POST",
					url: "/minify-nav",
					data: {minified : _minified},
					success: function(data){
						console.log(data);
					}
				});
			});

			var _minified = "{{ Session::get('minified') }}";

			if(_minified == "on"){
				$(".sub-menu").hide();
			}
		});
	</script>

	@yield("page_script")

</body>
</html>