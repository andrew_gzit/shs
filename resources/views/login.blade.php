<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>Login | SHS Admin</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->

	<link href="/css/jqueryui.min.css" rel="stylesheet">
	<link href="/css/bs.min.css" rel="stylesheet">

	<link href="/css/animate.css" rel="stylesheet" />
	<link href="/css/default.css" rel="stylesheet" id="theme">
	<link href="/css/style.min.css" rel="stylesheet">
	<link href="/css/style-responsive.min.css" rel="stylesheet">
	<link href="/css/custom.css" rel="stylesheet">

	<!-- ================== BEGIN BASE JS ================== -->
	<script async src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
	<!-- ================== END BASE JS ================== -->
</head>
<body class="pace-top bg-white">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade">
		<!-- begin login -->
		<div class="login login-with-news-feed">
			<!-- begin news-feed -->
			<div class="news-feed">
				<div class="news-image" style="background-image: url(/img/login-bg.jpg)"></div>
				<div class="news-caption">
					<h4 class="caption-title"><b>Syarikat Perkapalan Soo Hup Seng Sdn. Bhd.</b></h4>
					<p>
						The Trusted Shipping Solutions Since 1971
					</p>
				</div>
			</div>
			<!-- end news-feed -->
			<!-- begin right-content -->
			<div class="right-content">
				<!-- begin login-header -->
				<div class="login-header">
					<div class="brand">
						<b>SHS Admin</b>
					</div>
					<div class="icon">
						<i class="fa fa-sign-in"></i>
					</div>
				</div>
				<!-- end login-header -->
				<!-- begin login-content -->
				<div class="login-content">


					<form action="{{ action('NormalController@authenticate') }}" autocomplete="off" method="POST" class="margin-bottom-0" data-parsley-validate="true">
						{{ csrf_field() }}
						
						@if(session('msg'))
						<div class="alert {{ session('class') }}">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{!! session('msg') !!}
							{{ \Session::forget('msg') }}
						</div>
						@endif

						<div class="form-group m-b-20">
							<input type="text" class="form-control form-control-lg" placeholder="Username" name="username" data-parsley-required="true" />
						</div>
						<div class="form-group m-b-20">
							<input type="password" class="form-control form-control-lg" placeholder="Password" name="password" data-parsley-required="true" />
						</div>
						<div class="login-buttons">
							<button type="submit" class="btn btn-success btn-block btn-lg">Log In</button>
						</div>
					</form>
				</div>
				<!-- end login-content -->
			</div>
			<!-- end right-container -->
		</div>
		<!-- end login -->

	</div>
	<!-- end page container -->
	
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script src="/plugins/popper/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<!--[if lt IE 9]>
		<script src="../assets/crossbrowserjs/html5shiv.js"></script>
		<script src="../assets/crossbrowserjs/respond.min.js"></script>
		<script src="../assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
	<script src="/js/apps.js"></script>

	<script>
		$(".loader").hide();
		$(document).ready(function() {
			App.init();
		});
	</script>
</body>
</html>
