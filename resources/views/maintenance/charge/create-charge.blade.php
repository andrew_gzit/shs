@extends("layouts.nav")
@section('page_title', 'Create Charge')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('MaintenanceController@index_charge') }}">Charges</a></li>
	<li class="breadcrumb-item active">Create Charge</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Create Charge</h1>

@if(session('success'))
<div class="alert alert-green">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('success') }}
</div>
@endif

<form id="form_charge" method="POST" action="{{ action('MaintenanceController@store_charge') }}">
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-2">
					<div class="form-group">
						<label>Code</label>
						<input type="text" autofocus="on" class="form-control form-control-lg" name="txt_code" placeholder="Enter Charge Code" value="{{ old('txt_code') }}">
						<span class="text-danger">{{ $errors->first('txt_code') }}</span>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<label>Name</label>
						<input type="text" class="form-control form-control-lg text-capitalize" name="txt_name" placeholder="Enter Charge Name" value="{{ old('txt_name') }}">
						<span class="text-danger">{{ $errors->first('txt_name') }}</span>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<!-- <div class="col-lg-2 col-6">
					<div class="form-group m-b-0">
						<label>Unit Price</label>
					</div>
				</div> -->
				<div class="col-lg-2 col-6">
					<div class="form-group m-b-0">
						<label>Unit</label>
					</div>
				</div>
				<div class="col-lg-12">
					@if(old('ddl_unit'))
					@foreach(old('ddl_unit') as $unitkey => $val)
					@if(!empty($val))
					<div class="row unit_row">
						<!-- <div class="col-lg-2 col-sm-3 col-6 div_price">
							<div class="form-group m-b-0">
								<input type="text" data-type="number" autocomplete="off" class="form-control form-control-lg" name="txt_unitprice[]" placeholder="Enter Unit Price" value="{{ $val }}">
							</div>
						</div> -->
						<div class="col-lg-2 col-sm-3 col-6 div_unit">
							<div class="form-group m-b-0">
								<select name="ddl_unit[]" class="form-control form-control-lg ddl_unit">
									<optgroup label="PER">
										@foreach(\App\Unit::per_unit AS $key => $unit)
										<option value="{{ $unit }}" {{ old('ddl_unit.'.$unitkey) === $unit ? "selected" : ''}} >{{ \App\Unit::per_unit_text[$key] }}</option>
										@endforeach
									</optgroup>
									<optgroup label="PER 20">
										@foreach(\App\Unit::per20_unit AS $key => $unit)
										<option value="{{ $unit }}" {{ old('ddl_unit.'.$unitkey) === $unit ? "selected" : ''}} >{{ \App\Unit::per20_unit_text[$key] }}</option>
										@endforeach
									</optgroup>
									<optgroup label="PER 40">
										@foreach(\App\Unit::per40_unit AS $key => $unit)
										<option value="{{ $unit }}" {{ old('ddl_unit.'.$unitkey) === $unit ? "selected" : ''}} >{{ \App\Unit::per40_unit_text[$key] }}</option>
										@endforeach
									</optgroup>
								</select>
							</div>
						</div>
						<div class="col-lg-1 d-flex align-items-center f-w-700">
							@if(count(old('ddl_unit')) > 1)
							<label class='lbl-remove m-b-0 f-w-700'>REMOVE</label>
							@else
							<label class='lbl-remove m-b-0 f-w-700' style="display: none">REMOVE</label>
							@endif
						</div>
					</div>
					@endif
					@endforeach
					@else
					<div class="row unit_row">
						<!-- <div class="col-lg-2 col-sm-3 col-6 div_price">
							<div class="form-group m-b-0">
								<input type="text" data-type="number" autocomplete="off" class="form-control form-control-lg" name="txt_unitprice[]" placeholder="Enter Unit Price">
							</div>
						</div> -->
						<div class="col-lg-2 col-sm-3 col-6 div_unit">
							<div class="form-group m-b-0">
								<select name="ddl_unit[]" class="form-control form-control-lg ddl_unit">
									<optgroup label="PER">
										@foreach(\App\Unit::per_unit AS $key => $unit)
										<option value="{{ $unit }}">{{ \App\Unit::per_unit_text[$key] }}</option>
										@endforeach
									</optgroup>
									<optgroup label="PER 20">
										@foreach(\App\Unit::per20_unit AS $key => $unit)
										<option value="{{ $unit }}">{{ \App\Unit::per20_unit_text[$key] }}</option>
										@endforeach
									</optgroup>
									<optgroup label="PER 40">
										@foreach(\App\Unit::per40_unit AS $key => $unit)
										<option value="{{ $unit }}">{{ \App\Unit::per40_unit_text[$key] }}</option>
										@endforeach
                                    </optgroup>
                                    <optgroup label="PER 45">
										@foreach(\App\Unit::per45_unit AS $key => $unit)
										<option value="{{ $unit }}">{{ \App\Unit::per45_unit_text[$key] }}</option>
										@endforeach
									</optgroup>
								</select>
							</div>
						</div>
						<div class="col-lg-1 d-flex align-items-center">
							<label class='lbl-remove m-b-0 f-w-700' style="display: none">REMOVE</label>
						</div>
					</div>
					@endif
				</div>
				<div class="col-lg-12">
					<button type="button" id="btn_charges" class="btn btn-primary">Add Charges</button>
				</div>
			</div>
		</div>
	</div>
	<button type="button" id="btn_cancel" class="btn btn-danger btn-action">Cancel</button>
	<button type="button" id="btn_save" class="btn btn-primary btn-action">Save</button>
</div>

<!-- <input type="text" hidden="" id="txt_charges" name="txt_charges"> -->
</form>

@stop

@section("page_script")
<script type="text/javascript" src="/js/charge.js"></script>

<script type="text/javascript">
	$(function(){

		// $(document).on("blur", "input[data-type='number']", function(e){
		// 	var price = parseInt($(this).val());
		// 	if(!isNaN(price) && price != 0){
		// 		$(this).val(price);
		// 	} else {
		// 		$(this).val("");
		// 	}
		// });

		// $("#btn_charges").click(function(){
		// 	// $clone = $(".div_unit:first").clone();
		// 	// $clone.find("input.form-control").val("");
		// 	// // $clone.append("<div class='col-lg-1'><div class='form-group'><label class='lbl-remove'>REMOVE</label></div></div>");
		// 	// $(".div_unit:last").after($clone);
		// 	// $("input[name='txt_unitprice[]']").focus();

		// 	var $row = $(".unit_row:last");
		// 	var $clone = $row.clone();
		// 	$clone.find("input").val("");
		// 	$clone.find(".div_price label").remove();
		// 	$clone.find(".div_unit label").remove();
		// 	$row.after($clone);

		// 	$(".lbl-remove").show();

		// 	// var $price = $("#div_price").find(".form-group:last").clone();
		// 	// $price.find("label").remove();
		// 	// var $unit = $("#div_unit").find(".form-group:last").clone();
		// 	// $unit.find("label").remove();
		// 	// $("#div_price").find(".form-group:last").after($price);
		// 	// $("#div_unit").find(".form-group:last").after($unit);
		// });

		// $(document).on("click", ".lbl-remove", function(){
		// 	$(this).closest(".unit_row").remove();
		// 	if($(".unit_row").length == 1){
		// 		$(".lbl-remove").hide();
		// 	}
		// });

		// $("#btn_save").click(function(){

		// 	var chargesArr = [];

		// 	var valid = true;
		// 	$(".ddl_unit").each(function(){
		// 		var selected_val = $(this).val();
		// 		if($.inArray(selected_val, chargesArr) > -1){
		// 			swal("Oops", "Duplicate unit found.", "warning");
		// 			valid = false;
		// 			return false;
		// 		} else {
		// 			chargesArr.push(selected_val);
		// 		}
		// 	});

			// var charges = [];

			// var valid = true;

			// $(".unit_row").each(function(){
			// 	var obj = {};
				// obj.unitprice = $(this).find("input[name='txt_unitprice[]']").val();
				// if(parseInt(obj.unitprice) < 1 || isNaN(parseInt(obj.unitprice))){
				// 	swal("Oops", "Unit price cannot be empty!", "warning");
				// 	valid = false;
				// 	return false;
				// }

				// obj.unitsize = $(this).find("select[name='ddl_unitsize[]']").val();
				// obj.unit = $(this).find("select[name='ddl_unit[]']").val();
				// obj.unittext = $(this).find("select[name='ddl_unitsize[]']").find("option:selected").text();

			// 	var exist = containsObject(obj, charges);

			// 	if(!exist){
			// 		charges.push(obj);
			// 	} else {
			// 		valid = false;
			// 		return false;
			// 	}
			// });

		// 	if(valid){
		// 		// $("#txt_charges").val(JSON.stringify(charges));
		// 		$("#form_charge").submit();
		// 	}
		// });

		// function containsObject(obj, list) {
		// 	var i;
		// 	for (i = 0; i < list.length; i++) {
		// 		// var html =
		// 		// "<table class='table table-bordered m-t-10 f-s-15'><tr><th>Unit Price</th><th>Unit Size</th><th>Unit</th></tr><tr><td>" +
		// 		// "RM" + list[i].unitprice + "</td><td>" + list[i].unittext + "</td><td>" + list[i].unit + "</td><tr><td>" +
		// 		// "RM" + obj.unitprice + "</td><td>" + obj.unittext + "</td><td>" + obj.unit + "</td></tr></table>";

		// 		if (list[i].unitsize == obj.unitsize && list[i].unit == obj.unit) {
		// 			swal("Oops", "Duplicate unit found.", "warning");
		// 			return true;
		// 		}
		// 	}

		// 	return false;
		// }
	});
</script>
@stop
