@extends("layouts.nav")
@section('page_title', 'Edit Charge')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('MaintenanceController@index_charge') }}">Charges</a></li>
	<li class="breadcrumb-item active">Edit Charge</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Edit Charge - <span class="f-s-18">{{ $charge->name }}</span></h1>

@if(session('success'))
<div class="alert alert-green">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('success') }}
</div>
@endif

<form id="form_charge" method="POST" action="{{ action('MaintenanceController@put_charge', $charge->id) }}">
	@method('PUT')
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-2">
					<div class="form-group">
						<label>Code</label>
						<input type="text" class="form-control form-control-lg" name="txt_code" placeholder="Enter Charge Code" value="{{ $charge->code }}">
						<span class="text-danger">{{ $errors->first('txt_code') }}</span>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<label>Name</label>
						<input type="text" class="form-control form-control-lg text-capitalize" name="txt_name" placeholder="Enter Charge Name" value="{{ $charge->name }}">
						<span class="text-danger">{{ $errors->first('txt_name') }}</span>
					</div>
				</div>
				<div class="col-lg-12">
					<hr>
				</div>
				<!-- <div class="col-lg-2 col-6">
					<div class="form-group m-b-0">
						<label>Unit Price</label>
					</div>
				</div> -->
				<div class="col-lg-2 col-6">
					<div class="form-group m-b-0">
						<label>Unit</label>
					</div>
				</div>
				<div class="col-lg-12">
					@foreach($charge->getCharges as $key => $detail)
					<div class="row unit_row">
						<!-- <div class="col-lg-2 col-sm-3 col-6 div_price">
							<div class="form-group m-b-0">
								<input type="text" data-type="number" autocomplete="off" class="form-control form-control-lg" name="txt_unitprice[]" value="{{ $detail->unit_price }}" placeholder="Enter Unit Price">
							</div>
						</div> -->
						<div class="col-lg-2 col-sm-3 col-6 div_unit">
							<div class="form-group m-b-0">
								<select name="ddl_unit[]" class="form-control form-control-lg ddl_unit">
									<optgroup label="PER">
										@foreach(\App\Unit::per_unit AS $key => $unit)
										<option value="{{ $unit }}" {{ $detail->unit_size . "-" . $detail->unit === $unit ? "selected" : ''}}>{{ \App\Unit::per_unit_text[$key] }}</option>
										@endforeach
									</optgroup>
									<optgroup label="PER 20">
										@foreach(\App\Unit::per20_unit AS $key => $unit)
										<option value="{{ $unit }}" {{ $detail->unit_size . "-" . $detail->unit === $unit ? "selected" : ''}}>{{ \App\Unit::per20_unit_text[$key] }}</option>
										@endforeach
									</optgroup>
									<optgroup label="PER 40">
										@foreach(\App\Unit::per40_unit AS $key => $unit)
										<option value="{{ $unit }}" {{ $detail->unit_size . "-" . $detail->unit === $unit ? "selected" : ''}}>{{ \App\Unit::per40_unit_text[$key] }}</option>
										@endforeach
                                    </optgroup>
                                    <optgroup label="PER 45">
										@foreach(\App\Unit::per45_unit AS $key => $unit)
										<option value="{{ $unit }}" {{ $detail->unit_size . "-" . $detail->unit === $unit ? "selected" : ''}}>{{ \App\Unit::per45_unit_text[$key] }}</option>
										@endforeach
									</optgroup>
								</select>
							</div>
						</div>
						<div class="col-lg-1 d-flex align-items-center f-w-700">
							@if(count($charge->getCharges) == 1)
							<label class='lbl-remove m-b-0 f-w-700' style="display: none">REMOVE</label>
							@else
							<label class='lbl-remove m-b-0 f-w-700'>REMOVE</label>
							@endif
						</div>
					</div>
					@endforeach
				</div>
				<div class="col-lg-12">
					<button type="button" id="btn_charges" class="btn btn-primary">Add Charges</button>
				</div>
				<!-- <div class="col-lg-2">
					<div class="form-group m-0">
						<label>Unit Price</label>
					</div>
				</div>
				<div class="col-lg-1">
					<div class="form-group m-0">
						<label>Unit Size</label>
					</div>
				</div>
				<div class="col-lg-1">
					<div class="form-group m-0">
						<label>Unit</label>
					</div>
				</div> -->
				<!-- <div class="col-lg-12">
					@foreach($charge->getCharges as $key => $detail)
					<div class="row div_unit">
						<div class="col-lg-2">
							<div class="form-group">
								<input type="text" data-type="number" autocomplete="off" class="form-control form-control-lg" name="txt_unitprice[]" value="{{ $detail->unit_price }}" placeholder="Enter Unit Price">
							</div>
						</div>
						<div class="col-lg-1 div_unitsize">
							<div class="form-group">
								<select name="ddl_unitsize[]" class="form-control form-control-lg ddl_size">
									<option value="1" {{ ($detail->unit_size == 1) ? 'selected' : ''}}>PER</option>
									<option value="20" {{ ($detail->unit_size == 20) ? 'selected' : ''}}>PER 20</option>
									<option value="40" {{ ($detail->unit_size == 40) ? 'selected' : ''}}>PER 40</option>
								</select>
							</div>
						</div>
						<div class="col-lg-1 div_unit1">
							<div class="form-group">
								<select name="ddl_unit[]" class="form-control form-control-lg ddl_unit">
									<option value="MT" {{ ($detail->unit == 'MT') ? 'selected' : ''}}>MT</option>
									<option value="M3" {{ ($detail->unit == 'M3') ? 'selected' : ''}}>M3</option>
									<option value="GP" {{ ($detail->unit == 'GP') ? 'selected' : ''}}>GP</option>
									<option value="HC" {{ ($detail->unit == 'HC') ? 'selected' : ''}}>HC</option>
									<option value="SET" {{ ($detail->unit == 'ET') ? 'selected' : ''}}>SET</option>
									<option value="UNIT" {{ ($detail->unit == 'UNIT') ? 'selected' : ''}}>UNIT</option>
								</select>
							</div>
						</div>
						<div class='col-lg-1'><div class='form-group'><label class='lbl-remove'>REMOVE</label></div></div>
					</div>
					@endforeach
				</div> -->
				<div class="col-lg-12">
				</div>
			</div>
		</div>
	</div>

	<input type="text" hidden="" id="txt_charges" name="txt_charges">
	<button type="button" id="btn_cancel" class="btn btn-danger btn-action">Cancel</button>
	<button type="button" id="btn_save" class="btn btn-primary btn-action">Save</button>
</form>
@stop

@section("page_script")
<script type="text/javascript" src="/js/charge.js"></script>
<script type="text/javascript">
	$(function(){

		// $(document).on("blur", "input[data-type='number']", function(e){
		// 	var price = parseInt($(this).val());
		// 	if(!isNaN(price) && price != 0){
		// 		$(this).val(price);
		// 	} else {
		// 		$(this).val("");
		// 	}
		// });

		// $("#btn_save").click(function(){
		// 	var charges = [];

		// 	var valid = true;

		// 	$(".div_unit").each(function(){
		// 		var obj = {};
		// 		obj.unitprice = $(this).find("input[name='txt_unitprice[]']").val();

		// 		if(parseInt(obj.unitprice) < 1 || isNaN(parseInt(obj.unitprice))){
		// 			swal("Oops", "Unit price cannot be empty!", "warning");
		// 			valid = false;
		// 			return false;
		// 		}

		// 		obj.unitsize = $(this).find("select[name='ddl_unitsize[]']").val();
		// 		obj.unit = $(this).find("select[name='ddl_unit[]']").val();
		// 		// obj.unittext = $(this).find("select[name='ddl_unitsize[]']").find("option:selected").text();

		// 		var exist = containsObject(obj, charges);

		// 		if(!exist){
		// 			charges.push(obj);
		// 		} else {
		// 			valid = false;
		// 			return;
		// 		}
		// 	});

		// 	if(valid){
		// 		$("#txt_charges").val(JSON.stringify(charges));
		// 		$("#form_charge").submit();
		// 	}
		// });

		// $("#btn_charges").click(function(){
		// 	$clone = $(".div_unit:first").clone();
		// 	$clone.find("input.form-control").removeAttr("value").val("");
		// 	// $clone.append("<div class='col-lg-1'><div class='form-group'><label class='lbl-remove'>REMOVE</label></div></div>");
		// 	$(".div_unit:last").after($clone);
		// 	$(".lbl-remove").show();
		// 	$("input[name='txt_unitprice[]']").focus();
		// });

		// $(document).on("click", ".lbl-remove", function(){
		// 	$(this).closest(".div_unit").remove();
		// 	if($(".div_unit").length == 1){
		// 		$(".lbl-remove").hide();
		// 	}
		// });

		// function containsObject(obj, list) {
		// 	var i;
		// 	for (i = 0; i < list.length; i++) {
		// 		var html =
		// 		"<table class='table table-bordered m-t-10 f-s-15'><tr><th>Unit Price</th><th>Unit Size</th><th>Unit</th></tr><tr><td>" +
		// 		"RM" + list[i].unitprice + "</td><td>" + list[i].unittext + "</td><td>" + list[i].unit + "</td><tr><td>" +
		// 		"RM" + obj.unitprice + "</td><td>" + obj.unittext + "</td><td>" + obj.unit + "</td></tr></table>";

		// 		if (list[i].unitsize == obj.unitsize && list[i].unit == obj.unit) {
		// 			swal("Oops.", "Duplicate charge detail found.<br>Charge detail must be unique.<br>" + html, "warning");
		// 			return true;
		// 		}
		// 	}

		// 	return false;
		// }
	});
</script>
@stop
