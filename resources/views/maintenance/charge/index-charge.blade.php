@extends("layouts.nav")
@section('page_title', 'Charges')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item">
		<a href="{{ action('MaintenanceController@create_charge') }}"><button type="button" class="btn btn-primary"><i class="fa fa-plus m-r-5"></i>Create Charge</button></a>
	</li>
	<!-- <li class="breadcrumb-item active">Vessel Schedule</li> -->
</ol>

@stop

@section('content')
<h1 class="page-header">
	Charges
</h1>

@if(session('warning'))
<div class="alert alert-yellow">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('warning') }}
</div>
@endif

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<div class="table-responsive">
	<table class="table table-bordered table-valign-middle f-s-14">
		@if($charges->isNotEmpty())
		<thead class="thead-custom">
			<tr>
				<th width="1%" class="no-wrap">Code</th>
				<th>Name</th>
				<!-- <th width="1%" class="no-wrap">Charges</th> -->
				<th width="1%" class="no-wrap">Options</th>
			</tr>
		</thead>
		<tbody class="bg-white">
			@foreach($charges AS $ea)
			<tr>
				<td><strong>{{ $ea->code }}</strong></td>
				<td>{{ $ea->name }}</td>
				<!-- <td class="no-wrap">
					@foreach($ea->getCharges AS $detail)
					{{ ($detail->unit_size == 1) ? 'PER' : 'PER ' . $detail->unit_size }} {{ $detail->unit }}<br>
					@endforeach
				</td> -->
				<td>
					<div class="no-wrap">
						<button type="button" class="btn btn-info btn-view" data-id="{{ $ea->id }}">
							<i class="fa fa-eye"></i>
						</button>
						<a href="{{ action('MaintenanceController@edit_charge', Hashids::encode($ea->id)) }}" class="btn btn-success">
							<i class="fa fa-edit"></i>
						</a>
						<form method="POST" class="d-inline">
							@method('DELETE')
							@csrf
							<button type="button" data-id="{{ $ea->id }}" class="btn btn-danger btn-delete">
								<i class="fa fa-trash"></i>
							</button>
						</form>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
		@else
		<tbody class="bg-white">
			<tr class="text-center">
				<td>No charges to display.</td>
			</tr>
		</tbody>
		@endif
	</table>
</div>

<div class="modal fade" id="modal-view">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">View Charge</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<table id="table_view" class="table table-bordered table-striped table-valign-middle f-s-14">
					<thead>
						<tr>
							<th>Size</th>
							<th>Type</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
			</div>
		</div>
	</div>
</div>
@stop

@section("page_script")
<script type="text/javascript">
	$(function(){
		$(".btn-delete").click(function(){
			swal({
				title: "Confirm delete?",
				text: "Press Continue to proceed",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Continue',
				cancelButtonText: 'Cancel',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					var url = "/maintenance/charges/delete/" + $(this).attr("data-id");
					$(this).closest("form").attr("action", url).submit();
				}
			});
		});

		$(".btn-view").click(function(){

			var charge_id = $(this).attr("data-id");

			$.get("/charge-detail/" + charge_id, function(response){
				var rowArr = "";
				$.each(response, function(name, value) {
					// optionArr += "<optgroup label='" + name + "'>";
					value.forEach(function(element, index){
						var unit_text = "";
						if(element.unit_size == 1){
							unit_text = "PER";
						} else {
							unit_text = "PER " + element.unit_size;
						}
						rowArr += "<tr><td>" + unit_text + "</td><td>" + element.unit + "</td></tr>";
					});
				});

				$("#table_view").find("tbody").empty().append(rowArr);
				$("#modal-view").modal("toggle");
			});

		});
	});
</script>
@stop