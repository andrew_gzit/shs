@extends("layouts.nav")
@section('page_title', 'Base Company')

@section('content')
<h1 class="page-header">Base Company</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<form autocomplete="off" method="POST" action="{{ action('MaintenanceController@store_basecompany') }}">
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body p-20">
			<h4>Company Details</h4>
			<hr>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group m-b-15 {{ $errors->has('txt_name') ? 'has-error' : '' }}">
						<label>Name</label>
						<input type="text" data-type="alphadot" class="form-control form-control-lg m-b-5" name="txt_name" value="{{ old('txt_name') ? old('txt_name') : (($company) ? $company->name : '') }}" placeholder="Enter Company Name">
						@if($errors->has('txt_name'))
						<span class="text-danger">{{ $errors->first('txt_name') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group m-b-15 {{ $errors->has('txt_roc') ? 'has-error' : '' }}">
						<label>ROC No.</label>
						<input type="text" data-type="alphadash" class="form-control form-control-lg m-b-5" name="txt_roc" value="{{ old('txt_roc') ? old('txt_roc') : (($company) ? $company->roc_no : '') }}" placeholder="Enter ROC No.">
						<span class="text-danger">{{ $errors->first('txt_roc') }}</span>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group m-b-15 {{ $errors->has('txt_portcode') ? 'has-error' : '' }}">
						<label>Operator Code</label>
						<input type="text" class="form-control form-control-lg m-b-5" name="txt_portcode" value="{{ old('txt_portcode') ? old('txt_portcode') : (($company) ? $company->port_code : '') }}" placeholder="Enter Operator Code">
						<span class="text-danger">{{ $errors->first('txt_portcode') }}</span>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group m-b-15 {{ $errors->has('txt_bacode') ? 'has-error' : '' }}">
						<label>Agent Code</label>
						<input type="text" class="form-control form-control-lg m-b-5" name="txt_bacode" value="{{ old('txt_bacode') ? old('txt_bacode') : (($company) ? $company->ba_code : '') }}" placeholder="Enter Agent Code">
						<span class="text-danger">{{ $errors->first('txt_bacode') }}</span>
					</div>
				</div>
			</div>
			<hr>

			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label>Address</label>
						<input type="text" maxlength="80" id="txt_address1" class="txt-address-required form-control form-control-lg m-b-5" placeholder="Enter Address 1">
						<input type="text" maxlength="80" id="txt_address2" class="txt-address-required form-control form-control-lg m-b-5" placeholder="Enter Address 2">
						<input type="text" maxlength="80" id="txt_address3" class="form-control form-control-lg" placeholder="Enter Address 3">
					</div>
					<button type="button" id="btn_add_address" class="btn btn-primary">Add Address</button>
				</div>
				<div class="col-lg-6">
					<div id="div_addresses" class="row" style="max-height: 220px; overflow-y: auto">
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label>Email Address</label>
						<input type="text" id="txt_email" class="form-control form-control-lg m-b-5" placeholder="Enter Email Address">
					</div>
					<button type="button" id="btn_add_email" class="btn btn-primary">Add Email</button>
				</div>
				<div class="col-lg-6">
					<div id="div_emails" class="row" style="max-height: 220px; overflow-y: auto">
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group m-b-0 {{ $errors->has('txt_tel') ? 'has-error' : '' }}">
						<label>Telephone</label>
						<input type="text" maxlength="13" data-type="telephone" class="form-control form-control-lg m-b-0" data-title="Telephone number is invalid." name="txt_tel" value="{{ old('txt_tel') ? old('txt_tel') : (($company) ? $company->telephone : '') }}" placeholder="Enter Telephone">
						<span class="text-danger">{{ $errors->first('txt_tel') }}</span>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group m-b-0 {{ $errors->has('txt_fax') ? 'has-error' : '' }}">
						<label>Fax</label>
						<input type="text" maxlength="13" data-type="telephone" class="form-control form-control-lg m-b-0" data-title="Fax number is invalid." name="txt_fax" value="{{ old('txt_fax') ? old('txt_fax') : (($company) ? $company->fax : '') }}" placeholder="Enter Fax">
						<span class="text-danger">{{ $errors->first('txt_fax') }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<input type="text" hidden="" id="txt_addressArr" name="txt_addressArr">
	<input type="text" hidden="" id="txt_emailArr" name="txt_emailArr">
	<button id="btn_save" type="button" class="btn btn-primary btn-action btn-save">Save</button>
</form>

<div class="modal" id="modal-edit-addr">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
					<input type="text" maxlength="80" id="txt_edit_address0" class="txt-edit-address-required form-control form-control-lg m-b-5" placeholder="Enter Address 1">
					<input type="text" maxlength="80" id="txt_edit_address1" class="txt-edit-address-required form-control form-control-lg m-b-5" placeholder="Enter Address 2">
					<input type="text" maxlength="80" id="txt_edit_address2" class="form-control form-control-lg" placeholder="Enter Address 3">
				</div>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary pull-right" id="btn-edit-addr-save">Update Address</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modal-edit-email-addr">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
					<input type="text" id="txt_edit_email" class="form-control form-control-lg m-b-5" placeholder="Enter Email Address">
				</div>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary pull-right" id="btn-edit-email-addr-save">Update Email Address</button>
			</div>
		</div>
	</div>
</div>
@stop

@section("page_script")
<script type="text/javascript" src="/js/jquery.mask.js"></script>
<script type="text/javascript" src="/js/company_helper.js"></script>
<script type="text/javascript" src="/js/company.js"></script>

<script type="text/javascript">
	$(function(){

		if($(".has-error").length > 0){
			$(".has-error:first input").focus();
		}
		
		//Set company email if exists
		var email_obj = unescapeHTML("{{ old('txt_emailArr') }}");
		if(email_obj != ""){
			setEmail(JSON.parse(email_obj));
		} else {
			var company_addr = unescapeHTML("{{ ($company)?$company->getEmails:'' }}");
			if(company_addr != ""){
				setEmail(JSON.parse(company_addr));
			}
		}

		//Set company email if exists
		var old_addrArr = unescapeHTML(unescapeHTML("{{ old('txt_addressArr') }}"));
		if(old_addrArr != ""){

			var address_array = [];

			var parsed = JSON.parse(old_addrArr);

			parsed.forEach(function(element){
				var adrsObj = {};
				var splitted = element.split("<br>");
				var address = "";
				splitted.forEach(function (split){
					if(split.indexOf("<strong>") != -1){
						adrsObj.description = $(split).text();
					} else {
						if(split != ""){
							address += split + "<br>";
						}
					}
				});
				adrsObj.address = address;
				address_array.push(adrsObj);
			});
			
			setAddress(address_array);
		} else {
			var company_email = unescapeHTML(unescapeHTML("{{ ($company)?$company->getAddresses:'' }}"));
			if(company_email != ""){
				setAddress(JSON.parse(company_email));
			}
		}
	});
</script>
@stop