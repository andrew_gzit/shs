@extends("layouts.nav")
@section('page_title', 'Create Company')

@section('breadcrumb')
@if(count($companies) > 0)
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item active">
		<button id="btn_duplicate" type="button" class="btn btn-success"><i class="fa fa-copy m-r-10"></i>Duplicate Other Profile</button>
	</li>
</ol>
@endif
@stop

@section('content')
<h1 class="page-header">Create Company 
	@if(!empty($company))
	- <span class="f-s-18">{{ $company->name }}</span>
	@endif
</h1>

<form id="form_company" method="POST" autocomplete="off" action="{{ (!empty($company)) ? action('MaintenanceController@store_company') . '?cid=' . Hashids::encode($company->id) : action('MaintenanceController@store_company') }}">
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body p-20">
			<h4>Company Details</h4>
			<hr>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group m-b-15 {{ $errors->has('txt_name') ? 'has-error' : '' }}">
						<label>Name</label>
						<input type="text" autofocus="on" class="form-control form-control-lg m-b-5" name="txt_name" value="{{ old('txt_name') }}" placeholder="Enter Company Name">
						@if($errors->has('txt_name'))
						<span class="text-danger">{{ $errors->first('txt_name') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group m-b-15 {{ $errors->has('txt_roc') ? 'has-error' : '' }}">
						<label>ROC No.</label>
						<input type="text" data-type="alphadash" class="form-control form-control-lg m-b-5" name="txt_roc" value="{{ old('txt_roc') }}" placeholder="Enter ROC No.">
						<span class="text-danger">{{ $errors->first('txt_roc') }}</span>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group m-b-15 {{ $errors->has('txt_code') ? 'has-error' : '' }}">
						<label>Code</label>
						<input type="text" class="form-control form-control-lg m-b-5" name="txt_code" value="{{ old('txt_code') }}" placeholder="Enter Company Code">
						<span class="text-danger">{{ $errors->first('txt_code') }}</span>
					</div>
				</div>
			</div>
			<hr>

			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label>Address</label>
						<input type="text" autocomplete="something-new" id="txt_address_desc" class="form-control form-control-lg m-b-5" placeholder="Enter Address Description" maxlength="45" value="MAIN">
						<input type="text" autocomplete="something-new" id="txt_address1" class="txt-address-required form-control form-control-lg m-b-5" maxlength="80" placeholder="Enter Address 1">
						<input type="text" autocomplete="something-new" id="txt_address2" class="form-control form-control-lg m-b-5" maxlength="80" placeholder="Enter Address 2">
						<input type="text" autocomplete="something-new" id="txt_address3" class="form-control form-control-lg" maxlength="80" placeholder="Enter Address 3">
					</div>
					<button type="button" id="btn_add_address" class="btn btn-primary">Add Address</button>
				</div>
				<div class="col-lg-6">
					<div id="div_addresses" class="row" style="max-height: 220px; overflow-y: auto">
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label>Email Address</label>
						<input type="text" id="txt_email" class="form-control form-control-lg m-b-5" placeholder="Enter Email Address">
					</div>
					<button type="button" id="btn_add_email" class="btn btn-primary">Add Email</button>
				</div>
				<div class="col-lg-6">
					<div id="div_emails" class="row" style="max-height: 220px; overflow-y: auto">
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group m-b-15 {{ $errors->has('txt_tel') ? 'has-error' : '' }}">
						<label>Telephone</label>
						<input type="text" maxlength="13" data-type="telephone" data-title="Telephone number is invalid." class="form-control form-control-lg m-b-5 telephone" name="txt_tel" value="{{ old('txt_tel') }}" placeholder="Enter Telephone">
						<span class="text-danger">{{ $errors->first('txt_tel') }}</span>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group m-b-15 {{ $errors->has('txt_fax') ? 'has-error' : '' }}">
						<label>Fax</label>
						<input type="text" maxlength="13" data-type="telephone" data-title="Fax number is invalid." class="form-control form-control-lg m-b-5" name="txt_fax" value="{{ old('txt_fax') }}" placeholder="Enter Fax">
						<span class="text-danger">{{ $errors->first('txt_fax') }}</span>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group m-b-15 {{ $errors->has('txt_pic') ? 'has-error' : '' }}">
						<label>Person In Charge</label>
						<input type="text" data-type="alpha" class="form-control form-control-lg m-b-5" name="txt_pic" value="{{ old('txt_pic') }}" placeholder="Enter PIC">
						<span class="text-danger">{{ $errors->first('txt_pic') }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-inverse">
		<div class="panel-body p-20">
			<h4>Type of Company</h4>
			<hr>
			<div class="row m-b-20">
				<div class="col-md-12">
					<div class="form-group">
						<div class="checkbox checkbox-css checkbox-inline">
							<input type="checkbox" id="radio_shipper" name="radio_company_type[]" value="0" data-class="div_shipper" checked="" />
							<label for="radio_shipper">Shipper / Booking Party</label>
						</div>
						<div class="checkbox checkbox-css checkbox-inline">
							<input type="checkbox" id="radio_consignee" name="radio_company_type[]" value="1" data-class="div_consignee" />
							<label for="radio_consignee">Consignee</label>
						</div>
						<div class="checkbox checkbox-css checkbox-inline">
							<input type="checkbox" id="radio_agent" name="radio_company_type[]" value="3" data-class="div_agent" />
							<label for="radio_agent">Agent</label>
						</div>
					</div>
				</div>
			</div>
			<div id="div_settings" class="m-t-20">
				<h4>Settings</h4>
				<hr>
			</div>
			<div id="div_agent" class="div_companytype" style="display: none">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group m-b-15">
							<label>Port</label>
							<select name="txt_portid" class="select2 form-control form-control-lg">
								@foreach($ports AS $port)
								<option value="{{ $port->id }}">{{ $port->code }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group m-b-15 {{ $errors->has('txt_portcode') ? 'has-error' : '' }}">
							<label>Port Code</label>
							<input type="text" class="form-control form-control-lg m-b-5" name="txt_portcode" value="{{ old('txt_portcode') }}" placeholder="Enter Port Code">
							<span class="text-danger">{{ $errors->first('txt_portcode') }}</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group m-b-15 {{ $errors->has('txt_bacode') ? 'has-error' : '' }}">
							<label>Agent Code</label>
							<input type="text" class="form-control form-control-lg m-b-5" name="txt_bacode" value="{{ old('txt_bacode') }}" placeholder="Enter Agent Code">
							<span class="text-danger">{{ $errors->first('txt_bacode') }}</span>
						</div>
					</div>
				</div>
			</div>
			<div id="div_shipperradio">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group m-b-15">
							<label>Handling Method</label>
							<select id="txt_handlingmethod" class="form-control form-control-lg m-b-5" name="txt_handlingmethod">
								<option>Direct Delivery</option>
								<option>Via Shed</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group m-b-15">
							<div class="checkbox checkbox-css checkbox-inline">
								<input type="checkbox" id="radio_voyageschedule" name="radio_update[]" value="1" checked="" />
								<label for="radio_voyageschedule">Receive Voyage Schedule Updates</label>
							</div>
							<div class="checkbox checkbox-css checkbox-inline">
								<input type="checkbox" id="radio_deliverynotice" name="radio_update[]" value="2"/>
								<label for="radio_deliverynotice">Receive Delivery Notice</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="div_restrictionrow" class="panel panel-inverse">
		<div class="panel-body p-20">
			<h4>Restrictions</h4>
			<hr>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group m-b-0 vessel-group">
						<label>Vessel Name</label>
						<div class="row row_vessel">
							<div class="col-md-9">
								<select class="form-control form-control-lg ddl_vesselname m-b-10" name="ddl_vesselname[]"> 
									<option value="0">None</option>
									@foreach($vessels AS $ea)
									<option>{{ $ea->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-2 d-flex align-items-center">
								<div class='m-b-10'><label style="display: none" class='lbl-remove'>REMOVE</label></div>
							</div>
						</div>
						<button type="button" id="btn_add_vessel" class="btn btn-primary">Add Vessel</button>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group m-b-0">
						<label>Vessel Type</label>
						<select class="form-control form-control-lg ddl_restrictiontype" name="txt_vesseltype">
							<option value="0">None</option>
							<option>General Cargo Vessel</option>
							<option>Container Vessel</option>
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group m-b-0">
						<label>Age (Less Than)</label>
						<input type="text" data-type="number" maxlength="2" name="txt_vesselage" class="form-control form-control-lg" placeholder="Enter Age (Less Than)">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group m-b-0 flag-group">
						<label>Flag</label>
						<div class="row row_flag">
							<div class="col-md-9">
								<select class="form-control form-control-lg ddl_flag m-b-10" name="ddl_flag[]">
									<option selected value="0">None</option>
									<option>Autralia</option>
									<option>Brazil</option>
									<option>Canada</option>
									<option>China</option>
									<option>Germany</option>
									<option>India</option>
									<option>Indonesia</option>
									<option>Italy</option>
									<option>Japan</option>
									<option>Malaysia</option>
									<option>Netherlands</option>
									<option>Saudi Arabia</option>
									<option>Singapore</option>
									<option>South Korea</option>
									<option>Spain</option>
									<option>Taiwan</option>
									<option>Thailand</option>
									<option>Turkey</option>
									<option>Vietnam</option>
								</select>
							</div>
							<div class="col-md-2 d-flex align-items-center">
								<div class='m-b-10'><label style="display: none" class='lbl-remove'>REMOVE</label></div>
							</div>
						</div>
						<button type="button" id="btn_add_flag" class="btn btn-primary">Add Flag</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<input type="text" hidden="" id="txt_addressArr" name="txt_addressArr">
	<input type="text" hidden="" id="txt_emailArr" name="txt_emailArr">
	<input type="text" hidden="" id="txt_restrictionArr" name="txt_restrictionArr">
	<button type="button" class="btn btn-danger btn-cancel btn-action">Cancel</button>
	<button id="btn_save" type="button" class="btn btn-primary btn-action btn-save" value="save">Save</button>
	<button id="btn_proceed" type="button" class="btn btn-success btn-action pull-right btn-save" value="proceed">Save and Proceed to Charges</button>
	<input type="text" hidden="" id="txt_submittype" name="txt_submittype">
</form>

<div class="modal fade" id="modal-duplicate">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Duplicate Other Profile</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">

				<div class="form-group">
					<label>Company</label>
					<select id="ddl_company" class="form-control form-control-lg m-b-5">
						@foreach($companies AS $ea)
						<option value="{{ Hashids::encode($ea->id) }}">{{ $ea->name }}</option>
						@endforeach
					</select>
					<i>Select a company profile to duplicate the company's default charges.</i>
				</div>
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-danger" data-dismiss="modal">Close</a>
				<button id="btn_confirm_duplicate" type="button" class="btn btn-primary">Confirm</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modal-edit-addr">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
					<input type="text" maxlength="45" id="txt_edit_address_desc" class="form-control form-control-lg m-b-5" placeholder="Enter Address Description">
					<input type="text" maxlength="80" id="txt_edit_address0" class="txt-edit-address-required form-control form-control-lg m-b-5" placeholder="Enter Address 1">
					<input type="text" maxlength="80" id="txt_edit_address1" class="form-control form-control-lg m-b-5" placeholder="Enter Address 2">
					<input type="text" maxlength="80" id="txt_edit_address2" class="form-control form-control-lg" placeholder="Enter Address 3">
				</div>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary pull-right" id="btn-edit-addr-save">Update Address</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modal-edit-email-addr">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
					<input type="text" id="txt_edit_email" class="form-control form-control-lg m-b-5" placeholder="Enter Email Address">
				</div>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary pull-right" id="btn-edit-email-addr-save">Update Email Address</button>
			</div>
		</div>
	</div>
</div>
@stop

@section("page_script")
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script type="text/javascript" src="/js/jquery.mask.js"></script>
<script type="text/javascript" src="/js/company_helper.js"></script>
<script type="text/javascript" src="/js/company.js"></script>

<script type="text/javascript">

	//Select2 auto position
	(function($) {

		var Defaults = $.fn.select2.amd.require('select2/defaults');

		$.extend(Defaults.defaults, {
			dropdownPosition: 'auto'
		});

		var AttachBody = $.fn.select2.amd.require('select2/dropdown/attachBody');

		var _positionDropdown = AttachBody.prototype._positionDropdown;

		AttachBody.prototype._positionDropdown = function() {

			var $window = $(window);

			var isCurrentlyAbove = this.$dropdown.hasClass('select2-dropdown--above');
			var isCurrentlyBelow = this.$dropdown.hasClass('select2-dropdown--below');

			var newDirection = null;

			var offset = this.$container.offset();

			offset.bottom = offset.top + this.$container.outerHeight(false);

			var container = {
				height: this.$container.outerHeight(false)
			};

			container.top = offset.top;
			container.bottom = offset.top + container.height;

			var dropdown = {
				height: this.$dropdown.outerHeight(false)
			};

			var viewport = {
				top: $window.scrollTop(),
				bottom: $window.scrollTop() + $window.height()
			};

			var enoughRoomAbove = viewport.top < (offset.top - dropdown.height);
			var enoughRoomBelow = viewport.bottom > (offset.bottom + dropdown.height);

			var css = {
				left: offset.left,
				top: container.bottom
			};

		    // Determine what the parent element is to use for calciulating the offset
		    var $offsetParent = this.$dropdownParent;

		    // For statically positoned elements, we need to get the element
		    // that is determining the offset
		    if ($offsetParent.css('position') === 'static') {
		    	$offsetParent = $offsetParent.offsetParent();
		    }

		    var parentOffset = $offsetParent.offset();

		    css.top -= parentOffset.top
		    css.left -= parentOffset.left;

		    var dropdownPositionOption = this.options.get('dropdownPosition');

		    if (dropdownPositionOption === 'above' || dropdownPositionOption === 'below') {
		    	newDirection = dropdownPositionOption;
		    } else {

		    	if (!isCurrentlyAbove && !isCurrentlyBelow) {
		    		newDirection = 'below';
		    	}

		    	if (!enoughRoomBelow && enoughRoomAbove && !isCurrentlyAbove) {
		    		newDirection = 'above';
		    	} else if (!enoughRoomAbove && enoughRoomBelow && isCurrentlyAbove) {
		    		newDirection = 'below';
		    	}

		    }

		    if (newDirection == 'above' || (isCurrentlyAbove && newDirection !== 'below')) {
		    	css.top = container.top - parentOffset.top - dropdown.height;
		    }

		    if (newDirection != null) {
		    	this.$dropdown
		    	.removeClass('select2-dropdown--below select2-dropdown--above')
		    	.addClass('select2-dropdown--' + newDirection);
		    	this.$container
		    	.removeClass('select2-container--below select2-container--above')
		    	.addClass('select2-container--' + newDirection);
		    }

		    this.$dropdownContainer.css(css);
		};
	})(window.jQuery);

	$(function(){

		if($(".has-error").length > 0){
			$(".has-error:first input").focus();
		}

		$("#btn_test").click(function(){
			$("#form_company").submit();
		});

		$(".select2").select2({
			width: '100%',
			dropdownPosition: 'above'
		});

		$("#btn_duplicate").click(function(){
			$("#modal-duplicate").modal("toggle");
		});

		$("#btn_confirm_duplicate").click(function(){
			var company_id = $("#ddl_company").val();
			var url = "/maintenance/companies/create/?cid=" + company_id;
			window.location.href = url;
		});

		$("#btn_add_restriction").click(function(){
			var $clone = $(".div_restriction:first").clone();
			$clone .append("<div class='col-lg-1'><div class='form-group'><label class='lbl-remove'>REMOVE</label></div></div>");
			$clone.find(".ddl_restriction").hide();
			$clone.find("input").val("").show();
			$(".div_restriction:last").after($clone);
		});

		$(document).on("click", ".lbl-remove", function(){
			$(this).closest(".div_restriction").remove();
		});

		$(document).on("keydown", "input[data-type='number']", function(e){
			if(!((e.keyCode > 95 && e.keyCode < 106) || (e.keyCode > 47 && e.keyCode < 58) || e.keyCode == 8 || e.keyCode == 9) ) {
				e.preventDefault();
				return false;
			}
		});


        //Set company address
        var old_addrArr = unescapeHTML(unescapeHTML("{{ old('txt_addressArr') }}"));

        if(old_addrArr != ""){

        	var address_array = [];

        	var parsed = JSON.parse(old_addrArr);

        	parsed.forEach(function(element){
        		var adrsObj = {};
        		var splitted = element.split("<br>");
        		var address = "";
        		splitted.forEach(function (split){
        			if(split.indexOf("<strong>") != -1){
        				adrsObj.description = $(split).text();
        			} else {
        				if(split != ""){
        					address += split + "<br>";
        				}
        			}
        		});
        		adrsObj.address = address;
        		address_array.push(adrsObj);
        	});

        	// setAddress(JSON.parse(old_addrArr));
        	setAddress(address_array);
        }

        //Set company type
        var company_type = "{{ !empty(old('radio_company_type')) ? count(old('radio_company_type')) : '' }}";
        if(company_type != ""){
        	if(company_type == 1){
        		company_type = parseInt("{{ old('radio_company_type')[0] }}");
        		$("input[name='radio_company_type[]']:not([value='" + company_type + "'])").prop('checked', false);
        		$("input[name='radio_company_type[]'][value='" + company_type + "']").siblings("label").click();
        	} else {
        		$("input[name='radio_company_type[]']:not([value='3'])").siblings("label").click();
        	}
        }

        var handling = "{{ !empty(old('txt_handlingmethod')) ? old('txt_handlingmethod') : '' }}";
        if(handling != ""){
        	if(handling == "Via Shed"){
        		$("#txt_handlingmethod option:last").prop("selected", "selected");
        	}
        }

        var update_type = "{{ !empty(old('radio_update')) ? count(old('radio_update')) : '' }}";
        if(update_type != ""){
        	if(update_type == 1){
        		update_type = "{{ old('radio_update')[0] }}";
        		$("input[name='radio_update[]']:not([value='" + update_type + "'])").prop('checked', false);
        		if(update_type == 2){
        			$("input[name='radio_update[]'][value='" + update_type + "']").siblings("label").click();
        		}
        	} else {
        		if(!$("#radio_voyageschedule").prop("checked")){
        			$("input[name='radio_update[]']").siblings("label").click();
        		} else {
        			$("label[for='radio_deliverynotice']").click();
        		}
        	}
        } else {
        	$("label[for='radio_voyageschedule']").click();
        }

        //Set company email
        var email_obj = unescapeHTML("{{ old('txt_emailArr') }}");
        if(email_obj != ""){
        	setEmail(JSON.parse(email_obj));
        }

        //Set company restrictions
        var vesselname_obj = unescapeHTML("{{ json_encode(old('ddl_vesselname')) }}");
        if(vesselname_obj != "" && vesselname_obj != "null"){
        	var vesselname_json = JSON.parse(vesselname_obj);
        	var vesselname_counter = 0;
        	vesselname_json.forEach(function(elem, index){
        		if(elem != 0){
        			vesselname_counter++;
        			if(vesselname_counter > 1){
        				$("#btn_add_vessel").trigger("click");
        			}

        			$(".row_vessel:last").find("select").val(elem).trigger("change");
        		}
        	});
        }

        var flag_obj = unescapeHTML("{{ json_encode(old('ddl_flag')) }}");
        if(flag_obj != "" && vesselname_obj != "null"){
        	var flag_json = JSON.parse(flag_obj);
        	var flag_counter = 0;
        	flag_json.forEach(function(elem, index){
        		if(elem != 0){
        			flag_counter++;
        			if(flag_counter > 1){
        				$("#btn_add_flag").trigger("click");
        			}

        			$(".row_flag:last").find("select").val(elem).trigger("change");
        		}
        	});
        }

        //Set restriction vessel type
        var vesseltype_restr = "{{ old('txt_vesseltype') }}";
        if(vesseltype_restr != ""){
        	$("select[name='txt_vesseltype']").val(vesseltype_restr).trigger("change");
        }

        //Set restriction vessel age
        var vesselage = "{{ old('txt_vesselage') }}";
        $("input[name='txt_vesselage']").val(vesselage).trigger("change");


    });
</script>
@stop