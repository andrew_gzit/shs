@extends("layouts.nav")
@section('page_title', 'Edit Company')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('MaintenanceController@index_company') }}">Companies</a></li>
	<li class="breadcrumb-item active">Edit Company</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Edit Company - <span class="f-s-18">{{ $company->name }}</span></h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<form method="POST" autocomplete="off" action="{{ action('MaintenanceController@update_company', $company->id) }}">
	@method('PUT')
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body p-25">
			<h4>Company Details</h4>
			<hr>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group m-b-15 {{ $errors->has('txt_name') ? 'has-error' : '' }}">
						<label>Name</label>
						<input type="text" class="form-control form-control-lg m-b-5" name="txt_name" value="{{ $company->name }}" placeholder="Enter Company Name">
						@if($errors->has('txt_name'))
						<span class="text-danger">{{ $errors->first('txt_name') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group m-b-15 {{ $errors->has('txt_roc') ? 'has-error' : '' }}">
						<label>ROC No.</label>
						<input type="text" data-type="alphadash" class="form-control form-control-lg m-b-5" name="txt_roc" value="{{ $company->roc_no }}" placeholder="Enter ROC No.">
						<span class="text-danger">{{ $errors->first('txt_roc') }}</span>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group m-b-15 {{ $errors->has('txt_code') ? 'has-error' : '' }}">
						<label>Code</label>
						<input type="text" class="form-control form-control-lg m-b-5" name="txt_code" value="{{ (!empty(old('txt_code')) ? old('txt_code') : $company->getCompanyOfType()->code) }}" placeholder="Enter Company Code">
						<span class="text-danger">{{ $errors->first('txt_code') }}</span>
					</div>
				</div>
			</div>
			<hr>

			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label>Address</label>
						<input type="text" autocomplete="something-new" id="txt_address_desc" class="form-control form-control-lg m-b-5" maxlength="45" placeholder="Enter Address Description">
						<input type="text" autocomplete="something-new" id="txt_address1" class="txt-address-required form-control form-control-lg m-b-5" maxlength="80" placeholder="Enter Address 1">
						<input type="text" autocomplete="something-new" id="txt_address2" class="form-control form-control-lg m-b-5" maxlength="80" placeholder="Enter Address 2">
						<input type="text" autocomplete="something-new" id="txt_address3" class="form-control form-control-lg" maxlength="80" placeholder="Enter Address 3">
					</div>
					<button type="button" id="btn_add_address" class="btn btn-primary">Add Address</button>
				</div>
				<div class="col-lg-6">
					<div id="div_addresses" class="row" style="max-height: 220px; overflow-y: auto">
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label>Email Address</label>
						<input type="text" id="txt_email" class="form-control form-control-lg m-b-5" placeholder="Enter Email Address">
					</div>
					<button type="button" id="btn_add_email" class="btn btn-primary">Add Email</button>
				</div>
				<div class="col-lg-6">
					<div id="div_emails" class="row" style="max-height: 220px; overflow-y: auto">
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group m-b-15 {{ $errors->has('txt_tel') ? 'has-error' : '' }}">
						<label>Telephone</label>
						<input type="text" maxlength="13" data-title="Telephone number is invalid." data-type="telephone" class="telephone form-control form-control-lg m-b-5" name="txt_tel" value="{{ (!empty(old('txt_tel')) ? old('txt_tel') : $company->getCompanyOfType()->telephone) }}" placeholder="Enter Telephone">
						<span class="text-danger">{{ $errors->first('txt_tel') }}</span>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group m-b-15 {{ $errors->has('txt_fax') ? 'has-error' : '' }}">
						<label>Fax</label>
						<input type="text" maxlength="13" data-type="telephone" data-title="Fax number is invalid." class="form-control form-control-lg m-b-5" name="txt_fax" value="{{ (!empty(old('txt_fax')) ? old('txt_fax') : $company->getCompanyOfType()->fax) }}"placeholder="Enter Fax">
						<span class="text-danger">{{ $errors->first('txt_fax') }}</span>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group m-b-15 {{ $errors->has('txt_pic') ? 'has-error' : '' }}">
						<label>Person In Charge</label>
						<input type="text" class="form-control form-control-lg m-b-5" name="txt_pic" value="{{ (!empty(old('txt_pic')) ? old('txt_pic') : $company->getCompanyOfType()->pic) }}" placeholder="Enter PIC">
						<span class="text-danger">{{ $errors->first('txt_pic') }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-inverse">
		<div class="panel-body p-25">
			<h4>Type of Company</h4>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="d-block">Select Company Type</label>
						<div class="checkbox checkbox-css checkbox-inline">
							<input type="checkbox" id="radio_shipper" name="radio_company_type[]" value="0" data-class="div_shipper" checked="" />
							<label for="radio_shipper">Shipper / Booking Party</label>
						</div>
						<div class="checkbox checkbox-css checkbox-inline">
							<input type="checkbox" id="radio_consignee" name="radio_company_type[]" value="1" data-class="div_consignee" />
							<label for="radio_consignee">Consignee</label>
						</div>
						<div class="checkbox checkbox-css checkbox-inline">
							<input type="checkbox" id="radio_agent" name="radio_company_type[]" value="3" data-class="div_agent" />
							<label for="radio_agent">Agent</label>
						</div>
					</div>
				</div>
			</div>

			<div id="div_settings" class="m-t-20">
				<h4>Settings</h4>
				<hr>
			</div>

					<!-- <div id="div_shipperdetails" style="display: none">
						<h4>Shipper / Booking Party Details</h4>
						<hr>
					</div> -->
					
					<div id="div_agent" class="div_companytype" style="display: none">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group m-b-15">
									<label>Port</label>
									<select name="txt_portid" class="select2 form-control form-control-lg">
										@foreach($ports AS $port)
										<option value="{{ $port->id }}" {{ ($port->id == $company->getCompanyOfType()->port_id) ? 'selected' : '' }}>{{ $port->code }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group m-b-15 {{ $errors->has('txt_portcode') ? 'has-error' : '' }}">
									<label>Port Code</label>
									<input type="text" class="form-control form-control-lg m-b-5" name="txt_portcode" value="{{ $company->port_code }}" placeholder="Enter Port Code">
									<span class="text-danger">{{ $errors->first('txt_portcode') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group m-b-15 {{ $errors->has('txt_bacode') ? 'has-error' : '' }}">
									<label>Agent Code</label>
									<input type="text" class="form-control form-control-lg m-b-5" name="txt_bacode" value="{{ $company->ba_code }}" placeholder="Enter Agent Code">
									<span class="text-danger">{{ $errors->first('txt_bacode') }}</span>
								</div>
							</div>
						</div>
					</div>
					<div id="div_shipperradio">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group m-b-15">
									<label>Handling Method</label>
									<select class="form-control form-control-lg m-b-5" name="txt_handlingmethod">
										<option>Direct Delivery</option>
										<option {{ ($company->getCompanyOfType()->handling_method == 'Via Shed') ? 'selected' : '' }}>Via Shed</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group m-b-15">
									<div class="checkbox checkbox-css checkbox-inline">
										<input type="checkbox" id="radio_voyageschedule" name="radio_update[]" value="1" {{ ($company->getCompanyOfType()->voyage_updates == '1') ? 'checked=""' : '' }}  />
										<label for="radio_voyageschedule">Receive Voyage Schedule Updates</label>
									</div>
									<div class="checkbox checkbox-css checkbox-inline">
										<input type="checkbox" id="radio_deliverynotice" name="radio_update[]" value="2" {{ ($company->getCompanyOfType()->delivery_updates == '1') ? 'checked=""' : '' }} />
										<label for="radio_deliverynotice">Receive Delivery Notice</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- <div id="div_shipperconsignee" class="div_companytype" style="display: none">
						<h4 class="m-t-20">Consignee Details</h4>
						<hr>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group m-b-15 {{ $errors->has('txt_consignee_code') ? 'has-error' : '' }}">
									<label>Code</label>
									<input type="text" class="form-control form-control-lg m-b-5" name="txt_consignee_code" value="{{ (!empty(old('txt_consignee_code')) ? old('txt_consignee_code') : ((!empty($consignee)) ? $consignee->code : '')) }}" placeholder="Enter Company Code">
									<span class="text-danger">{{ $errors->first('txt_consignee_code') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group m-b-15 {{ $errors->has('txt_consignee_tel') ? 'has-error' : '' }}">
									<label>Telephone</label>
									<input type="text" data-type="telephone" class="form-control form-control-lg m-b-5" name="txt_consignee_tel" value="{{ (!empty(old('txt_consignee_tel')) ? old('txt_consignee_tel') : ((!empty($consignee)) ? $consignee->telephone : '')) }}" placeholder="Enter Telephone">
									<span class="text-danger">{{ $errors->first('txt_consignee_tel') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group m-b-15 {{ $errors->has('txt_consignee_fax') ? 'has-error' : '' }}">
									<label>Fax</label>
									<input type="text" data-type="telephone" class="form-control form-control-lg m-b-5" name="txt_consignee_fax" value="{{ (!empty(old('txt_consignee_fax')) ? old('txt_consignee_fax') : ((!empty($consignee)) ? $consignee->fax : '')) }}" placeholder="Enter Fax">
									<span class="text-danger">{{ $errors->first('txt_consignee_fax') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group m-b-15 {{ $errors->has('txt_consignee_pic') ? 'has-error' : '' }}">
									<label>Person In Charge</label>
									<input type="text" class="form-control form-control-lg m-b-5" name="txt_consignee_pic" 
									value="{{ (!empty(old('txt_consignee_pic')) ? old('txt_consignee_pic') : ((!empty($consignee)) ? $consignee->pic : '')) }}" placeholder="Enter PIC">
									<span class="text-danger">{{ $errors->first('txt_consignee_pic') }}</span>
								</div>
							</div>
						</div>
					</div> -->
				</div>
			</div>

	<!-- <div class="panel panel-inverse">
		<div class="panel-body p-25">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group m-b-40">
								<label class="d-block">Select Company Type</label>
								<div class="radio radio-css radio-inline">
									<input type="radio" id="radio_shipper" name="radio_company_type" value="0" data-class="div_shipper" checked="" />
									<label for="radio_shipper">Shipper / Booking Party</label>
								</div>
								<div class="radio radio-css radio-inline">
									<input type="radio" id="radio_consignee" name="radio_company_type" value="1" data-class="div_consignee" />
									<label for="radio_consignee">Consignee</label>
								</div>
								<div class="radio radio-css radio-inline">
									<input type="radio" id="radio_shipperconsignee" name="radio_company_type" value="2" data-class="div_shipperconsignee" />
									<label for="radio_shipperconsignee">Shipper / Booking Party And Consignee</label>
								</div>
								<div class="radio radio-css radio-inline">
									<input type="radio" id="radio_agent" name="radio_company_type" value="3" data-class="div_agent" />
									<label for="radio_agent">Agent</label>
								</div>
							</div>
						</div>
					</div>
					<div id="div_shipperdetails" style="display: none">
						<h4>Shipper / Booking Party Details</h4>
						<hr>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group m-b-15 {{ $errors->has('txt_code') ? 'has-error' : '' }}">
								<label>Code</label>
								<input type="text" class="form-control form-control-lg m-b-5" name="txt_code" value="{{ $company->getCompanyOfType()->code }}" placeholder="Enter Company Code">
								<span class="text-danger">{{ $errors->first('txt_code') }}</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group m-b-15 {{ $errors->has('txt_tel') ? 'has-error' : '' }}">
								<label>Telephone</label>
								<input type="text" data-type="telephone" class="form-control form-control-lg m-b-5" name="txt_tel" value="{{ $company->getCompanyOfType()->telephone }}" placeholder="Enter Telephone">
								<span class="text-danger">{{ $errors->first('txt_tel') }}</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group m-b-15 {{ $errors->has('txt_pic') ? 'has-error' : '' }}">
								<label>Person In Charge</label>
								<input type="text" class="form-control form-control-lg m-b-5" name="txt_pic" value="{{ $company->getCompanyOfType()->pic }}" placeholder="Enter PIC">
								<span class="text-danger">{{ $errors->first('txt_pic') }}</span>
							</div>
						</div>
					</div>
					<div id="div_shipper" class="div_companytype">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group m-b-15">
									<label>Handling Method</label>
									<select class="form-control form-control-lg m-b-5" name="txt_handlingmethod">
										<option value="Container">Container</option>
										<option value="Conventional" {{ ($company->getCompanyOfType()->handling_method == 'Conventional') ? 'selected' : '' }}>Conventional</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group m-b-15">
									<div class="radio radio-css radio-inline">
										<input type="radio" id="radio_voyageschedule" name="radio_update" value="1" checked="" />
										<label for="radio_voyageschedule">Receive Voyage Schedule Updates</label>
									</div>
									<div class="radio radio-css radio-inline">
										<input type="radio" id="radio_deliverynotice" name="radio_update" value="2" 
										{{ ($company->getCompanyOfType()->delivery_updates == 1) ?  'checked=""' : "" }} />
										<label for="radio_deliverynotice">Receive Delivery Notice</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="div_agent" class="div_companytype" style="display: none">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group m-b-15">
									<label>Port</label>
									<select name="txt_portid" class="form-control form-control-lg">
										@foreach($ports AS $port)
										<option value="{{ $port->id }}" {{ ($port->id == $company->getCompanyOfType()->port_id) ? 'selected' : '' }} >{{ $port->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
					<div id="div_shipperradio" style="display: none">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group m-b-15">
									<label>Handling Method</label>
									<select class="form-control form-control-lg m-b-5" name="txt_handlingmethod">
										<option>Container</option>
										<option {{ ($company->getCompanyOfType()->handling_method == 'Conventional') ? 'selected' : '' }}>Conventional</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group m-b-15">
									<div class="radio radio-css radio-inline">
										<input type="radio" id="radio_voyageschedule" name="radio_update" value="1" checked="" />
										<label for="radio_voyageschedule">Receive Voyage Schedule Updates</label>
									</div>
									<div class="radio radio-css radio-inline">
										<input type="radio" id="radio_deliverynotice" name="radio_update" value="2" {{ ($company->getCompanyOfType()->delivery_updates == '1') ? 'checked=""' : '' }} />
										<label for="radio_deliverynotice">Receive Delivery Notice</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="div_shipperconsignee" class="div_companytype" style="display: none">
						<h4 class="m-t-20">Consignee Details</h4>
						<hr>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group m-b-15 {{ $errors->has('txt_code') ? 'has-error' : '' }}">
									<label>Code</label>
									<input type="text" class="form-control form-control-lg m-b-5" name="txt_consignee_code" value="{{ (!empty($consignee)) ? $consignee->code : '' }}" placeholder="Enter Company Code">
									<span class="text-danger">{{ $errors->first('txt_code') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group m-b-15 {{ $errors->has('txt_tel') ? 'has-error' : '' }}">
									<label>Telephone</label>
									<input type="text" data-type="telephone" class="form-control form-control-lg m-b-5" name="txt_consignee_tel" value="{{ (!empty($consignee)) ? $consignee->telephone : '' }}" placeholder="Enter Telephone">
									<span class="text-danger">{{ $errors->first('txt_tel') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group m-b-15 {{ $errors->has('txt_pic') ? 'has-error' : '' }}">
									<label>Person In Charge</label>
									<input type="text" class="form-control form-control-lg m-b-5" name="txt_consignee_pic" value="{{ (!empty($consignee)) ? $consignee->pic : '' }}" placeholder="Enter PIC">
									<span class="text-danger">{{ $errors->first('txt_pic') }}</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
-->

<div id="div_restrictionrow" class="panel panel-inverse">
	<div class="panel-body p-25">
		<h4>Restrictions</h4>
		<hr>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group m-b-0 vessel-group">
					<label>Vessel Name</label>
					<div class="row row_vessel">
						<div class="col-md-9">
							<select class="form-control form-control-lg ddl_vesselname m-b-10" name="ddl_vesselname[]"> 
								<option value="0">None</option>
								@foreach($vessels AS $ea)
								<option>{{ $ea->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-3 d-flex align-items-center">
							<div class='m-b-10'><label style="display: none" class='lbl-remove'>REMOVE</label></div>
						</div>
					</div>
					<button type="button" id="btn_add_vessel" class="btn btn-primary">Add Vessel</button>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group m-b-0">
					<label>Vessel Type</label>
					<select class="form-control form-control-lg ddl_restrictiontype" name="txt_vesseltype">
						<option value="0">None</option>
						<option>General Cargo Vessel</option>
						<option>Container Vessel</option>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group m-b-0">
					<label>Age (Less Than)</label>
					<input type="text" data-type="number" maxlength="2" value="{{ !empty($restrictions) ? ( ($restrictions->has('age')) ? $restrictions['age']->first()->value : '' ) : '' }}" name="txt_vesselage" class="form-control form-control-lg" placeholder="Enter Age (Less Than)">
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group m-b-0 flag-group">
					<label>Flag</label>
					<div class="row row_flag">
						<div class="col-md-9">
							<select class="form-control form-control-lg ddl_flag m-b-10" name="ddl_flag[]">
								<option selected value="0">None</option>
								<option>Australia</option>
								<option>Brazil</option>
								<option>Canada</option>
								<option>China</option>
								<option>Germany</option>
								<option>India</option>
								<option>Indonesia</option>
								<option>Italy</option>
								<option>Japan</option>
								<option>Malaysia</option>
								<option>Netherlands</option>
								<option>Saudi Arabia</option>
								<option>Singapore</option>
								<option>South Korea</option>
								<option>Spain</option>
								<option>Taiwan</option>
								<option>Thailand</option>
								<option>Turkey</option>
								<option>Vietnam</option>
							</select>
						</div>
						<div class="col-md-3 d-flex align-items-center">
							<div class='m-b-10'><label style="display: none" class='lbl-remove'>REMOVE</label></div>
						</div>
					</div>
					<button type="button" id="btn_add_flag" class="btn btn-primary">Add Flag</button>
				</div>
			</div>
		</div>
	</div>
</div>

<input type="text" hidden="" id="txt_update" name="txt_update">
<input type="text" hidden="" id="txt_addressArr" name="txt_addressArr">
<input type="text" hidden="" id="txt_emailArr" name="txt_emailArr">
<button type="button" class="btn btn-danger btn-cancel btn-action">Cancel</button>
<button id="btn_save" type="button" class="btn btn-primary btn-action btn-save" value="save">Save</button>
<button id="btn_proceed" type="button" class="btn btn-success btn-action pull-right btn-save" value="proceed">Save and Proceed to Charges</button>
<!-- @if($company->type != $company::type_shipper && $company->type != $company::type_shipperconsignee)
<a href="{{ action('MaintenanceController@index_company') }}"><button type="button"  class="btn btn-success btn-action pull-right">Return to Company List</button></a>
@else
<a href="{{ action('MaintenanceController@edit_companycharges', Hashids::encode($company->id)) }}"><button type="button" class="btn btn-success btn-action pull-right">Edit Company Charges</button></a>
@endif -->
<input type="text" hidden="" id="txt_submittype" name="txt_submittype">
</form>

<div class="modal" id="modal-edit-addr">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
					<input type="text" maxlength="45" id="txt_edit_address_desc" class="form-control form-control-lg m-b-5" placeholder="Enter Address Description">
					<input type="text" maxlength="80" id="txt_edit_address0" class="txt-edit-address-required form-control form-control-lg m-b-5" placeholder="Enter Address 1">
					<input type="text" maxlength="80" id="txt_edit_address1" class="form-control form-control-lg m-b-5" placeholder="Enter Address 2">
					<input type="text" maxlength="80" id="txt_edit_address2" class="form-control form-control-lg" placeholder="Enter Address 3">
				</div>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary pull-right" id="btn-edit-addr-save">Update Address</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modal-edit-email-addr">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
					<input type="text" id="txt_edit_email" class="form-control form-control-lg m-b-5" placeholder="Enter Email Address">
				</div>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary pull-right" id="btn-edit-email-addr-save">Update Email Address</button>
			</div>
		</div>
	</div>
</div>
@stop

@section("page_script")
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script type="text/javascript" src="/js/jquery.mask.js"></script>
<script type="text/javascript" src="/js/company_helper.js"></script>
<script type="text/javascript" src="/js/company.js"></script>

<script type="text/javascript">

	//Select2 auto position
	(function($) {

		var Defaults = $.fn.select2.amd.require('select2/defaults');

		$.extend(Defaults.defaults, {
			dropdownPosition: 'auto'
		});

		var AttachBody = $.fn.select2.amd.require('select2/dropdown/attachBody');

		var _positionDropdown = AttachBody.prototype._positionDropdown;

		AttachBody.prototype._positionDropdown = function() {

			var $window = $(window);

			var isCurrentlyAbove = this.$dropdown.hasClass('select2-dropdown--above');
			var isCurrentlyBelow = this.$dropdown.hasClass('select2-dropdown--below');

			var newDirection = null;

			var offset = this.$container.offset();

			offset.bottom = offset.top + this.$container.outerHeight(false);

			var container = {
				height: this.$container.outerHeight(false)
			};

			container.top = offset.top;
			container.bottom = offset.top + container.height;

			var dropdown = {
				height: this.$dropdown.outerHeight(false)
			};

			var viewport = {
				top: $window.scrollTop(),
				bottom: $window.scrollTop() + $window.height()
			};

			var enoughRoomAbove = viewport.top < (offset.top - dropdown.height);
			var enoughRoomBelow = viewport.bottom > (offset.bottom + dropdown.height);

			var css = {
				left: offset.left,
				top: container.bottom
			};

		    // Determine what the parent element is to use for calciulating the offset
		    var $offsetParent = this.$dropdownParent;

		    // For statically positoned elements, we need to get the element
		    // that is determining the offset
		    if ($offsetParent.css('position') === 'static') {
		    	$offsetParent = $offsetParent.offsetParent();
		    }

		    var parentOffset = $offsetParent.offset();

		    css.top -= parentOffset.top
		    css.left -= parentOffset.left;

		    var dropdownPositionOption = this.options.get('dropdownPosition');

		    if (dropdownPositionOption === 'above' || dropdownPositionOption === 'below') {
		    	newDirection = dropdownPositionOption;
		    } else {

		    	if (!isCurrentlyAbove && !isCurrentlyBelow) {
		    		newDirection = 'below';
		    	}

		    	if (!enoughRoomBelow && enoughRoomAbove && !isCurrentlyAbove) {
		    		newDirection = 'above';
		    	} else if (!enoughRoomAbove && enoughRoomBelow && isCurrentlyAbove) {
		    		newDirection = 'below';
		    	}

		    }

		    if (newDirection == 'above' || (isCurrentlyAbove && newDirection !== 'below')) {
		    	css.top = container.top - parentOffset.top - dropdown.height;
		    }

		    if (newDirection != null) {
		    	this.$dropdown
		    	.removeClass('select2-dropdown--below select2-dropdown--above')
		    	.addClass('select2-dropdown--' + newDirection);
		    	this.$container
		    	.removeClass('select2-container--below select2-container--above')
		    	.addClass('select2-container--' + newDirection);
		    }

		    this.$dropdownContainer.css(css);
		};
	})(window.jQuery);

	$(function(){

		if($(".has-error").length > 0){
			$(".has-error:first input").focus();
		}

		$(".select2").select2({
			width: '100%',
			dropdownPosition: 'above'
		});

		var vessel_type = "{{ !empty($restrictions) ? (($restrictions->has('type')) ? $restrictions['type']->first()->value : '') : '' }}";
		if(vessel_type != ""){
			$("select[name='txt_vesseltype']").val(vessel_type).trigger('change');
		}

		var vessel_name = "{{ !empty($restrictions) ? (($restrictions->has('name')) ? $restrictions['name'] : '') : '' }}";
		var vesselArr = unescapeHTML(vessel_name);
		if(vesselArr != ""){
			var vesselJson = JSON.parse(vesselArr);
			vesselJson.forEach(function(elem, index){
				if(index > 0){
					$("#btn_add_vessel").click();
				}
				$("select[name='ddl_vesselname[]']:last").val(elem.value).trigger("change");
			});
		}

		var flags = "{{ !empty($restrictions) ? (($restrictions->has('flag')) ? $restrictions['flag'] : '') : '' }}";
		var flagArr = unescapeHTML(flags);
		if(flagArr != ""){
			var flagJson = JSON.parse(flagArr);
			flagJson.forEach(function(elem, index){
				if(index > 0){
					$("#btn_add_flag").click();
				}
				$("select[name='ddl_flag[]']:last").val(elem.value).trigger("change");
			});
		}

		//Set old input
		var company_addr = unescapeHTML("{{ $company->getAddresses }}");
		if(company_addr != ""){
			setAddress(JSON.parse(company_addr));
		}

		var company_type = "";

		//Set company type
		var old_company_type = "{{ (!empty(old('radio_company_type'))) ? count(old('radio_company_type')) : '' }}";
		if(old_company_type != ""){
			if(old_company_type == 2){
				$("input[name='radio_company_type[]']:not([value='3'])").siblings("label").click();
			} else {
				old_company_type = "{{ old('radio_company_type')[0] }}";
				$("input[name='radio_company_type[]']:not([value='" + old_company_type + "'])").siblings("label").click();
				$("input[name='radio_company_type[]'][value='" + old_company_type + "']").siblings("label").click();
			}
		} else {
			company_type = "{{ $company->type }}";
			if(company_type != ""){
				if(company_type != 2){
					$("input[name='radio_company_type[]']:not([value='" + company_type + "'])").siblings("label").click();
					$("input[name='radio_company_type[]'][value='" + company_type + "']").siblings("label").click();
				} else {
					$("input[name='radio_company_type[]']:not([value='3'])").siblings("label").click();
				}
			}
		}

		var old_update = "{{ !empty(old('radio_update')) ? count(old('radio_update')) : '' }}";
		if(old_update != ""){
			if(old_update == 2){
				$("input[name='radio_update[]']").prop("checked", true);
			} else if(old_update == 1) {
				old_update = "{{ old('radio_update')[0] }}";
				$("input[name='radio_update[]'][value='" + old_update + "']").prop("checked", true);
			}
		} else {
			
			// $("input[name='radio_update[]']").prop("checked", false);
			switch(company_type){
				case "{{ \App\Company::type_shipper }}":
				if("{{ $company->getCompanyOfType()->delivery_updates }}" == 1){
					$("#radio_deliverynotice").prop("checked", true);
				} else if("{{ $company->getCompanyOfType()->voyage_updates }}" == 1){
					$("#radio_voyageschedule").prop('checked', true);
				} else {
					$("input[name='radio_update[]']").prop("checked", false);
				}
				break;
			}
		}

		function br2nl(str) {
			return str.replace(/<br\s*\/?>/mg,"\n");
		}

		@if($company->isShipper())
		@if($company->getCompanyOfType()->voyage_updates == 1)

		@endif
		@if($company->getCompanyOfType()->delivery_updates == 1)
		@endif
		@endif


		var email_obj = unescapeHTML("{{ $company->getEmails }}");
		if(email_obj != ""){
			setEmail(JSON.parse(email_obj));
		}
		// email_obj = unescapeHTML(email_obj.replace(/&quot;/g, '\"'));
		// console.log(email_obj);
		// if(email_obj != ""){
		// 	var email_json = JSON.parse(email_obj);
		// 	email_json.forEach(function(elem, index){
		// 		console.log(elem);
		// 		var html = "<div class='col-lg-3 col-md-6'><div class='email-adrs'><span>" + elem.email + "</span><div class='adrs-del'><i class='fas fa-times'></i></div></div></div>";
		// 		$("#div_emails").append(html);
		// 	});
		// }

		var restr_obj = "{{ $company->getCompanyOfType()->getRestrictions }}";
		restr_obj = unescapeHTML(restr_obj.replace(/&quot;/g, '\"'));
		if(restr_obj != ""){
			var restr_json = JSON.parse(restr_obj);
			restr_json.forEach(function(elem, index){
				if(index == 0){
					$(".div_restriction").find("input").val(elem.value);
				} else {
					$("#btn_add_restriction").trigger("click");
					var $div_restr = $(".div_restriction:last");
					$div_restr.find(".ddl_restrictiontype").val(elem.type).trigger("change");
					if(elem.type == "name" || elem.type == "age"){
						$div_restr.find("input:last").val(elem.value);
					} else {
						$div_restr.find("input:last").hide();
						$div_restr.find("select:last").show().val(elem.value).trigger("change");
					}
				}
			});
		}

		var addressCount = $(".adrs-row").length;
		addressCount = 0 ? 1 : addressCount;
		$("#txt_address_desc").val("BRANCH " + addressCount);

	});
</script>
@stop