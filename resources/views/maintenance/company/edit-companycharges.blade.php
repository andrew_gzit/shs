@extends("layouts.nav")
@section('page_title', "Edit Company's Default Charges - $company->name")

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('MaintenanceController@index_company') }}">Companies</a></li>
	<li class="breadcrumb-item active">Edit Company's Default Charges</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Edit Company's Default Charges - <span class="f-s-18">{{ $company->name }}</span></h1>

@if(session('success'))
<div class="alert alert-green">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('success') }}
</div>
@endif

<div id="accordion" class="card-accordion">
	<div class="card">
		<div class="card-header bg-black text-white pointer-cursor" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true">
			Company's Default Charges
		</div>
		<div id="collapseOne" class="collapse show" data-parent="#accordion">
			<div class="card-body p-20">

				@if(count($defaultCharges) > 0)
				<div class="table-responsive">
					<table id="tbl_charges" class="table table-bordered table-striped table-condensed table-valign-middle m-b-0 f-s-14 ">
						<thead>
							<tr>
								<th>Charge</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($defaultCharges AS $c)
							<tr>
								<td>{{ $c->getCharge->code . ' - ' . $c->getCharge->name }}</td>
								<td width="1%">
									<button type="button" data-chargeid="{{ $c->charge_id }}" class="btn btn-success btn-view"><i class="fa fa-eye m-r-5"></i>View</button>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				@else
				<p class="text-center m-t-10 m-b-10 f-s-14">No charges has been added yet!</p>
				@endif



				<!-- <div class="table-responsive">
					<table id="tbl_charges" class="table table-bordered table-striped table-condensed table-valign-middle m-b-0 f-s-14 ">
						<thead>
							<tr>
								<th>Charge</th>
								<th>Type</th>
								<th width="1%" class="no-wrap">POL</th>
								<th width="1%" class="no-wrap">FPD</th>
								<th width="1%" class="no-wrap">Amount (RM)</th>
								<th width="1%" class="no-wrap">Payment</th>
								<th width="1%" class="no-wrap"></th>
							</tr>
						</thead>
						<tbody>
							@foreach($defaultCharges AS $charge)
							<tr class="charge_row" data-id="{{ $charge->id }}" data-pol="{{ $charge->pol }}" data-final-dest="{{ $charge->final_dest }}">
								<td class="charge_code no-wrap"><strong>{{ $charge->getCharge->code }}</strong> - {{ $charge->getCharge->name }}</td>
								<td class="charge_type no-wrap">{{ $charge->chargeDetailText() }}</td>
								<td class="charge_pol">{{ $charge->getPort($charge->pol) }}</td>
								<td class="charge_final">{{ $charge->getPort($charge->final_dest) }}</td>
								<td class="charge_price">{{ number_format($charge->price, 2) }}</td>
								<td class="charge_payment">{{ ($charge->payment == 'P') ? "Prepaid" : "Collect" }}</td>
								<td width="1%" class="no-wrap">
									<div class="d-flex">
										<button type="button" class="btn btn-success m-r-5 btn-edit"><i class="fa fa-edit"></i></button>
										<button type="button" class="btn btn-danger btn-delete"><i class="fa fa-trash"></i></button>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>				
					</table>
				</div>
			-->
		</div>
	</div>
</div>
</div>

<form method="POST" id="form_charge" action="{{ action('MaintenanceController@update_companycharges', $company->id) }}">
	@method('PUT')
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body p-20">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Charge Code</label>
						<select id="ddl_chargecode" class="form-control form-control-lg" name="ddl_chargecode">
							@foreach($charges AS $charge)
							<option value="{{ $charge->id }}">{{ $charge->code }} - {{ $charge->name }}</option>
							@endforeach
						</select>
					<!-- 	<div class="checkbox checkbox-css checkbox-inline">
							<input type="checkbox" id="chk_port" name="chk_port"/>
							<label for="chk_port">Unaffected by POL / FPD</label>
						</div> -->
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Port of Loading</label>
						<select id="ddl_pol" class="form-control form-control-lg ddl_port" name="ddl_pol">
							<option value="-">None</option>
							@foreach($ports AS $port)
							<option value="{{ $port->id }}">{{ $port->code }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Final Port of Discharge</label>
						<select id="ddl_final" class="form-control form-control-lg ddl_port" name="ddl_final">
							<option value="-">None</option>
							@foreach($ports AS $port)
							<option value="{{ $port->id }}">{{ $port->code }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>Payment</label>
						<select class="form-control form-control-lg" name="ddl_payment">
							<option value="P">Prepaid</option>
							<option value="C">Collect</option>
						</select>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-2 col-6">
					<div class="form-group m-b-0">
						<label>Amount</label>
					</div>
				</div>
				<div class="col-md-2 col-6">
					<div class="form-group m-b-0">
						<label>Type</label>
					</div>
				</div>
				<div class="col-md-6 col-6">
					<div class="form-group m-b-0">
						<label>Vessel</label>
					</div>
				</div>
			</div>
			<div class="row div-charges">
				<div class="col-md-2 col-4">
					<div class="form-group">
						<input type="number" class="form-control form-control-lg m-b-5 inp_num" name="txt_amount[]" placeholder="Enter Amount">
					</div>
				</div>
				<div class="col-md-2 col-4">
					<div class="form-group">
						<select class="form-control form-control-lg ddl_unit" name="ddl_unit[]">
							<option></option>
						</select>
					</div>
				</div>
				<div class="col-md-6 col-4">
					<div class="form-group">
						<select class="form-control form-control-lg ddl_vessel" name="ddl_vessel[]" multiple="multiple">
							@foreach($vessels AS $v)
							<option value="{{ $v->id }}">{{ $v->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class='col-md-1 d-flex' style="align-items: center">
					<div class='form-group'>
						<label class='lbl-remove' style="display: none">REMOVE</label>
					</div>
				</div>
			</div>
			<!-- <div class="row div-charges">
				<div class="col-md-4 col-lg-3">
					<div class="form-group m-b-15">
						<select name="ddl_charge[]" class="form-control form-control-lg">
							@foreach($charges AS $parent)
							<optgroup label="({{ $parent->code }}) {{ $parent->name }}">

								@foreach($parent->getCharges AS $child)
								<option value="{{ $child->id }}">({{ $parent->code }}) RM{{ $child->unit_price }} {{ ($child->unit_size == 1) ? 'PER' : 'PER ' . $child->unit_size }} {{ $child->unit }}</option>
								@endforeach

							</optgroup>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-4 col-lg-3">
					<div class="form-group m-b-15">
						<input type="text" data-type="number" class="form-control form-control-lg m-b-5" name="txt_amount[]" placeholder="Enter Price">
					</div>
				</div>
				<div class='col-lg-1'><div class='form-group'><label style="display: none" class='lbl-remove'>REMOVE</label></div></div>
			</div> -->
			<button type="button" id="btn_add_charges" class="btn btn-primary">Add Charges</button>
		</div>
	</div>

	<input hidden="" type="text" name="txt_charge" id="txt_charge">
	<button id="btn_return" type="button" class="btn btn-success btn-action pull-right">Return to Company List</button>
	<button id="btn_save" type="button" class="btn btn-primary btn-action">Save</button>
</form>

<div class="modal fade" id="modal-view">
	<div class="modal-dialog" style="max-width: 1000px">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="view-title"></h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">

				<ul id="view-pills" class="nav nav-pills"></ul>
				<div id="tab-content" class="tab-content p-0"></div>

			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-edit">
	<div class="modal-dialog" style="max-width: 1000px">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Charge</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Charge Code</label>
							<input type="text" readonly="true" id="txt_chargecode" class="form-control form-control-lg">
							<!-- <div class="checkbox checkbox-css checkbox-inline">
								<input type="checkbox" id="chk_edit_port" name="chk_edit_port"/>
								<label for="chk_edit_port">Unaffected by POL / FPD</label>
							</div> -->
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Type</label>
							<input type="text" readonly="true" id="txt_chargetype" class="form-control form-control-lg">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Port of Loading</label>
							<select class="form-control form-control-lg ddl_edit_port" id="ddl_edit_pol" name="ddl_edit_pol">
								<option value="-">None</option>
								@foreach($ports AS $port)
								<option value="{{ $port->id }}">{{ $port->code }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Final Port of Discharge</label>
							<select class="form-control form-control-lg ddl_edit_port" id="ddl_edit_final" name="ddl_edit_final">
								<option value="-">None</option>
								@foreach($ports AS $port)
								<option value="{{ $port->id }}">{{ $port->code }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Payment</label>
							<select id="ddl_edit_payment" class="form-control form-control-lg">
								<option value="P">Prepaid</option>
								<option value="C">Collect</option>
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Amount</label>
							<input type="number" class="form-control form-control-lg inp_num" id="txt_edit_amount" name="txt_edit_amount" placeholder="Enter Amount">
						</div>
					</div>
				</div>

				<div class="form-group">
					<label>Vessel</label>
					<select class="form-control form-control-lg ddl_vessels" id="ddl_edit_vessel" name="ddl_vessel_edit[]" multiple="multiple">
						@foreach($vessels AS $v)
						<option value="{{ $v->id }}">{{ $v->name }}</option>
						@endforeach
					</select>
				</div>

			</div>
			<div class="modal-footer">
				<input type="hidden" id="txt_chargeid">
				<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
				<button type="button" id="btn-edit-save" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</div>
</div>
@stop

@section("page_script")
<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
<link href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css" rel="stylesheet" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<link rel="stylesheet" href="/css/BsMultiSelect.min.css">
<script type="text/javascript" src="/js/BsMultiSelect.min.js"></script>

<script type="text/javascript">
	$(function(){

		var shipper_id = "{{ $shipper_id }}";
		var editingRow;

		$("#ddl_chargecode").select2();

		// View charges
		$(document).on("click", ".btn-view", function(){
			var charge_id = $(this).attr("data-chargeid");

			$.get("/maintenance/viewDefaultCharge/" + shipper_id + "/" + charge_id, function(response){
				console.log(response);
				if(response.success){
					var pills = response.pills;
					var data = response.data;

					var pills_html = "";
					var tab_html = "";

					$("#view-title").text(response.charge);

					// Build nav pills
					pills.forEach(function(elem, index){
						var active = "";

						if(index == 0){
							active = "active"
						}

						pills_html += "<li class='nav-items'>" +
						"<a href='#nav-pills-tab-" + index + "' data-toggle='tab' class='nav-link " + active + "'>" +
						"<span class='d-sm-block d-none'>" + elem + "</span>" +
						"</a></li>";
					});

					// Loop grouped by FPD first
					var counter = 0;

					$.each(data, function(index, elem){
						var div = '<div class="tab-pane fade ';

						if(counter == 0){
							div += 'active show"';
						} else {
							div += '"';
						}

						console.log('elemhere');
						console.log(elem);

						

						tab_html += div + ' id="nav-pills-tab-' + counter + '"><table class="table table-bordered table-valign-middle f-s-14">' +
						'<tr><th width="1%">POL</th><th width="1%">Unit</th>' +
						'<th width="1%" class="no-wrap">Amount (RM)</th><th width="1%">Payment</th><th>Vessel</th><th width="1%"></th></tr>';

						// Loop charges for selected FPD
						$.each(elem, function(index, charge){

							console.debug(index);
							console.log(charge);

							tab_html += '<tr data-charge-id="' + charge.id + '">' +
							'<td>' + charge.pol_code + '</td>' +
							'<td class="no-wrap">' + charge.unit_text + '</td>' +
							'<td>' + charge.price + '</td>' +
							'<td>' + charge.payment + '</td>' +
							'<td>' + charge.vessel_text + '</td>' +
							'<td width="1%" class="no-wrap">' + 
							'<button type="button" class="btn btn-success btn-edit-charge mr-2">Edit</button>' +
							'<button type="button" class="btn btn-danger btn-delete-charge">Delete</button>' +
							'</td>' +
							'</tr>';
						});

						// Close table and tab
						tab_html += "</table></div>";

						counter++;

					});

					$("#tab-content").html(tab_html);
					$("#view-pills").html(pills_html);

					$("#modal-view").modal("toggle");
				}
			});
		});


		$(document).on("click", ".btn-edit-charge", function(){
			var tr = $(this).closest("tr");
			editingRow = tr;
			var charge_id = tr.attr("data-charge-id");
			$.get("/maintenance/getDefaultCharge/" + charge_id, function(response){
				if(response.success){
					var data = response.data;
					var vessels = data.vessels;

					$("select[name='ddl_vessel_edit[]']").bsMultiSelect("Dispose");
					$("select[name='ddl_vessel_edit[]']").removeAttr("multiple");
					$("select[name='ddl_vessel_edit[]']").val([]);

					$("select[name='ddl_vessel_edit[]']").attr("multiple", "multiple");

					vessels.forEach(function(elem, index){
						$("select[name='ddl_vessel_edit[]']").find("option[value='" + elem.id + "']").prop('selected', true);
					});

					$("select[name='ddl_vessel_edit[]']").bsMultiSelect({
						useCss: true
					});

					if(data.pol != null){
						$("#ddl_edit_pol").val(data.pol);
						$("#ddl_edit_final").val(data.final_dest);
					} else {
						$("#ddl_edit_pol, #ddl_edit_final").val("-");
					}

					$("#txt_chargecode").val(data.charge_code);
					$("#txt_chargetype").val(data.unit_text);
					$("#txt_edit_amount").val(data.price.replace(/,/g, ''));
					$("#txt_chargeid").val(charge_id);

					$("#modal-edit").modal("toggle");

				}
			});
		});

		$(document).on("click", ".btn-delete-charge", function(){
			var $row = $(this).closest("tr");
			var charge_id = $row.attr("data-charge-id");
			swal({
				title: "Confirm delete?",
				text: "Press Continue to proceed",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Continue',
				cancelButtonText: 'Cancel',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					$.ajax({
						type: "DELETE",
						url: "/default-charge/" + charge_id,
						success: function(data){
							console.log(data);
							if(data.success){

								$("#modal-view").modal('toggle');
								swal("Delete successful", "Selected default charge has been deleted!", "success");
								setTimeout(function(){
									$("button[data-chargeid='" + data.charge_id + "']").click();
								},500);
							}
						},
						error: function(data){
							swal("Error", "Delete unsuccessful", "error");	
						}
					});
				}
			});
		});

		$(".ddl_vessel").bsMultiSelect({
			useCss: true
		});

		$(document).on("blur", ".inp_num", function(){
			var value = $(this).val();
			if(!value.match(/^[0-9]\d*(\.)?\d{0,2}$/)){
				$(this).val("");
			}
		});

		$(".ddl_port").select2();

		var $edit_row = "";

		$(document).on("click", ".btn-edit", function(){
			var $row = $(this).closest("tr");
			var charge_code = $row.find(".charge_code").text();
			var charge_type = $row.find(".charge_type").text();
			var price = $row.find(".charge_price").text();
			var pol = $row.attr("data-pol");
			var final_dest = $row.attr("data-final-dest");
			var charge_payment = $row.find(".charge_payment").text();

			$("#txt_chargecode").val(charge_code);
			$("#txt_chargetype").val(charge_type);

			if(pol != ""){
				$("#ddl_edit_pol").val(pol).trigger("change");
				$("#ddl_edit_final").val(final_dest).trigger("change");
			} else {
				$("#chk_edit_port").siblings("label").click();
			}
			
			$("#txt_edit_amount").val(price);
			$("#ddl_edit_payment").val(charge_payment[0]);

			$edit_row = $row;

			$("#modal-edit").modal("toggle");
		});

		$("#btn-edit-save").click(function(){
			var new_pol = "-";
			var new_final = "-";

			var new_pol_text = "-";
			var new_final_text = "-";

			var valid = true;

			// var checkbox_status = $("#chk_edit_port").is(":checked");

			var edit_pol =  $("#ddl_edit_pol").val();
			var edit_final = $("#ddl_edit_final").val();

			if(edit_pol !== '-' && edit_final !== '-'){
				if(edit_pol == edit_final){
					swal("Oops!", "Final Port of Discharge and Port of Loading cannot be the same!", "warning");
					return;
				}
			} else if(edit_pol == '-' ^ edit_final == '-'){
				swal("Oops!", "One of the port is not selected yet!", "warning");
				return;
			}

			
			new_pol_text = $("#ddl_edit_pol").find("option:selected").text();
			new_final_text = $("#ddl_edit_final").find("option:selected").text();

			if(new_pol_text == "None") {
				new_pol_text = "-";
			}

			if(new_final_text == "None") {
				new_final_text = "-";
			}

			// if(new_pol == new_final){
			// 	swal("Oops!", "Final Port of Discharge and Port of Loading cannot be the same!", "warning");
			// 	return;
			// }

			var new_price = $("#txt_edit_amount").val();
			if(new_price == ""){
				swal("Oops!", "Charge amount cannot be empty!", "warning");
				return;
			}

			var charge_id = $("#txt_chargeid").val();

			if(valid){
				$.ajax({
					type: "POST",
					url: "/default-charge/edit/" + charge_id,
					data: { 
						pol: edit_pol, 
						final_dest: edit_final,
						amount: new_price,
						payment: $("#ddl_edit_payment").val(),
						vessel: $("#ddl_edit_vessel").val()
					}, 
					success: function(data) {
						if(data.success){

							swal("Success", "Charge detail has been updated.", "success");
							$("#modal-view").modal('toggle');
							setTimeout(function(){
								$("button[data-chargeid='" + data.charge_id + "']").click();
							},500);

						} else {
							swal("Error", "Update failed. Please try again.", "error");
						}
					}
				});
			}

			// var new_payment = $("#ddl_edit_payment").find("option:selected").text();

			// $edit_row.find(".charge_pol").text(new_pol_text);
			// $edit_row.find(".charge_final").text(new_final_text);
			// $edit_row.find(".charge_price").text(new_price);
			// $edit_row.find(".charge_payment").text(new_payment);

			$("#modal-edit").modal("toggle");
		});

		$(document).on('hide.bs.modal','#modal-edit', function () {
			if($("#chk_edit_port").is(":checked")){
				$("#chk_edit_port").siblings("label").click();
			}
			$("#ddl_edit_port").removeAttr("disabled");
		})

		$("#chk_edit_port").change(function(){
			if($(this).is(":checked")){
				$(".ddl_edit_port").attr("disabled", "");
			} else {
				$(".ddl_edit_port").removeAttr("disabled");
			}
		});

		// var tbl_charges = $("#tbl_charges").DataTable({
		// 	"pageLength": 5,
		// 	"columnDefs": [
		// 	{ "orderable": false, "targets": [1,2,3,4,5,6] }
		// 	]
		// });

		$("#ddl_chargecode").change(function(){
			var charge_id = $(this).val();
			var url = "/charge-detail/" + charge_id;
			$.get(url, function(response){
				var optionArr = "";
				$.each(response, function(name, value) {
					optionArr += "<optgroup label='" + name + "'>";
					value.forEach(function(element, index){
						optionArr += "<option value='" + element.unit_size + "-" + element.unit + "'>" + element.text + "</option>";
					});
					optionArr += "</optgroup>";
				});

				$(".ddl_unit").each(function(){
					$(this).find("option, optgroup").remove();
					$(this).append(optionArr);
				});
			});
		});

		$("#ddl_chargecode").trigger("change");

		$("#chk_port").change(function(){
			if($(this).is(":checked")){
				$(".ddl_port").attr("disabled", "");
			} else {
				$(".ddl_port").removeAttr("disabled");
			}
		});

		$("input").attr("autocomplete", "off");

		$(document).on("keydown", "input[data-type='number']", function(e){
			if(!((e.keyCode > 95 && e.keyCode < 106) || (e.keyCode > 47 && e.keyCode < 58) || e.keyCode == 8 || e.keyCode == 9) ) {
				e.preventDefault();
				return false;
			}
		});

		$("#btn_add_charges").click(function(){
			$clone = $(".div-charges:first").clone();
			$clone.find("input").val("");
			$clone.find(".lbl-remove").show();

			$clone.find(".dashboardcode-bsmultiselect")
			.remove();

			var count = $(".div-charges").length;

			$clone.find(".ddl_vessel")
			.bsMultiSelect({
				useCss: true,
				containerClass: 'dashboardcode-bsmultiselect ms_' + count,
			});

			$clone.find(".ddl_vessel").attr("name", "ddl_vessel" + count + "[]");

			$(".div-charges:last").after($clone);

			$clone.find("input[type='number']").focus();

			if($(".div-charges").length > 1){
				$(".lbl-remove:first").show();
			}
		});

		$(document).on("click", ".lbl-remove", function(){
			$(this).closest(".div-charges").remove();

			if($(".div-charges").length > 1){
				$(".lbl-remove:first").show();
			} else {
				$(".lbl-remove:first").hide();
			}
		});

		$("#btn_save").click(function(){
			var fpd = $("#ddl_final").val();
			var pol = $("#ddl_pol").val();

			if(pol !== '-' && fpd !== '-'){
				if(pol == fpd){
					swal("Oops!", "Final Port of Discharge and Port of Loading cannot be the same!", "warning");
					return;
				}
			} else if(pol == '-' ^ fpd == '-'){
				swal("Oops!", "One of the port is not selected yet!", "warning");
				return;
			}

			var chargeArr = [];
			var valid = true;
			var submitForm = true;
			$(".div-charges").each(function(){
				var obj = {};

				obj.type = $(this).find(".ddl_unit").val();
				obj.price = $(this).find("input").val();
				obj.vessel = $(this).find(".ddl_vessel").val();

				chargeArr.forEach(function(elem){
					if($(elem.vessel).filter(obj.vessel).length == 1){
						swal("Oops!", "Duplicate vessel found.", "warning");
						valid = false;
						return false;
					}

					// if(elem.type == obj.type){
					// 	swal("Oops!", "Duplicate charge type found.", "warning");
					// 	valid = false;
					// 	return false;
					// }
				});

				if(obj.price == ""){
					$(this).find("input[type='number']").focus();
					valid = false;
				}

				if(!valid){
					submitForm = false;
					return false;
				} else {
					chargeArr.push(obj);
				}
			});

			var emptyArr = false;
			$(".ddl_vessel").each(function(){
				if($(this).val().length == 0 && !emptyArr){
					emptyArr = true;
				} else if(emptyArr) {
					swal("Oops!", "Duplicate charges found.", "warning");
					submitForm = false;
					return false;
				}
			});

			if(submitForm){
				$("#txt_charge").val(JSON.stringify(chargeArr));
				$("#form_charge").submit();				
			}
		});

		// var company_charges = <?php echo (!empty($company->getCompanyOfType()->getDefaultCharges)) ? $company->getCompanyOfType()->getDefaultCharges->toJson() : ''; ?>;

		// company_charges.forEach(function(elem, index){
		// 	if(index > 0){
		// 		$("#btn_add_charges").click();
		// 	}
		// 	$("select[name='ddl_charge[]']:last").val(elem.chargedetail_id);
		// 	$("input[name='txt_amount[]']:last").val(elem.price);
		// });

		// console.log(company_charges);

		$("#btn_return").click(function(){
			if($(".div-charges").length > 1){

				swal({
					title: "Are you sure?",
					text: "Press Continue to proceed to Companies page",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'Continue',
					cancelButtonText: 'Cancel',
					reverseButtons: true
				}).then((result) => {
					if (result.value) {
						window.location.replace('{{ action('MaintenanceController@index_company') }}');
					}
				});
			} else {
				window.location.replace('{{ action('MaintenanceController@index_company') }}');
			}
		});

		$(document).on("blur", "input[data-type='number']", function(e){
			var price = parseInt($(this).val());
			if(!isNaN(price) && price != 0){
				$(this).val(price.toFixed(2));
			} else {
				$(this).val("");
			}
		});

	});
</script>
@stop