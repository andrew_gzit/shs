@extends("layouts.nav")
@section('page_title', 'Companies')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item">
	<!-- 	<button id="filter" type="button" class="btn btn-primary m-r-10">
			<i class="fa fa-sliders-h m-r-5"></i>
			Filter
		</button> -->
		<a href="{{ action('MaintenanceController@create_company') }}"><button type="button" class="btn btn-primary"><i class="fa fa-plus m-r-10"></i>Create Company</button></a>
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">
	Companies
</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<div id="div_filter" class="panel panel-inverse">
	<form method="GET">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="d-block">Company Type</label>
						<div class="checkbox checkbox-css checkbox-inline">
							<input type="checkbox" id="radio_shipper" name="company_type" value="0" checked="" />
							<label for="radio_shipper">Shipper / Booking Party</label>
						</div>
						<div class="checkbox checkbox-css checkbox-inline">
							<input type="checkbox" id="radio_consignee" name="company_type" value="1" />
							<label for="radio_consignee">Consignee</label>
						</div>
						<div class="checkbox checkbox-css checkbox-inline">
							<input type="checkbox" id="radio_agent" name="company_type" value="3" />
							<label for="radio_agent">Agent</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row justify-content-center">
				<div class="col-lg-2">
					<button type="button" onclick="window.location = window.location.pathname;" class="btn btn-default btn-block">Reset</button>
				</div>
				<div class="col-lg-2">
					<button id="btn_filter" type="button" class="btn btn-primary btn-block">Apply Filter</button>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="table-responsive">
	<table id="table_company" class="table table-bordered table-valign-middle f-s-14">
		@if($companies->isNotEmpty())
		<thead class="thead-custom">
			<tr>
				<th width="1%" class="no-wrap">Code</th>
				<th>Name</th>
				<th>Address</th>
				<th width="1%" class="no-wrap">Type</th>
				<th width="1%" class="no-wrap">Options</th>
			</tr>
		</thead>
		<tbody class="bg-white">
			@foreach($companies AS $key => $comp)
			<tr>
				<td>{{ $comp->getCompanyOfType()->code }}</td>
				<td class="no-wrap">{{ $comp->name }}</td>
				<td>
					@foreach($comp->getAddresses AS $add)
					<span class="d-block"><strong>{{ $add->description }}</strong> {{ str_replace("<br>", " ", $add->address) }}</span>
					@endforeach
				</td>
				<td class="no-wrap">{{ $comp->getStatus() }}</td>
				<td>
					<div class="no-wrap">
						<a href="{{ action('MaintenanceController@edit_company', Hashids::encode($comp->id)) }}" class="btn btn-success p-r-10">
							<i class="fa fa-edit"></i>
						</a>
						<form method="POST" class="d-inline">
							@method('DELETE')
							@csrf
							<button type="button" data-id="{{ $comp->id }}" class="btn btn-danger btn-delete">
								<i class="fa fa-trash"></i>
							</button>
						</form>
						@if($comp->type == $comp::type_shipper || $comp->type == $comp::type_shipperconsignee)
						<a href="{{ action('MaintenanceController@edit_companycharges', Hashids::encode($comp->id)) }}" class="btn btn-warning btn-charges">
							<i class="fas fa-dollar-sign"></i>
						</a>
						@endif
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
		@else
		<tbody class="bg-white">
			<tr class="text-center">
				<td>No companies to display.</td>
			</tr>
		</tbody>
		@endif
	</table>
</div>
@stop

@section("page_script")
<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<link href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css" rel="stylesheet" />

<script type="text/javascript">
	$(function(){

		$("#btn_filter").click(function(){
			if($("#radio_shipper").is(":checked") && $("#radio_consignee").is(":checked")){
				$("#radio_shipper").val(2);
				$("#radio_consignee").removeAttr("name");
			}
			$(this).closest("form").submit();
		});

		var filter_type = "{{ $filter_type }}";
		if(filter_type === ""){
			$("#div_filter").hide();
		} else {
			$("input[name='company_type']").prop("checked", false);
			if(filter_type == 2){
				$("input[name='company_type']:not([value='3'])").prop("checked", true);
			} else {
				$("input[name='company_type'][value='" + filter_type + "']").prop("checked", true);
			}
			$("#div_filter").show();
		}

		$("#filter").click(function(){
			$("#div_filter").slideToggle();
		});

		$("input[name='company_type']").change(function(e){
			var $checked = $("input[name='company_type']:checked");
			if($checked.length == 0){
				$(this).prop("checked", true);
			}

			var company_type = $(this).val();

			switch(company_type){
				case "0":
				$("input[name='company_type'][value='3']").prop("checked", false);
				break;
				case "1":
				$("input[name='company_type'][value='3']").prop("checked", false);
				break;
				case "3":
				$("input[name='company_type']:not([value='3'])").prop("checked", false);
				break
			};
		});

		$("#table_company").DataTable({
			"columnDefs": [
			{ "orderable": false, "targets": [0,2,3,4] }
			],
			"order": [[ 1, "asc" ]],
		});

		$(document).on("click", ".btn-delete", function(){
			var $btn = $(this);
			var company_id = $(this).attr("data-id");
			swal({
				title: "Confirm delete?",
				text: "Press Continue to proceed",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Continue',
				cancelButtonText: 'Cancel',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					$.get('/check-company-delete/' + company_id, function(data){
						if(data.success){
							var url = "/maintenance/companies/delete/" + company_id;
							$btn.closest("form").attr("action", url).submit();
						} else {
							console.log(data);
							var text = data.msg;
							swal("Delete unsuccessful!", data.msg, "warning");
						}
					});
				}
			});
		});
	});
</script>
@stop