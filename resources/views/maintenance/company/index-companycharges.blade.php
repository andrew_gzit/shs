@extends("layouts.nav")
@section('page_title', "Create Company's Default Charges")

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
	<li class="breadcrumb-item"><a href="javascript:;">Companies</a></li>
	<li class="breadcrumb-item active">Create Company</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Create Company's Default Charges</h1>

@if(session('success'))
<div class="alert alert-green">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('success') }}
</div>
@endif

<form method="POST" id="form_charge" action="{{ action('MaintenanceController@store_companycharges', $company->id) }}">
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body p-25">
			<div class="row">
				<div class="col-md-2">
					<div class="form-group m-0">
						<label>Charge Type</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group m-0">
						<label>Price</label>
					</div>
				</div>
			</div>
			<div class="row div-charges">
				<div class="col-md-2">
					<div class="form-group m-b-15">
						<select name="ddl_charge[]" class="form-control form-control-lg">
							@foreach($charges AS $parent)
							<optgroup label="({{ $parent->code }}) {{ $parent->name }}">

								@foreach($parent->getCharges AS $child)
								<option value="{{ $child->id }}">({{ $parent->code }}) RM{{ $child->unit_price }} {{ ($child->unit_size == 1) ? 'PER' : 'PER ' . $child->unit_size }} {{ $child->unit }}</option>
								@endforeach

							</optgroup>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group m-b-15">
						<input type="text" data-type="number" class="form-control form-control-lg m-b-5" name="txt_amount[]" placeholder="Enter Price">
					</div>
				</div>
				<div class='col-lg-1'><div class='form-group'><label style="display: none" class='lbl-remove'>REMOVE</label></div></div>
			</div>
			<button type="button" id="btn_add_charges" class="btn btn-primary btn-action">Add Charges</button>
		</div>
	</div>

	<input hidden="" type="text" name="txt_charge" id="txt_charge">
	<button id="btn_save" type="button" class="btn btn-primary btn-action">Save</button>
</form>

@stop

@section("page_script")
<script type="text/javascript">
	$(function(){

		$(document).on("keydown", "input[data-type='number']", function(e){
			if(!((e.keyCode > 95 && e.keyCode < 106) || (e.keyCode > 47 && e.keyCode < 58) || e.keyCode == 8 || e.keyCode == 9) ) {
				e.preventDefault();
				return false;
			}
		});

		$("#btn_add_charges").click(function(){
			$clone = $(".div-charges:first").clone();
			$clone.find("input").val("");
			$clone.find(".lbl-remove").show();
			// $clone.append("<div class='col-lg-1'><div class='form-group'><label class='lbl-remove'>REMOVE</label></div></div>");
			$(".div-charges:last").after($clone);
			$clone.find("input").focus();

			if($(".div-charges").length > 1){
				$(".lbl-remove:first").show();
			}
		});

		$(document).on("click", ".lbl-remove", function(){
			$(this).closest(".div-charges").remove();

			if($(".div-charges").length > 1){
				$(".lbl-remove:first").show();
			} else {
				$(".lbl-remove:first").hide();
			}
		});

		$("#btn_save").click(function(){
			var chargeArr = [];
			var valid = true;
			var submitForm = true;
			$(".div-charges").each(function(){
				var obj = {};

				obj.type = $(this).find("select").val();
				obj.price = $(this).find("input").val();

				chargeArr.forEach(function(elem){
					if(elem.type == obj.type){
						swal("Oops", "Duplicate charge type found.", "warning");
						valid = false;
						return false;
					}
				});

				if(obj.price == ""){
					$(this).find("input").focus();
					valid = false;
				}

				if(!valid){
					submitForm = false;
					return false;
				} else {
					chargeArr.push(obj);
				}
			});

			if(submitForm){
				$("#txt_charge").val(JSON.stringify(chargeArr));
				$("#form_charge").submit();				
			}
		});


	});
</script>
@stop