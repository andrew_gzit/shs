@extends("layouts.nav")
@section('page_title', 'Create Package Type')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
	<li class="breadcrumb-item"><a href="javascript:;">Package Types</a></li>
	<li class="breadcrumb-item active">Create Package Type</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Create Package Type</h1>

<div class="panel panel-inverse">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="row m-b-10">
					<div class="col-md-12">
						<div class="form-group">
							<label>Package Code</label>
							<input type="text" autofocus="on" class="form-control form-control-lg" placeholder="Enter Package Code">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row m-b-10">
					<div class="col-md-12">
						<div class="form-group">
							<label>Package Type</label>
							<input type="text" autofocus="on" class="form-control form-control-lg" placeholder="Enter Package Type">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<button type="button" class="btn btn-primary btn-action">Save</button>
@stop

@section('page_script')
<script>
	$(document).ready(function() {
		//
	});
</script>
@stop