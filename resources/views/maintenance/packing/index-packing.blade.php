@extends("layouts.nav")
@section('page_title', 'Packing Types')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item">
		<a href="{{ action('MaintenanceController@create_packing_type') }}"><button type="button" class="btn btn-primary">Create Packing Type</button></a>
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Packing Types</h1>

<div class="table-responsive">
	<table id="table_vessels" class="table table-bordered" width="100%">
	    <thead class="thead-custom">
	        <tr>
	        	<th width="1%">#</th>
	            <th>Packing Code</th>
	            <th>Packing Type</th>
	            <th width="155px">Options</th>
	        </tr>
	    </thead>
	    <tbody class="bg-white">
	        <tr>
	            <td>1</td>
	            <td>U - 6785</td>
	            <td>Unit</td>
	            <td>
	            	<a href="javascript:;" class="btn btn-success btn-sm btn-fw">Edit</a>
	            	<a href="javascript:;" class="btn btn-danger btn-sm btn-fw">Delete</a>
	            </td>
	        </tr>
	    </tbody>
	</table>
</div>
@stop

@section('page_script')
<script>
	$(document).ready(function() {
		//
	});
</script>
@stop