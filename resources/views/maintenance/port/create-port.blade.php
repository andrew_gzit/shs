@extends("layouts.nav")
@section('page_title', 'Create Port')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('MaintenanceController@index_port') }}">Ports</a></li>
	<li class="breadcrumb-item active">Create Port</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Create Port</h1>

<form method="POST" action="{{ action('MaintenanceController@store_port') }}">
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('port_name') ? 'has-error' : '' }}">
						<label>Port Name</label>
						<input type="text" autofocus="on" class="form-control form-control-lg" name="port_name" placeholder="Enter Port Name" value="{{ old('port_name') }}" />
						<span class="text-danger">{{ $errors->first('port_name') }}</span>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('port_code') ? 'has-error' : '' }}">
						<label>Port Code</label>
						<input type="text" autofocus="on" class="form-control form-control-lg" name="port_code" placeholder="Enter Port Code" value="{{ old('port_code') }}" />
						<span class="text-danger">{{ $errors->first('port_code') }}</span>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('port_location') ? 'has-error' : '' }}">
						<label>Port Location</label>
						<input type="text" autofocus="on" class="form-control form-control-lg" name="port_location" placeholder="Enter Port Location" value="{{ old('port_location') }}" />
						<span class="text-danger">{{ $errors->first('port_location') }}</span>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('station_code') ? 'has-error' : '' }}">
						<label>Station Code</label>
						<input type="text" autofocus="on" class="form-control form-control-lg" name="station_code" placeholder="Enter Station Code" value="{{ old('station_code') }}" />
						<span class="text-danger">{{ $errors->first('station_code') }}</span>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('bl_prefix') ? 'has-error' : '' }}">
						<label>BL Prefix</label>
						<input type="text" autofocus="on" class="form-control form-control-lg" name="bl_prefix" placeholder="Enter BL Prefix" value="{{ old('bl_prefix') }}" />
						<span class="text-danger">{{ $errors->first('bl_prefix') }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<button type="button" id="btn_cancel" class="btn btn-danger btn-action">Cancel</button>
	<button type="submit" class="btn btn-primary btn-action">Save</button>
</form>
@stop

@section('page_script')
<script>
	$(document).ready(function() {
		$('#btn_cancel').click(function() {
			swal({
				title: 'Are your sure?',
				text: 'All your current progress will be lost.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Proceed',
				reverseButtons: true
			}).then((result) => {
				if(result.value) {
					window.location.replace('{{ action('MaintenanceController@index_port') }}');
				}
			});
		});
	});
</script>
@stop