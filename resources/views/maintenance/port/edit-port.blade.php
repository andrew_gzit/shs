@extends("layouts.nav")
@section('page_title', 'Edit Port')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('MaintenanceController@index_port') }}">Ports</a></li>
	<li class="breadcrumb-item active">Edit Port</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Edit Port - <span class="f-s-18">{{ $port->name }}</span></h1>

<form method="POST" autocomplete="off" action="{{ action('MaintenanceController@put_port', $port->id) }}">
	@method('PUT')
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body p-25">
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('port_name') ? 'has-error' : '' }}">
						<label>Port Name</label>
						<input type="text" class="form-control form-control-lg" name="port_name" placeholder="Enter Port Name" value="{{ !empty(old('port_name')) ? old('port_name') : $port->name }}" required />
						<span class="text-danger">{{ $errors->first('port_name') }}</span>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('port_code') ? 'has-error' : '' }}">
						<label>Port Code</label>
						<input type="text" class="form-control form-control-lg" name="port_code" placeholder="Enter Port Code" value="{{ !empty(old('port_code')) ? old('port_code') : $port->code }}" />
						<span class="text-danger">{{ $errors->first('port_code') }}</span>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('port_location') ? 'has-error' : '' }}">
						<label>Port Location</label>
						<input type="text" class="form-control form-control-lg" name="port_location" placeholder="Enter Port Location" value = "{{ !empty(old('port_location')) ? old('port_location') : $port->location }}" />
						<span class="text-danger">{{ $errors->first('port_location') }}</span>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('station_code') ? 'has-error' : '' }}">
						<label>Station Code</label>
						<input type="text" class="form-control form-control-lg" name="station_code" placeholder="Enter Station Code" value="{{ !empty(old('station_code')) ? old('station_code') : $port->station_code }}" />
						<span class="text-danger">{{ $errors->first('station_code') }}</span>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('bl_prefix') ? 'has-error' : '' }}">
						<label>BL Prefix</label>
						<input type="text" class="form-control form-control-lg" name="bl_prefix" placeholder="Enter BL Prefix" value="{{ !empty(old('bl_prefix')) ? old('bl_prefix') : $port->bl_prefix }}" />
						<span class="text-danger">{{ $errors->first('bl_prefix') }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<button type="button" id="btn_cancel" class="btn btn-danger btn-action">Cancel</button>
	<button type="submit" class="btn btn-primary btn-action">Save</button>
</form>
@stop

@section('page_script')
<script>
	$(document).ready(function() {
		$('#btn_cancel').click(function() {
			swal({
				title: 'Are your sure?',
				text: 'All your current progress will be lost.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Proceed',
				reverseButtons: true
			}).then((result) => {
				if(result.value) {
					window.location.replace('{{ action('MaintenanceController@index_port') }}');
				}
			});
		});
	});
</script>
@stop