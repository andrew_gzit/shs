@extends("layouts.nav")
@section('page_title', 'Ports')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item">
		<a href="{{ action('MaintenanceController@create_port') }}"><button type="button" class="btn btn-primary"><i class="fa fa-plus m-r-5"></i>Create Port</button></a>
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Ports</h1>

@if(session('success'))
<div class="alert alert-green">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('success') }}
</div>
@endif

<div class="table-responsive">
	<table class="table table-bordered f-s-14">
		@if($ports->isNotEmpty())
		<thead class="thead-custom">
			<tr>
				<th width="1%">#</th>
				<th>Name</th>
				<th>Code</th>
				{{-- <th>Location (code)</th> --}}
				<th>Station Code</th>
				<th>BL Prefix</th>
				<th width="1%">Options</th>
			</tr>
		</thead>
		<tbody class="bg-white">
			@foreach($ports as $port)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $port->name }}</td>
				<td>{{ $port->code }}</td>
				{{-- <td>{{ $port->location }} ({{ $port->location_code }})</td> --}}
				<td>{{ $port->station_code }}</td>
				<td>{{ $port->bl_prefix }}</td>
				<td class="no-wrap">
					<a href="{{ action('MaintenanceController@edit_port', Hashids::encode($port->id)) }}" class="btn btn-success"><i class="fa fa-edit"></i></a>
					<a href="javascript:;" data-id = "{{ $port->id }}" class="btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
			@endforeach
		</tbody>
		@else
		<tbody class="bg-white">
			<tr class="text-center">
				<td>No ports to display.</td>
			</tr>
		</tbody>
		@endif
	</table>
</div>

<form id="form_delete" method="POST" action="{{ action('MaintenanceController@delete_port') }}">
	@method('DELETE')
	@csrf
	<input type="hidden" name="port_id" />
</form>
@stop

@section('page_script')
<script>
	$(document).ready(function() {

		// Delete vessel
		$(document).on('click', '.btn-delete', function() {
			swal({
				title: "Confirm delete?",
				text: "Press Continue to proceed",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Continue',
				cancelButtonText: 'Cancel',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					$('#form_delete').find('input[name="port_id"]').val($(this).attr('data-id'));
					$('#form_delete').submit();
				}
			});
		});

		@if(session('error'))
		swal({
			title: "Deletion Failed",
			html: "{!! session('error') !!}",
			type: "error",
			confirmButtonText: "OK"
		});
		@endif

	});
</script>
@stop