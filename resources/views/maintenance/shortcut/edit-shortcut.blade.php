@extends("layouts.nav")
@section('page_title', 'Edit Shortcut Key')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('MaintenanceController@index_shortcut') }}">Shortcut Keys</a></li>
	<li class="breadcrumb-item active">Edit Shortcut Key</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Edit Shortcut Key</h1>

<form method="POST" action="{{ action('MaintenanceController@update_shortcut', $shortcut->id) }}">
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label>Function Key</label>
						<input type="text" class="form-control form-control-lg" readonly="" value="{{ $shortcut->shortcut_code }}">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Description Text</label>
						<textarea rows="7" name="txt_description" class="form-control form-control-lg" style="resize: vertical;">{{ $shortcut->description }}</textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
	<button type="button" class="btn btn-danger btn-action btn-cancel">Cancel</button>
	<button class="btn btn-primary btn-action">Save</button>
</form>
@stop

@section('page_script')
<script type="text/javascript">
	$(function(){
		$('.btn-cancel').click(function() {
			swal({
				title: 'Are your sure?',
				text: 'All your current progress will be lost.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Proceed',
				reverseButtons: true
			}).then((result) => {
				if(result.value) {
					window.location.href = '/maintenance/shortcut';
				}
			});
		});
	});
</script>
@stop