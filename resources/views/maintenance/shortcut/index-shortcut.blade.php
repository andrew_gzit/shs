@extends("layouts.nav")
@section('page_title', 'Shortcut Keys')

@section('content')
<h1 class="page-header">Shortcut Keys</h1>

@if(session('msg'))
    <div class="alert {{ session('class') }}">
    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('msg') }}
    </div>
@endif

<div class="table-responsive">
	<table class="table table-bordered table-valign-middle f-s-14 bg-white">
		<thead class="thead-custom">
			<tr>
				<th width="1%" class="no-wrap">Function Key</th>
				<th>Description</th>
				<th width="1%">Options</th>
			</tr>
		</thead>
		<tbody>
			@foreach($shortcuts AS $sc)
			<tr>
				<td>{{ $sc->shortcut_code }}</td>
				<td>{{ $sc->description }}</td>
				<td class="text-center">
					<a href="{{ action('MaintenanceController@edit_shortcut', Hashids::encode($sc->id)) }}" class="btn btn-success">
						<i class="fa fa-edit"></i>
					</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

<div class="modal fade" id="modal-view">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">View Charge</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-1">

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop