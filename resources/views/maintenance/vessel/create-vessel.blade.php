@extends("layouts.nav")
@section('page_title', 'Create Vessel')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('MaintenanceController@index_vessel') }}">Vessels</a></li>
	<li class="breadcrumb-item active">Create Vessel</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Create Vessel</h1>

<form method="POST" action="{{ action('MaintenanceController@store_vessel') }}">
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group {{ $errors->has('vessel_name') ? 'has-error' : '' }}">
						<label>Vessel Name</label>
						<input type="text" class="form-control form-control-lg" name="vessel_name" placeholder="Enter Vessel Name" value="{{ old('vessel_name') }}" required />
						<span class="text-danger">{{ $errors->first('vessel_name') }}</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Vessel Type</label>
						<select id="vessel_type" class="form-control form-control-lg" name="vessel_type">
							<option>General Cargo Vessel</option>
							<option>Container Vessel</option>
						</select>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label class="d-block">Semi-Container Service</label>
						<div class="radio radio-css radio-inline">
							<input type="radio" id="radio_semi_yes" name="radio_semi_type" value=1 />
							<label for="radio_semi_yes">Yes</label>
						</div>
						<div class="radio radio-css radio-inline">
							<input type="radio" id="radio_semi_no" name="radio_semi_type" value=0 checked="" />
							<label for="radio_semi_no">No</label>
						</div>
						<p class="p-t-5"><i>(only for General Cargo Vessel)</i></p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group {{ $errors->has('vessel_dwt') ? 'has-error' : '' }}">
						<label>Vessel DWT/TUES</label>
						<input type="number" class="form-control form-control-lg" min="0" name="vessel_dwt" placeholder="Enter Vessel DWT" value="{{ old('vessel_dwt') }}" required />
						<span class="text-danger m-t-5">{{ $errors->first('vessel_dwt') }}</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group {{ $errors->has('vessel_year') ? 'has-error' : '' }}">
						<label>Vessel Manufactured Year</label>
						<input type="text" autocomplete="off" class="form-control form-control-lg yearpicker" name="vessel_year" placeholder="Enter Manufactured Year" value="{{ old('vessel_year') }}" required />
						<span class="text-danger">{{ $errors->first('vessel_year') }}</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group {{ $errors->has('vessel_flag') ? 'has-error' : '' }}">
						<label>Vessel Flag</label>
						<select class="form-control form-control-lg" name="vessel_flag">
							<option>Autralia</option>
							<option>Brazil</option>
							<option>Canada</option>
							<option>China</option>
							<option>Germany</option>
							<option>India</option>
							<option>Indonesia</option>
							<option>Italy</option>
							<option>Japan</option>
							<option selected>Malaysia</option>
							<option>Netherlands</option>
							<option>Saudi Arabia</option>
							<option>Singapore</option>
							<option>South Korea</option>
							<option>Spain</option>
							<option>Taiwan</option>
							<option>Thailand</option>
							<option>Turkey</option>
							<option>Vietnam</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Default Shipment Type</label>
						<select id="default_shipment" class="form-control form-control-lg" name="vessel_default">
							<option disabled>FCL (unavailable for GCV w/o semi-container)</option>
							<option>LCL</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>

	<button type="button" id="btn_cancel" class="btn btn-danger btn-action">Cancel</button>
	<button type="submit" class="btn btn-primary btn-action">Save</button>
</form>
@stop

@section('page_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />

<script src="/js/vessel.js"></script>
@stop