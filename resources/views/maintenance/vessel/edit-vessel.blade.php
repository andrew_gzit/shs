@extends("layouts.nav")
@section('page_title', 'Edit Vessel')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('MaintenanceController@index_vessel') }}">Vessels</a></li>
	<li class="breadcrumb-item active">Edit Vessel</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Edit Vessel - <span class="f-s-18">{{ $vessel->name }}</span></h1>

<form method="POST" autocomplete="off" action="{{ action('MaintenanceController@put_vessel', $vessel->id) }}">
	@method('PUT')
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group {{ $errors->has('vessel_name') ? 'has-error' : '' }}">
						<label>Vessel Name</label>
						<input type="text" autofocus="" class="form-control form-control-lg" name="vessel_name" placeholder="Enter Vessel Name" value="{{ $vessel->name }}" />
						<span class="text-danger">{{ $errors->first('vessel_name') }}</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Vessel Type</label>
						<select id="vessel_type" class="form-control form-control-lg" name="vessel_type">
							<option {{ ($vessel->type == 'General Cargo Vessel') ? 'selected' : '' }} >General Cargo Vessel</option>
							<option {{ ($vessel->type == 'Container Vessel') ? 'selected' : '' }} >Container Vessel</option>
						</select>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label class="d-block">Semi-Container Service</label>
						<div class="radio radio-css radio-inline">
							<input type="radio" id="radio_semi_yes" name="radio_semi_type" value="1" {{ ($vessel->semicontainer_service == 1) ? 'checked' : '' }}/>
							<label class="lbl" for="radio_semi_yes">Yes</label>
						</div>
						<div class="radio radio-css radio-inline">
							<input type="radio" id="radio_semi_no" name="radio_semi_type" value="0" {{ ($vessel->semicontainer_service == 0) ? 'checked' : '' }} />
							<label class="lbl" for="radio_semi_no">No</label>
						</div>
						<p class="p-t-5"><i>(only for General Cargo Vessel)</i></p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group {{ $errors->has('vessel_dwt') ? 'has-error' : '' }}">
						<label>Vessel DWT/TUES</label>
						<input type="text" class="form-control form-control-lg" name="vessel_dwt" placeholder="Enter Vessel DWT" value="{{ $vessel->dwt }}" />
						<span class="text-danger m-t-5">{{ $errors->first('vessel_dwt') }}</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group {{ $errors->has('vessel_year') ? 'has-error' : '' }}">
						<label>Vessel Manufactured Year</label>
						<input type="text" class="form-control form-control-lg yearpicker" autocomplete="off" name="vessel_year" placeholder="Enter Manufactured Year" value="{{ $vessel->manufactured_year }}" />
						<span class="text-danger">{{ $errors->first('vessel_year') }}</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group {{ $errors->has('vessel_flag') ? 'has-error' : '' }}">
						<label>Vessel Flag</label>
						<select class="form-control form-control-lg" name="vessel_flag">
							<option {{ ($vessel->flag == 'Australia') ? 'selected' : '' }}>Autralia</option>
							<option {{ ($vessel->flag == 'Brazil') ? 'selected' : '' }}>Brazil</option>
							<option {{ ($vessel->flag == 'Canada') ? 'selected' : '' }}>Canada</option>
							<option {{ ($vessel->flag == 'China') ? 'selected' : '' }}>China</option>
							<option {{ ($vessel->flag == 'Germany') ? 'selected' : '' }}>Germany</option>
							<option {{ ($vessel->flag == 'India') ? 'selected' : '' }}>India</option>
							<option {{ ($vessel->flag == 'Indonesia') ? 'selected' : '' }}>Indonesia</option>
							<option {{ ($vessel->flag == 'Italy') ? 'selected' : '' }}>Italy</option>
							<option {{ ($vessel->flag == 'Japan') ? 'selected' : '' }}>Japan</option>
							<option {{ ($vessel->flag == 'Malaysia') ? 'selected' : '' }}>Malaysia</option>
							<option {{ ($vessel->flag == 'Netherlands') ? 'selected' : '' }}>Netherlands</option>
							<option {{ ($vessel->flag == 'Saudi Arabia') ? 'selected' : '' }}>Saudi Arabia</option>
							<option {{ ($vessel->flag == 'Singapore') ? 'selected' : '' }}>Singapore</option>
							<option {{ ($vessel->flag == 'South Korea') ? 'selected' : '' }}>South Korea</option>
							<option {{ ($vessel->flag == 'Spain') ? 'selected' : '' }}>Spain</option>
							<option {{ ($vessel->flag == 'Taiwan') ? 'selected' : '' }}>Taiwan</option>
							<option {{ ($vessel->flag == 'Thailand') ? 'selected' : '' }}>Thailand</option>
							<option {{ ($vessel->flag == 'Turkey') ? 'selected' : '' }}>Turkey</option>
							<option {{ ($vessel->flag == 'Vietnam') ? 'selected' : '' }}>Vietnam</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Default Shipment Type</label>
						<select id="default_shipment" class="form-control form-control-lg" name="vessel_default">
							<option {{ ($vessel->default_shipment == 'FCL') ? 'selected' : '' }}>FCL</option>
							<option {{ ($vessel->default_shipment == 'LCL') ? 'selected' : '' }}>LCL</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>

	<button type="button" id="btn_cancel" class="btn btn-danger btn-action">Cancel</button>
	<button type="submit" class="btn btn-primary btn-action">Save</button>
</form>
@stop

@section('page_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />

<script src="/js/vessel.js"></script>

<script type="text/javascript">
	$(function() {
		$('#vessel_type').trigger("change");
		@if($vessel->type == "Container Vessel")
		$('input[type=radio]').prop("disabled", true).prop("checked", false);
		@else
		@if($vessel->semicontainer_service == 1)
		$('#radio_semi_yes').prop("checked", true).trigger("change");
		@if($vessel->default_shipment == "FCL")
		$('#default_shipment option:first-child').prop("selected", true);
		@endif
		@endif
		@endif

		console.log("{{ $vessel->editable }}");

		@if(!$vessel->editable)
		$("#vessel_type, input[type=radio], #default_shipment").addClass('disabled').attr('tabindex', '-1');
		$(".lbl").css("pointer-events", "none").attr('tabindex', '-1');
		@endif
	});
</script>
@stop