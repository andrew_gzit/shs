@extends("layouts.nav")
@section('page_title', 'Vessels')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item">
		<a href="{{ action('MaintenanceController@create_vessel') }}"><button type="button" class="btn btn-primary">Create Vessel</button></a>
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Vessels</h1>

@if(session('success'))
<div class="alert alert-green">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('success') }}
</div>
@endif

<div class="table-responsive">
	<table id="table_vessels" class="table table-bordered table-valign-middle f-s-14" width="100%">
		@if($vessels->isNotEmpty())
		<thead class="thead-custom">
			<tr>
				<th width="1%">#</th>
				<th>Name</th>
				<th>Type</th>
				<th>DWT/TEUS</th>
				<th>Semi-Container Service</th>
				<th>Manufactured Year</th>
				<th width="1%">Options</th>
			</tr>
		</thead>
		<tbody class="bg-white">
			@foreach($vessels as $vessel)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $vessel->name }}</td>
				<td>{{ $vessel->type }}</td>
				<td>{{ $vessel->dwt }}</td>
				<td>
					@if($vessel->semicontainer_service == 1)
					<span class="text-green f-s-16"><i class="fas fa-check"></i></span>
					@else
					NA
					@endif
				</td>
				<td>{{ $vessel->manufactured_year }}</td>
				<td class="no-wrap">
					<a href="{{ action('MaintenanceController@edit_vessel', Hashids::encode($vessel->id)) }}" class="btn btn-success p-r-10">
						<i class="fa fa-edit"></i>
					</a>
					<a href="javascript:;" data-id = "{{ $vessel->id }}" class="btn btn-danger btn-delete">
						<i class="fa fa-trash"></i>
					</a>
				</td>
			</tr>
			@endforeach
		</tbody>
		@else
		<tbody class="bg-white">
			<tr class="text-center">
				<td>No vessels to display.</td>
			</tr>
		</tbody>
		@endif
	</table>
</div>

<form id="form_delete" method="POST" action="{{ action('MaintenanceController@delete_vessel') }}">
	@method('DELETE')
	@csrf
	<input type="hidden" name="vessel_id" />
</form>
@stop

@section('page_script')
<script>
	$(document).ready(function() {

		// Delete vessel
		$(document).on('click', '.btn-delete', function() {
			swal({
				title: "Confirm delete?",
				text: "Press Continue to proceed",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Continue',
				cancelButtonText: 'Cancel',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					$('#form_delete').find('input[name="vessel_id"]').val($(this).attr('data-id'));
					$('#form_delete').submit();
				}
			});
		});

		@if(session('error'))
		swal({
			title: "Deletion Failed",
			html: "{!! session('error') !!}",
			type: "error",
			confirmButtonText: "OK"
		});
		@endif

	});
</script>
@stop