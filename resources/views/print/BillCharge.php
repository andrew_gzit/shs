<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\BillLading;
use \App\CargoDetail;

class BillCharge extends Model
{
    protected $table = "bill_charges";

    public function getCharge(){
    	return $this->hasOne('\App\Charge', 'id', 'charge_code')->withTrashed();
    }

    public function getUnit(){
    	$str = explode("-", $this->charge_unit);
    	$bl = BillLading::find($this->bl_no);
    	$unit = 0;

        if($bl->vessel_type == "LCL"){
            foreach($bl->getCargos AS $cargo){
                $unit += $cargo->weight;
            }
        } else {
            foreach($bl->getCargos AS $cargo){
                $exist = CargoDetail::where("cargo_id", $cargo->id)->where("size", $str[0])->where("type", $str[1])->count();

                if($exist > 0){
                    $unit += $exist;
                }
            }
        }

        if($unit == 0){
            return 1;
        } else {
            return $unit;
        }
    }

    public function displayCharges(){
        $totalQty = $this->getTotalCargoQty($this->bl_no, $this->charge_unit);

        // $total_weight = this::getTotalCargoQty();

        // switch($str[1]){
        //     case "M3":
        //     $total_weight = $bl->getCargos->sum('volume');
        //     break;
        //     case "MT":
        //     $total_weight = $bl->getCargos->sum('weight');
        //     break;
        //     case "KG":
        //     $total_weight = $bl->getCargos->sum('weight');
        //     break;
        //     case "SET":
        //     $total_weight = 1;
        //     break;
        //     default:
        //     foreach($bl->getCargos AS $cargo){
        //         $total_weight += CargoDetail::where("cargo_id", $cargo->id)->where("size", $str[0])->where("type", $str[1])->count();
        //     }
        //     break;
        // }

        // if($str[1] == "M3"){
        //     $total_weight = $bl->getCargos->sum('volume');
        // } else {
        //     $total_weight = $bl->getCargos->sum('weight');
        // }

        if($totalQty == 0){
            $totalQty = 1;
        }

        return number_format($totalQty, 3) . " X " . $this->charge_rate . " / " . $this->getChargeUnit();
    }

    public function getChargeUnit(){
        $str = explode("-", $this->charge_unit);

        if($str[0] != 1){
            return $str[0] . '\'' . $str[1];
        } else {
            return $str[1];
        }

    }

    public function getTotalCargoQty($bl_no, $unit_str){
        $str = explode("-", $unit_str);
        $bl = BillLading::find($bl_no);

        $totalQty = 0;

        switch($str[1]){
            case "M3":
            $totalQty = $bl->getCargos->sum('volume');
            break;
            case "MT":
            $totalQty = $bl->getCargos->sum('weight');
            break;
            case "KG":
            $totalQty = $bl->getCargos->sum('weight');
            break;
            case "SET":
            $totalQty = 1;
            break;
            case "UNIT":
            $totalQty = $bl->getCargos->reduce(function($carry, $item){
                return $carry + $item->cargo_qty;
            });
            default:
            foreach($bl->getCargos AS $cargo){
                $totalQty += CargoDetail::where("cargo_id", $cargo->id)->where("size", $str[0])->where("type", $str[1])->count();
            }
            break;
        }
        return $totalQty;
    }  

    public function calculateCharges(){
        $totalQty = $this->getTotalCargoQty($this->bl_no, $this->charge_unit);

        if($totalQty == 0){
            $totalQty = 1;
        }

        return $totalQty * $this->charge_rate;
    }
}
