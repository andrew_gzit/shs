@extends("layouts.nav")

@section("content")
<form id="form_bl" method="POST" action="{{ action('BillLadingController@set_format_bl') }}?id={{$bl_id }}">
	@csrf
	<input hidden="" name="bl_id" value="{{ $bl_id }}">
	<input hidden="" name="txt_blformat" value="{{ $format }}">
	<input hidden="" name="txt_companyname1" value="{{ $cname1 }}">
	<input hidden="" name="txt_companyname2" value="{{ $cname2 }}">
</form>
@stop

@section("page_script")
<script type="text/javascript">
	$(function(){
		$('.loader').fadeIn(1);
		$("#form_bl").submit();
	});
</script>
@stop