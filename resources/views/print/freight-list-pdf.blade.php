<html>
<head>

	<style>

		@font-face {
			font-family: 'Open Sans';
			font-style: normal;
			font-weight: normal;
			src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
		}

		*, ::after, ::before {
			box-sizing: border-box;
		}

		.top3{
			height: 20mm;
			width: 105mm;
		}

		.top-bl{
			width: 105mm;
			text-align: center;
		}

		.bl-label{
			display: block;
			margin-bottom: 5px;
			font-size: 9px;
			font-weight: 400;
			padding-top: 0px;
		}

		.bl-content{
			display: block;
			padding-left: 20px;
			padding-bottom: 5px;
			font-weight: 400 !important;
			font-size: 11px !important;
		}

		.no-bottom-border{
			border-bottom: none;
		}

		.table-apollo-header{
			font-size: 9px;
			font-weight: 400 !important;
		}

		.table-apollo-header td{
			border: none !important;
		}

		.table-apollo-header .container-markings{
			width: 50mm;
		}

		.table-apollo-header .num-packages{
			width: 43mm;
		}

		.table-apollo-header .description-goods{
		}

		.table-apollo-header .width{
			width: 25mm;
		}

		.bl-company h2{
			padding-top: 1in;
		}

		.p-0{
			padding: 0 !important;
		}

		.bl-lbl-goods {
			display: block;
			text-align: center;
			font-size: 13px;
			font-weight: 400;
			margin-top: 10px;
		}

		td{
			border: 1px solid #000;
			padding: 5px;
			vertical-align: top;
		}

		main{
			padding: 5px;
			font-size: 11px;
			margin-left: -5px;
		}

		.table-borderless td{
			border: 0;
		}

		.detailed_desc{
			/*white-space: pre;*/
		}

		.table-manifest{
			margin-top: 10px !important;
		}

		.table-manifest-header th{
			border-bottom: 1px solid #000;
		}

		@page { margin-top: 150px; margin-bottom: 0px; margin-left: 20px; margin-right: 20px; size: A4 landscape }
		header { position: fixed; top: -145px; left: 0px; right: 0px; width: 100%; font-size: 12px; }
		/*p { page-break-after: always; }*/
		/*p:last-child { page-break-after: never; }*/

		body{
			font-family: sans-serif;
		}

		main{

		}

		.desc{
			font-family: "Helvetica", sans-serif;
			white-space: pre-wrap;
		}

		.table{
			width: 100%;
			border-collapse: collapse;
		}

		.m-0{
			margin: 0;
		}

		.no-wrap{
			white-space: nowrap;
		}
	</style>
</head>

<body>
	<header>
		<span style="position: absolute; font-size: 15px; top: 18px; font-weight: bold">{{ optional($base_company)->name }}</span>
		<h2 style="text-align: center">FREIGHT MANIFEST</h2>
		<table class="table table-borderless table-manifest">
			<tr>
				<td style="text-align: right; width: 100px"><strong>Vessel Name: </strong></td>
				<td style="width: 80px">{{ $bill_ladings[0]->getVessel->name }}</td>
				<td style="text-align: right; width: 150px"><strong>Date of Arrival: </strong></td>
				<td style="width: 80px">{{ !empty($bill_ladings[0]->voyage_id) ? $bill_ladings[0]->getVoyage->eta_pol->format('d/m/Y') : '-' }}</td>
				<td style="text-align: right; width: 150px"><strong>Port of Loading: </strong></td>
				<td>{{ strtoupper($bill_ladings[0]->pol_name) }}</td>
				<td style="text-align: right; width: 150px"><strong>Nationality: </strong></td>
				<td width="50px">{{ strtoupper($bill_ladings[0]->getVessel->flag) }}</td>
			</tr>
			<tr>
				<td style="text-align: right; width: 100px"><strong>Voyage No.: </strong></td>
				<td style="width: 80px">{{ $bill_ladings[0]->getVoyage->voyage_id }}</td>
				<td style="text-align: right; width: 150px"><strong>Date of Departure: </strong></td>
				<td style="width: 80px">{{ $bill_ladings[0]->pol_time() }}</td>
				<td style="text-align: right; width: 150px"><strong>Port of Discharge: </strong></td>
				<td>{{ strtoupper($bill_ladings[0]->fpd_name) }}</td>
				<td style="text-align: right; width: 150px"></td>
				<td width="50px"></td>
			</tr>
		</table>
		<table class="table table-manifest-header" style="margin-top: 10px">
			<thead>
				<tr>
					<th style="width: 223px">Shipper/Consignee/Notify Party</th>
					<th style="width: 128px">Marks & No.</th>
					<th style="width: 313px;">Cargo Description</th>
					<th style="width: 80px;">Weight</th>	
					<th style="width: 64px;">Meas.</th>
					<th style="width: 170px;">Charges</th>
					<th  class="no-wrap" style="text-align: right" >Amount</th>
					<th></th>
				</tr>
			</thead>
		</table>
	</header>
	<main>

		<div style="display: table; table-layout: fixed; width: 100%;">
			@foreach($bill_ladings AS $key => $bl)
			<div style="display: table-row;">
				<div style="display: table-cell; width: 20%">
					<strong style="display: block; text-decoration: underline;">BL No.: {{ $bl->bl_no }}</strong>
					<div>
						<strong style="display: block">SHIPPER</strong>
						{{ $bl->shipper_name }}<br>{!! nl2br($bl->shipper_add) !!}<br><br>
						<strong style="display: block">CONSIGNEE</strong>
						{{ $bl->consignee_name }}<br>{!! nl2br($bl->consignee_add) !!}<br><br>
						<strong style="display: block">NOTIFY PARTY</strong>
						@if($bl->notify_add == $bl->consignee_add)
						SAME AS CONSIGNEE
						@else
						<p style="margin: 0">{{ $bl->notify_name }}{!! (!empty($bl->notify_add)) ? '<br>' . nl2br($bl->notify_add) : '' !!}</p>
						@endif
					</div>
				</div>
				<div style="display: table-cell; width: 13%; vertical-align: top; padding-left: 10px; padding-top: 12px">
					@if(isset($bl->containers))
					<p style="width: 180px;">{{ $bl->containers }}</p>
					@else
					@foreach($bl->getCargos AS $cargo)
					@if($bl->vessel_type == "FCL")
					@foreach($cargo->containers AS $cont)
					<p class="m-0">{{ $cont->container_no }} / {{ $cont->seal_no }}</p>
					@endforeach
					@endif
					{!! nl2br($cargo->markings) !!}
					@endforeach
					@endif
					<!-- {!! !empty($cargo->markings) ? nl2br($cargo->markings) . '<br>' : '' !!} -->
				</div>
				<div style="display: table-cell; vertical-align: top; width: 29%; padding-top: 12px">
					@foreach($bl->getCargos AS $cargo)
					<div>{{ $cargo->cargo_name . ' ' . $cargo->cargo_desc }}</div>
					<div class="desc" style="word-break: all; max-width: 100px">{!! nl2br($cargo->detailed_desc) !!}</div>
					@endforeach
					<strong><br>FREIGHT {{ $bl->getFreightTerm() }}</strong>
				</div>
				<div style="display: table-cell; vertical-align: top; width: 14%; padding-top: 12px;">
					@if($bl->getCargos->sum('weight'))
					{{ number_format($bl->getCargos->sum('weight'), 3) . ' ' . $bl->getCargos[0]->weight_unit }}
					@endif
					<span style="width: 25px">&nbsp;&nbsp;&nbsp;&nbsp;</span>
					@if($bl->getCargos->sum('volume') > 0)
					{{ number_format($bl->getCargos->sum('volume'), 3) . ' M3'}}
					@endif
				</div>
				<div style="display: table-cell; vertical-align: top; padding-top: 17px">
					@foreach($bl->getCharges->where("charge_payment", "P") AS $chg)
					<div style="line-height: 8px;">
						<div style="display: inline-block; width: 50px">{{ $chg->getCharge->code }}</div>
						<div style="display: inline-block; width: 110px; white-space: nowrap;">{{ $chg->displayCharges() }}</div>
						<div style="display: inline-block; width: 80px; text-align: right; padding-right: 10px">MYR {{ number_format($chg->calculateCharges(), 2) }}</div>
						<div style="display: inline-block; text-align: right">{{ $chg->charge_payment }}</div>
					</div>
					@endforeach
					@if(count($bl->getCharges->where("charge_payment", "P")) > 0)
					<div style="text-align: right;">
						<div style="display: inline-block; float: right; margin-right: 15px; vertical-align: top;  margin-top: 10px; padding-top: 5px; padding-bottom: 5px; border-top: 2px solid; border-bottom: 2px solid">MYR {{ number_format($bl->getSumCharges("P"), 2) }}</div>
					</div>
					<div style="clear:both;"></div>
					@endif
					<br>
					@foreach($bl->getCharges->where("charge_payment", "C") AS $chgC)
					<div style="line-height: 8px">
						<div style="display: inline-block; width: 50px">{{ $chgC->getCharge->code }}</div>
						<div style="display: inline-block; width: 110px; white-space: nowrap;">{{ $chgC->displayCharges() }}</div>
						<div style="display: inline-block; width: 80px; text-align: right; padding-right: 10px">MYR {{ number_format($chgC->calculateCharges(), 2) }}</div>
						<div style="display: inline-block; text-align: right">{{ $chgC->charge_payment }}</div>
					</div>
					@endforeach
					@if(count($bl->getCharges->where("charge_payment", "C")) > 0)
					<div style="text-align: right;">
						<div style="display: inline-block; float: right; margin-right: 15px; vertical-align: top;  margin-top: 10px; padding-top: 5px; padding-bottom: 5px; border-top: 2px solid; border-bottom: 2px solid">MYR {{ number_format($bl->getSumCharges("C"), 2) }}</div>
					</div>
					@endif
				</div>
			</div>
			@if(!$loop->last)
			<div style="display: table-row;">
				<div style="display: table-cell;"><br><br></div>
				<div style="display: table-cell;">&nbsp;</div>
				<div style="display: table-cell;">&nbsp;</div>
				<div style="display: table-cell;">&nbsp;</div>
				<div style="display: table-cell;">&nbsp;</div>
			</div>
			@endif
			@endforeach
		</div>

<!-- 	</div>
</div>
<div style="height: 10px"></div> -->

<hr>
<div style="display: table; table-layout: fixed; width: 100%; line-height: 10px">
	<div style="display: table-cell; background: white; width: 18%"></div>
	<div style="display: table-cell; background: white; width: 14.5%; vertical-align: top"></div>
	<div style="display: table-cell; background: white; vertical-align: top; width: 27%">
		@if(is_array($total_cargo))
		@if($total_cargo[0] > 0)
		{{ $total_cargo[0] }} TWENTY FOOTER CONTAINER<br>
		@endif
		@if($total_cargo[1] > 0)
		{{ $total_cargo[1] }} FORTY FOOTER CONTAINER
		@endif
		@else
		{{ $total_cargo }} PACKAGES
		@endif
	</div>
	<div style="display: table-cell; background: white; vertical-align: top; width: 9%; text-align: right">
		{!! ($total_weight_mt > 0) ? number_format($total_weight_mt, 3) . ' MT<br>' : '' !!}{{ ($total_weight_kg > 0) ? number_format($total_weight_kg, 3) . ' KG' : '' }}
	</div>
	<div style="display: table-cell; background: white; vertical-align: top; width: 9%; text-align: left; padding-left: 10px">
		{{ ($total_volume > 0) ? number_format($total_volume, 3) . ' M3' : '' }}
	</div>
	<div style="display: table-cell; background: white; vertical-align: top; text-align: right;">
		{!! ($total_price_p > 0) ? '<div style="margin-bottom: 3px">MYR ' . number_format($total_price_p, 2) . '<span style="display: inline-block; width: 18px; float: right">P</span></div>' : '' !!}
		{!! ($total_price_c > 0) ? '<div style="margin-bottom: 3px">MYR ' . number_format($total_price_c, 2) . '<span style="display: inline-block; width: 18px; float: right">C</span></div>' : '' !!}
			{!! ($total_price_c != 0 && $total_price_p != 0) ? '<div style="border-top: 2px solid #000; display: inline-block; padding-top: 5px; float: right">MYR ' . number_format($total_amount, 2) . '   <span style="color: #fff">TT</span></div>' : "" !!}
		</div>
	</div>
</div>
</main>


</body>
</html>