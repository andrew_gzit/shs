@extends("layouts.nav")
@section('page_title', 'Print Shipping Documents')

@section('content')
<h1 class="page-header">Print Shipping Documents</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{!! session('msg') !!}
	{{ session::forget('msg') }}
</div>
@endif

<div class="panel panel-inverse">
	<div class="panel-heading">
		<h5 class="m-b-0 text-white">Filters</h5>
	</div>
	<form method="GET">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>Date Range (ETA POL)</label>
						<!-- <div class="input-group input-daterange">
							<input type="text" autocomplete="off" class="ddl form-control form-control-lg datepicker" name="start_date" placeholder="Date Start" />
							<span class="input-group-addon" style="line-height: 33px;">to</span>
							<input type="text" autocomplete="off" class="ddl form-control form-control-lg datepicker" name="end_date" placeholder="Date End" />
						</div> -->
						<div class="input-group input-daterange">
							<input type="text" id="start_date" class="filter form-control form-control-lg" autocomplete="off" name="start_date" placeholder="Date Start" />
							<span class="input-group-addon" style="line-height: 33px;">to</span>
							<input type="text" id="end_date" class="filter form-control form-control-lg" autocomplete="off" name="end_date" placeholder="Date End" />
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<label>Vessel</label>
						<select name="vessel" class="filter form-control form-control-lg">
							<option value="-">-- select an option --</option>
							@foreach($vessels AS $vessel)
							<option value="{{ $vessel->id }}">{{ $vessel->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row justify-content-center">
				<div class="col-lg-2">
					<button type="button" class="btn btn-white btn-block" onclick="window.location.replace(location.pathname)">Reset</button>
				</div>
				<div class="col-lg-2">
					<button id="btn_filter" type="button" class="btn btn-primary btn-block">Apply Filter</button>
				</div>
			</div>
		</div>
	</form>
</div>

<ul class="nav nav-tabs">
	<li class="nav-items">
		<a href="#div_outward" data-toggle="tab" class="nav-link active">
			<span class="d-sm-none">Outward</span>
			<span class="d-sm-block d-none">Outward</span>
		</a>
	</li>
	<li class="nav-items">
		<a href="#div_inward" data-toggle="tab" class="nav-link">
			<span class="d-sm-none">Inward</span>
			<span class="d-sm-block d-none">Inward</span>
		</a>
	</li>
</ul>

<div class="tab-content">
	<div class="tab-pane fade active show" id="div_outward">
		<table class="table table-bordered table-valign-middle f-s-14">
			<thead class="thead-custom">
				<tr>
					<th>Vessel</th>
					<th class="no-wrap">Voyage</th>
					<th>Arrival Date</th>
					<th>Departure Date</th>
					<th width="1%"><!-- <input id="chk_all" type="checkbox"> --></th>
				</tr>
			</thead>
			<tbody class="bg-white">
				@if(count($voyages) > 0)
				@foreach($voyages AS $ea)
				<tr>
					<td>{{ $ea->vessel->name }}</td>
					<td>{{ $ea->voyage_id }}</td>
					<td class="no-wrap">
						@if($ea->ata_pol)
						{{ date('d/m/Y', strtotime($ea->ata_pol)) }} <span class="label label-inverse label-actual m-l-10">A</span>
						@else
						{{ date('d/m/Y', strtotime($ea->eta_pol)) }} <span class="label label-inverse label-estimate m-l-10">E</span>
						@endif
					</td>
					<td class="no-wrap">
						@if($ea->atd_pol)
						{{ date('d/m/Y', strtotime($ea->atd_pol)) }} <span class="label label-inverse label-actual m-l-10">A</span>
						@else
						{{ date('d/m/Y', strtotime($ea->etd_pol)) }} <span class="label label-inverse label-estimate m-l-10">E</span>
						@endif
					</td>
					<td>
						<button class="btn btn-success btn-view" data-vessel="{{ $ea->id }}" data-vessel-name="{{ $ea->vessel->name }}" data-voyage="{{ $ea->voyage_id }}"><i class="fa fa-eye m-r-5"></i> View BL</button>
					</td>
				</tr>
				@endforeach
				@else
				<tr>
					<td colspan="10" class="text-center">
						No result found!
					</td>
				</tr>
				@endif
			</tbody>
		</table>
	</div>
	<div class="tab-pane fade" id="div_inward">
		<table class="table table-bordered table-valign-middle f-s-14">
			<thead class="thead-custom">
				<tr>
					<th width="25%">Vessel</th>
					<th class="no-wrap">Voyage</th>
					<th width="1%"></th>
				</tr>
			</thead>
			<tbody class="bg-white">
				@foreach($inw_voyages AS $voy)
				<tr>
					<td>{{ $voy->vessel }}</td>
					<td>{{ $voy->voyage }}</td>
					<td>
						<button class="btn btn-success btn-view-inw" data-vessel-name="{{ $voy->vessel }}" data-voyage="{{ $voy->voyage }}"><i class="fa fa-eye m-r-5"></i> View BL</button>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="modal-view-bl">
	<div class="modal-dialog" style="min-width: 800px; color: #242a30">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">View BL</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<form id="form_print" autocomplete="off" method="GET" target="_blank" action="{{ action('PrintController@view_print_bl') }}">
					@csrf
					<table class="table table-borderless f-s-14">
						<tbody>
							<tr>
								<td width="142"><strong>Vessel</strong></td>
								<td id="title_vessel"></td>
							</tr>
							<tr>
								<td width="142"><strong>Voyage</strong></td>
								<td id="title_voyage"></td>
							</tr>
							<tr class="div_format">
								<td width="142" class="no-wrap"><strong>BL Format</strong></td>
								<td>
									<select id="txt_blformat" class="form-control form-control-lg" name="txt_blformat">
										@if(env("SITE") == "SHS")
										<option value="apollo">Apollo Agencies (1980)</option>
										<option value="malsuria">Malsuria</option>
										@else
										<option value="malsuria">Malsuria</option>
										<option value="apollo">Apollo Agencies (1980)</option>
										@endif
									</select>
								</td>
							</tr>
							<tr class="div_format">
								<td width="142" class="no-wrap"><strong>Company Name</strong></td>
								<td>
									<input type="text" class="form-control form-control-lg m-b-10" name="txt_companyname1" maxlength="100" placeholder="Enter Company Name">
									<input type="text" class="form-control form-control-lg" name="txt_companyname2" maxlength="100" placeholder="Enter Company Name">
								</td>
							</tr>
						</tbody>
					</table>

					<div style="max-height: 500px; overflow-y: scroll;">
						<table id="table_view" class="table table-bordered table-striped table-valign-middle f-s-14">
							<thead>
								<tr>
									<th width="1%"><input id="chk_all" type="checkbox"></th>
									<th width="1%" class="no-wrap">BL No.</th>
									<th>POD</th>
								</tr>
							</thead>
							<tbody id="tbody_view">

							</tbody>
						</table>
					</div>
					
				</form>
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Return to Voyage List</a>

				<form id="form_arr" target="_blank" action="{{ action('PrintController@print_freightlist') }}">
					<input hidden="" name="bl">
				</form>

				<button disabled="" id="btn_manifest" type="button" class="btn btn-primary btn_print" value="manifest"><i class="fas fa-print m-r-5"></i>Print Manifest</button>
				<button disabled="" id="btn_freight" type="button" class="btn btn-primary btn_print" value="freight"><i class="fas fa-print m-r-5"></i>Print Freight List</button>

				<button disabled="" id="btn_print_so" type="button" class="btn btn-primary btn_print" value="so"><i class="fa fa-print m-r-5"></i>Print Shipping Order</button>
				<button disabled="" id="btn_print" type="button" class="btn btn-primary btn_print" value="bl"><i class="fa fa-print m-r-5"></i>Print BL</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-view-inw-bl">
	<div class="modal-dialog" style="min-width: 800px; color: #242a30">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">View BL</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<table class="table table-borderless f-s-14">
					<tbody>
						<tr>
							<td width="142"><strong>Vessel</strong></td>
							<td id="title_vessel_inw"></td>
						</tr>
						<tr>
							<td width="142"><strong>Voyage</strong></td>
							<td id="title_voyage_inw"></td>
						</tr>
					</tbody>
				</table>
				<table id="table_view_inw" class="table table-bordered table-striped table-valign-middle f-s-14">
					<thead>
						<tr>
							<th width="1%"><input id="chk_all_inw" type="checkbox"></th>
							<th width="1%" class="no-wrap">BL No.</th>
							<th>POD</th>
						</tr>
					</thead>
					<tbody id="tbody_view_inw">

					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<form target="_blank" action="{{ action('PrintController@print_manifest_inw') }}" method="GET">
					<input hidden="" id="input_bl_inw" name="bl">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Return to Voyage List</a>
					<button disabled="" type="button" class="btn btn-primary btn_manifest_inw"><i class="fas fa-print m-r-5"></i>Print Manifest</button>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

@section('page_script')
<link rel="stylesheet" href="/plugins/bootstrap-datepicker/bootstrap-datepicker.css">
<link rel="stylesheet" href="/plugins/bootstrap-datepicker/bootstrap-datepicker3.css">

<script src="/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
	$(function(){

		$("#btn_filter").click(function(){
			$(".filter").each(function(){
				if($(this).val() == "-" || $(this).val() == ""){
					$(this).removeAttr("name");
				}
			});
			$(this).closest("form").submit();
		});

		var select_format = false;

		$(".btn_print").click(function(){
			var type = $(this).val();

			var checked = [];

			$("input[name='chk_bl[]']:checked").each(function(){
				checked.push(parseInt($(this).val()));
			});

			switch(type){
				case "so":
				$("#form_print").attr("method", "post").attr("action", "{{ action('PrintController@view_print_so') }}").submit();
				break;
				case "bl":
				$("#form_print").attr("method", "post").attr("action", "{{ action('PrintController@view_print_bl') }}").submit();
				break;
				case "manifest":
				$("input[name='bl']").val(checked.toString());
				$("#form_arr").attr("action", "{{ action('PrintController@print_manifest') }}").submit();
				// window.open($(this).attr("href"), '_blank');
				return;
				break;
				case "freight":
				$("#form_arr input[name='bl']").val(checked.toString());
				$("#form_arr").attr("action", "{{ action('PrintController@print_freightlist') }}").submit();
				// window.open($(this).attr("href"), '_blank');
				return;
				break;
			}
		});

		$(".btn_manifest_inw").click(function(){
			var checked = [];

			$("input[name='chk_bl_inw[]']:checked").each(function(){
				checked.push(parseInt($(this).val()));
			});

			$("#input_bl_inw").val(checked.toString());

			$(this).closest("form").submit();
		});

		$(document).on("change", ".chk_bl", function(){
			if($(".chk_bl").length == $(".chk_bl:checked").length){
				$("#chk_all").prop("checked", true);
			} else {
				$("#chk_all").prop("checked", false);
			}

			if($(".chk_bl:checked").length > 0){
				$(".btn_print").removeAttr("disabled");
			} else {
				$(".btn_print").attr("disabled", "");
			}
		});

		$("#modal-view-bl").on('hidden.bs.modal', function(){
			$("#chk_all").prop("checked", false).trigger('change');
		});

		$("#chk_all").change(function(){
			if($(this).is(":checked")){
				$(".chk_bl").prop("checked", true).trigger('change');
			} else {
				$(".chk_bl").prop("checked", false).trigger('change');
			}
		});

		// $(document).on('focus', '.datetimepicker', function() {
		// 	$(this).datetimepicker({
		// 		format: 'DD/MM/YYYY'
		// 	});
		// });

		// $('.datepicker').datepicker({
		// 	todayHighlight: true
		// });

		$(".input-daterange").datepicker({
			todayHighlight: false,
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('#start_date').on('changeDate', function(ev){
			$("#end_date").focus();
		});

		$(document).on('click', '.btn-view', function() {
			var vessel_id = $(this).attr("data-vessel");
			var vessel_name = $(this).attr("data-vessel-name");
			var voyage_no = $(this).attr('data-voyage');

			$("#title_vessel").text(vessel_name);
			$("#title_voyage").text(voyage_no);

			$.ajax({
				type: "GET",
				url: '/print/get-bl/' + vessel_id,
				success: function(data){
					if(data.success){
						var bls = data.bls;
						var html = "";

						$("#btn_manifest").attr("href", "/voyage/" + data.hashed + "/print-manifest");
						$("#btn_freight").attr("href", "/voyage/" + data.hashed + "/print-fl");

						if(bls.length > 0){
							bls.forEach(function(elem, index){
								const { id, bl_no, destination } = elem;
								html += `<tr><td width='1%'><input type='checkbox' class='chk_bl' name='chk_bl[]' value='${id}'></td>` +
								`<td>${bl_no}</td>` +
								`<td>${destination}</td>` +
								`</tr>`;
							});

						} else {
							html += "<tr><td colspan='4' class='text-center'>No Bill Ladings found for this selected voyage!</td></tr>;"
						}

						$("#tbody_view").empty().append(html);
						$("#modal-view-bl").modal("toggle");
					}
				}
			});
		});

		$(document).on("click", ".btn-view-inw", function(){
			var vessel_name = $(this).attr("data-vessel-name");
			var voyage_no = $(this).attr("data-voyage");

			$("#title_vessel_inw").text(vessel_name);
			$("#title_voyage_inw").text(voyage_no);

			//Retrieve all BL by the same voyage name
			$.ajax({
				type: "GET",
				url: '/print/get-bl-inw/' + voyage_no,
				success: function(data){
					console.log(data);
					var bls = data.bls;
					var html = "";

					if(bls.length > 0){
						bls.forEach(function(bl, index){
							html += "<tr>" +
							"<td width='1%'><input type='checkbox' class='chk_bl_inw' name='chk_bl_inw[]' value='" + bl.id + "'>" +
							"<td>" + bl.bl_no + "</td>" +
							"<td>" + bl.pod_name + "</td>" +
							"</tr>";
						});
					} else {
						html += "<tr><td colspan='4' class='text-center'>No Bill Ladings found for this selected voyage!</td></tr>";
					}
					
					$("#tbody_view_inw").empty().append(html);
					$("#chk_all_inw").prop("checked", false);
					$("#modal-view-inw-bl").modal("toggle");
				}
			});
		});

		$(document).on("change", ".chk_bl_inw", function(){
			if($(".chk_bl_inw").length == $(".chk_bl_inw:checked").length){
				$("#chk_all_inw").prop("checked", true);
			} else {
				$("#chk_all_inw").prop("checked", false);
			}

			if($(".chk_bl_inw:checked").length > 0){
				$(".btn_manifest_inw").removeAttr("disabled");
			} else {
				$(".btn_manifest_inw").attr("disabled", "");
			}
		});

		$("#chk_all_inw").change(function(){
			if($(this).is(":checked")){
				$(".chk_bl_inw").prop("checked", true).trigger('change');
			} else {
				$(".chk_bl_inw").prop("checked", false).trigger('change');
			}
		});

	});
</script>
@stop