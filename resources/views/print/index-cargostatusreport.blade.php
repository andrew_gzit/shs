@extends("layouts.nav")
@section('page_title', 'Cargo Status Report')

@section('content')
<h1 class="page-header">Cargo Status Report</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{!! session('msg') !!}
	{{ session::forget('msg') }}
</div>
@endif

<div class="panel panel-inverse hidden-print">
	<div class="panel-body p-20">
		<div class="row">
			<div class="col-lg-3">
				<div class="form-group">
					<label>Vessel</label>
					<select id="ddl_vessel" class="ddl form-control form-control-lg">
						<option disabled="" selected="">-- select an option --</option>
						@foreach($vessels AS $ves)
						<option value="{{ $ves->id }}">{{ $ves->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label>Voyage</label>
					<select id="ddl_voyage" class="ddl form-control form-control-lg" disabled="">
						<option disabled="" selected="">-- select an option --</option>
					</select>
				</div>
			</div>
			<div class="col-lg-3 d-flex" style="align-items: flex-end;">
				<div class="form-group">
					<button id="btn_add" class="btn btn-primary btn-action" style="display: block">Add</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-inverse">
	<div class="panel-body p-20">
		<div id="div_report" class="f-s-15 text-black">
		</div>
	</div>
</div>

<button type="button" id="btn_clear" class="btn btn-danger btn-action hidden-print">Clear</button>
<button type="button" onclick="window.print();" class="btn btn-success btn-action hidden-print pull-right">Print</button>
@stop

@section("page_script")
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script type="text/javascript">
	$(function(){

		//Store voyage that has been added into the report
		var voy_id = [];

		$(".ddl").select2();

		$("#ddl_vessel").change(function(){
			retrieveVoyages($(this).val());
		});

		$("#btn_add").click(function(e){
			if(voyage_id != ""){
				var valid = false;
				$(this).attr("disabled", "");
				var voyage_id = $("#ddl_voyage").val();
				
				if(voy_id.indexOf(voyage_id) == -1){
					retrieveCargoStatus(voyage_id);
				} else {
					swal("Oops!", "You have already added this voyage", "warning");
					$(this).removeAttr("disabled");
				}
			}
		});

		$(document).on("click", ".btn-delete", function(){
			var div = $(this).closest("div");
			var voyage_id = $(this).attr("data-voyage");
			swal({
				title: "Remove this voyage from report?",
				text: "Press Continue to proceed",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Continue',
				cancelButtonText: 'Cancel',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					var index = voy_id.indexOf(voyage_id);
					div.remove();
					voy_id.splice(index, 1);
				}
			});
		});

		$("#btn_clear").click(function(){
			swal({
				title: "Remove all voyage from report?",
				text: "Press Continue to proceed",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Continue',
				cancelButtonText: 'Cancel',
				reverseButtons: true
			}).then((result) => {
				voy_id = [];
				$("#div_report").empty();
			});
		});

		//Retrieve list of voyage by vessel ID
		function retrieveVoyages(vessel_id){
			$.ajax({
				type: "POST",
				data: {id: vessel_id},
				url: '/bl/voyages/list',
				success: function(data){
					if(data.success == true) {
						var html = '<option disabled="" selected="" value="-">-- select an option --</option>';
						var added = 0;

						for(var i = 0; i < data.result.length; i++) {
							if(data.result[i].booking_status == 0){
								added++;
								html += '<option value="' + data.result[i].id + '">' + data.result[i].voyage_id + '</option>';
							}
						}

						if(added > 0) {
							$('#ddl_voyage').html(html).removeAttr("disabled");

						} else {
							$("#ddl_voyage").html('<option disabled="" selected="" value="-">-- select an option --</option>').attr("disabled", "");
						}
					} else {
						$('#ddl_voyage').html('<option disabled="" selected="" value="-">-- select an option --</option>').attr("disabled", "");
					}
				}
			});
		}

		//Retrieve cargo status by voyage ID
		function retrieveCargoStatus(voyage_id){
			var url = "/cargo-status/" + voyage_id;
			$.get(url, function(data){
				$("#div_report").append(data.cargo_status);
				voy_id.push(voyage_id);
				$("#btn_add").removeAttr("disabled");
			});
		}

		$( window ).resize(function() {
			$(".ddl").select2();
		});
	});
</script>

<style>
@media print{
	@page{
		margin-top: 0.5in;
	}
}
</style>
@stop