@extends("layouts.nav")
@section('page_title', 'Print Manifest')

@section('breadcrumb')
<ol class="breadcrumb pull-right btn-hidden">
	<li>
		<!-- <button type="button" onclick="window.print();" class="btn btn-success">
			<i class="fa fa-print m-r-5"></i>
			Print
		</button> -->
		<!-- <a href="/print/bl?tab=inward" class="btn btn-white">Return to Print Shipping Documents</a> -->
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Print Manifest</h1>

<ul class="nav nav-tabs">
	@foreach($ports AS $portname => $port)
	<li class="nav-items">
		<a href="#{{ $portname }}" data-toggle="tab" class="nav-link {{ $loop->first ? 'active' : '' }}">
			<span class="d-sm-none">Tab 1</span>
			<span class="d-sm-block d-none">{{ $portname }}</span>
		</a>
	</li>
	@endforeach
</ul>

<div class="tab-content">
@foreach($ports AS $portname => $port)
	<div class="tab-pane fade {{ $loop->first ? 'active show' : '' }}" id="{{ $portname }}">
		<object data="{{ $port }}" type="application/pdf" style="width: 100%; height: 80vh">
			alt : <a href="data/test.pdf">{{ $port }}.pdf</a>
		</object>
	</div>
@endforeach
</div>
@stop