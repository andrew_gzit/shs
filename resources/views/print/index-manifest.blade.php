@extends("layouts.nav")
@section('page_title', 'Print Manifest')

@section('breadcrumb')
<ol class="breadcrumb pull-right btn-hidden">
	<li>
		<a href="{{ action('PrintController@index_bl') }}" onclick="window.history.back();" class="btn-hidden"><button type="button" id="btn-print" style="display: inline-block" class="btn btn-white">Return to Schedule</button></a>
	<!-- 	<button type="button" onclick="window.print();" class="btn btn-success">
			<i class="fa fa-print m-r-5"></i>
			Print
		</button> -->
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Print Manifest</h1>

<ul class="nav nav-tabs">
	@if($merged_filename != null)
	<li class="nav-items">
		<a href="#merged" data-toggle="tab" class="nav-link">
			<span class="d-sm-none">ALL</span>
			<span class="d-sm-block d-none">ALL</span>
		</a>
	</li>
	@endif

	@foreach($ports AS $portname => $port)
	<li class="nav-items">
		<a href="#{{ $portname }}" data-toggle="tab" class="nav-link">
			<span class="d-sm-none">Tab 1</span>
			<span class="d-sm-block d-none">{{ $portname }}</span>
		</a>
	</li>
	@endforeach
</ul>

<div class="tab-content">
	@if($merged_filename != null)
	<div class="tab-pane fade" id="merged">
		<object data="/manifest_export/{{ $merged_filename }}.pdf" type="application/pdf" style="width: 100%; height: 80vh">
			alt : <a href="data/test.pdf">{{ $merged_filename }}.pdf</a>
		</object>
	</div>
	@endif

	@foreach($ports AS $portname => $port)
	<div class="tab-pane fade" id="{{ $portname }}">
		<object data="{{ $port }}" type="application/pdf" style="width: 100%; height: 80vh">
			alt : <a href="data/test.pdf">{{ $port }}.pdf</a>
		</object>
	</div>
	@endforeach
</div>
@stop

@section("page_script")
<script type="text/javascript">
	$(function(){
		$(".nav-tabs .nav-items:first").find("a").addClass("active");
		$(".tab-pane:first").addClass("active show");
	});
</script>

<style type="text/css">
@media print{
	@page{
		size: 20in 14in;
	}

	div.panel{
		overflow-x: hidden !important;
	}
}

body{
	-webkit-print-color-adjust: exact;
}
</style>
@stop