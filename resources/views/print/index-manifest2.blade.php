@extends("layouts.nav")
@section('page_title', 'Print Manifest')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item">
		<button id="btn-create-bl" type="button" class="btn btn-primary m-r-10">
			<i class="fa fa-sliders-h m-r-5"></i>
			Filter
		</button>
		<button type="button" class="btn btn-success pull-right m-b-20">
			<i class="fa fa-print m-r-5"></i>
			Print Selected
		</button>
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Print Manifest</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{!! session('msg') !!}
	{{ session::forget('msg') }}
</div>
@endif

<div class="panel panel-inverse">
	<div class="panel-heading">
		<h5 class="m-b-0 text-white">Filters</h5>
	</div>
	<form method="GET">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<div>
							<label>Vessel</label>
							<label class="lbl-filter-remove" style="display: none">Remove</label>
						</div>
						<select id="ddl_vessel" data-type="Vessel" name="vessel" class="ddl form-control form-control-lg">
							<option value="-">-- select an option --</option>
							@foreach($vessels AS $vessel)
							<option value="{{ $vessel->id }}">{{ $vessel->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<div>
							<label>Shipper</label>
							<label class="lbl-filter-remove" style="display: none">Remove</label>
						</div>
						<select id="ddl_shipper" name="shipper" class="ddl form-control form-control-lg">
							<option value="-">-- select an option --</option>
							@foreach($shippers AS $shipper)
							<option value="{{ $shipper->id }}">{{ $shipper->getCompanyOfType()->code }} - {{ $shipper->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<div>
							<label>Port of Discharge</label>
							<label class="lbl-filter-remove" style="display: none">Remove</label>
						</div>
						<select id="ddl_pod" name="pod" class="ddl form-control form-control-lg">
							<option value="-">-- select an option --</option>
							@foreach($ports AS $port)
							<option value="{{ $port->id }}">{{ $port->code }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<div>
							<label>Bill Of Lading</label>
							<label class="lbl-filter-remove" style="display: none">Remove</label>
						</div>
						<select id="ddl_bl" name="bl" class="ddl form-control form-control-lg">
							<option value="-">-- select an option --</option>
							@foreach($bls AS $bl)
							<option value="{{ $bl->id }}">{{ $bl->bl_no }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<p class="m-b-0"><i>* Filtering Bill of Lading will remove other filter options</i></p>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row justify-content-center">
				<div class="col-lg-2">
					<button type="button" class="btn btn-white btn-block">Reset</button>
				</div>
				<div class="col-lg-2">
					<button id="btn_filter" type="button" class="btn btn-primary btn-block">Apply Filter</button>
				</div>
			</div>
		</div>
	</form>
</div>

<table class="table-manifest-list table table-bordered table-valign-middle f-s-14">
	<thead class="thead-custom">
		<tr>
			<th width="1%"><input id="chk_all" type="checkbox"></th>
			<th width="1%" class="no-wrap">BL No.</th>
			<th width="1%">Vessel</th>
			<th width="1%" class="no-wrap">Voyage No.</th>
			<th width="1%">POL</th>
			<th width="1%">POD</th>
			<th>Shipper</th>
			<th>Consignee</th>
			<th>Name of Captain</th>
		</tr>
	</thead>
	<tbody class="bg-white">
		@if(count($filter) > 0)
		@foreach($filter AS $ea)
		<tr>
			<td>
				<input type="checkbox" class="chk_bl">
			</td>
			<td>{{ $ea->bl_no }}</td>
			<td>{{ $ea->getVessel->name }}</td>
			<td>{{ $ea->getVoyage->voyage_id }}</td>
			<td>{{ $ea->pol->code }}</td>
			<td>{{ $ea->pod->code }}</td>
			<td>{{ $ea->shipper->name }}</td>
			<td>{{ $ea->consignee->name }}</td>
			<td></td>
		</tr>
		@endforeach
		@else
		<tr>
			<td colspan="9" class="text-center">
				No result found!
			</td>
		</tr>
		@endif
	</tbody>
</table>
@stop

@section("page_script")
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script type="text/javascript">
	$(function(){

		$(".chk_bl").change(function(){
			if($(".chk_bl").length == $(".chk_bl:checked").length){
				$("#chk_all").prop("checked", true);
			} else {
				$("#chk_all").prop("checked", false);
			}
		});

		$("#chk_all").change(function(){
			if($(this).is(":checked")){
				$(".chk_bl").prop("checked", true);
			} else {
				$(".chk_bl").prop("checked", false);
			}
		});

		$(window).resize(function() {
			$(".ddl").select2();
		});

		$(".ddl").select2();

		$(".lbl-filter-remove").click(function(){
			$(this).closest(".form-group").find("select").val("-").trigger('change.select2');
			$(this).hide();
		});

		$(".ddl").change(function(){
			var $remove = $(this).closest(".form-group").find(".lbl-filter-remove");
			if($(this).val() != "-"){
				$remove.show();
			} else {
				$remove.hide();s
			}

			if(this.id == "ddl_bl"){
				$(".ddl").not($(this)).val("-").trigger('change.select2').closest(".form-group").find(".lbl-filter-remove").hide();
			} else {
				$("#ddl_bl").val("-").trigger('change.select2').closest(".form-group").find(".lbl-filter-remove").hide();
			}
		});

		$("#btn_filter").click(function(){
			$(".ddl").each(function(){
				if($(this).val() == "-"){
					$(this).removeAttr("name");
				}
			});
			$(this).closest("form").submit();
		});
	});
</script>
@stop