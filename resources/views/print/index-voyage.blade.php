@extends("layouts.nav")
@section('page_title', 'Print Voyage Schedule')

@section('content')
<h1 class="page-header">Print Voyage Schedule</h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{!! session('msg') !!}
	{{ session::forget('msg') }}
</div>
@endif

<div class="panel panel-inverse btn-hidden">
	<div class="panel-heading">
		<h5 class="m-b-0 text-white">Filters</h5>
	</div>
	<form method="GET">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>Date Range</label>
						<div class="input-group input-daterange">
							<input type="text" id="start_date" class="filter form-control form-control-lg" autocomplete="off" name="start_date" placeholder="Date Start" />
							<span class="input-group-addon" style="line-height: 33px;">to</span>
							<input type="text" id="end_date" class="filter form-control form-control-lg" autocomplete="off" name="end_date" placeholder="Date End" />
						</div>
					</div>
				</div>
				<!-- <div class="col-lg-4">
					<div class="form-group">
						<label>Voyage Status</label>
						<select class="form-control form-control-lg" name="status">
							<option value="1">On-Going</option>
							<option value="2">Completed</option>
							<option value="3">Cancelled</option>
						</select>
					</div>
				</div> -->
			</div>
		</div>
		<div class="panel-footer">
			<div class="row justify-content-center">
				<div class="col-lg-2">
					<button type="button" class="btn btn-white btn-block" onclick="window.location.replace(location.pathname)">Reset</button>
				</div>
				<div class="col-lg-2">
					<button id="btn_filter" type="submit" class="btn btn-primary btn-block">Apply Filter</button>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="panel panel-inverse">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				<button id="plain_format" class="btn btn-success pull-right m-b-10 btn-hidden"><i class="fa fa-print m-r-10"></i>Print Schedule</button>

				<div class="d-flex f-s-15 m-b-20">
					<div style="display: inline-block; color: #000;">
						<strong style="font-size: 24px">VOYAGE SCHEDULE</strong>
						<strong style="display: block">Date: {{ \Carbon\Carbon::now()->format('d.m.y') }}</strong>
					</div>
				</div>

				<table class="table table-condensed table-bordered table-striped f-s-14 table-valign-middle">
					<thead class="thead-custom text-center">
						<tr>
							<th rowspan="2" width="1%"></th>
							<th rowspan="2">Vessel</th>
							<th rowspan="2">SCN</th>
							<th rowspan="2">Voyage</th>
							<th colspan="2">Penang</th>
							<th colspan="3">Ports of Call</th>
							<th rowspan="2" width="1%">Booking Status</th>
						</tr>
						<tr>
							<th>ETA</th>
							<th>ETD</th>
							<th>ETA</th>
							<th>ETA</th>
							<th>ETA</th>
						</tr>
					</thead>
					<tbody>
						@foreach($voyages AS $voy)
						<tr style="color: #000">
							<td class="text-center">
								@if($voy->vessel->semicontainer_service == 1)
								*
								@endif
							</td>
							<td>
								{{ $voy->vessel->name }}
							</td>
							<td>{{ $voy->scn }}</td>
							<td>{{ $voy->voyage_id }}</td>
							<td class="text-center">{{ $voy->eta_pol->format('d M\' y') }}</td>
							<td class="text-center">{{ $voy->etd_pol->format('d M\' y') }}</td>

							@php
							$counter = 0;
							@endphp

							@foreach($voy->destinations AS $dest)

							@if($dest->eta > $now)
							
							@if($loop->index != 0)
							@php
							$counter++;
							@endphp
							<td class="text-center"><strong>{{ $dest->getPort->location }}</strong><br>{{ $dest->eta->format('d M\' y') }}</td>
							@endif

							<!-- If loop is equal to total loop count, and it is less than tree, needs to put empty cells -->
							@if($loop->iteration == $loop->count && $counter <= 4)
							@for ($i = 0; $i < 3 - $counter; $i++)
							<td></td>
							@endfor
							@endif

							@endif
							@if($counter == 3)
							@break
							@endif

							@endforeach
							<td class="text-center f-w-600">
								@switch($voy->booking_status)
								@case(0)
								Open
								@break 
								@case(1)
								Closed
								@break 
								@case(2)
								Cancelled
								@break 
								@endswitch
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>

				<p style="color: #000">
					Vessels with asterisk (*) provide semi-container service.
				</p>

				<p style="color: #000">The estimated dates are given without guarantee and subject to change without prior notice. Please feel free to contact us should you need to confirm on the schedules for booking of space</p>

				<div style="width: 100%; text-align: right; color: #000;">
					<strong style="margin-bottom: 0 !important;">{{ $company->name }} <span style="font-size: 12px">({{ $company->roc_no }})</span></strong>
					<p>
						{!! nl2br($company->getAddresses()->first()->address) !!}
						Tel. {{ $company->telephone }} &nbsp; &nbsp; Fax. {{ $company->fax }}<br>
						Email. {{ $company->getEmails()->first()->email }}<br>
					</p>
				</div>

			</div>
		</div>
	</div>
</div>
@stop

@section("page_script")
<link rel="stylesheet" href="/plugins/bootstrap-datepicker/bootstrap-datepicker.css">
<link rel="stylesheet" href="/plugins/bootstrap-datepicker/bootstrap-datepicker3.css">

<script src="/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="/js/print-format.js"></script>

<script type="text/javascript">
	$(function(){
		$(".input-daterange").datepicker({
			todayHighlight: false,
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$("#start_date").datepicker()
		.on("changeDate", function(e) {
			$("#end_date").focus();
		});
	});
</script>

<style type="text/css">
	@media print{

		@page{
			margin: 0;
			size: auto;
		}

		.table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
			background: #f2f3f4 !important;
			color: #000;
			-webkit-print-color-adjust: exact;
		}
	}
</style>
@stop