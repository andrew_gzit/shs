<html>
<head>
	<style>
	*, ::after, ::before {
		box-sizing: border-box;
	}

	.top3{
		height: 20mm !important;
		width: 100mm;
	}

	.top-bl{
		width: 104mm;
		text-align: center;
	}

	.bl-label{
		display: block;
		/*margin-bottom: 5px;*/
		font-size: 11px;
		font-weight: 400;
		padding-top: 0px;
	}

	.bl-content{
		display: block;
		padding-left: 10px;
		padding-bottom: 0px;
		font-weight: 400 !important;
		font-size: 11px !important;
	}

	.no-bottom-border{
		border-bottom: none;
	}

	.table-apollo-header{
		font-size: 11px;
		font-weight: 400 !important;
	}

	.table-apollo-header td{
		/*border: none !important;*/
	}

	.table-apollo-header .container-markings{
		width: 32.55mm;
	}

	.table-apollo-header .num-packages{
		width: 70mm;
		text-align: left;
	}

	.table-apollo-header .width{
		width: 32.55mm;
	}

	.p-0{
		padding: 0 !important;
	}

	.bl-lbl-goods {
		display: block;
		text-align: center;
		font-size: 13px;
		font-weight: 400;
		margin-top: 10px;
	}

	td{
		border: 1px solid #000;
		padding: 5px;
		vertical-align: top;
	}

	main{
		padding: 5px;
		font-size: 11px;
	}

	.table-borderless td{
		border: 0;
	}

	.detailed_desc{
		/*white-space: pre;*/
	}

	@page { margin-top: 450px; margin-bottom: 345px; margin-left: 0; margin-right: 0; size: A4 }
	header { position: fixed; top: -373px; left: -3px; right: 0px; height: 500px; width: 100% }
	footer { position: fixed; bottom: -255px; left: 0px; right: 0px; height: 350px; }
	/*p { page-break-after: always; }
	p:last-child { page-break-after: never; }*/

	.table{
		width: 100%;
		border-collapse: collapse;
	}

	.bottom-small{
		height: 9mm !important;
	}

	body{
		font-weight: 400;
	}
</style>

@if($format_type == "form")
<style type="text/css">
.print-hidden, .bl-label{
	visibility: hidden;
}

table, td{
	border-color: #fff !important;
}

body{
	border-color: #fff !important;
}
</style>
@endif

</head>

<body style="border-left: .5px solid #000; border-right: .5px solid #000; margin-top: -6px;">
	<header>
		<table class="report-container no-bottom-border">
			<thead class="report-header">
				<tr>
					<th class="report-header-cell no-bottom-border">
						<div class="header-info">
							<table class="table  bg-white text-black">
								<tr>
									<td class="top3" colspan="2">
										<label class="bl-label">Shipper</label>
										<span class="bl-content">{{ $bl->shipper_name }}<br>{!! nl2br($bl->shipper_add) !!}</span>
									</td>
									<td class="top-bl" rowspan="3" colspan="2">
										<div class="bl-company">
											<h2 class="print-hidden">BILL OF LADING</h2>
											@if($company_name1 || $company_name2)
											<div style="padding-top: 25mm">
												<span style="font-size: 20px;">{{ $company_name1 }}<br>{{ $company_name2 }}</span>
											</div>
											@endif
										</div>
									</td>
								</tr>
								<tr>
									<td class="top3" colspan="2">
										<label class="bl-label">Consignee</label>
										<span class="bl-content">{{ $bl->consignee_name }}<br>{!! nl2br($bl->consignee_add) !!}</span>
									</td>
								</tr>
								<tr>
									<td class="top3" colspan="2">
										<label class="bl-label">Notify Party</label>
										<span class="bl-content">
											@if($bl->notify_add == $bl->consignee_add)
											SAME AS CONSIGNEE
											@else
											{{ $bl->notify_name }}<br>{!! nl2br($bl->notify_add) !!}
											@endif
										</span>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<label class="bl-label">Vessel</label>
										<span class="bl-content">{{ $bl->getVessel->name }}</span>
									</td>
									<td>
										<label class="bl-label">Voy. No.</label>
										<span class="bl-content">{{ $bl->getVoyage->voyage_id }}</span>
									</td>
									<td>
										<label class="bl-label">B/L No.</label>
										<span class="bl-content">{{ $bl->bl_no }}</span>
									</td>
								</tr>
								<tr>
									<td>
										<label class="bl-label">Port of Loading</label>
										<span class="bl-content">{{ strtoupper($bl->pol_name) }}</span>
									</td>
									<td>
										<label class="bl-label">Port of Discharge</label>
										<span class="bl-content">{{ strtoupper($bl->pod_name) }}</span>
									</td>
									<td>
										<label class="bl-label">Freight Payable at</label>
										<span class="bl-content">PENANG</span>
									</td>
									<td>
										<label class="bl-label">No. of Original Bs/L</label>
										<span class="bl-content">{{ $bl->getNoBL() }}</span>
									</td>
								</tr>
								<tr>
									<td class="no-bottom-border">
										<label class="bl-label">Marks and numbers</label>
										<span class="bl-content">&nbsp;</span>
									</td>
									<td colspan="2" class="no-bottom-border">
										<label class="bl-label">Number and kind of packages : Description of goods</label>
										<span class="bl-content">&nbsp;</span>
									</td>
									<td class="no-bottom-border">
										<label class="bl-label">Gross weight : Measurement</label>
										<span class="bl-content">&nbsp;</span>
									</td>
								</tr>
								<!-- <tr>
									<td colspan="4" class="p-0 no-bottom-border">
										<table class="table table-apollo-header table-valign-top m-b-0">
											<tbody class="print-hidden">
												<tr>
													<td style="border-bottom: none" class="container-markings">Marks and numbers</td>
													<td style="border-bottom: none" class="num-packages">Number and kind of packages : Description of goods</td>
													<td style="border-bottom: none" class="width">Gross weight : Measurement</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr> -->
							</table>
						</div>
					</th>
				</tr>
			</thead>
		</table>
	</header>

	<footer>
		<div id="pagenum"></div>
		<table class="table">
			<tr>
				<td style="width: 50%">
					<label class="bl-label">Total Number of Packages (in words) :</label>
					<span class="bl-content">{{ $bl->getNoPackages() }}</span>
				</td>
				<td style="width: 50%; height: 7mm">
					<label class="bl-label">Place and date of issue</label>
					<span class="bl-content">{{ $bl->pol_name . ', ' . $bl->getBL_POL()->eta->format('d/m/Y') }}</span>
				</td>
			</tr>
			<tr>
				<td style="height: 37mm;">
					<label class="bl-label">Freight Charges</label>
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="print-hidden" style="text-align: center; font-size: 21px; height: 7mm;"><span style="font-weight: 600">AS AGENT FOR THE CARRIER</span><span style="font-weight: normal;">&nbsp;</span></td>
			</tr>
		</table>
	</footer>

	<main>
		@foreach($bl->getCargos AS $cargo)
		<div style="display: inline-block; padding-left: 5px; width: 200px; position: absolute; word-wrap: all; word-break: break-all;">
			@if($bl->vessel_type == "FCL")
			@foreach($cargo->containers AS $cont)
			{{ $cont->container_no . '/' . $cont->seal_no }}<br>
			@endforeach
			@endif
			{!! !empty($cargo->markings) ? nl2br($cargo->markings) . '<br>' : '' !!}
            {!! !empty($cargo->uncode) ? nl2br($cargo->uncode) . '<br>' : '' !!}
            {!! !empty($cargo->temperature) ? nl2br($cargo->temperature) . '<br>' : '' !!}
		</div>
		<div style="display: inline-block; position: absolute; width: 200px; left: 610px;">
			{{ number_format($cargo->weight, 3) . ' ' . $cargo->weight_unit . ' : ' }} {{ ($cargo->volume > 0 ) ? number_format($cargo->volume, 4) . ' M3' : '' }}
		</div>
		<div style="margin-left: 200px; word-wrap: all;">{{ $cargo->cargo_name . ' ' .  $cargo->cargo_desc}}<br><div style="white-space: pre;">{!! nl2br($cargo->detailed_desc) !!}</div>
		@endforeach
	</main>

<script type="text/php">
if ( isset($pdf) ) { 
    $pdf->page_script('
        if ($PAGE_COUNT > 1) {
            $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
            $size = 9;
            $pageText = "Page " . $PAGE_NUM . " of " . $PAGE_COUNT;
            $y = 200;
            $x = 540;
            $pdf->text($x, $y, $pageText, $font, $size);
        } 
    ');
}
</script>

</body>
</html>