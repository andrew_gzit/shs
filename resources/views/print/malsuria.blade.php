@extends("layouts.nav")
@section('page_title', 'Print BL')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li>
		@if(!empty($from))
		@if($from == "BL")
		<a href="{{ action('BillLadingController@index') }}" class="btn-hidden"><button type="button" id="btn-print" style="display: inline-block" class="btn btn-white m-r-5">Return to BL List</button></a>
		@else
		<a href="{{ action('BillLadingController@edit', $from) }}" class="btn-hidden"><button type="button" id="btn-print" style="display: inline-block" class="btn btn-white m-r-5">Return to BL Details</button></a>
		@endif
		@else
		<a href="{{ action('PrintController@index_bl') }}" class="btn-hidden"><button type="button" id="btn-print" style="display: inline-block" class="btn btn-white m-r-5">Return to Schedule</button></a>
		@endif
		
		<div class="dropdown" style="display: inline-block;">
			<button class="btn btn-success dropdown-toggle btn-hidden" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-print m-r-5"></i> Print
			</button>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
				<a class="dropdown-item" id="plain_format" href="#">Plain Format</a>
				<a class="dropdown-item" id="form_format" href="#">Form Format</a>
			</div>
		</div>
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Print Bill of Lading</h1>
@foreach($blArr AS $bl)
<table id="table_bl" class="table table-bordered bg-white table-bl table-malsuria" style="page-break-after: always;">
	<tbody>
		<tr>
			<td height="130" width="50%" colspan="2">
				<label class="bl-label">Shipper</label>
				<span class="bl-content">
					{{ $bl->shipper_name }}<br>
					{!! nl2br($bl->shipper_add) !!}
					
				</span>
			</td>
			<td rowspan="3" colspan="2" width="50%">
				<div class="bl-company p-t-0">
					<h2 class="m-b-30 print-hidden">BILL OF LADING</h2>
					<!--<div style="height: 170px">&nbsp;</div>-->
					@if($company_name1 || $company_name2)
					<h2 class="text-center d-block">
						{{ $company_name1 }}<br>
						{{ $company_name2 }}
					</h2>
					@endif
					<!-- <img src="/img/apollo-logo.jpeg" class="p-b-30">
					<h3 class="m-b-0">阿波罗代理(1980)有限公司<br>APOLLO AGENCIES (1980) SDN. BHD.</h3>
					<label>(Company No. 57467-U)</label> -->
				</div>
			</td>
		</tr>
		<tr>
			<td height="130" width="50%" colspan="2">
				<label class="bl-label">Consignee (if 'Order' state Notify Party)</label>
				<span class="bl-content">
					{{ $bl->consignee_name }}<br>
					{!! nl2br($bl->consignee_add) !!}
				</span>
			</td>
		</tr>
		<tr>
			<td height="130" width="50%" colspan="2">
				<label class="bl-label">Notify Party (only if not stated above: otherwise leave blank)</label>
				<span class="bl-content">
					@if($bl->notify_add == $bl->consignee_add)
					SAME AS CONSIGNEE
					@else
					{{ $bl->notify_name }}<br>
					{!! nl2br($bl->notify_add) !!}
					@endif
				</span>
			</td>
		</tr>
		<!-- <tr>
			<td height="1%" width="25%">
				<label class="bl-label">Local Vessel</label>
				<span class="bl-content">&nbsp;</span>
			</td>
			<td height="1%">
				<label class="bl-label">From</label>
				<span class="bl-content">&nbsp;</span>
			</td>
		</tr> -->
		<tr>
			<td colspan="2">
				<label class="bl-label bl-label-malsuria">Vessel:</label>
				<span class="bl-content">{{ $bl->getVessel->name }}</span>
			</td>
			<td>
				<label class="bl-label bl-label-malsuria">Voy. No.</label>
				<span class="bl-content">{{ $bl->getVoyage->voyage_id }}</span>
			</td>
			<td>
				<label class="bl-label bl-label-malsuria">B/L No.</label>
				<span class="bl-content">{{ $bl->bl_no }}</span>
			</td>
		</tr>
		<tr>
			<td>
				<label class="bl-label bl-label-malsuria">Port of Loading</label>
				<span class="bl-content">{{ strtoupper($bl->pol_name) }}</span>
			</td>
			<td>
				<label class="bl-label bl-label-malsuria">Port of Discharge</label>
				<span class="bl-content">{{ strtoupper($bl->pod_name) }}</span>
			</td>
			<td>
				<label class="bl-label bl-label-malsuria">Freight Payable at</label>
				<span class="bl-content">PENANG</span>
			</td>
			<td>
				<label class="bl-label bl-label-malsuria">No. of Original Bs/L</label>
				<span class="bl-content">{{ $bl->getNoBL() }}</span>
			</td>
		</tr>
		<tr class="row-malsuria-goods">
			<td width="25%">
				<label class="bl-label bl-label-malsuria">Marks and numbers:</label>
				<span class="bl-content">&nbsp;</span>
			</td>
			<td colspan="2">
				<label class="bl-label bl-label-malsuria">Number and kind of packages : Description of goods :</label>
				<span class="bl-content">&nbsp;</span>
			</td>
			<td width="25%">
				<label class="bl-label bl-label-malsuria">Gross weight : Measurement</label>
				<span class="bl-content">&nbsp;</span>
			</td>
		</tr>
		<!-- <tr>
			<td>
				<label class="bl-label">(Ocean) Vessel (Or Substituted equivalent)</label>
				<span class="bl-content">{{ $bl->getVessel->name }}</span>
			</td>
			<td>
				
			</td>
			
			
		</tr>
		<tr>
			
			<td>
				<label class="bl-label">Final Destination (if on carriage)</label>
			</td>
			
		</tr> -->
		<tr>
			<td colspan="4" class="p-0">
				<div style="height: 500px">
					<!-- <label class="bl-lbl-goods print-hidden">PARTICULARS FURNISHED BY SHIPPER OF GOODS</label> -->
					<table class="table table-borderless table-valign-top m-b-0">
					<!-- <thead class="print-hidden">
						<tr>
							<th class="no-wrap">Marks and numbers:</th>
							<th class="no-wrap">Number and kind of packages : Description of goods :</th>
							<th class="no-wrap">Gross weight : Measurement</th>
						</tr>
					</thead> -->
					<tbody class="bl_content_{{ $bl->id }}" data-id="{{ $bl->id }}">
				<!-- 		@foreach($bl->getCargos AS $cargo)
						<tr>
							<td width="1%" class="no-wrap">
								@foreach($cargo->containers AS $container)
								<span class="d-block">{{ $container->container_no }}/{{ $container->seal_no }}</span>
								@endforeach
							</td>
							<td width="1%">{{ $cargo->cargo_name }}</td>
							<td>{{ $cargo->cargo_desc }}</td>
							<td width="1%">{{ number_format($cargo->weight, 3) }}</td>
							<td width="1%">{{ number_format($cargo->volume, 3) }}</td>
						</tr>
						<tr>
							<td>{{ $cargo->remarks }}</td>
							<td></td>
							<td>{{ $cargo->detailed_desc }}</td>
							<td>
								<div class="sum_box">
									{{ number_format($bl->getCargos->sum('weight'), 3) }}
								</div>
							</td>
							<td>
								<div class="sum_box">
									{{ number_format($bl->getCargos->sum('volume'), 3) }}
								</div>
							</td>
						</tr>
						@endforeach -->
					</tbody>
				</table>
			</div>
			<label class="d-block div-paging pull-right p-r-20"></label>
				<!-- <div class="p-15 f-s-13" style="position: relative;">
					<label class="d-block m-0">FREIGHT {{ ($bl->freight_term == 0) ? "PREPAID" : "COLLECT" }}</label>
					<label>TOTAL NO. OF PACKAGES (IN WORDS) : {{ $bl->getNoPackages() }}</label>
				</div> -->
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<label class="bl-label bl-label-malsuria">Total Number of Packages (in words) :</label>
				<span class="bl-content">{{ $bl->getNoPackages() }}</span>
			</td>
			<td colspan="2">
				<label class="bl-label bl-label-malsuria">Place and date of issue</label>
				<span class="bl-content">{{ strtoupper($bl->pol_name) }}, {{ $bl->created_at->format("d/m/Y") }}</span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<label class="bl-label bl-label-malsuria">Freight and Charges :</label>
				<span class="bl-content">
					<div style="height: 130px">
						&nbsp;
					</div>
				</span>
			</td>
			<td colspan="2">
				<label class="bl-label bl-label-malsuria">&nbsp;</label>
				<span class="bl-content">&nbsp;</span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			</td>
			<td colspan="2" class="text-center">
				<h4 class="m-t-5 m-b-5">AS AGENT FOR THE CARRIER</h4>
			</td>
		</tr>
		<!-- <tr>
			<td rowspan="4" colspan="2">
				<div style="height: 500px">
					&nbsp;
				</div>
			</td>
			<td colspan="2">
				<div style="height: 150px">
					&nbsp;
				</div>
			</td>
		</tr> -->
		<!-- <tr>
			<td colspan="2">
				<label class="bl-label">Freight Charges:</label>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<label class="bl-label">Place and Date of Issues</label>
				<span class="bl-content"></span>
			</td>
		</tr> -->
		<!-- <tr>
			<td colspan="2">
				<label class="bl-label">IN WITNESS WHEREOF the above stated number of Bills of Lading all of this tenor and date have been signed one of which being accomplished, the others  to stand void.</label>
				<span class="bl-content">
					As Agent For The Carrier<br>
					APOLLO AGENCIES (1980) SDN. BHD.
				</span>
			</td>
		</tr> -->
	</tbody>
</table>
@endforeach
@stop

@section("page_script")
<script type="text/javascript" src="/js/print-format.js"></script>
<script type="text/javascript" src="/js/bl_malsuria.js"></script>

<script type="text/javascript">
	$(function(){
		$.ajaxSetup({async:false});

		var content = $("[class*='bl_content_']");
		// var bl_id = [];

		function checkHeight(param_bl_id){
			var height = $(".bl_content_" + param_bl_id).last().height();
			if(height >= 500){
				return "false";
			} else {
				return "true";
			}
		}

		var cargo_counter = 1;
		var container_counter = 1;

		function getCargo(bl_id){
			var $tbody = $(".bl_content_" + bl_id);
			var $table = $tbody.closest(".table-bl");
			
			var bl_content_counter = 1;

			$.get("/get-cargo/" + bl_id, function(data){
				if(data.success){
					var cargos = data.cargos;
					cargos.forEach(function(elem, index){

						var $tr = $("<tr id='cargo_id" + cargo_counter + "'></tr>");
						$tbody.append($tr);
						var container_td = "<td width='25%' id='container_id" + container_counter + "' data-complete='false'></td>";
						var $appended_td = $tbody.find("tr:last").append(container_td);

						var cargo_id = elem.id;

						var temp_bl_id = bl_id;

						$.get('/get-container/' + cargo_id, function(data){
							var containers = data.containers;

							containers.forEach(function(elem2, index){
								var bool = checkHeight(temp_bl_id);
								var span = "<span class='d-block'>" + elem2.container_no + "/" + elem2.seal_no + "</span>";

								//Reach height limit
								if(bool == "false"){

									$("#container_id" + container_counter).closest("tr").attr("data-complete", true);

									container_counter++;
									temp_bl_id = bl_id + "_" + bl_content_counter;
									bl_content_counter++;
									cargo_counter++;

									$clone = $table.clone();
									var $new_tbody = $clone.find("[class*='bl_content_']");
									$new_tbody.empty().removeClass().addClass("bl_content_" + temp_bl_id);
									var $new_tr = $("<tr id='cargo_id" + cargo_counter + "'></tr>");
									$new_tbody.append($new_tr);
									var new_td = "<td id='container_id" + container_counter + "'></td><td></td><td></td><td></td><td></td>";
									$new_tbody.find("tr:last").append(new_td);

									// $clone.find("[id*='container_id']").empty().attr("id", "container_id" + container_counter);
									// $tbody = $clone.find("[class*='bl_content_']").removeClass().addClass("bl_content_" + temp_bl_id);
									$table.after($clone);
								}

								$("#container_id" + container_counter).append(span);
							});
						});

						var marking_bool = checkHeight(bl_id);

						if(elem.markings != null){
							if(marking_bool == "false"){
								container_counter++;
								$clone = $table.clone();
								var $new_tbody = $clone.find("[class*='bl_content_']");
								$new_tbody.empty().removeClass().addClass("bl_content_" + temp_bl_id);
								var $new_tr = $("<tr id='cargo_id" + cargo_counter + "'></tr>");
								$new_tbody.append($new_tr);
								var new_td = "<td id='container_id" + container_counter + "'></td>";
								$new_tbody.find("tr:last").append(new_td);
								$("tbody[class^='bl_content_" + bl_id + "']:last").closest(".table-bl").after($clone);
							}

							$("#container_id" + container_counter).append("<div style='white-space: pre-line'>" + elem.markings + "</div>");
						}
						// $tr.find("td:first").append("<p>" + elem.markings + "</p>");

						if(marking_bool == "true"){
							cargo_counter++;
							container_counter++;
						}

						var cargo_name = elem.cargo_name;
						var desc = "";
						var cargo_detailed_desc = elem.detailed_desc;

						var cargo_uncode = elem.uncode;

						var cargo_remark = elem.remarks;
						var cargo_weight = elem.weight;
						var cargo_weight_unit = elem.weight_unit;
						var cargo_vol = elem.volume;

						// $tr.append("<td width='1%'>" + cargo_name + "</td>");

						// if(cargo_detailed_desc != ""){
						// 	desc += "<br><br>" + cargo_detailed_desc;
						// }

						// if(cargo_remark != ""){
						// 	desc += "<br><br>" + cargo_remark;
						// }

						var desc_bool = checkHeight(bl_id);

						var $desc = $tr.append("<td width='50%'>" + cargo_name + "<br>" + elem.cargo_desc + "</td>");

						if(desc_bool == "false"){

							$clone = $table.clone();
							var $new_tbody = $clone.find("[class*='bl_content_']");
							$new_tbody.empty().removeClass().addClass("bl_content_" + temp_bl_id);
							var $new_tr = $("<tr id='cargo_id" + cargo_counter + "'></tr>");
							$new_tbody.append($new_tr);
							var new_td = "<td id='container_id" + container_counter + "'></td>";
							$new_tbody.find("tr:last").append(new_td);
							$("tbody[class^='bl_content_" + bl_id + "']:last").closest(".table-bl").after($clone);

							var last_bl = $("[class^='bl_content_" + bl_id + "']:last");
							if(cargo_detailed_desc){
								last_bl.find("tr:last").append("<td></td><td></td>");
								last_bl.find("td:nth-child(2)").append("<div style='white-space: pre'>" + cargo_detailed_desc + "</div>");
							}

							if(cargo_uncode){
								last_bl.find("td:nth-child(1)").append("<div style='white-space: pre'>" + cargo_uncode + "</div>");
							}

							if(cargo_remark){
								last_bl.find("tr:last").find("td:nth-child(2)").append("<p>" + cargo_remark + "</p>");
							}
						} else {

							if(cargo_detailed_desc){
								if(checkHeight(bl_id)){
									$tr.find("td:nth-child(2)").append("<br><div style='white-space: pre-line'>" + cargo_detailed_desc + "</div>");
								} else {
									$("[class*='bl_content_" + bl_id + "']:last").find("tr:last").find("td:nth-child(3)").append("<br>" + cargo_detailed_desc);
								}
							}
							
							// if(cargo_detailed_desc){
							// 	if(checkHeight(bl_id)){
							// 		$tr.find("td:nth-child(2)").append("<br>" + cargo_detailed_desc);
							// 	} else {
							// 		$("[class*='bl_content_" + bl_id + "']:last").find("tr:last").find("td:nth-child(2)").append("<br>" + cargo_detailed_desc);
							// 	}
							// }
							

							if(cargo_remark){
								$tr.find("td:nth-child(2)").append("<br><br>" + cargo_remark);
							}
						}

						var cargo_str = "";

						if (cargo_weight == 0 || cargo_vol == 0) {
							cargo_str = "";
						} else {
							cargo_str = cargo_weight.toFixed(3) + " " + cargo_weight_unit + " : " + cargo_vol.toFixed(3);
						}

						$tr.append("<td>" + cargo_str + "</td>");
					});

					// printTotal(bl_id, data.weight, data.volume);
					printPaging(bl_id);
				}
			});
}

function printTotal(bl_id, weight, volume){
	var $tbody = $("[class*='bl_content_" + bl_id + "']:last");

	var html = "<tr>" + 
	"<td colspan='3'></td>" +
	"<td><div class='sum_box'>" + weight + "</div></td>" +
	"<td><div class='sum_box'>" + volume + "</div></td>";
	$tbody.append(html);
}

content.each(function(index, elem){
	var bl_id = $(elem).attr("data-id");
	getCargo(bl_id);
});
});

</script>

<style id="bl_print_css" type="text/css">

</style>
@stop