<html>
<head>
	<meta charset="utf-8" />

	<style>
		.desc{
			font-family: 'Helvetica', sans-serif;
			white-space: pre;
			color: black;
		}

		*, ::after, ::before {
			box-sizing: border-box;
		}

		body{
			font-family: sans-serif;
		}

		.top3{
			height: 20mm;
			width: 105mm;
		}

		.top-bl{
			width: 105mm;
			text-align: center;
		}

		.bl-label{
			display: block;
			margin-bottom: 5px;
			font-size: 9px;
			font-weight: 400;
			padding-top: 0px;
		}

		.bl-content{
			display: block;
			padding-left: 20px;
			padding-bottom: 5px;
			font-weight: 400 !important;
			font-size: 11px !important;
		}

		.no-bottom-border{
			border-bottom: none;
		}

		.table-apollo-header{
			font-size: 9px;
			font-weight: 400 !important;
		}

		.table-apollo-header td{
			border: none !important;
		}

		.table-apollo-header .container-markings{
			width: 50mm;
		}

		.table-apollo-header .num-packages{
			width: 43mm;
		}

		.table-apollo-header .description-goods{
		}

		.table-apollo-header .width{
			width: 25mm;
		}

		.bl-company h2{
			padding-top: 1in;
		}

		.p-0{
			padding: 0 !important;
		}

		.bl-lbl-goods {
			display: block;
			text-align: center;
			font-size: 13px;
			font-weight: 400;
			margin-top: 10px;
		}

		td{
			border: 1px solid #000;
			padding: 5px;
			vertical-align: top;
		}

		main{
			padding: 5px;
			margin-left: -5px;
			font-size: 11px;
		}

		.table-borderless td{
			border: 0;
		}

		.detailed_desc{
			/*white-space: pre;*/
		}

		.table-manifest{
			margin-top: 10px !important;
		}

		.table-manifest-header th{
			border-bottom: 1px solid #000;
		}

		@page { margin-top: 150px; margin-bottom: 5px; margin-left: 20px; margin-right: 20px; size: A4 landscape }
		header { position: fixed; top: -145px; left: 0px; right: 0px; width: 100%; font-size: 12px; }

		.table{
			width: 100%;
			border-collapse: collapse;
		}

		.m-0{
			margin: 0;
		}

		.no-wrap{
			white-space: nowrap;
		}

		.col-container {
			display: table; /* Make the container element behave like a table */
			width: 100%; /* Set full-width to expand the whole page */
		}

		.col {
			page-break-inside: all;
			display: table-cell; /* Make elements inside the container behave like table cells */
		}
	</style>
</head>

<body>
	<header>
		<span style="position: absolute; font-size: 15px; top: 18px; font-weight: bold">{{ optional($base_company)->name }}</span>
		<h2 style="text-align: center">OUTWARD MANIFEST</h2>
		<table class="table table-borderless table-manifest">
			<tr>
				<td style="text-align: right; width: 100px"><strong>Vessel Name: </strong></td>
				<td style="width: 150px">{{ optional($bill_ladings[0]->getVessel)->name ?? $bill_ladings[0]->vessel  }}</td>
				<td style="text-align: right; width: 150px"><strong>Date of Arrival: </strong></td>
				<td style="width: 50px">{{ !empty($bill_ladings[0]->voyage_id) ? $bill_ladings[0]->getVoyage->eta_pol->format('d/m/Y') : '-' }}</td>
				<td style="text-align: right; width: 200px"><strong>Port of Loading: </strong></td>
				<td>{{ strtoupper($bill_ladings[0]->pol_name) }}</td>
			</tr>
			<tr>
				<td style="text-align: right; width: 100px"><strong>Voyage No.: </strong></td>
				<td style="width: 150px">{{ !empty($bill_ladings[0]->voyage_id) ? $bill_ladings[0]->getVoyage->voyage_id : $bill_ladings[0]->voyage }}</td>
				<td style="text-align: right; width: 150px"><strong>Date of Departure: </strong></td>
				<td width="50px">{{ !empty($bill_ladings[0]->voyage_id) ? $bill_ladings[0]->getVoyage->etd_pol->format('d/m/Y') : '-' }}</td>
				<td style="text-align: right; width: 200px"><strong>Port of Discharge: </strong></td>
				<td>{{ strtoupper($bill_ladings[0]->fpd_name) }}</td>
			</tr>
		</table>
		<table class="table table-manifest-header" style="margin-top: 10px">
			<thead>
				<tr>
					<th style="width: 295px">Shipper/Consignee/Notify Party</th>
					<th style="width: 180px">Marks & No.</th>
					<th style="width: 470px">Cargo Description</th>
					<th style="width: 90px" class="no-wrap" style="text-align: right;">Weight</th>
					<th class="no-wrap" style="text-align: right;">Meas.</th>
				</tr>
			</thead>
		</table>
	</header>

	<main>
		@foreach($bill_ladings AS $key => $bl)
		<div style="background: white; margin-bottom: 20px; display: table;">
			<div style="display: table-row;">
				<div style="display: table-cell; background: white; width: 300px;">
					<strong style="display: block; text-decoration: underline">BL No.: {{ $bl->bl_no }}</strong>
					<div style="width: 300px">
						<strong style="display: block">SHIPPER</strong>
						{{ $bl->shipper_name }}<br>{!! nl2br($bl->shipper_add) !!}<br><br>
						<strong style="display: block">CONSIGNEE</strong>
						{{ $bl->consignee_name }}<br>{!! nl2br($bl->consignee_add) !!}<br><br>
						<strong style="display: block">NOTIFY PARTY</strong>
						@if($bl->notify_add == $bl->consignee_add)
						SAME AS CONSIGNEE
						@else
						<p style="margin: 0">{{ $bl->notify_name }}{!! (!empty($bl->notify_add)) ? '<br>' . nl2br($bl->notify_add) : '' !!}</p>
						@endif
					</div>
				</div>
				<div style="display: table-cell; background: white; width: 180px; vertical-align: top; padding-top: 12px;">
					@if(isset($bl->containers))
					<p style="width: 180px;">{{ $bl->containers }}</p>
					@else
					@foreach($bl->getCargos AS $cargo)
					@if($bl->vessel_type == "FCL")
					@foreach($cargo->containers AS $cont)
					<p class="m-0">{{ $cont->container_no }} / {{ $cont->seal_no }}</p>
					@endforeach
					@endif
					{!! !empty($cargo->markings) ? nl2br($cargo->markings) . '<br>' : '' !!}
					{!! !empty($cargo->temperature) ? nl2br($cargo->temperature) . '°C<br>' : '' !!}
					{!! !empty($cargo->uncode) ? nl2br($cargo->uncode) . '<br>' : '' !!}
					@endforeach
					@endif
				</div>
				<div style="display: table-cell; background: white; vertical-align: top; white-space: pre; width: 470px; padding-top: 12px">@if(!empty($bl->getCargos) && !isset($bl->no_packages))@foreach($bl->getCargos AS $cargo)<div>{{ $cargo->cargo_name }} {{ $cargo->cargo_desc }}</div><div class="desc">{!! nl2br($cargo->detailed_desc) !!}</div>@endforeach
				@else<div style="width: 420px">{{ $bl->no_packages }}<br>{{ $bl->detailed_desc }}<br>{{ $bl->remarks }}</div>
				@endif<strong><br>FREIGHT {{ $bl->getFreightTerm() }}</strong>
			</div>
			<div style="display: table-cell; background: white; vertical-align: top; width: 90px; padding-top: 12px;">
				@if(!empty($bl->getCargos))
				@if($bl->getCargos->sum('weight') > 0)
				{{ number_format($bl->getCargos->sum('weight'), 3) . ' ' . $bl->getCargos[0]->weight_unit }}
				@endif
				@endif
			</div>
			<div style="display: table-cell; background: white; vertical-align: top; width: 70px; padding-top: 12px;">
				@if(!empty($bl->getCargos))
				@if($bl->getCargos->sum('volume') > 0)
				{{ number_format($bl->getCargos->sum('volume'), 3) }} M3
				@endif
				@endif
			</div>
		</div>
	</div>
	@endforeach
</main>

<div style="font-size: 11px">
	<hr>
	<div style="background: white; display: table; line-height: 10px;">
		<div style="display: table-cell; background: white;  width: 300px;"></div>
		<div style="display: table-cell; background: white; width: 170px; vertical-align: top;"></div>
		<div style="display: table-cell; background: white; vertical-align: top; white-space: pre; width: 450px; text-align: left">
			@if(is_array($total_cargo))@if($total_cargo[0] > 0)
			<p style="margin: 0">{{ $total_cargo[0] }} TWENTY FOOTER CONTAINER</p>
			@endif
			@if($total_cargo[1] > 0)
			<p style="margin: 0">{{ $total_cargo[1] }} FORTY FOOTER CONTAINER</p>
			@endif
			@else
			{{ $total_cargo }} PACKAGES
			@endif
		</div>
		<div style="display: table-cell; background: white; vertical-align: top; width: 80px; text-align: right">{{ ($total_weight_mt > 0) ? number_format($total_weight_mt, 3) . ' MT' : '' }}<br>{{ ($total_weight_kg > 0) ? number_format($total_weight_kg, 3) . ' KG' : '' }}</div>
		<div style="display: table-cell; background: white; vertical-align: top; width: 80px; text-align: right">{{ ($total_volume > 0) ?  number_format($total_volume, 3) . ' M3' : '' }}</div>
	</div>
</div>

</body>
</html>