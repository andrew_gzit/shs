@extends("layouts.nav")
@section('page_title', 'Print Voyage Schedule')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li>
		<a href="{{ action('PrintController@index_bl') }}" class="btn-hidden"><button type="button" id="btn-print" style="display: inline-block" class="btn btn-white m-r-5">Return to Voyage List</button></a>
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Print Voyage Schedule</h1>

@stop