<html>
<head>
    <style>
        *, ::after, ::before {
            box-sizing: border-box;
        }

        body{
            font-family: sans-serif;
        }

        .top3{
            height: 18mm;
            width: 280px !important;
        }

        .bl-label{
            display: block;
            margin-bottom: 5px;
            font-size: 10px;
            font-weight: 400;
            padding-top: 0px;
        }

        .bl-content{
            display: block;
            padding-left: 0px;
            font-weight: 400 !important;
            font-size: 11px !important;
            padding-top: -6px;
        }

        .no-bottom-border{
            border-bottom: none;
        }

        .table-so-header{
            font-size: 10px;
            font-weight: 400 !important;
        }

        .table-so-header td{
            border-top: none;
            border-bottom: none;
            text-align: center;
        }

        .table-so-header td:first-child{
            border-left: none;
        }

        .bl-company h2{
            padding-top: 30px;
        }

        .p-0{
            padding: 0 !important;
        }

        .bl-lbl-goods {
            display: block;
            text-align: center;
            font-size: 13px;
            font-weight: 400;
            margin-top: 10px;
        }

        td{
            border: 1px solid #000;
            padding: 5px;
            vertical-align: top;
        }

        main{
            padding: 5px;
            font-size: 11px;
        }

        .table-borderless td{
            border: 0;
        }

        @page { margin-top: 432px; margin-bottom: 325px; margin-left: 100px; margin-right: 115px; size: A4; }
        header { position: fixed; top: -415px; left: -3px; right: 0px; height: 450px; width: 100% }
        footer { position: fixed; bottom: -315px; left: 0px; right: -115px; height: 300px; }
        /*p { page-break-after: always; }*/
        /*p:last-child { page-break-after: never; }*/


        .desc{
            font-family: 'Helvetica';
            white-space: pre;
        }

        .table{
            width: 100%;
            border-collapse: collapse;
        }

        .pull-right{
            float: right;
        }
    </style>

    @if($format_type == "form")
    <style type="text/css">
        .print-hidden, .bl-label{
            visibility: hidden;
        }

        table, td{
            border-color: #fff !important;
        }

        body{
            border-color: #fff !important;
        }
    </style>
    @endif

</head>

<body style="border-left: 1px solid #000; border-right: 1px solid #000; margin-top: -6px; margin-right: -115px">
    <header>
        <table class="report-container no-bottom-border">
            <thead class="report-header">
                <tr>
                    <th class="report-header-cell no-bottom-border">
                        <div class="header-info">
                            <table class="table  bg-white text-black">
                                <tr>
                                    <td class="top3" colspan="3">
                                        <label class="bl-label">SHIPPER</label>
                                        <span class="bl-content">{{ $bl->shipper_name }}<br>{!! nl2br($bl->shipper_add) !!}</span>
                                    </td>
                                    <td>
                                        <div style="padding: 10px; padding-top: 15px">
                                            <strong style="font-size: 18px; margin-bottom: 10px" class="print-hidden">SHIPPING ORDER</strong>
                                            <div style="font-size: 15px; font-weight: 400">
                                                <label>{{ $bl->bl_no }}</label>
                                                <span class="pull-right">{{ strtoupper($bl->created_at->format("d F Y")) }}</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="top3" colspan="3">
                                        <label class="bl-label">CONSIGNEE</label>
                                        <span class="bl-content">
                                            {{ $bl->consignee_name }}<br>
                                            {{ $bl->consignee_add }}
                                        </span>
                                    </td>
                                    <td rowspan="5">
                                        <div class="print-hidden" style="font-size: 10px; padding-top: 20px; text-align: center;">
                                            <strong>SYARIKAT PERKAPALAN SOO HUP SEND SDN. BHD.</strong>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="top3" colspan="3" style="position: relative;">
                                        <label class="bl-label">NOTIFY PARTY/FORWARDING AGENT</label>
                                        <span class="bl-content">
                                            @if($bl->notify_add == $bl->consignee_add)
                                            SAME AS CONSIGNEE
                                            @else
                                            {{ $bl->notify_name }}<br>
                                            {{ $bl->notify_add }}
                                            @endif
                                        </span>
                                        <span class="print-hidden" style="position: absolute; top: 55px; font-size: 11px; font-weight: 400">Forwarding Agent:</span>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2">
                                        <label class="bl-label">Mode of Conveyance to Port</label>
                                        <span class="bl-content">&nbsp;</span>
                                    </td>
                                    <td>
                                        <label class="bl-label">Location of Cargo</label>
                                        <span class="bl-content">&nbsp;</span>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2">
                                        <label class="bl-label">Vessel/Voyage No.</label>
                                        <span class="bl-content" style="padding-top: 3px">{{ $bl->getVessel->name }} / {{ $bl->getVoyage->voyage_id }}</span>
                                    </td>
                                    <td>
                                        <label class="bl-label">SCN</label>
                                        <span class="bl-content" style="padding-top: 3px">{{ $bl->getVoyage->scn }}</span>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label class="bl-label">Date of Arrival</label>
                                        <span class="bl-content" style="padding-top: 3px">{{ strtoupper($bl->getBL_POL()->eta->format('d M Y')) }}</span>
                                    </td>
                                    <td>
                                        <label class="bl-label">Date of Departure</label>
                                        <span class="bl-content" style="padding-top: 3px">{{ strtoupper($bl->getBL_POL()->etd->format('d M Y')) }}</span>
                                    </td>
                                    <td>
                                        <label class="bl-label">Port Discharge</label>
                                        <span class="bl-content" style="padding-top: 3px">{{ strtoupper($bl->fpd->location) }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="p-0">
                                        <table class="table table-so-header table-valign-top m-b-0">
                                            <tbody class="print-hidden">
                                                <tr>
                                                    <td style="width: 180px">Marks & Nos. or<br>Container Nos.</td>
                                                    <td style="width: 160px">Nos. and kind of <br>Packages</td>
                                                    <td>Description of goods</td>
                                                    <td style="width: 40px; white-space: nowrap;">Gross weight<br>(Tonne)</td>
                                                    <td style="width: 40px; border-right: none">Measurement<br>(M<sup>3</sup>)</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </th>
                </tr>
            </thead>
        </table>
    </header>

    <footer>
        <div style="padding-left: 10px; margin-top: -10px">
            <span style="font-size: 11px; display: inline-block;">FREIGHT {{ $bl->getFreightTerm() }} {{ ($bl->freight_term == 2) ? 'AT ' . $bl->pol_name : '' }}</span>
            <span class="bl-content" style="margin-bottom: 0; margin-left: 170px; display: inline-block;">{{ $bl->getNoPackages() }}</span>
        </div>
        <table class="table">
            <tr>
                <td class="print-hidden" colspan="2" style="font-size: 11px">FOR OFFICIAL USE</td>
            </tr>
            <tr>
                <td style="height: 3in;">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </footer>

    <main style="padding-top: 30px">
        <div style="display: inline-block; width: 170px; position: absolute; word-wrap: all; word-break: break-all;">
            @foreach($bl->getCargos AS $cargo)
            @if($bl->vessel_type == "FCL")
            @foreach($cargo->containers AS $cont)
            {{ $cont->container_no . '/' . $cont->seal_no }}<br>
            @endforeach
            @endif
            {!! !empty($cargo->markings) ? nl2br($cargo->markings) . '<br>' : '' !!}
            {!! !empty($cargo->temperature) ? '<span>TEMPERATURE :<br></span>' . nl2br($cargo->temperature) . '°C<br>' : '' !!}
            {!! !empty($cargo->uncode) ? '<span>UN NO. :<br></span>' . nl2br($cargo->uncode) . '<br>' : '' !!}
            @endforeach
        </div>
        <div style="display: inline-block; position: absolute; width: 100px; left: 508px; text-align: right;">
            @foreach($bl->getCargos AS $cargo)
            {{ $cargo->weight > 0 ? number_format($cargo->weight, 3) . ' ' . $cargo->weight_unit : '' }}<br>
            @endforeach
            @if(count($bl->getCargos) > 0)
            @if($bl->getCargos->where("weight", "!=", 0)->count() > 1)
            <div style="border-top: 1px solid #000; border-bottom: 1px solid #000; white-space: nowrap; margin-left: 25px; padding-top: 5px; padding-bottom: 5px; margin-top: 5px;">
                {{ number_format($bl->getCargos->sum('weight'), 3) . ' MT' }}</div>
            @endif
            @endif
        </div>
        <div style="display: inline-block; position: absolute; width: 100px; left: 590px; text-align: right;">
            @foreach($bl->getCargos AS $cargo)
            {{ $cargo->volume > 0 ? number_format($cargo->volume, 3) . ' ' . 'M3' : '' }}<br>
            @endforeach
            @if(count($bl->getCargos) > 0)
            @if($bl->getCargos->where("volume", "!=", 0)->count() > 1)
            <div style="border-top: 1px solid #000; border-bottom: 1px solid #000; margin-left: 25px; white-space: nowrap; padding-top: 5px; padding-bottom: 5px; margin-top: 5px">
                {{ number_format($bl->getCargos->sum('volume'), 3) . ' M3' }}</div>
            @endif
            @endif
        </div>
        <div style="margin-left: 165px;">
            @foreach($bl->getCargos AS $cargo)
            {{ $cargo->cargo_name . ' ' .  $cargo->cargo_desc}}<br>
            @if($cargo->detailed_desc)
            <div class="desc">{!! nl2br($cargo->detailed_desc) !!}</div>
            @endif
            @endforeach
        </div>
    </main>

    <script type="text/php">
            if ( isset($pdf) ) { 
            $pdf->page_script('
            if ($PAGE_COUNT > 1) {
            $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
            $size = 9;
            $pageText = "Page " . $PAGE_NUM . " of " . $PAGE_COUNT;
            $y = 580;
            $x = 540;
            $pdf->text($x, $y, $pageText, $font, $size);
        } 
        ');
    }
</script>

</body>
</html>