<html>
<head>
	<style>
		*, ::after, ::before {
			box-sizing: border-box;
		}

		.top3{
			height: 22mm;
			min-height: 17mm;
			width: 100mm;
		}

		.top-bl{
			width: 104mm;
			text-align: center;
		}

		.bl-label{
			display: block;
			margin-bottom: 0px;
			font-size: 11px;
			font-weight: 400;
			padding-top: 0px;
		}

		.bl-content{
			display: block;
			padding-left: 10px;
			/*padding-bottom: 5px;*/
			font-weight: 400 !important;
			font-size: 11px !important;
		}

		.no-bottom-border{
			border-bottom: none !important;
		}

		.table-apollo-header{
			font-size: 11px;
			font-weight: 400 !important;
		}

		.table-apollo-header td{
			/*border: none !important;*/
		}

		.table-apollo-header .container-markings{
			width: 33.5mm;
		}

		.table-apollo-header .num-packages{
			width: 70mm;
			text-align: left;
		}

		.table-apollo-header .width{
			width: 33.5mm;
		}

		.bl-company h2{
			padding-top: .6in;
		}

		.p-0{
			padding: 0 !important;
		}

		.bl-lbl-goods {
			display: block;
			text-align: center;
			font-size: 13px;
			font-weight: 400;
			margin-top: 10px;
		}

		td{
			border: 1px solid #000;
			padding: 5px;
			vertical-align: top;
		}

		main{
			padding: 5px;
			font-size: 12px;
		}

		.table-borderless td{
			border: 0;
		}

		@page { margin-top: 460px; margin-bottom: 355px; margin-left: 0; margin-right: 0; size: A4 }
		header { position: fixed; top: -415px; left: -3px; right: 0px; height: 100px; width: 100% }
		footer { position: fixed; bottom: -330px; left: 0px; right: 0px; height: 300px; }

		.table{
			width: 100%;
			border-collapse: collapse;
		}

		body{
			font-family: sans-serif;
		}

		.desc{
			font-family: 'Helvetica';
			white-space: pre;
		}

		footer .pagenum:before {
			content: counter(page);
		}

		.bottom-small{
			/*height: 9mm !important;*/
		}

        .page-border {
            position: fixed;
            left: 0px;
            top: -412px;
            bottom: -345px;
            right: 0px;
            border: 1px solid #000;
        }
	</style>

	@if($format_type == "form")
	<style type="text/css">
		.print-hidden, .bl-label{
			visibility: hidden;
		}


		table, td{
			border-color: #fff !important;
		}

		body{
			border-color: #fff !important;
            border-left-color: #fff !important;
            border-right-color: #fff !important;
		}

        .page-border {
            border-color: #fff !important;
        }
	</style>
	@endif

</head>

<body style="border-left: .5px solid #000; border-right: 1px solid #000; margin-top: -6px;">
    <div class="page-border"></div>
	<header>
		<table class="report-container no-bottom-border">
			<thead class="report-header">
				<tr>
					<th class="report-header-cell no-bottom-border">
						<div class="header-info">
							<table class="table  bg-white text-black">
								<tr>
									<td class="top3" colspan="2">
										<label class="bl-label">Shipper</label>
										<span class="bl-content" style="padding-left: 20px">
											{{ $bl->shipper_name }}<br>
											{!! nl2br($bl->shipper_add) !!}
										</span>
									</td>
									<td class="top-bl" rowspan="3" colspan="2">
										<div class="bl-company">
											<h2 class="print-hidden">BILL OF LADING</h2>
											@if($company_name1 || $company_name2)
											<div style=" text-align: center; font-size: 20px; padding-left: -45px; padding-top: -5px">
												<span style="display: block">{{ $company_name1 }}</span>
												<span style="display: block">{{ $company_name2 }}</span>
											</div>
											@endif
										</div>
									</td>
								</tr>
								<tr>
									<td class="top3" colspan="2">
										<label class="bl-label">Consignee</label>
										<span class="bl-content" style="padding-left: 20px">
											{{ $bl->consignee_name }}<br>
											{!! nl2br($bl->consignee_add) !!}
										</span>
									</td>
								</tr>
								<tr>
									<td class="top3" colspan="2" style="height: 78px">
										<label class="bl-label">Notify Party</label>
										<span class="bl-content" style="padding-left: 20px;">
											@if($bl->notify_add == $bl->consignee_add)
											SAME AS CONSIGNEE
											@else
											{{ $bl->notify_name }}<br>
											{!! nl2br($bl->notify_add) !!}
											@endif
										</span>
									</td>
								</tr>
								<tr>
									<td colspan="2" class="bottom-small">
										<label class="bl-label" style="margin-bottom: 4px">Vessel</label>
										<span class="bl-content" style="padding-left: 20px">{{ $bl->getVessel->name }}</span>
									</td>
									<td>
										<label class="bl-label" style="margin-bottom: 4px">Voy. No.</label>
										<span class="bl-content">{{ $bl->getVoyage->voyage_id }}</span>
									</td>
									<td>
										<label class="bl-label" style="margin-bottom: 4px">B/L No.</label>
										<span class="bl-content">{{ $bl->bl_no }}</span>
									</td>
								</tr>
								<tr>
									<td>
										<label class="bl-label" style="margin-bottom: 4px">Port of Loading</label>
										<span class="bl-content" style="padding-left: 20px">{{ strtoupper($bl->pol_name) }}</span>
									</td>
									<td>
										<label class="bl-label" style="margin-bottom: 4px">Port of Discharge</label>
										<span class="bl-content">{{ strtoupper($bl->pod_name) }}</span>
									</td>
									<td>
										<label class="bl-label" style="margin-bottom: 4px">Freight Payable at</label>
										<span class="bl-content">
											@if($bl->freight_term == 0 || $bl->freight_term == 2)
											{{ strtoupper($bl->pol_name) }}
											@else
											{{ strtoupper($bl->pod_name) }}
											@endif
										</span>
									</td>
									<td>
										<label class="bl-label" style="margin-bottom: 4px">No. of Original Bs/L</label>
										<span class="bl-content">{{ $bl->getNoBL() }}</span>
									</td>
								</tr>
								<tr>
									<td style="text-align: center" class="no-bottom-border">
										<label class="bl-label">
											@if(empty($company_name1) && empty($company_name2))
											Container No. and Seal No.<br>Marks & No.
											@else
											Marks and numbers
											@endif
										</label>
									</td>
									<td colspan="2" class="no-bottom-border">
										<label class="bl-label">Number and kind of packages : Description of goods<br>
											@if(empty($company_name1) && empty($company_name2))
											"SHIPPER'S LOAD, STOW, COUNT AND SEAL"
											@else
											&nbsp;
											@endif
										</label>
									</td>
									<td class="no-bottom-border">
										<label class="bl-label">Gross weight : Measurement</label>
									</td>
								</tr>
								<!-- <tr>
									<td colspan="4" class="p-0 no-bottom-border">
										<table class="table table-apollo-header table-valign-top m-b-0">
											<tbody class="print-hidden">
												<tr>
													<td style="border-bottom: none" class="container-markings">Container No. and Seal No.<br>Marks & No.</td>
													<td style="border-bottom: none" class="num-packages">Number and kind of packages : Description of goods<br>"SHIPPER'S LOAD, STOW, COUNT AND SEAL"</td>
													<td style="border-bottom: none" class="width">Gross weight : Measurement</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr> -->
							</table>
						</div>
					</th>
				</tr>
			</thead>
		</table>
	</header>

	<footer>
		<span class="bl-content" style="padding-left: 10px; margin-top: -30px">
            FREIGHT @if($bl->freight_term == 1) TO @endif {{ $bl->getFreightTerm() }} {{ ($bl->freight_term == 2) ? 'AT ' . $bl->pol_name : '' }}
        </span>
		<table class="table">
			<tr>
				<td style="width: 50%; height: 7mm">
					<label class="bl-label">Total Number of Packages (in words) :</label>
					<span class="bl-content">{{ $bl->getNoPackages() }}</span>
				</td>
				<td style="width: 50%; height: 7mm">
					<label class="bl-label">Place and date of issue</label>
					<span class="bl-content">{{ $bl->pol_name . ', ' . $bl->bl_date->format('d/m/Y') }}</span>
				</td>
			</tr>
			<tr>
				<td style="height: 37mm;">
					<label class="bl-label">Freight Charges</label>
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
			<tr>
				<td style="height: 7mm !important;"></td>
				<td class="print-hidden" style="text-align: center; font-weight: bold; font-size: 18px; height: 7mm !important;">AS AGENT FOR THE CARRIER<span style="font-weight: normal;">&nbsp;</span></td>
			</tr>
		</table>
	</footer>

	<main style="margin-top: -25px">

		<div style="margin-top: 20px;">
			<div style="display: inline-block; padding-left: 10px; width: 200px; position: absolute; word-wrap: all; word-break: break-all;">
				@foreach($bl->getCargos AS $cargo)
				@if($bl->vessel_type == "FCL")
				@foreach($cargo->containers AS $cont)
				{{ $cont->container_no . '/' . $cont->seal_no }}<br>
				@endforeach
				@endif
				{!! !empty($cargo->markings) ? nl2br($cargo->markings) . '<br>' : '' !!}
				{!! !empty($cargo->temperature) ? '<span>TEMPERATURE :<br></span>' . nl2br($cargo->temperature) . '°C<br>' : '' !!}
				{!! !empty($cargo->uncode) ? '<span>UN NO. :<br></span>' . nl2br($cargo->uncode) . '<br>' : '' !!}
				@endforeach
			</div>
			<div style="display: inline-block; position: absolute; width: 60px; white-space: nowrap; left: 600px; text-align: right;">
				@foreach($bl->getCargos AS $cargo)
				{{ $cargo->weight > 0 ? number_format($cargo->weight, 3) . ' ' . $cargo->weight_unit : '' }}<br>
				@endforeach
				@if(count($bl->getCargos) > 0)
				@if($bl->getCargos->where("weight", "!=", 0)->count() > 1)
				<div style="border-top: 1px solid #000; border-bottom: 1px solid #000; width: 60px; white-space: nowrap; padding-top: 5px; padding-bottom: 5px; margin-top: 5px">
					{{ number_format($bl->getCargos->sum('weight'), 3) . ' MT' }}</div>
					@endif
					@endif
				</div>
				<div style="display: inline-block; position: absolute; width: 60px; left: 700px; white-space: nowrap; text-align: right;">
					@foreach($bl->getCargos AS $cargo)
					{{ $cargo->volume > 0 ? number_format($cargo->volume, 3) . ' ' . 'M3' : '' }}<br>
					@endforeach
					@if(count($bl->getCargos) > 0)
					@if($bl->getCargos->where("volume", "!=", 0)->count() > 1)
					<div style="border-top: 1px solid #000; border-bottom: 1px solid #000; width: 60px; white-space: nowrap; padding-top: 5px; padding-bottom: 5px; margin-top: 5px">
						{{ number_format($bl->getCargos->sum('volume'), 3) . ' M3' }}</div>
						@endif
						@endif
					</div>
					<div style="margin-left: 200px;">
						@foreach($bl->getCargos AS $cargo)
						{{ $cargo->cargo_name . ' ' .  $cargo->cargo_desc}}<br>
						@if($cargo->detailed_desc)
						<div class="desc">{!! nl2br($cargo->detailed_desc) !!}</div>
						@endif
						@endforeach
					</div>
				</div>
			</main>

			<script type="text/php">
				if ( isset($pdf) ) {
				$pdf->page_script('
				if ($PAGE_COUNT > 1) {
				$font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
				$size = 9;
				$pageText = "Page " . $PAGE_NUM . " of " . $PAGE_COUNT;
				$y = 150;
				$x = 500;
				$pdf->text($x, $y, $pageText, $font, $size);
			}
			');
		}
	</script>

</body>
</html>
