@extends("layouts.nav")
@section('page_title', 'Reservation')

@section('content')
<h1 class="page-header">Reservation</h1>

@if(session('msg'))
<div class="alert {{ session('class') }} fade show">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('msg') }}
</div>
@endif

<form id="form_create_res" method="POST" action="{{ action('ReservationController@store') }}">
	@csrf
	<div class="panel">
		<div class="panel-body">
			<div class="row">
				<!-- <div class="col-md-2">
					<div class="form-group">
						<label>Date</label>
						<input id="txt_date" autocomplete="off" type="" class="form-control form-control-lg filter_date" name="txt_date">
					</div>
				</div> -->
				<div class="col-sm-6 col-lg-4">
					<div class="form-group">
						<label>Booking Party</label>
						<select id="booking_party" class="ddl form-control form-control-lg" name="txt_shipper">
							<option disabled="" selected="" value="-">-- select an option --</option>
							@foreach($shippers AS $ship)
							<option value="{{ $ship->getCompanyOfType()->id }}" data-company-id="{{ $ship->id }}" data-name="{{ $ship->name }}">{{ $ship->getCompanyOfType()->code }} - {{ $ship->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group m-b-0">
						<label>Intended Vessel / Voyage</label>
						<div class="d-flex">
							<input id="txt_voyage" type="text" name="txt_voyage" hidden="" value="{{ !empty($query) ? $query : '' }}">
							<input id="txt_intended" type="text" readonly="" placeholder="Click Schedule to view voyage list" class="form-control form-control-lg bg-white m-b-10" value="{{ !empty($query) ? $voy_text : '' }}">
							<button id="btn_schedule" type="button" class="btn btn-success btn-action m-l-10">Schedule</button>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-2">
					<div class="form-group">
						<label>Port Of Discharge</label>
						<select class="ddl form-control form-control-lg" name="txt_pod">
							<option value="null">None</option>
						</select>
					</div>
					<div class="form-group m-b-0">
						<label>Weight (MT)</label>
						<input type="text" autocomplete="off" name="txt_weight" data-type="number" data-title="Weight (MT)" class="required form-control form-control-lg" placeholder="Enter Weight">
					</div>
				</div>
				<div class="col-sm-12 col-lg-6">
					<div class="form-group m-b-0">
						<label>Remarks</label>
						<textarea rows="6" class="form-control form-control-lg" name="txt_remarks" placeholder="Enter Remarks"></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix m-b-20">
		<button id="btn-save" type="button" class="btn btn-primary btn-action pull-right">Create Reservation</button>
	</div>
</form>

<ul class="nav nav-tabs">
	<li class="nav-items">
		<a href="#default-tab-1" data-toggle="tab" class="nav-link active">
			<span class="d-sm-none">Reservation List</span>
			<span class="d-sm-block d-none">Reservation List</span>
		</a>
	</li>
	<li class="nav-items">
		<a href="#default-tab-2" data-toggle="tab" class="nav-link">
			<span class="d-sm-none">Cancelled Reservations</span>
			<span class="d-sm-block d-none">Cancelled Reservations</span>
		</a>
	</li>
</ul>
<div class="tab-content">
	<div class="tab-pane fade active show" id="default-tab-1">
		<table id="table_reservations" class="table table-bordered table-valign-middle f-s-14 bg-white m-b-0">
			<thead class="thead-custom">
				<tr>
					<th width="1%"></th>
					<th width="1%" class="no-wrap">No.</th>
					<th width="1%">Booking Party</th>
					<th>Vessel</th>
					<th>Voyage</th>
					<th>POD</th>
					<th width="1%" class="no-wrap">Weight</th>
					<th width="1%" class="no-wrap">Created On</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($reservations AS $res)
				<tr data-id="{{ $res->id }}">
					<td class="more-details">
						<i class="fa fa-plus"></i>
					</td>
					<td>
						@if($res->id > 9999)
						R{{ str_pad($res->id - 9999, 4, "0", STR_PAD_LEFT) }}
						@else
						R{{ str_pad($res->id, 4, "0", STR_PAD_LEFT) }}
						@endif
					</td>
					<td class="no-wrap">{{ $res->getCompany->name }}</td>
					<td>{{ !empty($res->voyage_id) ? $res->getVoyage->vessel->name : '-' }}</td>
					<td>{{ !empty($res->voyage_id) ? $res->getVoyage->voyage_id : '-' }}</td>
					<td>{{ ($res->pod_id != 0) ? $res->getPod->code : '-' }}</td>
					<td class="text-center no-wrap">{{ $res->weight - $res->getRemainWeight() . " / " . $res->weight }}</td>
					<!-- <td class="no-wrap">
						@if($res->getBooking()->count() <= 3)
						@foreach($res->getBooking() AS $res_bk)
						{{ $res_bk->booking_no }}<br>
						@endforeach
						@else
						<a href="javascript:void(0);" class="resv-view-more">View More ({{ $res->getBooking()->count() }})</a>
						@endif
					</td> -->
					<!-- <td>{{ $res->remarks }}</td> -->
					<td width="1%" class="no-wrap">{{ $res->created_at->format('d/m/Y h:i A') }}</td>
					<td width="1%">
						@if($res->weight > $res->weight - $res->getRemainWeight())
						@if(!empty($res->voyage_id) ? ($res->getVoyage->booking_status == 0 || $res->getVoyage->getReservedWeight() > 0) : true)
						<div class="dropdown">
							<button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-cog"></i>
							</button>
							<div class="dropdown-menu dropdown-menu-right f-s-14" aria-labelledby="dropdownMenuButton">

								@if(!empty($res->voyage_id))
								<a class="dropdown-item btn-book" href="{{ action('BookingController@create') }}?reservation={{$res->id}}" data-id="{{ $res->id }}">Book</a>
								<div class="dropdown-divider"></div>
								@endif
								<!-- <a href="javascript:(0);" class="btn btn-success btn-sm btn-fw btn-edit" data-id="{{ $res->id }}">Edit</a> -->
								<a href="javascript:(0);" class="dropdown-item btn-edit" data-id="{{ $res->id }}">
									<i class="fas fa-edit m-r-10"></i>Edit Reservation
								</a>
								<a class="dropdown-item btn-cancel" href="javascript:(0);">
									<form method="POST" class="d-inline" action="{{ action('ReservationController@delete', $res->id) }}">
										@method('DELETE')
										@csrf
									</form>
									<i class="fas fa-trash m-r-10"></i>
									Cancel Reservation
								</a>
							</div>
						</div>
						<!-- <div class="no-wrap">
							<a href="javascript:(0);" class="btn btn-success btn-sm btn-fw btn-edit" data-id="{{ $res->id }}">Edit</a>
							<form method="POST" class="d-inline" action="{{ action('ReservationController@delete', $res->id) }}">
								@method('DELETE')
								@csrf
								<button type="button" class="btn btn-danger btn-sm btn-fw btn-cancel">Cancel</button>
							</form>
						</div> -->
						@endif
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="tab-pane fade" id="default-tab-2">
		<table id="table_cancelled" class="table table-bordered table-valign-middle f-s-14 bg-white m-b-0">
			<thead class="thead-custom">
				<tr>
					<th width="1%" class="no-wrap">No.</th>
					<!-- <th>Date</th> -->
					<th>Booking Party</th>
					<th>Vessel</th>
					<th>Voyage</th>
					<th>Weight (MT)</th>
					<th>Created On</th>
					<th>Cancelled On</th>
				</tr>
			</thead>
			<tbody>
				@foreach($cancelled AS $res)
				<tr>
					<td width="1%">
						R{{ str_pad($res->id, 4, "0", STR_PAD_LEFT) }}
					</td>
					<!-- <td>{{ $res->date->format('d/m/Y') }}</td> -->
					<td>{{ $res->getCompany->name }}</td>
					<td>{{ !empty($res->voyage_id) ? $res->getVoyage->vessel->name : '-' }}</td>
					<td>{{ !empty($res->voyage_id) ? $res->getVoyage->voyage_id : '-' }}</td>
					<td>{{ $res->weight }}</td>
					<td>{{ $res->created_at->format('d/m/Y h:i A') }}</td>
					<td>{{ $res->deleted_at->format('d/m/Y h:i A') }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="modal-schedule">
	<div class="modal-dialog modal-lg" style="color: #242a30">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Voyage Schedule</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>Filter Date</label>
							<input type="text" id="sch_date" class="form-control form-control-lg filter_date">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>POL</label>
							<select id="sch_pol" class="modal_ddl form-control form-control-lg">
								@foreach($ports AS $port)
								<option value="{{ $port->id }}">{{ $port->code }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>POD</label>
							<select id="sch_pod" class="modal_ddl form-control form-control-lg">
								@foreach($ports AS $port)
								<option value="{{ $port->id }}" {{ ($port->code == 'MYPEN') ? 'selected' : ''}}>{{ $port->code }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>&nbsp;</label>
							<button type="button" id="btn_search" class="btn btn-success btn-action d-block">Search</button>
						</div>
					</div>
					<div class="col-md-12">
						<table class="table table-bordered table-striped table-valign-middle f-s-14 m-b-0">
							<thead>
								<tr>
									<th width="1%"></th>
									<th width="1%">Vessel</th>
									<th width="1%">Voyage</th>
									<th width="1%" class="no-wrap">ETA POL</th>
									<th width="1%" class="no-wrap">Weight</th>
									<th>Port Rotations</th>
								</tr>
							</thead>
							<tbody id="sch_tbody">
								@foreach($voyages AS $voy)
								<tr data-allow="{{ $voy->vessel->dwt - $voy->totalUsedWeight() }}">
									<td><input type="radio" name="radio_select" value="{{ $voy->id }}" style="margin-top: 6px;"></td>
									<td class="no-wrap">{{ $voy->vessel->name }}</td>
									<td class="no-wrap">{{ $voy->voyage_id }}</td>
									<td>{{ $voy->eta_pol->format('d/m/Y') }}</td>
									<td class="no-wrap">{{ $voy->totalUsedWeight() }} / {{ $voy->vessel->dwt }}</td>
									<td>
										@foreach($voy->destinations AS $dest)
										<span>{{ $dest->getPort->code }}</span>
										@if(!$loop->last)
										<span> > </span>
										@endif
										@endforeach
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Cancel</a>
				<button disabled="" id="btn_remove_voy" type="button" class="btn btn-danger">Remove Intended Voyage</button>
				<button disabled="" id="btn_select_voyage" type="button" class="btn btn-primary">Select Intended Voyage</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-edit" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-xl" style="color: #242a30">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Reservation</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="form_edit_res" autocomplete="off" method="POST">
				@method("PUT")
				@csrf
				<div class="modal-body">
					<div class="row">
						<!-- <div class="col-md-3">
							<div class="form-group">
								<label>Date</label>
								<input id="txt_edit_date" autocomplete="off" type="" class="form-control form-control-lg filter_date" name="txt_date">
							</div>
						</div> -->
						<div class="col-md-6 col-lg-5">
							<div class="form-group">
								<label>Booking Party</label>
								<select id="txt_edit_shipper" class="form-control form-control-lg" name="txt_shipper">
									@foreach($shippers AS $ship)
									<option value="{{ $ship->getCompanyOfType()->id }}" data-company-id="{{ $ship->id }}">{{ $ship->getCompanyOfType()->code }} - {{ $ship->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Intended Vessel / Voyage</label>
								<input id="txt_edit_voyage" type="text" name="txt_voyage" hidden="">
								<input id="txt_edit_intended" type="text" readonly="" class="form-control form-control-lg bg-white">
								<button type="button" id="btn_removevoy" class="btn btn-danger btn-sm m-t-10">Remove Vessel/Voyage</button>
							</div>
						</div>
						<div class="col-md-6 col-lg-2">
							<div class="form-group">
								<label>Port of Discharge</label>
								<select id="txt_edit_pod" name="txt_edit_pod" class="form-control form-control-lg">
									<option value="null">None</option>
								</select>
							</div>
							<div class="form-group">
								<label>Weight (MT)</label>
								<input id="txt_edit_weight" type="text" name="txt_weight" data-type="number" data-title="Weight (MT)" class="required form-control form-control-lg" placeholder="Enter Weight">
							</div>
						</div>
						<div class="col-md-12 col-lg-5">
							<div class="form-group">
								<label>Remarks</label>
								<textarea id="txt_edit_remarks" name="txt_remarks" class="form-control form-control-lg" rows="6"></textarea>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>Filter Date</label>
								<input type="text" id="edit_sch_date" autocomplete="off" class="form-control form-control-lg filter_date">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>POL</label>
								<select id="edit_sch_pol" class="modal_edit_ddl form-control form-control-lg">
									@foreach($ports AS $port)
									<option value="{{ $port->id }}" {{ ($port->code == 'MYPEN') ? 'selected' : ''}} >{{ $port->code }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>POD</label>
								<select id="edit_sch_pod" class="modal_edit_ddl form-control form-control-lg">
									@foreach($ports AS $port)
									<option value="{{ $port->id }}">{{ $port->code }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>&nbsp;</label>
								<button type="button" id="btn_edit_search" class="btn btn-success btn-action d-block">Search</button>
							</div>
						</div>
						<div class="col-md-12">
							<div style="max-height: 500px; overflow-y: auto">
								<table class="table table-bordered table-striped table-valign-middle f-s-14 m-b-0">
									<thead>
										<tr>
											<th width="1%"></th>
											<th width="1%">Vessel</th>
											<th width="1%">Voyage</th>
											<th width="1%" class="no-wrap">ETA POL</th>
											<th width="1%" class="no-wrap">Weight</th>
											<th>Port Destinations</th>
										</tr>
									</thead>
									<tbody id="edit_sch_tbody">
										@foreach($voyages AS $voy)
										<tr data-allow="{{ $voy->vessel->dwt - $voy->totalUsedWeight() }}">
											<td><input type="radio" name="edit_radio_select" value="{{ $voy->id }}" style="margin-top: 6px;"></td>
											<td class='no-wrap'>{{ $voy->vessel->name }}</td>
											<td class='no-wrap'>{{ $voy->voyage_id }}</td>
											<td>{{ $voy->eta_pol->format('d/m/Y') }}</td>
											<td class='no-wrap'>{{ $voy->totalUsedWeight() }} / {{ $voy->vessel->dwt }}</td>
											<td>
												@foreach($voy->destinations AS $dest)
												<span>{{ $dest->getPort->code }}</span>
												@if(!$loop->last)
												<span> > </span>
												@endif
												@endforeach
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Cancel</a>
					<button id="btn_savechanges" type="button" class="btn btn-primary">Save Changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
@stop

@section("page_script")
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<link href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css" rel="stylesheet" />

<script src="https://cdn.datatables.net/plug-ins/1.10.16/sorting/datetime-moment.js"></script>

<script type="text/javascript">
	$(function(){
		$(".ddl").select2();

		var edit_allow = 0;

		//Save current editing voyage id, company id
		var editing_voy_id = 0;
		var editing_company_id = 0;

		//Allowable DWT for selected voyage, will update based on selected voyage
		var allow = 0;

		$("#btn_remove_voy").click(function(){
			allow = 0;
			$("input[name='radio_select']:checked").prop("checked", false);
			$("#txt_voyage, #txt_intended").val("");
			$("#btn_select_voyage, #btn_remove_voy").attr("disabled", "");
			$("#modal-schedule").modal("toggle");
		});

		$("#btn_removevoy").click(function(){
			$("input[name='edit_radio_select']:checked").prop("checked", false);
			$("#txt_edit_voyage, #txt_edit_intended").val("");
			$("#txt_edit_pod").find("option:not(:first-child)").remove();
		});

		$(document).on("change", "input[name='edit_radio_select']", function(){
			var $tr = $(this).closest("tr");
			var voyage = $tr.find("td:nth-child(3)").text();
			var vessel = $tr.find("td:nth-child(2)").text();

			var voy_id = $("input[name='edit_radio_select']:checked").val();

			edit_allow = parseInt($tr.attr("data-allow"));

			//Update POD in modal
			getDestinations("edit", voy_id);

			var company_id = $("#txt_edit_shipper option:selected").attr('data-company-id');
			console.log($("#txt_edit_voyage").val());
			console.log(voy_id);

			//Dont check if same voyage as saved
			if(voy_id != editing_voy_id){
				//Check restriction
				$.get("/check-restriction/" + company_id + "/" + voy_id, function(data2){
					if(data2.success){
						if(data2.err.length > 0){
							var msg = data2.err.join("<br>");
							swal({
								title: 'Oops!',
								html: "Selected voyage has violated the following restriction(s): <br><br>" + msg + "<br><br> Press <strong>Proceed</strong> to continue",
								type: 'warning',
								showCancelButton: true,
								confirmButtonText: 'Proceed',
								reverseButtons: true,
								allowOutsideClick: false
							}).then((result) => {
								if(result.value) {

								} else {
								//Remove selected voyage
								$("input[name='edit_radio_select']:checked").prop("checked", false);

								$("#txt_edit_intended, #txt_edit_voyage").val("");
								return false;
							}
						});
						}
					} else {
						console.log(data2.msg);
						alert("Unable to retrieve shipper's restrictions");
					}
				});
			}

			$("#txt_edit_intended").val(vessel + " / " + voyage);
			$("#txt_edit_voyage").val($(this).val());
		});

		$("#txt_edit_shipper").change(function(){
			var selected_company_id = $(this).find("option:selected").attr("data-company-id");
			//Dont trigger check restriction if selected company is same as before / same voyage
			if($("input[name='edit_radio_select']:checked").length != 0){
				var selected_voy_id = $("input[name='edit_radio_select']:checked").val();
				if(selected_voy_id != editing_voy_id){
					$.get("/check-restriction/" + selected_company_id + "/" + selected_voy_id, function(data2){
						if(data2.success){
							if(data2.err.length > 0){
								var msg = data2.err.join("<br>");
								swal({
									title: 'Oops!',
									html: "Selected voyage has violated the following restriction(s): <br><br>" + msg + "<br><br> Press <strong>Proceed</strong> to continue",
									type: 'warning',
									showCancelButton: true,
									confirmButtonText: 'Proceed',
									reverseButtons: true,
									allowOutsideClick: false
								}).then((result) => {
									if(result.value) {

									} else {
									//Remove selected voyage
									$("input[name='edit_radio_select']:checked").prop("checked", false);

									$("#txt_edit_intended, #txt_edit_voyage").val("");
									return false;
								}
							});
							}
						} else {
							console.log(data2.msg);
							alert("Unable to retrieve shipper's restrictions");
						}
					});
				}
			}
		});

		$("#btn_savechanges").click(function(){
			var valid = true;
			$("#form_edit_res").find(".required").each(function(){
				if($(this).val().trim() == ""){
					swal("Oops!", $(this).attr("data-title") + " is required.", "warning");
					valid = false;
					return false;
				}
			});

			var input_weight = $("#txt_edit_weight").val();

			if(input_weight > edit_allow && $("#txt_edit_voyage").val() != ""){
				swal("Oops!", "Entered weight has exceeded allowable DWT of " + edit_allow + " for selected voyage.", "warning");
				valid = false;
				return false;
			}

			if($("#txt_edit_voyage").val() == ""){
				swal({
					title: "Save Changes",
					text: "Save reservation without intended vessel / voyage?",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'Continue',
					cancelButtonText: 'Cancel',
					reverseButtons: true
				}).then((result) => {
					if (result.value) {
						$("#form_edit_res").submit();
					} else {
						return false;
					}
				});
			} else {
				if(valid){
					$("#form_edit_res").submit();
				}
			}
			// if($("input[name='edit_radio_select']:checked").length == 0){
				
			// } else {
			// }
		});

		function format(bk_no, remarks){
			return "<div class='div-details'><div class='row'>" + 
			"<div class='col-md-1'><strong>Booking No.:</strong></div>" +
			"<div class='col-md-2'><span style='white-space: pre-wrap; display: inline-flex;'>" + bk_no + "</span></div>" +
			"<div class='col-md-1'><strong>Remarks:</strong></div>" +
			"<div class='col-md-8'><span style='white-space: pre-wrap; display: inline-block;'>" + remarks + "</span></div>" +
			"</div></div>";
		}

		$.fn.dataTable.moment('DD/MM/YYYY');
		var table = $("#table_reservations").DataTable({
			"columnDefs": [
			{ "orderable": false, "targets": [0,2,3,4,5,6,8] }
			],
			"order": [[ 1, "desc" ]],
		});

		$('#table_reservations tbody').on('click', 'td.more-details', function () {
			var tr = $(this).closest('tr');
			var row = table.row( tr );
			var width = $(this).closest("td").outerWidth();

			if (row.child.isShown()) {
				row.child.hide();
				tr.removeClass('shown');
				tr.find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
			}
			else {
				var res_id = tr.attr("data-id");

				$.get('/getReservationDetail/' + res_id, function(data){
					console.log(data);
					row.child( format(data.booking_no, data.remarks) , "p-0").show();
					tr.addClass('shown');
					$(".tbl-child").css("padding-left", width);
					$(tr).find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
					$('.div-details', row.child()).slideDown(600, 'easeOutQuint');
				});

				// $.get("/raw-part/more-details/" + part_id, function(resp){
				// 	if(resp.success){
				// 		row.child( format(resp) , "tbl-child").show();
				// 		$(".tbl-child").css("padding-left", width);
				// 		$(tr).find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
				// 		tr.addClass('shown');
				// 	} else {
				// 		alert("Details cannot be retrieved! Please refresh.");
				// 	}
				// });
			}
		});

		$("#table_cancelled").DataTable({
			"columnDefs": [
			{ "orderable": false, "targets": [1,2,3,4,5,6] }
			],
			"order": [[ 0, "desc" ]]
		});

		function searchVoyage(pol, pod, date, tbody_id, radio_name){
			// var pol = $("#edit_sch_pol").val();
			// var pod = $("#edit_sch_pod").val();
			// var date = $("#edit_sch_date").val();

			if(pol != pod){
				var url = "/get-voyage/" + pol + "/" + pod + "/" + date.replace(/\//g, "-");
				$.get(url, function(data){
					var html = "";
					if(data.success){
						var results = data.voyages;
						results.forEach(function(elem, index){
							if(elem.vessel.default_shipment == "LCL"){
								html += "<tr data-allow='" + elem.allow + "'>" +
								"<td width='1%'><input type='radio' value='" + elem.id + "' name='" + radio_name + "' style='margin-top: 6px;'></td>" +
								"<td width='1%' class='no-wrap'>" + elem.name + "</td>" +
								"<td width='1%' class='no-wrap'>" + elem.voyage_id + "</td>" +
								"<td width='1%'>" + elem.eta_text + "</td>" +
								"<td width='1%' class='no-wrap'>" + elem.used + " / " + elem.dwt +" </td>" +
								"<td>" + elem.port_destinations + "</td>"
								+"</tr>";
							}
						});
					} else {
						html = "<tr><td colspan='6' class='text-center'>No voyage found.</td></tr>";
					}
					$("#" + tbody_id).empty().append(html);
				});
			} else {
				swal("Same port selected", "Select a different port to filter the voyage list", "warning");
			}
		}

		$("#btn_edit_search").click(function(){
			var pol = $("#edit_sch_pol").val();
			var pod = $("#edit_sch_pod").val();
			var date = $("#edit_sch_date").val();

			searchVoyage(pol, pod, date, "edit_sch_tbody", "edit_radio_select");
		});

		$(document).on("click", ".btn-edit", function(){
			var res_id = $(this).attr("data-id");

			$.get('/get-reservation/' + res_id, function(data){
				if(data.success){
					var res = data.reservation;
					$("#txt_edit_date").val(res.date_text);

					$("#txt_edit_shipper").find("option[data-company-id='" + res.shipper_id + "']").prop("selected", true);

					$("#txt_edit_intended").val(res.intended);
					$("#txt_edit_weight").val(res.weight);
					$("#txt_edit_remarks").val(res.remarks);
					$("#txt_edit_voyage").val(res.voyage_id);

					edit_allow = parseInt(res.allow) + parseInt(res.weight);


					getDestinations("edit", res.voyage_id);

					editing_voy_id = res.voyage_id;
					editing_company_id = $("#txt_edit_shipper option:selected").attr("data-company-id");

					if(res.pod_id != 0){
						setTimeout(function(){
							$("#txt_edit_pod option[value='" + res.pod_id + "']").prop("selected", true);
						}, 500);
					}

					$("input[name='edit_radio_select']:checked").prop("checked", false);
				} else {
					swal("Oops!", "Reservation not found!", "error");
				}
			});


			$("#modal-edit").find("form").attr("action", "/reservation/" + res_id);
			$("#modal-edit").modal("toggle");
		});

		$("#btn-save").click(function(){
			var valid = true;
			$("#form_create_res").find(".required").each(function(){
				if($(this).val().trim() == ""){
					swal("Oops!", $(this).attr("data-title") + " is required.", "warning");
					valid = false;
					return false;
				}
			});

			var entered_weight = parseInt($("input[name='txt_weight']").val());
			if(entered_weight > allow && $("#txt_voyage").val() != ""){
				swal("Oops!", "Entered weight has exceeded allowable DWT of " + allow + " for selected voyage.", "warning");
				valid = false;
				return false;
			}

			if(valid){
				$(this).closest("form").submit();
			}
		});

		$(document).on("click", ".btn-cancel", function(){
			$btn = $(this);
			swal({
				title: "Confirm cancel reservation?",
				text: "Press Continue to proceed",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Continue',
				cancelButtonText: 'Cancel',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					$btn.find("form").submit();
				}
			});
		});

		$(document).on("keydown", "input[data-type='number']", function(e){
			if(!((e.keyCode > 95 && e.keyCode < 106) || (e.keyCode > 47 && e.keyCode < 58) || e.keyCode == 8 || e.keyCode == 9)) {
				e.preventDefault();
				return false;
			}
		});

		$(document).on("blur", "input[data-type='number']", function(e){
			var price = parseInt($(this).val());
			if(!isNaN(price) && price != 0){
				$(this).val(price);
			} else {
				$(this).val("");
			}
		});

		$(".filter_date").datetimepicker({
			format: "DD/MM/YYYY",
			date: new Date(),
			minDate: new Date()
		}).blur(function(){
			if($(this).val() == ""){
				$(this).data("DateTimePicker").destroy();
				$(this).datetimepicker({
					format: "DD/MM/YYYY",
					date: new Date(),
					minDate: new Date()
				});
			}
		});

		function checkRestriction(element){
			var voyage_id = element.val();
			var company_id = $("#booking_party").find("option:selected").attr("data-company-id");
			if(company_id != undefined){
				$.get("/check-restriction/" + company_id + "/" + voyage_id, function(data2){
					if(data2.success){
						if(data2.err.length > 0){
							var msg = data2.err.join("<br>");
							swal({
								title: 'Oops!',
								html: "Selected voyage has violated the following restriction(s): <br><br>" + msg + "<br><br> Press <strong>Proceed</strong> to continue",
								type: 'warning',
								showCancelButton: true,
								confirmButtonText: 'Proceed',
								reverseButtons: true,
								allowOutsideClick: false
							}).then((result) => {
								if(result.value) {

								} else {
								//Remove selected voyage
								element.prop("checked", false);

								//Disable buttons and remove text
								$("#btn_select_voyage, #btn_remove_voy").attr("disabled", "");

								$("#txt_intended").val("");
							}
						});
						}
					} else {
						console.log(data2.msg);
						alert("Unable to retrieve shipper's restrictions");
					}
				});
			}
		}

		$(document).on("change", "input[name='radio_select']", function(){
			var elem = $(this);
			if(elem.is(":checked")){
				$("#btn_select_voyage, #btn_remove_voy").removeAttr("disabled");
				checkRestriction(elem);
			} else {
				$("#btn_select_voyage, #btn_remove_voy").attr("disabled", "");
			}
		});

		$("#btn_select_voyage").click(function(){
			var voy_id = $("input[name='radio_select']:checked").val();
			var $tr = $("input[name='radio_select']:checked").closest("tr");
			allow = parseInt($tr.attr("data-allow"));
			var voyage = $tr.find("td:nth-child(3)").text();
			var vessel = $tr.find("td:nth-child(2)").text();
			$("#txt_intended").val(vessel + " / " + voyage);
			$("#txt_voyage").val(voy_id);

			getDestinations("new", voy_id);

			$("#modal-schedule").modal("toggle");
		});

		$("#btn_schedule").click(function(){
			$("#modal-schedule").modal("toggle");
		});

		$("#modal-schedule").on('shown.bs.modal', function(){
			$(".modal_ddl").select2({
				dropdownParent: $("#modal-schedule")
			});
		});

		$("#modal-edit").on('shown.bs.modal', function(){
			$(".modal_edit_ddl").select2({
				dropdownParent: $("#modal-edit")
			});
		});

		$("#btn_search").click(function(){
			var pol = $("#sch_pol").val();
			var pod = $("#sch_pod").val();
			var date = $("#sch_date").val();
			var url = "/get-voyage/" + pol + "/" + pod + "/" + date.replace(/\//g, "-");
			$.get(url, function(data){
				var html = "";
				if(data.success){
					var results = data.voyages;

					console.log(results);

					var row_count = 0;

					results.forEach(function(elem, index){
						if(elem.vessel.default_shipment == "LCL"){
							row_count++;

							html += "<tr data-allow='" + elem.allow + "'>" +
							"<td width='1%'><input type='radio' value='" + elem.id + "' name='radio_select' style='margin-top: 6px;'></td>" +
							"<td width='1%' class='no-wrap'>" + elem.name + "</td>" +
							"<td width='1%' class='no-wrap'>" + elem.voyage_id + "</td>" +
							"<td width='1%'>" + elem.eta_text + "</td>" +
							"<td width='1%' class='no-wrap'>" + elem.used + " / " + elem.dwt +" </td>" +
							"<td>" + elem.port_destinations + "</td>"
							+"</tr>";
						}
					});

					if(row_count == 0){
						html = "<tr><td colspan='6' class='text-center'>No voyage found.</td></tr>";
					}
				} else {
					html = "<tr><td colspan='6' class='text-center'>No voyage found.</td></tr>";
				}
				$("#sch_tbody").empty().append(html);
			});
			$("#btn_select_voyage, #btn_remove_voy").attr("disabled", "");
		});

		$("#booking_party").change(function() {
			var name = $(this).find('option:selected').attr("data-name");
			$(this).next().next().text(name);

			if($("#txt_intended").val() != ""){
				if($("input[name='radio_select']:checked").length == 1){
					checkRestriction($("input[name='radio_select']:checked"));
				} else {
					swal("Oops", "Selected voyage has already closed / sailed.", "error");
				}
			}
		});

		var query = ("{{ !empty($query) ? $query : '' }}");
		if(query != ''){
			allow = parseInt("{{ (!empty($allow) ? $allow : '') }}");
			$("input[name='radio_select'][value='" + query + "']").trigger("click");
			getDestinations("new", query);
		}

		$("")

		//Get destination ports for POD selection, remove POL from selection
		function getDestinations(type, voy_id){
			$.get("/get-destinations/" + voy_id, function(data){
				var dest = data.dest;
				var html = "<option selected='' value='null'>None</option>";
				dest.forEach(function(elem, index){
					if(elem.id != data.pol){
						html += "<option value='" + elem.id + "'>" + elem.code + "</option>";
					}
				});

				var elem = "select[name='txt_pod']";
				if(type == "edit"){
					elem = "select[name='txt_edit_pod']"
				}

				$(elem).empty().append(html);
			});
		}
	});
</script>
@stop
