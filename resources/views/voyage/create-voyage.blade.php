@extends("layouts.nav")
@section('page_title', 'Create Voyage')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('VoyageController@index') }}">Voyage Schedule</a></li>
	<li class="breadcrumb-item active">Create Voyage</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Create Voyage</h1>

<form id="voyage_form" method="POST" action="{{ action('VoyageController@store') }}" autocomplete="off">
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-12">
					<h4>Voyage Details</h4>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('voyage_id') ? 'has-error' : '' }}">
						<label>Voyage No</label>
						<input id="voyage_id" type="text" class="form-control form-control-lg" name="voyage_id" placeholder="Enter Voyage No" />
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('vessel_id') ? 'has-error' : '' }}">
						<label>Assigned Vessel</label>
						<select id="vessel_id" class="form-control form-control-lg" name="vessel_id">
							<option disabled selected value> -- select an option -- </option>
							@foreach($vessels as $vessel)
							<option value="{{ $vessel->id }}" data-type="{{ $vessel->type }}" data-dwt="{{ $vessel->dwt }}">{{ $vessel->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('allowable_teus') ? 'has-error' : '' }}p">
						<label>Capacity</label>
						<input id="vessel_dwt" type="text" class="form-control form-control-lg" name="allowable_teus" placeholder="Enter Capacity" value="{{ old('allowable_teus') }}" readonly />
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('scn') ? 'has-error' : '' }}">
						<label>Ship Call Number (SCN)</label>
						<input id="vessel_scn" type="text" class="form-control form-control-lg" name="scn" placeholder="Enter Ship Call Number (SCN)" />
						<span class="text-danger">{{ $errors->first('scn') }}</span>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>Booking Status</label>
						<select id="booking_status" class="form-control form-control-lg" name="booking_status">
							<option value="0" selected>Open</option>
							<option value="1">Closed</option>
							<option value="2">Cancel</option>
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('closing_timestamp') ? 'has-error' : '' }}">
						<label>Closing Date/Time</label>
						<input id="closing_timestamp" type="text" class="form-control form-control-lg closingdatetimepicker" name="closing_timestamp" placeholder="Enter Closing Date/Time" disabled />
						<span class="text-danger">{{ $errors->first('closing_timestamp') }}</span>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('cancel_remark') ? 'has-error' : '' }}">
						<label>Cancel Remark</label>
						<input id="cancel_remark" type="text" class="form-control form-control-lg" name="cancel_remark" placeholder="Enter Cancel Remark"disabled/>
						<span class="text-danger">{{ $errors->first('cancel_remark') }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-inverse">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-12">
					<h4>Port(s) Calling</h4>
				</div>
			</div>
			<hr>
			<div class="table-responsive m-b-20" style="position: relative;">
				<table id="ports_table" class="table table-bordered text-center f-s-14">
					<thead class="thead-custom">
						<tr>
							<th>Port Code</th>
							<th>Port Name</th>
							<th>ETA</th>
							<th>ATA</th>
							<th>ETD</th>
							<th>ATD</th>
							{{-- <th>Initial POL</th> --}}
							<th width=200px>Options</th>
						</tr>
					</thead>
					<tbody class="bg-white">
						<tr id="no_port"><td colspan=8>No ports to display.</td></tr>
					</tbody>
				</table>
				<div class="loader-port">
					<div class="loader-port-item text-center">
						<i class="fas fa-3x fa-spinner fa-spin"></i>
					</div>
				</div>
			</div>
			<div data-id="1" class="row port-row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>Port</label>
						<select id="port" class="ddl form-control form-control-lg">
							<option disabled selected value> -- select an option -- </option>
							@foreach($ports as $port)
							<option value="{{ $port->id }}" data-name="{{ $port->name }}">{{ $port->code }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label>ETA</label>
						<input id="port_eta" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ETA" />
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label>ATA</label>
						<input id="port_ata" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ATA" />
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label>ETD</label>
						<input id="port_etd" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ETD" />
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label>ATD</label>
						<input id="port_atd" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ATD" />
					</div>
				</div>
			</div>
			<button id="add_port" type="button" class="btn btn-success pull-right">Add Port</button>
		</div>
	</div>

	<input hidden="" name="txt_portcalling">
	<button type="button" id="btn_cancel" class="btn btn-danger btn-action m-r-10">Cancel</button>
	<button type="button" class="btn btn-primary btn-action btn-save">Save</button>
</form>

<div class="modal fade" id="modal_edit_port">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Port</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Port</label>
							<select id="port" class="form-control form-control-lg">
								<option disabled selected value> -- select an option -- </option>
								@foreach($ports as $port)
								<option value="{{ $port->id }}" data-name="{{ $port->name }}">{{ $port->code }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>ETA</label>
							<input id="edit_port_eta" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ETA" />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>ATA</label>
							<input id="edit_port_ata" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ATA" />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>ETD</label>
							<input id="edit_port_etd" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ETD" />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>ATD</label>
							<input id="edit_port_atd" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ATD" />
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a id="cancel_edit" href="javascript:;" class="btn btn-white">Cancel</a>
				<a id="save_port" href="javascript:;" class="btn btn-success">Save Changes</a>
			</div>
		</div>
	</div>
</div>
@stop

@section('page_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script src="/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />

<script src="/js/voyage/voyage_schedule.js?v={{ str_random(2) }}"></script>
<script src="/js/voyage/create_voyage.js?v={{ str_random(2) }}"></script>



<script>
	$(document).ready(function() {

		$(".ddl").select2();

		$('#btn_cancel').click(function() {
			swal({
				title: 'Are you sure?',
				text: 'Any progress will be lost.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Proceed',
				reverseButtons: true
			}).then((result) => {
				if(result.value) {
					window.location.replace('{{ action('VoyageController@index') }}');
				}
			});
		});

		// $('.btn-save').click(function(e) {
		// 	e.preventDefault();

		// 	var isClosingDisabled = $('#closing_timestamp').prop('disabled');

		// 	if($('#voyage_id').val() == '') {
		// 		showSwal('error', 'Error', '<b>Voyage No</b> can\'t be empty!');
		// 	} else if($('#vessel_id').val() == null) {
		// 		showSwal('error', 'Error', '<b>Assigned Vessel</b> can\'t be empty!');
		// 	} else if(isClosingDisabled == false && $('#closing_timestamp').val() == '') {
		// 		showSwal('error', 'Error', '<b>Closing Timestamp</b> can\'t be empty!');
		// 	} else if($('#booking_status').val() == 2 && $('#cancel_remark').val() == '') {
		// 		showSwal('error', 'Error', '<b>Cancel Remark</b> can\'t be empty!');
		// 	} else if(portCount < 2) {
		// 		showSwal('error', 'Error', '<b>Port(s) Calling</b> must be at least 2!');
		// 	} else {
		// 		var count = $('#ports_table tbody tr').length;
		// 		for(var i = 0; i < count; i++) {
		// 			var elem = $('#ports_table tbody tr:nth-child(' + (i + 1) + ')');
		// 			var details = [];

		// 			details.push(elem.attr('data-id'));
		// 			details.push(elem.attr('data-port'));
		// 			details.push(elem.find('td:nth-child(3)').text());
		// 			details.push(elem.find('td:nth-child(4)').text());
		// 			details.push(elem.find('td:nth-child(5)').text());
		// 			details.push(elem.find('td:nth-child(6)').text());

		// 			var port = details.join(';');

		// 			$('#voyage_form').append('<input type="hidden" name="port[]" value=' + port + ' />');
		// 		}

		// 	//Make sure voyage no and vessel is unique
		// 	var voy_id = $("#voyage_id").val().trim();
		// 	var ves_id = $("#vessel_id").val();

		// 	$(".btn-save").attr("disabled", "");

		// 	$.ajax({
		// 		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
		// 		url: '/validate/createvoyage',
		// 		type: 'POST',
		// 		data:
		// 		{
		// 			'voyage_id': voy_id,
		// 			'vessel_id': ves_id
		// 		},
		// 		success: function(data){
		// 			if(!data.success){
		// 				showSwal('error', 'Oops!', data.msg);
		// 				$(".btn-save").removeAttr("disabled");
		// 			} else {

		// 				//Format date before pass to backend
		// 				ports.forEach(function(port){
		// 					port.eta = (port.eta.isValid()) ? port.eta.format('DD/MM/YYYY') : null;
		// 					port.ata = (port.ata.isValid()) ? port.ata.format('DD/MM/YYYY') : null;
		// 					port.etd = (port.etd.isValid()) ? port.etd.format('DD/MM/YYYY') : null;
		// 					port.atd = (port.atd.isValid()) ? port.atd.format('DD/MM/YYYY') : null;
		// 				});

		// 				//Add port obj into input
		// 				$("input[name='txt_portcalling']").val(JSON.stringify(ports));

		// 				$('#voyage_form').submit();
		// 			}
		// 		}
		// 	});
		// }
	// });

		// $(document).on('click', '.btn-remove', function() {
		// 	swal({
		// 		title: 'Are you sure?',
		// 		text: 'This can\'t be undone.',
		// 		type: 'warning',
		// 		showCancelButton: true,
		// 		confirmButtonText: 'Delete',
		// 		reverseButtons: true
		// 	}).then((result) => {
		// 		if(result.value) {
		// 			var portID = $(this).closest('tr').attr('data-port');

		// 			$('#port option[value=' + portID + ']').prop('hidden', false).removeAttr("disabled");
		// 			$("#port").select2();
		// 			$('#modal_edit_port #port option[value=' + portID + ']').prop('hidden', false).removeAttr("disabled");
		// 			$('#modal_edit_port #port').select2();
		// 			$(this).closest('tr').remove();

		// 			portCount--;

		// 			if (portCount == 0) {
		// 				$('#ports_table tbody').append('<tr id="no_port"><td colspan="7">No ports to display.</td></tr>');
		// 			}

		// 			if($("#ports_table").find("tr:not(#no_port)").length > 1){
		// 				last_etd = moment($("#ports_table").find("tr:last > td:nth-child(5)").text(), 'DD/MM/YYYY').toDate();
		// 			} else {
		// 				last_etd = new Date();
		// 			}

		// 			// setLastETD(last_etd);
		// 		}
		// 	});
		// });

		$("#modal_edit_port").on('shown.bs.modal', function (e) {
			$("#modal_edit_port #port").select2({
				dropdownParent: $("#modal_edit_port")
			});
		});

		// //Reinitialize select2 after inserting
		// $("#port").select2();
	});
</script>
@stop
