@extends("layouts.nav")
@section('page_title', 'Edit Voyage')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item"><a href="/">Home</a></li>
	<li class="breadcrumb-item"><a href="{{ action('VoyageController@index') }}">Voyage Schedule</a></li>
	<li class="breadcrumb-item active">Edit Voyage</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Edit Voyage - <span class="f-s-18">{{ $voyage->voyage_id }}</span></h1>

@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{!! session('msg') !!}
</div>
@endif

<form id="voyage_form" method="POST" action="{{ action('VoyageController@update', $voyage->id) }}" autocomplete="off">
	@method('PUT')
	@csrf
	<div class="panel panel-inverse">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-12">
					<h4>Voyage Details</h4>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>Voyage No</label>
						<input id="voyage_id" type="text" class="form-control form-control-lg" name="voyage_id" value="{{ $voyage->voyage_id }}" placeholder="Enter Voyage No" />
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('vessel_id') ? 'has-error' : '' }}">
						<label>Assigned Vessel</label>
						<select id="vessel_id" class="form-control form-control-lg" name="vessel_id">
							@foreach($vessels as $vessel)
							<option value="{{ $vessel->id }}" data-type="{{ $vessel->type }}" data-dwt="{{ $vessel->dwt }}" {{ ($voyage->vessel_id == $vessel->id) ? 'selected' : '' }}>{{ $vessel->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('allowable_teus') ? 'has-error' : '' }}p">
						<label>Capacity</label>
						<input id="vessel_dwt" type="text" class="form-control form-control-lg" name="allowable_teus" placeholder="Enter Capacity" value="{{ $voyage->vessel->dwt }}" readonly />
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('scn') ? 'has-error' : '' }}">
						<label>Ship Call Number (SCN)</label>
						<input type="text" class="form-control form-control-lg" name="scn" placeholder="Enter Ship Call Number (SCN)" value="{{ $voyage->scn }}" />
						<span class="text-danger">{{ $errors->first('scn') }}</span>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>Booking Status</label>
						<select id="status_select" class="form-control form-control-lg" name="booking_status">
							<option value="0" {{ ($voyage->booking_status == 0) ? 'selected' : '' }}>Open</option>
							<option value="1" {{ ($voyage->booking_status == 1) ? 'selected' : '' }}>Closed</option>
							<option value="2" {{ ($voyage->booking_status == 2) ? 'selected' : '' }}>Cancel</option>
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<label>Closing Date/Time</label>
						<input id="closing_timestamp" type="text" autocomplete="off" class="form-control form-control-lg closingdatetimepicker1" name="closing_timestamp" placeholder="Enter Closing Date/Time" value="{{ ($voyage->closing_timestamp) ? date('d/m/Y H:i:s', strtotime($voyage->closing_timestamp)) : '' }}" />
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group {{ $errors->has('cancel_remark') ? 'has-error' : '' }}">
						<label>Cancel Remark</label>
						<input id="cancel_remark" type="text" class="form-control form-control-lg" name="cancel_remark" placeholder="Enter Cancel Remark" value="{{ $voyage->cancel_remark }}" disabled/>
						<span class="text-danger">{{ $errors->first('cancel_remark') }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-inverse">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-12">
					<h4>Port(s) Calling</h4>
				</div>
			</div>
			<hr>
			<div class="table-responsive m-b-20" style="position: relative;">
				<table id="ports_table" class="table table-bordered text-center f-s-14">
					<thead class="thead-custom">
						<tr>
							<th>Port Code</th>
							<th>Port Name</th>
							<th>ETA</th>
							<th>ATA</th>
							<th>ETD</th>
							<th>ATD</th>
							{{-- <th>Initial POL</th> --}}
							<th width=200px>Options</th>
						</tr>
					</thead>
					<tbody class="bg-white">
						<tr id="no_port"><td colspan=8>No ports to display.</td></tr>
					</tbody>
				</table>
				<div class="loader-port">
					<div class="loader-port-item text-center">
						<i class="fas fa-3x fa-spinner fa-spin"></i>
					</div>
				</div>
			</div>
			<div data-id="1" class="row port-row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>Port</label>
						<select id="port" class="form-control form-control-lg">
							<option disabled selected value> -- select an option -- </option>
							@foreach($ports as $port)
							<option value="{{ $port->id }}" data-name="{{ $port->name }}">{{ $port->code }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label>ETA</label>
						<input id="port_eta" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ETA" />
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label>ATA</label>
						<input id="port_ata" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ATA" />
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label>ETD</label>
						<input id="port_etd" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ETD" />
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label>ATD</label>
						<input id="port_atd" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ATD" />
					</div>
				</div>
			</div>
			<button id="add_port" type="button" class="btn btn-success pull-right">Add Port</button>
		</div>
	</div>

	<input hidden="" name="txt_portcalling">

	<button type="button" class="btn btn-danger btn-action m-r-10 btn_cancel">Cancel</button>
	<button type="button" class="btn btn-primary btn-action btn-save">Save</button>
</form>

<div class="modal fade" id="modal_edit_port">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Port</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Port</label>
							<select id="port" class="form-control form-control-lg">
								<option disabled selected value> -- select an option -- </option>
								@foreach($ports as $port)
								<option value="{{ $port->id }}" data-name="{{ $port->name }}">{{ $port->code }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>ETA</label>
							<input id="edit_port_eta" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ETA" />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>ATA</label>
							<input id="edit_port_ata" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ATA" />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>ETD</label>
							<input id="edit_port_etd" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ETD" />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>ATD</label>
							<input id="edit_port_atd" type="text" class="form-control form-control-lg datetimepicker" placeholder="Enter ATD" />
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a id="cancel_edit" href="javascript:;" class="btn btn-white">Cancel</a>
				<a id="save_port" href="javascript:;" class="btn btn-success">Save Changes</a>
			</div>
		</div>
	</div>
</div>
@stop

@section('page_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script src="/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />

<script src="/js/voyage/create_voyage.js"></script>
<script src="/js/voyage/voyage_schedule.js"></script>

<script>
	$(document).ready(function() {

		$('.btn_cancel').click(function(e) {
			e.preventDefault();
			swal({
				title: 'Are you sure?',
				text: 'Any unsaved progress will be lost.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Proceed',
				reverseButtons: true
			}).then((result) => {
				if(result.value) {
					window.location.replace('{{ action('VoyageController@index') }}');
				}
			});
		});

		@foreach($voyage->destinations()->get() AS $dest)

		var portObj = {};
		portObj.dest_id = "{{ $dest->id }}";
		portObj.id = "{{ $dest->getPort->id }}";
		portObj.code = "{{ $dest->getPort->code }}";
		portObj.name = "{{ $dest->getPort->name }}";
		portObj.eta = moment("{{ date('d/m/Y', strtotime($dest->eta)) }}", 'DD/MM/YYYY');
		portObj.ata = moment("{{ $dest->ata ? date('d/m/Y', strtotime($dest->ata)) : '' }}", 'DD/MM/YYYY');
		portObj.etd = moment("{{ date('d/m/Y', strtotime($dest->etd)) }}", 'DD/MM/YYYY');
		portObj.atd = moment("{{ $dest->atd ? date('d/m/Y', strtotime($dest->atd)) : '' }}", 'DD/MM/YYYY');

		console.log(portObj);

		$("#port option[value='" + portObj.id + "']").prop('disabled', true);

		addPort('create', portObj);

		// var portETA = '{{ date('d/m/Y', strtotime($dest->eta)) }}';
		// var portETD = '{{ date('d/m/Y', strtotime($dest->etd)) }}';

		// @if($dest->ata == null || $dest->ata == '-')
		// var portATA = '-';
		// @else
		// var portATA = '{{ date('d/m/Y', strtotime($dest->ata)) }}';
		// @endif

		// @if($dest->atd == null || $dest->atd == '-')
		// var portATD = '-';
		// @else
		// var portATD = '{{ date('d/m/Y', strtotime($dest->atd)) }}';
		// @endif

		// insertPort('{{ $dest->getPort->id }}', '{{ $dest->getPort->code }}', '{{ $dest->getPort->name }}', portETA, portATA, portETD, portATD);
		// $('#ports_table tbody tr:last-child').attr('data-id', '{{ $dest->id }}');
		// $('#port option[value=' + {{ $dest->getPort->id }} + ']').prop('hidden', true).prop("disabled", true);
		@endforeach

		// $(document).on('click', '.btn-remove1', function() {

		// 	var voy_dest_id = $(this).closest('tr').attr('data-dest-id');
		// 	var elem = $(this);

		// 	$.get("/checkPortAssigned/" + voy_dest_id, function(data){
		// 		if(data.success){
		// 			swal("Unable to remove port.", data.msg, "warning");
		// 		} else {
		// 			swal({
		// 				title: 'Are you sure?',
		// 				text: 'This can\'t be undone.',
		// 				type: 'warning',
		// 				showCancelButton: true,
		// 				confirmButtonText: 'Delete',
		// 				reverseButtons: true
		// 			}).then((result) => {
		// 				if(result.value) {
		// 					var portID = elem.closest('tr').attr('data-port');

		// 					$('#port option[value=' + portID + ']').attr('hidden', false).removeAttr("disabled");
		// 					$('#modal_edit_port #port option[value=' + portID + ']').prop('hidden', false).removeAttr("disabled");
		// 					elem.closest('tr').remove();

		// 					portCount--;

		// 					if (portCount == 0) {
		// 						$('#ports_table tbody').append('<tr id="no_port"><td colspan=6>No ports to display.</td></tr>');
		// 					}

		// 					$('#port').select2();
		// 				}
		// 			});
		// 		}
		// 	});
		// });


		$("#modal_edit_port").on('shown.bs.modal', function (e) {
			$("#modal_edit_port #port").select2({
				dropdownParent: $("#modal_edit_port")
			});
		});

		$("#vessel_id").trigger("change");

		var voyage_id = "{{ $voyage->id }}";

		validate_url = "/validate/createvoyage/" + voyage_id;

		// $('.btn-save').click(function(e) {
		// 	e.preventDefault();

		// 	var isClosingDisabled = $('#closing_timestamp').prop('disabled');
		// 	var valid = true;

		// 	if($('#voyage_id').val() == '') {
		// 		showSwal('error', 'Error', '<b>Voyage No</b> can\'t be empty!');
		// 		valid = false;
		// 	} else if($('#vessel_id').val() == null) {
		// 		showSwal('error', 'Error', '<b>Assigned Vessel</b> can\'t be empty!');
		// 		valid = false;
		// 	}  else if(isClosingDisabled == false && $('#closing_timestamp').val() == '') {
		// 		showSwal('error', 'Error', '<b>Closing Timestamp</b> can\'t be empty!');
		// 		valid = false;
		// 	} else if($('#booking_status').val() == 2 && $('#cancel_remark').val() == '') {
		// 		showSwal('error', 'Error', '<b>Cancel Remark</b> can\'t be empty!');
		// 		valid = false;
		// 	} else if(portCount < 2) {
		// 		showSwal('error', 'Error', '<b>Port(s) Calling</b> must be at least 2!');
		// 		valid = false;
		// 	}

		// 	if(valid) {
		// 		var count = $('#ports_table tbody tr').length;
		// 		for(var i = 0; i < count; i++) {
		// 			var elem = $('#ports_table tbody tr:nth-child(' + (i + 1) + ')');
		// 			var details = [];

		// 			details.push(elem.attr('data-id'));
		// 			details.push(elem.attr('data-port'));
		// 			details.push(elem.find('td:nth-child(3)').text());
		// 			details.push(elem.find('td:nth-child(4)').text());
		// 			details.push(elem.find('td:nth-child(5)').text());
		// 			details.push(elem.find('td:nth-child(6)').text());

		// 			var port = details.join(';');

		// 			$('#voyage_form').append('<input type="hidden" name="port[]" value=' + port + ' />');
		// 		}

		// 		//Make sure voyage no and vessel is unique
		// 		var voy_id = $("#voyage_id").val().trim();
		// 		var ves_id = $("#vessel_id").val();

		// 		$(".btn-save").attr("disabled", "");

		// 		$.ajax({
		// 			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
		// 			url: '/validate/createvoyage/' + voyage_id,
		// 			type: 'POST',
		// 			data:
		// 			{
		// 				'voyage_id': voy_id,
		// 				'vessel_id': ves_id
		// 			},
		// 			success: function(data){
		// 				if(!data.success){
		// 					showSwal('error', 'Oops!', data.msg);
		// 					$(".btn-save").removeAttr("disabled");
		// 				} else {

		// 					//Format date before pass to backend
		// 					ports.forEach(function(port){
		// 						port.eta = (port.eta.isValid()) ? port.eta.format('DD/MM/YYYY') : null;
		// 						port.ata = (port.ata.isValid()) ? port.ata.format('DD/MM/YYYY') : null;
		// 						port.etd = (port.etd.isValid()) ? port.etd.format('DD/MM/YYYY') : null;
		// 						port.atd = (port.atd.isValid()) ? port.atd.format('DD/MM/YYYY') : null;
		// 					});

		// 					//Add port obj into input
		// 					$("input[name='txt_portcalling']").val(JSON.stringify(ports));

		// 					$('#voyage_form').submit();
		// 				}
		// 			}
		// 		});
		// 	} else {
		// 		$(this).prop("disabled", false);
		// 	}
		// });

	});
</script>
@stop
