@extends("layouts.nav")
@section('page_title', 'Voyage Schedule')

@section('breadcrumb')
<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item">
		<button id="btn_filter" type="button" class="btn btn-primary m-r-5"><i class="fa fa-sliders-h m-r-5"></i>Filter</button>
		<a href="{{ action('VoyageController@create') }}"><button type="button" class="btn btn-primary"><i class="fa fa-plus m-r-10"></i>Create Voyage</button></a>
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Voyage Schedule</h1>

@if(session('success'))
<div class="alert alert-green fade show">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('success') }}
</div>
@endif

@if(session('error'))
<div class="alert alert-danger">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{!! session('error') !!}
</div>
@endif

@if(session('filtered'))
<div class="alert alert-green fade show">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{!! session('filtered') !!}
</div>
@endif

<form method="POST" action="/voyage/filter">
	@csrf
	<div id="div_filter" style="display: none" class="panel panel-inverse">
		<div class="panel-heading">
			<h5 class="m-b-0 text-white">Filters</h5>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-3 m-b-10">
					<div class="form-group">
						<label>Date Range</label>
						<div class="input-group input-daterange">
							<input type="text" class="form-control form-control-lg filter-date" autocomplete="off" name="filter_date_start" placeholder="Date Start" />
							<span class="input-group-addon" style="line-height: 33px;">to</span>
							<input type="text" class="form-control form-control-lg filter-date" autocomplete="off" name="filter_date_end" placeholder="Date End" />
						</div>
					</div>
				</div>
				<div class="col-lg-3 m-b-10">
					<div class="form-group">
						<label>Vessel</label>
						<select class="form-control form-control-lg" name="filter_vessel">
							<option selected value> -- select an option -- </option>
							@foreach($vessels AS $vessel)
							<option value="{{ $vessel->id }}">{{ $vessel->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-3 m-b-10">
					<div class="form-group">
						<label>Port of Loading</label>
						<select class="ddl form-control form-control-lg" name="filter_pol">
							<option selected value> -- select an option -- </option>
							@foreach($ports AS $port)
							<option value="{{ $port->id }}">{{ $port->code }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-3 m-b-10">
					<div class="form-group">
						<label>Destination Port</label>
						<select class="ddl form-control form-control-lg" name="filter_destination">
							<option selected value> -- select an option -- </option>
							@foreach($ports AS $port)
							<option value="{{ $port->id }}">{{ $port->code }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<p class="m-b-0"><i>* Leave blank if you do no wish to use any filters</i></p>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row justify-content-center">
				<div class="col-lg-2">
					<a href="{{ action('VoyageController@index') }}" class="btn btn-default btn-block">Reset</a>
				</div>
				<div class="col-lg-2">
					<button id="btn_submit" type="submit" class="btn btn-primary btn-block">Apply Filter</button>
				</div>
			</div>
		</div>
	</div>
</form>

<div class="table-responsive-md">
	<table id="table_vessels" class="table table-bordered table-valign-middle text-center f-s-14">
		<thead class="thead-custom">
			<tr>
				<th>Vessel</th>
				<th>Voyage</th>
				<th>POL</th>
				<th class="no-wrap">Arrival Date</th>
				<th class="no-wrap">Departure Date</th>
				<th width="1%" class="no-wrap">Vessel Capacity (TUES/MT)</th>
				<th width="1%">Status</th>
				<th>Closing Date</th>
				<th width="1%"></th>
			</tr>
		</thead>
		<tbody class="bg-white">
			@if(count($voyages) > 0)
			@foreach($voyages as $voyage)
			<tr>
				<td>{{ $voyage->vessel_name }}</td>
				<td>{{ $voyage->voyage_id }}</td>
				<td>{{ $voyage->pol_code }}</td>
				<td @if ($voyage->ata_pol) class="no-wrap col-ata m-l-10" @else class="no-wrap col-eta m-l-10" @endif>
					@if($voyage->ata_pol)
					{{ date('d/m/Y', strtotime($voyage->ata_pol)) }}
					@else
					{{ date('d/m/Y', strtotime($voyage->eta_pol)) }}
					@endif
				</td>
				<td @if ($voyage->atd_pol) class="no-wrap col-ata m-l-10" @else class="no-wrap col-eta m-l-10" @endif>
					@if($voyage->atd_pol)
					{{ date('d/m/Y', strtotime($voyage->atd_pol)) }}
					@else
					{{ date('d/m/Y', strtotime($voyage->etd_pol)) }}
					@endif
				</td>
				<td>
					@if($voyage instanceof App\Voyage)
					@if($voyage->totalUsedWeight() > 0)
					<span data-toggle="tooltip" data-html="true" title="Reserved: {{ $voyage->getReservedWeight() }} <br> Booked: {{ $voyage->getBookedWeight() }}">
						{{ $voyage->totalUsedWeight() . '/' . $voyage->vessel->dwt }}
					</span>
					@else
					<span>
						{{ $voyage->totalUsedWeight() . '/' . $voyage->vessel->dwt }}
					</span>
					@endif
					@else
					<span data-toggle="tooltip" data-html="true" title="Reserved: {{ $voyage->resv_weight }} <br> Booked: {{ $voyage->bk_weight }}">
						{{ $voyage->weight . '/' . $voyage->dwt }}
					</span>
					@endif
				</td>
				<td><b>{!! ($voyage->booking_status == 2) ? "<span class='text-indigo'>Cancelled</span>" : (($voyage->booking_status == 1) ? "<span class='text-danger'>Closed</span>" : "<span class='text-green'>Open</span>") !!}</b></td>
				<td>{{ ($voyage->closing_timestamp) ? date('d/m/Y H:i:s', strtotime($voyage->closing_timestamp)) : '-' }}</td>
				<td>
					<div class="dropdown">
						<button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-cog"></i>
						</button>
						<div class="dropdown-menu dropdown-menu-right f-s-14" aria-labelledby="dropdownMenuButton">
							@if($voyage->booking_status == 0 && Carbon\Carbon::now() < $voyage->eta_pol)
							@if($voyage instanceof App\Voyage)
							@if($voyage->totalUsedWeight() < $voyage->vessel->dwt)
							<a href="javascript:;" class="dropdown-item btn-booking" data-id="{{ $voyage->id }}">Book</a>
							@if($voyage->vessel->default_shipment == "LCL")
							<a href="javascript:;" class="dropdown-item btn-reservation" data-id="{{ $voyage->id }}">Reserve</a>
							@endif
							<div class="dropdown-divider"></div>
							@endif
							@else
							@if($voyage->weight < $voyage->dwt)
							<a href="javascript:;" class="dropdown-item btn-booking" data-id="{{ $voyage->id }}">Book</a>
							@if($voyage->default_shipment == "LCL")
							<a href="javascript:;" class="dropdown-item btn-reservation" data-id="{{ $voyage->id }}">Reserve</a>
							@endif
							<div class="dropdown-divider"></div>
							@endif
							@endif
							@endif
							<a href="javascript:;" class="dropdown-item btn-view-more" data-id="{{ $voyage->id }}"><i class="fas fa-info-circle m-r-10"></i>View More</a>
							<a href="{{ action('VoyageController@edit', Hashids::encode($voyage->id)) }}" class="dropdown-item"><i class="fas fa-edit m-r-10"></i>Edit Voyage</a>
							<a href="javascript:;" class="dropdown-item btn-delete" data-id="{{ $voyage->id }}"><i class="fa fa-trash m-r-10"></i>Delete Voyage</a>
						</div>
					</div>
				</td>
			</tr>
			@endforeach
			@else
			<tr>
				<td colspan="14">No voyages yet.</td>
			</tr>
			@endif
		</tbody>
	</table>
</div>

<div class="modal fade" id="modal_view_more">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Voyage Schedule Details</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body f-s-14">
				<!-- -->
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
				<a href="javascript:;" class="btn btn-warning btn-booking">Booking</a>
			</div>
		</div>
	</div>
</div>

<form id="form_delete" method="POST" action="{{ action('VoyageController@destroy') }}">
	@method('DELETE')
	@csrf
	<input type="hidden" name="voyage_id" />
</form>
@stop

@section('page_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />

<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<link href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css" rel="stylesheet" />

<script src="https://cdn.datatables.net/plug-ins/1.10.16/sorting/datetime-moment.js"></script>

<script src="/js/voyage/voyage.js"></script>
<script type="text/javascript">
	$(function(){
		$(".filter-date").datetimepicker({
			format: 'DD/MM/YYYY',
		});

		$(".ddl").select2();

		$.fn.dataTable.moment('DD/MM/YYYY');
		if($("#table_vessels tbody tr:first-child").find("td").length > 1){
			$("#table_vessels").DataTable({
				"columnDefs": [
				{ "orderable": false, "targets": [0,1,2,4,5,6,7,8] }
				],
				"order": [[ 3, "desc" ]],
			});
		}

		$("#btn_filter").click(function(){
			$("#div_filter").slideToggle();
			$(".ddl").select2();
		});
	});
</script>
@stop
