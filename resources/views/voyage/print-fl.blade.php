@extends("layouts.nav")
@section('page_title', 'Print Freight List')

@section('breadcrumb')
<ol class="breadcrumb pull-right btn-hidden">
	<li>
		<a href="{{ action('PrintController@index_bl') }}" class="btn-hidden"><button type="button" id="btn-print" style="display: inline-block" class="btn btn-white m-r-5">Return to Schedule</button></a>
		<button type="button" onclick="window.print();" class="btn btn-success">
			<i class="fa fa-print m-r-5"></i>
			Print
		</button>
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Print Freight List</h1>

<div class="panel p-10 bg-white table-responsive f-s-14 text-black">
	<table class="table table-borderless" style="border-bottom: 1px solid #000;">
		<tbody>
			<tr>
				<td>-</td>
				<td></td>
			</tr>
			<tr>
				<td>FREIGHT LIST</td>
				<td>Page No.: </td>
			</tr>
		</tbody>
	</table>
	<table class="table table-borderless" style="border-bottom: 1px solid #000;">
		<tbody>
			<tr>
				<td>Vessel Name<span class="pull-right">:</span></td>
				<td>{{ $voy->vessel->name }}</td>
				<td>Port of Loading</td>
				<td>{{ $voy->pol->code }}</td>
			</tr>
			<tr>
				<td>Voyage No.<span class="pull-right">:</span></td>
				<td>{{ $voy->voyage_id }}</td>
				<td>Date of Sailing from Port</td>
				<td>{{ $voy->etd_pol }}</td>
			</tr>
			<tr>
				<td>Nationality<span class="pull-right">:</span></td>
				<td>{{ strtoupper($voy->vessel->flag) }}</td>
				<td>Port of Discharge</td>
				<td>{{ $voy->lastDestination()->first()->getPort->code }}</td>
			</tr>
		</tbody>
	</table>
	<div class="row">
		@foreach($voy->getBL AS $bl)
		<div class="col-md-3">
			<table class="table table-borderless">
				<tbody>
					<tr>
						<td>BL No.: {{ $bl->bl_no }}</td>
					</tr>
					<tr>
						<td style="border-bottom: 1px solid #000 !important;">Shipper/Consignee/Notify Party</td>
					</tr>
					<tr>
						<td>
							<p class="m-0">{{ $bl->shipper_name }}</p>
							<p class="m-0">{{ $bl->consignee_name }}</p>
							<p class="m-0">{{ $bl->notify_name }}</p>
							<br>
							@foreach($bl->getCargos AS $cargo)
							{{ $cargo->cargo_name }} {{ ($bl->coc == 1) ? "COC" : "SOC" }} STC
							<p style="white-space: pre">{!! $cargo->detailed_desc !!}</p>
							@foreach($cargo->containers AS $cont)
							<p class="m-0">{{ $cont->container_no }} / {{ $cont->seal_no }}</p>
							@endforeach
							@endforeach
						</td>

					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-9">
			<table class="table table-borderless">
				<tbody>
					<tr>
						<td colspan="7">FREIGHT PAYABLE AT</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid #000 !important; padding-left: 0">Billing Party & Charges</td>
						<td style="border-bottom: 1px solid #000 !important; padding-right: 0" class="text-right">Rate</td>
						<td style="border-bottom: 1px solid #000 !important;"></td>
						<td style="border-bottom: 1px solid #000 !important; padding-right: 0" class="text-right">Amount</td>
						<td></td>
					</tr>
					@foreach($bl->getCharges->where("charge_payment", "P") AS $charge)
					@php
					$sum = $charge->where("charge_payment", "P")->sum("charge_rate");
					@endphp
					<tr>
						<td class="p-0">
							{{ $charge->charge_code }}
						</td>
						<td class="no-wrap p-0">
							1.00 UNIT
						</td>
						<td class="p-0 text-right">
							{{ $charge->charge_unit }}
						</td>
						<td class="p-0 p-r-15 text-right">
							{{ number_format($charge->charge_rate, 2) }}
						</td>
						<td class="p-0 text-left">MYR</td>
						<td class="p-0 text-right">
							{{ number_format($charge->charge_rate, 2) }}
						</td>
						<td class="p-0 text-right">{{ $charge->charge_payment }}</td>
					</tr>
					@endforeach
					<tr>
						<td class="p-l-0" style="vertical-align: middle" colspan="4">{{ $bl->shipper_name }}</td>
						<td class="text-right p-r-0 p-l-0" colspan="2">
							<div class="sum_box">
								<span class="pull-left">MYR</span>{{ !empty($sum) ? number_format($sum, 2) : 0 }}
							</div>
						</td>
						<td>&nbsp;</td>
					</tr>
					@foreach($bl->getCharges->where("charge_payment", "C") AS $charge1)
					@php
					$sum = $charge1->where("charge_payment", "C")->sum("charge_rate")
					@endphp
					<tr>
						<td class="p-0">
							{{ $charge1->charge_code }}
						</td>
						<td class="no-wrap p-0">
							1.00 UNIT
						</td>
						<td class="p-0 text-right">
							{{ $charge1->charge_unit }}
						</td>
						<td class="p-0 p-r-15 text-right">
							{{ number_format($charge1->charge_rate, 2) }}
						</td>
						<td class="p-0 text-left">MYR</td>
						<td class="p-0 text-right">
							{{ number_format($charge1->charge_rate, 2) }}
						</td>
						<td class="p-0 text-right">{{ $charge1->charge_payment }}</td>
					</tr>
					@endforeach
					<tr>
						<td class="p-l-0" style="vertical-align: middle" colspan="4">SYARIKAT PERKAPALAN SOO HUP SENG SDN BHD</td>
						<td colspan="2" class="text-right p-r-0 p-l-0">
							<div class="sum_box">
								<span class="pull-left">MYR</span>{{ !empty($sum) ? number_format($sum, 2) : 0 }}
							</div>
						</td>
						<td>&nbsp;</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-12">
			<hr style="background: #000 !important; border:1px solid #000 !important; height:0px;">
		</div>
		@endforeach
	</div>
</div>
@stop