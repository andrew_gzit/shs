@extends("layouts.nav")
@section('page_title', 'Print Manifest')

@section('breadcrumb')
<ol class="breadcrumb pull-right btn-hidden">
	<li>
		<a href="{{ action('PrintController@index_bl') }}" class="btn-hidden"><button type="button" id="btn-print" style="display: inline-block" class="btn btn-white m-r-5">Return to Schedule</button></a>
		<button type="button" onclick="window.print();" class="btn btn-success">
			<i class="fa fa-print m-r-5"></i>
			Print
		</button>
	</li>
</ol>
@stop

@section('content')
<h1 class="page-header">Print Manifest</h1>

<div class="panel p-10 bg-white table-responsive">
	<table class="table table-borderless table-condensed f-s-14 manifest" style="color: #000; min-width: 1600px;">
		<tbody>
			<tr>
				<td colspan="5">
					<strong class="f-s-15">SYARIKAT PERKAPALAN SOO HUP SENG SDN BHD</strong>
				</td>
			</tr>
			<tr>
				<td colspan="10">
					<div class="kastam">
						KASTAM DI RAJA MALAYSIA / ROYAL MALAYSIA CUSTOMS<br>
						DAFTAR MUATAN KELUAR / OUTWARD MANIFEST<br>
						(SEKSYEN DAN AKTA KASTAM 1967)<br>
						(SECTIONS 57 DAN 58 OF THE CUSTOMS ACT 1967)
					</div>
				</td>
			</tr>
		<!-- 	<tr>
				<td width="1%" class="no-wrap">NAMA BAHTERA</td>
				<td><span class="m-content">LEMIN</span></td>
				<td width="20%">&nbsp;</td>
				<td width="1%" class="no-wrap">BANGSA</td>
				<td width="10%"><span class="m-content">MALAYSIAN</span></td>
				<td width="1%" class="no-wrap">KASTAM NO.</td>
				<td width="10%"><span class="m-content">5</span></td>
			</tr> -->
	<!-- 		<tr>
				<td width="1%" class="no-wrap">NAME OF VESSEL</td>
				<td>
					<span class="m-content m-empty-content">
						<hr>
					</span>
				</td>
				<td width="20%">&nbsp;</td>
				<td width="1%" class="no-wrap">NATIONALITY</td>
				<td>
					<span class="m-content m-empty-content">
						<hr>
					</span>
				</td>
				<td width="1%" class="no-wrap">CUSTOM NO.</td>
				<td>
					<span class="m-content m-empty-content">
						<hr>
					</span>
				</td>
			</tr> -->
			<tr>
				<td class="no-wrap">NAMA BAHTERA</td>
				<td><span class="m-content">{{ $voyage->vessel->name }}</span></td>
				<td class="no-wrap">VOY</td>
				<td><span class="m-content">{{ $voyage->voyage_id }}</span></td>
				<td></td>
				<td></td>
				<td>BANGSA</td>
				<td><span class="m-content text-uppercase">{{ $voyage->vessel->flag }}</span></td>
				<td>KASTAM NO.</td>
				<td><span class="m-content">5</span></td>
			</tr>
			<tr>
				<td class="no-wrap">NAME OF VESSEL</td>
				<td colspan="3"><span class="m-content m-empty-content"><hr></span></td>
				<td></td>
				<td></td>
				<td>NATIONALITY</td>
				<td><span class="m-content m-empty-content"><hr></span></td>
				<td>CUSTOM NO.</td>
				<td><span class="m-content m-empty-content"><hr></span></td>
			</tr>
			<tr height="20px"></tr>
			<tr>
				<td width="1%" class="no-wrap">WAKIL</td>
				<td><span class="m-content">SYARIKAT PERKAPALAN SOO HUP SENG SDN BHD</span></td>
				<td width="1%" class="no-wrap">TARIKH BERLEPAS</td>
				<td><span class="m-content text-uppercase">{{ $voyage->eta_pol->format('d M Y') }}</span></td>
				<td width="1%" class="no-wrap">DARI PELABUHAN</td>
				<td><span class="m-content">PENANG</span></td>
				<td width="1%" class="no-wrap">MENUJU KE MANA</td>
				<td><span class="m-content">{{ $voyage->fpd->location }}</span></td>
				<td width="1%" class="no-wrap">KAPITAN BAHTERA</td>
				<td><span class="m-content"></span></td>
			</tr>
			<tr>
				<td width="1%" class="no-wrap">AGENT</td>
				<td><span class="m-content m-empty-content"><hr></span></td>
				<td width="1%" class="no-wrap">DATE OF DEPARTURE</td>
				<td><span class="m-content m-empty-content"><hr></span></td>
				<td width="1%" class="no-wrap">FROM PORT</td>
				<td><span class="m-content m-empty-content"><hr></span></td>
				<td width="1%" class="no-wrap">WHITHER BOUND</td>
				<td><span class="m-content m-empty-content"><hr></span></td>
				<td width="1%" class="no-wrap">NAME OF CAPTAIN</td>
				<td><span class="m-content m-empty-content"><hr></span></td>
			</tr>
		</tbody>
	</table>
	<table class="table table-manifest2-content f-s-14" style="color: #000;">
		@foreach($voyage->getBL AS $bl)
		<tr>
			<td colspan="7">BL No.: {{ $bl->bl_no }}</td>
		</tr>
		<tr class="tr-bl">
			<td width="20%">
				<p class="manifest-title">Shipper/Consignee/Notify Party</p>
				<p>
					{{ $bl->shipper->name }}
					<br>
					{{ $bl->shipper_add }}
				</p>
				<p>
					{{ $bl->consignee->name }}
					<br>
					{{ $bl->consignee_add }}
				</p>
				@if($bl->notify_add == $bl->consignee_add)
				<p>SAME AS CONSIGNEE</p>
				@else
				<p>
					{{ $bl->notify->name }}
					<br>
					{{ $bl->notify_add}}
				</p>
				@endif
			</td>
			<td width="1%" class="no-wrap">
				<p class="manifest-title">Container No. / Markings</p>
				<!-- @foreach($bl->getCargos AS $cargo)
				@if(strpos($cargo->cargo_name, "&") == -1)
				<p class="m-0">{{ $cargo->cargo_name }} / {!! nl2br($cargo->markings) !!}</p>
				@else
				@foreach(explode("&", $cargo->cargo_name) AS $ea)
				<p class="m-0">{{ $ea }} / {!! nl2br($cargo->markings) !!}</p>
				@endforeach
				@endif
				@endforeach -->
				@foreach($bl->getCargos AS $cargo)
					@foreach($cargo->containers AS $cont)
					<p class="m-0">{{ $cont->container_no }} / {{ $cont->seal_no }}</p>
					@endforeach
					<br>
					<p>{{ $cargo->markings }}</p>
				@endforeach
			</td>
			<td>
				<p class="manifest-title">Cargo Description</p>
				@foreach($bl->getCargos AS $cargo)
				<p class="m-0">{{ $cargo->cargo_name }}</p>
				<p class="m-0">{{ $cargo->cargo_desc }}</p>
				<p class="m-0">{!! nl2br($cargo->detailed_desc) !!}</p>
				@endforeach
			</td>
			<td width="1%" class="no-wrap">
				<p class="manifest-title">Gross Weight</p>
				@php
				$sum_weight = 0;
				@endphp
				@foreach($bl->getCargos AS $cargo)
				@php
				$sum_weight += $cargo->weight
				@endphp
				<p class="m-0">{{ number_format($cargo->weight, 3) }} {{ $cargo->weight_unit }}</p>
				<p class="m-0">{{ number_format($sum_weight, 3) }} {{ $cargo->weight_unit }}</p>
				@endforeach
			</td>
			<td width="1%" class="no-wrap">
				<p class="manifest-title">Measurement</p>
				@foreach($bl->getCargos AS $cargo)
				<p class="m-0">{{ number_format($cargo->volume, 3) }} M3</p>
				@endforeach
			</td>
			<td>
				<p class="manifest-title">Remarks</p>
				@foreach($bl->getCargos AS $cargo)
				<p class="m-0">{!! nl2br($cargo->remarks) !!}</p>
				@endforeach
			</td>
		</tr>
		@endforeach
	</table>
	<!-- <table class="table table-borderless table-manifest-content f-s-14" style="min-width: 1600px">
		<thead>
			<tr>
				<th width="1%">NO. S.M<br>B/L NO.</th>
				<th width="1%">P.MUAT<br>PT.LOAD</th>
				<th width="15%">PENGIRIM<br>CONSIGNORS</th>
				<th width="15%">DIKIRIM KEPADA<br>CONSIGNEES</th>
				<th>JUM. BUNGKUS<br>QTY PKGS.</th>
				<th>TANDA-TANDA ATAS BUNGKUSAN<br>MARKS AND NUMBERS</th>
				<th>KETERANGAN BARANG-BARANG<br>DESCRIPTION OF GOODS</th>
				<th width="1%" class="no-wrap">BANYAKNYA<br>M/TON</th>
				<th width="1%" class="no-wrap">ATAU BERAT<br>M3</th>
			</tr>
		</thead>
		<tbody>
			@foreach($voyage->getBL AS $bl)
			<tr>
				<td>{{ $bl->bl_no }}</td>
				<td>{{ strtoupper($bl->pol->location) }}</td>
				<td>
					<p>{{ $bl->shipper->name }}</p>
					{{ $bl->shipper_add }}
				</td>
				<td>
					<p>{{ $bl->consignee->name }}</p>
					{{ $bl->consignee_add }}
					<br>
					@if($bl->notify_add == $bl->consignee_add)
					<br>SAME AS CONSIGNEE
					@else
					{{ $bl->notify->name }}
					<br>
					{{ $bl->notify_add}}
					@endif
				</td>
				<td>
					@foreach($bl->getCargos AS $cargo)
					@if(strpos($cargo->cargo_name, "&") == -1)
					<p class="m-0">{{ $cargo->cargo_name }}</p>
					@else
					@foreach(explode("&", $cargo->cargo_name) AS $ea)
					<p class="m-0">{{ $ea }}</p>
					@endforeach
					@endif
					@endforeach
					<div class="sum_box">
						{{ $bl->getTotalQty() }}
					</div>
				</td>
				<td>
					@foreach($bl->getCargos AS $cargo)
					<p class="m-0">{!! nl2br($cargo->markings) !!}</p>
					@endforeach
				</td>
				<td>
					@foreach($bl->getCargos AS $cargo)
					<p class="m-0">{{ $cargo->cargo_desc }}</p>
					<p class="m-0">{!! nl2br($cargo->detailed_desc) !!}</p>
					@endforeach
				</td>
				<td>
					@foreach($bl->getCargos AS $cargo)
					<p class="m-0">{{ number_format($cargo->weight, 3) }}</p>
					@endforeach
				</td>
				<td>
					@foreach($bl->getCargos AS $cargo)
					<p class="m-0">{{ number_format($cargo->volume, 3) }}</p>
					@endforeach
				</td>
			</tr>
			@endforeach
		</tbody>
	</table> -->
</div>
@stop

@section("page_script")
<style type="text/css">
@media print{
	@page{
		size: 20in 14in;
	}
}
</style>
@stop