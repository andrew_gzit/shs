<?php

use Illuminate\Support\Facades\Mail;
use App\Mail\BookingConfirmation_FCL;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'NormalController@login')->name('login');
Route::post('/authenticate', 'NormalController@authenticate');
Route::get('/logout', 'NormalController@logout');


Route::middleware(['auth'])->group(function () {
	Route::get('/', 'NormalController@index');





	Route::post('/minify-nav', 'NormalController@minify_nav');

/*
 * Email Routes
 */
Route::get('/mail', function () {
	$booking = App\Booking::find(13);

	Mail::to("andrews.kkg@gmail.com")->send(new BookingConfirmation_FCL($booking));
    // return new App\Mail\BookingConfirmation_FCL($booking);
});

Route::get('/inw_bl', function(){
	$bl = App\InwardBL::find(11);
	Mail::to("andrews.kkg@gmail.com")->send(new App\Mail\InwardBillLading($bl));
	// return new App\Mail\InwardBillLading($bl);
});

/*
 * Reservation Routes
 */
Route::get('/reservation', 'ReservationController@index');
Route::get('/getReservationDetail/{id}', 'ReservationController@getReservationDetail');

Route::get('/get-voyage/{pol}/{pod}/{date}', 'ReservationController@get_voyage');
Route::get('/get-voyage1/{voy_id}/{pol}/{pod}/{date}', 'ReservationController@get_voyage1');

Route::post('/reservation', 'ReservationController@store');
Route::get('/get-reservation/{id}', 'ReservationController@get_reservation');
Route::put('/reservation/{id}', 'ReservationController@update');
Route::delete('/reservation/{id}', 'ReservationController@delete');

/*
 * Booking Routes
 */
Route::get('/booking', 'BookingController@index');
Route::post('/email-booking/{bk_id}', 'BookingController@email_booking');
Route::get('/checkTransferBL/{bk_id}', 'BookingController@check_transfer_bl');

Route::get('/booking/create', 'BookingController@create');

Route::get('/check-restriction/{company_id}/{voy_id}', 'BookingController@check_restriction');

Route::post('/booking/create', 'BookingController@store');
Route::get('/booking/{bk_no}/edit', 'BookingController@edit');
Route::put('/booking/{id}/edit', 'BookingController@update');
Route::delete('/booking/{id}', 'BookingController@delete');

Route::post('/booking/{bk_no}/recover', 'BookingController@recover_bk');

Route::get('/get-booking/{id}', 'BookingController@get_booking');

Route::get('/get-vessel-shipment/{voy_id}/{vessel_id?}', 'BookingController@get_vessel_shipment');
Route::get('/get-destinations/{voy_id}', 'BookingController@get_destinations');

Route::get('/booking/{hash}/cargo', 'BookingController@create_cargo');
Route::post('/booking/{hash}/cargo', 'BookingController@store_cargo');

/*
 * Voyage Schedule Routes
 */
Route::get('/voyage', 'VoyageController@index')->name('index-voyage');
Route::post('/voyage', 'VoyageController@voyage_details');
Route::post('/voyage/filter', 'VoyageController@filter');
Route::get('/voyage/create', 'VoyageController@create');

Route::post('/validate/createvoyage/{voyage_id?}', 'VoyageController@validate_create');

Route::post('/voyage/create', 'VoyageController@store');
Route::get('/voyage/edit/{hash}', 'VoyageController@edit');
Route::put('/voyage/edit/{id}', 'VoyageController@update');
Route::delete('/voyage/delete', 'VoyageController@destroy');

Route::get('/voyage/{hash}/print-fl', 'VoyageController@print_fl');

Route::get('/checkPortAssigned/{voy_dest_id}', 'VoyageController@checkPortAssigned');

Route::get('/voyage/{code}/print-manifest', 'VoyageController@print_manifest');

Route::get('/bl', 'BillLadingController@index')->name('index-bl');
Route::post('/billLadingDT', 'BillLadingController@billLadingDT');

Route::delete('/bl/batch-delete', 'BillLadingController@batch_delete');
Route::post('/bl/{hash}/recover', 'BillLadingController@recover_bl');

Route::post('/bl/list', 'BillLadingController@get_bls');
Route::post('/bl/voyages/list', 'BillLadingController@get_voyages');
Route::post('bl/vessel/details', 'BillLadingController@vessel_details');
Route::get('/bl/create', 'BillLadingController@create');
Route::post('/bl/create', 'BillLadingController@store');
Route::post('/bl/create/company/address', 'BillLadingController@company_address');
Route::post('/bl/part', 'BillLadingController@part_bl');
Route::get('/bl/edit/{hash}', 'BillLadingController@edit');
Route::put('/bl/edit/{id}', 'BillLadingController@update');

Route::get('/validate-voyage/{bl_id}/{voy_id}', 'BillLadingController@validate_voyage');

Route::delete('/bl/edit/{code}', 'BillLadingController@destroy');
Route::get('/bl/{hash}/cargo', 'BillLadingController@cargo')->name('index-cargo');
Route::get('/bl/{hash}/cargo/create', 'BillLadingController@create_cargo');
Route::post('/bl/{hash}/cargo/create', 'BillLadingController@store_cargo');
Route::get('/bl/{hashBL}/cargo/{hashCG}/edit', 'BillLadingController@edit_cargo');
Route::post('/bl/{hashBL}/cargo/{hashCG}/edit', 'BillLadingController@update_cargo');
Route::delete('/bl/cargo/delete/{id}', 'BillLadingController@delete_cargo');
Route::get('/bl/{hash}/charges', 'BillLadingController@charges');
Route::post('/bl/{hash}/charges/create', 'BillLadingController@store_charges');
Route::post('/bl/{hash}/charges/edit', 'BillLadingController@edit_charges');
Route::get('/bl/charges/delete/{id}', 'BillLadingController@delete_charges');

//For BL details redirect to print BL
Route::get('/bl-print/{id}/{format}/{cname1?}/{cname2?}', 'BillLadingController@redirect_format');

Route::post('/bl/set-format', 'BillLadingController@set_format_bl');
Route::get('/bl/{hash}/print', 'BillLadingController@print_bl');
Route::get('/bl/{hash}/print-so', 'BillLadingController@print_so');

Route::get('/shortcut-list', 'BillLadingController@shortcut_list');

Route::get('/get-address/{id}', 'BillLadingController@get_address');


/*
 * Inward BL Routes
 */
Route::get('/inward-bl/create', 'InwardBLController@create');
Route::get('/voyage-list/{vessel_name}', 'InwardBLController@voyage_list');
Route::get('/get-ports', 'InwardBLController@get_ports');
Route::post('/inward-bl/create', 'InwardBLController@store');
Route::get('/inward-bl/{hashed}', 'InwardBLController@edit');
Route::post('/inward-bl/{hashed}', 'InwardBLController@update');
Route::delete('/inward-bl/', 'InwardBLController@delete');

Route::get('/inward-bl-check/{bl_no}', 'InwardBLController@inw_bl_check');

Route::post('/email-consignee/{id}', 'InwardBLController@email_consignee');
Route::get('/noa_pdf/{id}', 'InwardBLController@noa_pdf');

Route::post('/uploadEdi', 'InwardBLController@upload_edi');


/*
 * Printing Routes
 */
// Route::get('/print/manifest', 'PrintController@index_manifest2');
Route::get('/print/bl', 'PrintController@index_bl');
Route::get('/print/get-bl/{id}', 'PrintController@get_bl');

//Get inward BL
Route::get('/print/get-bl-inw/{id}', 'PrintController@get_bl_inw');

Route::post('/print/bl-print', 'PrintController@view_print_bl');
Route::post('/print/so-print', 'PrintController@view_print_so');

Route::get('/get-cargo/{id}', 'PrintController@get_cargo');
Route::get('/get-container/{cargo_id}', 'PrintController@get_container');

Route::get('/print/voyage', 'PrintController@index_voyage');
Route::post('/print/voyage-print', 'PrintController@view_print_voyage');

Route::get('/malsuria', 'PrintController@index_malsuria');
Route::get('/shinyang', 'PrintController@index_shinyang');

Route::get('/print/cargo-status-report', 'PrintController@index_cargostatus');
Route::get('/cargo-status/{voyage_id}', 'PrintController@get_cargostatus');

Route::get('/print/frieght-list', 'PrintController@print_freightlist');
//New print manifest
Route::get('/print/manifest', 'PrintController@print_manifest');
Route::get('/print/manifest-inward', 'PrintController@print_manifest_inw');

/*
 * Maintenance Routes
 */
Route::get('/maintenance/base-company', 'MaintenanceController@index_basecompany');
Route::post('/maintenance/base-company', 'MaintenanceController@store_basecompany');

Route::get('/maintenance/companies', 'MaintenanceController@index_company');
Route::get('/maintenance/companies/create', 'MaintenanceController@create_company');
Route::post('/maintenance/companies/create', 'MaintenanceController@store_company');
Route::get('/maintenance/companies/edit/{hash}', 'MaintenanceController@edit_company');
Route::put('/maintenance/companies/edit/{id}', 'MaintenanceController@update_company');
Route::delete('/maintenance/companies/delete/{id}', 'MaintenanceController@delete_company');

Route::get('/check-company-delete/{id}', 'MaintenanceController@check_company_delete');

Route::get('/maintenance/companies/create/{hash}/charges', 'MaintenanceController@index_companycharges');
Route::post('/maintenance/companies/create/{id}/charges', 'MaintenanceController@store_companycharges');
Route::get('/maintenance/companies/edit/{id}/charges', 'MaintenanceController@edit_companycharges');
Route::put('/maintenance/companies/edit/{id}/charges', 'MaintenanceController@update_companycharges');

Route::get('/charge-detail/{id}/{bl_id?}/{payment?}', 'MaintenanceController@get_chargedetail');
Route::post('/default-charge/edit/{id}', 'MaintenanceController@update_defaultcharge');
Route::delete('/default-charge/{id}', 'MaintenanceController@delete_defaultcharge');

// View default charges details
Route::get('/maintenance/viewDefaultCharge/{shipper_id}/{charge_id}', 'MaintenanceController@viewDefaultCharge');
Route::get('/maintenance/getDefaultCharge/{charge_id}', 'MaintenanceController@getDefaultCharge');

Route::get('/maintenance/vessels', 'MaintenanceController@index_vessel')->name('index-vessel');
Route::get('/maintenance/vessels/create', 'MaintenanceController@create_vessel');
Route::post('/maintenance/vessels/create', 'MaintenanceController@store_vessel');
Route::get('/maintenance/vessels/edit/{hash}', 'MaintenanceController@edit_vessel');
Route::put('/maintenance/vessels/edit/{id}', 'MaintenanceController@put_vessel');
Route::delete('/maintenance/vessels/delete', 'MaintenanceController@delete_vessel');

Route::get('/maintenance/ports', 'MaintenanceController@index_port')->name('index-port');
Route::get('/maintenance/ports/create', 'MaintenanceController@create_port');
Route::post('/maintenance/ports/create', 'MaintenanceController@store_port');
Route::get('/maintenance/ports/edit/{hash}', 'MaintenanceController@edit_port');
Route::put('/maintenance/ports/edit/{id}', 'MaintenanceController@put_port');
Route::delete('/maintenance/ports/delete', 'MaintenanceController@delete_port');

Route::get('/maintenance/charges', 'MaintenanceController@index_charge')->name('index-charge');;
Route::get('/maintenance/charges/create', 'MaintenanceController@create_charge');
Route::post('/maintenance/charges/create', 'MaintenanceController@store_charge');
Route::get('/maintenance/charges/edit/{hash}', 'MaintenanceController@edit_charge');
Route::put('/maintenance/charges/edit/{id}', 'MaintenanceController@put_charge');
Route::delete('/maintenance/charges/delete/{id}', 'MaintenanceController@delete_charge');

Route::get('/maintenance/packings', 'MaintenanceController@index_packing_type');
Route::get('/maintenance/packings/create', 'MaintenanceController@create_packing_type');

Route::get('/maintenance/shortcut', 'MaintenanceController@index_shortcut');
Route::get('/maintenance/shortcut/edit/{hash}', 'MaintenanceController@edit_shortcut');
Route::post('/maintenance/shortcut/edit/{id}', 'MaintenanceController@update_shortcut');

});
